inherit core-image

# We need stateless-rootfs, otherwise the systemd distro features calls out to sytemctl during image build which fails
IMAGE_FEATURES = "stateless-rootfs"
EXTRA_IMAGE_FEATURES = ""

# Don't want locale-base-* (for now at least)
IMAGE_LINGUAS = ""

# Don't try to talk to pid 1 in rpm scripts
export SYSTEMD_OFFLINE = "1"

# ldconfig is statically linked, so breaks pseudo, neuter it
export PSEUDO_REWRITE_EXEC = "${IMAGE_ROOTFS}/sbin/ldconfig=${RECIPE_SYSROOT_NATIVE}/usr/bin/ldconfig,${IMAGE_ROOTFS}/usr/sbin/ldconfig=${RECIPE_SYSROOT_NATIVE}/usr/bin/ldconfig"

# Avoid accidentally talking to host
export DBUS_SYSTEM_BUS_ADDRESS = "unix:path=/does/not/exist"

CORE_IMAGE_BASE_INSTALL = "bash coreutils"
CORE_IMAGE_EXTRA_INSTALL = ""

ROOTFS_BOOTSTRAP_INSTALL = ""

# We don't do SDKs (yet), so drop all those deps
SDK_DEPENDS = ""


IMAGE_LOG_CHECK_EXCLUDES = "dbus-org.freedesktop.resolve1.service"

do_pre_setup_rootfs () {
  # Fix some ordering issue betweend filesystem creating the sbin symlink and something owning a file in /sbin
  mkdir -p ${IMAGE_ROOTFS}/usr/sbin
  ln -s usr/sbin ${IMAGE_ROOTFS}/sbin
  mkdir -p ${IMAGE_ROOTFS}/usr/lib64
  ln -s usr/lib64 ${IMAGE_ROOTFS}/lib64
}

RPM_PREPROCESS_COMMANDS = "do_pre_setup_rootfs"
