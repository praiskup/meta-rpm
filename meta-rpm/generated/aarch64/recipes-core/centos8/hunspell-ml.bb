SUMMARY = "generated recipe based on hunspell-ml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ml = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ml-0.1-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-ml.sha256sum] = "32bd6d2085d3bff0b4a06d6fcb779137b92670344e4e629cf892f3ddac803d8d"
