SUMMARY = "generated recipe based on system-config-printer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs glib-2.0 libusb1 pkgconfig-native systemd-libs"
RDEPENDS_system-config-printer-libs = "bash gobject-introspection gtk3 platform-python python3-cups python3-dbus python3-gobject python3-pycurl python3-requests"
RPM_SONAME_REQ_system-config-printer-udev = "ld-linux-aarch64.so.1 libc.so.6 libcups.so.2 libglib-2.0.so.0 libudev.so.1 libusb-1.0.so.0"
RDEPENDS_system-config-printer-udev = "cups-libs glib2 glibc libusbx platform-python system-config-printer-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/system-config-printer-libs-1.5.11-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/system-config-printer-udev-1.5.11-13.el8.aarch64.rpm \
          "

SRC_URI[system-config-printer-libs.sha256sum] = "bbf858e39acba6a9c722d038c2ba9389756e2a034abd4e82aa89c0dacfa0bdfd"
SRC_URI[system-config-printer-udev.sha256sum] = "e1f99565f78e39cd3c5e908a0eee1496516cc6e8bb02daf1565d76bd941ea0be"
