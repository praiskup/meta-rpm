SUMMARY = "generated recipe based on python-lesscpy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-lesscpy = "platform-python python3-ply python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-lesscpy-0.13.0-4.el8.noarch.rpm \
          "

SRC_URI[python3-lesscpy.sha256sum] = "1d4658cbc9782f7f5b8ba5226e240e9a6e57e3ae4740ddaad2c5e03a913a620d"
