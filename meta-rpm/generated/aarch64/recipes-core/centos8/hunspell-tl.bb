SUMMARY = "generated recipe based on hunspell-tl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-tl-0.20050109-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-tl.sha256sum] = "0292a2372658730113dc6e847e73766376b6d18a8f181e7f4c2dd2969b094368"
