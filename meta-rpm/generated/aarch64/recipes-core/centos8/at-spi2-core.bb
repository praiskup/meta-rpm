SUMMARY = "generated recipe based on at-spi2-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus dbus-libs glib-2.0 libx11 libxtst pkgconfig-native"
RPM_SONAME_PROV_at-spi2-core = "libatspi.so.0"
RPM_SONAME_REQ_at-spi2-core = "ld-linux-aarch64.so.1 libX11.so.6 libXtst.so.6 libatspi.so.0 libc.so.6 libdbus-1.so.3 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_at-spi2-core = "dbus dbus-libs glib2 glibc libX11 libXtst"
RPM_SONAME_REQ_at-spi2-core-devel = "libatspi.so.0"
RPROVIDES_at-spi2-core-devel = "at-spi2-core-dev (= 2.28.0)"
RDEPENDS_at-spi2-core-devel = "at-spi2-core dbus-devel glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/at-spi2-core-2.28.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/at-spi2-core-devel-2.28.0-1.el8.aarch64.rpm \
          "

SRC_URI[at-spi2-core.sha256sum] = "67d07364fb62b2c10228d4f9a192a0241cd167a7207c910fb41aa0621a746058"
SRC_URI[at-spi2-core-devel.sha256sum] = "e99f3002ebdf21a364bc21e37c7f6e1fe1a55525d2de95d0e7fae5d494c47aa1"
