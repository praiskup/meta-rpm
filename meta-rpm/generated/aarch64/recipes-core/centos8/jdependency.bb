SUMMARY = "generated recipe based on jdependency srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jdependency = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem objectweb-asm"
RDEPENDS_jdependency-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdependency-1.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdependency-javadoc-1.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jdependency.sha256sum] = "dbf0d483fa01babeec2116c4d2b69bab65b39dd3bbef946050f87952c53e2365"
SRC_URI[jdependency-javadoc.sha256sum] = "d8ebedbc14125fe9923b6cd0dbaabf352e701baff7b4c616a983055f26e18327"
