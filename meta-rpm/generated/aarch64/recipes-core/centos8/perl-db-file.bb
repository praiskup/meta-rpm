SUMMARY = "generated recipe based on perl-DB_File srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db perl pkgconfig-native"
RPM_SONAME_REQ_perl-DB_File = "ld-linux-aarch64.so.1 libc.so.6 libdb-5.3.so libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-DB_File = "glibc libdb perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-DB_File-1.842-1.el8.aarch64.rpm \
          "

SRC_URI[perl-DB_File.sha256sum] = "9e50df00de0aed3c96ab8ce58975f524990c423df9646a3e806a197343a5fcfc"
