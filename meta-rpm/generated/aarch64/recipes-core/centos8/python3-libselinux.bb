SUMMARY = "generated recipe based on libselinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-libselinux = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libselinux.so.1"
RDEPENDS_python3-libselinux = "glibc libselinux platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libselinux-2.9-3.el8.aarch64.rpm \
          "

SRC_URI[python3-libselinux.sha256sum] = "9bbafce460344a3b3feb1d72955271ae2cd2f58daba2777331739a90aec5a777"
