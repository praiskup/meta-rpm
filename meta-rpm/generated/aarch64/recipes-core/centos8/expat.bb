SUMMARY = "generated recipe based on expat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_expat = "libexpat.so.1"
RPM_SONAME_REQ_expat = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1"
RDEPENDS_expat = "glibc"
RPM_SONAME_REQ_expat-devel = "libexpat.so.1"
RPROVIDES_expat-devel = "expat-dev (= 2.2.5)"
RDEPENDS_expat-devel = "expat pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/expat-2.2.5-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/expat-devel-2.2.5-3.el8.aarch64.rpm \
          "

SRC_URI[expat.sha256sum] = "6b081b6a5b780eefe8fc5f8f6c4291b7558ddd6107de8a8bd9610799be9662e4"
SRC_URI[expat-devel.sha256sum] = "399a3d4f935d29982b6bd86f8da9b3c89f46c4d15945e0f4456a85d75baeccce"
