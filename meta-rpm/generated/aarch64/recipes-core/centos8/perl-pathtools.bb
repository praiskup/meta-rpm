SUMMARY = "generated recipe based on perl-PathTools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-PathTools = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-PathTools = "glibc perl-Carp perl-Errno perl-Exporter perl-Scalar-List-Utils perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-PathTools-3.74-1.el8.aarch64.rpm \
          "

SRC_URI[perl-PathTools.sha256sum] = "5e9fefaa81b6fa8ad2a06277619be99bf9c760cdc6250011d624483f47e57c9d"
