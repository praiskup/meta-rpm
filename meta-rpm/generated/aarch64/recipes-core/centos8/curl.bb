SUMMARY = "generated recipe based on curl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "brotli e2fsprogs krb5-libs libidn2 libmetalink libnghttp2 libpsl libssh openldap openssl pkgconfig-native zlib"
RPM_SONAME_REQ_curl = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libmetalink.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_curl = "glibc libcurl libmetalink openssl-libs zlib"
RPM_SONAME_PROV_libcurl = "libcurl.so.4"
RPM_SONAME_REQ_libcurl = "ld-linux-aarch64.so.1 libbrotlidec.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libnghttp2.so.14 libpsl.so.5 libpthread.so.0 libssh.so.4 libssl.so.1.1 libz.so.1"
RDEPENDS_libcurl = "brotli glibc krb5-libs libcom_err libidn2 libnghttp2 libpsl libssh openldap openssl-libs zlib"
RPM_SONAME_REQ_libcurl-devel = "libcurl.so.4"
RPROVIDES_libcurl-devel = "libcurl-dev (= 7.61.1)"
RDEPENDS_libcurl-devel = "bash libcurl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/curl-7.61.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcurl-7.61.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcurl-devel-7.61.1-12.el8.aarch64.rpm \
          "

SRC_URI[curl.sha256sum] = "3634ee6e1315be7e38344925938bd37dfc5222ae009fabb1d23964b913963041"
SRC_URI[libcurl.sha256sum] = "67392e5e28761144e206e22c79ff533d1b672e849805b68da3449b240c8b8082"
SRC_URI[libcurl-devel.sha256sum] = "830c822eb5f80f561230cef68c8b0983590456a1fc1b05fd01301e5968365a7b"
