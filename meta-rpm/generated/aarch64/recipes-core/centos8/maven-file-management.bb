SUMMARY = "generated recipe based on maven-file-management srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-file-management = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-io maven-shared-utils plexus-utils"
RDEPENDS_maven-file-management-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-file-management-3.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-file-management-javadoc-3.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-file-management.sha256sum] = "87f9cfe8253dd97fe0e84e8fb43fc8d334221a515671c23bd4573951e5f4fbac"
SRC_URI[maven-file-management-javadoc.sha256sum] = "3538c88b28bbad2d1da85d936fff8c6a8cd1e5b7688c6d871f0efea64ad34a6a"
