SUMMARY = "generated recipe based on libquvi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 libgcrypt libgpg-error libproxy lua pkgconfig-native"
RPM_SONAME_PROV_libquvi = "libquvi-0.9-0.9.4.so"
RPM_SONAME_REQ_libquvi = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libgcrypt.so.20 libglib-2.0.so.0 libgpg-error.so.0 liblua-5.3.so libm.so.6 libproxy.so.1"
RDEPENDS_libquvi = "glib2 glibc libcurl libgcrypt libgpg-error libproxy libquvi-scripts lua-libs"
RPROVIDES_libquvi-devel = "libquvi-dev (= 0.9.4)"
RDEPENDS_libquvi-devel = "libquvi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libquvi-0.9.4-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libquvi-devel-0.9.4-12.el8.aarch64.rpm \
          "

SRC_URI[libquvi.sha256sum] = "4be4175b7f6e58f16e4c03cc80f3e45515103b25b77919da947a736e6c2d28bc"
SRC_URI[libquvi-devel.sha256sum] = "2ac296713452514fab7dbbf7781b00dd5c8201e0a3bd1ee7cd5a4e14c32ac008"
