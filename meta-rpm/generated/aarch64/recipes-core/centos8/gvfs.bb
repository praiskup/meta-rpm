SUMMARY = "generated recipe based on gvfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs fuse gcr glib-2.0 gnome-online-accounts json-glib libarchive libcap libcdio libcdio-paranoia libgcrypt libgdata libgpg-error libgphoto2 libgudev libimobiledevice libmtp libplist libsecret libsoup-2.4 libusb1 libxml2 p11-kit pkgconfig-native polkit samba systemd-libs udisks2"
RPM_SONAME_PROV_gvfs = "libgvfsdaemon.so"
RPM_SONAME_REQ_gvfs = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcap.so.2 libcdio.so.18 libcdio_cdda.so.2 libcdio_paranoia.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libgvfscommon.so libgvfsdaemon.so libm.so.6 libp11-kit.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libsystemd.so.0 libudisks2.so.0 libutil.so.1 libxml2.so.2"
RDEPENDS_gvfs = "avahi-glib avahi-libs bash desktop-file-utils gcr glib2 glibc gsettings-desktop-schemas gvfs-client libcap libcdio libcdio-paranoia libgudev libsecret libsoup libudisks2 libxml2 p11-kit polkit-libs systemd-libs udisks2"
RPM_SONAME_REQ_gvfs-afc = "ld-linux-aarch64.so.1 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libimobiledevice.so.6 libp11-kit.so.0 libplist.so.3 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-afc = "bash gcr glib2 glibc gvfs gvfs-client libimobiledevice libplist libsecret p11-kit usbmuxd"
RPM_SONAME_REQ_gvfs-afp = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgvfscommon.so libgvfsdaemon.so libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-afp = "bash gcr glib2 glibc gvfs gvfs-client libgcrypt libgpg-error libsecret p11-kit"
RPM_SONAME_REQ_gvfs-archive = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-archive = "bash gcr glib2 glibc gvfs gvfs-client libarchive libsecret p11-kit"
RPM_SONAME_PROV_gvfs-client = "libgioremote-volume-monitor.so libgvfscommon.so libgvfsdbus.so"
RPM_SONAME_REQ_gvfs-client = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libpthread.so.0 libutil.so.1"
RDEPENDS_gvfs-client = "glib2 glibc"
RPROVIDES_gvfs-devel = "gvfs-dev (= 1.36.2)"
RDEPENDS_gvfs-devel = "gvfs-client"
RPM_SONAME_REQ_gvfs-fuse = "ld-linux-aarch64.so.1 libc.so.6 libfuse.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libpthread.so.0 libutil.so.1"
RDEPENDS_gvfs-fuse = "fuse fuse-libs glib2 glibc gvfs gvfs-client"
RPM_SONAME_REQ_gvfs-goa = "ld-linux-aarch64.so.1 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgdata.so.22 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libjson-glib-1.0.so.0 libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libutil.so.1 libxml2.so.2"
RDEPENDS_gvfs-goa = "bash gcr glib2 glibc gnome-online-accounts gvfs gvfs-client json-glib libgdata libsecret libsoup libxml2 p11-kit"
RPM_SONAME_REQ_gvfs-gphoto2 = "ld-linux-aarch64.so.1 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgphoto2.so.6 libgphoto2_port.so.12 libgudev-1.0.so.0 libgvfscommon.so libgvfsdaemon.so libm.so.6 libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-gphoto2 = "bash gcr glib2 glibc gvfs gvfs-client libgphoto2 libgudev libsecret p11-kit"
RPM_SONAME_REQ_gvfs-mtp = "ld-linux-aarch64.so.1 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libgvfscommon.so libgvfsdaemon.so libmtp.so.9 libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libusb-1.0.so.0 libutil.so.1"
RDEPENDS_gvfs-mtp = "bash gcr glib2 glibc gvfs gvfs-client libgudev libmtp libsecret libusbx p11-kit"
RPM_SONAME_REQ_gvfs-smb = "ld-linux-aarch64.so.1 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libsmbclient.so.0 libutil.so.1"
RDEPENDS_gvfs-smb = "bash gcr glib2 glibc gvfs gvfs-client libsecret libsmbclient p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-afc-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-afp-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-archive-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-client-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-devel-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-fuse-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-goa-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-gphoto2-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-mtp-1.36.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gvfs-smb-1.36.2-8.el8.aarch64.rpm \
          "

SRC_URI[gvfs.sha256sum] = "a2a47fbcec6b99f57620118c6115f94c328483216ef021ef9c0a5cad13bd9017"
SRC_URI[gvfs-afc.sha256sum] = "d4b861a705d95078fa5bb26c6c384f71ebb96a08b907cf5ffc39f5b2964feacc"
SRC_URI[gvfs-afp.sha256sum] = "c24888f8e9f7e24b6c5dfe340abb093855e40a9830d6e1a61261c086ea253114"
SRC_URI[gvfs-archive.sha256sum] = "582a83a8b7d22ffeb7cac0658c2adba32dd3c9c637d8fe0ce5941f010fb85dcf"
SRC_URI[gvfs-client.sha256sum] = "ee19f48ee3ee1c5ad2997e78ee9cfad0ef23e34a55fe1ee23d83ba2c8a8bf49f"
SRC_URI[gvfs-devel.sha256sum] = "955b733ba31441031e03f570f6870cbe8ac57a86ec40c445adc46736850f6b25"
SRC_URI[gvfs-fuse.sha256sum] = "a08bfcd9a656098f401c1ce4f7f0fb6d2d601729bcaf152a9243e229b566fe47"
SRC_URI[gvfs-goa.sha256sum] = "7cad6a3df3134b9ff45d458367720e79296aae07fb80d828ae8e10803a9932b7"
SRC_URI[gvfs-gphoto2.sha256sum] = "51c190073b3e93011840e61a99a53ed45887f1ac85bf3cf16304517fb135d95c"
SRC_URI[gvfs-mtp.sha256sum] = "571a2082ad7ceb7b450627b22b6dc76c5c41204e8f9444ccf54bcebfbac1efd1"
SRC_URI[gvfs-smb.sha256sum] = "915c16db8bbdfd6066428cbf078a1bee4a5213c0a6937ddc7f7a54cac7037464"
