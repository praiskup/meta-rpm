SUMMARY = "generated recipe based on cracklib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_cracklib = "libcrack.so.2"
RPM_SONAME_REQ_cracklib = "ld-linux-aarch64.so.1 libc.so.6 libcrack.so.2 libz.so.1"
RDEPENDS_cracklib = "bash glibc gzip zlib"
RPM_SONAME_REQ_cracklib-devel = "libcrack.so.2"
RPROVIDES_cracklib-devel = "cracklib-dev (= 2.9.6)"
RDEPENDS_cracklib-devel = "cracklib"
RDEPENDS_cracklib-dicts = "cracklib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cracklib-2.9.6-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cracklib-dicts-2.9.6-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cracklib-devel-2.9.6-15.el8.aarch64.rpm \
          "

SRC_URI[cracklib.sha256sum] = "54efb853142572e1c2872e351838fc3657b662722ff6b2913d1872d4752a0eb8"
SRC_URI[cracklib-devel.sha256sum] = "f286a9a9267118aa3dfdb0a224cf24cf3a453b3b44a334a61c1911f19c87a184"
SRC_URI[cracklib-dicts.sha256sum] = "d61741af0ffe96c55f588dd164b9c3c93e7c7175c7e616db25990ab3e16e0f22"
