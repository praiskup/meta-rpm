SUMMARY = "generated recipe based on redhat-support-tool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_redhat-support-tool = "platform-python python3-dateutil python3-lxml python3-magic python36 redhat-support-lib-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-support-tool-0.11.2-2.el8.noarch.rpm \
          "

SRC_URI[redhat-support-tool.sha256sum] = "5a37e42898257a8775519277e1b617481b2c5f94cd603318d8bdc01077abce05"
