SUMMARY = "generated recipe based on perl-XML-RegExp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-RegExp = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-XML-RegExp-0.04-14.el8.noarch.rpm \
          "

SRC_URI[perl-XML-RegExp.sha256sum] = "059d13b6d3e2742805f20e3d28db64d2314ae0f7423c42ad014048c18760d077"
