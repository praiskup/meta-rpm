SUMMARY = "generated recipe based on unzip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 pkgconfig-native"
RPM_SONAME_REQ_unzip = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6"
RDEPENDS_unzip = "bash bzip2-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/unzip-6.0-43.el8.aarch64.rpm \
          "

SRC_URI[unzip.sha256sum] = "5aeff11e1245ddcd4f958bb79649abbba18e9470e3ef5c86b60a2496d01cc7d7"
