SUMMARY = "generated recipe based on iscsi-initiator-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "isns-utils kmod libgcc libmount openssl pkgconfig-native platform-python3 systemd-libs"
RPM_SONAME_PROV_iscsi-initiator-utils = "libiscsi.so.0 libopeniscsiusr.so.0.2.0"
RPM_SONAME_REQ_iscsi-initiator-utils = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libisns.so.0 libkmod.so.2 libmount.so.1 libopeniscsiusr.so.0.2.0 librt.so.1 libsystemd.so.0"
RDEPENDS_iscsi-initiator-utils = "bash glibc iscsi-initiator-utils-iscsiuio isns-utils-libs kmod-libs libmount openssl-libs systemd systemd-libs"
RPM_SONAME_REQ_iscsi-initiator-utils-iscsiuio = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 libsystemd.so.0"
RDEPENDS_iscsi-initiator-utils-iscsiuio = "bash glibc iscsi-initiator-utils libgcc systemd-libs"
RPM_SONAME_PROV_python3-iscsi-initiator-utils = "libiscsi.cpython-36m-aarch64-linux-gnu.so"
RPM_SONAME_REQ_python3-iscsi-initiator-utils = "ld-linux-aarch64.so.1 libc.so.6 libiscsi.so.0 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-iscsi-initiator-utils = "glibc iscsi-initiator-utils platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iscsi-initiator-utils-6.2.0.878-4.gitd791ce0.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iscsi-initiator-utils-iscsiuio-6.2.0.878-4.gitd791ce0.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-iscsi-initiator-utils-6.2.0.878-4.gitd791ce0.el8.aarch64.rpm \
          "

SRC_URI[iscsi-initiator-utils.sha256sum] = "f4e004b2c15c9af38ffb8fdf2ce819763351a57cae3060c4b3500d00b85dff49"
SRC_URI[iscsi-initiator-utils-iscsiuio.sha256sum] = "b2e9ec658d394a40efffec0b5a719251185e85edb4bce0d6fa64819a42b95ec9"
SRC_URI[python3-iscsi-initiator-utils.sha256sum] = "80c84183732b7e7fa6cee8c24393509f6dceecf906b08068c82f848beae927df"
