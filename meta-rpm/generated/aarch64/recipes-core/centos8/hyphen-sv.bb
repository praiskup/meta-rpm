SUMMARY = "generated recipe based on hyphen-sv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-sv = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-sv-1.00.1-18.el8.noarch.rpm \
          "

SRC_URI[hyphen-sv.sha256sum] = "128f9952afd15c611d12af4986e70269ea3c0ca548939f5dee7a53a4ba14681a"
