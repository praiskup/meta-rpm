SUMMARY = "generated recipe based on mod_fcgid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mod_fcgid = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_mod_fcgid = "glibc httpd systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_fcgid-2.3.9-16.el8.aarch64.rpm \
          "

SRC_URI[mod_fcgid.sha256sum] = "4a2fb6a8e782b66208210b31f15ab01fc890ee42fc77d8bf4324d502cb591b69"
