SUMMARY = "generated recipe based on libnotify srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libnotify = "libnotify.so.4"
RPM_SONAME_REQ_libnotify = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libnotify.so.4"
RDEPENDS_libnotify = "gdk-pixbuf2 glib2 glibc"
RPM_SONAME_REQ_libnotify-devel = "libnotify.so.4"
RPROVIDES_libnotify-devel = "libnotify-dev (= 0.7.7)"
RDEPENDS_libnotify-devel = "gdk-pixbuf2-devel glib2-devel libnotify pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libnotify-0.7.7-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libnotify-devel-0.7.7-5.el8.aarch64.rpm \
          "

SRC_URI[libnotify.sha256sum] = "8c2b8123f35882131dfe7327db5ec5a4831941123f1c69e4145cf10becd3568a"
SRC_URI[libnotify-devel.sha256sum] = "af3459bb6156be23f8d9af1c1caf2f0233e423b571a611eccb040e2fed249011"
