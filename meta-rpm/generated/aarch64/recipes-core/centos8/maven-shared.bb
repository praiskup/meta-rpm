SUMMARY = "generated recipe based on maven-shared srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-shared = "java-1.8.0-openjdk-headless javapackages-filesystem maven-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-shared-22-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-shared.sha256sum] = "ec93f5431459b078a0b25062ebf8996557a5de8e59777745e4cc0db7e420c1e8"
