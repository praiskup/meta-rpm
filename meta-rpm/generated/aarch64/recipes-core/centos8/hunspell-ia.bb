SUMMARY = "generated recipe based on hunspell-ia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ia = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ia-0.20050226-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-ia.sha256sum] = "8bf77f9d201ff75f5b8c92914eca4a16ea8b1e38995f128b96a796e4236fd863"
