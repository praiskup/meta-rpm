SUMMARY = "generated recipe based on xorg-x11-xtrans-devel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_xorg-x11-xtrans-devel = "xorg-x11-xtrans-dev (= 1.3.5)"
RDEPENDS_xorg-x11-xtrans-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xorg-x11-xtrans-devel-1.3.5-6.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-xtrans-devel.sha256sum] = "d7107f2dd28c8778c7f26f1193056666fb4f1ce67577c128cd5c049a4bb89dac"
