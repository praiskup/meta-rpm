SUMMARY = "generated recipe based on python-jsonschema srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jsonschema = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-jsonschema-2.6.0-4.el8.noarch.rpm \
          "

SRC_URI[python3-jsonschema.sha256sum] = "0feac495306bbe6e3149a352025bea8d23cda512859a230cbd3f510cf48caaf7"
