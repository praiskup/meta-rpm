SUMMARY = "generated recipe based on mod_http2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnghttp2 openssl pkgconfig-native"
RPM_SONAME_REQ_mod_http2 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libnghttp2.so.14"
RDEPENDS_mod_http2 = "glibc httpd libnghttp2 openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_http2-1.11.3-3.module_el8.2.0+486+c01050f0.1.aarch64.rpm \
          "

SRC_URI[mod_http2.sha256sum] = "b3aea3ed0d8a8ad60d6a2a4a29a4e6d9db12a056de0bb7136a7cea97e5b1ed32"
