SUMMARY = "generated recipe based on nghttp2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "c-ares libev libgcc libnghttp2 openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_nghttp2 = "ld-linux-aarch64.so.1 libc.so.6 libcares.so.2 libcrypto.so.1.1 libdl.so.2 libev.so.4 libgcc_s.so.1 libm.so.6 libnghttp2.so.14 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libsystemd.so.0 libz.so.1"
RDEPENDS_nghttp2 = "bash c-ares glibc libev libgcc libnghttp2 libstdc++ openssl-libs platform-python systemd systemd-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/nghttp2-1.33.0-3.el8_2.1.aarch64.rpm \
          "

SRC_URI[nghttp2.sha256sum] = "02a0c5bef59474a8a4ba4f6082325fccaac6bcfebceafb263e6a52f47d5fcb91"
