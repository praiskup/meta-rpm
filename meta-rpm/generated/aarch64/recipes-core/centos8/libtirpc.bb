SUMMARY = "generated recipe based on libtirpc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/librpc"
DEPENDS = "e2fsprogs krb5-libs pkgconfig-native"
RPM_SONAME_PROV_libtirpc = "libtirpc.so.3"
RPM_SONAME_REQ_libtirpc = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0"
RDEPENDS_libtirpc = "glibc krb5-libs libcom_err"
RPM_SONAME_REQ_libtirpc-devel = "libtirpc.so.3"
RPROVIDES_libtirpc-devel = "libtirpc-dev (= 1.1.4)"
RDEPENDS_libtirpc-devel = "bash libtirpc man-db pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtirpc-1.1.4-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtirpc-devel-1.1.4-4.el8.aarch64.rpm \
          "

SRC_URI[libtirpc.sha256sum] = "5f5d63e14ca77e84ae79d1d78569ad9326af6c107be137461800527afb5613eb"
SRC_URI[libtirpc-devel.sha256sum] = "9ccba4e325e9ed8266009fda5516aa878f7b8eb05616aad9bb44c23bb11b6e16"
