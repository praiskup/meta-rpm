SUMMARY = "generated recipe based on dcraw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jasper lcms2 libjpeg-turbo pkgconfig-native"
RPM_SONAME_REQ_dcraw = "ld-linux-aarch64.so.1 libc.so.6 libjasper.so.4 libjpeg.so.62 liblcms2.so.2 libm.so.6"
RDEPENDS_dcraw = "glibc jasper-libs lcms2 libjpeg-turbo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dcraw-9.27.0-9.el8.aarch64.rpm \
          "

SRC_URI[dcraw.sha256sum] = "23367bd2f8bd8ef69d219051075032fb91f4544b2986e518861375ebf272baf4"
