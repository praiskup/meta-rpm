SUMMARY = "generated recipe based on fuse srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_fuse = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_fuse = "fuse-common glibc which"
RPM_SONAME_REQ_fuse-devel = "libfuse.so.2 libulockmgr.so.1"
RPROVIDES_fuse-devel = "fuse-dev (= 2.9.7)"
RDEPENDS_fuse-devel = "fuse-libs pkgconf-pkg-config"
RPM_SONAME_PROV_fuse-libs = "libfuse.so.2 libulockmgr.so.1"
RPM_SONAME_REQ_fuse-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_fuse-libs = "glibc"
RPM_SONAME_REQ_fuse3 = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_fuse3 = "fuse-common glibc"
RPM_SONAME_REQ_fuse3-devel = "libfuse3.so.3"
RPROVIDES_fuse3-devel = "fuse3-dev (= 3.2.1)"
RDEPENDS_fuse3-devel = "fuse3-libs pkgconf-pkg-config"
RPM_SONAME_PROV_fuse3-libs = "libfuse3.so.3"
RPM_SONAME_REQ_fuse3-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_fuse3-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse-2.9.7-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse-common-3.2.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse-devel-2.9.7-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse-libs-2.9.7-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse3-3.2.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse3-devel-3.2.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fuse3-libs-3.2.1-12.el8.aarch64.rpm \
          "

SRC_URI[fuse.sha256sum] = "80a077200c6c5b1aa555eb6da9b68a31ef68c2b46cbe439093df2d56d691416b"
SRC_URI[fuse-common.sha256sum] = "0326a8efe43f4234ea6b2b2eaa7d91c8c99bebe5bdd59a6c56d944d71605cd25"
SRC_URI[fuse-devel.sha256sum] = "54542db6df3363ee3b57ca97fc8d3194fa84b64d286432bf475e6c4391195fdd"
SRC_URI[fuse-libs.sha256sum] = "0431ac0a9ad2ae9d657a66e9a5dc9326b232732e9967088990c09e826c6f3071"
SRC_URI[fuse3.sha256sum] = "9abf544b5a0312b62dbd0752611f5bbb9088c2fea62b0999074a466104f463eb"
SRC_URI[fuse3-devel.sha256sum] = "c415b76e30e504af37330707753f9eecd7950a12182bd11f9e2d8111aa26cfa5"
SRC_URI[fuse3-libs.sha256sum] = "f0855727ade654d9343623bb00b0611e06de18e5a9192b37a840924d3acb33f5"
