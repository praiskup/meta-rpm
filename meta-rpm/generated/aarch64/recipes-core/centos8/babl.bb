SUMMARY = "generated recipe based on babl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_babl = "libbabl-0.1.so.0"
RPM_SONAME_REQ_babl = "ld-linux-aarch64.so.1 libbabl-0.1.so.0 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_babl = "glibc"
RPM_SONAME_REQ_babl-devel = "libbabl-0.1.so.0"
RPROVIDES_babl-devel = "babl-dev (= 0.1.52)"
RDEPENDS_babl-devel = "babl pkgconf-pkg-config"
RDEPENDS_babl-devel-docs = "babl-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/babl-0.1.52-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/babl-devel-0.1.52-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/babl-devel-docs-0.1.52-1.el8.noarch.rpm \
          "

SRC_URI[babl.sha256sum] = "f17b88f79c5321db4fa5eb28e737d4c28afe8c53bbe262b25ee2e66318c13db6"
SRC_URI[babl-devel.sha256sum] = "dd3e0d30d427476811a8552cc8fe42199c6d3194dc2a9b0bdc1ae5ab3d8268e1"
SRC_URI[babl-devel-docs.sha256sum] = "5991ce2d0dbc7e0f3dd5c3add25d44083733b724264446e7a7c4974b88f28940"
