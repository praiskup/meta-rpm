SUMMARY = "generated recipe based on powertop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libnl ncurses pciutils pkgconfig-native"
RPM_SONAME_REQ_powertop = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libncurses.so.6 libncursesw.so.6 libnl-3.so.200 libnl-genl-3.so.200 libpci.so.3 libpthread.so.0 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_powertop = "bash coreutils glibc libgcc libnl3 libstdc++ ncurses-libs pciutils-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/powertop-2.11-1.el8.aarch64.rpm \
          "

SRC_URI[powertop.sha256sum] = "dcce8964aaefde1dd52dbc66a889d9b637f3e1c776dcb6bc8174acb62ff0d866"
