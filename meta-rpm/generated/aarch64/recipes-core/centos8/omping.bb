SUMMARY = "generated recipe based on omping srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_omping = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_omping = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/omping-0.0.4-14.el8.aarch64.rpm \
          "

SRC_URI[omping.sha256sum] = "7874cd44c20e57a71677dbd8ed21394058ab90bea3e9cfab436d21dfa187558d"
