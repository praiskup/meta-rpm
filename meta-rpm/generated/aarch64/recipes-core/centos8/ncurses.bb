SUMMARY = "generated recipe based on ncurses srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_ncurses = "ld-linux-aarch64.so.1 libc.so.6 libtic.so.6 libtinfo.so.6"
RDEPENDS_ncurses = "glibc ncurses-libs"
RPM_SONAME_PROV_ncurses-c++-libs = "libncurses++.so.6 libncurses++w.so.6"
RPM_SONAME_REQ_ncurses-c++-libs = "ld-linux-aarch64.so.1 libc.so.6 libform.so.6 libformw.so.6 libgcc_s.so.1 libm.so.6 libmenu.so.6 libmenuw.so.6 libncurses.so.6 libncursesw.so.6 libpanel.so.6 libpanelw.so.6 libstdc++.so.6"
RDEPENDS_ncurses-c++-libs = "glibc libgcc libstdc++ ncurses-libs"
RPM_SONAME_PROV_ncurses-compat-libs = "libform.so.5 libformw.so.5 libmenu.so.5 libmenuw.so.5 libncurses++.so.5 libncurses++w.so.5 libncurses.so.5 libncursesw.so.5 libpanel.so.5 libpanelw.so.5 libtic.so.5 libtinfo.so.5"
RPM_SONAME_REQ_ncurses-compat-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libform.so.5 libformw.so.5 libgcc_s.so.1 libm.so.6 libmenu.so.5 libmenuw.so.5 libncurses.so.5 libncursesw.so.5 libpanel.so.5 libpanelw.so.5 libstdc++.so.6 libtinfo.so.5"
RDEPENDS_ncurses-compat-libs = "glibc libgcc libstdc++ ncurses-base"
RPM_SONAME_REQ_ncurses-devel = "libform.so.6 libformw.so.6 libmenu.so.6 libmenuw.so.6 libncurses++.so.6 libncurses++w.so.6 libpanel.so.6 libpanelw.so.6 libtic.so.6 libtinfo.so.6"
RPROVIDES_ncurses-devel = "ncurses-dev (= 6.1)"
RDEPENDS_ncurses-devel = "bash ncurses-c++-libs ncurses-libs pkgconf-pkg-config"
RPM_SONAME_PROV_ncurses-libs = "libform.so.6 libformw.so.6 libmenu.so.6 libmenuw.so.6 libncurses.so.6 libncursesw.so.6 libpanel.so.6 libpanelw.so.6 libtic.so.6 libtinfo.so.6"
RPM_SONAME_REQ_ncurses-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libncurses.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_ncurses-libs = "glibc ncurses-base"
RDEPENDS_ncurses-term = "ncurses-base"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-6.1-7.20180224.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-base-6.1-7.20180224.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-c++-libs-6.1-7.20180224.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-compat-libs-6.1-7.20180224.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-devel-6.1-7.20180224.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-libs-6.1-7.20180224.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ncurses-term-6.1-7.20180224.el8.noarch.rpm \
          "
SRC_URI = "file://ncurses-6.1-7.20180224.el8.patch"

SRC_URI[ncurses.sha256sum] = "313b9d0ba8b2734cde9edfdf477616c1e535998238e43a1b6465cf946eb92c0b"
SRC_URI[ncurses-base.sha256sum] = "1dfed63c2b675064c87d3e95c433a1c4515b24c28a7815e643e6f3d84814e79d"
SRC_URI[ncurses-c++-libs.sha256sum] = "3f0f3126a42ec179683277539586b9496b70c43b04bec681f17ba014bbd8214d"
SRC_URI[ncurses-compat-libs.sha256sum] = "f75377e1444114d4bad09eebe6ef11c2ba45f430366e384b6443706c199e64ec"
SRC_URI[ncurses-devel.sha256sum] = "8416e93829ed3ce3bf8806764065398565154356e358c82cc7ad8443fde7b23c"
SRC_URI[ncurses-libs.sha256sum] = "4d62bee2b38b7ae79937139f3e246ddefd184da953a70263d633f770d30b60c3"
SRC_URI[ncurses-term.sha256sum] = "355b2914ac4393437af8ee1e594b757e68864d9f4489b58e0d98450cafe243f0"
