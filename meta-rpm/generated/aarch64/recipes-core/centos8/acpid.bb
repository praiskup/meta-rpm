SUMMARY = "generated recipe based on acpid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_acpid = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_acpid = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/acpid-2.0.30-2.el8.aarch64.rpm \
          "

SRC_URI[acpid.sha256sum] = "f8ef1fcabd3019662331809312ac22b2e7dc20cd29331807c5f969736c0a0d54"
