SUMMARY = "generated recipe based on newt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3 popt slang"
RPM_SONAME_PROV_newt = "libnewt.so.0.52"
RPM_SONAME_REQ_newt = "ld-linux-aarch64.so.1 libc.so.6 libnewt.so.0.52 libpopt.so.0 libslang.so.2"
RDEPENDS_newt = "glibc popt slang"
RPM_SONAME_REQ_newt-devel = "libnewt.so.0.52"
RPROVIDES_newt-devel = "newt-dev (= 0.52.20)"
RDEPENDS_newt-devel = "newt pkgconf-pkg-config slang-devel"
RPM_SONAME_REQ_python3-newt = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libnewt.so.0.52 libpthread.so.0 libpython3.6m.so.1.0 libslang.so.2 libutil.so.1"
RDEPENDS_python3-newt = "glibc newt platform-python python3-libs slang"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/newt-devel-0.52.20-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-newt-0.52.20-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/newt-0.52.20-11.el8.aarch64.rpm \
          "

SRC_URI[newt.sha256sum] = "b0b43ae9d3f38a2bd20dd4452a3d6e791407ac3b24b71019605e8caf8adaf79e"
SRC_URI[newt-devel.sha256sum] = "43e56054272fecc6ac7739e678d3ea4472ff6647916fa7338546d182447e1fff"
SRC_URI[python3-newt.sha256sum] = "b5ba9dca0d2192e7382adcf4a3ccf33b69d729add82d5dd3bea61cbdb0523028"
