SUMMARY = "generated recipe based on python-pycurl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pycurl = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_python3-pycurl = "glibc libcurl openssl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pycurl-7.43.0.2-4.el8.aarch64.rpm \
          "

SRC_URI[python3-pycurl.sha256sum] = "959955f27f3d96a144ad1cdc9e53705083571349a893db18f1bd6a919e5b49aa"
