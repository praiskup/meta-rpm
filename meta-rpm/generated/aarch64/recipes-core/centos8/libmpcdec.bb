SUMMARY = "generated recipe based on libmpcdec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmpcdec = "libmpcdec.so.5"
RPM_SONAME_REQ_libmpcdec = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libmpcdec = "glibc"
RPM_SONAME_REQ_libmpcdec-devel = "libmpcdec.so.5"
RPROVIDES_libmpcdec-devel = "libmpcdec-dev (= 1.2.6)"
RDEPENDS_libmpcdec-devel = "libmpcdec"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmpcdec-1.2.6-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmpcdec-devel-1.2.6-20.el8.aarch64.rpm \
          "

SRC_URI[libmpcdec.sha256sum] = "19d132d460ea4a0aa08e73d5bdf00721a5ca43b41c831eab5ae4afcec0f8e500"
SRC_URI[libmpcdec-devel.sha256sum] = "c91a88493b409eba10432d03924bfc259e8eb7257df6f9cdbe930a699880420e"
