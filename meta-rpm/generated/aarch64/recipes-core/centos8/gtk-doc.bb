SUMMARY = "generated recipe based on gtk-doc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gtk-doc = "bash docbook-style-xsl docbook-utils libxslt pkgconf-pkg-config platform-python python3-six source-highlight"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtk-doc-1.28-1.el8.aarch64.rpm \
          "

SRC_URI[gtk-doc.sha256sum] = "3a4530c96bac7700be1a79e33df2c86d9d0dec8e259606efaf88178ac78b41f6"
