SUMMARY = "generated recipe based on perl-Sys-CPU srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-CPU = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-CPU = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Sys-CPU-0.61-14.el8.aarch64.rpm \
          "

SRC_URI[perl-Sys-CPU.sha256sum] = "29b09f41b37e3f684e5344246197f6d035c77cb9c0530c6582e51cd2cb60aeeb"
