SUMMARY = "generated recipe based on perl-HTTP-Negotiate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Negotiate = "perl-Exporter perl-HTTP-Message perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-HTTP-Negotiate-6.01-19.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Negotiate.sha256sum] = "418e50e6a6f59a7ac47286566485f6ea59797a933772b57fa5a575580b81ee2f"
