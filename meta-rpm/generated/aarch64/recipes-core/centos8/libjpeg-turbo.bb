SUMMARY = "generated recipe based on libjpeg-turbo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "jpeg"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libjpeg-turbo = "libjpeg.so.62"
RPM_SONAME_REQ_libjpeg-turbo = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libjpeg-turbo = "glibc"
RPM_SONAME_REQ_libjpeg-turbo-devel = "libjpeg.so.62"
RPROVIDES_libjpeg-turbo-devel = "libjpeg-turbo-dev (= 1.5.3)"
RDEPENDS_libjpeg-turbo-devel = "libjpeg-turbo pkgconf-pkg-config"
RPM_SONAME_REQ_libjpeg-turbo-utils = "ld-linux-aarch64.so.1 libc.so.6 libjpeg.so.62"
RDEPENDS_libjpeg-turbo-utils = "glibc libjpeg-turbo"
RPM_SONAME_PROV_turbojpeg = "libturbojpeg.so.0"
RPM_SONAME_REQ_turbojpeg = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_turbojpeg = "glibc"
RPM_SONAME_REQ_turbojpeg-devel = "libturbojpeg.so.0"
RPROVIDES_turbojpeg-devel = "turbojpeg-dev (= 1.5.3)"
RDEPENDS_turbojpeg-devel = "pkgconf-pkg-config turbojpeg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libjpeg-turbo-1.5.3-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libjpeg-turbo-devel-1.5.3-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libjpeg-turbo-utils-1.5.3-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/turbojpeg-1.5.3-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/turbojpeg-devel-1.5.3-10.el8.aarch64.rpm \
          "

SRC_URI[libjpeg-turbo.sha256sum] = "32159dea385836628c3b80b2481376ba562d8290ad2343e04935ca275cdf35f3"
SRC_URI[libjpeg-turbo-devel.sha256sum] = "c159795e6ff080bc1ee073c0303a1d4eb1af2f9f3d124d96375c7cd6d6b2fed7"
SRC_URI[libjpeg-turbo-utils.sha256sum] = "1e3e6f81f90b463c0ad6c0f2751a4f4fc4b5ed92c7d1fab535855849f396f236"
SRC_URI[turbojpeg.sha256sum] = "b9cbe2cf881c84b23f5a2bd67584e93656dec64e8ecb07ebb5a2f20c3d81d683"
SRC_URI[turbojpeg-devel.sha256sum] = "0c5a0b67411513b3db0d206dfcf6fa5f59ba7e5f675426c3641f9b7fe22b7989"
