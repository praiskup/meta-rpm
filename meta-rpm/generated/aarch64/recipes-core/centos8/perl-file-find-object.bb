SUMMARY = "generated recipe based on perl-File-Find-Object srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Find-Object = "perl-Carp perl-Class-XSAccessor perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-Find-Object-0.3.2-5.el8.noarch.rpm \
          "

SRC_URI[perl-File-Find-Object.sha256sum] = "f41f45a2e44c4da57dfbbde46919d67f9c40c647ff650345582496c7a8654228"
