SUMMARY = "generated recipe based on ctags srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ctags = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_ctags = "glibc"
RDEPENDS_ctags-etags = "bash chkconfig ctags"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ctags-5.8-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ctags-etags-5.8-22.el8.aarch64.rpm \
          "

SRC_URI[ctags.sha256sum] = "014ec954b753721ff642287b52536b045102959057f32c02d8e63ccc69aa8c4c"
SRC_URI[ctags-etags.sha256sum] = "1744c8f1079016b73868d45c35ba287c1718db58a4c0eef235857366bd3a201b"
