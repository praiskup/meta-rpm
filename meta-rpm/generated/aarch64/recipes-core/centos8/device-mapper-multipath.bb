SUMMARY = "generated recipe based on device-mapper-multipath srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs json-c libaio libgcc pkgconfig-native readline systemd-libs userspace-rcu"
RPM_SONAME_REQ_device-mapper-multipath = "ld-linux-aarch64.so.1 libc.so.6 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libmpathcmd.so.0 libmpathpersist.so.0 libmultipath.so.0 libpthread.so.0 libreadline.so.7 libsystemd.so.0 libudev.so.1 liburcu.so.6"
RDEPENDS_device-mapper-multipath = "bash device-mapper device-mapper-libs device-mapper-multipath-libs glibc kpartx libgcc readline systemd systemd-libs userspace-rcu"
RPM_SONAME_REQ_device-mapper-multipath-devel = "libmpathcmd.so.0 libmpathpersist.so.0"
RPROVIDES_device-mapper-multipath-devel = "device-mapper-multipath-dev (= 0.8.3)"
RDEPENDS_device-mapper-multipath-devel = "device-mapper-multipath device-mapper-multipath-libs"
RPM_SONAME_PROV_device-mapper-multipath-libs = "libcheckcciss_tur.so libcheckdirectio.so libcheckemc_clariion.so libcheckhp_sw.so libcheckrdac.so libcheckreadsector0.so libchecktur.so libforeign-nvme.so libmpathcmd.so.0 libmpathpersist.so.0 libmultipath.so.0 libprioalua.so libprioana.so libprioconst.so libpriodatacore.so libprioemc.so libpriohds.so libpriohp_sw.so libprioiet.so libprioontap.so libpriopath_latency.so libpriorandom.so libpriordac.so libpriosysfs.so libprioweightedpath.so"
RPM_SONAME_REQ_device-mapper-multipath-libs = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libm.so.6 libmpathcmd.so.0 libmultipath.so.0 libpthread.so.0 libsystemd.so.0 libudev.so.1 liburcu.so.6"
RDEPENDS_device-mapper-multipath-libs = "device-mapper-libs glibc libaio libgcc systemd-libs userspace-rcu"
RPM_SONAME_REQ_kpartx = "ld-linux-aarch64.so.1 libc.so.6 libdevmapper.so.1.02"
RDEPENDS_kpartx = "bash device-mapper-libs glibc"
RPM_SONAME_PROV_libdmmp = "libdmmp.so.0.2.0"
RPM_SONAME_REQ_libdmmp = "ld-linux-aarch64.so.1 libc.so.6 libjson-c.so.4 libmpathcmd.so.0 libpthread.so.0"
RDEPENDS_libdmmp = "device-mapper-multipath device-mapper-multipath-libs glibc json-c"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-multipath-0.8.3-3.el8_2.3.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-multipath-libs-0.8.3-3.el8_2.3.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kpartx-0.8.3-3.el8_2.3.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libdmmp-0.8.3-3.el8_2.3.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/device-mapper-multipath-devel-0.8.3-3.el8_2.3.aarch64.rpm \
          "

SRC_URI[device-mapper-multipath.sha256sum] = "e12050bb93536655477c7c5a990c6f9660d948736f11c9a80b0eda1ec4f7bd29"
SRC_URI[device-mapper-multipath-devel.sha256sum] = "2a3fefe8c023daed200e7f04704f3edd27fbe95b829a30226818680af7d9ab32"
SRC_URI[device-mapper-multipath-libs.sha256sum] = "40f5ea2c8f033a5cddb20943daff86502f271b23b31e297c2dca167644a0e821"
SRC_URI[kpartx.sha256sum] = "a352709061600f812280aaf75538dd717783851eb4100d66d0ba4d0e450e1891"
SRC_URI[libdmmp.sha256sum] = "f1f3b982722042a989c26574bb760745d192586ea92931370c771b892c4aee36"
