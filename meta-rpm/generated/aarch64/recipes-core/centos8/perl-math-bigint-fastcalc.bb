SUMMARY = "generated recipe based on perl-Math-BigInt-FastCalc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Math-BigInt-FastCalc = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Math-BigInt-FastCalc = "glibc perl-Math-BigInt perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Math-BigInt-FastCalc-0.500.600-6.el8.aarch64.rpm \
          "

SRC_URI[perl-Math-BigInt-FastCalc.sha256sum] = "efd5a8aede450ef84f3de5ce5c62f6902619425b4e24e46ff8eed9fd4e4a451e"
