SUMMARY = "generated recipe based on gcc-toolset-9-ltrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libselinux pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-ltrace = "ld-linux-aarch64.so.1 libc.so.6 libdw.so.1 libelf.so.1 libselinux.so.1 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-ltrace = "elfutils-libelf elfutils-libs gcc-toolset-9-runtime glibc libselinux libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-ltrace-0.7.91-1.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-ltrace.sha256sum] = "9616be1a6a1ce29ecc92bd7eda5e811e9eb697464cdb172da4cc9c0631ef610c"
