SUMMARY = "generated recipe based on flite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native"
RPM_SONAME_PROV_flite = "libflite.so.1 libflite_cmu_time_awb.so.1 libflite_cmu_us_kal.so.1 libflite_cmu_us_kal16.so.1 libflite_cmulex.so.1 libflite_usenglish.so.1"
RPM_SONAME_REQ_flite = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libflite.so.1 libflite_cmu_time_awb.so.1 libflite_cmu_us_kal.so.1 libflite_cmulex.so.1 libflite_usenglish.so.1 libm.so.6"
RDEPENDS_flite = "alsa-lib glibc"
RPM_SONAME_REQ_flite-devel = "libflite.so.1 libflite_cmu_time_awb.so.1 libflite_cmu_us_kal.so.1 libflite_cmu_us_kal16.so.1 libflite_cmulex.so.1 libflite_usenglish.so.1"
RPROVIDES_flite-devel = "flite-dev (= 1.3)"
RDEPENDS_flite-devel = "flite"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/flite-1.3-31.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/flite-devel-1.3-31.el8.aarch64.rpm \
          "

SRC_URI[flite.sha256sum] = "a564731a38a2ba10c75a11cc2542c615ddc3a26e57ef827880db07eb5077f4c3"
SRC_URI[flite-devel.sha256sum] = "00ea98d0b000adbb77540ef7d0ed65e74fcd310e01a11c1ec24252aee41a8d83"
