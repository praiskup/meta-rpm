SUMMARY = "generated recipe based on perl-Module-Implementation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Implementation = "perl-Carp perl-Module-Runtime perl-Try-Tiny perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Module-Implementation-0.09-15.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Implementation.sha256sum] = "f91716b2780593bea26a14889785ff076e8990ad2bd1ab3dc285ffaed04c33b1"
