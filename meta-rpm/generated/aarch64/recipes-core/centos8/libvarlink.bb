SUMMARY = "generated recipe based on libvarlink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libvarlink = "libvarlink.so.0"
RPM_SONAME_REQ_libvarlink = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1"
RDEPENDS_libvarlink = "glibc libgcc"
RPM_SONAME_REQ_libvarlink-devel = "libvarlink.so.0"
RPROVIDES_libvarlink-devel = "libvarlink-dev (= 18)"
RDEPENDS_libvarlink-devel = "libvarlink pkgconf-pkg-config"
RPM_SONAME_REQ_libvarlink-util = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1"
RDEPENDS_libvarlink-util = "glibc libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libvarlink-18-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libvarlink-util-18-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvarlink-devel-18-3.el8.aarch64.rpm \
          "

SRC_URI[libvarlink.sha256sum] = "13ec634fa98495861fe381b8a53c3cf9a49f998e45d64f6962970aeb859b9c8c"
SRC_URI[libvarlink-devel.sha256sum] = "bd3f028a7687b298bf28ce1ae32acce12a6fef1ea6a2992bb88c94e1a38f39b8"
SRC_URI[libvarlink-util.sha256sum] = "e87816910c07c09d3108a417259b6bdcce2bd6d05d808b299dc407b42a0a6f62"
