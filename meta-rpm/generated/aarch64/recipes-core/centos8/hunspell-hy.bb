SUMMARY = "generated recipe based on hunspell-hy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hy = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-hy-0.20.0-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-hy.sha256sum] = "ec52e3aa8e7c4b49f80390b8005d2e08727e04a303470965cb8074ad578b240c"
