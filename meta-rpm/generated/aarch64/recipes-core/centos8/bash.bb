SUMMARY = "generated recipe based on bash srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_bash = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libtinfo.so.6"
RDEPENDS_bash = "filesystem glibc ncurses-libs"
RDEPENDS_bash-doc = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bash-4.4.19-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bash-doc-4.4.19-10.el8.aarch64.rpm \
          "

SRC_URI[bash.sha256sum] = "b4db0c7eb9ac568e423eede22018935de0281f7a96099aa5b32fb56ae5968c40"
SRC_URI[bash-doc.sha256sum] = "e02df061e772e5217514785903e7eb0ac0b507d669231673ba1f84eb5e125d7f"
