SUMMARY = "generated recipe based on keyutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_keyutils = "ld-linux-aarch64.so.1 libc.so.6 libkeyutils.so.1 libresolv.so.2"
RDEPENDS_keyutils = "bash glibc keyutils-libs"
RPM_SONAME_PROV_keyutils-libs = "libkeyutils.so.1"
RPM_SONAME_REQ_keyutils-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_keyutils-libs = "glibc"
RPM_SONAME_REQ_keyutils-libs-devel = "libkeyutils.so.1"
RPROVIDES_keyutils-libs-devel = "keyutils-libs-dev (= 1.5.10)"
RDEPENDS_keyutils-libs-devel = "keyutils-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/keyutils-1.5.10-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/keyutils-libs-1.5.10-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/keyutils-libs-devel-1.5.10-6.el8.aarch64.rpm \
          "

SRC_URI[keyutils.sha256sum] = "edaaf208de0881eeebf3a2b3995c3c7c86bad3e78633c519dcec441753af9688"
SRC_URI[keyutils-libs.sha256sum] = "54e61e873e45000433bec3387112f7ead3c4cb9731045caa8033ff5b14ee46b1"
SRC_URI[keyutils-libs-devel.sha256sum] = "8f4cb9ea49200a2be257831d1f90931f3165369489155a2a5ee27915eb1f4377"
