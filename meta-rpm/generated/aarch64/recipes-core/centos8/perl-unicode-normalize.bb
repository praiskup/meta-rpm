SUMMARY = "generated recipe based on perl-Unicode-Normalize srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-Normalize = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-Normalize = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Unicode-Normalize-1.25-396.el8.aarch64.rpm \
          "

SRC_URI[perl-Unicode-Normalize.sha256sum] = "481a23b0de093a92f4308796f602913563d89a6ca2f132ceb638222ce8978378"
