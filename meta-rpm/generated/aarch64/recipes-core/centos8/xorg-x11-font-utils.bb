SUMMARY = "generated recipe based on xorg-x11-font-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libfontenc pkgconfig-native zlib"
RPM_SONAME_REQ_xorg-x11-font-utils = "ld-linux-aarch64.so.1 libc.so.6 libfontenc.so.1 libfreetype.so.6 libm.so.6 libz.so.1"
RDEPENDS_xorg-x11-font-utils = "bash freetype glibc libfontenc pkgconf-pkg-config zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-font-utils-7.5-40.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-font-utils.sha256sum] = "96b3dd4b99941d16f6a608ebdd8489ca01b3e0b4a1db575b23b2e19567b3d530"
