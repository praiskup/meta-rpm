SUMMARY = "generated recipe based on libfontenc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libfontenc = "libfontenc.so.1"
RPM_SONAME_REQ_libfontenc = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1"
RDEPENDS_libfontenc = "glibc zlib"
RPM_SONAME_REQ_libfontenc-devel = "libfontenc.so.1"
RPROVIDES_libfontenc-devel = "libfontenc-dev (= 1.1.3)"
RDEPENDS_libfontenc-devel = "libfontenc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libfontenc-1.1.3-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libfontenc-devel-1.1.3-8.el8.aarch64.rpm \
          "

SRC_URI[libfontenc.sha256sum] = "a61fbee44c00c6c04de4804bde905cb32a65465ab225f6332b3ef7f5c948d6c3"
SRC_URI[libfontenc-devel.sha256sum] = "a4f3d967dc7b47b89633710c9407bb85a2c2430e6b5e734bef91b536c63327f4"
