SUMMARY = "generated recipe based on libcap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libcap = "libcap.so.2"
RPM_SONAME_REQ_libcap = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2"
RDEPENDS_libcap = "glibc"
RPM_SONAME_REQ_libcap-devel = "libcap.so.2"
RPROVIDES_libcap-devel = "libcap-dev (= 2.26)"
RDEPENDS_libcap-devel = "libcap pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcap-2.26-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcap-devel-2.26-3.el8.aarch64.rpm \
          "

SRC_URI[libcap.sha256sum] = "b1e0bff93ffc3e984e665b236712eec0f2a45e62bb63470eb5465b9c1619ddb5"
SRC_URI[libcap-devel.sha256sum] = "d5888e3250f55c1be3c0d39468d85c38915efe8f1e4ad2d0244bc5f57dd64cde"
