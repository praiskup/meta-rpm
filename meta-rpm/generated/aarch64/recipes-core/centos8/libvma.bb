SUMMARY = "generated recipe based on libvma srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libnl pkgconfig-native rdma-core"
RPM_SONAME_PROV_libvma = "libvma.so.8"
RPM_SONAME_REQ_libvma = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libibverbs.so.1 libm.so.6 libmlx5.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 librdmacm.so.1 librt.so.1 libstdc++.so.6 libvma.so.8"
RDEPENDS_libvma = "bash glibc libgcc libibverbs libnl3 librdmacm libstdc++ pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvma-8.9.5-1.el8.aarch64.rpm \
          "

SRC_URI[libvma.sha256sum] = "3e28b6c044e11c53020427ec5d577b6b15dcfab70a2a95ca58a516ea139aa34f"
