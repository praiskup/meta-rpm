SUMMARY = "generated recipe based on python-psycopg2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpq pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-psycopg2 = "ld-linux-aarch64.so.1 libc.so.6 libpq.so.5 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-psycopg2 = "glibc libpq platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-psycopg2-2.7.5-7.el8.aarch64.rpm \
          "

SRC_URI[python3-psycopg2.sha256sum] = "ab93f2d063f092dca4c2e0776adc3bafdd2d0a3889004af0c9705963231a6abe"
