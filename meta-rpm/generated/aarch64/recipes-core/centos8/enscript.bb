SUMMARY = "generated recipe based on enscript srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_enscript = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_enscript = "bash glibc info perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/enscript-1.6.6-17.el8.aarch64.rpm \
          "

SRC_URI[enscript.sha256sum] = "eae5bf587d852992f5dadb38a8f7ef5476f3fb798452b21513c0eec78966389a"
