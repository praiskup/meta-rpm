SUMMARY = "generated recipe based on enchant srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 hunspell libgcc pkgconfig-native"
RPM_SONAME_PROV_enchant = "libenchant.so.1 libenchant_myspell.so"
RPM_SONAME_REQ_enchant = "ld-linux-aarch64.so.1 libc.so.6 libenchant.so.1 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libhunspell-1.6.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_enchant = "glib2 glibc hunspell libgcc libstdc++"
RPM_SONAME_REQ_enchant-devel = "libenchant.so.1"
RPROVIDES_enchant-devel = "enchant-dev (= 1.6.0)"
RDEPENDS_enchant-devel = "enchant glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/enchant-1.6.0-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/enchant-devel-1.6.0-21.el8.aarch64.rpm \
          "

SRC_URI[enchant.sha256sum] = "fa68fe83d83eab740a46c4feaf5e82c28ed6bfd5b9ee36fbba8305219aee5a81"
SRC_URI[enchant-devel.sha256sum] = "4cc8565f7ceba4fbcd29dd1e53bd8d1c8daba2d5c950b683fec7b7925d5086e1"
