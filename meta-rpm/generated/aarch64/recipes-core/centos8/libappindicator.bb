SUMMARY = "generated recipe based on libappindicator srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-glib-devel gdk-pixbuf glib-2.0 gtk+3 libdbusmenu libindicator pango pkgconfig-native"
RPM_SONAME_PROV_libappindicator-gtk3 = "libappindicator3.so.1"
RPM_SONAME_REQ_libappindicator-gtk3 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbusmenu-glib.so.4 libdbusmenu-gtk3.so.4 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libindicator3.so.7 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_libappindicator-gtk3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libdbusmenu libdbusmenu-gtk3 libindicator-gtk3 pango"
RPM_SONAME_REQ_libappindicator-gtk3-devel = "libappindicator3.so.1"
RPROVIDES_libappindicator-gtk3-devel = "libappindicator-gtk3-dev (= 12.10.0)"
RDEPENDS_libappindicator-gtk3-devel = "dbus-glib-devel gtk3-devel libappindicator-gtk3 libdbusmenu-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libappindicator-gtk3-12.10.0-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libappindicator-gtk3-devel-12.10.0-19.el8.aarch64.rpm \
          "

SRC_URI[libappindicator-gtk3.sha256sum] = "a038f21ceef95bd8cb2ab3fd5287f9faa8d40665eb0fe8bb472ff20c82870e73"
SRC_URI[libappindicator-gtk3-devel.sha256sum] = "302e2462a71783210d263cb8998b7ebe194ff8221e131c6f5605e43b58d9fd74"
