SUMMARY = "generated recipe based on httpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "apr apr-util brotli expat libpcre libselinux libxcrypt libxml2 lua openldap openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_httpd = "ld-linux-aarch64.so.1 libapr-1.so.0 libaprutil-1.so.0 libbrotlienc.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libexpat.so.1 liblua-5.3.so libm.so.6 libpcre.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libz.so.1"
RDEPENDS_httpd = "apr apr-util bash brotli centos-logos-httpd expat glibc httpd-filesystem httpd-tools libselinux libxcrypt lua-libs mailcap mod_http2 pcre systemd systemd-libs zlib"
RPROVIDES_httpd-devel = "httpd-dev (= 2.4.37)"
RDEPENDS_httpd-devel = "apr-devel apr-util-devel bash httpd perl-interpreter perl-libs pkgconf-pkg-config"
RDEPENDS_httpd-filesystem = "bash shadow-utils"
RDEPENDS_httpd-manual = "httpd"
RPM_SONAME_REQ_httpd-tools = "ld-linux-aarch64.so.1 libapr-1.so.0 libaprutil-1.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libexpat.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1"
RDEPENDS_httpd-tools = "apr apr-util expat glibc libxcrypt openssl-libs"
RPM_SONAME_REQ_mod_ldap = "ld-linux-aarch64.so.1 libc.so.6 liblber-2.4.so.2 libldap_r-2.4.so.2 libpthread.so.0"
RDEPENDS_mod_ldap = "apr-util-ldap glibc httpd openldap"
RPM_SONAME_REQ_mod_proxy_html = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libxml2.so.2"
RDEPENDS_mod_proxy_html = "glibc httpd libxml2"
RPM_SONAME_REQ_mod_session = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_mod_session = "glibc httpd"
RPM_SONAME_REQ_mod_ssl = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libssl.so.1.1"
RDEPENDS_mod_ssl = "bash glibc httpd httpd-filesystem openssl-libs sscg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/httpd-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/httpd-devel-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/httpd-filesystem-2.4.37-21.module_el8.2.0+494+1df74eae.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/httpd-manual-2.4.37-21.module_el8.2.0+494+1df74eae.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/httpd-tools-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_ldap-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_proxy_html-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_session-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_ssl-2.4.37-21.module_el8.2.0+494+1df74eae.aarch64.rpm \
          "

SRC_URI[httpd.sha256sum] = "0e439adb44f568c67a87b9721455a9dfc169248ba5c831d2f35dad1804aaab1d"
SRC_URI[httpd-devel.sha256sum] = "1efe38f2ad4b8ad5a309706f5922d3c76a160aae9a3106bb2ade4e2737bde81d"
SRC_URI[httpd-filesystem.sha256sum] = "4d2289ce76df7bda61fb994b2e51bc3a5ac4efefcf1f7027c001b8d555b1263f"
SRC_URI[httpd-manual.sha256sum] = "269b77fe906a5e120f948ae9036350b8f98b7739c500d56b3fb6aa5dfb617886"
SRC_URI[httpd-tools.sha256sum] = "1e75d6a1a5a3a1b07c9119d3a72c9a92777cb10fdc5e11b1fd9fd582b91ddd1e"
SRC_URI[mod_ldap.sha256sum] = "3421472fab8a4c07917d7b1de02b22b93e1a5c869edadaec1c4d9c1aec6d1fbc"
SRC_URI[mod_proxy_html.sha256sum] = "0c085bf5e693523cbd21f5746b29ec8be6fcf6cd19f08ac6594cda4e0913392e"
SRC_URI[mod_session.sha256sum] = "40990c71e9191d95a276cf739f300192baba9bc59ca90d89b3783bbc4714aecc"
SRC_URI[mod_ssl.sha256sum] = "c3a7fc43d5fd55e29724f432b913bef2e7b56016c91c99e48a5e87911b573773"
