SUMMARY = "generated recipe based on ksh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ksh = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libutil.so.1"
RDEPENDS_ksh = "bash chkconfig coreutils diffutils glibc grep sed systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ksh-20120801-254.el8.aarch64.rpm \
          "

SRC_URI[ksh.sha256sum] = "b80345abe0734785c0bee0ea1eaf719ad20c058cd76e9ac761f4d55cb9c6e026"
