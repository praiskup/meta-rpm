SUMMARY = "generated recipe based on cairo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype glib-2.0 libpng libx11 libxcb libxext libxrender pixman pkgconfig-native zlib"
RPM_SONAME_PROV_cairo = "libcairo-script-interpreter.so.2 libcairo.so.2"
RPM_SONAME_REQ_cairo = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcairo-script-interpreter.so.2 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libm.so.6 libpixman-1.so.0 libpng16.so.16 libpthread.so.0 librt.so.1 libxcb-render.so.0 libxcb-shm.so.0 libxcb.so.1 libz.so.1"
RDEPENDS_cairo = "fontconfig freetype glib2 glibc libX11 libXext libXrender libpng libxcb pixman zlib"
RPM_SONAME_REQ_cairo-devel = "libcairo-script-interpreter.so.2 libcairo.so.2"
RPROVIDES_cairo-devel = "cairo-dev (= 1.15.12)"
RDEPENDS_cairo-devel = "cairo fontconfig-devel freetype-devel glib2-devel libX11-devel libXext-devel libXrender-devel libpng-devel libxcb-devel pixman-devel pkgconf-pkg-config"
RPM_SONAME_PROV_cairo-gobject = "libcairo-gobject.so.2"
RPM_SONAME_REQ_cairo-gobject = "libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpixman-1.so.0 libpng16.so.16 libpthread.so.0 librt.so.1 libxcb-render.so.0 libxcb-shm.so.0 libxcb.so.1 libz.so.1"
RDEPENDS_cairo-gobject = "cairo fontconfig freetype glib2 glibc libX11 libXext libXrender libpng libxcb pixman zlib"
RPM_SONAME_REQ_cairo-gobject-devel = "libcairo-gobject.so.2"
RPROVIDES_cairo-gobject-devel = "cairo-gobject-dev (= 1.15.12)"
RDEPENDS_cairo-gobject-devel = "cairo-devel cairo-gobject glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cairo-1.15.12-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cairo-devel-1.15.12-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cairo-gobject-1.15.12-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cairo-gobject-devel-1.15.12-3.el8.aarch64.rpm \
          "

SRC_URI[cairo.sha256sum] = "3187b5a82f1e6906539903c42b3bbd0b9979f00ae41d73c52e69e239f1090bf7"
SRC_URI[cairo-devel.sha256sum] = "ca21fe152344fb074f3fda7f7ecf078a198fbe31c447f2d0a9ee89944dcb3fde"
SRC_URI[cairo-gobject.sha256sum] = "c98e748866b96eab0e8ea4170fe312cb48fd46db98e23c6125b1262c4b8b495d"
SRC_URI[cairo-gobject-devel.sha256sum] = "ea3ac4342ee3ad91fdfdefa1f39733ed9890c2eee6c0a577f40e7269d4971757"
