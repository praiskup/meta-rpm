SUMMARY = "generated recipe based on gtksourceview3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libxml2 pango pkgconfig-native"
RPM_SONAME_PROV_gtksourceview3 = "libgtksourceview-3.0.so.1"
RPM_SONAME_REQ_gtksourceview3 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libxml2.so.2"
RDEPENDS_gtksourceview3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libxml2 pango"
RPM_SONAME_REQ_gtksourceview3-devel = "libgtksourceview-3.0.so.1"
RPROVIDES_gtksourceview3-devel = "gtksourceview3-dev (= 3.24.9)"
RDEPENDS_gtksourceview3-devel = "glib2-devel gtk3-devel gtksourceview3 libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtksourceview3-3.24.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtksourceview3-devel-3.24.9-1.el8.aarch64.rpm \
          "

SRC_URI[gtksourceview3.sha256sum] = "f1068cf74e93853f939651fb7ea6d0cbb03abdb3ccaf4669f55bc5de73372e83"
SRC_URI[gtksourceview3-devel.sha256sum] = "5543be47f90260a58625664a143532b13f33cda02e71e50eea55a5fbcea1163a"
