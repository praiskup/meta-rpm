SUMMARY = "generated recipe based on ninja-build srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_ninja-build = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_ninja-build = "emacs-filesystem glibc libgcc libstdc++ vim-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ninja-build-1.8.2-1.el8.aarch64.rpm \
          "

SRC_URI[ninja-build.sha256sum] = "56a44dd2827d3734801cd0e2daffc40929fcc1c11a61ce52259e4e670e41eb2c"
