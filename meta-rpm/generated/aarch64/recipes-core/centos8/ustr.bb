SUMMARY = "generated recipe based on ustr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_ustr = "libustr-1.0.so.1"
RPM_SONAME_REQ_ustr = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_ustr = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ustr-1.0.4-26.el8.aarch64.rpm \
          "

SRC_URI[ustr.sha256sum] = "d7da690f77bd6b827de0cc44ecdce578bbe936ded70b407b04965f6891671022"
