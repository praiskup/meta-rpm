SUMMARY = "generated recipe based on google-crosextra-carlito-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_google-crosextra-carlito-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-crosextra-carlito-fonts-1.103-0.8.20130920.el8.noarch.rpm \
          "

SRC_URI[google-crosextra-carlito-fonts.sha256sum] = "dbb4e1c8fb24913484260e7ddd17108b778356e88404bd2004963bf526950c71"
