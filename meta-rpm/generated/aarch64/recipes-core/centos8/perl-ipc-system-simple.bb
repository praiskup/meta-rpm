SUMMARY = "generated recipe based on perl-IPC-System-Simple srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-System-Simple = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-IPC-System-Simple-1.25-17.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-System-Simple.sha256sum] = "3bb97849ad44d6889629ae8aa672479f1cbc4472030d26d4f532c1faeadd8063"
