SUMMARY = "generated recipe based on pyatspi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyatspi = "at-spi2-core platform-python python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyatspi-2.26.0-6.el8.noarch.rpm \
          "

SRC_URI[python3-pyatspi.sha256sum] = "be3ee68852bc003d3bb01dfd27fcace3a19c6d136a2e09330ebe76656e0ee73a"
