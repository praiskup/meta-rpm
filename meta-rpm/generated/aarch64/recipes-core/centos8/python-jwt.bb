SUMMARY = "generated recipe based on python-jwt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jwt = "platform-python python3-cryptography"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-jwt-1.6.1-2.el8.noarch.rpm \
          "

SRC_URI[python3-jwt.sha256sum] = "ebeb05a4a9cdd5d060859eabac1349029c0120615c98fc939e26664c030655c1"
