SUMMARY = "generated recipe based on hunspell-pa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-pa = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-pa-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-pa.sha256sum] = "70bba4e05bae80c26b853caa4f61b0ac23f7d95ba8f1c44eed56eba65e1c8e0b"
