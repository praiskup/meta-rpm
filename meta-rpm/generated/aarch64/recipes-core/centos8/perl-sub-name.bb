SUMMARY = "generated recipe based on perl-Sub-Name srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sub-Name = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sub-Name = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Sub-Name-0.21-7.el8.aarch64.rpm \
          "

SRC_URI[perl-Sub-Name.sha256sum] = "9f867b26b990d2d6091d30262ce77fda6494a4510f68eb391c8f732f8363702d"
