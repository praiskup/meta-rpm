SUMMARY = "generated recipe based on cmocka srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libcmocka = "libcmocka.so.0"
RPM_SONAME_REQ_libcmocka = "ld-linux-aarch64.so.1 libc.so.6 librt.so.1"
RDEPENDS_libcmocka = "glibc"
RPM_SONAME_REQ_libcmocka-devel = "libcmocka.so.0"
RPROVIDES_libcmocka-devel = "libcmocka-dev (= 1.1.5)"
RDEPENDS_libcmocka-devel = "cmake-filesystem libcmocka pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcmocka-1.1.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcmocka-devel-1.1.5-1.el8.aarch64.rpm \
          "

SRC_URI[libcmocka.sha256sum] = "576e368e64479caa4045fc7ecc2974729a8d14acf81c29bf8ace9abbb08041e9"
SRC_URI[libcmocka-devel.sha256sum] = "8b0d3c7138c69f1b353fb4a7cb31a704699e5e81e292796e15da3e70e6a4836a"
