SUMMARY = "generated recipe based on patch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr libselinux pkgconfig-native"
RPM_SONAME_REQ_patch = "ld-linux-aarch64.so.1 libattr.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_patch = "glibc libattr libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/patch-2.7.6-11.el8.aarch64.rpm \
          "

SRC_URI[patch.sha256sum] = "67e37051f4bc8b3f1b946c2a34eb993bddc223d995a27575535c328303bc0312"
