SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_selinux-policy-sandbox = "bash selinux-policy-minimum selinux-policy-targeted"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-sandbox-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy-sandbox.sha256sum] = "298de058f7adecfaed18b0873dce2092d3621c475383b79311b14972e918d774"
