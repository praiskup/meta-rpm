SUMMARY = "generated recipe based on sudo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit libselinux openldap pam pkgconfig-native zlib"
RPM_SONAME_PROV_sudo = "libsudo_noexec.so libsudo_util.so.0"
RPM_SONAME_REQ_sudo = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libdl.so.2 liblber-2.4.so.2 libldap-2.4.so.2 libpam.so.0 libpthread.so.0 libselinux.so.1 libsudo_util.so.0 libutil.so.1 libz.so.1"
RDEPENDS_sudo = "audit-libs bash coreutils glibc libselinux openldap pam vim-minimal zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sudo-1.8.29-5.el8.aarch64.rpm \
          "

SRC_URI[sudo.sha256sum] = "65a786a7a1a90863797e4199537465bf256c8374feece67d865a81b1db959b74"
