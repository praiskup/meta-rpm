SUMMARY = "generated recipe based on libqb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libqb = "libqb.so.0"
RPM_SONAME_REQ_libqb = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 libqb.so.0"
RDEPENDS_libqb = "glibc"
RPM_SONAME_REQ_libqb-devel = "libqb.so.0"
RPROVIDES_libqb-devel = "libqb-dev (= 1.0.3)"
RDEPENDS_libqb-devel = "libqb pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libqb-1.0.3-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libqb-devel-1.0.3-10.el8.aarch64.rpm \
          "

SRC_URI[libqb.sha256sum] = "5ae93d7e3cc055c055f79f3755e668c1983f61ac289c7357dd478eee3efda277"
SRC_URI[libqb-devel.sha256sum] = "f160fa66ae233e613bd2e50def1a1d54cdceb481af51ae137aba598b74b491b1"
