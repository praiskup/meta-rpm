SUMMARY = "generated recipe based on librhsm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib libgcc openssl pkgconfig-native"
RPM_SONAME_PROV_librhsm = "librhsm.so.0"
RPM_SONAME_REQ_librhsm = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0"
RDEPENDS_librhsm = "glib2 glibc json-glib libgcc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/librhsm-0.0.3-3.el8.aarch64.rpm \
          "

SRC_URI[librhsm.sha256sum] = "399982f23dfd580a6e22c7b7f4234c834219ad9a993b539d8242273ce31a1916"
