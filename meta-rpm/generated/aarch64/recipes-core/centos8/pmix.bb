SUMMARY = "generated recipe based on pmix srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevent munge pkgconfig-native"
RPM_SONAME_PROV_pmix = "libpmi.so.1 libpmi2.so.1 libpmix.so.2"
RPM_SONAME_REQ_pmix = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libm.so.6 libmunge.so.2 libpthread.so.0"
RDEPENDS_pmix = "environment-modules glibc libevent munge-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pmix-2.1.1-1.el8.aarch64.rpm \
          "

SRC_URI[pmix.sha256sum] = "1f57b13644d41d3a17fb292c0920db5eb44090c656aa665a49a1b7f26a3c37ed"
