SUMMARY = "generated recipe based on perl-Path-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Path-Tiny = "perl-Carp perl-Digest perl-Digest-MD5 perl-Digest-SHA perl-Exporter perl-File-Path perl-File-Temp perl-PathTools perl-Unicode-UTF8 perl-constant perl-interpreter perl-libs perl-threads"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Path-Tiny-0.104-5.el8.noarch.rpm \
          "

SRC_URI[perl-Path-Tiny.sha256sum] = "bf7ea27f9ec8a74969dad805edec72bdb5b5aeaa2fd11ac7ac5d3a8d29f3afb7"
