SUMMARY = "generated recipe based on perl-Module-Install srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Install = "perl-Archive-Zip perl-CPAN perl-CPAN-Meta perl-Carp perl-Devel-PPPort perl-ExtUtils-MakeMaker perl-ExtUtils-Manifest perl-File-Path perl-File-Remove perl-File-Temp perl-Module-Build perl-Module-CoreList perl-Module-ScanDeps perl-PathTools perl-Socket perl-YAML-Tiny perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Module-Install-1.19-2.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Install.sha256sum] = "3de42fa4fd1810d280fff5e8fb807e20fa243f12f175ac47337d0fbc66392c07"
