SUMMARY = "generated recipe based on perl-YAML-Syck srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-YAML-Syck = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-YAML-Syck = "glibc perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-YAML-Syck-1.30-5.el8.aarch64.rpm \
          "

SRC_URI[perl-YAML-Syck.sha256sum] = "d6e55b13d648636a72ea72bc5c559d4bbb58cf18b661e5d2f9270bfffca829af"
