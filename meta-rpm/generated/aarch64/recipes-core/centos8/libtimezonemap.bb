SUMMARY = "generated recipe based on libtimezonemap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 json-glib librsvg libsoup-2.4 pango pkgconfig-native"
RPM_SONAME_PROV_libtimezonemap = "libtimezonemap.so.1"
RPM_SONAME_REQ_libtimezonemap = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 librsvg-2.so.2 libsoup-2.4.so.1"
RDEPENDS_libtimezonemap = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 json-glib librsvg2 libsoup pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtimezonemap-0.4.5.1-3.el8.aarch64.rpm \
          "

SRC_URI[libtimezonemap.sha256sum] = "e71178f90f4252b0aa0295e1476c6417adb6e8e38f67dce3dcfe06401202ba85"
