SUMMARY = "generated recipe based on perl-Digest srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Digest = "perl-Carp perl-Exporter perl-MIME-Base64 perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Digest-1.17-395.el8.noarch.rpm \
          "

SRC_URI[perl-Digest.sha256sum] = "7e67dba2509f90064325e62e49412d5284a55f09b7b6878b9c8222692c500dde"
