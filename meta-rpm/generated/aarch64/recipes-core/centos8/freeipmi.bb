SUMMARY = "generated recipe based on freeipmi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcrypt pkgconfig-native"
RPM_SONAME_PROV_freeipmi = "libfreeipmi.so.17 libipmiconsole.so.2 libipmidetect.so.0 libipmimonitoring.so.6"
RPM_SONAME_REQ_freeipmi = "ld-linux-aarch64.so.1 libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libipmiconsole.so.2 libipmidetect.so.0 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi = "bash glibc libgcrypt systemd"
RPM_SONAME_REQ_freeipmi-bmc-watchdog = "ld-linux-aarch64.so.1 libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi-bmc-watchdog = "bash freeipmi glibc libgcrypt"
RPM_SONAME_REQ_freeipmi-devel = "libfreeipmi.so.17 libipmiconsole.so.2 libipmidetect.so.0 libipmimonitoring.so.6"
RPROVIDES_freeipmi-devel = "freeipmi-dev (= 1.6.1)"
RDEPENDS_freeipmi-devel = "freeipmi pkgconf-pkg-config"
RPM_SONAME_REQ_freeipmi-ipmidetectd = "ld-linux-aarch64.so.1 libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi-ipmidetectd = "bash freeipmi glibc libgcrypt"
RPM_SONAME_REQ_freeipmi-ipmiseld = "ld-linux-aarch64.so.1 libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi-ipmiseld = "bash freeipmi glibc libgcrypt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/freeipmi-1.6.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/freeipmi-bmc-watchdog-1.6.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/freeipmi-ipmidetectd-1.6.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/freeipmi-ipmiseld-1.6.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/freeipmi-devel-1.6.1-1.el8.aarch64.rpm \
          "

SRC_URI[freeipmi.sha256sum] = "04ebfcc220ea7b3dc5a285e6d6740bca2bb76c06540449965a5b3c62d184335c"
SRC_URI[freeipmi-bmc-watchdog.sha256sum] = "f853e97999d520dab2e8ff7246fb74398e2de4e8ff895de8156e94392123227f"
SRC_URI[freeipmi-devel.sha256sum] = "89ea1aca488d3020183834c0524c6c68f6c0a70a14704cd2d3c8fea79d3434fb"
SRC_URI[freeipmi-ipmidetectd.sha256sum] = "5b10d0f0611c5852f5cdd5df342223618fa0bf7946fba22b67815928831d04b2"
SRC_URI[freeipmi-ipmiseld.sha256sum] = "4a2046f73ef3ebc41c5bb7d7f0a59ce1c0f158fee3bf11aad581d92de470c3c5"
