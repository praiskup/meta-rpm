SUMMARY = "generated recipe based on hunspell-quh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-quh = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-quh-0.20110816-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-quh.sha256sum] = "b14b07eb8d0acb844c8aa6de08cf54802feeb6de2ed6bb57292bc8323c64f891"
