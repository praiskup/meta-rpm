SUMMARY = "generated recipe based on icoutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng pkgconfig-native zlib"
RPM_SONAME_REQ_icoutils = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_icoutils = "glibc libpng perl-Getopt-Long perl-HTTP-Message perl-PathTools perl-interpreter perl-libwww-perl zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/icoutils-0.32.3-2.el8.aarch64.rpm \
          "

SRC_URI[icoutils.sha256sum] = "677c0dd13d6c860e7a523cf9b3d6202edad9dc96ab346a02cfd7143fb96b497b"
