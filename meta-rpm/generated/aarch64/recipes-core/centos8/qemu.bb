SUMMARY = "generated recipe based on qemu-kvm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ceph curl cyrus-sasl-lib device-mapper-multipath dtc glib-2.0 gnutls libaio libgcc libgcrypt libgpg-error libiscsi libpng libseccomp libssh libusb1 libxkbcommon lzo numactl pixman pkgconfig-native rdma-core snappy systemd-libs zlib"
RPM_SONAME_REQ_qemu-guest-agent = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libudev.so.1"
RDEPENDS_qemu-guest-agent = "bash glib2 glibc libgcc libstdc++ systemd systemd-libs"
RPM_SONAME_REQ_qemu-img = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-img = "glib2 glibc gnutls libaio libgcc libgcrypt libgpg-error libstdc++ zlib"
RDEPENDS_qemu-kvm = "qemu-kvm-block-curl qemu-kvm-block-iscsi qemu-kvm-block-rbd qemu-kvm-block-ssh qemu-kvm-core"
RPM_SONAME_REQ_qemu-kvm-block-curl = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-curl = "glib2 glibc gnutls libcurl libgcc libgcrypt libgpg-error libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-iscsi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libiscsi.so.8 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-iscsi = "glib2 glibc gnutls libgcc libgcrypt libgpg-error libiscsi libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-rbd = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librados.so.2 librbd.so.1 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-rbd = "glib2 glibc gnutls libgcc libgcrypt libgpg-error librados2 librbd1 libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-ssh = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssh.so.4 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-ssh = "glib2 glibc gnutls libgcc libgcrypt libgpg-error libssh libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-common = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libmpathpersist.so.0 libmultipath.so.0 libpthread.so.0 librt.so.1 libstdc++.so.6 libudev.so.1 libutil.so.1 libxkbcommon.so.0 libz.so.1"
RDEPENDS_qemu-kvm-common = "bash device-mapper-multipath-libs glib2 glibc glibc-common gnutls libgcc libgcrypt libgpg-error libstdc++ libxkbcommon platform-python shadow-utils systemd systemd-libs zlib"
RPM_SONAME_REQ_qemu-kvm-core = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libdl.so.2 libfdt.so.1 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libibumad.so.3 libibverbs.so.1 liblzo2.so.2 libm.so.6 libnuma.so.1 libpixman-1.so.0 libpng16.so.16 libpthread.so.0 librdmacm.so.1 librt.so.1 libsasl2.so.3 libseccomp.so.2 libsnappy.so.1 libstdc++.so.6 libusb-1.0.so.0 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-core = "bash cyrus-sasl-lib edk2-aarch64 glib2 glibc gnutls libaio libfdt libgcc libgcrypt libgpg-error libibumad libibverbs libpng librdmacm libseccomp libstdc++ libusbx lzo numactl-libs pixman qemu-img qemu-kvm-common snappy zlib"
RPM_SONAME_REQ_qemu-kvm-tests = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-tests = "bash glib2 glibc gnutls libgcc libgcrypt libgpg-error libstdc++ platform-python qemu-kvm zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-guest-agent-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-img-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-block-curl-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-block-iscsi-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-block-rbd-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-block-ssh-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-common-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qemu-kvm-core-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qemu-kvm-tests-2.12.0-99.module_el8.2.0+524+f765f7e0.4.aarch64.rpm \
          "

SRC_URI[qemu-guest-agent.sha256sum] = "1c7df94ed47a4d158511da745263aa01701b33892262f231972196c0e2e409ff"
SRC_URI[qemu-img.sha256sum] = "05172989b942102bfa3cc6a94d6e78b5a35d4c031598d062b1e379c3012a5aef"
SRC_URI[qemu-kvm.sha256sum] = "accf5af08f6c293d093459b84a96f1f19740711fd6df10ed03ff4142639b5925"
SRC_URI[qemu-kvm-block-curl.sha256sum] = "082b05d4cdf07319c0e04df66282252f9f218ad1e7bd8894ab63a419ae572ce8"
SRC_URI[qemu-kvm-block-iscsi.sha256sum] = "8cc77cdd6b5df1c4db804b0a9b544dfa5ff990452ba5f3c26828e8cae74985fc"
SRC_URI[qemu-kvm-block-rbd.sha256sum] = "4a9209be5def4033c8f81148e1a3141fb98f6f85a5299040f6310c8c039c20d0"
SRC_URI[qemu-kvm-block-ssh.sha256sum] = "a8dab4f0b4f4f80dd70610aeea88c9dac00f04cb6bf20fb7c7839766c08cc776"
SRC_URI[qemu-kvm-common.sha256sum] = "39b389078975dd13741a6e4c054e1d679d58f06320ef25c9edd9655c860ac7a4"
SRC_URI[qemu-kvm-core.sha256sum] = "7db8eca17586b480cada2fda22d3d9b171f473e43bf308293dae1493f98f63b2"
SRC_URI[qemu-kvm-tests.sha256sum] = "9d4494869922eb183dc9d34148a8406c7eb648f69fe6e19ed3fe8dde5ce13516"
