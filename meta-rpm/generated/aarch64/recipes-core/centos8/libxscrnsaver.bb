SUMMARY = "generated recipe based on libXScrnSaver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXScrnSaver = "libXss.so.1"
RPM_SONAME_REQ_libXScrnSaver = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXScrnSaver = "glibc libX11 libXext"
RPM_SONAME_REQ_libXScrnSaver-devel = "libXss.so.1"
RPROVIDES_libXScrnSaver-devel = "libXScrnSaver-dev (= 1.2.3)"
RDEPENDS_libXScrnSaver-devel = "libX11-devel libXScrnSaver libXext-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXScrnSaver-1.2.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXScrnSaver-devel-1.2.3-1.el8.aarch64.rpm \
          "

SRC_URI[libXScrnSaver.sha256sum] = "808db25034351af27b16fd49ba857fab1bd6b295123281e1d525b4341e0a6fd7"
SRC_URI[libXScrnSaver-devel.sha256sum] = "836b52bbed47d9ca4c98197dbf86b62bfc5e2fc669b05df844256ea9e359be38"
