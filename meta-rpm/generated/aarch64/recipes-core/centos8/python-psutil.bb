SUMMARY = "generated recipe based on python-psutil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-psutil = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-psutil = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-psutil-5.4.3-10.el8.aarch64.rpm \
          "

SRC_URI[python3-psutil.sha256sum] = "e10a61b18ba2ea79a1d1aadf2e7fb15229df182a49a636d67bb46b00cea2b4d6"
