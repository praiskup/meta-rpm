SUMMARY = "generated recipe based on libgpod srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 libimobiledevice libplist libusb1 libxml2 pkgconfig-native sg3-utils sqlite3 zlib"
RPM_SONAME_PROV_libgpod = "libgpod.so.4"
RPM_SONAME_REQ_libgpod = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpod.so.4 libimobiledevice.so.6 libm.so.6 libplist.so.3 libpthread.so.0 libsgutils2.so.2 libsqlite3.so.0 libusb-1.0.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_libgpod = "gdk-pixbuf2 glib2 glibc libimobiledevice libplist libusbx libxml2 sg3_utils-libs sqlite-libs systemd zlib"
RPM_SONAME_REQ_libgpod-devel = "libgpod.so.4"
RPROVIDES_libgpod-devel = "libgpod-dev (= 0.8.3)"
RDEPENDS_libgpod-devel = "gdk-pixbuf2-devel glib2-devel libgpod libimobiledevice-devel pkgconf-pkg-config"
RDEPENDS_libgpod-doc = "libgpod"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgpod-0.8.3-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgpod-devel-0.8.3-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgpod-doc-0.8.3-23.el8.aarch64.rpm \
          "

SRC_URI[libgpod.sha256sum] = "9cdb1d17970370d83121d0c8243b3da3f8960d4765c15bf72f640161919539e9"
SRC_URI[libgpod-devel.sha256sum] = "cadac274daf4bf06be4f41b2aee46714e70446a4c09aefb30c2ba49de5d22f25"
SRC_URI[libgpod-doc.sha256sum] = "13958fc8515f6d481745e004452c7d48ff5c4339003764bbc895edf16024e5c3"
