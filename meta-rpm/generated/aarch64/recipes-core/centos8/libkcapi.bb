SUMMARY = "generated recipe based on libkcapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libkcapi = "libkcapi.so.1"
RPM_SONAME_REQ_libkcapi = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libkcapi = "glibc systemd"
RPM_SONAME_REQ_libkcapi-hmaccalc = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libkcapi.so.1"
RDEPENDS_libkcapi-hmaccalc = "glibc libkcapi"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libkcapi-1.1.1-16_1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libkcapi-hmaccalc-1.1.1-16_1.el8.aarch64.rpm \
          "

SRC_URI[libkcapi.sha256sum] = "1320860e686f2c065412f51b96538ec373ac521a90d83450cecb12ac7d1c8166"
SRC_URI[libkcapi-hmaccalc.sha256sum] = "afe4286cd6ffb6412f6790c0c8c6c14f9b1b207642f81926c178f9dea4f457ff"
