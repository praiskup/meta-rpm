SUMMARY = "generated recipe based on maven-dependency-tree srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-dependency-tree = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-util plexus-containers-component-annotations"
RDEPENDS_maven-dependency-tree-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-dependency-tree-3.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-dependency-tree-javadoc-3.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-dependency-tree.sha256sum] = "37431388a8934fcc32f8ce2994131caea34f743675ad9207c2427ad0a433cc45"
SRC_URI[maven-dependency-tree-javadoc.sha256sum] = "a37085a0b6dfde37557dd105b9dc9fc9eab4e0510ddcacb9b894749e0133f0c0"
