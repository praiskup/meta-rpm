SUMMARY = "generated recipe based on libXi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxfixes pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXi = "libXi.so.6"
RPM_SONAME_REQ_libXi = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXi = "glibc libX11 libXext"
RPM_SONAME_REQ_libXi-devel = "libXi.so.6"
RPROVIDES_libXi-devel = "libXi-dev (= 1.7.9)"
RDEPENDS_libXi-devel = "libX11-devel libXext-devel libXfixes-devel libXi pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXi-1.7.9-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXi-devel-1.7.9-7.el8.aarch64.rpm \
          "

SRC_URI[libXi.sha256sum] = "cf929c00f52b76f1a9b092160e45a5cd5c5925b3adebba492afefcf0c3e76a27"
SRC_URI[libXi-devel.sha256sum] = "67d3fac81521cdbd7daeb0f2f01cd0c5b73be6d6b795ca828879e28b71185a2f"
