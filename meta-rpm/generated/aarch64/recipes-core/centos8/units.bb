SUMMARY = "generated recipe based on units srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native readline"
RPM_SONAME_REQ_units = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libreadline.so.7"
RDEPENDS_units = "bash glibc info platform-python readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/units-2.17-5.el8.aarch64.rpm \
          "

SRC_URI[units.sha256sum] = "afbdd1f40f6a7c53e8fac1c9cd531d1167d9242aed1ae9099c190e85fc8d9faa"
