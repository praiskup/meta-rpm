SUMMARY = "generated recipe based on mythes-sv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-sv = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-sv-1.3-13.el8.noarch.rpm \
          "

SRC_URI[mythes-sv.sha256sum] = "8205e5e9a063e5f98ee6fd4633710319e7360d8c0128f4a105ca3d929e832782"
