SUMMARY = "generated recipe based on perl-Thread-Queue srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Thread-Queue = "perl-Carp perl-Scalar-List-Utils perl-libs perl-threads-shared"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Thread-Queue-3.13-1.el8.noarch.rpm \
          "

SRC_URI[perl-Thread-Queue.sha256sum] = "53149cdc42eb5d0522e8259c18053410b25a975f8906feb1a3b372258779d36c"
