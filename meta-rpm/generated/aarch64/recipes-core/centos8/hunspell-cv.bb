SUMMARY = "generated recipe based on hunspell-cv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-cv = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-cv-1.06-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-cv.sha256sum] = "e8bad501b681bf2c6e08f2eee041ad359c24b774feda5bb06e5115a7b6c808a8"
