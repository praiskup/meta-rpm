SUMMARY = "generated recipe based on grafana-pcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_grafana-pcp = "grafana"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-pcp-1.0.5-3.el8.noarch.rpm \
          "

SRC_URI[grafana-pcp.sha256sum] = "0037aa9511284622e81132ec94996ba967911b99c406be1965b98217e3daf37a"
