SUMMARY = "generated recipe based on libdatrie srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdatrie = "libdatrie.so.1"
RPM_SONAME_REQ_libdatrie = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libdatrie = "glibc"
RPM_SONAME_REQ_libdatrie-devel = "ld-linux-aarch64.so.1 libc.so.6 libdatrie.so.1"
RPROVIDES_libdatrie-devel = "libdatrie-dev (= 0.2.9)"
RDEPENDS_libdatrie-devel = "glibc libdatrie pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdatrie-0.2.9-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdatrie-devel-0.2.9-7.el8.aarch64.rpm \
          "

SRC_URI[libdatrie.sha256sum] = "cdab185b15a5de20df3c2a675332c18346ac155f20f8ae70953c7e3104816ae5"
SRC_URI[libdatrie-devel.sha256sum] = "eb2dfe7ad5f4923a628dabd473a3ea8f436c273f68cef9d7421519ae3cd7ccf3"
