SUMMARY = "generated recipe based on a52dec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liba52 = "liba52.so.0"
RPM_SONAME_REQ_liba52 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_liba52 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liba52-0.7.4-32.el8.aarch64.rpm \
          "

SRC_URI[liba52.sha256sum] = "5c1a9b0ab9e1438763e41ab608bc11eb1ed2264bd1f42d8a0a998b6a283c7f94"
