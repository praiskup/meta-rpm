SUMMARY = "generated recipe based on scotch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 mpich openmpi pkgconfig-native zlib"
RPM_SONAME_PROV_ptscotch-mpich = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPM_SONAME_REQ_ptscotch-mpich = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libm.so.6 libmpi.so.12 libpthread.so.0 libptscotch.so.0 libptscotcherrexit.so.0 librt.so.1 libscotch.so.0 libz.so.1"
RDEPENDS_ptscotch-mpich = "bzip2-libs glibc mpich zlib"
RPM_SONAME_REQ_ptscotch-mpich-devel = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPROVIDES_ptscotch-mpich-devel = "ptscotch-mpich-dev (= 6.0.5)"
RDEPENDS_ptscotch-mpich-devel = "ptscotch-mpich"
RDEPENDS_ptscotch-mpich-devel-parmetis = "ptscotch-mpich-devel"
RPM_SONAME_PROV_ptscotch-openmpi = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPM_SONAME_REQ_ptscotch-openmpi = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libm.so.6 libmpi.so.40 libpthread.so.0 libptscotch.so.0 libptscotcherrexit.so.0 librt.so.1 libscotch.so.0 libz.so.1"
RDEPENDS_ptscotch-openmpi = "bzip2-libs glibc openmpi zlib"
RPM_SONAME_REQ_ptscotch-openmpi-devel = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPROVIDES_ptscotch-openmpi-devel = "ptscotch-openmpi-dev (= 6.0.5)"
RDEPENDS_ptscotch-openmpi-devel = "ptscotch-openmpi"
RPM_SONAME_PROV_scotch = "libesmumps.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0 libscotchmetis.so.0"
RPM_SONAME_REQ_scotch = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libm.so.6 libpthread.so.0 librt.so.1 libscotch.so.0 libscotcherrexit.so.0 libz.so.1"
RDEPENDS_scotch = "bzip2-libs glibc zlib"
RPM_SONAME_REQ_scotch-devel = "libesmumps.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0 libscotchmetis.so.0"
RPROVIDES_scotch-devel = "scotch-dev (= 6.0.5)"
RDEPENDS_scotch-devel = "scotch"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ptscotch-mpich-6.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ptscotch-mpich-devel-6.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ptscotch-mpich-devel-parmetis-6.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ptscotch-openmpi-6.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ptscotch-openmpi-devel-6.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/scotch-6.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/scotch-devel-6.0.5-1.el8.aarch64.rpm \
          "

SRC_URI[ptscotch-mpich.sha256sum] = "662bc46046f53c93e38cced8e5a42e1e5d93e5d2fd4915877c0d9e0cb9d5e0db"
SRC_URI[ptscotch-mpich-devel.sha256sum] = "c8f48207259720aaf1106557c435ff42a30dfb96a3e302f20681f40b90e4bcc2"
SRC_URI[ptscotch-mpich-devel-parmetis.sha256sum] = "c247ee77d52856b3fd6d44d5aa0714fd287e37685a905246eadf99f321b77788"
SRC_URI[ptscotch-openmpi.sha256sum] = "e9b611413fa4df88687d266b56c712364363080bcdc3c245f5e89af8a61c24a3"
SRC_URI[ptscotch-openmpi-devel.sha256sum] = "5a62272c8e6ae16645810380b350f3ad83b5e4bf842b15537a32dc57aa04f6dc"
SRC_URI[scotch.sha256sum] = "e1b513f38c241027b2670bfa123b6f02c850526aa82aaccaa2bdce19f793e1bd"
SRC_URI[scotch-devel.sha256sum] = "f1449119aa25bdf64eafd7d33b75dc625c4bb2db7974f6059d0d08e20a9a7996"
