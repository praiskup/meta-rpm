SUMMARY = "generated recipe based on sound-theme-freedesktop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sound-theme-freedesktop = "bash coreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sound-theme-freedesktop-0.8-9.el8.noarch.rpm \
          "

SRC_URI[sound-theme-freedesktop.sha256sum] = "8432cb92b20c99b7538cc95260c87ef32a0c3f8a95b2623368917fabaf61249b"
