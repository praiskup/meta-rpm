SUMMARY = "generated recipe based on jdom2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jdom2 = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jdom2-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdom2-2.0.6-12.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdom2-javadoc-2.0.6-12.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jdom2.sha256sum] = "0f9ee79c89edbc0bde40620b24228b8bfad25ce8d054640e7cdf6b017d18dbd9"
SRC_URI[jdom2-javadoc.sha256sum] = "1e9e47cd88208c9745404d828e00f3f3c56af7f6772300476dda2cc98dedb786"
