SUMMARY = "generated recipe based on perl-DateTime-TimeZone-SystemV srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-TimeZone-SystemV = "perl-Carp perl-Date-ISO8601 perl-Params-Classify perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-DateTime-TimeZone-SystemV-0.010-3.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-TimeZone-SystemV.sha256sum] = "45c02e8ed9faea64766be961999308de2fe7decb5b905731f968ac024b165318"
