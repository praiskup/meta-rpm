SUMMARY = "generated recipe based on x3270 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxaw libxmu libxt ncurses openssl pkgconfig-native readline"
RPM_SONAME_REQ_x3270 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libssl.so.1.1 libutil.so.1"
RDEPENDS_x3270 = "glibc openssl-libs"
RPM_SONAME_REQ_x3270-text = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libncursesw.so.6 libreadline.so.7 libssl.so.1.1 libtinfo.so.6 libutil.so.1"
RDEPENDS_x3270-text = "glibc ncurses-libs openssl-libs readline x3270"
RPM_SONAME_REQ_x3270-x11 = "ld-linux-aarch64.so.1 libX11.so.6 libXaw.so.7 libXmu.so.6 libXt.so.6 libc.so.6 libcrypto.so.1.1 libssl.so.1.1 libutil.so.1"
RDEPENDS_x3270-x11 = "bash glibc libX11 libXaw libXmu libXt openssl-libs x3270 xorg-x11-font-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/x3270-x11-3.6ga5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/x3270-3.6ga5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/x3270-text-3.6ga5-1.el8.aarch64.rpm \
          "

SRC_URI[x3270.sha256sum] = "9e33a2795829157a7c03124a6fe83c6a7de123b1608987018a5535a11c3cd912"
SRC_URI[x3270-text.sha256sum] = "bf0840072ecdd53ff20f6f0b863542c97394be7ae9e9a3ba46179fe768991370"
SRC_URI[x3270-x11.sha256sum] = "e9d0ab6654f1bafd0383b891453280a64a6f94306d2fdb787ea9f350cab145e5"
