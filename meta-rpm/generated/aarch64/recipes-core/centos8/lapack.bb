SUMMARY = "generated recipe based on lapack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc pkgconfig-native"
RPM_SONAME_PROV_blas = "libblas.so.3 libcblas.so.3"
RPM_SONAME_REQ_blas = "ld-linux-aarch64.so.1 libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_blas = "glibc libgfortran"
RPM_SONAME_REQ_blas-devel = "libblas.so.3 libcblas.so.3"
RPROVIDES_blas-devel = "blas-dev (= 3.8.0)"
RDEPENDS_blas-devel = "blas gcc-gfortran pkgconf-pkg-config"
RPM_SONAME_PROV_blas64 = "libblas64_.so.3 libcblas64_.so.3"
RPM_SONAME_REQ_blas64 = "ld-linux-aarch64.so.1 libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_blas64 = "blas glibc libgfortran"
RPM_SONAME_PROV_lapack = "liblapack.so.3 liblapacke.so.3"
RPM_SONAME_REQ_lapack = "ld-linux-aarch64.so.1 libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_lapack = "blas glibc libgfortran"
RPM_SONAME_REQ_lapack-devel = "liblapack.so.3 liblapacke.so.3"
RPROVIDES_lapack-devel = "lapack-dev (= 3.8.0)"
RDEPENDS_lapack-devel = "blas-devel lapack pkgconf-pkg-config"
RDEPENDS_lapack-static = "lapack-devel"
RPM_SONAME_PROV_lapack64 = "liblapack64_.so.3"
RPM_SONAME_REQ_lapack64 = "ld-linux-aarch64.so.1 libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_lapack64 = "blas blas64 glibc libgfortran"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/blas-3.8.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/blas64-3.8.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lapack-3.8.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lapack64-3.8.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/blas-devel-3.8.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lapack-devel-3.8.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lapack-static-3.8.0-8.el8.aarch64.rpm \
          "

SRC_URI[blas.sha256sum] = "178c0ae3a94f295fd40ecb7e084cf0cdfb4ca0b3e4d0424b0d3cb0cd6c463279"
SRC_URI[blas-devel.sha256sum] = "62297bf1d6551c89d70e886c61fbc8b9c68490b6d62fc317883175f3c0d2ec98"
SRC_URI[blas64.sha256sum] = "eff103975d8a496f25c5f0552a6e1b701d3071e3b3c66485ede808d9652ba391"
SRC_URI[lapack.sha256sum] = "3a4698505ff9c44ea90a54330611758b2f6ef343e540386c3685035339dd4d98"
SRC_URI[lapack-devel.sha256sum] = "3abd4c370c652fab254a61cf45517bb087ae8596171feaff51c0abf49a9954af"
SRC_URI[lapack-static.sha256sum] = "ced9db89b104e27ef4dd8c8ae546d1ce8cb500c8f2e7ea28a83c9c93a1ab9a40"
SRC_URI[lapack64.sha256sum] = "a27187e1202c138f14108bf83e7f87cdd775946896f6ee1cdbe27d56a9dc33ea"
