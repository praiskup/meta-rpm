SUMMARY = "generated recipe based on pkgconf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "pkgconfig"
RPM_SONAME_PROV_libpkgconf = "libpkgconf.so.3"
RPM_SONAME_REQ_libpkgconf = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libpkgconf = "glibc"
RPM_SONAME_REQ_pkgconf = "ld-linux-aarch64.so.1 libc.so.6 libpkgconf.so.3"
RDEPENDS_pkgconf = "glibc libpkgconf"
RDEPENDS_pkgconf-pkg-config = "bash pkgconf pkgconf-m4"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpkgconf-1.4.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pkgconf-1.4.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pkgconf-m4-1.4.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pkgconf-pkg-config-1.4.2-1.el8.aarch64.rpm \
          "

SRC_URI[libpkgconf.sha256sum] = "8f3e34df67e6c4a20bd7617f17d1199f0441a626fbab8059ddc6bf06c7ff4e78"
SRC_URI[pkgconf.sha256sum] = "9a2c046a45d46e681f417f3b438d4bb5a21e1b93deacb59d906b8aa08a7535ad"
SRC_URI[pkgconf-m4.sha256sum] = "56187f25e8ae7c2a5ce228d13c6e93b9c6a701960d61dff8ad720a8879b6059e"
SRC_URI[pkgconf-pkg-config.sha256sum] = "aadca7b635ac2b30c3463a4edfe38eaee2c6064181cb090694619186747f3950"
