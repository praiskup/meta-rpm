SUMMARY = "generated recipe based on mtdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_mtdev = "libmtdev.so.1"
RPM_SONAME_REQ_mtdev = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mtdev = "glibc"
RPM_SONAME_REQ_mtdev-devel = "ld-linux-aarch64.so.1 libc.so.6 libmtdev.so.1"
RPROVIDES_mtdev-devel = "mtdev-dev (= 1.1.5)"
RDEPENDS_mtdev-devel = "glibc mtdev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mtdev-1.1.5-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mtdev-devel-1.1.5-12.el8.aarch64.rpm \
          "

SRC_URI[mtdev.sha256sum] = "ba8ef4b42a21026f2acd3441cac6186d07c7c534ca2c55b469dc8bb90853c4c5"
SRC_URI[mtdev-devel.sha256sum] = "e16a2906074b20b66648c8cccf501eb8d1d98179fbd3bff00638a623716ea40e"
