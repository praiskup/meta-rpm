SUMMARY = "generated recipe based on ilmbase srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd mesa-libglu pkgconfig-native"
RPM_SONAME_PROV_ilmbase = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmThread-2_2.so.12 libImath-2_2.so.12"
RPM_SONAME_REQ_ilmbase = "ld-linux-aarch64.so.1 libIex-2_2.so.12 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_ilmbase = "glibc libgcc libstdc++"
RPM_SONAME_REQ_ilmbase-devel = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmThread-2_2.so.12 libImath-2_2.so.12"
RPROVIDES_ilmbase-devel = "ilmbase-dev (= 2.2.0)"
RDEPENDS_ilmbase-devel = "ilmbase libglvnd-devel mesa-libGLU-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ilmbase-2.2.0-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ilmbase-devel-2.2.0-11.el8.aarch64.rpm \
          "

SRC_URI[ilmbase.sha256sum] = "796ab391d7f248470e0da2fc89f53fb9b7261751fda06dcc9e555f5d2e8b99fa"
SRC_URI[ilmbase-devel.sha256sum] = "565be666f5a8c5400555aca28929d64eb875e568490bff59ab6cd0637e8829bc"
