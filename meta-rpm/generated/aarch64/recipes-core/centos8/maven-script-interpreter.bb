SUMMARY = "generated recipe based on maven-script-interpreter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-script-interpreter = "ant-lib bsh java-1.8.0-openjdk-headless javapackages-filesystem maven-lib plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-script-interpreter-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-script-interpreter-1.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-script-interpreter-javadoc-1.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-script-interpreter.sha256sum] = "040fb5ae1d5878b5b328b6f30ce939f86913f3e645cc0cba64ebfb38f7e0ed45"
SRC_URI[maven-script-interpreter-javadoc.sha256sum] = "e9c24b26ac07b1c833be3d3c94cb6b434cb681097e95f37524e3b3217b696abb"
