SUMMARY = "generated recipe based on httpcomponents-project srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_httpcomponents-project = "apache-parent java-1.8.0-openjdk-headless javapackages-filesystem maven-jar-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/httpcomponents-project-9-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[httpcomponents-project.sha256sum] = "3df3229103ab3b0837b522d26c71bf66aaba921e54c55951d85d65e5251a277c"
