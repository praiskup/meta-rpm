SUMMARY = "generated recipe based on libXres srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXres = "libXRes.so.1"
RPM_SONAME_REQ_libXres = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXres = "glibc libX11 libXext"
RPM_SONAME_REQ_libXres-devel = "libXRes.so.1"
RPROVIDES_libXres-devel = "libXres-dev (= 1.2.0)"
RDEPENDS_libXres-devel = "libX11-devel libXext-devel libXres pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXres-1.2.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libXres-devel-1.2.0-4.el8.aarch64.rpm \
          "

SRC_URI[libXres.sha256sum] = "1108e2ed1ad4d6ca179baee2c637b7b85630b00d14d6c961ded69d9576fe6b1a"
SRC_URI[libXres-devel.sha256sum] = "5427c33fc774a81d812550ef2efbc0f7919a0ab92a88235f90b163cd46a2c852"
