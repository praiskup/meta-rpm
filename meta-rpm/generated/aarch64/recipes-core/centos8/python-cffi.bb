SUMMARY = "generated recipe based on python-cffi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cffi = "ld-linux-aarch64.so.1 libc.so.6 libffi.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-cffi = "glibc libffi platform-python python3-libs python3-pycparser"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-cffi-1.11.5-5.el8.aarch64.rpm \
          "

SRC_URI[python3-cffi.sha256sum] = "7cf94e71d42aecccf095c8225aabe5085f8cf7fb4f956fabbe04d23ba7688029"
