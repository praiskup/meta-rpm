SUMMARY = "generated recipe based on procps-ng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native systemd-libs"
RPM_SONAME_PROV_procps-ng = "libprocps.so.7"
RPM_SONAME_REQ_procps-ng = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libncurses.so.6 libncursesw.so.6 libprocps.so.7 libsystemd.so.0 libtinfo.so.6"
RPROVIDES_procps-ng = "procps (= 3.3.15)"
RDEPENDS_procps-ng = "glibc ncurses-libs systemd-libs"
RDEPENDS_procps-ng-i18n = "procps-ng"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/procps-ng-3.3.15-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/procps-ng-i18n-3.3.15-1.el8.noarch.rpm \
          "

SRC_URI[procps-ng.sha256sum] = "9ce1e465eaea7a0024ca3679052997575b62e97e63eb5055c874240e922b3918"
SRC_URI[procps-ng-i18n.sha256sum] = "ad21d640344f96b0e5d089ae2096579db5267231eafa3a1e126361988dfee099"
