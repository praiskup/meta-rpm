SUMMARY = "generated recipe based on xmlrpc-c srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc openssl pkgconfig-native"
RPM_SONAME_PROV_xmlrpc-c = "libxmlrpc.so.3 libxmlrpc_abyss.so.3 libxmlrpc_openssl.so.1 libxmlrpc_server.so.3 libxmlrpc_server_abyss.so.3 libxmlrpc_server_cgi.so.3 libxmlrpc_util.so.4 libxmlrpc_xmlparse.so.3 libxmlrpc_xmltok.so.3"
RPM_SONAME_REQ_xmlrpc-c = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libpthread.so.0 libssl.so.1.1 libxmlrpc.so.3 libxmlrpc_abyss.so.3 libxmlrpc_server.so.3 libxmlrpc_util.so.4 libxmlrpc_xmlparse.so.3 libxmlrpc_xmltok.so.3"
RDEPENDS_xmlrpc-c = "glibc libgcc openssl-libs"
RPM_SONAME_PROV_xmlrpc-c-c++ = "libxmlrpc++.so.8 libxmlrpc_abyss++.so.8 libxmlrpc_cpp.so.8 libxmlrpc_packetsocket.so.8 libxmlrpc_server++.so.8 libxmlrpc_server_abyss++.so.8 libxmlrpc_server_cgi++.so.8 libxmlrpc_server_pstream++.so.8 libxmlrpc_util++.so.8"
RPM_SONAME_REQ_xmlrpc-c-c++ = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libstdc++.so.6 libxmlrpc++.so.8 libxmlrpc.so.3 libxmlrpc_abyss++.so.8 libxmlrpc_abyss.so.3 libxmlrpc_packetsocket.so.8 libxmlrpc_server++.so.8 libxmlrpc_server.so.3 libxmlrpc_server_abyss.so.3 libxmlrpc_util++.so.8 libxmlrpc_util.so.4"
RDEPENDS_xmlrpc-c-c++ = "glibc libgcc libstdc++ xmlrpc-c"
RPM_SONAME_PROV_xmlrpc-c-client = "libxmlrpc_client.so.3"
RPM_SONAME_REQ_xmlrpc-c-client = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libxmlrpc.so.3 libxmlrpc_util.so.4"
RDEPENDS_xmlrpc-c-client = "glibc libcurl xmlrpc-c"
RPM_SONAME_PROV_xmlrpc-c-client++ = "libxmlrpc_client++.so.8"
RPM_SONAME_REQ_xmlrpc-c-client++ = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libstdc++.so.6 libxmlrpc++.so.8 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_packetsocket.so.8 libxmlrpc_util++.so.8 libxmlrpc_util.so.4"
RDEPENDS_xmlrpc-c-client++ = "glibc libgcc libstdc++ xmlrpc-c xmlrpc-c-c++ xmlrpc-c-client"
RPM_SONAME_REQ_xmlrpc-c-devel = "libxmlrpc++.so.8 libxmlrpc.so.3 libxmlrpc_abyss++.so.8 libxmlrpc_abyss.so.3 libxmlrpc_client++.so.8 libxmlrpc_client.so.3 libxmlrpc_cpp.so.8 libxmlrpc_openssl.so.1 libxmlrpc_packetsocket.so.8 libxmlrpc_server++.so.8 libxmlrpc_server.so.3 libxmlrpc_server_abyss++.so.8 libxmlrpc_server_abyss.so.3 libxmlrpc_server_cgi++.so.8 libxmlrpc_server_cgi.so.3 libxmlrpc_server_pstream++.so.8 libxmlrpc_util++.so.8 libxmlrpc_util.so.4 libxmlrpc_xmlparse.so.3 libxmlrpc_xmltok.so.3"
RPROVIDES_xmlrpc-c-devel = "xmlrpc-c-dev (= 1.51.0)"
RDEPENDS_xmlrpc-c-devel = "bash libcurl-devel openssl-devel pkgconf-pkg-config xmlrpc-c xmlrpc-c-c++ xmlrpc-c-client xmlrpc-c-client++"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xmlrpc-c-1.51.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xmlrpc-c-client-1.51.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlrpc-c-c++-1.51.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlrpc-c-client++-1.51.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlrpc-c-devel-1.51.0-5.el8.aarch64.rpm \
          "

SRC_URI[xmlrpc-c.sha256sum] = "5f371b2951b8b433246c46ea6084ed2626b0bd9b2a1ea6a2e7135e024fbac5da"
SRC_URI[xmlrpc-c-c++.sha256sum] = "6bd5d3bace4d6b1214350489b883a1f50b754259014e172a80004ffe9d0898ca"
SRC_URI[xmlrpc-c-client.sha256sum] = "a4a6b4d9eb3a3373885b08591c0b7a646fb61d74a2e1c836422798a55b4a0c26"
SRC_URI[xmlrpc-c-client++.sha256sum] = "82984f48243c1284bc311e66edc89119b92ca44e559236696644349c2c719e38"
SRC_URI[xmlrpc-c-devel.sha256sum] = "dbbff93f696985db6b84d823755d09719eb40b5fc2617910224756ea847700e8"
