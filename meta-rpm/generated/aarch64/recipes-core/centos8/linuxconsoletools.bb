SUMMARY = "generated recipe based on linuxconsoletools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sdl"
RPM_SONAME_REQ_linuxconsoletools = "ld-linux-aarch64.so.1 libSDL-1.2.so.0 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_linuxconsoletools = "SDL bash gawk glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/linuxconsoletools-1.6.0-4.el8.aarch64.rpm \
          "

SRC_URI[linuxconsoletools.sha256sum] = "1f7ca88082105d8af04c111c9ddf1c15c8a153713a052aca0b37c7ee52d85a61"
