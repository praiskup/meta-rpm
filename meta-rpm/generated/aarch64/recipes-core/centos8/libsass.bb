SUMMARY = "generated recipe based on libsass srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libsass = "libsass.so.0"
RPM_SONAME_REQ_libsass = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libsass = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libsass-devel = "libsass.so.0"
RPROVIDES_libsass-devel = "libsass-dev (= 3.4.5)"
RDEPENDS_libsass-devel = "libsass pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsass-3.4.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsass-devel-3.4.5-5.el8.aarch64.rpm \
          "

SRC_URI[libsass.sha256sum] = "e757f2c76322b9930682c14af89d804d997886385ced26d4359db98154d15240"
SRC_URI[libsass-devel.sha256sum] = "21aed863cbfe59317f46db62e0e970e6e38cf66d6268b680f23ce78377585ca4"
