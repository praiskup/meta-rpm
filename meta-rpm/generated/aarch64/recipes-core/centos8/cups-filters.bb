SUMMARY = "generated recipe based on cups-filters srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs cups-libs dbus-libs e2fsprogs fontconfig freetype glib-2.0 krb5-libs lcms2 libgcc libjpeg-turbo libpng libxcrypt pkgconfig-native poppler qpdf tiff zlib"
RPM_SONAME_REQ_cups-filters = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsfilters.so.1 libcupsimage.so.2 libdbus-1.so.3 libdl.so.2 libfontconfig.so.1 libfontembed.so.1 libfreetype.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libjpeg.so.62 libk5crypto.so.3 libkrb5.so.3 liblcms2.so.2 libm.so.6 libpng16.so.16 libpoppler.so.78 libpthread.so.0 libqpdf.so.18 libstdc++.so.6 libtiff.so.5 libz.so.1"
RDEPENDS_cups-filters = "avahi-glib avahi-libs bash bc cups cups-filesystem cups-filters-libs cups-libs dbus-libs fontconfig freetype ghostscript glib2 glibc grep krb5-libs lcms2 libcom_err liberation-mono-fonts libgcc libjpeg-turbo libpng libstdc++ libtiff libxcrypt poppler poppler-utils qpdf-libs sed systemd which zlib"
RPM_SONAME_REQ_cups-filters-devel = "libcupsfilters.so.1 libfontembed.so.1"
RPROVIDES_cups-filters-devel = "cups-filters-dev (= 1.20.0)"
RDEPENDS_cups-filters-devel = "cups-filters-libs pkgconf-pkg-config"
RPM_SONAME_PROV_cups-filters-libs = "libcupsfilters.so.1 libfontembed.so.1"
RPM_SONAME_REQ_cups-filters-libs = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libdbus-1.so.3 libdl.so.2 libgssapi_krb5.so.2 libjpeg.so.62 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpng16.so.16 libpthread.so.0 libtiff.so.5 libz.so.1"
RDEPENDS_cups-filters-libs = "cups-libs dbus-libs glibc krb5-libs libcom_err libjpeg-turbo libpng libtiff libxcrypt zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-filters-1.20.0-19.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-filters-libs-1.20.0-19.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cups-filters-devel-1.20.0-19.el8.0.1.aarch64.rpm \
          "

SRC_URI[cups-filters.sha256sum] = "d0782eed902b2cefb7a5e936e8f2d277f77f4cb810389e625c058c3cd1ac6c18"
SRC_URI[cups-filters-devel.sha256sum] = "e4adaa47d116f036de9364b5edd485aa03d743253de9c4958e320e91e021b0f1"
SRC_URI[cups-filters-libs.sha256sum] = "10376d0d802518fa1dca370b981a84a0041fec630f74f28d5aa813d63bf86889"
