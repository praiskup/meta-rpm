SUMMARY = "generated recipe based on groff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_groff = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_groff = "bash coreutils gawk glibc groff-base info libgcc libstdc++"
RPM_SONAME_REQ_groff-base = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_groff-base = "bash glibc libgcc libstdc++ sed"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/groff-base-1.22.3-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/groff-1.22.3-18.el8.aarch64.rpm \
          "

SRC_URI[groff.sha256sum] = "b95072371c56dab04eb35177b65c03ef9d45e0c4aa2d8379f5b714ba7d9f0113"
SRC_URI[groff-base.sha256sum] = "eb12a527155e0e3ef20e1815fd6395e85d9439a0a895b743e6429e09691e56a2"
