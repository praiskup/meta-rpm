SUMMARY = "generated recipe based on python-augeas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-augeas = "augeas-libs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-augeas-0.5.0-12.el8.noarch.rpm \
          "

SRC_URI[python3-augeas.sha256sum] = "6d712705344b87cac29e6dc7a285e2fb6a8c4cbc677229d1f1a4d52cd970cb7f"
