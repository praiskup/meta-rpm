SUMMARY = "generated recipe based on gfs2-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libblkid libuuid ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_gfs2-utils = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libncurses.so.6 libtinfo.so.6 libuuid.so.1 libz.so.1"
RDEPENDS_gfs2-utils = "bash glibc libblkid libuuid lvm2-lockd ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gfs2-utils-3.2.0-7.el8.aarch64.rpm \
          "

SRC_URI[gfs2-utils.sha256sum] = "46277671eee12a224b73f9b0d3c12c265cc446d6915ef7b0667db09fd8b11f6a"
