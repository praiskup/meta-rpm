SUMMARY = "generated recipe based on gcc-toolset-9-systemtap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi-libs bzip2 json-c libgcc ncurses nspr nss pkgconfig-native readline rpm sqlite3 xz zlib"
RDEPENDS_gcc-toolset-9-systemtap = "bash gcc-toolset-9-runtime gcc-toolset-9-systemtap-client gcc-toolset-9-systemtap-devel"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-client = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libbz2.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6 libz.so.1"
RDEPENDS_gcc-toolset-9-systemtap-client = "avahi-libs bash bzip2-libs coreutils gcc-toolset-9-runtime gcc-toolset-9-systemtap-runtime glibc grep libgcc libstdc++ nspr nss nss-util openssh-clients perl-interpreter readline rpm-libs sed sqlite-libs unzip xz-libs zip zlib"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-devel = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libbz2.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6 libz.so.1"
RPROVIDES_gcc-toolset-9-systemtap-devel = "gcc-toolset-9-systemtap-dev (= 4.1)"
RDEPENDS_gcc-toolset-9-systemtap-devel = "avahi-libs bash bzip2-libs gcc gcc-toolset-9-runtime glibc libgcc libstdc++ make nspr nss nss-util readline rpm-libs sqlite-libs xz-libs zlib"
RDEPENDS_gcc-toolset-9-systemtap-initscript = "bash chkconfig gcc-toolset-9-runtime gcc-toolset-9-systemtap initscripts"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-runtime = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libjson-c.so.4 libm.so.6 libncurses.so.6 libnspr4.so libnss3.so libnssutil3.so libpanel.so.6 libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_gcc-toolset-9-systemtap-runtime = "bash gcc-toolset-9-runtime glibc json-c libgcc libstdc++ ncurses-libs nspr nss nss-util shadow-utils zlib"
RPROVIDES_gcc-toolset-9-systemtap-sdt-devel = "gcc-toolset-9-systemtap-sdt-dev (= 4.1)"
RDEPENDS_gcc-toolset-9-systemtap-sdt-devel = "gcc-toolset-9-runtime python3-pyparsing python36"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-server = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_gcc-toolset-9-systemtap-server = "avahi-libs bash chkconfig coreutils gcc-toolset-9-runtime gcc-toolset-9-systemtap-devel glibc initscripts libgcc libstdc++ nspr nss nss-util shadow-utils unzip zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-4.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-client-4.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-devel-4.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-initscript-4.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-runtime-4.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-sdt-devel-4.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-systemtap-server-4.1-4.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-systemtap.sha256sum] = "0418859b1412d786ad39c0cd4c2053140a5341c9bb93db781e629c4cd1491977"
SRC_URI[gcc-toolset-9-systemtap-client.sha256sum] = "dae5be473c6b787601374dba3aa7cac58b21a1d23da5e971750207e240cf6f48"
SRC_URI[gcc-toolset-9-systemtap-devel.sha256sum] = "49e3b8cbbfd21b50a8a9a4d96e638e512c578fffa8d4d50806991a45d2fc4543"
SRC_URI[gcc-toolset-9-systemtap-initscript.sha256sum] = "30ab7ddcecd2f114139d1978a1e47f3e440f62882587d4fea8412e70098ca0d2"
SRC_URI[gcc-toolset-9-systemtap-runtime.sha256sum] = "cd6d906225f0ad0d54381f73f59f851fa5cc9747afd5251fbc604d5bcf6883d0"
SRC_URI[gcc-toolset-9-systemtap-sdt-devel.sha256sum] = "a22a34dd1649593e7ab45be8428d272efec1c477cd6cc986e65276f5e14f3bf6"
SRC_URI[gcc-toolset-9-systemtap-server.sha256sum] = "479375d73963be6fb163a171b7e613316de7cb0fe53a5d04dd4b3bac18c6867d"
