SUMMARY = "generated recipe based on gcc-toolset-9-make srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-make = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_gcc-toolset-9-make = "gcc-toolset-9-runtime glibc"
RPROVIDES_gcc-toolset-9-make-devel = "gcc-toolset-9-make-dev (= 4.2.1)"
RDEPENDS_gcc-toolset-9-make-devel = "gcc-toolset-9-runtime"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-make-4.2.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-make-devel-4.2.1-2.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-make.sha256sum] = "338d10ee9cc71413cf87317413c9aadf7902df13968bc76a2b757611511140b7"
SRC_URI[gcc-toolset-9-make-devel.sha256sum] = "8e7239b052ff41e461b846c1ba40aab8d931f96917eb917bd193d4b992856f81"
