SUMMARY = "generated recipe based on texinfo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_texinfo = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-Getopt-Long perl-MIME-Base64 perl-PathTools perl-Pod-Simple perl-Storable perl-Text-Unidecode perl-Unicode-EastAsianWidth perl-Unicode-Normalize perl-interpreter perl-libintl-perl perl-libs"
RDEPENDS_texinfo-tex = "bash texinfo texlive-collection-basic texlive-epsf texlive-tetex"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/texinfo-6.5-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/texinfo-tex-6.5-6.el8.aarch64.rpm \
          "

SRC_URI[texinfo.sha256sum] = "64e622773b5a51bc1a2ce927bd4914408bc19556a9ce4ff48d223e0fb4f9ef51"
SRC_URI[texinfo-tex.sha256sum] = "892b5faba137708d91c5c552ce5aa3eeebdaaadd09ae33a1c8df89cf766da781"
