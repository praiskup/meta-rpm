SUMMARY = "generated recipe based on perl-XML-Catalog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-Catalog = "perl-Text-ParseWords perl-URI perl-XML-Parser perl-interpreter perl-libs perl-libwww-perl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-Catalog-1.03-11.el8.noarch.rpm \
          "

SRC_URI[perl-XML-Catalog.sha256sum] = "42f957bab55cc0da2ff38e30a07f48ba8e3d676c5e0d9d3b3de7e7ca52cbf441"
