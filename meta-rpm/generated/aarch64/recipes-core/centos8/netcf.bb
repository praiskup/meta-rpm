SUMMARY = "generated recipe based on netcf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "augeas libgcrypt libgpg-error libnl libxml2 libxslt pkgconfig-native readline"
RPM_SONAME_REQ_netcf = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libnetcf.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libreadline.so.7 libxml2.so.2 libxslt.so.1"
RDEPENDS_netcf = "augeas-libs glibc libgcrypt libgpg-error libnl3 libxml2 libxslt netcf-libs readline systemd"
RPM_SONAME_REQ_netcf-devel = "libnetcf.so.1"
RPROVIDES_netcf-devel = "netcf-dev (= 0.2.8)"
RDEPENDS_netcf-devel = "netcf-libs pkgconf-pkg-config"
RPM_SONAME_PROV_netcf-libs = "libnetcf.so.1"
RPM_SONAME_REQ_netcf-libs = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libxml2.so.2 libxslt.so.1"
RDEPENDS_netcf-libs = "augeas-libs bash glibc libgcrypt libgpg-error libnl3 libxml2 libxslt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/netcf-0.2.8-12.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/netcf-devel-0.2.8-12.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/netcf-libs-0.2.8-12.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[netcf.sha256sum] = "0599b70dc210c3ae8fb06af62baf0601d7782dbf7a8cfc7dfff5559ff4c373f8"
SRC_URI[netcf-devel.sha256sum] = "eb6bf1df6a27785cbf0363362ea34e2a8408c9eea17992a92024872e43d447f3"
SRC_URI[netcf-libs.sha256sum] = "cafb987f37ec4a385b7ea5db4abb9478c73158a01aa3ea897d369267eddf185d"
