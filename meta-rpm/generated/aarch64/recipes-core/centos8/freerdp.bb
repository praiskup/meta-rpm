SUMMARY = "generated recipe based on freerdp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib cups-libs glib-2.0 gsm gstreamer1.0 gstreamer1.0-plugins-base libjpeg-turbo libx11 libxcursor libxext libxfixes libxi libxinerama libxkbcommon libxkbfile libxrandr libxrender libxv openssl pkgconfig-native pulseaudio systemd-libs wayland"
RPM_SONAME_REQ_freerdp = "ld-linux-aarch64.so.1 libX11.so.6 libXcursor.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libXv.so.1 libc.so.6 libfreerdp-client2.so.2 libfreerdp2.so.2 libm.so.6 librt.so.1 libuwac0.so.0 libwinpr-tools2.so.2 libwinpr2.so.2"
RDEPENDS_freerdp = "freerdp-libs glibc libX11 libXcursor libXext libXfixes libXi libXinerama libXrandr libXrender libXv libwinpr"
RPM_SONAME_REQ_freerdp-devel = "libfreerdp-client2.so.2 libfreerdp2.so.2 libuwac0.so.0"
RPROVIDES_freerdp-devel = "freerdp-dev (= 2.0.0)"
RDEPENDS_freerdp-devel = "cmake cmake-filesystem freerdp-libs libwinpr-devel libxkbcommon-devel openssl-devel pkgconf-pkg-config wayland-devel"
RPM_SONAME_PROV_freerdp-libs = "libaudin-client-alsa.so libaudin-client-oss.so libaudin-client-pulse.so libaudin-client.so libdisp-client.so libdrive-client.so libecho-client.so libfreerdp-client2.so.2 libfreerdp2.so.2 libgeometry-client.so libparallel-client.so libprinter-client.so librdpei-client.so librdpgfx-client.so librdpsnd-client-alsa.so librdpsnd-client-fake.so librdpsnd-client-oss.so librdpsnd-client-pulse.so libserial-client.so libtsmf-client-alsa-audio.so libtsmf-client-gstreamer-decoder.so libtsmf-client-oss-audio.so libtsmf-client-pulse-audio.so libtsmf-client.so libuwac0.so.0 libvideo-client.so"
RPM_SONAME_REQ_freerdp-libs = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libasound.so.2 libc.so.6 libcrypto.so.1.1 libcups.so.2 libfreerdp2.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libgsm.so.1 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libjpeg.so.62 libpthread.so.0 libpulse.so.0 librt.so.1 libssl.so.1.1 libwayland-client.so.0 libwinpr2.so.2 libxkbcommon.so.0 libxkbfile.so.1"
RDEPENDS_freerdp-libs = "alsa-lib cups-libs glib2 glibc gsm gstreamer1 gstreamer1-plugins-base libX11 libXext libjpeg-turbo libwayland-client libwinpr libxkbcommon libxkbfile openssl-libs pulseaudio-libs"
RPM_SONAME_PROV_libwinpr = "libwinpr-tools2.so.2 libwinpr2.so.2"
RPM_SONAME_REQ_libwinpr = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libsystemd.so.0 libwinpr2.so.2"
RDEPENDS_libwinpr = "glibc openssl-libs systemd-libs"
RPM_SONAME_REQ_libwinpr-devel = "libwinpr-tools2.so.2 libwinpr2.so.2"
RPROVIDES_libwinpr-devel = "libwinpr-dev (= 2.0.0)"
RDEPENDS_libwinpr-devel = "cmake cmake-filesystem libwinpr openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/freerdp-2.0.0-46.rc4.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/freerdp-libs-2.0.0-46.rc4.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwinpr-2.0.0-46.rc4.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwinpr-devel-2.0.0-46.rc4.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/freerdp-devel-2.0.0-46.rc4.el8_2.2.aarch64.rpm \
          "

SRC_URI[freerdp.sha256sum] = "d5aa9c5509ad14a4ad9b215e8d945adf5c8070dea770ec9065a005795f8b2caa"
SRC_URI[freerdp-devel.sha256sum] = "c88a648922c066c339b02a5d03677576fc0094a0d165b3a145b329ff7c92a29d"
SRC_URI[freerdp-libs.sha256sum] = "98e65344ba6f97ff529191f21580f1363d1c8f2fe859e9e2f81a85624dc78b24"
SRC_URI[libwinpr.sha256sum] = "844993a48dfb25cf14d66b4b076b6ca82a18050b4ab5879e81f73e1c9df5d9b7"
SRC_URI[libwinpr-devel.sha256sum] = "2ee64d45e91455a49985c4bef0b58adcbc41dc1dc819fff0c5b6b54967db50a6"
