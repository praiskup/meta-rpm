SUMMARY = "generated recipe based on perl-Module-Runtime srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Runtime = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Module-Runtime-0.016-2.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Runtime.sha256sum] = "0d52c081cf5a2950f3e3118cb2fded8b4005988833364b9bbeac8c7691a8b0d5"
