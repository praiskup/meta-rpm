SUMMARY = "generated recipe based on dmidecode srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dmidecode = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_dmidecode = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dmidecode-3.2-5.el8.aarch64.rpm \
          "

SRC_URI[dmidecode.sha256sum] = "361c4d180f7fb945c258c0cc40e8cc9309677ad68747b674a27d25fb85f267a6"
