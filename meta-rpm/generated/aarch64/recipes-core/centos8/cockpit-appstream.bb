SUMMARY = "generated recipe based on cockpit-appstream srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib pcp pkgconfig-native systemd-libs"
RDEPENDS_cockpit-dashboard = "cockpit-bridge"
RDEPENDS_cockpit-machines = "cockpit-bridge cockpit-system libvirt-client libvirt-daemon-kvm libvirt-dbus"
RDEPENDS_cockpit-packagekit = "PackageKit cockpit-bridge"
RPM_SONAME_REQ_cockpit-pcp = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpcp.so.3 libpthread.so.0 libsystemd.so.0 libutil.so.1"
RDEPENDS_cockpit-pcp = "bash cockpit-bridge glib2 glibc json-glib pcp pcp-libs systemd-libs"
RDEPENDS_cockpit-storaged = "cockpit-system platform-python python3-dbus udisks2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cockpit-dashboard-211.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cockpit-machines-211.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cockpit-packagekit-211.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cockpit-pcp-211.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cockpit-storaged-211.3-1.el8.noarch.rpm \
          "

SRC_URI[cockpit-dashboard.sha256sum] = "2ac4287eb055f077d74da9fda25e01d503b6a4f99316e8e0d239e72d1d227445"
SRC_URI[cockpit-machines.sha256sum] = "1b5572899062cdddf17fe7d365e30f1f040cf701ce09f3dd32e89da09315d2d7"
SRC_URI[cockpit-packagekit.sha256sum] = "e06e94340451d85a6cc4759761273fdf25bfbf3ff2ad1acf8390ff90b5b1850a"
SRC_URI[cockpit-pcp.sha256sum] = "bee425ed244fdaf73135119d1f32066b260a6763f4033249dc0ade1ea1619011"
SRC_URI[cockpit-storaged.sha256sum] = "171fa25674e6ba69ea9063334e87b00b51749c6ca58129a8b52bb2b6b084c5e2"
