SUMMARY = "generated recipe based on plexus-archiver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-archiver = "apache-commons-compress java-1.8.0-openjdk-headless javapackages-filesystem plexus-io plexus-utils xz-java"
RDEPENDS_plexus-archiver-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-archiver-3.6.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-archiver-javadoc-3.6.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-archiver.sha256sum] = "bf7779df1df0fc87fbfc3f4e2a2eb82022ae03ec37f04e0c9898a708ee2344bf"
SRC_URI[plexus-archiver-javadoc.sha256sum] = "59d212f8426028de5eb387e90262f753a4a19921986003f043e76bfc26831395"
