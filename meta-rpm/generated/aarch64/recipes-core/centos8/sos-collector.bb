SUMMARY = "generated recipe based on sos-collector srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sos-collector = "platform-python python3-pexpect python3-six sos"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sos-collector-1.8-2.el8.noarch.rpm \
          "

SRC_URI[sos-collector.sha256sum] = "30a0ad88b5af550f586949c6adff8dce8633ec60fe92519506c5438860f9fac3"
