SUMMARY = "generated recipe based on libwebp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libwebp = "libwebp.so.7 libwebpdecoder.so.3 libwebpdemux.so.2 libwebpmux.so.3"
RPM_SONAME_REQ_libwebp = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0 libwebp.so.7"
RDEPENDS_libwebp = "glibc"
RPM_SONAME_REQ_libwebp-devel = "libwebp.so.7 libwebpdecoder.so.3 libwebpdemux.so.2 libwebpmux.so.3"
RPROVIDES_libwebp-devel = "libwebp-dev (= 1.0.0)"
RDEPENDS_libwebp-devel = "libwebp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwebp-1.0.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwebp-devel-1.0.0-1.el8.aarch64.rpm \
          "

SRC_URI[libwebp.sha256sum] = "11e6a2d18b50968a18f01f4d722cfab9a9c6ba4734de9f43e2041da3b02fccde"
SRC_URI[libwebp-devel.sha256sum] = "01368b5cf4ab99d0017f1d12d1964cb493ef0d521d08f34dcc244fc96b686422"
