SUMMARY = "generated recipe based on perl-HTML-Parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-HTML-Parser = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-HTML-Parser = "glibc perl-Carp perl-Exporter perl-HTML-Tagset perl-HTTP-Message perl-IO perl-URI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-HTML-Parser-3.72-14.el8.aarch64.rpm \
          "

SRC_URI[perl-HTML-Parser.sha256sum] = "2e0fd132e4da64c8a5d1de19d50ba422ee792ced3bd9ccb50c6732d5626a1b10"
