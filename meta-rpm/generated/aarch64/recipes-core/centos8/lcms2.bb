SUMMARY = "generated recipe based on lcms2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lcms2 = "liblcms2.so.2"
RPM_SONAME_REQ_lcms2 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_lcms2 = "glibc"
RPM_SONAME_REQ_lcms2-devel = "liblcms2.so.2"
RPROVIDES_lcms2-devel = "lcms2-dev (= 2.9)"
RDEPENDS_lcms2-devel = "lcms2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lcms2-2.9-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lcms2-devel-2.9-2.el8.aarch64.rpm \
          "

SRC_URI[lcms2.sha256sum] = "34ea16047923efc4629285dccf4cfb1fe3153f3dd324c7e746bede88550dd632"
SRC_URI[lcms2-devel.sha256sum] = "fa1e8777524129537eaa4dd06bb2a1dc30a5e23ed7a7bb4a6570bd74e48880d9"
