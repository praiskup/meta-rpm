SUMMARY = "generated recipe based on python-requests-oauthlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-requests-oauthlib = "platform-python python3-oauthlib python3-requests"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-requests-oauthlib-1.0.0-1.el8.noarch.rpm \
          "

SRC_URI[python3-requests-oauthlib.sha256sum] = "a774ed251a85f919890d6623e0e935b89b4883d76687c75c94e9bbd1bec1247c"
