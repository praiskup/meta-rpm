SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gd libpng pkgconfig-native zlib"
RPM_SONAME_REQ_glibc-utils = "ld-linux-aarch64.so.1 libc.so.6 libgd.so.3 libm.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_glibc-utils = "bash gd glibc libpng perl-interpreter zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/glibc-utils-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-utils.sha256sum] = "eb2e4c2cda910a7ddd5e6d946e658c517345721ea89c2bad8acb140de4d12501"
