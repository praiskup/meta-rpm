SUMMARY = "generated recipe based on perl-libxml-perl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-libxml-perl = "perl-Carp perl-IO perl-XML-Parser perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-libxml-perl-0.08-33.el8.noarch.rpm \
          "

SRC_URI[perl-libxml-perl.sha256sum] = "e03a200b24b75aeb421bc5a5a34270663606693303980e3f7cf91ded3b0d43aa"
