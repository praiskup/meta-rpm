SUMMARY = "generated recipe based on ypbind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnsl2 libtirpc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_ypbind = "ld-linux-aarch64.so.1 libc.so.6 libnsl.so.2 libpthread.so.0 libsystemd.so.0 libtirpc.so.3"
RDEPENDS_ypbind = "bash glibc libnsl2 libtirpc nss_nis rpcbind systemd systemd-libs yp-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ypbind-2.5-2.el8.aarch64.rpm \
          "

SRC_URI[ypbind.sha256sum] = "81a0eca4bc92f71b42851be104199afa3dff555f7abb98a343e6509cb859375e"
