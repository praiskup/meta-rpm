SUMMARY = "generated recipe based on libglvnd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libglvnd = "libGLdispatch.so.0"
RPM_SONAME_REQ_libglvnd = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libglvnd = "glibc"
RPROVIDES_libglvnd-core-devel = "libglvnd-core-dev (= 1.2.0)"
RDEPENDS_libglvnd-core-devel = "pkgconf-pkg-config"
RPM_SONAME_REQ_libglvnd-devel = "libEGL.so.1 libGL.so.1 libGLESv1_CM.so.1 libGLESv2.so.2 libGLX.so.0 libGLdispatch.so.0 libOpenGL.so.0"
RPROVIDES_libglvnd-devel = "libglvnd-dev (= 1.2.0)"
RDEPENDS_libglvnd-devel = "libX11-devel libglvnd libglvnd-core-devel libglvnd-egl libglvnd-gles libglvnd-glx libglvnd-opengl pkgconf-pkg-config"
RPM_SONAME_PROV_libglvnd-egl = "libEGL.so.1"
RPM_SONAME_REQ_libglvnd-egl = "ld-linux-aarch64.so.1 libGLdispatch.so.0 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_libglvnd-egl = "glibc libglvnd mesa-libEGL"
RPM_SONAME_PROV_libglvnd-gles = "libGLESv1_CM.so.1 libGLESv2.so.2"
RPM_SONAME_REQ_libglvnd-gles = "ld-linux-aarch64.so.1 libGLdispatch.so.0 libc.so.6 libdl.so.2"
RDEPENDS_libglvnd-gles = "glibc libglvnd mesa-libEGL"
RPM_SONAME_PROV_libglvnd-glx = "libGL.so.1 libGLX.so.0"
RPM_SONAME_REQ_libglvnd-glx = "ld-linux-aarch64.so.1 libGLX.so.0 libGLdispatch.so.0 libX11.so.6 libXext.so.6 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_libglvnd-glx = "glibc libX11 libXext libglvnd mesa-libGL"
RPM_SONAME_PROV_libglvnd-opengl = "libOpenGL.so.0"
RPM_SONAME_REQ_libglvnd-opengl = "ld-linux-aarch64.so.1 libGLdispatch.so.0 libc.so.6 libdl.so.2"
RDEPENDS_libglvnd-opengl = "glibc libglvnd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-1.2.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-core-devel-1.2.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-devel-1.2.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-egl-1.2.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-gles-1.2.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-glx-1.2.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libglvnd-opengl-1.2.0-6.el8.aarch64.rpm \
          "

SRC_URI[libglvnd.sha256sum] = "e18ac04f114ff185be3785c5abfe0ee03d11e037bcf7e4df6b63664f0ab3c456"
SRC_URI[libglvnd-core-devel.sha256sum] = "60542c5b82fb7aacb87ee6db3032c2b56ffdbc799ef5bfd7e8762ed9d170f1f0"
SRC_URI[libglvnd-devel.sha256sum] = "d27e614497a20405f9b9cc14f1e36916d2455d217a6a12efb3dbca9403d2fc40"
SRC_URI[libglvnd-egl.sha256sum] = "4bdb1f9cc935e0f6ce94c5b96969698886584e41483721b4ce7fa49f10522a88"
SRC_URI[libglvnd-gles.sha256sum] = "1f443c78abf6cb9ebb68a3ac88b4deb206f26e0abe55af46a32881d5edefd761"
SRC_URI[libglvnd-glx.sha256sum] = "579a376211617521e32e2c12bc115cfeb91315bc6bb1543d42842eb2439e33b4"
SRC_URI[libglvnd-opengl.sha256sum] = "06d95325a9d465a671345968d774893df3a2a7300c3b9f45f54220bdcf4a22d7"
