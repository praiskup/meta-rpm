SUMMARY = "generated recipe based on clucene srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_clucene-contribs-lib = "libclucene-contribs-lib.so.1"
RPM_SONAME_REQ_clucene-contribs-lib = "ld-linux-aarch64.so.1 libc.so.6 libclucene-core.so.1 libclucene-shared.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_clucene-contribs-lib = "clucene-core glibc libgcc libstdc++ zlib"
RPM_SONAME_PROV_clucene-core = "libclucene-core.so.1 libclucene-shared.so.1"
RPM_SONAME_REQ_clucene-core = "ld-linux-aarch64.so.1 libc.so.6 libclucene-shared.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_clucene-core = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_clucene-core-devel = "libclucene-contribs-lib.so.1 libclucene-core.so.1 libclucene-shared.so.1"
RPROVIDES_clucene-core-devel = "clucene-core-dev (= 2.3.3.4)"
RDEPENDS_clucene-core-devel = "clucene-contribs-lib clucene-core pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clucene-core-2.3.3.4-31.20130812.e8e3d20git.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/clucene-contribs-lib-2.3.3.4-31.20130812.e8e3d20git.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/clucene-core-devel-2.3.3.4-31.20130812.e8e3d20git.el8.aarch64.rpm \
          "

SRC_URI[clucene-contribs-lib.sha256sum] = "8bcc7900dcd440597a74412e5a446457bcff8955772fa8ae0734dbb55482ae11"
SRC_URI[clucene-core.sha256sum] = "a3eeb4b3463bccebea9a31c651d0864d7e2048b3375c8ad9af6872743a4d30a3"
SRC_URI[clucene-core-devel.sha256sum] = "6a540f2f3a31f7abf277f760624a868f460ecc6ef2bfcbd2baff910fd7c57b05"
