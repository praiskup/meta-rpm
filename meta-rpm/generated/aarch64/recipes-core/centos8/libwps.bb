SUMMARY = "generated recipe based on libwps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libwps = "libwps-0.4.so.4"
RPM_SONAME_REQ_libwps = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libwps = "glibc libgcc librevenge libstdc++"
RPM_SONAME_REQ_libwps-devel = "libwps-0.4.so.4"
RPROVIDES_libwps-devel = "libwps-dev (= 0.4.9)"
RDEPENDS_libwps-devel = "librevenge-devel libwps pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwps-0.4.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwps-devel-0.4.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwps-doc-0.4.9-1.el8.noarch.rpm \
          "

SRC_URI[libwps.sha256sum] = "7c9688d11e9e6f1cf19e3c89b73129802b1cf46a33e1edc779c577d68416b806"
SRC_URI[libwps-devel.sha256sum] = "4c96ad6dd45db10b8dfaf5df84c159d59d1c8bccafd92081fa7ec1901fbfb148"
SRC_URI[libwps-doc.sha256sum] = "b48b388c2af589a229c8ef1581d1a78709c38a5fc89d53ab359e7f72d0ce70b3"
