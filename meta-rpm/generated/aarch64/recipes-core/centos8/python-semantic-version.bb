SUMMARY = "generated recipe based on python-semantic_version srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-semantic_version = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-semantic_version-2.6.0-5.el8.noarch.rpm \
          "

SRC_URI[python3-semantic_version.sha256sum] = "a3bd7684ba2b6952c8d7ce3fe9055f9a29ed47ebee5acb0207e8c183d754b659"
