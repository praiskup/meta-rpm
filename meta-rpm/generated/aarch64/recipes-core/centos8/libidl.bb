SUMMARY = "generated recipe based on libIDL srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libIDL = "libIDL-2.so.0"
RPM_SONAME_REQ_libIDL = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0"
RDEPENDS_libIDL = "glib2 glibc"
RPM_SONAME_REQ_libIDL-devel = "libIDL-2.so.0"
RPROVIDES_libIDL-devel = "libIDL-dev (= 0.8.14)"
RDEPENDS_libIDL-devel = "bash glib2-devel info libIDL pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libIDL-0.8.14-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libIDL-devel-0.8.14-15.el8.aarch64.rpm \
          "

SRC_URI[libIDL.sha256sum] = "125010e5ea5c9f239ce513319eadc5f4ddfd1337788934fc3c5066042f871963"
SRC_URI[libIDL-devel.sha256sum] = "c9f2f917b8fdc5f51988c4da53396aff1f769fc1b90c2dd329b49d974aa45e15"
