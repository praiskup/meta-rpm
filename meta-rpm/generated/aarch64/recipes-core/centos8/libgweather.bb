SUMMARY = "generated recipe based on libgweather srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf geocode-glib glib-2.0 gtk+3 libsoup-2.4 libxml2 pkgconfig-native"
RPM_SONAME_PROV_libgweather = "libgweather-3.so.15"
RPM_SONAME_REQ_libgweather = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgeocode-glib.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_libgweather = "gdk-pixbuf2 geocode-glib glib2 glibc gtk3 libsoup libxml2"
RPM_SONAME_REQ_libgweather-devel = "libgweather-3.so.15"
RPROVIDES_libgweather-devel = "libgweather-dev (= 3.28.2)"
RDEPENDS_libgweather-devel = "geocode-glib-devel glib2-devel gtk3-devel libgweather libsoup-devel libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgweather-3.28.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgweather-devel-3.28.2-2.el8.aarch64.rpm \
          "

SRC_URI[libgweather.sha256sum] = "b094a39740b8d8b827df615c06cc278784772746fb1df0de6244931b4dd95b7e"
SRC_URI[libgweather-devel.sha256sum] = "b7d506384c3216edbce8ee7aacd0059667494e853184aac06bec4be1a4ce5a18"
