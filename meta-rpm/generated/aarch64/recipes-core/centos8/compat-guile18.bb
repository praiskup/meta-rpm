SUMMARY = "generated recipe based on compat-guile18 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libtool libxcrypt ncurses pkgconfig-native readline"
RPM_SONAME_PROV_compat-guile18 = "libguile-srfi-srfi-1-v-3.so.3 libguile-srfi-srfi-13-14-v-3.so.3 libguile-srfi-srfi-4-v-3.so.3 libguile-srfi-srfi-60-v-2.so.2 libguile.so.17 libguilereadline-v-17.so.17"
RPM_SONAME_REQ_compat-guile18 = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libgmp.so.10 libguile-srfi-srfi-1-v-3.so.3 libguile-srfi-srfi-13-14-v-3.so.3 libguile-srfi-srfi-4-v-3.so.3 libguile-srfi-srfi-60-v-2.so.2 libguile.so.17 libguilereadline-v-17.so.17 libltdl.so.7 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libtinfo.so.6"
RDEPENDS_compat-guile18 = "bash glibc gmp libtool-ltdl libxcrypt ncurses-libs readline"
RPM_SONAME_REQ_compat-guile18-devel = "libguile.so.17"
RPROVIDES_compat-guile18-devel = "compat-guile18-dev (= 1.8.8)"
RDEPENDS_compat-guile18-devel = "bash compat-guile18 gmp-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/compat-guile18-1.8.8-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/compat-guile18-devel-1.8.8-22.el8.aarch64.rpm \
          "

SRC_URI[compat-guile18.sha256sum] = "4d3282701e8ba4a3dd5b1b43b96d41f8a7b843f8cd1ca04200fb3a9458065c5a"
SRC_URI[compat-guile18-devel.sha256sum] = "923f2c95acecc29d9214219ae80153bcde16fc7a89a671c249d80dd8c6fc43a2"
