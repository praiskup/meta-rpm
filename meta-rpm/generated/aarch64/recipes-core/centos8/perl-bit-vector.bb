SUMMARY = "generated recipe based on perl-Bit-Vector srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Bit-Vector = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Bit-Vector = "glibc perl-Carp-Clan perl-Exporter perl-Storable perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Bit-Vector-7.4-11.el8.aarch64.rpm \
          "

SRC_URI[perl-Bit-Vector.sha256sum] = "c7118370611a650ab84db62c54455d4c9a90694488a776267818e8b3e16ab080"
