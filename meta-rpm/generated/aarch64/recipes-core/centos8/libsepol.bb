SUMMARY = "generated recipe based on libsepol srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsepol = "libsepol.so.1"
RPM_SONAME_REQ_libsepol = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsepol = "bash glibc"
RPM_SONAME_REQ_libsepol-devel = "libsepol.so.1"
RPROVIDES_libsepol-devel = "libsepol-dev (= 2.9)"
RDEPENDS_libsepol-devel = "libsepol pkgconf-pkg-config"
RDEPENDS_libsepol-static = "libsepol-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsepol-2.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsepol-devel-2.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsepol-static-2.9-1.el8.aarch64.rpm \
          "

SRC_URI[libsepol.sha256sum] = "236c00d37f3127a7d399a11ff45f019554b8f7bfaefd12965705f9ca0d4f3908"
SRC_URI[libsepol-devel.sha256sum] = "d6e8908674c8cbc64987999d2d56c4e79728a8662b8a4d352eb778604245c27c"
SRC_URI[libsepol-static.sha256sum] = "fefcbbda4c7700378c42c66a1a9cedd2c55aa50f1d50c21047e6e34134d32f73"
