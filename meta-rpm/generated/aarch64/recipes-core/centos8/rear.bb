SUMMARY = "generated recipe based on rear srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rear = "attr bash bc binutils crontabs ethtool gawk gzip iproute iputils openssl parted tar util-linux xorriso"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rear-2.4-12.el8.aarch64.rpm \
          "

SRC_URI[rear.sha256sum] = "06ddd4c932372a60d3118367cb77861b67bfb5467f1833a8c8ddf50fc70ef0a2"
