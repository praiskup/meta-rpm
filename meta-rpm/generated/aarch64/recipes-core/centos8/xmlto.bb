SUMMARY = "generated recipe based on xmlto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xmlto = "libc.so.6"
RDEPENDS_xmlto = "bash docbook-dtds docbook-style-xsl flex glibc libxslt util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xmlto-0.0.28-7.el8.aarch64.rpm \
          "

SRC_URI[xmlto.sha256sum] = "aefdc14f99d85e1e7c3087efe6972cc444432d7ef91bfb224d925fe739d7e91d"
