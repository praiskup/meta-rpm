SUMMARY = "generated recipe based on xsane srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf gimp glib-2.0 gtk2 lcms2 libjpeg-turbo libpng pango pkgconfig-native sane-backends tiff zlib"
RPM_SONAME_REQ_xsane = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 libsane.so.1 libtiff.so.5 libz.so.1"
RDEPENDS_xsane = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 hicolor-icon-theme lcms2 libjpeg-turbo libpng libtiff pango sane-backends-libs xsane-common zlib"
RPM_SONAME_REQ_xsane-gimp = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpui-2.0.so.0 libgimpwidgets-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 libsane.so.1 libtiff.so.5 libz.so.1"
RDEPENDS_xsane-gimp = "atk bash cairo fontconfig freetype gdk-pixbuf2 gimp gimp-libs glib2 glibc gtk2 lcms2 libjpeg-turbo libpng libtiff pango sane-backends-libs xsane-common zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xsane-0.999-30.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xsane-common-0.999-30.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xsane-gimp-0.999-30.el8.aarch64.rpm \
          "

SRC_URI[xsane.sha256sum] = "93917b532c4100c44600bf72c848071b6fe3c9e155e5696d597edcabf955d821"
SRC_URI[xsane-common.sha256sum] = "7c986caf9b911adeafa17f5bd6b35551d6ce160ede35cebf36036fedfbc34dd2"
SRC_URI[xsane-gimp.sha256sum] = "6326fe6a7af050f195fc77f89b43e7d3d48385c2bd7bbad56bcc0cf137dca76b"
