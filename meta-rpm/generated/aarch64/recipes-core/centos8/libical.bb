SUMMARY = "generated recipe based on libical srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc pkgconfig-native"
RPM_SONAME_PROV_libical = "libical.so.3 libical_cxx.so.3 libicalss.so.3 libicalss_cxx.so.3 libicalvcal.so.3"
RPM_SONAME_REQ_libical = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libical.so.3 libical_cxx.so.3 libicalss.so.3 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libical = "glibc libgcc libicu libstdc++ tzdata"
RPM_SONAME_REQ_libical-devel = "libical.so.3 libical_cxx.so.3 libicalss.so.3 libicalss_cxx.so.3 libicalvcal.so.3"
RPROVIDES_libical-devel = "libical-dev (= 3.0.3)"
RDEPENDS_libical-devel = "cmake-filesystem libical libicu-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libical-devel-3.0.3-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libical-3.0.3-3.el8.aarch64.rpm \
          "

SRC_URI[libical.sha256sum] = "4d8df0420268f900cdb680de84237ac1838d50de4e8dc627d9e86882604f87c0"
SRC_URI[libical-devel.sha256sum] = "335b0091600b12ec0604b10e6f1d42d80e3152afa6de01cedf1479f76a9918cb"
