SUMMARY = "generated recipe based on perl-Class-Factory-Util srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Factory-Util = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Class-Factory-Util-1.7-27.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Factory-Util.sha256sum] = "38d3a3c2f2346fc92412b31d5ae2bbedf64f0a39a02df6d7e40f4dd3632fb01c"
