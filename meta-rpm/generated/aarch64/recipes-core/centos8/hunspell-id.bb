SUMMARY = "generated recipe based on hunspell-id srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-id = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-id-0.20040812-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-id.sha256sum] = "d213c2077c54ccc0b172541738d593f6827a62a97abe555b71ac280183846c2b"
