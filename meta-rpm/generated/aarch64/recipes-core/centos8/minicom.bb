SUMMARY = "generated recipe based on minicom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lockdev ncurses pkgconfig-native"
RPM_SONAME_REQ_minicom = "ld-linux-aarch64.so.1 libc.so.6 liblockdev.so.1 libtinfo.so.6"
RDEPENDS_minicom = "bash glibc lockdev lrzsz ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/minicom-2.7.1-9.el8.aarch64.rpm \
          "

SRC_URI[minicom.sha256sum] = "8c50b373448c3b1211214abb5d961b2e0a4fb3c512da11e044623d2483764bcc"
