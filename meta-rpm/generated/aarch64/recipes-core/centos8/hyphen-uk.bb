SUMMARY = "generated recipe based on hyphen-uk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-uk = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-uk-0.20030903-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-uk.sha256sum] = "9f42ad00bff56dd9411c0ac4153fb42c22001143dd68f7efb890cdf9c11409d4"
