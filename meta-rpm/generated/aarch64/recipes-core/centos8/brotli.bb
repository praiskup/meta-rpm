SUMMARY = "generated recipe based on brotli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_brotli = "libbrotlicommon.so.1 libbrotlidec.so.1 libbrotlienc.so.1"
RPM_SONAME_REQ_brotli = "ld-linux-aarch64.so.1 libbrotlicommon.so.1 libc.so.6 libm.so.6"
RDEPENDS_brotli = "glibc"
RPM_SONAME_REQ_brotli-devel = "libbrotlicommon.so.1 libbrotlidec.so.1 libbrotlienc.so.1"
RPROVIDES_brotli-devel = "brotli-dev (= 1.0.6)"
RDEPENDS_brotli-devel = "brotli pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brotli-devel-1.0.6-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/brotli-1.0.6-1.el8.aarch64.rpm \
          "

SRC_URI[brotli.sha256sum] = "53d4648b9fcbc0b226fa9eb09963c0e0468438a9ed3e67cceced09650ddd347b"
SRC_URI[brotli-devel.sha256sum] = "6ed3632178395365360df90af5ef24817221765a2d7fc77a62e761e923528504"
