SUMMARY = "generated recipe based on liblockfile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liblockfile = "liblockfile.so.1"
RPM_SONAME_REQ_liblockfile = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_liblockfile = "glibc"
RPM_SONAME_REQ_liblockfile-devel = "liblockfile.so.1"
RPROVIDES_liblockfile-devel = "liblockfile-dev (= 1.14)"
RDEPENDS_liblockfile-devel = "liblockfile"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liblockfile-1.14-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liblockfile-devel-1.14-1.el8.aarch64.rpm \
          "

SRC_URI[liblockfile.sha256sum] = "302798a75961d583b545cd1f34c4dd2c107e0dea9f14cb7d477453de0e49e196"
SRC_URI[liblockfile-devel.sha256sum] = "a622f334f24944a48b480168b22460f61808bd865772d05c6d7ef86769d68b27"
