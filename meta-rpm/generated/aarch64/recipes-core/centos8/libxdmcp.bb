SUMMARY = "generated recipe based on libXdmcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xdmcp"
DEPENDS = "pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXdmcp = "libXdmcp.so.6"
RPM_SONAME_REQ_libXdmcp = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libXdmcp = "glibc"
RPM_SONAME_REQ_libXdmcp-devel = "libXdmcp.so.6"
RPROVIDES_libXdmcp-devel = "libXdmcp-dev (= 1.1.2)"
RDEPENDS_libXdmcp-devel = "libXdmcp pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXdmcp-1.1.2-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libXdmcp-devel-1.1.2-11.el8.aarch64.rpm \
          "

SRC_URI[libXdmcp.sha256sum] = "a60d3afb057a29c61a6e7383af32c518a0b3894d386db6fbbbd4c090e6c05441"
SRC_URI[libXdmcp-devel.sha256sum] = "5545873f9c674e395ed3646feac84d40db675794603dbb34943b442abadab4c8"
