SUMMARY = "generated recipe based on libnsl2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtirpc pkgconfig-native"
RPM_SONAME_PROV_libnsl2 = "libnsl.so.2"
RPM_SONAME_REQ_libnsl2 = "ld-linux-aarch64.so.1 libc.so.6 libtirpc.so.3"
RDEPENDS_libnsl2 = "glibc libtirpc"
RPM_SONAME_REQ_libnsl2-devel = "libnsl.so.2"
RPROVIDES_libnsl2-devel = "libnsl2-dev (= 1.2.0)"
RDEPENDS_libnsl2-devel = "libnsl2 libtirpc-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnsl2-1.2.0-2.20180605git4a062cf.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnsl2-devel-1.2.0-2.20180605git4a062cf.el8.aarch64.rpm \
          "

SRC_URI[libnsl2.sha256sum] = "b33276781f442757afd5e066ead95ec79927f2aed608a368420f230d5ee28686"
SRC_URI[libnsl2-devel.sha256sum] = "345b0e76cbe4195ae007a30946254ce6907e7011ad1ae646bce2c23f023043f9"
