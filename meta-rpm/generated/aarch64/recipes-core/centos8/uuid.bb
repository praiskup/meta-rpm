SUMMARY = "generated recipe based on uuid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_uuid = "libossp-uuid.so.16"
RPM_SONAME_REQ_uuid = "ld-linux-aarch64.so.1 libc.so.6 libossp-uuid.so.16"
RDEPENDS_uuid = "glibc"
RPM_SONAME_REQ_uuid-devel = "libossp-uuid.so.16"
RPROVIDES_uuid-devel = "uuid-dev (= 1.6.2)"
RDEPENDS_uuid-devel = "bash pkgconf-pkg-config uuid"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/uuid-1.6.2-42.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/uuid-devel-1.6.2-42.el8.aarch64.rpm \
          "

SRC_URI[uuid.sha256sum] = "d170559f81203819d1a21853e9f797c783783e7a51845032536ad6400b7df05f"
SRC_URI[uuid-devel.sha256sum] = "62e68327b5b64acda2c9bf01c2ba502333d344f83484c57a9173ce26889137d8"
