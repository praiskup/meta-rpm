SUMMARY = "generated recipe based on redhat-lsb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng12 pkgconfig-native"
RPROVIDES_redhat-lsb = "lsb-release (= 4.1)"
RDEPENDS_redhat-lsb = "bash redhat-lsb-core redhat-lsb-cxx redhat-lsb-desktop redhat-lsb-languages redhat-lsb-printing"
RDEPENDS_redhat-lsb-core = "at bash bc binutils coreutils cpio cronie cups-client diffutils ed file findutils gawk gettext glibc glibc-common grep gzip hostname libgcc m4 mailx make man-db ncurses-compat-libs pam passwd patch postfix procps-ng psmisc redhat-lsb-submod-security sed shadow-utils spax systemd tar time util-linux util-linux-user zlib"
RDEPENDS_redhat-lsb-cxx = "libstdc++ redhat-lsb-core"
RPM_SONAME_REQ_redhat-lsb-desktop = "libpng12.so.0"
RDEPENDS_redhat-lsb-desktop = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 gtk2 libICE libSM libX11 libXext libXft libXi libXrender libXt libXtst libjpeg-turbo libpng libpng12 libxml2 mesa-libGL mesa-libGLU pango redhat-lsb-core redhat-lsb-submod-multimedia xdg-utils"
RDEPENDS_redhat-lsb-languages = "perl-B-Lint perl-CGI perl-CPAN perl-Class-ISA perl-Env perl-ExtUtils-MakeMaker perl-File-CheckTree perl-Getopt-Long perl-Locale-Codes perl-Locale-Maketext perl-PathTools perl-Pod-Checker perl-Pod-LaTeX perl-Pod-Plainer perl-Scalar-List-Utils perl-Sys-Syslog perl-Test-Harness perl-Test-Simple perl-Text-Soundex perl-Time-HiRes perl-XML-LibXML perl-autodie perl-interpreter python36 redhat-lsb-core"
RDEPENDS_redhat-lsb-printing = "cups-filters cups-libs ghostscript redhat-lsb-core"
RDEPENDS_redhat-lsb-submod-multimedia = "alsa-lib"
RDEPENDS_redhat-lsb-submod-security = "nspr nss"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-core-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-cxx-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-desktop-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-languages-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-printing-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-submod-multimedia-4.1-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-lsb-submod-security-4.1-47.el8.aarch64.rpm \
          "

SRC_URI[redhat-lsb.sha256sum] = "b98c0862d327e77488809fe6618eef1c16fc6de30c1c96d8fd89bcdd8633357c"
SRC_URI[redhat-lsb-core.sha256sum] = "75affe53d41076eb4cf315c34b96b1844856a2cc73db24acdc2c1aa3646d2cbe"
SRC_URI[redhat-lsb-cxx.sha256sum] = "a80028585f65c9839194ecadef70599a4b5cfaa6a48fd63ea975b7665ecbbcae"
SRC_URI[redhat-lsb-desktop.sha256sum] = "c33b746c310e3bd1cef1f0f89cbb89eb994d029d8953f9ddf055a3138c13d653"
SRC_URI[redhat-lsb-languages.sha256sum] = "c03f2d7d2ce48c99227b9576beab9d1e0cff5b8942052b7890ada40a6ee1e36f"
SRC_URI[redhat-lsb-printing.sha256sum] = "b537385fe1fe73203767ff39d2f9a5ef154543c4d2f53f5117bfc6ed937f2eab"
SRC_URI[redhat-lsb-submod-multimedia.sha256sum] = "d38a81e5bfc8dc12482baca771d6974ac2fa0f799a41e5369824a00d168c48e5"
SRC_URI[redhat-lsb-submod-security.sha256sum] = "09fbe8c0b8b1ffd7ea7e9ffa7c523ff72f91b8c015faf862c036ef2a4c61fbfa"
