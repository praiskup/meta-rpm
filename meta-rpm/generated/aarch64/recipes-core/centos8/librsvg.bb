SUMMARY = "generated recipe based on librsvg2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk+3 libcroco libgcc libpng libxml2 pango pkgconfig-native zlib"
RPM_SONAME_PROV_librsvg2 = "libpixbufloader-svg.so librsvg-2.so.2"
RPM_SONAME_REQ_librsvg2 = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libcroco-0.6.so.3 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 libpthread.so.0 librsvg-2.so.2 libxml2.so.2 libz.so.1"
RDEPENDS_librsvg2 = "cairo fontconfig freetype gdk-pixbuf2 glib2 glibc libcroco libgcc libpng libxml2 pango zlib"
RPM_SONAME_REQ_librsvg2-devel = "librsvg-2.so.2"
RPROVIDES_librsvg2-devel = "librsvg2-dev (= 2.42.7)"
RDEPENDS_librsvg2-devel = "cairo-devel gdk-pixbuf2-devel glib2-devel librsvg2 pkgconf-pkg-config"
RPM_SONAME_REQ_librsvg2-tools = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcroco-0.6.so.3 libfontconfig.so.1 libfreetype.so.6 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 libpthread.so.0 librsvg-2.so.2 libxml2.so.2 libz.so.1"
RDEPENDS_librsvg2-tools = "atk cairo cairo-gobject fontconfig freetype gdk-pixbuf2 glib2 glibc gtk3 libcroco libpng librsvg2 libxml2 pango zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librsvg2-2.42.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librsvg2-devel-2.42.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librsvg2-tools-2.42.7-3.el8.aarch64.rpm \
          "

SRC_URI[librsvg2.sha256sum] = "03b12476899e9df304a360382ce13526dc2786654ac84f8192720e8f303c3e3b"
SRC_URI[librsvg2-devel.sha256sum] = "7f9f9b61610b41092ae89dfec1b1516511d9bce4ef1e89b94401d9d4616543bc"
SRC_URI[librsvg2-tools.sha256sum] = "06da1374cb4502afe96b9ae588f72c9729b90a4c2720b9476e4a823350cc4f63"
