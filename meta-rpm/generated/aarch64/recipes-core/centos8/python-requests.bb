SUMMARY = "generated recipe based on python-requests srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-requests = "platform-python python3-chardet python3-idna python3-urllib3"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-requests-2.20.0-2.1.el8_1.noarch.rpm \
          "

SRC_URI[python3-requests.sha256sum] = "003ee19ec5b88de212c3246bdfdb3e97a9910a25a219fd7cf5030b7bc666fea9"
