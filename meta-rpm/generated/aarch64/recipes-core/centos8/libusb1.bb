SUMMARY = "generated recipe based on libusbx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libusbx = "libusb-1.0.so.0"
RPM_SONAME_REQ_libusbx = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libudev.so.1"
RDEPENDS_libusbx = "glibc systemd-libs"
RPM_SONAME_REQ_libusbx-devel = "libusb-1.0.so.0"
RPROVIDES_libusbx-devel = "libusbx-dev (= 1.0.22)"
RDEPENDS_libusbx-devel = "libusbx pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libusbx-1.0.22-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libusbx-devel-1.0.22-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libusbx-devel-doc-1.0.22-1.el8.noarch.rpm \
          "

SRC_URI[libusbx.sha256sum] = "24145e86f4b02fac2175f3d8e9f38749618a8b770b45ae2a5a92311c220c9ff0"
SRC_URI[libusbx-devel.sha256sum] = "400dc4393a909bf1397ec57c4406720b157f45befdf0f4458827c12080caab8b"
SRC_URI[libusbx-devel-doc.sha256sum] = "e3a9b98b692c8346bf5a0427bb2deecf7b634908e411b0d4242cdeba5b94c5f5"
