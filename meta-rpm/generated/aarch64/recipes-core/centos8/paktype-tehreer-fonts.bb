SUMMARY = "generated recipe based on paktype-tehreer-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_paktype-tehreer-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/paktype-tehreer-fonts-4.1-8.el8.noarch.rpm \
          "

SRC_URI[paktype-tehreer-fonts.sha256sum] = "a23bd0879f0c7843cd449731b38446a9c65ad86b25985a253d5bd6bbb4b91368"
