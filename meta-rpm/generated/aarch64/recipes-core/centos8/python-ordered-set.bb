SUMMARY = "generated recipe based on python-ordered-set srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-ordered-set = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-ordered-set-2.0.2-4.el8.noarch.rpm \
          "

SRC_URI[python3-ordered-set.sha256sum] = "b01c567dbd2b9bf82301b254a8962a8fb8f30d12a8f566145eee0f3936f47b5d"
