SUMMARY = "generated recipe based on openchange srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libldb libtalloc libtevent pkgconfig-native samba"
RPM_SONAME_PROV_openchange = "libmapi-openchange.so.0 libmapiadmin.so.0 libmapipp.so.0 libocpf.so.0"
RPM_SONAME_REQ_openchange = "ld-linux-aarch64.so.1 libc.so.6 libdcerpc-binding.so.0 libdcerpc-samr.so.0 libdcerpc.so.0 libgcc_s.so.1 libldb.so.2 libm.so.6 libmapi-openchange.so.0 libndr-standard.so.0 libndr.so.0 libpthread.so.0 libsamba-credentials.so.0 libsamba-hostconfig.so.0 libsamba-util.so.0 libstdc++.so.6 libtalloc.so.2 libtevent.so.0"
RDEPENDS_openchange = "glibc libgcc libldb libstdc++ libtalloc libtevent samba-client-libs samba-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openchange-2.3-24.el8.0.2.aarch64.rpm \
          "

SRC_URI[openchange.sha256sum] = "f7ff0dfa3e5db7b91f35260729334188eaa08ef5da951663c8da1bf6feecd64c"
