SUMMARY = "generated recipe based on libraw1394 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libraw1394 = "libraw1394.so.11"
RPM_SONAME_REQ_libraw1394 = "ld-linux-aarch64.so.1 libc.so.6 libraw1394.so.11"
RDEPENDS_libraw1394 = "glibc"
RPM_SONAME_REQ_libraw1394-devel = "libraw1394.so.11"
RPROVIDES_libraw1394-devel = "libraw1394-dev (= 2.1.2)"
RDEPENDS_libraw1394-devel = "libraw1394 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libraw1394-2.1.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libraw1394-devel-2.1.2-5.el8.aarch64.rpm \
          "

SRC_URI[libraw1394.sha256sum] = "ca62f0a9cd7f557ae09a833bace14437969f41af3e517e778e6994fe1d70857c"
SRC_URI[libraw1394-devel.sha256sum] = "789649b9fcd057c040cd4958d37846b450c12227a3bce906ae90cd642b918dab"
