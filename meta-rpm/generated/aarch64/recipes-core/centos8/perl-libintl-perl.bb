SUMMARY = "generated recipe based on perl-libintl-perl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-libintl-perl = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-libintl-perl = "glibc perl-Carp perl-Encode perl-Exporter perl-IO perl-PathTools perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-libintl-perl-1.29-2.el8.aarch64.rpm \
          "

SRC_URI[perl-libintl-perl.sha256sum] = "b450ffc0b8f7124704f7179e61ba4d131e1c00ec76ff03cd5d4eaed7898ed517"
