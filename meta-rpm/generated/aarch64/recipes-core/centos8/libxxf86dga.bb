SUMMARY = "generated recipe based on libXxf86dga srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXxf86dga = "libXxf86dga.so.1"
RPM_SONAME_REQ_libXxf86dga = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXxf86dga = "glibc libX11 libXext"
RPM_SONAME_REQ_libXxf86dga-devel = "libXxf86dga.so.1"
RPROVIDES_libXxf86dga-devel = "libXxf86dga-dev (= 1.1.4)"
RDEPENDS_libXxf86dga-devel = "libX11-devel libXext-devel libXxf86dga pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXxf86dga-1.1.4-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXxf86dga-devel-1.1.4-12.el8.aarch64.rpm \
          "

SRC_URI[libXxf86dga.sha256sum] = "e902132d57851ee7ff4a0eb40949c085a175ee61f7271791278c81f5af70e080"
SRC_URI[libXxf86dga-devel.sha256sum] = "39180d255bab84217236a3dab494f1755f7cb698fd77c889ac537ec4d01ac354"
