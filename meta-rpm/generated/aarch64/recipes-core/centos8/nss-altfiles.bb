SUMMARY = "generated recipe based on nss-altfiles srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_nss-altfiles = "libnss_altfiles.so.2"
RPM_SONAME_REQ_nss-altfiles = "libpthread.so.0"
RDEPENDS_nss-altfiles = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-altfiles-2.18.1-12.el8.aarch64.rpm \
          "

SRC_URI[nss-altfiles.sha256sum] = "e6efdfca0a04a17fd2ce9b37e1379b6025dd68c9ec5199dbee758700326c2922"
