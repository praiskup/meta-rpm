SUMMARY = "generated recipe based on perl-IO-HTML srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-HTML = "perl-Carp perl-Encode perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-IO-HTML-1.001-10.el8.noarch.rpm \
          "

SRC_URI[perl-IO-HTML.sha256sum] = "a6b0ba9d8cedf297aa28d1935ace7dfc4715b752047423fd9fd4bb0504a84d95"
