SUMMARY = "generated recipe based on dtc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dtc = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_dtc = "bash glibc"
RPM_SONAME_PROV_libfdt = "libfdt.so.1"
RPM_SONAME_REQ_libfdt = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libfdt = "glibc"
RPM_SONAME_REQ_libfdt-devel = "libfdt.so.1"
RPROVIDES_libfdt-devel = "libfdt-dev (= 1.4.6)"
RDEPENDS_libfdt-devel = "libfdt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libfdt-1.4.6-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dtc-1.4.6-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libfdt-devel-1.4.6-1.el8.aarch64.rpm \
          "

SRC_URI[dtc.sha256sum] = "1a50bd59d6d219b2d88f7458131eaa3c700672ab4f8977db293e927a4ebfc141"
SRC_URI[libfdt.sha256sum] = "46cc9167fb4c53c0dd4d6729ace7b8a1d4187cf1e176986ff431470ca06701e6"
SRC_URI[libfdt-devel.sha256sum] = "020db7c38312c07796a91b5a280613ecc1659878fc39ea76b599b3d186e6d263"
