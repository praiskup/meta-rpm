SUMMARY = "generated recipe based on dconf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_dconf = "libdconf.so.1 libdconfsettings.so"
RPM_SONAME_REQ_dconf = "ld-linux-aarch64.so.1 libc.so.6 libdconf.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_dconf = "bash dbus glib2 glibc"
RPM_SONAME_REQ_dconf-devel = "libdconf.so.1"
RPROVIDES_dconf-devel = "dconf-dev (= 0.28.0)"
RDEPENDS_dconf-devel = "dconf glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dconf-0.28.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dconf-devel-0.28.0-3.el8.aarch64.rpm \
          "

SRC_URI[dconf.sha256sum] = "94f4b040c1f7de168d82f300ae9c4fc6f9b601b8310aba85aa6469625f8b30e2"
SRC_URI[dconf-devel.sha256sum] = "50902b8d0c612dd3c8153fa8b2f0f587673515299f6d83beace4c595e63d3a58"
