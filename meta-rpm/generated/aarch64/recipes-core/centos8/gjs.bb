SUMMARY = "generated recipe based on gjs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gobject-introspection gtk+3 libffi libgcc libx11 libxext mozjs60 pango pkgconfig-native readline"
RPM_SONAME_PROV_gjs = "libgjs.so.0"
RPM_SONAME_REQ_gjs = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libffi.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libgjs.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libm.so.6 libmozjs-60.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libreadline.so.7 librt.so.1 libstdc++.so.6"
RDEPENDS_gjs = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gobject-introspection gtk3 libX11 libXext libffi libgcc libstdc++ mozjs60 pango readline"
RPM_SONAME_REQ_gjs-devel = "libgjs.so.0"
RPROVIDES_gjs-devel = "gjs-dev (= 1.56.2)"
RDEPENDS_gjs-devel = "cairo-devel cairo-gobject-devel gjs glib2-devel gobject-introspection-devel gtk3-devel libffi-devel mozjs60-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gjs-1.56.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gjs-devel-1.56.2-4.el8.aarch64.rpm \
          "

SRC_URI[gjs.sha256sum] = "8cc51b55bd165a66347799ed881a6d6914f017c35e28bb7933acf1608697b5ce"
SRC_URI[gjs-devel.sha256sum] = "a5be64ced74b3ae6ee7f048d59f6661917885db6cb22498802a07a6e9139e588"
