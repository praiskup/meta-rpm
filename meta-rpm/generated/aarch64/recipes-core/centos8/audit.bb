SUMMARY = "generated recipe based on audit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "krb5-libs libcap-ng pkgconfig-native"
RPM_SONAME_REQ_audispd-plugins = "ld-linux-aarch64.so.1 libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libgssapi_krb5.so.2 libkrb5.so.3 libpthread.so.0"
RDEPENDS_audispd-plugins = "audit audit-libs glibc krb5-libs libcap-ng"
RPM_SONAME_REQ_audispd-plugins-zos = "ld-linux-aarch64.so.1 libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0"
RDEPENDS_audispd-plugins-zos = "audit audit-libs glibc libcap-ng openldap"
RPM_SONAME_REQ_audit = "ld-linux-aarch64.so.1 libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libgssapi_krb5.so.2 libkrb5.so.3 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_audit = "audit-libs bash coreutils glibc initscripts krb5-libs libcap-ng systemd"
RPM_SONAME_PROV_audit-libs = "libaudit.so.1 libauparse.so.0"
RPM_SONAME_REQ_audit-libs = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcap-ng.so.0 libpthread.so.0"
RDEPENDS_audit-libs = "glibc libcap-ng"
RPM_SONAME_REQ_audit-libs-devel = "libaudit.so.1 libauparse.so.0"
RPROVIDES_audit-libs-devel = "audit-libs-dev (= 3.0)"
RDEPENDS_audit-libs-devel = "audit-libs kernel-headers pkgconf-pkg-config"
RPM_SONAME_REQ_python3-audit = "ld-linux-aarch64.so.1 libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libpthread.so.0"
RDEPENDS_python3-audit = "audit-libs glibc libcap-ng platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audispd-plugins-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audispd-plugins-zos-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audit-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audit-libs-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audit-libs-devel-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-audit-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
          "

SRC_URI[audispd-plugins.sha256sum] = "08c5de6df1b9fea6c893fff720b6aec60c76d3fd83c0d18a4ba6f839e3f7dc14"
SRC_URI[audispd-plugins-zos.sha256sum] = "cb96812f914341b67836fd076a7bea4bcedc7ee1824a0e5acc3ec07e783c7e97"
SRC_URI[audit.sha256sum] = "2036b917615991af2730df046d2cea4b66a69d9592a4e97faeb7134bbdba45ed"
SRC_URI[audit-libs.sha256sum] = "11811c556a3bdc9c572c0ab67d3106bd1de3406c9d471de03e028f041b5785c3"
SRC_URI[audit-libs-devel.sha256sum] = "0bdc44f5e68de41c88d4d687af86a92693ce02e62e6335869b610b24cf769fea"
SRC_URI[python3-audit.sha256sum] = "122fe05bd35778f2887e7f5cad32e8e93247fbbd71bd3da5ed78f788d529d028"
