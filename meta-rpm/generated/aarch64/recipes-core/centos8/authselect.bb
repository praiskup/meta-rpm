SUMMARY = "generated recipe based on authselect srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux pkgconfig-native popt"
RPM_SONAME_REQ_authselect = "ld-linux-aarch64.so.1 libauthselect.so.1 libc.so.6 libpopt.so.0 libselinux.so.1"
RDEPENDS_authselect = "authselect-libs glibc libselinux popt"
RDEPENDS_authselect-compat = "authselect bash platform-python sed"
RPM_SONAME_PROV_authselect-libs = "libauthselect.so.1"
RPM_SONAME_REQ_authselect-libs = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_authselect-libs = "bash coreutils findutils gawk glibc grep libselinux sed systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/authselect-compat-1.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/authselect-1.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/authselect-libs-1.1-2.el8.aarch64.rpm \
          "

SRC_URI[authselect.sha256sum] = "a5d616eda2b80c58b429388fc966a542b5d9b712649101e7eb1b12fde7de8bf0"
SRC_URI[authselect-compat.sha256sum] = "a315fa901f4ddce49ebfd194a2fafb2100fd0d7b83646f982815684a5b167472"
SRC_URI[authselect-libs.sha256sum] = "817f81f1b356adb7722324f8e85048db6d48202822a5a57a13d8088cd059151a"
