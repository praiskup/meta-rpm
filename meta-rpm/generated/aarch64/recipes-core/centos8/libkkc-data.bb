SUMMARY = "generated recipe based on libkkc-data srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libkkc-data-0.2.7-12.el8.aarch64.rpm \
          "

SRC_URI[libkkc-data.sha256sum] = "e9ae707aab4cd9fb3402c7a0fa38169a9d738c07dc6baebab4a3666470742110"
