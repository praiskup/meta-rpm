SUMMARY = "generated recipe based on perl-Class-Inspector srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Inspector = "perl-Exporter perl-PathTools perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Class-Inspector-1.32-2.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Inspector.sha256sum] = "24b9a381c389c94514357feee5367002456dfef44163aac27c99400e91c9da0b"
