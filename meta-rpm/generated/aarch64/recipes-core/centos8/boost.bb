SUMMARY = "generated recipe based on boost srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 icu libgcc mpich openmpi pkgconfig-native platform-python3 zlib"
RDEPENDS_boost = "boost-atomic boost-chrono boost-container boost-context boost-coroutine boost-date-time boost-fiber boost-filesystem boost-graph boost-iostreams boost-locale boost-log boost-math boost-program-options boost-random boost-regex boost-serialization boost-signals boost-stacktrace boost-system boost-test boost-thread boost-timer boost-type_erasure boost-wave"
RPM_SONAME_PROV_boost-atomic = "libboost_atomic.so.1.66.0"
RPM_SONAME_REQ_boost-atomic = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-atomic = "glibc libgcc libstdc++"
RDEPENDS_boost-build = "boost-jam platform-python"
RPM_SONAME_PROV_boost-chrono = "libboost_chrono.so.1.66.0"
RPM_SONAME_REQ_boost-chrono = "ld-linux-aarch64.so.1 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-chrono = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-container = "libboost_container.so.1.66.0"
RPM_SONAME_REQ_boost-container = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-container = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-context = "libboost_context.so.1.66.0"
RPM_SONAME_REQ_boost-context = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-context = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-coroutine = "libboost_coroutine.so.1.66.0"
RPM_SONAME_REQ_boost-coroutine = "ld-linux-aarch64.so.1 libboost_chrono.so.1.66.0 libboost_context.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-coroutine = "boost-chrono boost-context boost-system boost-thread glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-date-time = "libboost_date_time.so.1.66.0"
RPM_SONAME_REQ_boost-date-time = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-date-time = "glibc libgcc libstdc++"
RPM_SONAME_REQ_boost-devel = "libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_container.so.1.66.0 libboost_context.so.1.66.0 libboost_coroutine.so.1.66.0 libboost_date_time.so.1.66.0 libboost_fiber.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_graph.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_locale.so.1.66.0 libboost_log.so.1.66.0 libboost_log_setup.so.1.66.0 libboost_math_c99.so.1.66.0 libboost_math_c99f.so.1.66.0 libboost_math_c99l.so.1.66.0 libboost_math_tr1.so.1.66.0 libboost_math_tr1f.so.1.66.0 libboost_math_tr1l.so.1.66.0 libboost_prg_exec_monitor.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_serialization.so.1.66.0 libboost_signals.so.1.66.0 libboost_stacktrace_addr2line.so.1.66.0 libboost_stacktrace_basic.so.1.66.0 libboost_stacktrace_noop.so.1.66.0 libboost_system.so.1.66.0 libboost_timer.so.1.66.0 libboost_type_erasure.so.1.66.0 libboost_unit_test_framework.so.1.66.0 libboost_wave.so.1.66.0 libboost_wserialization.so.1.66.0"
RPROVIDES_boost-devel = "boost-dev (= 1.66.0)"
RDEPENDS_boost-devel = "boost boost-atomic boost-chrono boost-container boost-context boost-coroutine boost-date-time boost-fiber boost-filesystem boost-graph boost-iostreams boost-locale boost-log boost-math boost-program-options boost-random boost-regex boost-serialization boost-signals boost-stacktrace boost-system boost-test boost-timer boost-type_erasure boost-wave libicu-devel"
RDEPENDS_boost-examples = "boost-devel"
RPM_SONAME_PROV_boost-fiber = "libboost_fiber.so.1.66.0"
RPM_SONAME_REQ_boost-fiber = "ld-linux-aarch64.so.1 libboost_context.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-fiber = "boost-context boost-filesystem boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-filesystem = "libboost_filesystem.so.1.66.0"
RPM_SONAME_REQ_boost-filesystem = "ld-linux-aarch64.so.1 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-filesystem = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-graph = "libboost_graph.so.1.66.0"
RPM_SONAME_REQ_boost-graph = "ld-linux-aarch64.so.1 libboost_regex.so.1.66.0 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-graph = "boost-regex glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-graph-mpich = "libboost_graph_parallel.so.1.66.0"
RPM_SONAME_REQ_boost-graph-mpich = "ld-linux-aarch64.so.1 libboost_mpi.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-graph-mpich = "boost-mpich boost-serialization glibc libgcc libstdc++ mpich"
RPM_SONAME_PROV_boost-graph-openmpi = "libboost_graph_parallel.so.1.66.0"
RPM_SONAME_REQ_boost-graph-openmpi = "ld-linux-aarch64.so.1 libboost_mpi.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-graph-openmpi = "boost-openmpi boost-serialization glibc libgcc libstdc++ openmpi"
RPM_SONAME_PROV_boost-iostreams = "libboost_iostreams.so.1.66.0"
RPM_SONAME_REQ_boost-iostreams = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_boost-iostreams = "bzip2-libs glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_boost-jam = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_boost-jam = "glibc"
RPM_SONAME_PROV_boost-locale = "libboost_locale.so.1.66.0"
RPM_SONAME_REQ_boost-locale = "ld-linux-aarch64.so.1 libboost_chrono.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-locale = "boost-chrono boost-system boost-thread glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-log = "libboost_log.so.1.66.0 libboost_log_setup.so.1.66.0"
RPM_SONAME_REQ_boost-log = "ld-linux-aarch64.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_log.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-log = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-regex boost-system boost-thread glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-math = "libboost_math_c99.so.1.66.0 libboost_math_c99f.so.1.66.0 libboost_math_c99l.so.1.66.0 libboost_math_tr1.so.1.66.0 libboost_math_tr1f.so.1.66.0 libboost_math_tr1l.so.1.66.0"
RPM_SONAME_REQ_boost-math = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-math = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-mpich = "libboost_mpi.so.1.66.0"
RPM_SONAME_REQ_boost-mpich = "ld-linux-aarch64.so.1 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-mpich = "boost-serialization glibc libgcc libstdc++ mpich"
RPM_SONAME_REQ_boost-mpich-devel = "libboost_graph_parallel.so.1.66.0 libboost_mpi.so.1.66.0"
RPROVIDES_boost-mpich-devel = "boost-mpich-dev (= 1.66.0)"
RDEPENDS_boost-mpich-devel = "boost-devel boost-graph-mpich boost-mpich"
RPM_SONAME_PROV_boost-mpich-python3 = "libboost_mpi_python3.so.1.66.0"
RPM_SONAME_REQ_boost-mpich-python3 = "ld-linux-aarch64.so.1 libboost_mpi.so.1.66.0 libboost_mpi_python3.so.1.66.0 libboost_python3.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-mpich-python3 = "boost-mpich boost-python3 boost-serialization glibc libgcc libstdc++ mpich platform-python python3-libs python3-mpich"
RPM_SONAME_PROV_boost-numpy3 = "libboost_numpy3.so.1.66.0"
RPM_SONAME_REQ_boost-numpy3 = "ld-linux-aarch64.so.1 libboost_python3.so.1.66.0 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-numpy3 = "boost-python3 glibc libgcc libstdc++ python3-libs python3-numpy"
RPM_SONAME_PROV_boost-openmpi = "libboost_mpi.so.1.66.0"
RPM_SONAME_REQ_boost-openmpi = "ld-linux-aarch64.so.1 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-openmpi = "boost-serialization glibc libgcc libstdc++ openmpi"
RPM_SONAME_REQ_boost-openmpi-devel = "libboost_graph_parallel.so.1.66.0 libboost_mpi.so.1.66.0"
RPROVIDES_boost-openmpi-devel = "boost-openmpi-dev (= 1.66.0)"
RDEPENDS_boost-openmpi-devel = "boost-devel boost-graph-openmpi boost-openmpi"
RPM_SONAME_PROV_boost-openmpi-python3 = "libboost_mpi_python3.so.1.66.0"
RPM_SONAME_REQ_boost-openmpi-python3 = "ld-linux-aarch64.so.1 libboost_mpi.so.1.66.0 libboost_mpi_python3.so.1.66.0 libboost_python3.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-openmpi-python3 = "boost-openmpi boost-python3 boost-serialization glibc libgcc libstdc++ openmpi platform-python python3-libs python3-openmpi"
RPM_SONAME_PROV_boost-program-options = "libboost_program_options.so.1.66.0"
RPM_SONAME_REQ_boost-program-options = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-program-options = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-python3 = "libboost_python3.so.1.66.0"
RPM_SONAME_REQ_boost-python3 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-python3 = "glibc libgcc libstdc++ python3-libs"
RPM_SONAME_REQ_boost-python3-devel = "libboost_numpy3.so.1.66.0 libboost_python3.so.1.66.0"
RPROVIDES_boost-python3-devel = "boost-python3-dev (= 1.66.0)"
RDEPENDS_boost-python3-devel = "boost-devel boost-numpy3 boost-python3"
RPM_SONAME_PROV_boost-random = "libboost_random.so.1.66.0"
RPM_SONAME_REQ_boost-random = "ld-linux-aarch64.so.1 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-random = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-regex = "libboost_regex.so.1.66.0"
RPM_SONAME_REQ_boost-regex = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-regex = "glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-serialization = "libboost_serialization.so.1.66.0 libboost_wserialization.so.1.66.0"
RPM_SONAME_REQ_boost-serialization = "ld-linux-aarch64.so.1 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-serialization = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-signals = "libboost_signals.so.1.66.0"
RPM_SONAME_REQ_boost-signals = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-signals = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-stacktrace = "libboost_stacktrace_addr2line.so.1.66.0 libboost_stacktrace_basic.so.1.66.0 libboost_stacktrace_noop.so.1.66.0"
RPM_SONAME_REQ_boost-stacktrace = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-stacktrace = "glibc libgcc libstdc++"
RDEPENDS_boost-static = "boost-devel"
RPM_SONAME_PROV_boost-system = "libboost_system.so.1.66.0"
RPM_SONAME_REQ_boost-system = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-system = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-test = "libboost_prg_exec_monitor.so.1.66.0 libboost_unit_test_framework.so.1.66.0"
RPM_SONAME_REQ_boost-test = "ld-linux-aarch64.so.1 libboost_system.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-test = "boost-system boost-timer glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-thread = "libboost_thread.so.1.66.0"
RPM_SONAME_REQ_boost-thread = "ld-linux-aarch64.so.1 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-thread = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-timer = "libboost_timer.so.1.66.0"
RPM_SONAME_REQ_boost-timer = "ld-linux-aarch64.so.1 libboost_chrono.so.1.66.0 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-timer = "boost-chrono boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-type_erasure = "libboost_type_erasure.so.1.66.0"
RPM_SONAME_REQ_boost-type_erasure = "ld-linux-aarch64.so.1 libboost_chrono.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-type_erasure = "boost-chrono boost-system boost-thread glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-wave = "libboost_wave.so.1.66.0"
RPM_SONAME_REQ_boost-wave = "ld-linux-aarch64.so.1 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-wave = "boost-chrono boost-date-time boost-filesystem boost-system boost-thread glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-atomic-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-chrono-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-container-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-context-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-coroutine-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-date-time-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-devel-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-fiber-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-filesystem-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-graph-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-iostreams-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-locale-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-log-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-math-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-program-options-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-random-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-regex-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-serialization-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-signals-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-stacktrace-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-system-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-test-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-thread-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-timer-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-type_erasure-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/boost-wave-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-build-1.66.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-doc-1.66.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-examples-1.66.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-graph-mpich-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-graph-openmpi-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-jam-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-mpich-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-mpich-devel-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-mpich-python3-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-numpy3-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-openmpi-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-openmpi-devel-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-openmpi-python3-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-python3-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-python3-devel-1.66.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/boost-static-1.66.0-7.el8.aarch64.rpm \
          "

SRC_URI[boost.sha256sum] = "b5b365b8b901a34cb3a8bc7256e385c2059286cfb8f77b2eb461a4de3389c99c"
SRC_URI[boost-atomic.sha256sum] = "4f2e6431d9738ad644b9742ab6f42d6c96401b190c8e9f493b4d760b801a4de3"
SRC_URI[boost-build.sha256sum] = "620180b4ace4b81b9b4c349eae2c1163fb036b847b9c5bb6f4a8f681b4375955"
SRC_URI[boost-chrono.sha256sum] = "c5a9722a03aa40f5bdfc26ec5df9a9a0c81d3698d508ec6db0107f72318ac8c2"
SRC_URI[boost-container.sha256sum] = "403ff9469174c057fae84925a2ba062f4807280863abe6eb489c5c5348d044f1"
SRC_URI[boost-context.sha256sum] = "9c6701e8dc872bfb92c3155235fa1e0bfd689e141f6a0eedeb92db9ae75e2bfb"
SRC_URI[boost-coroutine.sha256sum] = "f0aacb7dd3defe79a541bdb7ff68978ec1630c4191c1652f3dadbd7841c48b32"
SRC_URI[boost-date-time.sha256sum] = "159a3ccdb4c044948f24c09a922fef6a035c862aac17c35b0a0a4dda68590f61"
SRC_URI[boost-devel.sha256sum] = "ed76c37cd797c84f745e047d06a8d8d1e7b000ea4f29e163fc47ce0b7eb8e165"
SRC_URI[boost-doc.sha256sum] = "debee86c107bf8f326a71d189cae237e806d3b993dcb6b2d319ad098c730f832"
SRC_URI[boost-examples.sha256sum] = "c8961375f400f1a1aa53c4676c9f59ac95cc758332f94e8c2521b661fb2a7c35"
SRC_URI[boost-fiber.sha256sum] = "d796246f1ab831b822ffe0753c157ada675f6cab6363be2cdf43b3830cde0b3f"
SRC_URI[boost-filesystem.sha256sum] = "c470d82c0b90d593a36b291836f32d29e429ee257b5ce5c30e74d8d445e04b9d"
SRC_URI[boost-graph.sha256sum] = "90e95c4521396ae43c82f9bcf737b9244bde8f41c7150adee64462740d71bdaa"
SRC_URI[boost-graph-mpich.sha256sum] = "abab0a9c09b775afe0718dc346c112983a92ec5d9ee361712da26b43eed047e6"
SRC_URI[boost-graph-openmpi.sha256sum] = "44ccad1f14dedfe3cca80cd21ae20f647485f945a9f8444f363a4801c4c0f36a"
SRC_URI[boost-iostreams.sha256sum] = "b69e5cc32cf53399fdc0b1926d1bf48fb1741eaf146edac61ec5f9449b453692"
SRC_URI[boost-jam.sha256sum] = "81934018e3b6b3472bb5451a2a2a0524b0885a2c626bd0899aef79aa8c21cc3b"
SRC_URI[boost-locale.sha256sum] = "d38c1c734d9df6c52351764940fde8eb88aff58afabd9bc2c3d212181c493704"
SRC_URI[boost-log.sha256sum] = "94eedae7d64c1066a0ec94f48691fc3b0ab4adf12a859c5104dbabe6222f2565"
SRC_URI[boost-math.sha256sum] = "8d25a50e1b953500ee525e74a9b74e8fe6f3c308f88788d378cce733e83739eb"
SRC_URI[boost-mpich.sha256sum] = "124d4315c690af70ff984ae018abb37190409965524930d06069d39f8a15736d"
SRC_URI[boost-mpich-devel.sha256sum] = "efec4ea13c60374fc04e7af52744c7083537341e22ab6fb2e7bcc856d7e7a742"
SRC_URI[boost-mpich-python3.sha256sum] = "746917693649fe6b098fea77e5a55659f5ea0b88ca1b8b9410f43894bb2b98dd"
SRC_URI[boost-numpy3.sha256sum] = "6c2be60de0a635a7da60ac87caa93ed6410d2604940c923656568ecd54b12399"
SRC_URI[boost-openmpi.sha256sum] = "266eab85b6d1dd56153d98bebb58f45ebd793570764599b6c870218a1935675e"
SRC_URI[boost-openmpi-devel.sha256sum] = "e074db7025fae48b4d03b25615c1d101bbbac7fb1be529a2b9d32c09e4618f05"
SRC_URI[boost-openmpi-python3.sha256sum] = "a2f01e794bcb255ca7d318dfee7248144c8532ba1bf1cb509955914d427226e8"
SRC_URI[boost-program-options.sha256sum] = "30d48de396fc68b497ad4b613765ab761152a8eb9c1d5b62f253df3a3dd43a81"
SRC_URI[boost-python3.sha256sum] = "aa5f41d54a84480e3faacf1301710dd608052a6e5445c837bc24bae9bbbc5e84"
SRC_URI[boost-python3-devel.sha256sum] = "6e278b113c409f7138a077ce2a3e462fee0be50b07d081c02ba78448231764d8"
SRC_URI[boost-random.sha256sum] = "7276fbf6172b6b743d2ef70b4e8f3fc8d6af8d940f99292181ff140b766177dc"
SRC_URI[boost-regex.sha256sum] = "eeec0538c8a539b748e2873a708ca73ae4f33b0de1329b9fed7a38969bc85b54"
SRC_URI[boost-serialization.sha256sum] = "ecc6a76cd2e1d41560ae8a0cf22685fb2f5d55f4c6628b7d92d1f104cfa225fb"
SRC_URI[boost-signals.sha256sum] = "f927cd96227a28762de568c8eef8929f4d7f1bb6ceebab391c51d6c38694297b"
SRC_URI[boost-stacktrace.sha256sum] = "92b2200264096be36c28254a061b9c5117f11a5db385138e4945bd7b79b8f172"
SRC_URI[boost-static.sha256sum] = "f06f495063e117a0bbe5e489f2289d4f9f0958847f86cca34b498871063986ae"
SRC_URI[boost-system.sha256sum] = "944e87af4b2652ebefe552f3673d987d8a2bbecaa5b2413418644d2b0b1bfc7f"
SRC_URI[boost-test.sha256sum] = "d6661ddcace024b9875cfcf17bcdb590a8af796a9d81a33179f3b359849cb872"
SRC_URI[boost-thread.sha256sum] = "79e455a34d21c98532ef511714809801c5ed0f0c41a94ca4aa4838cfb5971b85"
SRC_URI[boost-timer.sha256sum] = "9fce3b8fdc1d9205964a5005d880638893921b2e5adad37f5a6b87f23912fd1f"
SRC_URI[boost-type_erasure.sha256sum] = "9037f962ea2558f0fbc3ee56c0ca0254ce5e8f1ad71193213abdaacea2a3feb2"
SRC_URI[boost-wave.sha256sum] = "8e29433a0e607fb00b3041facdf4bcce47383f486be9d67141c15a8e2e1de998"
