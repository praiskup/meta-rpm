SUMMARY = "generated recipe based on xorg-x11-server srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/xserver-xf86"
DEPENDS = "audit dbus-libs libdmx libdrm libepoxy libglvnd libpciaccess libselinux libx11 libxau libxaw libxcb libxdmcp libxext libxfixes libxfont2 libxi libxmu libxpm libxrender libxshmfence libxt mesa openssl pixman pkgconfig-native systemd-libs wayland xcb-util xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm xorg-x11-proto-devel"
RPM_SONAME_REQ_xorg-x11-server-Xdmx = "ld-linux-aarch64.so.1 libX11.so.6 libXau.so.6 libXaw.so.7 libXdmcp.so.6 libXext.so.6 libXfixes.so.3 libXfont2.so.2 libXi.so.6 libXmu.so.6 libXmuu.so.1 libXpm.so.4 libXrender.so.1 libXt.so.6 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdmx.so.1 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xdmx = "audit-libs glibc libX11 libXau libXaw libXdmcp libXext libXfixes libXfont2 libXi libXmu libXpm libXrender libXt libdmx libselinux libxshmfence openssl-libs pixman systemd-libs xorg-x11-server-common"
RPM_SONAME_REQ_xorg-x11-server-Xephyr = "ld-linux-aarch64.so.1 libGL.so.1 libX11-xcb.so.1 libX11.so.6 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libdrm.so.2 libepoxy.so.0 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libudev.so.1 libxcb-glx.so.0 libxcb-icccm.so.4 libxcb-image.so.0 libxcb-keysyms.so.1 libxcb-randr.so.0 libxcb-render-util.so.0 libxcb-render.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-util.so.1 libxcb-xf86dri.so.0 libxcb-xkb.so.1 libxcb-xv.so.0 libxcb.so.1 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xephyr = "audit-libs dbus-libs glibc libX11 libX11-xcb libXau libXdmcp libXfont2 libdrm libepoxy libglvnd-glx libselinux libxcb libxshmfence openssl-libs pixman systemd-libs xcb-util xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm xorg-x11-server-common"
RPM_SONAME_REQ_xorg-x11-server-Xnest = "ld-linux-aarch64.so.1 libGL.so.1 libX11.so.6 libXau.so.6 libXdmcp.so.6 libXext.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xnest = "audit-libs glibc libX11 libXau libXdmcp libXext libXfont2 libglvnd-glx libselinux libxshmfence openssl-libs pixman systemd-libs xorg-x11-server-common"
RPM_SONAME_PROV_xorg-x11-server-Xorg = "libexa.so libfb.so libfbdevhw.so libglamoregl.so libglx.so libshadow.so libshadowfb.so libvgahw.so libwfb.so"
RPM_SONAME_REQ_xorg-x11-server-Xorg = "ld-linux-aarch64.so.1 libGL.so.1 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libdrm.so.2 libepoxy.so.0 libgbm.so.1 libm.so.6 libpciaccess.so.0 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libudev.so.1 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xorg = "audit-libs bash dbus-libs glibc libXau libXdmcp libXfont2 libdrm libepoxy libglvnd-egl libglvnd-glx libpciaccess libselinux libxshmfence mesa-libgbm openssl-libs pixman systemd systemd-libs xorg-x11-drv-fbdev xorg-x11-drv-libinput xorg-x11-server-common"
RPM_SONAME_REQ_xorg-x11-server-Xvfb = "ld-linux-aarch64.so.1 libGL.so.1 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xvfb = "audit-libs bash glibc libXau libXdmcp libXfont2 libglvnd-glx libselinux libxshmfence openssl-libs pixman systemd-libs xorg-x11-server-common xorg-x11-xauth"
RPM_SONAME_REQ_xorg-x11-server-Xwayland = "ld-linux-aarch64.so.1 libEGL.so.1 libGL.so.1 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdrm.so.2 libepoxy.so.0 libgbm.so.1 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libwayland-client.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xwayland = "audit-libs glibc libXau libXdmcp libXfont2 libdrm libepoxy libglvnd-egl libglvnd-glx libselinux libwayland-client libxshmfence mesa-libgbm openssl-libs pixman systemd-libs xorg-x11-server-common"
RDEPENDS_xorg-x11-server-common = "pixman xkeyboard-config xorg-x11-xkb-utils"
RPROVIDES_xorg-x11-server-devel = "xorg-x11-server-dev (= 1.20.6)"
RDEPENDS_xorg-x11-server-devel = "bash libXfont2-devel libpciaccess-devel mesa-libGL-devel pixman-devel pkgconf-pkg-config xorg-x11-proto-devel xorg-x11-util-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xdmx-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xephyr-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xnest-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xorg-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xvfb-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xwayland-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-common-1.20.6-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xorg-x11-server-devel-1.20.6-3.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-server-Xdmx.sha256sum] = "e84a47bcd84f73cd1d9e6577d5cdfcac7dfc928236a97caa53ee30121c0640d9"
SRC_URI[xorg-x11-server-Xephyr.sha256sum] = "11764eb8fc5a2e174e6b47a2cf72d200a63666e914961f2dd01406b80b44fd43"
SRC_URI[xorg-x11-server-Xnest.sha256sum] = "3e161adc918292cb610c3289f6cc44e6260d3c3346a255b840daa8b83d5a3b54"
SRC_URI[xorg-x11-server-Xorg.sha256sum] = "37aacc38279dd56ab356c5a806e8fbb7f0e0af923e860c2c7c4e5b55f91217f8"
SRC_URI[xorg-x11-server-Xvfb.sha256sum] = "6b00f26d4e9acf28ec1330ac795e83b3cefe5629e46b8394336ea5d0f445ed2d"
SRC_URI[xorg-x11-server-Xwayland.sha256sum] = "3403e2c3265a6452684fee26f4e0f5384dc4b950d669f5c331c37d78217853d4"
SRC_URI[xorg-x11-server-common.sha256sum] = "26972796d6f9e277c8fa379689e77d6b435369eeea62366af8adbf7ca3418531"
SRC_URI[xorg-x11-server-devel.sha256sum] = "bacf68354fa990ba0814be574299055fc0a1449f0b509212c0fde991c0e6c64a"
