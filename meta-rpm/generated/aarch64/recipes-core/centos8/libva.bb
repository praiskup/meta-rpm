SUMMARY = "generated recipe based on libva srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdrm libglvnd libx11 libxext libxfixes pkgconfig-native wayland"
RPM_SONAME_PROV_libva = "libva-drm.so.2 libva-glx.so.2 libva-wayland.so.2 libva-x11.so.2 libva.so.2"
RPM_SONAME_REQ_libva = "ld-linux-aarch64.so.1 libGL.so.1 libX11.so.6 libXext.so.6 libXfixes.so.3 libc.so.6 libdl.so.2 libdrm.so.2 libva-x11.so.2 libva.so.2 libwayland-client.so.0"
RDEPENDS_libva = "glibc libX11 libXext libXfixes libdrm libglvnd-glx libwayland-client mesa-filesystem"
RPM_SONAME_REQ_libva-devel = "libva-drm.so.2 libva-glx.so.2 libva-wayland.so.2 libva-x11.so.2 libva.so.2"
RPROVIDES_libva-devel = "libva-dev (= 2.5.0)"
RDEPENDS_libva-devel = "libva pkgconf-pkg-config wayland-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libva-2.5.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libva-devel-2.5.0-2.el8.aarch64.rpm \
          "

SRC_URI[libva.sha256sum] = "68d418193d665ff69db9f04e8e07711b36db4ab143a9a3565084950d96b5cf4d"
SRC_URI[libva-devel.sha256sum] = "b1e344f12fba66542e9050f32b5244c5176993ef9eabd76dac3095b34027851b"
