SUMMARY = "generated recipe based on hunspell-gu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-gu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-gu-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-gu.sha256sum] = "dec80188e5bc457b4aca61dee98d819738671c52daa109697dd0e97db295fd53"
