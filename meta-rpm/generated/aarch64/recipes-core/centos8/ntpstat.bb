SUMMARY = "generated recipe based on ntpstat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ntpstat = "bash chrony"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ntpstat-0.5-2.el8.noarch.rpm \
          "

SRC_URI[ntpstat.sha256sum] = "39adac00747d8f9825b936a3b6613c0ee73fe0720b5b26b8bdb26a902bb59b4a"
