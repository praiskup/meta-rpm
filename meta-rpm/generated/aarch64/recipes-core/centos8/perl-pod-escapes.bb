SUMMARY = "generated recipe based on perl-Pod-Escapes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Escapes = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Pod-Escapes-1.07-395.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Escapes.sha256sum] = "545cd23ad8e4f71a5109551093668fd4b5e1a50d6a60364ce0f04f64eecd99d1"
