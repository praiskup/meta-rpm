SUMMARY = "generated recipe based on spice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "celt051 cyrus-sasl-lib glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libcacard libjpeg-turbo lz4 openssl opus orc pixman pkgconfig-native spice-protocol zlib"
RPM_SONAME_PROV_spice-server = "libspice-server.so.1"
RPM_SONAME_REQ_spice-server = "ld-linux-aarch64.so.1 libc.so.6 libcelt051.so.0 libcrypto.so.1.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libjpeg.so.62 liblz4.so.1 libm.so.6 libopus.so.0 liborc-0.4.so.0 libpixman-1.so.0 libpthread.so.0 librt.so.1 libsasl2.so.3 libssl.so.1.1 libz.so.1"
RDEPENDS_spice-server = "celt051 cyrus-sasl-lib glib2 glibc gstreamer1 gstreamer1-plugins-base libjpeg-turbo lz4-libs openssl-libs opus orc pixman zlib"
RPM_SONAME_REQ_spice-server-devel = "libspice-server.so.1"
RPROVIDES_spice-server-devel = "spice-server-dev (= 0.14.2)"
RDEPENDS_spice-server-devel = "glib2-devel libcacard-devel openssl-devel pixman-devel pkgconf-pkg-config spice-protocol spice-server"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-server-0.14.2-1.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/spice-server-devel-0.14.2-1.el8_2.1.aarch64.rpm \
          "

SRC_URI[spice-server.sha256sum] = "7f36ee303866640b813df64545a858a1a62ca0eed0001a3857fe8e0d7255c7cc"
SRC_URI[spice-server-devel.sha256sum] = "a98d9418bcf351e95e4c90b2f875f7860222d8319cf519cc95fee1119de95c08"
