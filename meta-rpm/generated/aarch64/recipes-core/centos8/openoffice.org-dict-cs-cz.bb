SUMMARY = "generated recipe based on openoffice.org-dict-cs_CZ srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-cs = "hunspell"
RDEPENDS_hyphen-cs = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-cs-20080822-8.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-cs-20080822-8.el8.noarch.rpm \
          "

SRC_URI[hunspell-cs.sha256sum] = "e0d8398048fd87145c7a563b99772adee2a082b5aac07993a695dff99bb1cb93"
SRC_URI[hyphen-cs.sha256sum] = "5581d99a60ae2c760a8126dfa81417f74d256c744ea2bdb3674619acbd81fd76"
