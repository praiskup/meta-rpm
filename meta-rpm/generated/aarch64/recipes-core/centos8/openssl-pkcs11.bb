SUMMARY = "generated recipe based on openssl-pkcs11 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_openssl-pkcs11 = "libp11.so.3"
RPM_SONAME_REQ_openssl-pkcs11 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2"
RDEPENDS_openssl-pkcs11 = "glibc openssl openssl-libs p11-kit-trust"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-pkcs11-0.4.10-2.el8.aarch64.rpm \
          "

SRC_URI[openssl-pkcs11.sha256sum] = "f758b3e76f41ecb5340e7def046acd9f91242ebe7060ad2d509381584075ead8"
