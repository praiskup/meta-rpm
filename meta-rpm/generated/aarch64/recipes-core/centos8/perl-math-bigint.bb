SUMMARY = "generated recipe based on perl-Math-BigInt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Math-BigInt = "perl-Carp perl-Exporter perl-Math-Complex perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Math-BigInt-1.9998.11-7.el8.noarch.rpm \
          "

SRC_URI[perl-Math-BigInt.sha256sum] = "1d1025a6011d08616a6d61722d5f027662ca9f49c67a55db4b9218050538276d"
