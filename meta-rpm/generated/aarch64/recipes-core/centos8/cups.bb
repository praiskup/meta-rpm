SUMMARY = "generated recipe based on cups srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl avahi-libs cups-libs dbus-libs e2fsprogs gnutls krb5-libs libgcc libusb1 libxcrypt pam pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_cups = "ld-linux-aarch64.so.1 libacl.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupscgi.so.1 libcupsimage.so.2 libcupsmime.so.1 libcupsppdc.so.1 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpam.so.0 libpthread.so.0 libstdc++.so.6 libsystemd.so.0 libusb-1.0.so.0 libz.so.1"
RDEPENDS_cups = "acl avahi-libs bash cups-client cups-filesystem cups-filters cups-libs dbus dbus-libs glibc gnutls grep krb5-libs libacl libcom_err libgcc libstdc++ libusbx libxcrypt pam sed systemd systemd-libs zlib"
RPM_SONAME_REQ_cups-client = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_cups-client = "avahi-libs bash chkconfig cups-libs glibc gnutls krb5-libs libcom_err libxcrypt zlib"
RPM_SONAME_REQ_cups-ipptool = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_cups-ipptool = "avahi-libs cups-libs glibc gnutls krb5-libs libcom_err libxcrypt zlib"
RPM_SONAME_REQ_cups-lpd = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_cups-lpd = "avahi-libs bash cups cups-libs glibc gnutls krb5-libs libcom_err libxcrypt zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-2.2.6-33.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-client-2.2.6-33.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-filesystem-2.2.6-33.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-ipptool-2.2.6-33.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cups-lpd-2.2.6-33.el8.aarch64.rpm \
          "

SRC_URI[cups.sha256sum] = "13b432dee5d507a0822d9a8e2323af3fbb2b1c7798e67cfc37a17c1392955a17"
SRC_URI[cups-client.sha256sum] = "ddb74da504a8001af48d3054f48a2e9ba81cd24d8d67f80eb459fcd3c758be1d"
SRC_URI[cups-filesystem.sha256sum] = "d3de38fb431b6e3525bb5314983f6df75a9109785ee7895a5377cbcb900e6a2a"
SRC_URI[cups-ipptool.sha256sum] = "29c0ebc1e16a97f0adbcee213312aa31925a6c5b79dd457a1a5710cc33ab0152"
SRC_URI[cups-lpd.sha256sum] = "cc8e40ae57ec989a571d11da8cb27ccb727ac50dc93d4773e02f026a654119ea"
