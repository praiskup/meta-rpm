SUMMARY = "generated recipe based on gnome-logs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native systemd-libs"
RPM_SONAME_REQ_gnome-logs = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_gnome-logs = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gsettings-desktop-schemas gtk3 pango systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-logs-3.28.5-3.el8.aarch64.rpm \
          "

SRC_URI[gnome-logs.sha256sum] = "b628794635b09071a7282801aad0b5ce81c3170de5e81daac761b83cd6fc51f6"
