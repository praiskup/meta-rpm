SUMMARY = "generated recipe based on postfix srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib db icu libnsl2 libpcre libpq mariadb-connector-c openldap openssl pkgconfig-native sqlite3 tinycdb"
RPM_SONAME_REQ_postfix = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libnsl.so.2 libresolv.so.2 libsasl2.so.3 libssl.so.1.1"
RDEPENDS_postfix = "bash chkconfig cyrus-sasl-lib diffutils glibc hostname libdb libicu libnsl2 openssl openssl-libs shadow-utils systemd"
RPM_SONAME_REQ_postfix-cdb = "ld-linux-aarch64.so.1 libc.so.6 libcdb.so.1"
RDEPENDS_postfix-cdb = "glibc postfix tinycdb"
RPM_SONAME_REQ_postfix-ldap = "ld-linux-aarch64.so.1 libc.so.6 liblber-2.4.so.2 libldap-2.4.so.2"
RDEPENDS_postfix-ldap = "glibc openldap postfix"
RPM_SONAME_REQ_postfix-mysql = "libc.so.6 libm.so.6 libmariadb.so.3"
RDEPENDS_postfix-mysql = "glibc mariadb-connector-c postfix"
RPM_SONAME_REQ_postfix-pcre = "ld-linux-aarch64.so.1 libc.so.6 libpcre.so.1"
RDEPENDS_postfix-pcre = "glibc pcre postfix"
RDEPENDS_postfix-perl-scripts = "perl-Date-Calc perl-Getopt-Long perl-IO perl-interpreter perl-libs postfix"
RPM_SONAME_REQ_postfix-pgsql = "ld-linux-aarch64.so.1 libc.so.6 libpq.so.5"
RDEPENDS_postfix-pgsql = "glibc libpq postfix"
RPM_SONAME_REQ_postfix-sqlite = "ld-linux-aarch64.so.1 libc.so.6 libsqlite3.so.0"
RDEPENDS_postfix-sqlite = "glibc postfix sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-cdb-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-ldap-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-mysql-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-pcre-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-perl-scripts-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-pgsql-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postfix-sqlite-3.3.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/postfix-3.3.1-12.el8.aarch64.rpm \
          "

SRC_URI[postfix.sha256sum] = "fd38ebe7ef3c2977ae3a74d36def530cfdde2d1f6970930fc9d9758b88b82152"
SRC_URI[postfix-cdb.sha256sum] = "58e3b6b3e3dba71cf4231a0b583f8715e25f49425bdab2e32ed0d452e104c025"
SRC_URI[postfix-ldap.sha256sum] = "0ece30be380fbdec6a3fba805c7363839e8d0948b48c3ed4af1d6864e801d3dd"
SRC_URI[postfix-mysql.sha256sum] = "65c826b0ec91cfbb5dc2a399d4245a6ec80725858f9388626b2a29343469227b"
SRC_URI[postfix-pcre.sha256sum] = "d89c9099f8a8e4cdcd483038569b61f54e1c01d40d16cc0219870aa53aa10c5b"
SRC_URI[postfix-perl-scripts.sha256sum] = "eb934447ccf92e4944aeb250cf44e96113452aa2053b384fa9b2b5d7762a6c60"
SRC_URI[postfix-pgsql.sha256sum] = "d474f769be0381b62fc92d279e56e210d5b9898034a238b7ff36b89a22845372"
SRC_URI[postfix-sqlite.sha256sum] = "24dce6de51a41c250bd65115c47dc3a1795fa4264c6e5dd903eb402da24a7220"
