SUMMARY = "generated recipe based on gcab srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_libgcab1 = "libgcab-1.0.so.0"
RPM_SONAME_REQ_libgcab1 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libz.so.1"
RDEPENDS_libgcab1 = "glib2 glibc libgcc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgcab1-1.1-1.el8.aarch64.rpm \
          "

SRC_URI[libgcab1.sha256sum] = "2b631d5095ee3489538ca00499ac65e2815ace2d1feed0996cbbab54762b2fa8"
