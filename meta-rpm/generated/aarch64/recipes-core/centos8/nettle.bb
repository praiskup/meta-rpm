SUMMARY = "generated recipe based on nettle srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp pkgconfig-native"
RPM_SONAME_PROV_nettle = "libhogweed.so.4 libnettle.so.6"
RPM_SONAME_REQ_nettle = "ld-linux-aarch64.so.1 libc.so.6 libgmp.so.10 libnettle.so.6"
RDEPENDS_nettle = "bash glibc gmp info"
RPM_SONAME_REQ_nettle-devel = "libhogweed.so.4 libnettle.so.6"
RPROVIDES_nettle-devel = "nettle-dev (= 3.4.1)"
RDEPENDS_nettle-devel = "gmp-devel nettle pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nettle-devel-3.4.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nettle-3.4.1-1.el8.aarch64.rpm \
          "

SRC_URI[nettle.sha256sum] = "4d39ab38529ac4ef18c984de994a170822ba06cecd019d3847ee388f92c55ac8"
SRC_URI[nettle-devel.sha256sum] = "27fb346f376bc8c647ec45f36ccda7366013d282e16152390bf30dce996c58cb"
