SUMMARY = "generated recipe based on gnutls srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "autogen gmp libgcc libidn2 libtasn1 libunistring nettle p11-kit pkgconfig-native unbound"
RPM_SONAME_PROV_gnutls = "libgnutls.so.30"
RPM_SONAME_REQ_gnutls = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libhogweed.so.4 libidn2.so.0 libnettle.so.6 libp11-kit.so.0 libtasn1.so.6 libunistring.so.2"
RDEPENDS_gnutls = "crypto-policies glibc gmp libidn2 libtasn1 libunistring nettle p11-kit p11-kit-trust"
RPM_SONAME_PROV_gnutls-c++ = "libgnutlsxx.so.28"
RPM_SONAME_REQ_gnutls-c++ = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgnutls.so.30 libm.so.6 libstdc++.so.6"
RDEPENDS_gnutls-c++ = "glibc gnutls libgcc libstdc++"
RPM_SONAME_PROV_gnutls-dane = "libgnutls-dane.so.0"
RPM_SONAME_REQ_gnutls-dane = "ld-linux-aarch64.so.1 libc.so.6 libgnutls.so.30 libunbound.so.2"
RDEPENDS_gnutls-dane = "glibc gnutls unbound-libs"
RPM_SONAME_REQ_gnutls-devel = "libgnutls-dane.so.0 libgnutls.so.30 libgnutlsxx.so.28"
RPROVIDES_gnutls-devel = "gnutls-dev (= 3.6.8)"
RDEPENDS_gnutls-devel = "bash gnutls gnutls-c++ gnutls-dane info libtasn1-devel nettle-devel p11-kit-devel pkgconf-pkg-config"
RPM_SONAME_REQ_gnutls-utils = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libgnutls-dane.so.0 libgnutls.so.30 libhogweed.so.4 libidn2.so.0 libnettle.so.6 libopts.so.25 libp11-kit.so.0 libtasn1.so.6 libunistring.so.2"
RDEPENDS_gnutls-utils = "autogen-libopts glibc gmp gnutls gnutls-dane libidn2 libtasn1 libunistring nettle p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnutls-c++-3.6.8-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnutls-dane-3.6.8-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnutls-devel-3.6.8-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnutls-utils-3.6.8-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gnutls-3.6.8-11.el8_2.aarch64.rpm \
          "

SRC_URI[gnutls.sha256sum] = "aeab3873f101b77861a5b45983fe9e9183502863c6fde595beba9f6aa16a88f7"
SRC_URI[gnutls-c++.sha256sum] = "cc27919f6d07191a3c53542afdee09c3886ec7afc4f0785a9305dd337210ede8"
SRC_URI[gnutls-dane.sha256sum] = "40a1cfb81852a881ba5bebe9ec0fb2b5d2d5678f9e217c645946312b9d3d3ccc"
SRC_URI[gnutls-devel.sha256sum] = "b0471b3cd8582358d1b1d90b7dcc1f427babdc3454b18755e82f94bfc63278be"
SRC_URI[gnutls-utils.sha256sum] = "ffb6ea659f20916a7dbacf7d9cc063536a3df3ba4baa22f8ff407d7a03a772a3"
