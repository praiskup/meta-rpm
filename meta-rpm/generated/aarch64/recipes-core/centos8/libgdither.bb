SUMMARY = "generated recipe based on libgdither srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgdither = "libgdither.so.1"
RPM_SONAME_REQ_libgdither = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libgdither = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgdither-0.6-17.el8.aarch64.rpm \
          "

SRC_URI[libgdither.sha256sum] = "e19bb156ab03b9de3a049cf7ecfa788074857e3806b3b55b0a8ae89cd2ee8606"
