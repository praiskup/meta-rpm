SUMMARY = "generated recipe based on ed srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ed = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_ed = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ed-1.14.2-4.el8.aarch64.rpm \
          "

SRC_URI[ed.sha256sum] = "8da0d3933057098da9b1ab02205cee55380c9905314f24f64694952206de19e4"
