SUMMARY = "generated recipe based on gutenprint srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo cups-libs e2fsprogs fontconfig freetype gdk-pixbuf gimp glib-2.0 gtk2 krb5-libs libusb1 libxcrypt pango pkgconfig-native zlib"
RPM_SONAME_REQ_gutenprint = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgutenprint.so.2 libm.so.6"
RDEPENDS_gutenprint = "glibc gutenprint-libs"
RPM_SONAME_REQ_gutenprint-cups = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libdl.so.2 libgssapi_krb5.so.2 libgutenprint.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libusb-1.0.so.0 libz.so.1"
RDEPENDS_gutenprint-cups = "bash cups cups-libs glibc gutenprint gutenprint-libs krb5-libs libcom_err libusbx libxcrypt platform-python zlib"
RPM_SONAME_PROV_gutenprint-libs = "libgutenprint.so.2"
RPM_SONAME_REQ_gutenprint-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_gutenprint-libs = "glibc"
RPM_SONAME_PROV_gutenprint-libs-ui = "libgutenprintui2.so.1"
RPM_SONAME_REQ_gutenprint-libs-ui = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libgutenprint.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gutenprint-libs-ui = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 gutenprint-libs pango"
RPM_SONAME_REQ_gutenprint-plugin = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpui-2.0.so.0 libgimpwidgets-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libgutenprint.so.2 libgutenprintui2.so.1 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gutenprint-plugin = "atk cairo fontconfig freetype gdk-pixbuf2 gimp gimp-libs glib2 glibc gtk2 gutenprint gutenprint-libs gutenprint-libs-ui pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gutenprint-5.2.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gutenprint-cups-5.2.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gutenprint-doc-5.2.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gutenprint-libs-5.2.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gutenprint-libs-ui-5.2.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gutenprint-plugin-5.2.14-3.el8.aarch64.rpm \
          "

SRC_URI[gutenprint.sha256sum] = "9d67e466030a224bf1a14525fe4998d7c9fc2699e8bb4153c8a8fb272647d774"
SRC_URI[gutenprint-cups.sha256sum] = "f5efa77a4a877f85fa6dff720a3b5657b27c1d973c91df4a4b6bf90410772cd6"
SRC_URI[gutenprint-doc.sha256sum] = "2e0a368734fd194b1b7fa4b908c604e3d7a76179bd116a31f6ad32a0cd13e79d"
SRC_URI[gutenprint-libs.sha256sum] = "90c5ebef739bbe99397444401d009e1d7b6145a013ca07625c444eee9303571f"
SRC_URI[gutenprint-libs-ui.sha256sum] = "52fb0b3afdb4aeed5c487fec5c1b2e4b6b9f4fa6f96068bcdb9caf908fe649b5"
SRC_URI[gutenprint-plugin.sha256sum] = "eb66d8e7cf8b86ba7dd5e6f96a88cd9afd216b0f48abf8b4d611e89cd473d1c1"
