SUMMARY = "generated recipe based on libtool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "m4 pkgconfig-native"
RDEPENDS_libtool = "autoconf automake bash findutils gcc info sed tar"
RPM_SONAME_PROV_libtool-ltdl = "libltdl.so.7"
RPM_SONAME_REQ_libtool-ltdl = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libtool-ltdl = "glibc"
RPM_SONAME_REQ_libtool-ltdl-devel = "libltdl.so.7"
RPROVIDES_libtool-ltdl-devel = "libtool-ltdl-dev (= 2.4.6)"
RDEPENDS_libtool-ltdl-devel = "automake bash libtool-ltdl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtool-2.4.6-25.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtool-ltdl-devel-2.4.6-25.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtool-ltdl-2.4.6-25.el8.aarch64.rpm \
          "
SRC_URI = "file://libtool-2.4.6-25.el8.patch file://libtool-2.4.6-25.el8-sysroot.patch"

SRC_URI[libtool.sha256sum] = "27a9641276339d3659656957019ca7e072611d5d78bf7d2d223908b93e77b919"
SRC_URI[libtool-ltdl.sha256sum] = "0b45f752d477abd5e83d2c5966891b41a042e5f74c1e7c503a42ff5aba231c9b"
SRC_URI[libtool-ltdl-devel.sha256sum] = "ff6081c88dd00e3cf89dbf13cf561020af8f6d0ec3dd25d944b1d0936ae970e4"
