SUMMARY = "generated recipe based on speexdsp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_speexdsp = "libspeexdsp.so.1"
RPM_SONAME_REQ_speexdsp = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_speexdsp = "glibc"
RPM_SONAME_REQ_speexdsp-devel = "libspeexdsp.so.1"
RPROVIDES_speexdsp-devel = "speexdsp-dev (= 1.2)"
RDEPENDS_speexdsp-devel = "pkgconf-pkg-config speexdsp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/speexdsp-1.2-0.13.rc3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/speexdsp-devel-1.2-0.13.rc3.el8.aarch64.rpm \
          "

SRC_URI[speexdsp.sha256sum] = "849bc8585dc285fc3a2857fd1541f29c8f944ba42dd4166a10dc1b86d2877ac8"
SRC_URI[speexdsp-devel.sha256sum] = "7484f039f98485fabab7e1e4f360610ee44f98aee5bb5ccfc3dd9bfb37bee8b6"
