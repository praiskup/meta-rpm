SUMMARY = "generated recipe based on rubygem-rspec-mocks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec-mocks = "rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rubygem-rspec-mocks-3.7.0-4.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec-mocks.sha256sum] = "6db7ce942cc1a9b0340f0bae44d50ae293f99838a51deff80f4839985e07db88"
