SUMMARY = "generated recipe based on glib-networking srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gnutls libproxy p11-kit pkgconfig-native"
RPM_SONAME_PROV_glib-networking = "libgiognomeproxy.so libgiognutls.so libgiolibproxy.so"
RPM_SONAME_REQ_glib-networking = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libp11-kit.so.0 libproxy.so.1"
RDEPENDS_glib-networking = "ca-certificates glib2 glibc gnutls gsettings-desktop-schemas libproxy p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glib-networking-2.56.1-1.1.el8.aarch64.rpm \
          "

SRC_URI[glib-networking.sha256sum] = "aeb3b5ca60e55077ecf9da81d6bcb50a86b909808d7373f3f8a372dabcc1eedb"
