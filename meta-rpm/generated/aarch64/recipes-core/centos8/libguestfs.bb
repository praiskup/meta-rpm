SUMMARY = "generated recipe based on libguestfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fuse glib-2.0 jansson libconfig libgcc libpcre libselinux libtirpc libvirt libxcrypt libxml2 ncurses perl pkgconfig-native readline ruby xz"
RPM_SONAME_PROV_libguestfs = "libguestfs.so.0"
RPM_SONAME_REQ_libguestfs = "ld-linux-aarch64.so.1 libc.so.6 libfuse.so.2 libgcc_s.so.1 libguestfs.so.0 libjansson.so.4 libpcre.so.1 libpthread.so.0 libselinux.so.1 libtirpc.so.3 libvirt.so.0 libxml2.so.2"
RDEPENDS_libguestfs = "acl attr augeas-libs bash binutils bzip2 coreutils cpio cryptsetup dhcp-client diffutils dosfstools e2fsprogs edk2-aarch64 file findutils fuse fuse-libs gawk gdisk genisoimage glibc grep gzip hivex iproute jansson kernel kmod less libacl libcap libdb-utils libgcc libselinux libtirpc libvirt-daemon-kvm libvirt-libs libxml2 lsscsi lvm2 lzop mdadm parted pcre policycoreutils procps-ng psmisc qemu-img scrub sed squashfs-tools supermin systemd systemd-libs systemd-udev tar util-linux xz yajl"
RDEPENDS_libguestfs-bash-completion = "bash-completion libguestfs-tools-c"
RPM_SONAME_REQ_libguestfs-benchmarking = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libguestfs.so.0 libm.so.6 libpcre.so.1 libpthread.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_libguestfs-benchmarking = "glibc libgcc libguestfs libvirt-libs libxml2 pcre perl-Getopt-Long perl-Pod-Usage perl-interpreter perl-libs"
RPM_SONAME_REQ_libguestfs-devel = "libguestfs.so.0"
RPROVIDES_libguestfs-devel = "libguestfs-dev (= 1.38.4)"
RDEPENDS_libguestfs-devel = "bash libguestfs libguestfs-tools-c pkgconf-pkg-config xz"
RDEPENDS_libguestfs-gfs2 = "gfs2-utils libguestfs"
RPM_SONAME_PROV_libguestfs-gobject = "libguestfs-gobject-1.0.so.0"
RPM_SONAME_REQ_libguestfs-gobject = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libguestfs.so.0 libpthread.so.0"
RDEPENDS_libguestfs-gobject = "glib2 glibc libguestfs"
RPM_SONAME_REQ_libguestfs-gobject-devel = "libguestfs-gobject-1.0.so.0"
RPROVIDES_libguestfs-gobject-devel = "libguestfs-gobject-dev (= 1.38.4)"
RDEPENDS_libguestfs-gobject-devel = "glib2-devel libguestfs-devel libguestfs-gobject pkgconf-pkg-config"
RDEPENDS_libguestfs-inspect-icons = "icoutils libguestfs netpbm-progs"
RPM_SONAME_PROV_libguestfs-java = "libguestfs_jni.so.1"
RPM_SONAME_REQ_libguestfs-java = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libguestfs.so.0 libpthread.so.0"
RDEPENDS_libguestfs-java = "glibc java-1.8.0-openjdk-headless javapackages-tools libgcc libguestfs"
RPM_SONAME_REQ_libguestfs-java-devel = "libguestfs_jni.so.1"
RPROVIDES_libguestfs-java-devel = "libguestfs-java-dev (= 1.38.4)"
RDEPENDS_libguestfs-java-devel = "libguestfs libguestfs-java"
RDEPENDS_libguestfs-javadoc = "javapackages-tools libguestfs libguestfs-java"
RDEPENDS_libguestfs-man-pages-ja = "libguestfs"
RDEPENDS_libguestfs-man-pages-uk = "libguestfs"
RDEPENDS_libguestfs-rescue = "iputils libguestfs-tools-c lsof openssh-clients pciutils strace vim-minimal"
RDEPENDS_libguestfs-rsync = "libguestfs rsync"
RDEPENDS_libguestfs-tools = "libguestfs libguestfs-tools-c perl-File-Temp perl-Getopt-Long perl-Pod-Usage perl-Sys-Guestfs perl-Sys-Virt perl-hivex perl-interpreter perl-libintl-perl perl-libs"
RPM_SONAME_REQ_libguestfs-tools-c = "ld-linux-aarch64.so.1 libc.so.6 libconfig.so.9 libcrypt.so.1 libdl.so.2 libfuse.so.2 libgcc_s.so.1 libguestfs.so.0 libjansson.so.4 liblzma.so.5 libm.so.6 libncurses.so.6 libpcre.so.1 libpthread.so.0 libreadline.so.7 libtinfo.so.6 libtirpc.so.3 libvirt.so.0 libxml2.so.2"
RDEPENDS_libguestfs-tools-c = "bash curl fuse-libs glibc gnupg2 hexedit jansson less libconfig libgcc libguestfs libtirpc libvirt-libs libxcrypt libxml2 man-db ncurses-libs pcre readline vim-minimal xz xz-libs"
RDEPENDS_libguestfs-xfs = "libguestfs xfsprogs"
RPM_SONAME_PROV_lua-guestfs = "libluaguestfs.so"
RPM_SONAME_REQ_lua-guestfs = "ld-linux-aarch64.so.1 libc.so.6 libguestfs.so.0 libpthread.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_lua-guestfs = "glibc libguestfs libvirt-libs libxml2 lua"
RPM_SONAME_REQ_ocaml-libguestfs = "ld-linux-aarch64.so.1 libc.so.6 libguestfs.so.0"
RDEPENDS_ocaml-libguestfs = "glibc libguestfs ocaml-runtime"
RPROVIDES_ocaml-libguestfs-devel = "ocaml-libguestfs-dev (= 1.38.4)"
RDEPENDS_ocaml-libguestfs-devel = "ocaml-libguestfs"
RPM_SONAME_REQ_perl-Sys-Guestfs = "ld-linux-aarch64.so.1 libc.so.6 libguestfs.so.0 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-Guestfs = "glibc libguestfs perl-interpreter perl-libs"
RPM_SONAME_PROV_python3-libguestfs = "libguestfsmod.cpython-36m-aarch64-linux-gnu.so"
RPM_SONAME_REQ_python3-libguestfs = "ld-linux-aarch64.so.1 libc.so.6 libguestfs.so.0 libpthread.so.0"
RDEPENDS_python3-libguestfs = "glibc libguestfs platform-python"
RPM_SONAME_REQ_ruby-libguestfs = "ld-linux-aarch64.so.1 libc.so.6 libguestfs.so.0 libruby.so.2.5"
RDEPENDS_ruby-libguestfs = "glibc libguestfs ruby ruby-libs"
RPM_SONAME_REQ_virt-dib = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libguestfs.so.0 libm.so.6 libpcre.so.1 libpthread.so.0 libxml2.so.2"
RDEPENDS_virt-dib = "curl glibc kpartx libgcc libguestfs libxml2 pcre qemu-img which"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-bash-completion-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-benchmarking-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-devel-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-gfs2-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-gobject-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-gobject-devel-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-inspect-icons-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-java-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-java-devel-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-javadoc-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-man-pages-ja-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-man-pages-uk-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-rescue-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-rsync-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-tools-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-tools-c-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-xfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lua-guestfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Sys-Guestfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ruby-libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/virt-dib-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-libguestfs-devel-1.38.4-15.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[libguestfs.sha256sum] = "84ef4fa0dfb503d5c04efe2172966f8f85d4efa12c17b573ace80b75303f64a4"
SRC_URI[libguestfs-bash-completion.sha256sum] = "5582a8d5f2daf51aae560a6192542b3dace22921baa89a3f7ebb0847d04f23a5"
SRC_URI[libguestfs-benchmarking.sha256sum] = "387900494ad28f900499b864cea0fa6ed51f1a5578813c9265701debcdb3e4c2"
SRC_URI[libguestfs-devel.sha256sum] = "0dc172b819156c203973f6802d31eef7939c4d52ab389449979b32c424c20a41"
SRC_URI[libguestfs-gfs2.sha256sum] = "9bbac57cb8dd16a6a15ee5e540e94ddc2afcad3acf70ac76ed8a8ca7baf81237"
SRC_URI[libguestfs-gobject.sha256sum] = "ee10b7b662fcd822c9795897539064e49b1ed300ed28e7dbbeebec0313a42708"
SRC_URI[libguestfs-gobject-devel.sha256sum] = "9acb53f645e2ad7da789561ad1efee0061e88f02b6215f3f107da8b3ae097607"
SRC_URI[libguestfs-inspect-icons.sha256sum] = "a0ba3f814b6d8c684ca3437393ec11e313e99251995fd2fddd48e7bd48bc5541"
SRC_URI[libguestfs-java.sha256sum] = "3ad194b7ab3425b0577dea9a620bc98c6948bb77d8c760cf17b7e76a9bab4ae9"
SRC_URI[libguestfs-java-devel.sha256sum] = "945c989bafd25ba9817f3eb3edefdb63f49cc17cfb41f66dcc1f7e8042002fe8"
SRC_URI[libguestfs-javadoc.sha256sum] = "199803c27fa9aad06df934d2589a4436ce8ca88fc2aed99b6573401ce9f020a0"
SRC_URI[libguestfs-man-pages-ja.sha256sum] = "2cbef59a05cae1fc6aa338fba86ec1a689218ada63ccdacd4111d1c800be336b"
SRC_URI[libguestfs-man-pages-uk.sha256sum] = "21eb020d66726ebbaa9811e1eb273d945873bfadc11799cfbcaa571c61835c1c"
SRC_URI[libguestfs-rescue.sha256sum] = "eca0932a3836f19b93578a7150b7440ea2ceb6213daed94de44c5c7f7635ba10"
SRC_URI[libguestfs-rsync.sha256sum] = "94652e75a12a967a74ac9fb420a72cdd9b30135b8e88e188ebe1833bfb090811"
SRC_URI[libguestfs-tools.sha256sum] = "5b89187a857e342ea6974a35951a018b2e04ed64775d128094bb6b2e7e6f7b1c"
SRC_URI[libguestfs-tools-c.sha256sum] = "af14c8e0ffb52837c4fd83c990757ca47538284123ab081711353c86aae4ccf9"
SRC_URI[libguestfs-xfs.sha256sum] = "fe786017586e7780f62149f9d22d4d3044dc9d171b691d7a9ad67471bc79280b"
SRC_URI[lua-guestfs.sha256sum] = "f336af631a6a7f6862282975c92a994db8458d66c8bdb7ebf4ab1ba944991611"
SRC_URI[ocaml-libguestfs.sha256sum] = "20113e355ec4b52494b0ce21cdb7d8f0919065494405bd2162a862ba721050b3"
SRC_URI[ocaml-libguestfs-devel.sha256sum] = "94e325f9a981785b5d79b40ab01674fd18e08a06f1bd2bb234888ffe4b91cfd6"
SRC_URI[perl-Sys-Guestfs.sha256sum] = "5595c8727b2562c353f7a06a7bc5b7aa1a45cac2c54d2d937d69299bc97e194f"
SRC_URI[python3-libguestfs.sha256sum] = "d758532fa9e2bc2cf83cd294ca11a6ecfcb6bf94643be55a0d5e1b739b3ec7d5"
SRC_URI[ruby-libguestfs.sha256sum] = "fa4ef75e9926e23b888be7310234a6a9715d1a4d130bbb3a9d18d36d6c393d41"
SRC_URI[virt-dib.sha256sum] = "de83867251838634226f6719195112003e2680cc15fb9f4afa765879e0be84a3"
