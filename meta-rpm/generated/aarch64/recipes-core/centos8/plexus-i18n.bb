SUMMARY = "generated recipe based on plexus-i18n srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-i18n = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default plexus-utils"
RDEPENDS_plexus-i18n-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-i18n-1.0-0.11.b10.4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-i18n-javadoc-1.0-0.11.b10.4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-i18n.sha256sum] = "6d6e97ebcbb1a99b4d72f4ad41dcfcaa09fa714c342e828af14ecb4ae9c76a2e"
SRC_URI[plexus-i18n-javadoc.sha256sum] = "95e1ccc507de52e3b7038113b71312d7d094fa7c4673644fe284ff61ec81029f"
