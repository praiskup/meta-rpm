SUMMARY = "generated recipe based on nkf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_nkf = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_nkf = "glibc"
RPM_SONAME_REQ_perl-NKF = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-NKF = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/nkf-2.1.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-NKF-2.1.4-8.el8.aarch64.rpm \
          "

SRC_URI[nkf.sha256sum] = "81568142b54da7716a853ffa1f0e78e04719330f4f55f253f65e01fbcc3949df"
SRC_URI[perl-NKF.sha256sum] = "75b129972abd722ee1eea4d9c41cb5897f36edd1bb6798b6ad698051312f6b43"
