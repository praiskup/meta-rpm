SUMMARY = "generated recipe based on hunspell-hsb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hsb = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-hsb-0.20060327.3-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-hsb.sha256sum] = "17dcb83131672baaed5b91038adb65dca9ca189846ea7a48225dd043955388c3"
