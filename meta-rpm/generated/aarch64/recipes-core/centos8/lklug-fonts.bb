SUMMARY = "generated recipe based on lklug-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lklug-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lklug-fonts-0.6-17.20090803cvs.el8.noarch.rpm \
          "

SRC_URI[lklug-fonts.sha256sum] = "2dbe25ed9f3df5fd8723f5a460f9957d80966287cc930e2b8b05ed2fddc90ba3"
