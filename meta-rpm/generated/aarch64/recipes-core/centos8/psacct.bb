SUMMARY = "generated recipe based on psacct srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_psacct = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_psacct = "bash chkconfig coreutils glibc info systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/psacct-6.6.3-4.el8.aarch64.rpm \
          "

SRC_URI[psacct.sha256sum] = "d9bd5cbd70eb715511c99ec937152424a0785c7cca037ed160ede72f114506f0"
