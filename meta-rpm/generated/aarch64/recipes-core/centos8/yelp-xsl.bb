SUMMARY = "generated recipe based on yelp-xsl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/yelp-xsl-3.28.0-2.el8.noarch.rpm \
          "

SRC_URI[yelp-xsl.sha256sum] = "bb949bd5467f7c59fbb4690035f941f47facadd8bd5378ac931c98099d059e03"
