SUMMARY = "generated recipe based on perl-Sys-Syslog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-Syslog = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-Syslog = "glibc perl-Carp perl-Exporter perl-Socket perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Sys-Syslog-0.35-397.el8.aarch64.rpm \
          "

SRC_URI[perl-Sys-Syslog.sha256sum] = "5de97a27af7e36b66df58725af46b31640cff19c5f4320b23bceddc60bdb70cc"
