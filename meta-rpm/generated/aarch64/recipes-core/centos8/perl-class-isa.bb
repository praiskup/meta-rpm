SUMMARY = "generated recipe based on perl-Class-ISA srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-ISA = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Class-ISA-0.36-1022.el8.noarch.rpm \
          "

SRC_URI[perl-Class-ISA.sha256sum] = "05b990cae721cfe078bd0434e4a4dba5a219c25d6f87b43f1d006bbb351eb51c"
