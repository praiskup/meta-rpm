SUMMARY = "generated recipe based on rhel-system-roles srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rhel-system-roles = "bash platform-python python3-jmespath"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rhel-system-roles-1.0-10.el8_1.noarch.rpm \
          "

SRC_URI[rhel-system-roles.sha256sum] = "3730ea6d8a038b0dcad96c2ba13bbc7946a6b944d164c98e49ae1e93175d0e0f"
