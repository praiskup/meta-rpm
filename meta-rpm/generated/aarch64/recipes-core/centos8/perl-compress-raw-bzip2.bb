SUMMARY = "generated recipe based on perl-Compress-Raw-Bzip2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 perl pkgconfig-native"
RPM_SONAME_REQ_perl-Compress-Raw-Bzip2 = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Compress-Raw-Bzip2 = "bzip2-libs glibc perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Compress-Raw-Bzip2-2.081-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Compress-Raw-Bzip2.sha256sum] = "27aa5c77963440c9cd577f5fc4afd133564c8eff6ed8f708160c86468438c2c0"
