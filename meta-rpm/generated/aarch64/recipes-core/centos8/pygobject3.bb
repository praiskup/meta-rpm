SUMMARY = "generated recipe based on pygobject3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo glib-2.0 gobject-introspection libffi pkgconfig-native"
RPROVIDES_pygobject3-devel = "pygobject3-dev (= 3.28.3)"
RDEPENDS_pygobject3-devel = "glib2-devel gobject-introspection-devel libffi-devel pkgconf-pkg-config python3-gobject"
RPM_SONAME_REQ_python3-gobject = "ld-linux-aarch64.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgirepository-1.0.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_python3-gobject = "cairo cairo-gobject glib2 glibc gobject-introspection platform-python python3-cairo python3-gobject-base"
RPM_SONAME_REQ_python3-gobject-base = "ld-linux-aarch64.so.1 libc.so.6 libffi.so.6 libgirepository-1.0.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_python3-gobject-base = "glib2 glibc gobject-introspection libffi platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-gobject-3.28.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-gobject-base-3.28.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pygobject3-devel-3.28.3-1.el8.aarch64.rpm \
          "

SRC_URI[pygobject3-devel.sha256sum] = "e5317234f334d7848a98ece38790de8c723ee44e25eb5b3026b279c2cb2b15b9"
SRC_URI[python3-gobject.sha256sum] = "6299c46d95e3dcf38f135a3cf51f412b1fdc7b03f2bdb92e79410a52cb437905"
SRC_URI[python3-gobject-base.sha256sum] = "ec8aa634bc4c3de87a638b609bd8aa94bcf7f06b9d532ac1eb8f9a5032f8276e"
