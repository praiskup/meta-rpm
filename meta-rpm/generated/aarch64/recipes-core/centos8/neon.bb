SUMMARY = "generated recipe based on neon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs expat krb5-libs libproxy openssl pakchois pkgconfig-native zlib"
RPM_SONAME_PROV_neon = "libneon.so.27"
RPM_SONAME_REQ_neon = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libexpat.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpakchois.so.0 libproxy.so.1 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_neon = "ca-certificates expat glibc krb5-libs libcom_err libproxy openssl-libs pakchois zlib"
RPM_SONAME_REQ_neon-devel = "libneon.so.27"
RPROVIDES_neon-devel = "neon-dev (= 0.30.2)"
RDEPENDS_neon-devel = "bash expat-devel neon openssl-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/neon-0.30.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/neon-devel-0.30.2-6.el8.aarch64.rpm \
          "

SRC_URI[neon.sha256sum] = "56bb826d77e2561899c06cb588e9cc80af27e1391f3589745ea5fef8925a1ecc"
SRC_URI[neon-devel.sha256sum] = "4a9637293039f656a1980ce78e55a0e9dd658f04624923c2e82627d090bec6d9"
