SUMMARY = "generated recipe based on python-pymongo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-bson = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-bson = "glibc platform-python python3-libs"
RPM_SONAME_REQ_python3-pymongo = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pymongo = "glibc platform-python python3-bson python3-libs"
RDEPENDS_python3-pymongo-gridfs = "platform-python python3-pymongo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python-pymongo-doc-3.6.1-11.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-bson-3.6.1-11.module_el8.1.0+245+c39af44f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pymongo-3.6.1-11.module_el8.1.0+245+c39af44f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pymongo-gridfs-3.6.1-11.module_el8.1.0+245+c39af44f.aarch64.rpm \
          "

SRC_URI[python-pymongo-doc.sha256sum] = "bdbe6138d3b5e266be82dc935a32d4a80eb85849c5b035cd5a3de410d0227fea"
SRC_URI[python3-bson.sha256sum] = "4256bffaf30bb6243fb7e231bcf19975099814406fc7c8dd0fdb2214e94e30c0"
SRC_URI[python3-pymongo.sha256sum] = "11a442778645e0e988a6b70dd727dedbd20f945735d6a4c31793f557d0e8a63f"
SRC_URI[python3-pymongo-gridfs.sha256sum] = "45c16663da3d98574298de333000a7afbb5525dde6d564e61bcd9859c23b38ff"
