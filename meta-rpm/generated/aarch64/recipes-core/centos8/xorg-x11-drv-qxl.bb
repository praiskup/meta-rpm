SUMMARY = "generated recipe based on xorg-x11-drv-qxl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxfont2 pkgconfig-native spice systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-qxl = "ld-linux-aarch64.so.1 libc.so.6 libudev.so.1"
RDEPENDS_xorg-x11-drv-qxl = "glibc systemd-libs xorg-x11-server-Xorg"
RPM_SONAME_REQ_xorg-x11-server-Xspice = "ld-linux-aarch64.so.1 libXfont2.so.2 libc.so.6 libpthread.so.0 libspice-server.so.1"
RDEPENDS_xorg-x11-server-Xspice = "glibc libXfont2 platform-python spice-server xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-qxl-0.1.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-Xspice-0.1.5-11.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-qxl.sha256sum] = "5748a3e5df5c419eba9cc40251a5a74c8751be3670486e887610226805c5411d"
SRC_URI[xorg-x11-server-Xspice.sha256sum] = "a4981ff320f8f335c7241a520842496ec1746281d1132950179274ed924bd954"
