SUMMARY = "generated recipe based on sysstat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lm-sensors pkgconfig-native"
RPM_SONAME_REQ_sysstat = "ld-linux-aarch64.so.1 libc.so.6 libsensors.so.4"
RDEPENDS_sysstat = "bash findutils glibc lm_sensors-libs systemd xz"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sysstat-11.7.3-2.el8.aarch64.rpm \
          "

SRC_URI[sysstat.sha256sum] = "b19e45d64e9b9f9b9557905ad695df8088d578ec9a74236402588065068cf707"
