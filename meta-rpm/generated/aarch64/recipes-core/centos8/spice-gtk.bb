SUMMARY = "generated recipe based on spice-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl atk cairo celt051 cyrus-sasl-lib gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 json-glib libcacard libepoxy libjpeg-turbo libusb1 libx11 lz4 openssl opus pango pixman pkgconfig-native polkit pulseaudio spice-protocol usbredir zlib"
RPM_SONAME_PROV_spice-glib = "libspice-client-glib-2.0.so.8"
RPM_SONAME_REQ_spice-glib = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libcacard.so.0 libcelt051.so.0 libcrypto.so.1.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 liblz4.so.1 libm.so.6 libopus.so.0 libpixman-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsasl2.so.3 libssl.so.1.1 libusb-1.0.so.0 libusbredirhost.so.1 libusbredirparser.so.1 libz.so.1"
RDEPENDS_spice-glib = "celt051 cyrus-sasl-lib glib2 glibc gstreamer1 gstreamer1-plugins-base json-glib libacl libcacard libjpeg-turbo libusbx lz4-libs openssl-libs opus pixman polkit-libs pulseaudio-libs pulseaudio-libs-glib2 usbredir zlib"
RPM_SONAME_REQ_spice-glib-devel = "libspice-client-glib-2.0.so.8"
RPROVIDES_spice-glib-devel = "spice-glib-dev (= 0.37)"
RDEPENDS_spice-glib-devel = "glib2-devel openssl-devel pixman-devel pkgconf-pkg-config spice-glib spice-protocol"
RDEPENDS_spice-gtk = "spice-glib"
RPM_SONAME_REQ_spice-gtk-tools = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libc.so.6 libcacard.so.0 libcairo-gobject.so.2 libcairo.so.2 libcelt051.so.0 libcrypto.so.1.1 libepoxy.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 liblz4.so.1 libm.so.6 libopus.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpixman-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsasl2.so.3 libspice-client-glib-2.0.so.8 libspice-client-gtk-3.0.so.5 libssl.so.1.1 libusb-1.0.so.0 libusbredirhost.so.1 libusbredirparser.so.1 libz.so.1"
RDEPENDS_spice-gtk-tools = "atk cairo cairo-gobject celt051 cyrus-sasl-lib gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 json-glib libX11 libcacard libepoxy libjpeg-turbo libusbx lz4-libs openssl-libs opus pango pixman pulseaudio-libs pulseaudio-libs-glib2 spice-glib spice-gtk3 usbredir zlib"
RPM_SONAME_PROV_spice-gtk3 = "libspice-client-gtk-3.0.so.5"
RPM_SONAME_REQ_spice-gtk3 = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libc.so.6 libcacard.so.0 libcairo-gobject.so.2 libcairo.so.2 libcelt051.so.0 libcrypto.so.1.1 libepoxy.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 liblz4.so.1 libm.so.6 libopus.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpixman-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsasl2.so.3 libspice-client-glib-2.0.so.8 libssl.so.1.1 libusb-1.0.so.0 libusbredirhost.so.1 libusbredirparser.so.1 libz.so.1"
RDEPENDS_spice-gtk3 = "atk cairo cairo-gobject celt051 cyrus-sasl-lib gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 json-glib libX11 libcacard libepoxy libjpeg-turbo libusbx lz4-libs openssl-libs opus pango pixman pulseaudio-libs pulseaudio-libs-glib2 spice-glib usbredir zlib"
RPM_SONAME_REQ_spice-gtk3-devel = "libspice-client-gtk-3.0.so.5"
RPROVIDES_spice-gtk3-devel = "spice-gtk3-dev (= 0.37)"
RDEPENDS_spice-gtk3-devel = "gtk3-devel pkgconf-pkg-config spice-glib-devel spice-gtk3"
RDEPENDS_spice-gtk3-vala = "spice-gtk3 spice-gtk3-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-glib-0.37-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-glib-devel-0.37-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-gtk-0.37-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-gtk-tools-0.37-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-gtk3-0.37-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-gtk3-devel-0.37-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-gtk3-vala-0.37-1.el8_2.2.aarch64.rpm \
          "

SRC_URI[spice-glib.sha256sum] = "49b01ee1527727166e0fb2e791cff67a3357cfe5bf44df4b0b77db54a3347a56"
SRC_URI[spice-glib-devel.sha256sum] = "ec652d3b68216441cfea40bcee92fc35554d3c5e40d5261b689ccb89778f93aa"
SRC_URI[spice-gtk.sha256sum] = "280c82b88f3f3ce32245d2ce6066e8c5b44f144ad5cf3a90834a9baa7951bc0e"
SRC_URI[spice-gtk-tools.sha256sum] = "5065e73245f3ea9d454bf769ac614a03fb32ac588279b2f8e29eff7f05804ec6"
SRC_URI[spice-gtk3.sha256sum] = "9f373b917b896785dd8e7aa88c88350a7a62f8e3d6349347f224e4f156a9ee3d"
SRC_URI[spice-gtk3-devel.sha256sum] = "c195ee23e898a8b7eaf8b7e88fd5396293c8a4c728c7d4abe61efa69c0db452e"
SRC_URI[spice-gtk3-vala.sha256sum] = "a3d390ef380a054ea53f0f93f042049fb5065cc7ec4e99c4ecb7e7a29e9b080a"
