SUMMARY = "generated recipe based on libmusicbrainz5 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libxml2 neon pkgconfig-native"
RPM_SONAME_PROV_libmusicbrainz5 = "libmusicbrainz5.so.1"
RPM_SONAME_REQ_libmusicbrainz5 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libneon.so.27 libstdc++.so.6 libxml2.so.2"
RDEPENDS_libmusicbrainz5 = "glibc libgcc libstdc++ libxml2 neon"
RPM_SONAME_REQ_libmusicbrainz5-devel = "libmusicbrainz5.so.1"
RPROVIDES_libmusicbrainz5-devel = "libmusicbrainz5-dev (= 5.1.0)"
RDEPENDS_libmusicbrainz5-devel = "libmusicbrainz5 libxml2-devel neon-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmusicbrainz5-5.1.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmusicbrainz5-devel-5.1.0-10.el8.aarch64.rpm \
          "

SRC_URI[libmusicbrainz5.sha256sum] = "4b9fee002da72b5b8631bd22e647baefe7c08a6e8505137406132a9ed6750362"
SRC_URI[libmusicbrainz5-devel.sha256sum] = "d8bcdf33c0f4d169c520b9d7c6931ec6dbb410b30159e7d46e89f1acd18ac46b"
