SUMMARY = "generated recipe based on libinput srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevdev libwacom mtdev pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libinput = "libinput.so.10"
RPM_SONAME_REQ_libinput = "ld-linux-aarch64.so.1 libc.so.6 libevdev.so.2 libinput.so.10 libm.so.6 libmtdev.so.1 libudev.so.1 libwacom.so.2"
RDEPENDS_libinput = "bash glibc libevdev libwacom mtdev systemd-libs"
RPM_SONAME_REQ_libinput-devel = "libinput.so.10"
RPROVIDES_libinput-devel = "libinput-dev (= 1.14.3)"
RDEPENDS_libinput-devel = "libinput pkgconf-pkg-config"
RPM_SONAME_REQ_libinput-utils = "ld-linux-aarch64.so.1 libc.so.6 libevdev.so.2 libinput.so.10 libudev.so.1"
RDEPENDS_libinput-utils = "glibc libevdev libinput platform-python python3-evdev python3-pyudev systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libinput-1.14.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libinput-utils-1.14.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libinput-devel-1.14.3-1.el8.aarch64.rpm \
          "

SRC_URI[libinput.sha256sum] = "4b04e9e349c64edbc850acc3a31d2dcf76c2e69e555ac6d1ad59655942ce577b"
SRC_URI[libinput-devel.sha256sum] = "b447c3999ce1f02804da36e60eb15f312130a705343437c11b59fd785e387639"
SRC_URI[libinput-utils.sha256sum] = "792b120c5e64f77e146945d7ff02b5f20ddd04e63ee479bf61fc2047f65fdf8f"
