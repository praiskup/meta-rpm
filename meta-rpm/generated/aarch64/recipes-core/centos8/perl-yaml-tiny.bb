SUMMARY = "generated recipe based on perl-YAML-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-YAML-Tiny = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-YAML-Tiny-1.73-2.el8.noarch.rpm \
          "

SRC_URI[perl-YAML-Tiny.sha256sum] = "c3377ae1ef5a3f30342498a1bd00ea2cf107246b7c3d7560e01352bb95e78584"
