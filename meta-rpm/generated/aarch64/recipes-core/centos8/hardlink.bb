SUMMARY = "generated recipe based on hardlink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre2 pkgconfig-native"
RPM_SONAME_REQ_hardlink = "ld-linux-aarch64.so.1 libc.so.6 libpcre2-8.so.0"
RDEPENDS_hardlink = "glibc pcre2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/hardlink-1.3-6.el8.aarch64.rpm \
          "

SRC_URI[hardlink.sha256sum] = "0d6de2febd0e0ef4fa74eb8f3cffa1b194118e4b7bfe4d2010bf4903ce2c4096"
