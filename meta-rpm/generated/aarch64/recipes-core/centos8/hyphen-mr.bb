SUMMARY = "generated recipe based on hyphen-mr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-mr = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-mr-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-mr.sha256sum] = "3542ec1bd57272b335b6043da1cbe810ed52b45cefb1e644de3ba5eef0b923db"
