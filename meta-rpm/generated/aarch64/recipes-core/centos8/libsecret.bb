SUMMARY = "generated recipe based on libsecret srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcrypt libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libsecret = "libsecret-1.so.0"
RPM_SONAME_REQ_libsecret = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libpthread.so.0 libsecret-1.so.0"
RDEPENDS_libsecret = "glib2 glibc libgcrypt libgpg-error"
RPM_SONAME_REQ_libsecret-devel = "libsecret-1.so.0"
RPROVIDES_libsecret-devel = "libsecret-dev (= 0.18.6)"
RDEPENDS_libsecret-devel = "glib2-devel libsecret pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsecret-0.18.6-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsecret-devel-0.18.6-1.el8.aarch64.rpm \
          "

SRC_URI[libsecret.sha256sum] = "6665ea7ce8357d9678ed40a58981a554bf0b843b434c839755bc784aef6f2a85"
SRC_URI[libsecret-devel.sha256sum] = "08929c9fdf6f571cbaeb483b506b71ca16244ed44e248d49dfe4e05ded014122"
