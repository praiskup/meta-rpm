SUMMARY = "generated recipe based on hyphen-lt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-lt = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-lt-0.20100531-13.el8.noarch.rpm \
          "

SRC_URI[hyphen-lt.sha256sum] = "6641b51968bd8d62ee50f302042393d78c3c29fd793ed306f9f5bccd450982b1"
