SUMMARY = "generated recipe based on lockdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lockdev = "liblockdev.so.1"
RPM_SONAME_REQ_lockdev = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lockdev = "bash glibc shadow-utils systemd"
RPM_SONAME_REQ_lockdev-devel = "liblockdev.so.1"
RPROVIDES_lockdev-devel = "lockdev-dev (= 1.0.4)"
RDEPENDS_lockdev-devel = "lockdev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lockdev-1.0.4-0.28.20111007git.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lockdev-devel-1.0.4-0.28.20111007git.el8.aarch64.rpm \
          "

SRC_URI[lockdev.sha256sum] = "e39856881fbcdd7f3150172acea29cbcacbb9c83f7caf7849e631d8fd3922c6b"
SRC_URI[lockdev-devel.sha256sum] = "c87ad42a7df089fb6b77e8889058b51b5dc563855f7e70dcd4f253b99f4ebb26"
