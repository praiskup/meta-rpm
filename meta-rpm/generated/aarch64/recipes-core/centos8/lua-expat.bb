SUMMARY = "generated recipe based on lua-expat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat pkgconfig-native"
RPM_SONAME_REQ_lua-expat = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1"
RDEPENDS_lua-expat = "expat glibc lua-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lua-expat-1.3.0-12.el8.1.aarch64.rpm \
          "

SRC_URI[lua-expat.sha256sum] = "ad02e7d1cd8b0ca7a66720bd257107db3c005f81123e83781dcd4fe260954647"
