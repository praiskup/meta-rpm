SUMMARY = "generated recipe based on malaga-suomi-voikko srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/malaga-suomi-voikko-1.19-5.el8.aarch64.rpm \
          "

SRC_URI[malaga-suomi-voikko.sha256sum] = "6f168d0670447b079acef74e604c5f5cc1ee0f571fa50ede7139cdfbb31ea85a"
