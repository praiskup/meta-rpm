SUMMARY = "generated recipe based on libijs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libijs = "libijs-0.35.so"
RPM_SONAME_REQ_libijs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libijs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libijs-0.35-5.el8.aarch64.rpm \
          "

SRC_URI[libijs.sha256sum] = "b332f6f26ba72db474ec475671ca96c578a74a4c767bb47b96c56d540648bc81"
