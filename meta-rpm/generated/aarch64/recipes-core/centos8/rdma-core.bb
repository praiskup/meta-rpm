SUMMARY = "generated recipe based on rdma-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native systemd-libs"
RPM_SONAME_PROV_ibacm = "libibacmp.so"
RPM_SONAME_REQ_ibacm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libibumad.so.3 libibverbs.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libsystemd.so.0"
RDEPENDS_ibacm = "bash glibc libibumad libibverbs libnl3 rdma-core systemd systemd-libs"
RPM_SONAME_PROV_infiniband-diags = "libibmad.so.5 libibnetdisc.so.5"
RPM_SONAME_REQ_infiniband-diags = "ld-linux-aarch64.so.1 libc.so.6 libibmad.so.5 libibnetdisc.so.5 libibumad.so.3"
RDEPENDS_infiniband-diags = "bash glibc libibumad perl-interpreter"
RPM_SONAME_REQ_iwpmd = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libpthread.so.0 libsystemd.so.0"
RDEPENDS_iwpmd = "bash glibc libnl3 rdma-core systemd systemd-libs"
RPM_SONAME_PROV_libibumad = "libibumad.so.3"
RPM_SONAME_REQ_libibumad = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libibumad = "bash glibc rdma-core"
RPM_SONAME_PROV_libibverbs = "libbnxt_re-rdmav25.so libcxgb4-rdmav25.so libhfi1verbs-rdmav25.so libhns-rdmav25.so libi40iw-rdmav25.so libibverbs.so.1 libmlx4.so.1 libmlx5.so.1 libqedr-rdmav25.so librxe-rdmav25.so libsiw-rdmav25.so libvmw_pvrdma-rdmav25.so"
RPM_SONAME_REQ_libibverbs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libibverbs.so.1 libmlx4.so.1 libmlx5.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0"
RDEPENDS_libibverbs = "bash glibc libnl3 perl-interpreter rdma-core"
RPM_SONAME_REQ_libibverbs-utils = "ld-linux-aarch64.so.1 libc.so.6 libibverbs.so.1"
RDEPENDS_libibverbs-utils = "glibc libibverbs"
RPM_SONAME_PROV_librdmacm = "librdmacm.so.1 librspreload.so"
RPM_SONAME_REQ_librdmacm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libibverbs.so.1 libnl-3.so.200 libpthread.so.0 librdmacm.so.1"
RDEPENDS_librdmacm = "bash glibc libibverbs libnl3 rdma-core"
RPM_SONAME_REQ_librdmacm-utils = "ld-linux-aarch64.so.1 libc.so.6 libibverbs.so.1 libpthread.so.0 librdmacm.so.1"
RDEPENDS_librdmacm-utils = "glibc libibverbs librdmacm"
RPM_SONAME_REQ_rdma-core = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libsystemd.so.0 libudev.so.1"
RDEPENDS_rdma-core = "bash dracut glibc kmod libnl3 pciutils systemd systemd-libs"
RPM_SONAME_REQ_rdma-core-devel = "libibmad.so.5 libibnetdisc.so.5 libibumad.so.3 libibverbs.so.1 libmlx4.so.1 libmlx5.so.1 librdmacm.so.1"
RPROVIDES_rdma-core-devel = "rdma-core-dev (= 26.0)"
RDEPENDS_rdma-core-devel = "ibacm infiniband-diags libibumad libibverbs librdmacm pkgconf-pkg-config rdma-core"
RPM_SONAME_REQ_srp_daemon = "ld-linux-aarch64.so.1 libc.so.6 libibumad.so.3 libibverbs.so.1 libpthread.so.0"
RDEPENDS_srp_daemon = "bash glibc libibumad libibverbs rdma-core systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ibacm-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/infiniband-diags-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iwpmd-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libibumad-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libibverbs-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libibverbs-utils-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/librdmacm-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/librdmacm-utils-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rdma-core-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rdma-core-devel-26.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/srp_daemon-26.0-8.el8.aarch64.rpm \
          "

SRC_URI[ibacm.sha256sum] = "52c3b63be3ff083e8d83ac95f9b29202b7eaf767947f41eebf45acf6ee44cf04"
SRC_URI[infiniband-diags.sha256sum] = "7f18eb0dfbf82e28690a6e6b77f4c7d37054254a59359f7bdc371949d02ce58c"
SRC_URI[iwpmd.sha256sum] = "0111cea6840ac1c1163a500298ef9c0d12480050775d925cb1a3a25e863ea16b"
SRC_URI[libibumad.sha256sum] = "ba8be2d6c6fbdef9ebedb1fde4dfdfc1ed66fc35ba11659be1e040da08e6e8af"
SRC_URI[libibverbs.sha256sum] = "99952744df5a2111d185632bda340969aa5b5a9d3d61ae3b1b67de01b252bd68"
SRC_URI[libibverbs-utils.sha256sum] = "f729f35cc96e0826bc00eff9e8ceb398310179d4caaeafa730124719be3305e2"
SRC_URI[librdmacm.sha256sum] = "efbf99c5847bb3ef712d6f04eb612c1bd840c2e1f141f25225fb9b07f63f0b49"
SRC_URI[librdmacm-utils.sha256sum] = "98bd41c8fba37e272605b4b96636e9cfbbaa8d9b03a9a591ea805495dcabc7b3"
SRC_URI[rdma-core.sha256sum] = "9e3d8691f6c89459488058a794ebcd5fa0e25306b5d800a1a290d153fa4e33c9"
SRC_URI[rdma-core-devel.sha256sum] = "7b12d8e3cbb9d9142aee27ca235a6b0284eb56893d28ec6697992b814e52163e"
SRC_URI[srp_daemon.sha256sum] = "4151fc4cc99658665c57741942a1a0ef3012c826f023ae628ddb41a45aae9652"
