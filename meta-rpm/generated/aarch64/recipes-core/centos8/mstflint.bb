SUMMARY = "generated recipe based on mstflint srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost curl libgcc libxml2 openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_mstflint = "ld-linux-aarch64.so.1 libboost_filesystem.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_mstflint = "bash boost-filesystem boost-regex boost-system glibc libcurl libgcc libstdc++ libxml2 openssl-libs python36 xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mstflint-4.13.3-2.el8.aarch64.rpm \
          "

SRC_URI[mstflint.sha256sum] = "223d6639f1a5c49ced9070657f1c5f85c262c149ff7bc71388a711d2c102c769"
