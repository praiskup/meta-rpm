SUMMARY = "generated recipe based on libhugetlbfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libhugetlbfs = "libhugetlbfs.so libhugetlbfs_privutils.so"
RPM_SONAME_REQ_libhugetlbfs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libhugetlbfs = "bash glibc"
RPROVIDES_libhugetlbfs-devel = "libhugetlbfs-dev (= 2.21)"
RDEPENDS_libhugetlbfs-devel = "libhugetlbfs"
RPM_SONAME_REQ_libhugetlbfs-utils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libhugetlbfs-utils = "glibc libhugetlbfs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libhugetlbfs-2.21-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libhugetlbfs-devel-2.21-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libhugetlbfs-utils-2.21-12.el8.aarch64.rpm \
          "

SRC_URI[libhugetlbfs.sha256sum] = "e0717abcfcde50136f3ba63f731af7ae020eb62e012c29d1950a9bc345b2008e"
SRC_URI[libhugetlbfs-devel.sha256sum] = "2b56fc77a16c9627209d46392e8d90d9d4af4bc36998e9aa56fe23e6dbdd7c9f"
SRC_URI[libhugetlbfs-utils.sha256sum] = "d23ee1dce8334172401e1c45793941e2283d8853817c7f55aac696408d2a5797"
