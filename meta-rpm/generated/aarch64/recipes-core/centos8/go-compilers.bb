SUMMARY = "generated recipe based on go-compilers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/go-compilers-golang-compiler-1-20.el8.aarch64.rpm \
          "

SRC_URI[go-compilers-golang-compiler.sha256sum] = "713e525ee3073b1f3168f400cc7e4b3ee109afb99445eba6f16d3366ff0d3f05"
