SUMMARY = "generated recipe based on ipxe srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ipxe-roms = "ipxe-roms-qemu"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ipxe-bootimgs-20181214-5.git133f4c47.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ipxe-roms-20181214-5.git133f4c47.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ipxe-roms-qemu-20181214-5.git133f4c47.el8.noarch.rpm \
          "

SRC_URI[ipxe-bootimgs.sha256sum] = "221175bd39af942c0fbd9c9062a44f6a1279fa631d7082b3a65d19feec0d7886"
SRC_URI[ipxe-roms.sha256sum] = "a28bc4cafdae2277b040c49441b9131837579de021e16fe409fb64412a6fe30c"
SRC_URI[ipxe-roms-qemu.sha256sum] = "06505b9e36fb960e3ad3bee4e5276f98ca2dbf04695172cbe35a208effb76d18"
