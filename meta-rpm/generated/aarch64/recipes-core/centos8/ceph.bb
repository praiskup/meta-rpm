SUMMARY = "generated recipe based on ceph srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost libblkid libgcc nspr nss pkgconfig-native systemd-libs"
RPM_SONAME_REQ_libcephfs-devel = "libcephfs.so.2"
RPROVIDES_libcephfs-devel = "libcephfs-dev (= 12.2.7)"
RDEPENDS_libcephfs-devel = "libcephfs2 librados-devel"
RPM_SONAME_PROV_libcephfs2 = "libcephfs.so.2"
RPM_SONAME_REQ_libcephfs2 = "ld-linux-aarch64.so.1 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_libcephfs2 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util"
RPM_SONAME_REQ_librados-devel = "ld-linux-aarch64.so.1 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 librados.so.2 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RPROVIDES_librados-devel = "librados-dev (= 12.2.7)"
RDEPENDS_librados-devel = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util"
RPM_SONAME_PROV_librados2 = "libceph-common.so.0 librados.so.2"
RPM_SONAME_REQ_librados2 = "ld-linux-aarch64.so.1 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_librados2 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc libstdc++ nspr nss nss-util"
RPM_SONAME_REQ_libradosstriper-devel = "libradosstriper.so.1"
RPROVIDES_libradosstriper-devel = "libradosstriper-dev (= 12.2.7)"
RDEPENDS_libradosstriper-devel = "librados-devel libradosstriper1"
RPM_SONAME_PROV_libradosstriper1 = "libradosstriper.so.1"
RPM_SONAME_REQ_libradosstriper1 = "ld-linux-aarch64.so.1 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 librados.so.2 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_libradosstriper1 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util"
RPM_SONAME_REQ_librbd-devel = "librbd.so.1"
RPROVIDES_librbd-devel = "librbd-dev (= 12.2.7)"
RDEPENDS_librbd-devel = "librados-devel librbd1"
RPM_SONAME_PROV_librbd1 = "librbd.so.1"
RPM_SONAME_REQ_librbd1 = "ld-linux-aarch64.so.1 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 librados.so.2 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6 libudev.so.1"
RDEPENDS_librbd1 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librados2-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librbd1-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcephfs-devel-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcephfs2-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librados-devel-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libradosstriper-devel-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libradosstriper1-12.2.7-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librbd-devel-12.2.7-9.el8.aarch64.rpm \
          "

SRC_URI[libcephfs-devel.sha256sum] = "f1e8bf7514cfb28170ebcd83e11b940ef8b2b63f221b40cf985d2fb6ca5c1dff"
SRC_URI[libcephfs2.sha256sum] = "f9c34fc7184c147de5aee0dbb98e0d05e36b424c04abf53666497f1edffbdd14"
SRC_URI[librados-devel.sha256sum] = "a8b29ae34371ae7b8f1f4a5a7af8602ffa064ae8d05b653db5cf4e281ca49030"
SRC_URI[librados2.sha256sum] = "a329eaf1417673ec3cfcc3fbc44e72411c977d1de028af1acb081cfa4dca5f35"
SRC_URI[libradosstriper-devel.sha256sum] = "8ea3b5252d105e79d6c9be6d0b2ab1eacb19b3a93fa006397df0e680506c044e"
SRC_URI[libradosstriper1.sha256sum] = "85a44d5a4b8d0d93bfb92384a1aefc2e539a0c04c654e0603586452c2ee7863f"
SRC_URI[librbd-devel.sha256sum] = "582aef54c5749a6648930e654a61ffa7f8748a4c6cd83c8eae0b1bfba38640de"
SRC_URI[librbd1.sha256sum] = "f000ced1ab3678288a63e8ed373f84f0c7d36bea90ed4c8f92d404213fdb45ab"
