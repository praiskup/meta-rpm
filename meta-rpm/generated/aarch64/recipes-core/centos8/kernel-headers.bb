SUMMARY = "generated recipe based on kernel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "linux-libc-headers"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-headers-4.18.0-193.28.1.el8_2.aarch64.rpm \
          "

SRC_URI[kernel-headers.sha256sum] = "ff9cece106d7bb9c4ed75c507911c3e61c3a06f5f4e9eccf17028af57132023c"
