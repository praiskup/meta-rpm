SUMMARY = "generated recipe based on startup-notification srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxcb pkgconfig-native xcb-util"
RPM_SONAME_PROV_startup-notification = "libstartup-notification-1.so.0"
RPM_SONAME_REQ_startup-notification = "ld-linux-aarch64.so.1 libX11-xcb.so.1 libX11.so.6 libc.so.6 libxcb-util.so.1 libxcb.so.1"
RDEPENDS_startup-notification = "glibc libX11 libX11-xcb libxcb xcb-util"
RPM_SONAME_REQ_startup-notification-devel = "libstartup-notification-1.so.0"
RPROVIDES_startup-notification-devel = "startup-notification-dev (= 0.12)"
RDEPENDS_startup-notification-devel = "libX11-devel pkgconf-pkg-config startup-notification"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/startup-notification-0.12-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/startup-notification-devel-0.12-15.el8.aarch64.rpm \
          "

SRC_URI[startup-notification.sha256sum] = "a1e16911a1afab249b48cbc58199acd1d7f06ad065952f4756fc4e782ea310b3"
SRC_URI[startup-notification-devel.sha256sum] = "650302665500882a51a92a3a1abd8ba9c9989f2ce569d1ed68ecd5dd01c6adcc"
