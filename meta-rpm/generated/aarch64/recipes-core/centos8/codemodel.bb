SUMMARY = "generated recipe based on codemodel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_codemodel = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/codemodel-2.6-24.el8.noarch.rpm \
          "

SRC_URI[codemodel.sha256sum] = "fae6a23029097af0fefd4f29749bf28ac3b590867cf4ac24df3e64165e3caa80"
