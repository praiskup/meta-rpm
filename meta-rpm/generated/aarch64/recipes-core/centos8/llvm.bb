SUMMARY = "generated recipe based on llvm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi libgcc ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_llvm = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_llvm = "glibc libgcc libstdc++ llvm-libs ncurses-libs platform-python zlib"
RPM_SONAME_REQ_llvm-devel = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_llvm-devel = "llvm-dev (= 9.0.1)"
RDEPENDS_llvm-devel = "bash chkconfig cmake-filesystem glibc libgcc libstdc++ llvm llvm-libs"
RDEPENDS_llvm-doc = "llvm"
RPM_SONAME_PROV_llvm-libs = "libLLVM-9.so libLTO.so.9 libRemarks.so.9"
RPM_SONAME_REQ_llvm-libs = "ld-linux-aarch64.so.1 libLLVM-9.so libLTO.so.9 libRemarks.so.9 libc.so.6 libdl.so.2 libffi.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_llvm-libs = "glibc libffi libgcc libstdc++ ncurses-libs zlib"
RPM_SONAME_REQ_llvm-test = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_llvm-test = "bash binutils findutils gcc glibc libgcc libstdc++ llvm llvm-devel llvm-libs ncurses-libs python3-lit zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-9.0.1-5.module_el8.2.0+461+2e15bd5f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-devel-9.0.1-5.module_el8.2.0+461+2e15bd5f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-doc-9.0.1-5.module_el8.2.0+461+2e15bd5f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-googletest-9.0.1-5.module_el8.2.0+461+2e15bd5f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-libs-9.0.1-5.module_el8.2.0+461+2e15bd5f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-static-9.0.1-5.module_el8.2.0+461+2e15bd5f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/llvm-test-9.0.1-5.module_el8.2.0+461+2e15bd5f.aarch64.rpm \
          "

SRC_URI[llvm.sha256sum] = "9a452dbd8896f9a270db48e4f31fe906f977914d9cf0eaaa620fffc4bd422169"
SRC_URI[llvm-devel.sha256sum] = "b030758a71b24c493c5d245b646ad29fc72464a1c37291c34cc150f452769a28"
SRC_URI[llvm-doc.sha256sum] = "078a9ed150a50fa0bb2716611614c6fd85a737ccbf845879d1f417deee12fa59"
SRC_URI[llvm-googletest.sha256sum] = "3f6ebf54f8db469428b65d13900626e79a78265d706635c76ebb2b8ab7c7627b"
SRC_URI[llvm-libs.sha256sum] = "46dd0f2db15c54b7abfed75d05795a306864920892ffd2c8e112b69d85fb778e"
SRC_URI[llvm-static.sha256sum] = "c3ceffe90b565a442e1adefa428b3a412e8dbdf77679a5de524cb7756a00ae70"
SRC_URI[llvm-test.sha256sum] = "69bc68012ac45d71860a349193f5d97edd7ca13d3cc2d379c661d9c6bb61da84"
