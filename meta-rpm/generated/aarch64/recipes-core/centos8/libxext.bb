SUMMARY = "generated recipe based on libXext srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xext"
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXext = "libXext.so.6"
RPM_SONAME_REQ_libXext = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libXext = "glibc libX11"
RPM_SONAME_REQ_libXext-devel = "libXext.so.6"
RPROVIDES_libXext-devel = "libXext-dev (= 1.3.3)"
RDEPENDS_libXext-devel = "libX11-devel libXext pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXext-1.3.3-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXext-devel-1.3.3-9.el8.aarch64.rpm \
          "

SRC_URI[libXext.sha256sum] = "2af81148071438a7dac001103d220d42ed0944eab430ac1f6255a382ce251028"
SRC_URI[libXext-devel.sha256sum] = "85a682c8199eef9c267f9c2aec7ac9f98330f0105867daea111d1c66bd675182"
