SUMMARY = "generated recipe based on ps_mem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ps_mem = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ps_mem-3.6-6.el8.noarch.rpm \
          "

SRC_URI[ps_mem.sha256sum] = "2e090e7ab301900d1a26e63cf164f8fded56ca897a5b9402f6801ac1682ff4fb"
