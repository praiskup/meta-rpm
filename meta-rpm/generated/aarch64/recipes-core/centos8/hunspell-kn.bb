SUMMARY = "generated recipe based on hunspell-kn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-kn = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-kn-1.0.3-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-kn.sha256sum] = "94759ede0d759f16db8fd9e5fe98424b2da1a2d5fcc4c7eccab21a7787c5b1d9"
