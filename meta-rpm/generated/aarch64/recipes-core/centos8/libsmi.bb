SUMMARY = "generated recipe based on libsmi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsmi = "libsmi.so.2"
RPM_SONAME_REQ_libsmi = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libsmi.so.2"
RDEPENDS_libsmi = "bash gawk glibc wget"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsmi-0.4.8-22.el8.aarch64.rpm \
          "

SRC_URI[libsmi.sha256sum] = "10979db0393ca397662bfa705dde770716f2a3bed6d40b303a4cdd2029e81f24"
