SUMMARY = "generated recipe based on libsemanage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit bzip2 libselinux libsepol pkgconfig-native platform-python3"
RPM_SONAME_PROV_libsemanage = "libsemanage.so.1"
RPM_SONAME_REQ_libsemanage = "ld-linux-aarch64.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libselinux.so.1 libsepol.so.1"
RDEPENDS_libsemanage = "audit-libs bzip2-libs glibc libselinux libsepol"
RPM_SONAME_REQ_python3-libsemanage = "ld-linux-aarch64.so.1 libc.so.6 libpython3.6m.so.1.0 libsemanage.so.1"
RDEPENDS_python3-libsemanage = "glibc libsemanage platform-python python3-libs python3-libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsemanage-2.9-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libsemanage-2.9-2.el8.aarch64.rpm \
          "

SRC_URI[libsemanage.sha256sum] = "e0218803aefbded09bab7afb048bc836258ed2674cb7e42bbd1ac4d2264a0174"
SRC_URI[python3-libsemanage.sha256sum] = "5969efca67eea479bb19af5a7a7052c1548eb5e58cd8278cbd4e8a4036427915"
