SUMMARY = "generated recipe based on mod_lookup_identity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs pkgconfig-native"
RPM_SONAME_REQ_mod_lookup_identity = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3"
RDEPENDS_mod_lookup_identity = "dbus-libs glibc httpd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_lookup_identity-1.0.0-4.el8.aarch64.rpm \
          "

SRC_URI[mod_lookup_identity.sha256sum] = "fb1eb8138f0b73ec28d4a4a784a6cdeddd530642ec8d5cf41d25701a1e9d314f"
