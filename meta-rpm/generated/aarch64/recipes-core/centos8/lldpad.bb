SUMMARY = "generated recipe based on lldpad srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libconfig libnl pkgconfig-native"
RPM_SONAME_PROV_lldpad = "liblldp_clif.so.1"
RPM_SONAME_REQ_lldpad = "ld-linux-aarch64.so.1 libc.so.6 libconfig.so.9 libdl.so.2 liblldp_clif.so.1 libnl-3.so.200 librt.so.1"
RDEPENDS_lldpad = "bash glibc libconfig libnl3 readline systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lldpad-1.0.1-13.git036e314.el8.aarch64.rpm \
          "

SRC_URI[lldpad.sha256sum] = "6bb31c6b68bce46e9bcb2beff6ce0ce3c8bf13e3598b35743ccda5b6237b4b6b"
