SUMMARY = "generated recipe based on gtkmm30 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atkmm cairomm gdk-pixbuf glib-2.0 glibmm24 gtk+3 libgcc libsigc++20 pangomm pkgconfig-native"
RPM_SONAME_PROV_gtkmm30 = "libgdkmm-3.0.so.1 libgtkmm-3.0.so.1"
RPM_SONAME_REQ_gtkmm30 = "ld-linux-aarch64.so.1 libatkmm-1.6.so.1 libc.so.6 libcairomm-1.0.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgdkmm-3.0.so.1 libgiomm-2.4.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpangomm-1.4.so.1 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_gtkmm30 = "atkmm cairomm gdk-pixbuf2 glib2 glibc glibmm24 gtk3 libgcc libsigc++20 libstdc++ pangomm"
RPM_SONAME_REQ_gtkmm30-devel = "libgdkmm-3.0.so.1 libgtkmm-3.0.so.1"
RPROVIDES_gtkmm30-devel = "gtkmm30-dev (= 3.22.2)"
RDEPENDS_gtkmm30-devel = "atkmm-devel cairomm-devel gdk-pixbuf2-devel glibmm24-devel gtk3-devel gtkmm30 pangomm-devel pkgconf-pkg-config"
RDEPENDS_gtkmm30-doc = "glibmm24-doc gtkmm30"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtkmm30-3.22.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtkmm30-devel-3.22.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtkmm30-doc-3.22.2-2.el8.noarch.rpm \
          "

SRC_URI[gtkmm30.sha256sum] = "599fcb35bc444dd01c8401ae03ff4e2e2240aeea6e79cad10e24f75c97670f09"
SRC_URI[gtkmm30-devel.sha256sum] = "f4d59eb8ecb53aa6b04b8b7b2af2b7370ad5275143e124e4910553458f1b209a"
SRC_URI[gtkmm30-doc.sha256sum] = "0f3a7f29d1db91459062de197733ce4ae63081a8d28ed75aaf6d5e915814448e"
