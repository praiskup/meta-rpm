SUMMARY = "generated recipe based on mobile-broadband-provider-info srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mobile-broadband-provider-info-20190618-2.el8.noarch.rpm \
          "

SRC_URI[mobile-broadband-provider-info.sha256sum] = "e16983240ccdedef9e7084edbbee91fe79adc56a9649e13e567e28815506e8ac"
