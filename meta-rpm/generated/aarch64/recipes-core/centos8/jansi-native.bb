SUMMARY = "generated recipe based on jansi-native srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jansi-native = "hawtjni-runtime java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jansi-native-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jansi-native-1.7-7.module_el8.0.0+30+832da3a1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jansi-native-javadoc-1.7-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jansi-native.sha256sum] = "41f5892af6754922a5b7dba101a69acb1e922e99fef0b096b14e4956cef457e4"
SRC_URI[jansi-native-javadoc.sha256sum] = "e4e4b8a99f6bbedfb695306cb21084f07c098c8864ac210894119357be17d6b8"
