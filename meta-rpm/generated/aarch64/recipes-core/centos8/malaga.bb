SUMMARY = "generated recipe based on malaga srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 ncurses pango pkgconfig-native readline"
RPM_SONAME_PROV_libmalaga = "libmalaga.so.7"
RPM_SONAME_REQ_libmalaga = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libm.so.6"
RDEPENDS_libmalaga = "glib2 glibc"
RPM_SONAME_REQ_malaga = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libmalaga.so.7 libncurses.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libreadline.so.7 libtinfo.so.6"
RDEPENDS_malaga = "atk bash cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 info libmalaga ncurses-libs pango readline"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmalaga-7.12-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/malaga-7.12-23.el8.aarch64.rpm \
          "

SRC_URI[libmalaga.sha256sum] = "bd04a5679be1f23090c673ebfacd3b41b720b9552f4608c8a9093a2e3f934869"
SRC_URI[malaga.sha256sum] = "5ff9b598d89dc8efa00485a1780324d67e94ea1b6f32f10155354fdfe8420f4a"
