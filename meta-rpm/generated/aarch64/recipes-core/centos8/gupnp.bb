SUMMARY = "generated recipe based on gupnp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gssdp libsoup-2.4 libuuid libxml2 pkgconfig-native"
RPM_SONAME_PROV_gupnp = "libgupnp-1.0.so.4"
RPM_SONAME_REQ_gupnp = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssdp-1.0.so.3 libpthread.so.0 libsoup-2.4.so.1 libuuid.so.1 libxml2.so.2"
RDEPENDS_gupnp = "dbus glib2 glibc gssdp libsoup libuuid libxml2"
RPM_SONAME_REQ_gupnp-devel = "libgupnp-1.0.so.4"
RPROVIDES_gupnp-devel = "gupnp-dev (= 1.0.3)"
RDEPENDS_gupnp-devel = "gssdp-devel gupnp libsoup-devel libuuid-devel libxml2-devel pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gupnp-1.0.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gupnp-devel-1.0.3-2.el8.aarch64.rpm \
          "

SRC_URI[gupnp.sha256sum] = "5b25b7070af8207e178a6e1c38c41b23d36b1122011495780b0feeb9fb929e78"
SRC_URI[gupnp-devel.sha256sum] = "2abe6fefd5690d58f6d544162f1b3dbbb5776a00bce1ff829e3618be501f534d"
