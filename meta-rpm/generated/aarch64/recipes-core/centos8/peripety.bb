SUMMARY = "generated recipe based on peripety srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_peripety = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libsystemd.so.0"
RDEPENDS_peripety = "bash glibc libgcc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/peripety-0.1.2-3.el8.aarch64.rpm \
          "

SRC_URI[peripety.sha256sum] = "6057d1702634e1efef0d17f601d7d59a86dd72e4494a207f506861d52855ba3a"
