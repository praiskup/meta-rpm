SUMMARY = "generated recipe based on rubygem-mongo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-mongo = "ruby rubygems"
RDEPENDS_rubygem-mongo-doc = "rubygem-mongo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-mongo-2.5.1-2.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-mongo-doc-2.5.1-2.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-mongo.sha256sum] = "ff1d82d3e5ac749fe8c6abe997b4c83bcbef258355550cb2584137541b44d3f3"
SRC_URI[rubygem-mongo-doc.sha256sum] = "b444b052fb08f19633cb973703c23069e7d732bd68e4ae644088551f268b0de4"
