SUMMARY = "generated recipe based on perl-IO-Compress srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-Compress = "perl-Carp perl-Compress-Raw-Bzip2 perl-Compress-Raw-Zlib perl-Encode perl-Exporter perl-IO perl-PathTools perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-IO-Compress-2.081-1.el8.noarch.rpm \
          "

SRC_URI[perl-IO-Compress.sha256sum] = "96d6268cbade9694771c645e0c2d7e8f4e26929415afc52fac25a57f211d6ed4"
