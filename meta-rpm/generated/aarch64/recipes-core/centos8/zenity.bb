SUMMARY = "generated recipe based on zenity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libnotify libx11 pango pkgconfig-native"
RPM_SONAME_REQ_zenity = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_zenity = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libnotify pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/zenity-3.28.1-1.el8.aarch64.rpm \
          "

SRC_URI[zenity.sha256sum] = "f1679c245724c755f89d10c12aeda4cd4172af24f8ca00e47371e6501f099b30"
