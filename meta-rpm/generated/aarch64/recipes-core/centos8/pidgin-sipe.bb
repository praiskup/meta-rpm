SUMMARY = "generated recipe based on pidgin-sipe srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs e2fsprogs farstream02 glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base krb5-libs libnice libxml2 nspr nss pidgin pkgconfig-native"
RPM_SONAME_PROV_purple-sipe = "libsipe.so"
RPM_SONAME_REQ_purple-sipe = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libdbus-1.so.3 libdl.so.2 libfarstream-0.2.so.5 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstrtp-1.0.so.0 libk5crypto.so.3 libkrb5.so.3 libnice.so.10 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libpurple.so.0 libsmime3.so libssl3.so libxml2.so.2"
RDEPENDS_purple-sipe = "dbus-libs farstream02 glib2 glibc gssntlmssp gstreamer1 gstreamer1-plugins-base krb5-libs libcom_err libnice libpurple libxml2 nspr nss nss-util"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/purple-sipe-1.23.2-1.el8.aarch64.rpm \
          "

SRC_URI[purple-sipe.sha256sum] = "2a94cefe2f026e55d877d61975282f0d52b6e9ac8ec8b8cd893d85abbf2e0427"
