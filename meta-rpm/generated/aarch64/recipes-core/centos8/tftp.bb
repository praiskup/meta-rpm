SUMMARY = "generated recipe based on tftp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_tftp = "ld-linux-aarch64.so.1 libc.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_tftp = "glibc ncurses-libs readline"
RPM_SONAME_REQ_tftp-server = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_tftp-server = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tftp-5.2-24.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tftp-server-5.2-24.el8.aarch64.rpm \
          "

SRC_URI[tftp.sha256sum] = "aa7a184b35aae407afa0703d4d7b92cba4e8bc0c6c0761b45c2160fe7f3b3ed4"
SRC_URI[tftp-server.sha256sum] = "fc2dbc48cbb25ab3a8357c8c270779a04f93357c6e39ee4957819e290c59b462"
