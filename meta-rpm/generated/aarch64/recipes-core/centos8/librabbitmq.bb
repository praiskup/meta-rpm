SUMMARY = "generated recipe based on librabbitmq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_librabbitmq = "librabbitmq.so.4"
RPM_SONAME_REQ_librabbitmq = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0 librt.so.1 libssl.so.1.1"
RDEPENDS_librabbitmq = "glibc openssl-libs"
RPM_SONAME_REQ_librabbitmq-devel = "librabbitmq.so.4"
RPROVIDES_librabbitmq-devel = "librabbitmq-dev (= 0.9.0)"
RDEPENDS_librabbitmq-devel = "librabbitmq openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/librabbitmq-0.9.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librabbitmq-devel-0.9.0-1.el8.aarch64.rpm \
          "

SRC_URI[librabbitmq.sha256sum] = "20bec34b35e19ad814e2dde538b4bf59af73e053fab7c99a1ef1a79d274370af"
SRC_URI[librabbitmq-devel.sha256sum] = "22541d782d61848432b1424bed61c4abf459f5de1c0882e694698d0f67695a2c"
