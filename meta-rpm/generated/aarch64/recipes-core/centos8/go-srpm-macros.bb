SUMMARY = "generated recipe based on go-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/go-srpm-macros-2-16.el8.noarch.rpm \
          "

SRC_URI[go-srpm-macros.sha256sum] = "a7c54270d5d486df52b4c82b86028cfaa07f71216130d38d87606a4893af6a57"
