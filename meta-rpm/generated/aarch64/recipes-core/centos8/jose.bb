SUMMARY = "generated recipe based on jose srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jansson libgcc openssl pkgconfig-native zlib"
RPM_SONAME_REQ_jose = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libjansson.so.4 libjose.so.0 libpthread.so.0 libz.so.1"
RDEPENDS_jose = "glibc jansson libgcc libjose openssl-libs zlib"
RPM_SONAME_PROV_libjose = "libjose.so.0"
RPM_SONAME_REQ_libjose = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libjansson.so.4 libpthread.so.0 libz.so.1"
RDEPENDS_libjose = "glibc jansson libgcc openssl-libs zlib"
RPM_SONAME_REQ_libjose-devel = "libjose.so.0"
RPROVIDES_libjose-devel = "libjose-dev (= 10)"
RDEPENDS_libjose-devel = "jansson-devel libjose openssl-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jose-10-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libjose-10-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libjose-devel-10-2.el8.aarch64.rpm \
          "

SRC_URI[jose.sha256sum] = "639fff10dbc5e5d65e3d31921ed87cf91c3bb6569d7864440b63399a33f1c9d8"
SRC_URI[libjose.sha256sum] = "c7e70bd1d1141f094858f92448442887ba1e887a35a069992ba3a049c9f0c6cb"
SRC_URI[libjose-devel.sha256sum] = "3e353d36a286c71741b12f1254b05e294f680368903488bef207d6cc4aae6cd5"
