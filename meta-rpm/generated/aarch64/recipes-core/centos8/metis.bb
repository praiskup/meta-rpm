SUMMARY = "generated recipe based on metis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_metis = "libmetis.so.0"
RPM_SONAME_REQ_metis = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgomp.so.1 libm.so.6 libmetis.so.0 libpthread.so.0"
RDEPENDS_metis = "glibc libgcc libgomp"
RPM_SONAME_REQ_metis-devel = "libmetis.so.0"
RPROVIDES_metis-devel = "metis-dev (= 5.1.0)"
RDEPENDS_metis-devel = "metis"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/metis-5.1.0-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/metis-devel-5.1.0-17.el8.aarch64.rpm \
          "

SRC_URI[metis.sha256sum] = "cdde886f1a697a557259593d6fd2ebe69c74f6d948df754830ace2b6eca998c7"
SRC_URI[metis-devel.sha256sum] = "494c6d9a080cec7db1a73f7dc2335111c2e5a7842896d0088f41b7264978e7df"
