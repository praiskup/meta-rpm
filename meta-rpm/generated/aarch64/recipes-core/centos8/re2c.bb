SUMMARY = "generated recipe based on re2c srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_re2c = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_re2c = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/re2c-0.14.3-2.el8.aarch64.rpm \
          "

SRC_URI[re2c.sha256sum] = "230f78dad2cde1676746585a36ace5a77a28e2355144c7431edf9b01a20c00c9"
