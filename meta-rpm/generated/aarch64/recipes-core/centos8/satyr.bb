SUMMARY = "generated recipe based on satyr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native rpm zlib"
RPM_SONAME_PROV_satyr = "libsatyr.so.3"
RPM_SONAME_REQ_satyr = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 librpm.so.8 librpmio.so.8 libsatyr.so.3 libstdc++.so.6 libz.so.1"
RDEPENDS_satyr = "elfutils-libelf elfutils-libs glibc libstdc++ rpm-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/satyr-0.26-2.el8.aarch64.rpm \
          "

SRC_URI[satyr.sha256sum] = "7e34d29ca7d99d6f1bc3958a9bbac403a452b02c7f1e86a2187cecebd3e4d1a8"
