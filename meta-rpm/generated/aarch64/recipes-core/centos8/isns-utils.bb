SUMMARY = "generated recipe based on isns-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_isns-utils = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libisns.so.0"
RDEPENDS_isns-utils = "bash glibc isns-utils-libs openssl-libs systemd"
RPM_SONAME_REQ_isns-utils-devel = "libisns.so.0"
RPROVIDES_isns-utils-devel = "isns-utils-dev (= 0.99)"
RDEPENDS_isns-utils-devel = "isns-utils-libs"
RPM_SONAME_PROV_isns-utils-libs = "libisns.so.0"
RPM_SONAME_REQ_isns-utils-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1"
RDEPENDS_isns-utils-libs = "glibc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/isns-utils-0.99-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/isns-utils-devel-0.99-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/isns-utils-libs-0.99-1.el8.aarch64.rpm \
          "

SRC_URI[isns-utils.sha256sum] = "9dd5d575ffe7fb60b3a13dcac25d26acf4112d3de665c5c78331de2d2a388c8e"
SRC_URI[isns-utils-devel.sha256sum] = "10601f2d2148634da06dd5de7a0113489687f5771faa7da5785c38bbbd5c31a5"
SRC_URI[isns-utils-libs.sha256sum] = "004f664ef6047dc43fde74d56f7db38fbb10450ce82022046eac4e99155424a2"
