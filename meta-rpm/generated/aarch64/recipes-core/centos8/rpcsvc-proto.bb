SUMMARY = "generated recipe based on rpcsvc-proto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_rpcgen = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_rpcgen = "glibc"
RPROVIDES_rpcsvc-proto-devel = "rpcsvc-proto-dev (= 1.3.1)"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rpcgen-1.3.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rpcsvc-proto-devel-1.3.1-4.el8.aarch64.rpm \
          "

SRC_URI[rpcgen.sha256sum] = "b0a7d79c0df671d60e84c6db14c8a7b123cf4e64114e058efb51160565fedf5a"
SRC_URI[rpcsvc-proto-devel.sha256sum] = "a3613f307e7b7e2c9ccad6286ad9ee41170026db0cf7509611d5f6ce78c5e249"
