SUMMARY = "generated recipe based on ibus-table srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ibus-table = "bash ibus platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-1.9.18-3.el8.noarch.rpm \
          "

SRC_URI[ibus-table.sha256sum] = "6b31ecdeed115c00a505dfb56e1cdaad1445bc0292d9ad4e299848a412ba9506"
