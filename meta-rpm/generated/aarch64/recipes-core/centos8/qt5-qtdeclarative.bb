SUMMARY = "generated recipe based on qt5-qtdeclarative srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qt5-qtdeclarative = "libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickParticles.so.5 libQt5QuickShapes.so.5 libQt5QuickTest.so.5 libQt5QuickWidgets.so.5 libqmldbg_debugger.so libqmldbg_inspector.so libqmldbg_local.so libqmldbg_messages.so libqmldbg_native.so libqmldbg_nativedebugger.so libqmldbg_preview.so libqmldbg_profiler.so libqmldbg_quickprofiler.so libqmldbg_server.so libqmldbg_tcp.so"
RPM_SONAME_REQ_qt5-qtdeclarative = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickParticles.so.5 libQt5QuickShapes.so.5 libQt5QuickTest.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_qt5-qtdeclarative = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_qt5-qtdeclarative-devel = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickParticles.so.5 libQt5QuickShapes.so.5 libQt5QuickTest.so.5 libQt5QuickWidgets.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtdeclarative-devel = "qt5-qtdeclarative-dev (= 5.12.5)"
RDEPENDS_qt5-qtdeclarative-devel = "cmake-filesystem glibc libgcc libglvnd-glx libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtdeclarative-examples = "libchartsplugin.so libqmlimageproviderplugin.so libqmlimageresponseproviderplugin.so libqmlqtimeexampleplugin.so libqmltextballoonplugin.so"
RPM_SONAME_REQ_qt5-qtdeclarative-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickTest.so.5 libQt5QuickWidgets.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtdeclarative-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RDEPENDS_qt5-qtdeclarative-static = "qt5-qtdeclarative-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtdeclarative-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtdeclarative-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtdeclarative-examples-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qt5-qtdeclarative-static-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtdeclarative.sha256sum] = "4f007278c214e6ac07ffc239be31cd17b2a2b48515babd5a9ff85d534d34a860"
SRC_URI[qt5-qtdeclarative-devel.sha256sum] = "c5d0fa8824ddbaf74b0a149b1d08901a3187858fb8d14476a0a902783f6045f3"
SRC_URI[qt5-qtdeclarative-examples.sha256sum] = "68fec4eb3657e38678787dc4a2544d2fa3f24e9b7e6d2eab1497efa2baf889ce"
SRC_URI[qt5-qtdeclarative-static.sha256sum] = "eea10deef1ed445d4ad4286021346c7bb82e13c1fcb2fe9aa8d78cdfe4b29ba8"
