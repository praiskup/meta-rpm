SUMMARY = "generated recipe based on xz-java srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xz-java = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_xz-java-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xz-java-1.8-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xz-java-javadoc-1.8-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xz-java.sha256sum] = "54a5d4d61c6bdc75213b592412cee42e00ee635c53efc67856f203ad5b0487f2"
SRC_URI[xz-java-javadoc.sha256sum] = "598882729e5add41906e0be8a4d6d170da6bcc836754e72faf05890a6e498848"
