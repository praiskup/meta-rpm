SUMMARY = "generated recipe based on jabberpy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jabberpy = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-jabberpy-0.5-0.38.el8.noarch.rpm \
          "

SRC_URI[python3-jabberpy.sha256sum] = "745cecd5373e8f2175f3e11716ea906368c046f21e9b448d4e0a991e555003d6"
