SUMMARY = "generated recipe based on apr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libuuid libxcrypt pkgconfig-native"
RPM_SONAME_PROV_apr = "libapr-1.so.0"
RPM_SONAME_REQ_apr = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0 libuuid.so.1"
RDEPENDS_apr = "glibc libuuid libxcrypt"
RPM_SONAME_REQ_apr-devel = "libapr-1.so.0"
RPROVIDES_apr-devel = "apr-dev (= 1.6.3)"
RDEPENDS_apr-devel = "apr bash pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-1.6.3-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-devel-1.6.3-9.el8.aarch64.rpm \
          "

SRC_URI[apr.sha256sum] = "eb7c476025dedd4c3ec6fcef585d346f2cfc5696e9639aacd902da5fb7632571"
SRC_URI[apr-devel.sha256sum] = "41d5a7ffb1d0adae63f5982dc1cc0a495ce7d26aba9f2176907ee9dbdc465ee0"
