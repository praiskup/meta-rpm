SUMMARY = "generated recipe based on qt5-qtconnectivity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bluez libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtconnectivity = "libQt5Bluetooth.so.5 libQt5Nfc.so.5"
RPM_SONAME_REQ_qt5-qtconnectivity = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Bluetooth.so.5 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Nfc.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libbluetooth.so.3 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtconnectivity = "bluez-libs glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtconnectivity-devel = "libQt5Bluetooth.so.5 libQt5Nfc.so.5"
RPROVIDES_qt5-qtconnectivity-devel = "qt5-qtconnectivity-dev (= 5.12.5)"
RDEPENDS_qt5-qtconnectivity-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtconnectivity"
RPM_SONAME_REQ_qt5-qtconnectivity-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Bluetooth.so.5 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Nfc.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtconnectivity-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtconnectivity qt5-qtdeclarative"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtconnectivity-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtconnectivity-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtconnectivity-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtconnectivity.sha256sum] = "40d3da866fd0ac0142fe4e53c9e57aca4b40746ace0ebf228188906bb1d93268"
SRC_URI[qt5-qtconnectivity-devel.sha256sum] = "04bd2139062767129f586775f96c0cba4452425be46bfebf986ec3d55a590205"
SRC_URI[qt5-qtconnectivity-examples.sha256sum] = "821a964095c230cfe030412e03a2fcb2636b1ca926d69d7d361421935a1e5732"
