SUMMARY = "generated recipe based on ipcalc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ipcalc = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_ipcalc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ipcalc-0.2.4-4.el8.aarch64.rpm \
          "

SRC_URI[ipcalc.sha256sum] = "93f4b9f5198e2b04dd8d176ab127660739b18d33438150b03392925f09321fe8"
