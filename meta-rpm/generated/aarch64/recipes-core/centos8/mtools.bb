SUMMARY = "generated recipe based on mtools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mtools = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mtools = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mtools-4.0.18-14.el8.aarch64.rpm \
          "

SRC_URI[mtools.sha256sum] = "2e35d899b1a7d8b40066ff182f9d6f8437f791ce093acf409852e671e51748cc"
