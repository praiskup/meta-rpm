SUMMARY = "generated recipe based on lftp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcc ncurses pkgconfig-native readline zlib"
RPM_SONAME_PROV_lftp = "liblftp-jobs.so.0 liblftp-network.so liblftp-pty.so liblftp-tasks.so.0"
RPM_SONAME_REQ_lftp = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 liblftp-jobs.so.0 liblftp-network.so liblftp-pty.so liblftp-tasks.so.0 libm.so.6 libreadline.so.7 libstdc++.so.6 libtinfo.so.6 libutil.so.1 libz.so.1"
RDEPENDS_lftp = "bash glibc gnutls libgcc libstdc++ ncurses-libs readline zlib"
RDEPENDS_lftp-scripts = "bash lftp perl-DBI perl-Digest-MD5 perl-String-CRC32 perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lftp-4.8.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lftp-scripts-4.8.4-1.el8.noarch.rpm \
          "

SRC_URI[lftp.sha256sum] = "66d0b3de3a1424f1c27acdee3fd9b2610f3a767b394e8361e7132943fddf18cd"
SRC_URI[lftp-scripts.sha256sum] = "dec4a9ba7184391e3c4edb12767507362f20a6dfc3151b532144837ff738e381"
