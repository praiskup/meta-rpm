SUMMARY = "generated recipe based on ostree srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl fuse glib-2.0 gpgme libarchive libgcc libgpg-error libmount libselinux openssl pkgconfig-native systemd-libs xz zlib"
RPM_SONAME_REQ_ostree = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libfuse.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 liblzma.so.5 libmount.so.1 libostree-1.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libz.so.1"
RDEPENDS_ostree = "bash dracut fuse-libs glib2 glibc gnupg2 gpgme libarchive libcurl libgcc libgpg-error libmount libselinux openssl-libs ostree-libs systemd systemd-libs xz-libs zlib"
RPM_SONAME_REQ_ostree-devel = "libostree-1.so.1"
RPROVIDES_ostree-devel = "ostree-dev (= 2019.6)"
RDEPENDS_ostree-devel = "glib2-devel ostree-libs pkgconf-pkg-config"
RDEPENDS_ostree-grub2 = "bash grub2-efi-aa64 ostree"
RPM_SONAME_PROV_ostree-libs = "libostree-1.so.1"
RPM_SONAME_REQ_ostree-libs = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 liblzma.so.5 libmount.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libz.so.1"
RDEPENDS_ostree-libs = "glib2 glibc gpgme libarchive libcurl libgcc libgpg-error libmount libselinux openssl-libs systemd-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ostree-2019.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ostree-devel-2019.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ostree-grub2-2019.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ostree-libs-2019.6-2.el8.aarch64.rpm \
          "

SRC_URI[ostree.sha256sum] = "493dbfa4a75ca4ef72caf9edb8d5fffc69286fff3ae13b8318738c1f1a0d7769"
SRC_URI[ostree-devel.sha256sum] = "6d5ff9baaf02a040923723db465222c5abad157fe6f3fdbcd6532c332555329d"
SRC_URI[ostree-grub2.sha256sum] = "31e3814533666273902fd388d27bf03c6c65f66187ac09a90d41de1ca04611b5"
SRC_URI[ostree-libs.sha256sum] = "7920b311b342b1a5d7f085d229e37920a77101691ab6d38e77f99d780b6361b4"
