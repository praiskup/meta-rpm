SUMMARY = "generated recipe based on python-six srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-six = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-six-1.11.0-8.el8.noarch.rpm \
          "

SRC_URI[python3-six.sha256sum] = "a04cb3117395b962edc32bf45d8411f240632476b0706b2df7f4a1a87b2ce34b"
