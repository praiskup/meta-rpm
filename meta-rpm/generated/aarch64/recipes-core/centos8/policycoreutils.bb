SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-glib dbus-libs glib-2.0 libcap-ng libpcre libselinux libsemanage libsepol pam pkgconfig-native selinux-policy"
RPM_SONAME_REQ_policycoreutils = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libselinux.so.1 libsemanage.so.1 libsepol.so.1"
RDEPENDS_policycoreutils = "audit-libs bash coreutils diffutils gawk glibc grep libselinux libselinux-utils libsemanage libsepol rpm sed util-linux"
RDEPENDS_policycoreutils-dbus = "platform-python python3-policycoreutils python3-slip-dbus"
RPM_SONAME_REQ_policycoreutils-devel = "ld-linux-aarch64.so.1 libc.so.6"
RPROVIDES_policycoreutils-devel = "policycoreutils-dev (= 2.9)"
RDEPENDS_policycoreutils-devel = "dnf glibc make platform-python policycoreutils-python-utils selinux-policy-devel"
RPM_SONAME_REQ_policycoreutils-newrole = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcap-ng.so.0 libpam.so.0 libpam_misc.so.0 libselinux.so.1"
RDEPENDS_policycoreutils-newrole = "audit-libs glibc libcap-ng libselinux pam policycoreutils"
RDEPENDS_policycoreutils-python-utils = "platform-python python3-policycoreutils"
RPM_SONAME_REQ_policycoreutils-restorecond = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libpcre.so.1 libselinux.so.1"
RDEPENDS_policycoreutils-restorecond = "bash dbus-glib dbus-libs glib2 glibc libselinux pcre"
RDEPENDS_python3-policycoreutils = "checkpolicy platform-python policycoreutils python3-audit python3-libselinux python3-libsemanage python3-setools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-2.9-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-dbus-2.9-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-devel-2.9-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-newrole-2.9-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-python-utils-2.9-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/policycoreutils-restorecond-2.9-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-policycoreutils-2.9-9.el8.noarch.rpm \
          "

SRC_URI[policycoreutils.sha256sum] = "7a01981bdba9de7d9a6634b6d66717da6cc9e4df1db03f3c339f23bb8dba7a60"
SRC_URI[policycoreutils-dbus.sha256sum] = "8e68ceb79f0bc7e77950e32fbad71e7db9ec047f10e35988e4e2dc7ccdd4a245"
SRC_URI[policycoreutils-devel.sha256sum] = "bb411f7fffd4a2b3404f5222fe4d66bcfb0a3a03cb296939e83d9a9e30939c81"
SRC_URI[policycoreutils-newrole.sha256sum] = "568133a047e98e7b1ed37e783393d8dbf1dffc994ef52b4759ad8959f059a2cc"
SRC_URI[policycoreutils-python-utils.sha256sum] = "f1b53326dec969c70f4c61da7241474ff7428d35d235301836ad1c0ce3031cab"
SRC_URI[policycoreutils-restorecond.sha256sum] = "fb7e58bef6bdb5c67cf2785a3943894a231495aeb17291e850322a78f5adefa9"
SRC_URI[python3-policycoreutils.sha256sum] = "f5b2e42438721527f01405474819f3346eb470315371086bc5aedcb46d34633e"
