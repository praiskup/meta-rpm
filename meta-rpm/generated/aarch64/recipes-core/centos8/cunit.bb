SUMMARY = "generated recipe based on CUnit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_CUnit = "libcunit.so.1"
RPM_SONAME_REQ_CUnit = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_CUnit = "glibc"
RPM_SONAME_REQ_CUnit-devel = "libcunit.so.1"
RPROVIDES_CUnit-devel = "CUnit-dev (= 2.1.3)"
RDEPENDS_CUnit-devel = "CUnit pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/CUnit-2.1.3-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/CUnit-devel-2.1.3-17.el8.aarch64.rpm \
          "

SRC_URI[CUnit.sha256sum] = "c3e19fa83a9174f5989e2cb963520598e52ff462e7edd4dbd10670c5d4afbb35"
SRC_URI[CUnit-devel.sha256sum] = "42bd2bd938da5ea302dc5daddf1cc8c4a37fb9b15a8514d3bf7d80c040cfcb28"
