SUMMARY = "generated recipe based on clutter-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo clutter cogl gdk-pixbuf glib-2.0 gtk+3 json-glib libdrm libglvnd libx11 libxcomposite libxdamage libxext libxfixes libxi libxkbcommon libxrandr mesa pango pkgconfig-native wayland"
RPM_SONAME_PROV_clutter-gtk = "libclutter-gtk-1.0.so.0"
RPM_SONAME_REQ_clutter-gtk = "ld-linux-aarch64.so.1 libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libdrm.so.2 libgbm.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxkbcommon.so.0"
RDEPENDS_clutter-gtk = "atk cairo cairo-gobject clutter cogl gdk-pixbuf2 glib2 glibc gtk3 json-glib libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libdrm libglvnd-egl libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon mesa-libgbm pango"
RPM_SONAME_REQ_clutter-gtk-devel = "libclutter-gtk-1.0.so.0"
RPROVIDES_clutter-gtk-devel = "clutter-gtk-dev (= 1.8.4)"
RDEPENDS_clutter-gtk-devel = "clutter-devel clutter-gtk gtk3-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clutter-gtk-1.8.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/clutter-gtk-devel-1.8.4-3.el8.aarch64.rpm \
          "

SRC_URI[clutter-gtk.sha256sum] = "8748a8f2208ec1534c084e8cfc8472ad83c19e69d34dcc628443f8e916c7ab45"
SRC_URI[clutter-gtk-devel.sha256sum] = "d69708fbb2cde726318a043475b01a0662218519b21a5e0b7106acdbb542d53d"
