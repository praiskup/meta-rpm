SUMMARY = "generated recipe based on python-dateutil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-dateutil = "platform-python python3-six tzdata"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-dateutil-2.6.1-6.el8.noarch.rpm \
          "

SRC_URI[python3-dateutil.sha256sum] = "c5b5967a094ced90899052a82e2c245529b75ba3f46e0ce1a89cfc95edb935ea"
