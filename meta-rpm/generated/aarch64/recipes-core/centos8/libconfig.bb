SUMMARY = "generated recipe based on libconfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libconfig = "libconfig++.so.9 libconfig.so.9"
RPM_SONAME_REQ_libconfig = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libconfig = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libconfig-devel = "libconfig++.so.9 libconfig.so.9"
RPROVIDES_libconfig-devel = "libconfig-dev (= 1.5)"
RDEPENDS_libconfig-devel = "bash info libconfig pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libconfig-1.5-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libconfig-devel-1.5-9.el8.aarch64.rpm \
          "

SRC_URI[libconfig.sha256sum] = "61d9134d4209d9db5c2d88ad8e907d426ba78c698c6e9750a7be034ee0d677b7"
SRC_URI[libconfig-devel.sha256sum] = "4da84f7db68a41eb09db91b63e4ea115ba36e1ceb541151172e48906d49ca9f7"
