SUMMARY = "generated recipe based on p11-kit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi libtasn1 pkgconfig-native systemd-libs"
RPM_SONAME_PROV_p11-kit = "libp11-kit.so.0"
RPM_SONAME_REQ_p11-kit = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libp11-kit.so.0 libpthread.so.0"
RDEPENDS_p11-kit = "glibc libffi"
RPM_SONAME_REQ_p11-kit-devel = "libp11-kit.so.0"
RPROVIDES_p11-kit-devel = "p11-kit-dev (= 0.23.14)"
RDEPENDS_p11-kit-devel = "p11-kit pkgconf-pkg-config"
RPM_SONAME_REQ_p11-kit-server = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libp11-kit.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_p11-kit-server = "glibc libffi p11-kit systemd-libs"
RPM_SONAME_REQ_p11-kit-trust = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libp11-kit.so.0 libpthread.so.0 libtasn1.so.6"
RDEPENDS_p11-kit-trust = "bash chkconfig glibc libffi libtasn1 p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/p11-kit-0.23.14-5.el8_0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/p11-kit-devel-0.23.14-5.el8_0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/p11-kit-server-0.23.14-5.el8_0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/p11-kit-trust-0.23.14-5.el8_0.aarch64.rpm \
          "

SRC_URI[p11-kit.sha256sum] = "fdf69d6c98f6a9bae8a280b16a043841bb086470c3366e0ec75eb46c2ade7013"
SRC_URI[p11-kit-devel.sha256sum] = "723b3cc2f95875edea7ca309db79d403207bd93753357642db5722b6088de49a"
SRC_URI[p11-kit-server.sha256sum] = "86d1d42bd38fa861040d3f492e8a7b5577f9f6184dc5db847fa98054f4ec21f7"
SRC_URI[p11-kit-trust.sha256sum] = "a629464b907faa92e7d294120a95b492078e2f05122e51810c4f4912ff8f95de"
