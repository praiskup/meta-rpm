SUMMARY = "generated recipe based on alsa-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib fftw libgcc libsamplerate0 ncurses pkgconfig-native"
RPM_SONAME_REQ_alsa-utils = "ld-linux-aarch64.so.1 libasound.so.2 libatopology.so.2 libc.so.6 libdl.so.2 libformw.so.6 libm.so.6 libmenuw.so.6 libncursesw.so.6 libpanelw.so.6 libpthread.so.0 librt.so.1 libsamplerate.so.0 libtinfo.so.6"
RDEPENDS_alsa-utils = "alsa-lib bash glibc libsamplerate ncurses-libs systemd"
RPM_SONAME_REQ_alsa-utils-alsabat = "ld-linux-aarch64.so.1 libasound.so.2 libatopology.so.2 libc.so.6 libdl.so.2 libfftw3f.so.3 libgcc_s.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_alsa-utils-alsabat = "alsa-lib bash fftw-libs-single glibc libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-utils-1.2.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-utils-alsabat-1.2.1-2.el8.aarch64.rpm \
          "

SRC_URI[alsa-utils.sha256sum] = "207b6dd57d6493caa5b50c7865421105305ee92eff404f56b8710e00ee709e13"
SRC_URI[alsa-utils-alsabat.sha256sum] = "1cc0f4d9494f137efd8cc4131fc26fdb68ee05e73246597cb498e2728f5982c1"
