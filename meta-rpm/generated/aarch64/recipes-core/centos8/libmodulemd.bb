SUMMARY = "generated recipe based on libmodulemd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "file glib-2.0 libgcc libyaml pkgconfig-native rpm"
RPM_SONAME_PROV_libmodulemd = "libmodulemd.so.2"
RPM_SONAME_REQ_libmodulemd = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libmagic.so.1 libmodulemd.so.2 librpmio.so.8 libyaml-0.so.2"
RDEPENDS_libmodulemd = "file-libs glib2 glibc libgcc libyaml rpm-libs"
RPM_SONAME_REQ_libmodulemd-devel = "libmodulemd.so.2"
RPROVIDES_libmodulemd-devel = "libmodulemd-dev (= 2.8.2)"
RDEPENDS_libmodulemd-devel = "glib2-devel libmodulemd libyaml-devel pkgconf-pkg-config rpm-devel"
RPM_SONAME_PROV_libmodulemd1 = "libmodulemd.so.1"
RPM_SONAME_REQ_libmodulemd1 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libmodulemd.so.1 libyaml-0.so.2"
RDEPENDS_libmodulemd1 = "glib2 glibc libgcc libyaml"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmodulemd-2.8.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmodulemd1-1.8.16-0.2.8.2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmodulemd-devel-2.8.2-1.el8.aarch64.rpm \
          "

SRC_URI[libmodulemd.sha256sum] = "9e4ae04bd819ea26fdb85b502748c2588ef189e24c9308440a2876310ffe815b"
SRC_URI[libmodulemd-devel.sha256sum] = "2ffc0c870e208a695e466c016579189787431876f767a6227b5f0ec1bd9fdc23"
SRC_URI[libmodulemd1.sha256sum] = "294838dcff05b62a7211aaaf67bd350de7d2762483d087587e1ba4c8472c134e"
