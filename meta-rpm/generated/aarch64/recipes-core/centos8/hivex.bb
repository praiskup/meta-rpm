SUMMARY = "generated recipe based on hivex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxml2 perl pkgconfig-native readline ruby"
RPM_SONAME_PROV_hivex = "libhivex.so.0"
RPM_SONAME_REQ_hivex = "ld-linux-aarch64.so.1 libc.so.6 libhivex.so.0 libreadline.so.7 libxml2.so.2"
RDEPENDS_hivex = "bash glibc libxml2 readline"
RPM_SONAME_REQ_hivex-devel = "libhivex.so.0"
RPROVIDES_hivex-devel = "hivex-dev (= 1.3.15)"
RDEPENDS_hivex-devel = "hivex pkgconf-pkg-config"
RPM_SONAME_REQ_ocaml-hivex = "ld-linux-aarch64.so.1 libc.so.6 libhivex.so.0"
RDEPENDS_ocaml-hivex = "glibc hivex ocaml-runtime"
RPROVIDES_ocaml-hivex-devel = "ocaml-hivex-dev (= 1.3.15)"
RDEPENDS_ocaml-hivex-devel = "hivex-devel ocaml-hivex"
RPM_SONAME_REQ_perl-hivex = "ld-linux-aarch64.so.1 libc.so.6 libhivex.so.0 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-hivex = "glibc hivex perl-Carp perl-Encode perl-Exporter perl-Getopt-Long perl-Pod-Usage perl-interpreter perl-libs"
RPM_SONAME_PROV_python3-hivex = "libhivexmod.cpython-36m-aarch64-linux-gnu.so"
RPM_SONAME_REQ_python3-hivex = "ld-linux-aarch64.so.1 libc.so.6 libhivex.so.0"
RDEPENDS_python3-hivex = "glibc hivex platform-python"
RPM_SONAME_REQ_ruby-hivex = "ld-linux-aarch64.so.1 libc.so.6 libhivex.so.0 libruby.so.2.5"
RDEPENDS_ruby-hivex = "glibc hivex ruby ruby-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hivex-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hivex-devel-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ruby-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-hivex-devel-1.3.15-7.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[hivex.sha256sum] = "3f316f3a3ed82c24ffc18303145d160e0311ebf9f9c1574ed90ece1cbc8266a6"
SRC_URI[hivex-devel.sha256sum] = "88a1d39c59b2cbc77fffd64e4ee08fda82b6416ec81829afe2d99b63f086a314"
SRC_URI[ocaml-hivex.sha256sum] = "b07eb8e263fb73a725c048458153876fbb6b1524720801d2529f16e552efc25a"
SRC_URI[ocaml-hivex-devel.sha256sum] = "2d1706507d7ba8d3e0c7e362292c4238974612fabfb31f98eabcd2c08b759bfd"
SRC_URI[perl-hivex.sha256sum] = "60394338ff1cf8e19334efa5e8763e1a33bfd9f4123a26bf5cf54c34d7a92059"
SRC_URI[python3-hivex.sha256sum] = "eb918e14e4f77319d2425bd5cbc8d5e5fa5f12a361b70cb9e264482c86b7e55c"
SRC_URI[ruby-hivex.sha256sum] = "039c4a9aade8194a2a66bc28ec14b0184dea226d0ad21a55de9ea09964d8b250"
