SUMMARY = "generated recipe based on sscg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ding-libs libtalloc openssl pkgconfig-native popt"
RPM_SONAME_REQ_sscg = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpath_utils.so.1 libpopt.so.0 libssl.so.1.1 libtalloc.so.2"
RDEPENDS_sscg = "glibc libpath_utils libtalloc openssl-libs popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sscg-2.3.3-14.el8.aarch64.rpm \
          "

SRC_URI[sscg.sha256sum] = "b0ee4de357e56889a4f62eb2294ff18997150ca2cef4d208a6eca566114997b8"
