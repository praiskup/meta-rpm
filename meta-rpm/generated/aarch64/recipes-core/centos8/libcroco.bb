SUMMARY = "generated recipe based on libcroco srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_PROV_libcroco = "libcroco-0.6.so.3"
RPM_SONAME_REQ_libcroco = "ld-linux-aarch64.so.1 libc.so.6 libcroco-0.6.so.3 libglib-2.0.so.0 libxml2.so.2"
RDEPENDS_libcroco = "glib2 glibc libxml2"
RPM_SONAME_REQ_libcroco-devel = "libcroco-0.6.so.3"
RPROVIDES_libcroco-devel = "libcroco-dev (= 0.6.12)"
RDEPENDS_libcroco-devel = "bash glib2-devel libcroco libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcroco-0.6.12-4.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcroco-devel-0.6.12-4.el8_2.1.aarch64.rpm \
          "

SRC_URI[libcroco.sha256sum] = "0022ec2580783f68e603e9d4751478c28f2b383c596b4e896469077748771bfe"
SRC_URI[libcroco-devel.sha256sum] = "3967d727462bd60e3856d7a7c7a164212f2f245f055755d1a35bbf874d7fbf8d"
