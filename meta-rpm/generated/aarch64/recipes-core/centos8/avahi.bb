SUMMARY = "generated recipe based on avahi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk avahi-libs cairo dbus-libs expat gdbm gdk-pixbuf glib-2.0 gtk+3 libcap libdaemon pango pkgconfig-native"
RPM_SONAME_PROV_avahi = "libavahi-core.so.7"
RPM_SONAME_REQ_avahi = "ld-linux-aarch64.so.1 libavahi-common.so.3 libavahi-core.so.7 libc.so.6 libcap.so.2 libdaemon.so.0 libdbus-1.so.3 libdl.so.2 libexpat.so.1 libpthread.so.0"
RDEPENDS_avahi = "avahi-libs bash coreutils dbus dbus-libs expat glibc libcap libdaemon shadow-utils systemd"
RPM_SONAME_REQ_avahi-autoipd = "ld-linux-aarch64.so.1 libc.so.6 libdaemon.so.0"
RDEPENDS_avahi-autoipd = "avahi-libs bash glibc libdaemon shadow-utils"
RPM_SONAME_PROV_avahi-compat-howl = "libhowl.so.0"
RPM_SONAME_REQ_avahi-compat-howl = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_avahi-compat-howl = "avahi-libs dbus-libs glibc"
RPM_SONAME_REQ_avahi-compat-howl-devel = "libhowl.so.0"
RPROVIDES_avahi-compat-howl-devel = "avahi-compat-howl-dev (= 0.7)"
RDEPENDS_avahi-compat-howl-devel = "avahi-compat-howl avahi-devel pkgconf-pkg-config"
RPM_SONAME_PROV_avahi-compat-libdns_sd = "libdns_sd.so.1"
RPM_SONAME_REQ_avahi-compat-libdns_sd = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_avahi-compat-libdns_sd = "avahi-libs dbus-libs glibc"
RPM_SONAME_REQ_avahi-compat-libdns_sd-devel = "libdns_sd.so.1"
RPROVIDES_avahi-compat-libdns_sd-devel = "avahi-compat-libdns_sd-dev (= 0.7)"
RDEPENDS_avahi-compat-libdns_sd-devel = "avahi-compat-libdns_sd avahi-devel pkgconf-pkg-config"
RPM_SONAME_PROV_avahi-glib = "libavahi-glib.so.1"
RPM_SONAME_REQ_avahi-glib = "ld-linux-aarch64.so.1 libavahi-common.so.3 libc.so.6 libglib-2.0.so.0 libpthread.so.0"
RDEPENDS_avahi-glib = "avahi-libs glib2 glibc"
RPM_SONAME_PROV_avahi-gobject = "libavahi-gobject.so.0"
RPM_SONAME_REQ_avahi-gobject = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_avahi-gobject = "avahi-glib avahi-libs dbus-libs glib2 glibc"
RPM_SONAME_PROV_avahi-ui-gtk3 = "libavahi-ui-gtk3.so.0"
RPM_SONAME_REQ_avahi-ui-gtk3 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libdl.so.2 libgdbm.so.6 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_avahi-ui-gtk3 = "atk avahi-glib avahi-libs cairo cairo-gobject dbus-libs gdbm-libs gdk-pixbuf2 glib2 glibc gtk3 pango"
RDEPENDS_python3-avahi = "avahi avahi-libs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/avahi-ui-gtk3-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/avahi-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/avahi-autoipd-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/avahi-glib-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/avahi-gobject-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-avahi-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/avahi-compat-howl-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/avahi-compat-howl-devel-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/avahi-compat-libdns_sd-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/avahi-compat-libdns_sd-devel-0.7-19.el8.aarch64.rpm \
          "

SRC_URI[avahi.sha256sum] = "99c797c362dcab26dcd7248faafa4a7d9534862c06cf02983fb53f48246906bd"
SRC_URI[avahi-autoipd.sha256sum] = "e6c3370643eaab2660925be5aeacafa0feff1c5666421db685e6d4e0155d4587"
SRC_URI[avahi-compat-howl.sha256sum] = "26f5a37059ee70fada013de13103b6da6c87a6d5fc3b261f97d6ea424fb28a1d"
SRC_URI[avahi-compat-howl-devel.sha256sum] = "2797d91323a512a2a44d1c1f00f5b7804847267fd5f303aca312c84ce65fe08a"
SRC_URI[avahi-compat-libdns_sd.sha256sum] = "fe0ef387c881b592940bd5ad6a080a9b6bad782790ac0993404fc8674cbc5100"
SRC_URI[avahi-compat-libdns_sd-devel.sha256sum] = "41b373cb05e32cda27c08e2b2d3ddcb913f01ee2f3785c5d026ed0426ebe0341"
SRC_URI[avahi-glib.sha256sum] = "9664d5eab13c5dc1d92bcf7f031a88bd2c95e0e7953538cbb5175e2212faca5d"
SRC_URI[avahi-gobject.sha256sum] = "76b3eb4eeac282ca1b8e44a0e707943d5e7309b2986219fb1989690a68bc7774"
SRC_URI[avahi-ui-gtk3.sha256sum] = "e9db72059c8b6376c499fe50cfd8a68d5f887df273200470c2ba5e818113c0ed"
SRC_URI[python3-avahi.sha256sum] = "65fceef28fd6f1a84aea0b86bcc704c9491305c62050f898a8a2b1643825131d"
