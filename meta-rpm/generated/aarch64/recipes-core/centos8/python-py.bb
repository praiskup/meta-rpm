SUMMARY = "generated recipe based on python-py srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-py = "platform-python platform-python-setuptools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-py-1.5.3-4.el8.noarch.rpm \
          "

SRC_URI[python3-py.sha256sum] = "1f1d5883ad101c202c483d6dda638d728f1245697753bde9e73c701c8c5acefb"
