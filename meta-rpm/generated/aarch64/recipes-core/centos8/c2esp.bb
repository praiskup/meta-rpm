SUMMARY = "generated recipe based on c2esp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-filters cups-libs jbigkit pkgconfig-native zlib"
RPM_SONAME_REQ_c2esp = "ld-linux-aarch64.so.1 libc.so.6 libcups.so.2 libcupsfilters.so.1 libcupsimage.so.2 libjbig85.so.2.1 libz.so.1"
RDEPENDS_c2esp = "cups-filesystem cups-filters-libs cups-libs glibc jbigkit-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/c2esp-2.7-14.el8.aarch64.rpm \
          "

SRC_URI[c2esp.sha256sum] = "2e655f9c67ca6d7a72e3a558a7a9af5c4179f116e173f59582c9dd64ea2c214d"
