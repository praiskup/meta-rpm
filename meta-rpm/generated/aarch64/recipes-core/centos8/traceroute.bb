SUMMARY = "generated recipe based on traceroute srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_traceroute = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_traceroute = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/traceroute-2.1.0-6.el8.aarch64.rpm \
          "

SRC_URI[traceroute.sha256sum] = "1914ffd356506dd43b92fd943594f3216d4c6416aae71c3863937978effc5f80"
