SUMMARY = "generated recipe based on recode srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_recode = "librecode.so.0"
RPM_SONAME_REQ_recode = "ld-linux-aarch64.so.1 libc.so.6 librecode.so.0"
RDEPENDS_recode = "bash glibc info"
RPM_SONAME_REQ_recode-devel = "librecode.so.0"
RPROVIDES_recode-devel = "recode-dev (= 3.6)"
RDEPENDS_recode-devel = "recode"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/recode-3.6-47.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/recode-devel-3.6-47.el8.aarch64.rpm \
          "

SRC_URI[recode.sha256sum] = "57f4ed8667cf22b5733e731da6c0bacd70c70a1984f684ae6af7a27b66f765e1"
SRC_URI[recode-devel.sha256sum] = "4ceb4ec3f05ce3e24180af892fc35928984143a0f963059daf89daf34f94818d"
