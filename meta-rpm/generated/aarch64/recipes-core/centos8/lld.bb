SUMMARY = "generated recipe based on lld srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc llvm pkgconfig-native"
RPM_SONAME_REQ_lld = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libgcc_s.so.1 liblldCOFF.so.9 liblldCommon.so.9 liblldDriver.so.9 liblldELF.so.9 liblldMinGW.so.9 liblldWasm.so.9 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_lld = "glibc libgcc libstdc++ lld-libs llvm-libs"
RPM_SONAME_REQ_lld-devel = "liblldCOFF.so.9 liblldCommon.so.9 liblldCore.so.9 liblldDriver.so.9 liblldELF.so.9 liblldMachO.so.9 liblldMinGW.so.9 liblldReaderWriter.so.9 liblldWasm.so.9 liblldYAML.so.9"
RPROVIDES_lld-devel = "lld-dev (= 9.0.1)"
RDEPENDS_lld-devel = "lld-libs"
RPM_SONAME_PROV_lld-libs = "liblldCOFF.so.9 liblldCommon.so.9 liblldCore.so.9 liblldDriver.so.9 liblldELF.so.9 liblldMachO.so.9 liblldMinGW.so.9 liblldReaderWriter.so.9 liblldWasm.so.9 liblldYAML.so.9"
RPM_SONAME_REQ_lld-libs = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libgcc_s.so.1 liblldCOFF.so.9 liblldCommon.so.9 liblldCore.so.9 liblldMachO.so.9 liblldReaderWriter.so.9 liblldYAML.so.9 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_lld-libs = "glibc libgcc libstdc++ llvm-libs"
RPM_SONAME_REQ_lld-test = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libgcc_s.so.1 liblldDriver.so.9 liblldMachO.so.9 liblldYAML.so.9 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_lld-test = "bash glibc libgcc libstdc++ lld lld-libs llvm-libs llvm-test python3-lit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lld-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lld-devel-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lld-libs-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lld-test-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
          "

SRC_URI[lld.sha256sum] = "538fa6e3fc5ce276788ab49a6ac0f9d64f7ca671f00f9ce240461b34b6d19a15"
SRC_URI[lld-devel.sha256sum] = "1ce2bda348e5e5f13adf9f2af85c63fa559e56621be3a6f3cde4ece87febabba"
SRC_URI[lld-libs.sha256sum] = "d682a3c8458912b4e559d386ddceeae82ba5c733815fb2fb0ccc26413b8f6d32"
SRC_URI[lld-test.sha256sum] = "327ba591a015f5a7615301528d4f020e77adb0ae2ecde39fac540801e7516930"
