SUMMARY = "generated recipe based on lame srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lame-devel = "libmp3lame.so.0"
RPROVIDES_lame-devel = "lame-dev (= 3.100)"
RDEPENDS_lame-devel = "lame-libs"
RPM_SONAME_PROV_lame-libs = "libmp3lame.so.0"
RPM_SONAME_REQ_lame-libs = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_lame-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lame-libs-3.100-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lame-devel-3.100-6.el8.aarch64.rpm \
          "

SRC_URI[lame-devel.sha256sum] = "edbd4babaf599aeeb8fdc05ededd4ebbb4a5c87dc2c28c6c5fdf0468f180bdcd"
SRC_URI[lame-libs.sha256sum] = "3fb2526b44df32fc8275c5ae0545a69582367a22b691dbaafc2808b3f8c97243"
