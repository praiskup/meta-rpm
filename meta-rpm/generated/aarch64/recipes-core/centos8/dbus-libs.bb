SUMMARY = "generated recipe based on dbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemd-libs"
RPM_SONAME_PROV_dbus-libs = "libdbus-1.so.3"
RPM_SONAME_REQ_dbus-libs = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libsystemd.so.0"
RDEPENDS_dbus-libs = "glibc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbus-libs-1.12.8-10.el8_2.aarch64.rpm \
          "

SRC_URI[dbus-libs.sha256sum] = "5a83122cbfac17bbd1faf868454c23ac6b929e0136babfb5885571b6d6b8900e"
