SUMMARY = "generated recipe based on zlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_zlib = "libz.so.1"
RPM_SONAME_REQ_zlib = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_zlib = "glibc"
RPM_SONAME_REQ_zlib-devel = "libz.so.1"
RPROVIDES_zlib-devel = "zlib-dev (= 1.2.11)"
RDEPENDS_zlib-devel = "pkgconf-pkg-config zlib"
RDEPENDS_zlib-static = "zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/zlib-1.2.11-16.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/zlib-devel-1.2.11-16.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/zlib-static-1.2.11-16.el8_2.aarch64.rpm \
          "

SRC_URI[zlib.sha256sum] = "4a2fb9e372305315838c5edad08827cb6b857aedb1f56880bec65194f2dd3dd0"
SRC_URI[zlib-devel.sha256sum] = "f36bd4f92324070bd95c54cdd54eb90b7ff4e44253a4ec80f074b935e69fdf28"
SRC_URI[zlib-static.sha256sum] = "a341b50aaf8bedf51b0b84856628b72da74f33ab1b137e3ac04f7339c55ba213"
