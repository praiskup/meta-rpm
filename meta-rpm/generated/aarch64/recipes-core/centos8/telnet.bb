SUMMARY = "generated recipe based on telnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_telnet = "ld-linux-aarch64.so.1 libc.so.6 libncurses.so.6 libtinfo.so.6 libutil.so.1"
RDEPENDS_telnet = "glibc ncurses-libs"
RPM_SONAME_REQ_telnet-server = "ld-linux-aarch64.so.1 libc.so.6 libutil.so.1"
RDEPENDS_telnet-server = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/telnet-0.17-73.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/telnet-server-0.17-73.el8_1.1.aarch64.rpm \
          "

SRC_URI[telnet.sha256sum] = "04e15af5e974cfe51b52b5851ecd9cc8454bf7246ee129431ad2d05548f9e643"
SRC_URI[telnet-server.sha256sum] = "40769ea4bd4cc06cbc3f677ea25dd78626e76d3d6869e45c4ffb6d8a2b277945"
