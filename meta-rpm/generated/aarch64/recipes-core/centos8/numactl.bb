SUMMARY = "generated recipe based on numactl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_numactl = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libnuma.so.1 librt.so.1"
RDEPENDS_numactl = "glibc numactl-libs"
RPM_SONAME_REQ_numactl-devel = "libnuma.so.1"
RPROVIDES_numactl-devel = "numactl-dev (= 2.0.12)"
RDEPENDS_numactl-devel = "numactl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_numactl-libs = "libnuma.so.1"
RPM_SONAME_REQ_numactl-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_numactl-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/numactl-2.0.12-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/numactl-devel-2.0.12-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/numactl-libs-2.0.12-9.el8.aarch64.rpm \
          "

SRC_URI[numactl.sha256sum] = "14883a90b0199533f25205949fc611f9e2ad94bbaf9f379d4835d3dfd0c80caf"
SRC_URI[numactl-devel.sha256sum] = "88386256de4cc1905ade4f222a45961495b580420d3af4bdbba9d6a25f2ad318"
SRC_URI[numactl-libs.sha256sum] = "4a66eae8743226c88c9aafbcf0c83a34a0ac3643a5fe84e392dffbfb81cd61cd"
