SUMMARY = "generated recipe based on mesa-libGLU srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native"
RPM_SONAME_PROV_mesa-libGLU = "libGLU.so.1"
RPM_SONAME_REQ_mesa-libGLU = "ld-linux-aarch64.so.1 libGL.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_mesa-libGLU = "glibc libgcc libglvnd-glx libstdc++"
RPM_SONAME_REQ_mesa-libGLU-devel = "libGLU.so.1"
RPROVIDES_mesa-libGLU-devel = "mesa-libGLU-dev (= 9.0.0)"
RDEPENDS_mesa-libGLU-devel = "gl-manpages libglvnd-devel mesa-libGLU pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libGLU-9.0.0-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libGLU-devel-9.0.0-15.el8.aarch64.rpm \
          "

SRC_URI[mesa-libGLU.sha256sum] = "34112c8e097ec101835bdefaf767499d0aa94d4908f9b08e06ad480acf6733ae"
SRC_URI[mesa-libGLU-devel.sha256sum] = "5e31ff86396a2b0d3428bd6c4ef6444778baf18fc71c416508b6ff2f71ea2eaa"
