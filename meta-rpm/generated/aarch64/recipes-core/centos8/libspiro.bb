SUMMARY = "generated recipe based on libspiro srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libspiro = "libspiro.so.0"
RPM_SONAME_REQ_libspiro = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libspiro = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libspiro-20150131-8.el8.aarch64.rpm \
          "

SRC_URI[libspiro.sha256sum] = "025de4f9b7b292c2328a938f99bb040208870eadb1ce3b04f583fc82626e0f44"
