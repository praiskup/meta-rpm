SUMMARY = "generated recipe based on perl-gettext srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Locale-gettext = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Locale-gettext = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Locale-gettext-1.07-9.el8.aarch64.rpm \
          "

SRC_URI[perl-Locale-gettext.sha256sum] = "0cab8bba02800d4da5f7b024998f9966aea82beebe74bb802acc12e2e9c3afa0"
