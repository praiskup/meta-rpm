SUMMARY = "generated recipe based on socket_wrapper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_socket_wrapper = "libsocket_wrapper.so.0"
RPM_SONAME_REQ_socket_wrapper = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 libsocket_wrapper.so.0"
RDEPENDS_socket_wrapper = "cmake-filesystem glibc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/socket_wrapper-1.2.3-1.el8.aarch64.rpm \
          "

SRC_URI[socket_wrapper.sha256sum] = "9182419c6474152f6cdb59b543ada25c9341b4df91e9d1d8507a8182b735c785"
