SUMMARY = "generated recipe based on chrony srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap libedit libseccomp nettle pkgconfig-native"
RPM_SONAME_REQ_chrony = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libedit.so.0 libm.so.6 libnettle.so.6 libpthread.so.0 libseccomp.so.2"
RDEPENDS_chrony = "bash glibc libcap libedit libseccomp nettle shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/chrony-3.5-1.el8.aarch64.rpm \
          "

SRC_URI[chrony.sha256sum] = "26408472966a698ac0ac5bfd326e749af9c82029eb443fb04d3b897ef3260b5c"
