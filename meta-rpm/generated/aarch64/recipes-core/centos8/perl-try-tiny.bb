SUMMARY = "generated recipe based on perl-Try-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Try-Tiny = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Try-Tiny-0.30-2.el8.noarch.rpm \
          "

SRC_URI[perl-Try-Tiny.sha256sum] = "e8d3c0bd7fb1d99e205411c660ddb6c29ff1bf23abcf01fc197eb88d076311a9"
