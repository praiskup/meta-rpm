SUMMARY = "generated recipe based on perl-Importer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Importer = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Importer-0.025-1.el8.noarch.rpm \
          "

SRC_URI[perl-Importer.sha256sum] = "c5c677f3b13f4ebb407a0a240f31988410eebe35001be5bf87c1a4b1215b348d"
