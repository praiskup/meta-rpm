SUMMARY = "generated recipe based on perl-Params-Classify srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Classify = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Classify = "glibc perl-Devel-CallChecker perl-Exporter perl-Scalar-List-Utils perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Params-Classify-0.015-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Params-Classify.sha256sum] = "1f35b8bcf4fbe46ddb5456fdbe6986ce89b65b76ff76403371efe5b61cf6a012"
