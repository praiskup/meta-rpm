SUMMARY = "generated recipe based on xrestop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxres ncurses pkgconfig-native"
RPM_SONAME_REQ_xrestop = "ld-linux-aarch64.so.1 libX11.so.6 libXRes.so.1 libXext.so.6 libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_xrestop = "glibc libX11 libXext libXres ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xrestop-0.4-21.el8.aarch64.rpm \
          "

SRC_URI[xrestop.sha256sum] = "d8ecc6f05de80552f242263445252bc40b9a8a3896e379db1a1a2fa318c98c97"
