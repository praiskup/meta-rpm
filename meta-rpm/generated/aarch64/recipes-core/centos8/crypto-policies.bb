SUMMARY = "generated recipe based on crypto-policies srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_crypto-policies = "bash coreutils grep platform-python sed"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/crypto-policies-20191128-2.git23e1bf1.el8.noarch.rpm \
          "

SRC_URI[crypto-policies.sha256sum] = "f4c0c0c82d8de26e41b5a3b777bb94eee85ea14671e7b55b368edf57cd85282f"
