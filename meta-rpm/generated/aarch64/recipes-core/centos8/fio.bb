SUMMARY = "generated recipe based on fio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ceph libaio numactl pkgconfig-native rdma-core zlib"
RPM_SONAME_REQ_fio = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libdl.so.2 libibverbs.so.1 libm.so.6 libnuma.so.1 libpthread.so.0 librados.so.2 librbd.so.1 librdmacm.so.1 librt.so.1 libz.so.1"
RDEPENDS_fio = "bash glibc libaio libibverbs librados2 librbd1 librdmacm numactl-libs platform-python zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fio-3.7-3.el8.aarch64.rpm \
          "

SRC_URI[fio.sha256sum] = "f84727f187b7878da3b57742a08681b7fee8d0c5b097a154607f25dafbc54762"
