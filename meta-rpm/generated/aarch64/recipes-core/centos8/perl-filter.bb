SUMMARY = "generated recipe based on perl-Filter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Filter = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Filter = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Filter-1.58-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Filter.sha256sum] = "aec42190f460f8a383dc709b7d9e7d5a242393cf14edade5ed1e988789d4c55c"
