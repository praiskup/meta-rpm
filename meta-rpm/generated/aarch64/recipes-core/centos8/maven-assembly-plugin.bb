SUMMARY = "generated recipe based on maven-assembly-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-assembly-plugin = "apache-commons-codec apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem maven-archiver maven-artifact-transfer maven-common-artifact-filters maven-file-management maven-filtering maven-lib maven-shared-io plexus-archiver plexus-interpolation plexus-io plexus-utils"
RDEPENDS_maven-assembly-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-assembly-plugin-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-assembly-plugin-javadoc-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-assembly-plugin.sha256sum] = "63506e4a561b53f0fcc0b929d4167898fbc528913cceb868e645e8785ad15ff2"
SRC_URI[maven-assembly-plugin-javadoc.sha256sum] = "695a8764f15f69bf20dcd8ede03f5d24b8c6e68b7d09daa429593bce2d20933c"
