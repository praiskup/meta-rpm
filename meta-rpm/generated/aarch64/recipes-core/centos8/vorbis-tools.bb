SUMMARY = "generated recipe based on vorbis-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl flac libao libgcc libogg libvorbis pkgconfig-native speex"
RPM_SONAME_REQ_vorbis-tools = "ld-linux-aarch64.so.1 libFLAC.so.8 libao.so.4 libc.so.6 libcurl.so.4 libgcc_s.so.1 libm.so.6 libogg.so.0 libpthread.so.0 libspeex.so.1 libvorbis.so.0 libvorbisenc.so.2 libvorbisfile.so.3"
RDEPENDS_vorbis-tools = "flac-libs glibc libao libcurl libgcc libogg libvorbis speex"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vorbis-tools-1.4.0-28.el8.aarch64.rpm \
          "

SRC_URI[vorbis-tools.sha256sum] = "2a501d2721804677b124e865316f343851816970a4a755e53061c4bcde389b3e"
