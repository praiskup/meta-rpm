SUMMARY = "generated recipe based on gsm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_gsm = "libgsm.so.1"
RPM_SONAME_REQ_gsm = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_gsm = "glibc"
RPM_SONAME_REQ_gsm-devel = "libgsm.so.1"
RPROVIDES_gsm-devel = "gsm-dev (= 1.0.17)"
RDEPENDS_gsm-devel = "gsm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gsm-1.0.17-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gsm-devel-1.0.17-5.el8.aarch64.rpm \
          "

SRC_URI[gsm.sha256sum] = "2feeabbbfa8ba99a9d2f02025a9cb8a79ec6f4acf98658725d52583ddf86bddb"
SRC_URI[gsm-devel.sha256sum] = "2649a7f20a890ff8c47495e41a4ac40a45b74b3aa4caf6ffb411fada23a9b56b"
