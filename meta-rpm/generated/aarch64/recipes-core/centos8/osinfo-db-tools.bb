SUMMARY = "generated recipe based on osinfo-db-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib libarchive libxml2 pkgconfig-native"
RPM_SONAME_REQ_osinfo-db-tools = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpthread.so.0 libxml2.so.2"
RDEPENDS_osinfo-db-tools = "glib2 glibc gvfs json-glib libarchive libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/osinfo-db-tools-1.5.0-4.el8.aarch64.rpm \
          "

SRC_URI[osinfo-db-tools.sha256sum] = "c0641dfe36a882aa41c38d0b6fd3952d21bf67263704343a9d2f4f22c21971e5"
