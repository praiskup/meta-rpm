SUMMARY = "generated recipe based on gcc-toolset-9-valgrind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-valgrind = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0"
RDEPENDS_gcc-toolset-9-valgrind = "gcc-toolset-9-runtime glibc libgcc perl-interpreter"
RPROVIDES_gcc-toolset-9-valgrind-devel = "gcc-toolset-9-valgrind-dev (= 3.15.0)"
RDEPENDS_gcc-toolset-9-valgrind-devel = "gcc-toolset-9-runtime gcc-toolset-9-valgrind pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-valgrind-3.15.0-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-valgrind-devel-3.15.0-9.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-valgrind.sha256sum] = "ae64c9244d282ec687bf967344deb09101aaa22dc81bd54d3f7374472b7739f0"
SRC_URI[gcc-toolset-9-valgrind-devel.sha256sum] = "6408e572f3600d38fb92ff64a5d10818c4d456a0a4d12cde89b6dd1a79c0b1e5"
