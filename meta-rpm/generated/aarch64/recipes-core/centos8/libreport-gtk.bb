SUMMARY = "generated recipe based on libreport srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk augeas cairo gdk-pixbuf glib-2.0 gtk+3 libtar pango pkgconfig-native satyr systemd-libs"
RPM_SONAME_PROV_libreport-gtk = "libreport-gtk.so.0"
RPM_SONAME_REQ_libreport-gtk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libaugeas.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libreport-gtk.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-gtk = "atk augeas-libs cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libreport libreport-plugin-reportuploader libtar pango satyr systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-gtk-2.9.5-10.el8.aarch64.rpm \
          "

SRC_URI[libreport-gtk.sha256sum] = "5518d14368b4d9812aed92a079198a03a09d9044c15d2f576fa617a8233770d8"
