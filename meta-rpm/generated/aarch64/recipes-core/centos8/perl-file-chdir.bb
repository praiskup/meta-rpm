SUMMARY = "generated recipe based on perl-File-chdir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-chdir = "perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-chdir-0.1011-5.el8.noarch.rpm \
          "

SRC_URI[perl-File-chdir.sha256sum] = "f2eb0420ccad39db24dd8b1b32a7ec03b89256ab5c6150d9d1e5d34cfb2d90d8"
