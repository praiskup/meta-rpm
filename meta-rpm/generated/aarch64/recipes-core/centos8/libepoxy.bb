SUMMARY = "generated recipe based on libepoxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd pkgconfig-native"
RPM_SONAME_PROV_libepoxy = "libepoxy.so.0"
RPM_SONAME_REQ_libepoxy = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libepoxy = "glibc"
RPM_SONAME_REQ_libepoxy-devel = "libepoxy.so.0"
RPROVIDES_libepoxy-devel = "libepoxy-dev (= 1.5.3)"
RDEPENDS_libepoxy-devel = "libepoxy libglvnd-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libepoxy-1.5.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libepoxy-devel-1.5.3-1.el8.aarch64.rpm \
          "

SRC_URI[libepoxy.sha256sum] = "f85f149281cfc2e86f2ec7a95b0d95516ba0ee752bdee4ed752ead97063ed448"
SRC_URI[libepoxy-devel.sha256sum] = "cbe53fd6d36b84f6f40a65a65d71802a0ebfc4c72e26fdd8958b85d7bb0ab1bd"
