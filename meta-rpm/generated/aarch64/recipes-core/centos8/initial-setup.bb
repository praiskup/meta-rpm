SUMMARY = "generated recipe based on initial-setup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_initial-setup = "anaconda-tui bash platform-python python3-libreport systemd util-linux"
RDEPENDS_initial-setup-gui = "anaconda-gui bash gtk3 initial-setup metacity platform-python xorg-x11-server-Xorg xorg-x11-xinit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/initial-setup-0.3.62.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/initial-setup-gui-0.3.62.1-1.el8.aarch64.rpm \
          "

SRC_URI[initial-setup.sha256sum] = "2b915787848f76e455c79b9d840883e5acc560637f5fc6017c576101de143fd7"
SRC_URI[initial-setup-gui.sha256sum] = "90fbdb7ac179275e7ee6b91e12f294043634047ccb1d010c1bb380ede72145be"
