SUMMARY = "generated recipe based on hunspell-dsb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-dsb = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-dsb-1.4.8-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-dsb.sha256sum] = "0e6660162f66b1b3fb8efdb85848ce03b0dcd541a3d628aa735f78bfa03f44eb"
