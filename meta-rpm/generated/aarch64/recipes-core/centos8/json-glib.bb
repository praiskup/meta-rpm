SUMMARY = "generated recipe based on json-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_json-glib = "libjson-glib-1.0.so.0"
RPM_SONAME_REQ_json-glib = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_json-glib = "glib2 glibc"
RPM_SONAME_REQ_json-glib-devel = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0"
RPROVIDES_json-glib-devel = "json-glib-dev (= 1.4.4)"
RDEPENDS_json-glib-devel = "glib2 glib2-devel glibc json-glib pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/json-glib-devel-1.4.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/json-glib-1.4.4-1.el8.aarch64.rpm \
          "

SRC_URI[json-glib.sha256sum] = "01e70480bb032d5e0b60c5e732d4302d3a0ce73d1502a1729280d2b36e7e1c1a"
SRC_URI[json-glib-devel.sha256sum] = "ac81cbe4c937fc98646f0808a6e77254aa5f3af97066acb0e34afeb0040eeacc"
