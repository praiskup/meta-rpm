SUMMARY = "generated recipe based on net-snmp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libxcrypt lm-sensors mariadb-connector-c openssl perl pkgconfig-native rpm zlib"
RPM_SONAME_REQ_net-snmp = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libnetsnmptrapd.so.35 libperl.so.5.26 libpthread.so.0 libresolv.so.2 librpm.so.8 librpmio.so.8 libsensors.so.4 libssl.so.1.1 libutil.so.1 libz.so.1"
RDEPENDS_net-snmp = "bash glibc libxcrypt lm_sensors-libs mariadb-connector-c net-snmp-agent-libs net-snmp-libs openssl-libs perl-Data-Dumper perl-IO perl-interpreter perl-libs rpm-libs systemd zlib"
RPM_SONAME_PROV_net-snmp-agent-libs = "libnetsnmpagent.so.35 libnetsnmphelpers.so.35 libnetsnmpmibs.so.35 libnetsnmptrapd.so.35"
RPM_SONAME_REQ_net-snmp-agent-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libperl.so.5.26 libpthread.so.0 libresolv.so.2 librpm.so.8 librpmio.so.8 libsensors.so.4 libssl.so.1.1 libutil.so.1 libz.so.1"
RDEPENDS_net-snmp-agent-libs = "glibc libxcrypt lm_sensors-libs mariadb-connector-c net-snmp-libs openssl-libs perl-libs rpm-libs zlib"
RPM_SONAME_REQ_net-snmp-devel = "libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmphelpers.so.35 libnetsnmpmibs.so.35 libnetsnmptrapd.so.35"
RPROVIDES_net-snmp-devel = "net-snmp-dev (= 5.8)"
RDEPENDS_net-snmp-devel = "bash elfutils-devel elfutils-libelf-devel gcc lm_sensors-devel net-snmp-agent-libs net-snmp-libs openssl-devel perl-devel rpm-devel"
RPM_SONAME_PROV_net-snmp-libs = "libnetsnmp.so.35"
RPM_SONAME_REQ_net-snmp-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libssl.so.1.1"
RDEPENDS_net-snmp-libs = "glibc openssl-libs"
RPM_SONAME_REQ_net-snmp-utils = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libm.so.6 libnetsnmp.so.35 libssl.so.1.1"
RDEPENDS_net-snmp-utils = "glibc net-snmp-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/net-snmp-5.8-14.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/net-snmp-agent-libs-5.8-14.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/net-snmp-devel-5.8-14.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/net-snmp-utils-5.8-14.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/net-snmp-libs-5.8-14.el8_2.1.aarch64.rpm \
          "

SRC_URI[net-snmp.sha256sum] = "b25340f40f5807fd3a201541a3038a815279fe52f126f294c2cfabc473271758"
SRC_URI[net-snmp-agent-libs.sha256sum] = "fa46a4569a6e887fa36ce46efb7c245bcbfd9a3e6377f09cfd725c83266cbc38"
SRC_URI[net-snmp-devel.sha256sum] = "1de606852d46c4d9e3fb9e80793ab8596ad9d8016e5bc9e8d693fecd88ada1ca"
SRC_URI[net-snmp-libs.sha256sum] = "5cc1062c9f98d0312ac7f56f7c04a5f421baea15dd5fb4e4bbd3fe9ff7104050"
SRC_URI[net-snmp-utils.sha256sum] = "59ef96d6d55f8de0d451e06288f3d50c32def4f9c7a8f43fc8646c72455db511"
