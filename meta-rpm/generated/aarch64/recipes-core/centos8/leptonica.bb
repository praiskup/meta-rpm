SUMMARY = "generated recipe based on leptonica srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "giflib libjpeg-turbo libpng libwebp pkgconfig-native tiff zlib"
RPM_SONAME_PROV_leptonica = "liblept.so.5"
RPM_SONAME_REQ_leptonica = "ld-linux-aarch64.so.1 libc.so.6 libgif.so.7 libjpeg.so.62 libm.so.6 libpng16.so.16 libtiff.so.5 libwebp.so.7 libz.so.1"
RDEPENDS_leptonica = "giflib glibc libjpeg-turbo libpng libtiff libwebp zlib"
RPM_SONAME_REQ_leptonica-devel = "liblept.so.5"
RPROVIDES_leptonica-devel = "leptonica-dev (= 1.76.0)"
RDEPENDS_leptonica-devel = "leptonica pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/leptonica-1.76.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/leptonica-devel-1.76.0-2.el8.aarch64.rpm \
          "

SRC_URI[leptonica.sha256sum] = "a783562f607015b80a9afb3a11e3ceb284ca3d5d8d2cbfb7ca0175476c591f44"
SRC_URI[leptonica-devel.sha256sum] = "5af96279af27dfa53188048864d202e96e0859be2acc7cd6077a072e98a0c3e4"
