SUMMARY = "generated recipe based on socat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native readline"
RPM_SONAME_REQ_socat = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libreadline.so.7 librt.so.1 libssl.so.1.1 libutil.so.1"
RDEPENDS_socat = "glibc openssl-libs readline"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/socat-1.7.3.3-2.el8.aarch64.rpm \
          "

SRC_URI[socat.sha256sum] = "52499753fae7bc329992c766bdf96df780742ff9926bc511c8ce544951d4710f"
