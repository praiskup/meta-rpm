SUMMARY = "generated recipe based on tpm2-abrmd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs glib-2.0 pkgconfig-native tpm2-tss"
RPM_SONAME_PROV_tpm2-abrmd = "libtss2-tcti-tabrmd.so.0"
RPM_SONAME_REQ_tpm2-abrmd = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libtss2-mu.so.0 libtss2-sys.so.0"
RDEPENDS_tpm2-abrmd = "bash dbus-libs glib2 glibc systemd tpm2-tss"
RPM_SONAME_REQ_tpm2-abrmd-devel = "libtss2-tcti-tabrmd.so.0"
RPROVIDES_tpm2-abrmd-devel = "tpm2-abrmd-dev (= 2.1.1)"
RDEPENDS_tpm2-abrmd-devel = "glib2-devel pkgconf-pkg-config tpm2-abrmd tpm2-tss-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm2-abrmd-2.1.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tpm2-abrmd-devel-2.1.1-3.el8.aarch64.rpm \
          "

SRC_URI[tpm2-abrmd.sha256sum] = "a79063250c134090848eca3e0e9cb6a596074ce46b3067ec0e1b0cab0172a0d1"
SRC_URI[tpm2-abrmd-devel.sha256sum] = "ef1cc6cbacb6d64bc0b5f166b155f1c45a504bb228bf3087ad4f72bae940654f"
