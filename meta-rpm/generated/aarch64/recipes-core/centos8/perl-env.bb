SUMMARY = "generated recipe based on perl-Env srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Env = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Env-1.04-395.el8.noarch.rpm \
          "

SRC_URI[perl-Env.sha256sum] = "815fd786796dfec255b40b5285310f39dbb605fe63d89ecdbff93f3017b87b67"
