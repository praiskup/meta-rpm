SUMMARY = "generated recipe based on xfsdump srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr libgcc libuuid ncurses pkgconfig-native xfsprogs"
RPM_SONAME_REQ_xfsdump = "ld-linux-aarch64.so.1 libattr.so.1 libc.so.6 libgcc_s.so.1 libhandle.so.1 libncurses.so.6 libpthread.so.0 libtinfo.so.6 libuuid.so.1"
RDEPENDS_xfsdump = "attr glibc libattr libgcc libuuid ncurses-libs xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xfsdump-3.1.8-2.el8.aarch64.rpm \
          "

SRC_URI[xfsdump.sha256sum] = "63bd2e9c7b02ce2fe75e64b4793d26b47ffe75dd345599b93766fa726d7c7c04"
