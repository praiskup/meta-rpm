SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit libblkid libcap-ng libmount libselinux libuser libutempter libuuid libxcrypt ncurses pam pkgconfig-native systemd-libs zlib"
RPM_SONAME_PROV_libfdisk = "libfdisk.so.1"
RPM_SONAME_REQ_libfdisk = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libuuid.so.1"
RDEPENDS_libfdisk = "glibc libblkid libuuid"
RPM_SONAME_REQ_libfdisk-devel = "libfdisk.so.1"
RPROVIDES_libfdisk-devel = "libfdisk-dev (= 2.32.1)"
RDEPENDS_libfdisk-devel = "libblkid-devel libfdisk libuuid-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libsmartcols = "libsmartcols.so.1"
RPM_SONAME_REQ_libsmartcols = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsmartcols = "glibc"
RPM_SONAME_REQ_libsmartcols-devel = "libsmartcols.so.1"
RPROVIDES_libsmartcols-devel = "libsmartcols-dev (= 2.32.1)"
RDEPENDS_libsmartcols-devel = "libsmartcols pkgconf-pkg-config"
RPM_SONAME_REQ_util-linux = "ld-linux-aarch64.so.1 libaudit.so.1 libblkid.so.1 libc.so.6 libcap-ng.so.0 libcrypt.so.1 libfdisk.so.1 libm.so.6 libmount.so.1 libncursesw.so.6 libpam.so.0 libpam_misc.so.0 librt.so.1 libselinux.so.1 libsmartcols.so.1 libsystemd.so.0 libtinfo.so.6 libudev.so.1 libutempter.so.0 libutil.so.1 libuuid.so.1 libz.so.1"
RDEPENDS_util-linux = "audit-libs bash coreutils glibc libblkid libcap-ng libfdisk libmount libselinux libsmartcols libutempter libuuid libxcrypt ncurses-libs pam systemd-libs zlib"
RPM_SONAME_REQ_util-linux-user = "ld-linux-aarch64.so.1 libc.so.6 libpam.so.0 libpam_misc.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_util-linux-user = "glibc libselinux libuser pam util-linux"
RPM_SONAME_REQ_uuidd = "ld-linux-aarch64.so.1 libc.so.6 librt.so.1 libsystemd.so.0 libuuid.so.1"
RDEPENDS_uuidd = "bash glibc libuuid shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libfdisk-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libfdisk-devel-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsmartcols-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsmartcols-devel-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/util-linux-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/util-linux-user-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/uuidd-2.32.1-22.el8.aarch64.rpm \
          "

SRC_URI[libfdisk.sha256sum] = "1a5effb774547364f567f532c9399b9f2ed23f7f5046b5e0e8a61e039fcd2c7d"
SRC_URI[libfdisk-devel.sha256sum] = "f352d67b3b2831fc7c5cc5fffd78db2ef42e99774fb030d4b2863e2fc3d0b677"
SRC_URI[libsmartcols.sha256sum] = "34210c44da983ae0ce72d931dbf22dda005c3d29d23dfaf37946c0599f4cbe83"
SRC_URI[libsmartcols-devel.sha256sum] = "5edc0b6e73a6f5933798cd601e84af13082f57468a4c8868259f915b3698a3da"
SRC_URI[util-linux.sha256sum] = "08b4e113d09456f62a740f3952733ba8e4109e8ef289757f83fa4e404cd61231"
SRC_URI[util-linux-user.sha256sum] = "ba76b8d760f2d72cc08d406818d44aeeafd28daa9e4d6e547bd9004635280421"
SRC_URI[uuidd.sha256sum] = "88745f9148250c0718dce5f19711222064eff0e1d69feea7aa6dab16e6f45445"
