SUMMARY = "generated recipe based on perl-Pod-Simple srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Simple = "perl-Carp perl-Encode perl-Getopt-Long perl-PathTools perl-Pod-Escapes perl-Text-Tabs+Wrap perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Pod-Simple-3.35-395.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Simple.sha256sum] = "51c3ee5d824bdde0a8faa10c99841c2590c0c26edfb17125aa97945a688c83ed"
