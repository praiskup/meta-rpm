SUMMARY = "generated recipe based on libgphoto2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gd libexif libjpeg-turbo libtool libusb1 libxml2 lockdev pkgconfig-native"
RPM_SONAME_PROV_libgphoto2 = "libgphoto2.so.6 libgphoto2_port.so.12"
RPM_SONAME_REQ_libgphoto2 = "ld-linux-aarch64.so.1 libc.so.6 libexif.so.12 libgd.so.3 libgphoto2.so.6 libgphoto2_port.so.12 libjpeg.so.62 liblockdev.so.1 libltdl.so.7 libm.so.6 libpthread.so.0 libusb-1.0.so.0 libxml2.so.2"
RDEPENDS_libgphoto2 = "bash gd glibc libexif libjpeg-turbo libtool-ltdl libusbx libxml2 lockdev"
RPM_SONAME_REQ_libgphoto2-devel = "libgphoto2.so.6 libgphoto2_port.so.12"
RPROVIDES_libgphoto2-devel = "libgphoto2-dev (= 2.5.16)"
RDEPENDS_libgphoto2-devel = "bash libexif-devel libgphoto2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgphoto2-2.5.16-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgphoto2-devel-2.5.16-3.el8.aarch64.rpm \
          "

SRC_URI[libgphoto2.sha256sum] = "8717257ec17d697dbfc1957abdca744894d5b3e2b66f51ca2d93bec628ba4358"
SRC_URI[libgphoto2-devel.sha256sum] = "472c7d940ab063fddee4a35b1cb1e0c1204d87eb7c92ed35c4dfb376fcd1f6a3"
