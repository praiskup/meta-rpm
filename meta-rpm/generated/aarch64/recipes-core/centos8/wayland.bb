SUMMARY = "generated recipe based on wayland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libffi libxml2 pkgconfig-native"
RPM_SONAME_PROV_libwayland-client = "libwayland-client.so.0"
RPM_SONAME_REQ_libwayland-client = "ld-linux-aarch64.so.1 libc.so.6 libffi.so.6 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_libwayland-client = "glibc libffi"
RPM_SONAME_PROV_libwayland-cursor = "libwayland-cursor.so.0"
RPM_SONAME_REQ_libwayland-cursor = "ld-linux-aarch64.so.1 libc.so.6 libffi.so.6 libm.so.6 libpthread.so.0 librt.so.1 libwayland-client.so.0"
RDEPENDS_libwayland-cursor = "glibc libffi libwayland-client"
RPM_SONAME_PROV_libwayland-egl = "libwayland-egl.so.1"
RPM_SONAME_REQ_libwayland-egl = "libc.so.6"
RDEPENDS_libwayland-egl = "glibc"
RPM_SONAME_PROV_libwayland-server = "libwayland-server.so.0"
RPM_SONAME_REQ_libwayland-server = "ld-linux-aarch64.so.1 libc.so.6 libffi.so.6 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_libwayland-server = "glibc libffi"
RPM_SONAME_REQ_wayland-devel = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxml2.so.2"
RPROVIDES_wayland-devel = "wayland-dev (= 1.17.0)"
RDEPENDS_wayland-devel = "expat glibc libwayland-client libwayland-cursor libwayland-egl libwayland-server libxml2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwayland-client-1.17.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwayland-cursor-1.17.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwayland-egl-1.17.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwayland-server-1.17.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/wayland-devel-1.17.0-1.el8.aarch64.rpm \
          "

SRC_URI[libwayland-client.sha256sum] = "fd52bc2d889b324ca4cfaff7ba4bb47c6fc3303275a211a0d63ebb8dd68f0fb1"
SRC_URI[libwayland-cursor.sha256sum] = "05e50e017555b3d538ace645035006fd0809df9fd2815534932841db885b8c9b"
SRC_URI[libwayland-egl.sha256sum] = "d77a6ae553fe6b9364202bb0cc1618246998c4b5b54358126ead3f70a52a8272"
SRC_URI[libwayland-server.sha256sum] = "37cb0c62f8d87921dce4f391947c987c1dc711d7e33d181a442fb65405896516"
SRC_URI[wayland-devel.sha256sum] = "33341726184652b3b1594a19fe52127ee9eff2c4e96a73ac7464bdb8496f5d2e"
