SUMMARY = "generated recipe based on nmap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libpcap libpcre openssl pkgconfig-native"
RPM_SONAME_REQ_nmap = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpcap.so.1 libpcre.so.1 libssl.so.1.1 libstdc++.so.6"
RDEPENDS_nmap = "glibc libgcc libpcap libstdc++ nmap-ncat openssl-libs pcre"
RPM_SONAME_REQ_nmap-ncat = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpcap.so.1 libssl.so.1.1"
RDEPENDS_nmap-ncat = "bash glibc libpcap openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nmap-7.70-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nmap-ncat-7.70-5.el8.aarch64.rpm \
          "

SRC_URI[nmap.sha256sum] = "14d499620fbc918f84da657963aa64cd9fbe9765b8a1a43af34ba28bf1720429"
SRC_URI[nmap-ncat.sha256sum] = "8a71ce754f45c62be372e88350ef338d30bf3aa97c766a3d241f58ec12520c2d"
