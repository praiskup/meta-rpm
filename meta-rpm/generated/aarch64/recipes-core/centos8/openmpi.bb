SUMMARY = "generated recipe based on openmpi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc hwloc libevent libfabric libgcc opensm pkgconfig-native pmix rdma-core torque ucx zlib"
RPM_SONAME_PROV_openmpi = "libmpi.so.40 libmpi_cxx.so.40 libmpi_java.so.40 libmpi_mpifh.so.40 libmpi_usempi_ignore_tkr.so.40 libmpi_usempif08.so.40 liboshmem.so.40"
RPM_SONAME_REQ_openmpi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libfabric.so.1 libgcc_s.so.1 libgfortran.so.5 libhwloc.so.5 libibverbs.so.1 libm.so.6 libmpi.so.40 libmpi_mpifh.so.40 liboshmem.so.40 libosmcomp.so.5 libpmix.so.2 libpthread.so.0 librdmacm.so.1 librt.so.1 libstdc++.so.6 libtorque.so.2 libucm.so.0 libucp.so.0 libucs.so.0 libuct.so.0 libutil.so.1 libz.so.1"
RDEPENDS_openmpi = "environment-modules glibc hwloc-libs libevent libfabric libgcc libgfortran libibverbs librdmacm libstdc++ opensm-libs openssh-clients perl-interpreter perl-libs pmix torque-libs ucx zlib"
RPM_SONAME_REQ_openmpi-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libhwloc.so.5 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libmpi_java.so.40 libmpi_mpifh.so.40 libmpi_usempi_ignore_tkr.so.40 libmpi_usempif08.so.40 liboshmem.so.40 libpthread.so.0 librt.so.1 libutil.so.1 libz.so.1"
RPROVIDES_openmpi-devel = "openmpi-dev (= 4.0.2)"
RDEPENDS_openmpi-devel = "gcc-gfortran glibc hwloc-libs libevent openmpi pkgconf-pkg-config rpm-mpi-hooks zlib"
RDEPENDS_python3-openmpi = "openmpi platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openmpi-4.0.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openmpi-devel-4.0.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-openmpi-4.0.2-2.el8.aarch64.rpm \
          "

SRC_URI[openmpi.sha256sum] = "324f062649facbf10d8632a4dfd74279122a9969f9e5b05c7b099be85b5fdc8d"
SRC_URI[openmpi-devel.sha256sum] = "18a8fd947b8a8f134d073c51e9041b92a8cb1a076dd043d709a6f9a37d8c94be"
SRC_URI[python3-openmpi.sha256sum] = "8479d02ecca2220fed14f311133ed5d4756a3b7e41c2cb5d0184c95fbcdea652"
