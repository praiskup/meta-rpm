SUMMARY = "generated recipe based on perl-IO-Socket-IP srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-Socket-IP = "perl-Carp perl-Errno perl-IO perl-Socket perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-IO-Socket-IP-0.39-5.el8.noarch.rpm \
          "

SRC_URI[perl-IO-Socket-IP.sha256sum] = "c7dfa7d5a4917eed3b44af87ffb9c6e18242ecd847a8edd140bcdd178cc8946a"
