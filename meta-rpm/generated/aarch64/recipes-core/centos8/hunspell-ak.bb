SUMMARY = "generated recipe based on hunspell-ak srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ak = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ak-0.9.1-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-ak.sha256sum] = "6c30db9290078fbff864d776c73c876807562133ac5e490bca31f16336da13b4"
