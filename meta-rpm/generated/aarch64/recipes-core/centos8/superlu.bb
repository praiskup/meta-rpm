SUMMARY = "generated recipe based on SuperLU srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atlas pkgconfig-native"
RPM_SONAME_PROV_SuperLU = "libsuperlu.so.5.1"
RPM_SONAME_REQ_SuperLU = "ld-linux-aarch64.so.1 libc.so.6 libsatlas.so.3"
RDEPENDS_SuperLU = "atlas glibc"
RPM_SONAME_REQ_SuperLU-devel = "libsuperlu.so.5.1"
RPROVIDES_SuperLU-devel = "SuperLU-dev (= 5.2.0)"
RDEPENDS_SuperLU-devel = "SuperLU"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/SuperLU-5.2.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/SuperLU-devel-5.2.0-7.el8.aarch64.rpm \
          "

SRC_URI[SuperLU.sha256sum] = "798daad818f83624cf8c815f32a3bb1a707c3659700a4aad4df126d9d0389536"
SRC_URI[SuperLU-devel.sha256sum] = "79269950d9cedf7863074d17abd41240c8962759c41ed505c71b1281fb9a7c71"
