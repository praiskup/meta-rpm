SUMMARY = "generated recipe based on transfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng libxpm pkgconfig-native"
RPM_SONAME_REQ_transfig = "ld-linux-aarch64.so.1 libXpm.so.4 libc.so.6 libm.so.6 libpng16.so.16"
RDEPENDS_transfig = "bash bc ghostscript glibc libXpm libpng netpbm-progs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/transfig-3.2.6a-4.el8.aarch64.rpm \
          "

SRC_URI[transfig.sha256sum] = "c60ce742a809f6a3e0bdd4ea8c77f44e770e66a022fbf59e8d138ec28b429b8a"
