SUMMARY = "generated recipe based on nss srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "nspr pkgconfig-native sqlite3 zlib"
RPM_SONAME_PROV_nss = "libnss3.so libsmime3.so libssl3.so"
RPM_SONAME_REQ_nss = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0"
RDEPENDS_nss = "bash crypto-policies glibc nspr nss-softokn nss-sysinit nss-util p11-kit-trust"
RPROVIDES_nss-devel = "nss-dev (= 3.53.1)"
RDEPENDS_nss-devel = "bash nspr-devel nss nss-softokn-devel nss-util-devel pkgconf-pkg-config"
RPM_SONAME_PROV_nss-softokn = "libnssdbm3.so libsoftokn3.so"
RPM_SONAME_REQ_nss-softokn = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnspr4.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsqlite3.so.0"
RDEPENDS_nss-softokn = "glibc nspr nss-softokn-freebl nss-util sqlite-libs"
RPROVIDES_nss-softokn-devel = "nss-softokn-dev (= 3.53.1)"
RDEPENDS_nss-softokn-devel = "bash nspr-devel nss-softokn nss-softokn-freebl-devel nss-util-devel pkgconf-pkg-config"
RPM_SONAME_PROV_nss-softokn-freebl = "libfreebl3.so libfreeblpriv3.so"
RPM_SONAME_REQ_nss-softokn-freebl = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_nss-softokn-freebl = "bash glibc nspr nss-util"
RPROVIDES_nss-softokn-freebl-devel = "nss-softokn-freebl-dev (= 3.53.1)"
RDEPENDS_nss-softokn-freebl-devel = "nss-softokn-freebl"
RPM_SONAME_PROV_nss-sysinit = "libnsssysinit.so"
RPM_SONAME_REQ_nss-sysinit = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnspr4.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0"
RDEPENDS_nss-sysinit = "bash coreutils glibc nspr nss nss-util sed"
RPM_SONAME_REQ_nss-tools = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libz.so.1"
RDEPENDS_nss-tools = "glibc nspr nss nss-util zlib"
RPM_SONAME_PROV_nss-util = "libnssutil3.so"
RPM_SONAME_REQ_nss-util = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnspr4.so libplc4.so libplds4.so libpthread.so.0"
RDEPENDS_nss-util = "glibc nspr"
RPROVIDES_nss-util-devel = "nss-util-dev (= 3.53.1)"
RDEPENDS_nss-util-devel = "bash nspr-devel nss-util pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-devel-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-softokn-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-softokn-devel-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-softokn-freebl-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-softokn-freebl-devel-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-sysinit-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-tools-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-util-3.53.1-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-util-devel-3.53.1-11.el8_2.aarch64.rpm \
          "

SRC_URI[nss.sha256sum] = "875824099c4c366625948471cdb01ec8ac8840efad0a83e2ca6c89503cacfb0a"
SRC_URI[nss-devel.sha256sum] = "881ad0d1f34a5d03a5189778f1a6b3e2e1f6a5cf6d3861df29031ffb2d8607e5"
SRC_URI[nss-softokn.sha256sum] = "e8e21adceac78f59119c23fd61bbc5862a561bd4546374e33af6acaeb2054170"
SRC_URI[nss-softokn-devel.sha256sum] = "9cf94f10ea1184bc692187367254e1552df666412afef6ab9229d735bc41935f"
SRC_URI[nss-softokn-freebl.sha256sum] = "2fceb671fbc4fe9178ff7447e41db3f42b9250d129851ef0f00e472b531318da"
SRC_URI[nss-softokn-freebl-devel.sha256sum] = "000b4f3f89520f7157a33f53232a7240cd90901a38fa0b853abd8a6012de37a4"
SRC_URI[nss-sysinit.sha256sum] = "e4c4c0c1e5ead2cecfd7ec9223d674fc4062bf7e06712fc23276ba5030d8496d"
SRC_URI[nss-tools.sha256sum] = "465b63505c55919c1e9116bd097ca62a6fab430c920cf9ff2b2cd71847b472cb"
SRC_URI[nss-util.sha256sum] = "47ae4dcdc671fee25037bd8bfffb9ff72a8115a7847a7d5b67ab8ca33d23cb6d"
SRC_URI[nss-util-devel.sha256sum] = "5bcbdc48a06e4ce53577ab3f378a07987391b105340470b80438a85328219f72"
