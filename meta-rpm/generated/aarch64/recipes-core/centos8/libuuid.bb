SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuuid = "libuuid.so.1"
RPM_SONAME_REQ_libuuid = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libuuid = "glibc"
RPM_SONAME_REQ_libuuid-devel = "libuuid.so.1"
RPROVIDES_libuuid-devel = "libuuid-dev (= 2.32.1)"
RDEPENDS_libuuid-devel = "libuuid pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libuuid-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libuuid-devel-2.32.1-22.el8.aarch64.rpm \
          "

SRC_URI[libuuid.sha256sum] = "ab4fa82c638ca0a31ffcb693e88d6db05f2b8efc38d3275de7a4bfa46c349097"
SRC_URI[libuuid-devel.sha256sum] = "06a37c45995bc80902e34b67719b40d6b18395c517601c500c19b1c3ac73eb4d"
