SUMMARY = "generated recipe based on setools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_setools = "python3-setools"
RDEPENDS_setools-console = "libselinux platform-python python3-setools"
RDEPENDS_setools-console-analyses = "libselinux platform-python python3-networkx python3-setools"
RDEPENDS_setools-gui = "platform-python python3-networkx python3-qt5 python3-setools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/setools-4.2.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/setools-console-analyses-4.2.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/setools-gui-4.2.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/setools-console-4.2.2-2.el8.aarch64.rpm \
          "

SRC_URI[setools.sha256sum] = "1a5f023e5daf3afffe113fa6b01ef943cda987ba01c9cec9615759b7955b13a4"
SRC_URI[setools-console.sha256sum] = "bbf1d6fb8e237c7d760f196370aa87111716b023c915197f1ce36f2342889982"
SRC_URI[setools-console-analyses.sha256sum] = "91b15785b6fcc9f5f02e351f4dd6df0b10ec3b427c8eaa7060c2aa1f40e58a5c"
SRC_URI[setools-gui.sha256sum] = "7ca815f1ccb8cdbc8ee2c11bf2a8cc583908df1de478f34ea75743013e30365c"
