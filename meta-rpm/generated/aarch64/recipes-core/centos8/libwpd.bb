SUMMARY = "generated recipe based on libwpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libwpd = "libwpd-0.10.so.10"
RPM_SONAME_REQ_libwpd = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libwpd = "glibc libgcc librevenge libstdc++"
RPM_SONAME_REQ_libwpd-devel = "libwpd-0.10.so.10"
RPROVIDES_libwpd-devel = "libwpd-dev (= 0.10.2)"
RDEPENDS_libwpd-devel = "librevenge-devel libwpd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwpd-0.10.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwpd-devel-0.10.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwpd-doc-0.10.2-2.el8.noarch.rpm \
          "

SRC_URI[libwpd.sha256sum] = "35fa77ed41472d5da9db8e5ccc5abb67ea20d05592d6d970f3037a83cf90a8be"
SRC_URI[libwpd-devel.sha256sum] = "66ac09630920840031308fb3c3b8380889efc417553cdcb5157480d7387500f9"
SRC_URI[libwpd-doc.sha256sum] = "a14c7cad6c6c0b2ae75c644674e538b42b1ec833af6586452af3fe54146e04f7"
