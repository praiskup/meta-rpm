SUMMARY = "generated recipe based on flex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_flex = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_flex = "bash glibc info m4"
RPROVIDES_flex-devel = "flex-dev (= 2.6.1)"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/flex-2.6.1-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/flex-doc-2.6.1-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/flex-devel-2.6.1-9.el8.aarch64.rpm \
          "

SRC_URI[flex.sha256sum] = "5738e61f12e104dcd444133d9395e9b4277650ef22827d4b0515a075a31ef8b0"
SRC_URI[flex-devel.sha256sum] = "eccf69260ae9b6ebc9f4acc0b71f38fe7c8bcf4971caf8979bcab58a4219108e"
SRC_URI[flex-doc.sha256sum] = "7be21cdf9ac23fe0b675b5c8c35ed8f3837a7887db890d57e4b196367a293480"
