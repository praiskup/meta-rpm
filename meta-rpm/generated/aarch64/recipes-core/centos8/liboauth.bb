SUMMARY = "generated recipe based on liboauth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl nspr nss pkgconfig-native"
RPM_SONAME_PROV_liboauth = "liboauth.so.0"
RPM_SONAME_REQ_liboauth = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so"
RDEPENDS_liboauth = "glibc libcurl nspr nss nss-util"
RPM_SONAME_REQ_liboauth-devel = "liboauth.so.0"
RPROVIDES_liboauth-devel = "liboauth-dev (= 1.0.3)"
RDEPENDS_liboauth-devel = "libcurl-devel liboauth nss-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liboauth-1.0.3-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liboauth-devel-1.0.3-9.el8.aarch64.rpm \
          "

SRC_URI[liboauth.sha256sum] = "53151e71a163e9a237dae48e59adf0c5a5bbcbfb1021b7b6e5528e066dc13302"
SRC_URI[liboauth-devel.sha256sum] = "d50b4bff01a4acffc3c2eb54a69449a6324721868712d1f9a273a4bc02b49bd7"
