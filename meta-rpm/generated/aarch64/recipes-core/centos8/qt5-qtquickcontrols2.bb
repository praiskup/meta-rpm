SUMMARY = "generated recipe based on qt5-qtquickcontrols2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtquickcontrols2 = "libQt5QuickControls2.so.5 libQt5QuickTemplates2.so.5"
RPM_SONAME_REQ_qt5-qtquickcontrols2 = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickControls2.so.5 libQt5QuickTemplates2.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtquickcontrols2 = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtgraphicaleffects"
RPM_SONAME_REQ_qt5-qtquickcontrols2-devel = "libQt5QuickControls2.so.5 libQt5QuickTemplates2.so.5"
RPROVIDES_qt5-qtquickcontrols2-devel = "qt5-qtquickcontrols2-dev (= 5.12.5)"
RDEPENDS_qt5-qtquickcontrols2-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtquickcontrols2"
RPM_SONAME_REQ_qt5-qtquickcontrols2-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickControls2.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtquickcontrols2-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtquickcontrols2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtquickcontrols2-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtquickcontrols2-examples-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qt5-qtquickcontrols2-devel-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtquickcontrols2.sha256sum] = "3ff75cc7373248eb02b1a43bc635574824841e2b4e83eff56c16dfa3982187cf"
SRC_URI[qt5-qtquickcontrols2-devel.sha256sum] = "4e41a8428b826078b405bcc2f1565a4938ed996991c355f39fe7820c7b5fd571"
SRC_URI[qt5-qtquickcontrols2-examples.sha256sum] = "1d5a14d955e711b069aa22c5f9b2dc000ae12bd8e95f7e9784f33104fc197ec1"
