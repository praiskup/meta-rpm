SUMMARY = "generated recipe based on cdparanoia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cdparanoia = "ld-linux-aarch64.so.1 libc.so.6 libcdda_interface.so.0 libcdda_paranoia.so.0 libm.so.6 librt.so.1"
RDEPENDS_cdparanoia = "cdparanoia-libs glibc"
RPM_SONAME_REQ_cdparanoia-devel = "libcdda_interface.so.0 libcdda_paranoia.so.0"
RPROVIDES_cdparanoia-devel = "cdparanoia-dev (= 10.2)"
RDEPENDS_cdparanoia-devel = "cdparanoia-libs"
RPM_SONAME_PROV_cdparanoia-libs = "libcdda_interface.so.0 libcdda_paranoia.so.0"
RPM_SONAME_REQ_cdparanoia-libs = "ld-linux-aarch64.so.1 libc.so.6 libcdda_interface.so.0 libm.so.6 librt.so.1"
RDEPENDS_cdparanoia-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cdparanoia-10.2-27.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cdparanoia-libs-10.2-27.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cdparanoia-devel-10.2-27.el8.aarch64.rpm \
          "

SRC_URI[cdparanoia.sha256sum] = "28499bf008fb54750b0cb6c2ae1c4ec2c2019ae2ead59d6d4e60a3e33424464a"
SRC_URI[cdparanoia-devel.sha256sum] = "5cfa2268a72ada7b67586006ccba5365fd1f42615143c5f64b68c22320865e0c"
SRC_URI[cdparanoia-libs.sha256sum] = "0700829e657137a79b700ed9fea848ee0ea158ab2d1521b21ae244fd01569ef6"
