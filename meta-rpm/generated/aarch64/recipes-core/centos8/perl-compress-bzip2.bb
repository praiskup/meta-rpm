SUMMARY = "generated recipe based on perl-Compress-Bzip2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 perl pkgconfig-native"
RPM_SONAME_REQ_perl-Compress-Bzip2 = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Compress-Bzip2 = "bzip2-libs glibc perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Compress-Bzip2-2.26-6.el8.aarch64.rpm \
          "

SRC_URI[perl-Compress-Bzip2.sha256sum] = "c81309da689f9cbb33c6802231d646ea6c55e5b8cb14f4f4ffd3463ae63cd04e"
