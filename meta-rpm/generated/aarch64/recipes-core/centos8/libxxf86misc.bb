SUMMARY = "generated recipe based on libXxf86misc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXxf86misc = "libXxf86misc.so.1"
RPM_SONAME_REQ_libXxf86misc = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXxf86misc = "glibc libX11 libXext"
RPM_SONAME_REQ_libXxf86misc-devel = "libXxf86misc.so.1"
RPROVIDES_libXxf86misc-devel = "libXxf86misc-dev (= 1.0.4)"
RDEPENDS_libXxf86misc-devel = "libX11-devel libXext-devel libXxf86misc pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXxf86misc-1.0.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXxf86misc-devel-1.0.4-1.el8.aarch64.rpm \
          "

SRC_URI[libXxf86misc.sha256sum] = "61cb127f52d08d3503d740a90a60320a167ae9cf083bf260b9574ba6cc2c6c96"
SRC_URI[libXxf86misc-devel.sha256sum] = "8a2ba8383d0d1682b0fb646bd774b1cb26cea39f175526a92bf42dcae6ec07b5"
