SUMMARY = "generated recipe based on mt-st srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mt-st = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mt-st = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mt-st-1.1-24.el8.aarch64.rpm \
          "

SRC_URI[mt-st.sha256sum] = "1a8d8ea22ea15837ab8525157ee0b577c6d00864390a7b749c5e2385bdf5acc4"
