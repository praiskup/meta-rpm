SUMMARY = "generated recipe based on flatpak-builder srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl elfutils glib-2.0 json-glib libgcc libsoup-2.4 libxml2 libyaml ostree pkgconfig-native"
RPM_SONAME_REQ_flatpak-builder = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libelf.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libostree-1.so.1 libpthread.so.0 libsoup-2.4.so.1 libxml2.so.2 libyaml-0.so.2"
RDEPENDS_flatpak-builder = "binutils bzip2 elfutils elfutils-libelf flatpak git-core glib2 glibc json-glib libcurl libgcc libsoup libxml2 libyaml ostree ostree-libs patch tar unzip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/flatpak-builder-1.0.9-2.el8.aarch64.rpm \
          "

SRC_URI[flatpak-builder.sha256sum] = "85b20623f062a18fd94fc83b727a0c21561a9018d9f93f658719860f95a3b944"
