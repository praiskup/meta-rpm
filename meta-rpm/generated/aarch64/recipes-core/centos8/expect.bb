SUMMARY = "generated recipe based on expect srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native tcl"
RPM_SONAME_PROV_expect = "libexpect5.45.4.so"
RPM_SONAME_REQ_expect = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libexpect5.45.4.so libm.so.6 libtcl8.6.so libutil.so.1"
RDEPENDS_expect = "bash glibc tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/expect-5.45.4-5.el8.aarch64.rpm \
          "

SRC_URI[expect.sha256sum] = "daae7b6a48fa4d498f9323255a5767ed4ea67ce2385ee88690b32a07bbb4c2ba"
