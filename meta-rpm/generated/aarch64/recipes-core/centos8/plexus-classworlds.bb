SUMMARY = "generated recipe based on plexus-classworlds srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-classworlds = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_plexus-classworlds-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-classworlds-2.5.2-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-classworlds-javadoc-2.5.2-9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-classworlds.sha256sum] = "ee8287c83fca482a67e132804864ed91ae0875beb1d39ea8a3a11649ce75b8a1"
SRC_URI[plexus-classworlds-javadoc.sha256sum] = "0abc61ee9015d0af7ebbfc95fd4618d3ecda6ed19f06538c985f7f5874d116a1"
