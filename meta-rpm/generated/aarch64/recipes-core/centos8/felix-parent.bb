SUMMARY = "generated recipe based on felix-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_felix-parent = "apache-parent easymock java-1.8.0-openjdk-headless javapackages-filesystem maven-antrun-plugin maven-compiler-plugin mockito"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-parent-4-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[felix-parent.sha256sum] = "360e3886384ceb9a305f1cdc9d0b9a73a9d8edf8005aaddada4882bf482ace64"
