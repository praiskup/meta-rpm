SUMMARY = "generated recipe based on fuse-sshfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fuse glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_fuse-sshfs = "ld-linux-aarch64.so.1 libc.so.6 libfuse.so.2 libglib-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0"
RDEPENDS_fuse-sshfs = "fuse fuse-libs glib2 glibc openssh-clients"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/fuse-sshfs-2.8-5.el8.aarch64.rpm \
          "

SRC_URI[fuse-sshfs.sha256sum] = "066d5e10f1ca340dc619ea500a73b1f016fb4b8f9efcc8364e614118c1a16b7c"
