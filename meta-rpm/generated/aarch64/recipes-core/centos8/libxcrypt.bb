SUMMARY = "generated recipe based on libxcrypt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/crypt"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libxcrypt = "libcrypt.so.1"
RPM_SONAME_REQ_libxcrypt = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libxcrypt = "glibc"
RPM_SONAME_REQ_libxcrypt-devel = "libcrypt.so.1"
RPROVIDES_libxcrypt-devel = "libxcrypt-dev (= 4.1.1)"
RDEPENDS_libxcrypt-devel = "glibc-devel glibc-headers libxcrypt pkgconf-pkg-config"
RDEPENDS_libxcrypt-static = "glibc-static libxcrypt-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libxcrypt-4.1.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libxcrypt-devel-4.1.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libxcrypt-static-4.1.1-4.el8.aarch64.rpm \
          "

SRC_URI[libxcrypt.sha256sum] = "2221fb6f38fb36025c3283afe04e009bb1cc688144e71c5a9edc6717f22012c0"
SRC_URI[libxcrypt-devel.sha256sum] = "ec6b25000a12129b67ed2b26419a00e439e4679b4d26e0d77ea67dbc2671f376"
SRC_URI[libxcrypt-static.sha256sum] = "9d9399689b9ad6e174f17b597a8ad47a821562a4fbe7b4393afe8eed692a21b4"
