SUMMARY = "generated recipe based on hyphen-fr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-fr = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-fr-3.0-1.el8.noarch.rpm \
          "

SRC_URI[hyphen-fr.sha256sum] = "0d8a0cda187b4354aa697a6a47f042a735ed7ce4da1e912e071b1684e6a9c513"
