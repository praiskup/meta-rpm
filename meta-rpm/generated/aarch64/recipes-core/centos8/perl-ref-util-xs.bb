SUMMARY = "generated recipe based on perl-Ref-Util-XS srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Ref-Util-XS = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Ref-Util-XS = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Ref-Util-XS-0.117-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Ref-Util-XS.sha256sum] = "bdb7fa0279811a321d3d4859d03963be6ed36eb61d28420d1b4a0fe6565e5337"
