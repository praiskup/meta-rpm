SUMMARY = "generated recipe based on babeltrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils glib-2.0 libuuid pkgconfig-native popt"
RPM_SONAME_PROV_libbabeltrace = "libbabeltrace-ctf-metadata.so.1 libbabeltrace-ctf-text.so.1 libbabeltrace-ctf.so.1 libbabeltrace-dummy.so.1 libbabeltrace-lttng-live.so.1 libbabeltrace.so.1"
RPM_SONAME_REQ_libbabeltrace = "ld-linux-aarch64.so.1 libbabeltrace-ctf.so.1 libbabeltrace.so.1 libc.so.6 libdw.so.1 libelf.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libpopt.so.0 libpthread.so.0 libuuid.so.1"
RDEPENDS_libbabeltrace = "elfutils-libelf elfutils-libs glib2 glibc libuuid popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libbabeltrace-1.5.4-2.el8.aarch64.rpm \
          "

SRC_URI[libbabeltrace.sha256sum] = "20033ea98b340cb9191b2213bd0a48c57960a5d1f7c9165495de43c40e1db75b"
