SUMMARY = "generated recipe based on ibus-kkc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 ibus json-glib libgee libkkc pango pkgconfig-native"
RPM_SONAME_REQ_ibus-kkc = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgee-0.8.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libibus-1.0.so.5 libjson-glib-1.0.so.0 libkkc.so.2 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus-kkc = "atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 ibus ibus-libs json-glib libgee libkkc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-kkc-1.5.22-9.el8.aarch64.rpm \
          "

SRC_URI[ibus-kkc.sha256sum] = "803120476ead82f7565b711de2f61466b29d27f37d18c321afc33bfa132746c1"
