SUMMARY = "generated recipe based on fipscheck srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_fipscheck = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1"
RDEPENDS_fipscheck = "fipscheck-lib glibc openssl-libs"
RPM_SONAME_REQ_fipscheck-devel = "libfipscheck.so.1"
RPROVIDES_fipscheck-devel = "fipscheck-dev (= 1.5.0)"
RDEPENDS_fipscheck-devel = "fipscheck-lib"
RPM_SONAME_PROV_fipscheck-lib = "libfipscheck.so.1"
RPM_SONAME_REQ_fipscheck-lib = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_fipscheck-lib = "fipscheck glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fipscheck-1.5.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fipscheck-lib-1.5.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/fipscheck-devel-1.5.0-4.el8.aarch64.rpm \
          "

SRC_URI[fipscheck.sha256sum] = "6a5f0fd839aad1ef9a435346b7c7f7f6c892af6b3856732b32bbe2aaf75a1d39"
SRC_URI[fipscheck-devel.sha256sum] = "7b8903fa4eaaada6d613c4160d0d03f164ad11b4b3b5dd7267a937d9f897b817"
SRC_URI[fipscheck-lib.sha256sum] = "eaaa11596a1f530e106bdb240bfc04a60964deabe5f7a847cbddd60622f708ba"
