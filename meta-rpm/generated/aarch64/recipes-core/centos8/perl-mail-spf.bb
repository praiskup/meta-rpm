SUMMARY = "generated recipe based on perl-Mail-SPF srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Mail-SPF = "bash chkconfig perl-Error perl-Getopt-Long perl-IO perl-Net-DNS perl-NetAddr-IP perl-URI perl-constant perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Mail-SPF-2.9.0-15.el8.noarch.rpm \
          "

SRC_URI[perl-Mail-SPF.sha256sum] = "9a6d15afd9c563ebd16eef5433adc75801e3a6b3f9bb3448098aa4d87f3c0f8f"
