SUMMARY = "generated recipe based on hunspell-wa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-wa = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-wa-0.4.17-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-wa.sha256sum] = "7c4b66a093781943f8843a97e8b112d31e96fb452bb6bddb9aa8fb6ffb2eb577"
