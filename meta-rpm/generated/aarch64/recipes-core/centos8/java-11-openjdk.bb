SUMMARY = "generated recipe based on java-11-openjdk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib freetype giflib lcms2 libgcc libjpeg-turbo libpng libx11 libxext libxi libxrender libxtst pkgconfig-native zlib"
RPM_SONAME_REQ_java-11-openjdk = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXi.so.6 libXrender.so.1 libXtst.so.6 libc.so.6 libdl.so.2 libgif.so.7 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_java-11-openjdk = "bash fontconfig giflib glibc java-11-openjdk-headless libX11 libXcomposite libXext libXi libXrender libXtst libjpeg-turbo libpng xorg-x11-fonts-Type1 zlib"
RDEPENDS_java-11-openjdk-demo = "java-11-openjdk"
RPM_SONAME_REQ_java-11-openjdk-devel = "libc.so.6 libdl.so.2 libpthread.so.0 libz.so.1"
RPROVIDES_java-11-openjdk-devel = "java-11-openjdk-dev (= 11.0.9.11)"
RDEPENDS_java-11-openjdk-devel = "bash chkconfig glibc java-11-openjdk zlib"
RPM_SONAME_REQ_java-11-openjdk-headless = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libdl.so.2 libfreetype.so.6 libgcc_s.so.1 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpthread.so.0 libstdc++.so.6 libthread_db.so.1 libz.so.1"
RDEPENDS_java-11-openjdk-headless = "alsa-lib bash ca-certificates chkconfig copy-jdk-configs cups-libs freetype glibc javapackages-filesystem lcms2 libgcc libjpeg-turbo libstdc++ lksctp-tools tzdata-java zlib"
RDEPENDS_java-11-openjdk-javadoc = "bash chkconfig javapackages-filesystem"
RDEPENDS_java-11-openjdk-javadoc-zip = "bash chkconfig javapackages-filesystem"
RDEPENDS_java-11-openjdk-jmods = "java-11-openjdk-devel"
RDEPENDS_java-11-openjdk-src = "java-11-openjdk-headless"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-demo-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-devel-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-headless-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-javadoc-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-javadoc-zip-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-jmods-11.0.9.11-0.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-11-openjdk-src-11.0.9.11-0.el8_2.aarch64.rpm \
          "

SRC_URI[java-11-openjdk.sha256sum] = "bd5b40fa1d9b387bf74fca81268960340a0d375961f62c9f9603001cfb301e76"
SRC_URI[java-11-openjdk-demo.sha256sum] = "404a6cdba8e8e487fa382523b729bfb0403bc69709c19c57882cfee9e8ed01c9"
SRC_URI[java-11-openjdk-devel.sha256sum] = "bbbc44eaeef3eeb39f8ca9e003d2e15356b4fb5aac414f8cf69cc7ffa2c06b05"
SRC_URI[java-11-openjdk-headless.sha256sum] = "9fc838f335c43afc3e59d0abeacf4e92f61d4575a604f11412b37cf4797b4c7d"
SRC_URI[java-11-openjdk-javadoc.sha256sum] = "a5e1d1f711445dc4e94c01912f48814cf023fde2242d72a09847c837b10661da"
SRC_URI[java-11-openjdk-javadoc-zip.sha256sum] = "962d36d1ddb4d973bfbccefb062483b952dcaf266645937e970deb6f968d4c79"
SRC_URI[java-11-openjdk-jmods.sha256sum] = "57bdf58aafa18bd84971721ffd3c0a71ca87dc791b6948b1a8c185d5cdbc9aac"
SRC_URI[java-11-openjdk-src.sha256sum] = "84b597ffa96da8c15a4e595a9f914b75a7220553ab6fdbbbb78dd247161a7219"
