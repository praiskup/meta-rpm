SUMMARY = "generated recipe based on poppler srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo fontconfig freetype glib-2.0 lcms2 libgcc libjpeg-turbo libpng nspr nss openjpeg2 pkgconfig-native qt5-qtbase tiff"
RPM_SONAME_PROV_poppler = "libpoppler.so.78"
RPM_SONAME_REQ_poppler = "ld-linux-aarch64.so.1 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libjpeg.so.62 liblcms2.so.2 libm.so.6 libnspr4.so libnss3.so libopenjp2.so.7 libpng16.so.16 libpthread.so.0 libsmime3.so libstdc++.so.6 libtiff.so.5"
RDEPENDS_poppler = "fontconfig freetype glibc lcms2 libgcc libjpeg-turbo libpng libstdc++ libtiff nspr nss openjpeg2 poppler-data"
RPM_SONAME_PROV_poppler-cpp = "libpoppler-cpp.so.0"
RPM_SONAME_REQ_poppler-cpp = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libpoppler.so.78 libstdc++.so.6"
RDEPENDS_poppler-cpp = "glibc libgcc libstdc++ poppler"
RPM_SONAME_REQ_poppler-cpp-devel = "libpoppler-cpp.so.0"
RPROVIDES_poppler-cpp-devel = "poppler-cpp-dev (= 0.66.0)"
RDEPENDS_poppler-cpp-devel = "pkgconf-pkg-config poppler-cpp poppler-devel"
RPM_SONAME_REQ_poppler-devel = "libpoppler.so.78"
RPROVIDES_poppler-devel = "poppler-dev (= 0.66.0)"
RDEPENDS_poppler-devel = "pkgconf-pkg-config poppler"
RPM_SONAME_PROV_poppler-glib = "libpoppler-glib.so.8"
RPM_SONAME_REQ_poppler-glib = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpoppler.so.78 libpthread.so.0 libstdc++.so.6"
RDEPENDS_poppler-glib = "cairo freetype glib2 glibc libgcc libstdc++ poppler"
RPM_SONAME_REQ_poppler-glib-devel = "libpoppler-glib.so.8"
RPROVIDES_poppler-glib-devel = "poppler-glib-dev (= 0.66.0)"
RDEPENDS_poppler-glib-devel = "cairo-devel glib2-devel pkgconf-pkg-config poppler-devel poppler-glib"
RPM_SONAME_PROV_poppler-qt5 = "libpoppler-qt5.so.1"
RPM_SONAME_REQ_poppler-qt5 = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libpoppler.so.78 libstdc++.so.6"
RDEPENDS_poppler-qt5 = "glibc libgcc libstdc++ poppler qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_poppler-qt5-devel = "libpoppler-qt5.so.1"
RPROVIDES_poppler-qt5-devel = "poppler-qt5-dev (= 0.66.0)"
RDEPENDS_poppler-qt5-devel = "pkgconf-pkg-config poppler-devel poppler-qt5 qt5-qtbase-devel"
RPM_SONAME_REQ_poppler-utils = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 liblcms2.so.2 libm.so.6 libpoppler.so.78 libpthread.so.0 libstdc++.so.6"
RDEPENDS_poppler-utils = "cairo freetype glibc lcms2 libgcc libstdc++ poppler"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/poppler-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/poppler-glib-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/poppler-utils-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/poppler-cpp-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/poppler-cpp-devel-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/poppler-devel-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/poppler-glib-devel-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/poppler-qt5-0.66.0-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/poppler-qt5-devel-0.66.0-26.el8.aarch64.rpm \
          "

SRC_URI[poppler.sha256sum] = "433b9be64ddde5c4fb8d696a91e010e2387adc0487397ff8b66e967de86b2879"
SRC_URI[poppler-cpp.sha256sum] = "349f9d91db70c45cd8cd7e97e20dfa894ea73c5cbd96b8a770dfcf8972c209f5"
SRC_URI[poppler-cpp-devel.sha256sum] = "9d338b8dd2ee6d33152fce574b15c1e99f943fafb26e5a7ac293475b93b0b6bf"
SRC_URI[poppler-devel.sha256sum] = "c4e9b8b4ebfb214bbf3e27fe8d8ffccbe674bdd9f4cd41bfaba27392798cc435"
SRC_URI[poppler-glib.sha256sum] = "488444f5064880532a29a6318adc535735f0ee3252fb4c3225714b2d8ea3c0b2"
SRC_URI[poppler-glib-devel.sha256sum] = "a7c8b1029086deacb97cd997ba8387b4b4eb0aea0d60b2ecab2bbaca09dd2119"
SRC_URI[poppler-qt5.sha256sum] = "111037cce9c1adbf25d8d0a9d99b79628536bfb9bed8436a358dc15a18560fc4"
SRC_URI[poppler-qt5-devel.sha256sum] = "8f16f2b2d8507efc33f7265a50a5435681167b09f53de8285b5f6632acf1df9b"
SRC_URI[poppler-utils.sha256sum] = "99f7b39bca8f2ad239f974f9216c2210d99c5fe82da22e6874b8569bc2c6dff1"
