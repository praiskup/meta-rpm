SUMMARY = "generated recipe based on python-markdown srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-markdown = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-markdown-2.6.11-2.el8.noarch.rpm \
          "

SRC_URI[python3-markdown.sha256sum] = "fbe39a3c7ca604e0e6f938b2872af266871fded84d854e8ff3369588b8901ee5"
