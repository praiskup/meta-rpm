SUMMARY = "generated recipe based on nano srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "file ncurses pkgconfig-native"
RPM_SONAME_REQ_nano = "ld-linux-aarch64.so.1 libc.so.6 libmagic.so.1 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_nano = "bash file-libs glibc info ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nano-2.9.8-1.el8.aarch64.rpm \
          "

SRC_URI[nano.sha256sum] = "5dede39557f41e20d025f8c30f979ab472fd75dbc22c8bcd5f38310c507af268"
