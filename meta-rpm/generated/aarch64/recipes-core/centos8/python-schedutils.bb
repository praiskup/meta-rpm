SUMMARY = "generated recipe based on python-schedutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-schedutils = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-schedutils = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-schedutils-0.6-6.el8.aarch64.rpm \
          "

SRC_URI[python3-schedutils.sha256sum] = "7d11819e0646e34f1489fbe9f6673a6cd701657d8ae1fe0ca370f7421150163f"
