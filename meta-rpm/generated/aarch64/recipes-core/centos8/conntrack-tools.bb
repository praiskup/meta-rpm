SUMMARY = "generated recipe based on conntrack-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl libnetfilter-conntrack libnetfilter-cthelper libnetfilter-cttimeout libnetfilter-queue libnfnetlink pkgconfig-native systemd-libs"
RPM_SONAME_REQ_conntrack-tools = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libmnl.so.0 libnetfilter_conntrack.so.3 libnetfilter_cthelper.so.0 libnetfilter_cttimeout.so.1 libnetfilter_queue.so.1 libnfnetlink.so.0 libsystemd.so.0"
RDEPENDS_conntrack-tools = "bash glibc libmnl libnetfilter_conntrack libnetfilter_cthelper libnetfilter_cttimeout libnetfilter_queue libnfnetlink systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/conntrack-tools-1.4.4-10.el8.aarch64.rpm \
          "

SRC_URI[conntrack-tools.sha256sum] = "c28cd81ea569e59fbaf5611356aae7a860ac4eaa6764c0f7871bfa4dd260797a"
