SUMMARY = "generated recipe based on lohit-tamil-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-tamil-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-tamil-fonts-2.91.3-3.el8.noarch.rpm \
          "

SRC_URI[lohit-tamil-fonts.sha256sum] = "1deabcccba286eab2982f43975b591f766a1de159b0271dfae50d76d712f960d"
