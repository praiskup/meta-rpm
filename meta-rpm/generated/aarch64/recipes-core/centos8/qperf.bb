SUMMARY = "generated recipe based on qperf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native rdma-core"
RPM_SONAME_REQ_qperf = "ld-linux-aarch64.so.1 libc.so.6 libibverbs.so.1 librdmacm.so.1"
RDEPENDS_qperf = "glibc libibverbs librdmacm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qperf-0.4.11-1.el8.aarch64.rpm \
          "

SRC_URI[qperf.sha256sum] = "f4720be420fe3134a8efa8f277453671bef452059d946e83504916dcd735524e"
