SUMMARY = "generated recipe based on execstack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libselinux pkgconfig-native"
RPM_SONAME_REQ_execstack = "ld-linux-aarch64.so.1 libc.so.6 libelf.so.1 libselinux.so.1"
RDEPENDS_execstack = "coreutils elfutils-libelf findutils gawk glibc grep libselinux util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/execstack-0.5.0-15.el8.aarch64.rpm \
          "

SRC_URI[execstack.sha256sum] = "c6f1eeb5ee40445029af94d99fd296f581975685b23e11c7aaf64b083490c97d"
