SUMMARY = "generated recipe based on libfabric srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native rdma-core"
RPM_SONAME_PROV_libfabric = "libfabric.so.1"
RPM_SONAME_REQ_libfabric = "ld-linux-aarch64.so.1 libatomic.so.1 libc.so.6 libdl.so.2 libfabric.so.1 libibverbs.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 librdmacm.so.1 librt.so.1"
RDEPENDS_libfabric = "glibc libatomic libibverbs libnl3 librdmacm"
RPM_SONAME_REQ_libfabric-devel = "libfabric.so.1"
RPROVIDES_libfabric-devel = "libfabric-dev (= 1.9.0rc1)"
RDEPENDS_libfabric-devel = "libfabric pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libfabric-1.9.0rc1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libfabric-devel-1.9.0rc1-1.el8.aarch64.rpm \
          "

SRC_URI[libfabric.sha256sum] = "5f2c731c1a35f2a75c1cd6bb26c25af40418a028615af60d74026f8746c97e40"
SRC_URI[libfabric-devel.sha256sum] = "0c2430cfb8e420a69276e22eb3ba6b64bd1eea044575c97e67a83a65a83e19d3"
