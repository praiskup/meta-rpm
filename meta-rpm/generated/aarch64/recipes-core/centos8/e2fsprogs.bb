SUMMARY = "generated recipe based on e2fsprogs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fuse libblkid libuuid pkgconfig-native"
RPM_SONAME_REQ_e2fsprogs = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcom_err.so.2 libdl.so.2 libe2p.so.2 libext2fs.so.2 libfuse.so.2 libss.so.2 libuuid.so.1"
RDEPENDS_e2fsprogs = "e2fsprogs-libs fuse-libs glibc libblkid libcom_err libss libuuid"
RPM_SONAME_REQ_e2fsprogs-devel = "libe2p.so.2 libext2fs.so.2"
RPROVIDES_e2fsprogs-devel = "e2fsprogs-dev (= 1.45.4)"
RDEPENDS_e2fsprogs-devel = "bash e2fsprogs-libs gawk info libcom_err-devel pkgconf-pkg-config"
RPM_SONAME_PROV_e2fsprogs-libs = "libe2p.so.2 libext2fs.so.2"
RPM_SONAME_REQ_e2fsprogs-libs = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2"
RDEPENDS_e2fsprogs-libs = "glibc libcom_err"
RPM_SONAME_PROV_libcom_err = "libcom_err.so.2"
RPM_SONAME_REQ_libcom_err = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_libcom_err = "glibc"
RPM_SONAME_REQ_libcom_err-devel = "libcom_err.so.2"
RPROVIDES_libcom_err-devel = "libcom_err-dev (= 1.45.4)"
RDEPENDS_libcom_err-devel = "bash libcom_err pkgconf-pkg-config"
RPM_SONAME_PROV_libss = "libss.so.2"
RPM_SONAME_REQ_libss = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libdl.so.2"
RDEPENDS_libss = "glibc libcom_err"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/e2fsprogs-1.45.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/e2fsprogs-devel-1.45.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/e2fsprogs-libs-1.45.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcom_err-1.45.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcom_err-devel-1.45.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libss-1.45.4-3.el8.aarch64.rpm \
          "

SRC_URI[e2fsprogs.sha256sum] = "221f25564a65cb210bc8188937dba5eabe7d73fb5a60620352f1c8620042a383"
SRC_URI[e2fsprogs-devel.sha256sum] = "4a0456958e6a0f04951163c86dfa19c244bf900c480ba662d7531eaaa1f7a1a7"
SRC_URI[e2fsprogs-libs.sha256sum] = "f1d73a278128e8353f82fe032f5901fc25950c21862c353256a8ab2dd6059ab1"
SRC_URI[libcom_err.sha256sum] = "16182b45246cd3137a5842206f9dcf790f8db45603499322c02603089b7c9f20"
SRC_URI[libcom_err-devel.sha256sum] = "78cb93ca55c97e45ff11bd65205600b6ce76dcfaca369758e4db28d5a76ce0c6"
SRC_URI[libss.sha256sum] = "468796a612f016c85544286a93b39cfc8d2f340de625fbcc6c27012ff2494ce3"
