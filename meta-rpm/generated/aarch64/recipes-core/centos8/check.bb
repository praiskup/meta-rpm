SUMMARY = "generated recipe based on check srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_check = "libcheck.so.0"
RPM_SONAME_REQ_check = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_check = "bash glibc info libgcc"
RPM_SONAME_REQ_check-devel = "libcheck.so.0"
RPROVIDES_check-devel = "check-dev (= 0.12.0)"
RDEPENDS_check-devel = "check pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/check-0.12.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/check-devel-0.12.0-2.el8.aarch64.rpm \
          "

SRC_URI[check.sha256sum] = "f3fe2305ef2d98e95a5907a6bc6977bc6f5e9a5887dc5100e06621552e344e3b"
SRC_URI[check-devel.sha256sum] = "047b485ccf8d036641aa7dcf40f73699567ac9e1fd84bf24a4a30999a69264ad"
