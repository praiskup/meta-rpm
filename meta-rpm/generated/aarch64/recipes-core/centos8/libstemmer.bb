SUMMARY = "generated recipe based on libstemmer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libstemmer = "libstemmer.so.0"
RPM_SONAME_REQ_libstemmer = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libstemmer = "glibc"
RPM_SONAME_REQ_libstemmer-devel = "libstemmer.so.0"
RPROVIDES_libstemmer-devel = "libstemmer-dev (= 0)"
RDEPENDS_libstemmer-devel = "libstemmer"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libstemmer-0-10.585svn.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libstemmer-devel-0-10.585svn.el8.aarch64.rpm \
          "

SRC_URI[libstemmer.sha256sum] = "9d17e43486ac5add558ad43514b81444a6e2603d5f9e8d24b89d41ae5b98b4f6"
SRC_URI[libstemmer-devel.sha256sum] = "bbcf5568a80a04e6bb3ea0846691f9ae893c569fff2e4f73203ecda86beb5a60"
