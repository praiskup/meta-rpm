SUMMARY = "generated recipe based on tcsh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcrypt ncurses pkgconfig-native"
RPM_SONAME_REQ_tcsh = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libtinfo.so.6"
RDEPENDS_tcsh = "bash coreutils glibc grep libxcrypt ncurses-libs sed"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tcsh-6.20.00-12.el8.aarch64.rpm \
          "

SRC_URI[tcsh.sha256sum] = "5524599567dca40c258affb812698dd8951cda147446a90fb19cb73efd2d9df2"
