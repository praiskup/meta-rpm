SUMMARY = "generated recipe based on oddjob srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs libselinux libxml2 pam pkgconfig-native"
RPM_SONAME_REQ_oddjob = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libselinux.so.1 libxml2.so.2"
RDEPENDS_oddjob = "bash dbus dbus-libs glibc libselinux libxml2 psmisc systemd"
RPM_SONAME_REQ_oddjob-mkhomedir = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libpam.so.0 libselinux.so.1"
RDEPENDS_oddjob-mkhomedir = "bash dbus-libs dbus-tools glibc grep libselinux oddjob pam psmisc sed"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/oddjob-0.34.4-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/oddjob-mkhomedir-0.34.4-7.el8.aarch64.rpm \
          "

SRC_URI[oddjob.sha256sum] = "9101ebf3a322fa9f1ed7867e0399afa74a4eb807ffaa26fb5af0dc70b6511164"
SRC_URI[oddjob-mkhomedir.sha256sum] = "0c2318fe55367444c65d843c3b92e9ed57b0abbf69e8909f094c46a9b2cb8f9f"
