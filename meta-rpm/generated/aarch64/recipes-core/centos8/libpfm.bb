SUMMARY = "generated recipe based on libpfm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_PROV_libpfm = "libpfm.so.4"
RPM_SONAME_REQ_libpfm = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libpfm = "glibc"
RPM_SONAME_REQ_libpfm-devel = "libpfm.so.4"
RPROVIDES_libpfm-devel = "libpfm-dev (= 4.10.1)"
RDEPENDS_libpfm-devel = "libpfm"
RDEPENDS_libpfm-static = "libpfm"
RPM_SONAME_REQ_python3-libpfm = "ld-linux-aarch64.so.1 libc.so.6 libpfm.so.4 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-libpfm = "glibc libpfm platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpfm-4.10.1-2.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpfm-devel-4.10.1-2.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpfm-static-4.10.1-2.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-libpfm-4.10.1-2.el8_2.1.aarch64.rpm \
          "

SRC_URI[libpfm.sha256sum] = "17eb3b92de4417932087074aa3ec9374de1027e74802656b29069110a4407bd8"
SRC_URI[libpfm-devel.sha256sum] = "b601004dc1f265d627b843b9b380c211e0182b7d66a9656f32980594b2636a75"
SRC_URI[libpfm-static.sha256sum] = "f869070381c1d2d81aa2a517178f95033e443ebfc1da41f2a3d50068ece2a783"
SRC_URI[python3-libpfm.sha256sum] = "88a9b96724fd169af3c394d59ae8faff0adc3d9acc2b06fdeb5f640218850094"
