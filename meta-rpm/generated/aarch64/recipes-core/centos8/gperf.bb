SUMMARY = "generated recipe based on gperf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gperf = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gperf = "bash glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gperf-3.1-5.el8.aarch64.rpm \
          "

SRC_URI[gperf.sha256sum] = "6bb0e8d76aefd4dc6bf1b7ee097383eed3b9a4cfe3f1f06688fb8feab6c028e6"
