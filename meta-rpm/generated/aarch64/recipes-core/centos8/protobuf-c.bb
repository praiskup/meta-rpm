SUMMARY = "generated recipe based on protobuf-c srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native protobuf"
RPM_SONAME_PROV_protobuf-c = "libprotobuf-c.so.1"
RPM_SONAME_REQ_protobuf-c = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_protobuf-c = "glibc"
RPM_SONAME_REQ_protobuf-c-compiler = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libprotobuf.so.15 libprotoc.so.15 libpthread.so.0 libstdc++.so.6"
RDEPENDS_protobuf-c-compiler = "glibc libgcc libstdc++ protobuf protobuf-c protobuf-compiler"
RPM_SONAME_REQ_protobuf-c-devel = "libprotobuf-c.so.1"
RPROVIDES_protobuf-c-devel = "protobuf-c-dev (= 1.3.0)"
RDEPENDS_protobuf-c-devel = "pkgconf-pkg-config protobuf-c protobuf-c-compiler"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/protobuf-c-1.3.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/protobuf-c-compiler-1.3.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/protobuf-c-devel-1.3.0-4.el8.aarch64.rpm \
          "

SRC_URI[protobuf-c.sha256sum] = "5a987dd3025eb9f180ad277f0c329dfa51117a75358084207dcf01da2956c0c2"
SRC_URI[protobuf-c-compiler.sha256sum] = "a20e7896f9de91753c1395c5d50c2f84a3e347c88e5ca26f31a608973a7d73bf"
SRC_URI[protobuf-c-devel.sha256sum] = "28f63344de5cbbfa315d1b3c76aa825298f86e399d191b19f2cc9e9e8fb068ba"
