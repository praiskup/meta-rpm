SUMMARY = "generated recipe based on diffutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_diffutils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_diffutils = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/diffutils-3.6-6.el8.aarch64.rpm \
          "

SRC_URI[diffutils.sha256sum] = "8cbebc0fa970ceca4f479ee292eaad155084987be2cf7f97bbafe4a529319c98"
