SUMMARY = "generated recipe based on libshout srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libogg libtheora libvorbis pkgconfig-native speex"
RPM_SONAME_PROV_libshout = "libshout.so.3"
RPM_SONAME_REQ_libshout = "ld-linux-aarch64.so.1 libc.so.6 libogg.so.0 libpthread.so.0 libspeex.so.1 libtheora.so.0 libvorbis.so.0"
RDEPENDS_libshout = "glibc libogg libtheora libvorbis speex"
RPM_SONAME_REQ_libshout-devel = "libshout.so.3"
RPROVIDES_libshout-devel = "libshout-dev (= 2.2.2)"
RDEPENDS_libshout-devel = "libogg-devel libshout libtheora-devel libvorbis-devel pkgconf-pkg-config speex-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libshout-2.2.2-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libshout-devel-2.2.2-19.el8.aarch64.rpm \
          "

SRC_URI[libshout.sha256sum] = "e22842ff714c30e205ebebb3cbee14a5fe313a9fa57ca663363627e07daa10d1"
SRC_URI[libshout-devel.sha256sum] = "e6ec5a45d72094b73879bbb963ae0dae00cb17be0e1ea9c3da07149924078cf7"
