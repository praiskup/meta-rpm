SUMMARY = "generated recipe based on perl-IO-String srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-String = "perl-Data-Dumper perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-IO-String-1.08-31.el8.noarch.rpm \
          "

SRC_URI[perl-IO-String.sha256sum] = "97326e2416d5ccaa7e9cd36436c21619a23bad8de089f08a429a4e7fa4e01c2d"
