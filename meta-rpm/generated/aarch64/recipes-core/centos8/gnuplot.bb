SUMMARY = "generated recipe based on gnuplot srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo fontconfig freetype gd glib-2.0 libgcc libjpeg-turbo libpng libwebp libx11 libxpm lua pango pkgconfig-native qt5-qtbase qt5-qtsvg tiff zlib"
RPM_SONAME_REQ_gnuplot = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libX11.so.6 libXpm.so.4 libc.so.6 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgd.so.3 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 liblua-5.3.so libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libstdc++.so.6 libtiff.so.5 libwebp.so.7 libz.so.1"
RDEPENDS_gnuplot = "bash cairo chkconfig dejavu-sans-fonts fontconfig freetype gd glib2 glibc gnuplot-common libX11 libXpm libgcc libjpeg-turbo libpng libstdc++ libtiff libwebp lua-libs pango qt5-qtbase qt5-qtbase-gui qt5-qtsvg zlib"
RPM_SONAME_REQ_gnuplot-common = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libcairo.so.2 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libz.so.1"
RDEPENDS_gnuplot-common = "bash cairo glib2 glibc info libX11 pango zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnuplot-5.2.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnuplot-common-5.2.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gnuplot-doc-5.2.4-1.el8.noarch.rpm \
          "

SRC_URI[gnuplot.sha256sum] = "6d9fdf7e67258beff08808bcd448c779d78fe49c3f0531987d46ca39af5c5a19"
SRC_URI[gnuplot-common.sha256sum] = "1c149d54804f30e8faa0bf4e21529df336b25af9fdec89a8a671785d72fd0580"
SRC_URI[gnuplot-doc.sha256sum] = "72d50d00a0e1d718216d177dfa5ed23c6dc9f2ee5ef76829416a147c64e6bd17"
