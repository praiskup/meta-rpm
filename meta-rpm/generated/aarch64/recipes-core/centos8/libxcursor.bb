SUMMARY = "generated recipe based on libXcursor srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxfixes libxrender pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXcursor = "libXcursor.so.1"
RPM_SONAME_REQ_libXcursor = "ld-linux-aarch64.so.1 libX11.so.6 libXfixes.so.3 libXrender.so.1 libc.so.6"
RDEPENDS_libXcursor = "glibc libX11 libXfixes libXrender"
RPM_SONAME_REQ_libXcursor-devel = "libXcursor.so.1"
RPROVIDES_libXcursor-devel = "libXcursor-dev (= 1.1.15)"
RDEPENDS_libXcursor-devel = "libX11-devel libXcursor libXfixes-devel libXrender-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXcursor-1.1.15-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXcursor-devel-1.1.15-3.el8.aarch64.rpm \
          "

SRC_URI[libXcursor.sha256sum] = "9e52d8a6d683d745b6f07a80c7f0518a7a3d741bded2650e1c940615e3c1dd2b"
SRC_URI[libXcursor-devel.sha256sum] = "33b4e6d58f8810d3d0aed571b6fe285d926a802a7305023137f4c7c9355129af"
