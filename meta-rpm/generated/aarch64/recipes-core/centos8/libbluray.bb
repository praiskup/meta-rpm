SUMMARY = "generated recipe based on libbluray srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libxml2 pkgconfig-native"
RPM_SONAME_PROV_libbluray = "libbluray.so.2"
RPM_SONAME_REQ_libbluray = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libpthread.so.0 libxml2.so.2"
RDEPENDS_libbluray = "fontconfig freetype glibc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libbluray-1.0.2-3.el8.aarch64.rpm \
          "

SRC_URI[libbluray.sha256sum] = "3f3133b3fafdb23aa41b1253f7d2001ffc1992993e93d545252e572d792d34a8"
