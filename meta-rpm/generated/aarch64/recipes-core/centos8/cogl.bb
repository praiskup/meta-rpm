SUMMARY = "generated recipe based on cogl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gdk-pixbuf glib-2.0 libdrm libglvnd libx11 libxcomposite libxdamage libxext libxfixes libxrandr mesa pango pkgconfig-native wayland"
RPM_SONAME_PROV_cogl = "libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20"
RPM_SONAME_REQ_cogl = "ld-linux-aarch64.so.1 libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXrandr.so.2 libc.so.6 libcairo.so.2 libcogl.so.20 libdrm.so.2 libgbm.so.1 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libwayland-client.so.0 libwayland-egl.so.1 libwayland-server.so.0"
RDEPENDS_cogl = "cairo gdk-pixbuf2 glib2 glibc libX11 libXcomposite libXdamage libXext libXfixes libXrandr libdrm libglvnd-egl libwayland-client libwayland-egl libwayland-server mesa-libgbm pango"
RPM_SONAME_REQ_cogl-devel = "libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20"
RPROVIDES_cogl-devel = "cogl-dev (= 1.22.2)"
RDEPENDS_cogl-devel = "cairo-devel cogl gdk-pixbuf2-devel glib2-devel libX11-devel libXcomposite-devel libXdamage-devel libXext-devel libXfixes-devel libXrandr-devel libdrm-devel libglvnd-devel mesa-libgbm-devel pkgconf-pkg-config wayland-devel"
RDEPENDS_cogl-doc = "cogl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cogl-1.22.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cogl-devel-1.22.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cogl-doc-1.22.2-10.el8.noarch.rpm \
          "

SRC_URI[cogl.sha256sum] = "a36983fca23f61b744fe500cdecd763f93bbbde8e5840f022d3ae5ad6992352f"
SRC_URI[cogl-devel.sha256sum] = "407b7620993b77bbb0707b343e342487a53ea5da259fccd50934d5e6dd062831"
SRC_URI[cogl-doc.sha256sum] = "7a3acd7e2f8202bf0a26f8b1db4808cc54269283f5d03404fb27512023025bb9"
