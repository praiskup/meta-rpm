SUMMARY = "generated recipe based on mozilla-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mozilla-filesystem-1.9-18.el8.aarch64.rpm \
          "

SRC_URI[mozilla-filesystem.sha256sum] = "981cef426f95be024fb1ec6d8d99cffb04c99db3ac8005813b849c2402fc998a"
