SUMMARY = "generated recipe based on totem-pl-parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libarchive libgcrypt libquvi libxml2 pkgconfig-native"
RPM_SONAME_PROV_totem-pl-parser = "libtotem-plparser-mini.so.18 libtotem-plparser.so.18"
RPM_SONAME_REQ_totem-pl-parser = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libquvi-0.9-0.9.4.so libxml2.so.2"
RDEPENDS_totem-pl-parser = "glib2 glibc libarchive libgcrypt libquvi libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/totem-pl-parser-3.26.1-2.el8.aarch64.rpm \
          "

SRC_URI[totem-pl-parser.sha256sum] = "3b81d4a0a84adf2f95f8df91a140d316a77a60192fbfd1f09897478927c94bf5"
