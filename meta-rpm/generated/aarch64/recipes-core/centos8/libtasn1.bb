SUMMARY = "generated recipe based on libtasn1 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libtasn1 = "libtasn1.so.6"
RPM_SONAME_REQ_libtasn1 = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libtasn1 = "glibc"
RPM_SONAME_REQ_libtasn1-devel = "libtasn1.so.6"
RPROVIDES_libtasn1-devel = "libtasn1-dev (= 4.13)"
RDEPENDS_libtasn1-devel = "bash info libtasn1 pkgconf-pkg-config"
RPM_SONAME_REQ_libtasn1-tools = "ld-linux-aarch64.so.1 libc.so.6 libtasn1.so.6"
RDEPENDS_libtasn1-tools = "glibc libtasn1"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtasn1-devel-4.13-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtasn1-tools-4.13-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtasn1-4.13-3.el8.aarch64.rpm \
          "

SRC_URI[libtasn1.sha256sum] = "3401ccfb7fd08c12578b6257b4dac7e94ba5f4cd70fc6a234fd90bb99d1bb108"
SRC_URI[libtasn1-devel.sha256sum] = "27f7231220ce81b6b4e8b4d8b8b74f960846ddd64f0e8180c6963c7b0eac866b"
SRC_URI[libtasn1-tools.sha256sum] = "6113c7e0a3dce12c29c002465c1de42e6102edcb7796f1cc922fb87d024e127f"
