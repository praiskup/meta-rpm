SUMMARY = "generated recipe based on liblouis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liblouis = "liblouis.so.2"
RPM_SONAME_REQ_liblouis = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_liblouis = "bash glibc info"
RDEPENDS_python3-louis = "liblouis platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liblouis-2.6.2-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-louis-2.6.2-21.el8.noarch.rpm \
          "

SRC_URI[liblouis.sha256sum] = "ac49851ec02aab0f8b811f1ad5a99027cd71b790b8cf6b6fd32e7888959b906f"
SRC_URI[python3-louis.sha256sum] = "3708d5c74a26e71fad43cffba2be1f3482a924148f75c4ec1ef6a8c607953ea7"
