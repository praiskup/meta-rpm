SUMMARY = "generated recipe based on openjpeg2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lcms2 libpng pkgconfig-native tiff zlib"
RPM_SONAME_PROV_openjpeg2 = "libopenjp2.so.7"
RPM_SONAME_REQ_openjpeg2 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_openjpeg2 = "glibc"
RPM_SONAME_REQ_openjpeg2-devel = "libopenjp2.so.7"
RPROVIDES_openjpeg2-devel = "openjpeg2-dev (= 2.3.1)"
RDEPENDS_openjpeg2-devel = "openjpeg2 openjpeg2-tools pkgconf-pkg-config"
RPM_SONAME_REQ_openjpeg2-tools = "ld-linux-aarch64.so.1 libc.so.6 liblcms2.so.2 libm.so.6 libopenjp2.so.7 libpng16.so.16 libpthread.so.0 librt.so.1 libtiff.so.5 libz.so.1"
RDEPENDS_openjpeg2-tools = "glibc lcms2 libpng libtiff openjpeg2 zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openjpeg2-2.3.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openjpeg2-devel-docs-2.3.1-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openjpeg2-tools-2.3.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openjpeg2-devel-2.3.1-6.el8.aarch64.rpm \
          "

SRC_URI[openjpeg2.sha256sum] = "01e0e6a6e1e4809fa915582d41f6e18591a995127827fe28d5de971a77d63b31"
SRC_URI[openjpeg2-devel.sha256sum] = "31dca4f61a684cda17ce5091d5367109ccc49abc84c9cf230fc4cfbbccc493f3"
SRC_URI[openjpeg2-devel-docs.sha256sum] = "9a40317d1eccfb66639e65008c3d201df5cfc53225a45c9770dfd2edd398bf09"
SRC_URI[openjpeg2-tools.sha256sum] = "61ed13859bbd7eb6289ca89b70070622937a356a0a8bcc261a69330dc535c4b1"
