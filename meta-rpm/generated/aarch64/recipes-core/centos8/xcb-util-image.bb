SUMMARY = "generated recipe based on xcb-util-image srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native xcb-util"
RPM_SONAME_PROV_xcb-util-image = "libxcb-image.so.0"
RPM_SONAME_REQ_xcb-util-image = "ld-linux-aarch64.so.1 libc.so.6 libxcb-shm.so.0 libxcb-util.so.1 libxcb.so.1"
RDEPENDS_xcb-util-image = "glibc libxcb xcb-util"
RPM_SONAME_REQ_xcb-util-image-devel = "libxcb-image.so.0"
RPROVIDES_xcb-util-image-devel = "xcb-util-image-dev (= 0.4.0)"
RDEPENDS_xcb-util-image-devel = "libxcb-devel pkgconf-pkg-config xcb-util-image"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xcb-util-image-0.4.0-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xcb-util-image-devel-0.4.0-9.el8.aarch64.rpm \
          "

SRC_URI[xcb-util-image.sha256sum] = "7e2efafd0d732399b1106c24c58de97ba72ab4a5011bfc9d3933e8ef8defd9b3"
SRC_URI[xcb-util-image-devel.sha256sum] = "bc4596c0d4a8073597e48bd07b8a9743b4fec7e8e69d20dfd7b8932d4e3414b1"
