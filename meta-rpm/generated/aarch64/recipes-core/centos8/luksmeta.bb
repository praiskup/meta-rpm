SUMMARY = "generated recipe based on luksmeta srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cryptsetup cryptsetup-libs pkgconfig-native"
RPM_SONAME_PROV_libluksmeta = "libluksmeta.so.0"
RPM_SONAME_REQ_libluksmeta = "ld-linux-aarch64.so.1 libc.so.6 libcryptsetup.so.12"
RDEPENDS_libluksmeta = "cryptsetup-libs glibc"
RPM_SONAME_REQ_libluksmeta-devel = "libluksmeta.so.0"
RPROVIDES_libluksmeta-devel = "libluksmeta-dev (= 9)"
RDEPENDS_libluksmeta-devel = "cryptsetup-devel libluksmeta pkgconf-pkg-config"
RPM_SONAME_REQ_luksmeta = "ld-linux-aarch64.so.1 libc.so.6 libcryptsetup.so.12 libluksmeta.so.0"
RDEPENDS_luksmeta = "cryptsetup-libs glibc libluksmeta"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libluksmeta-9-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libluksmeta-devel-9-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/luksmeta-9-4.el8.aarch64.rpm \
          "

SRC_URI[libluksmeta.sha256sum] = "165b83a4d4870c7aec7f871ac08d0bc7db1d5c9a5d966e1b8d9bd38d974e500b"
SRC_URI[libluksmeta-devel.sha256sum] = "8999bf9bd7e42536cc86b3f876e2c33c8b5c6edda4c0de3e51640c0b64444f46"
SRC_URI[luksmeta.sha256sum] = "983fcd8b2c1ab80737c9df01da446260645a2697d779339963b725e8735efd5d"
