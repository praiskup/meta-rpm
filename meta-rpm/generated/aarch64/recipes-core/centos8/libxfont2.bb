SUMMARY = "generated recipe based on libXfont2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libfontenc pkgconfig-native xorg-x11-proto-devel zlib"
RPM_SONAME_PROV_libXfont2 = "libXfont2.so.2"
RPM_SONAME_REQ_libXfont2 = "ld-linux-aarch64.so.1 libc.so.6 libfontenc.so.1 libfreetype.so.6 libm.so.6 libz.so.1"
RDEPENDS_libXfont2 = "freetype glibc libfontenc zlib"
RPM_SONAME_REQ_libXfont2-devel = "libXfont2.so.2"
RPROVIDES_libXfont2-devel = "libXfont2-dev (= 2.0.3)"
RDEPENDS_libXfont2-devel = "freetype-devel libXfont2 libfontenc-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXfont2-2.0.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libXfont2-devel-2.0.3-2.el8.aarch64.rpm \
          "

SRC_URI[libXfont2.sha256sum] = "7f649f30ac82dba8ddc147b9e5102bf8a28d7423e6d818efd0ca1e870077349c"
SRC_URI[libXfont2-devel.sha256sum] = "3eb05779f3cc27f34c5389b5d4debad20fcf2f3e2d74c80f6928fa21a37cace3"
