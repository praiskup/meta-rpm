SUMMARY = "generated recipe based on bacula srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl libcap libgcc libpq libxcrypt lzo mariadb-connector-c ncurses openssl pkgconfig-native readline sqlite3 zlib"
RPM_SONAME_REQ_bacula-client = "ld-linux-aarch64.so.1 libacl.so.1 libbac-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzo2.so.2 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-client = "bacula-common bacula-libs bash glibc libacl libcap libgcc libstdc++ lzo openssl-libs systemd zlib"
RDEPENDS_bacula-common = "bacula-libs bash shadow-utils"
RPM_SONAME_REQ_bacula-console = "ld-linux-aarch64.so.1 libbac-9.0.6.so libbaccfg-9.0.6.so libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libreadline.so.7 libssl.so.1.1 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_bacula-console = "bacula-libs glibc libgcc libstdc++ ncurses-libs openssl-libs readline"
RPM_SONAME_REQ_bacula-director = "ld-linux-aarch64.so.1 libbac-9.0.6.so libbaccats-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libbacsql-9.0.6.so libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-director = "bacula-common bacula-libs bacula-libs-sql bash glibc libcap libgcc libstdc++ openssl-libs perl-interpreter perl-libs systemd zlib"
RPM_SONAME_PROV_bacula-libs = "libbac-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libbacsd-9.0.6.so"
RPM_SONAME_REQ_bacula-libs = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-libs = "glibc libcap libgcc libstdc++ openssl-libs zlib"
RPM_SONAME_PROV_bacula-libs-sql = "libbaccats-9.0.6.so libbacsql-9.0.6.so"
RPM_SONAME_REQ_bacula-libs-sql = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libmariadb.so.3 libpq.so.5 libpthread.so.0 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-libs-sql = "bash glibc libgcc libpq libstdc++ libxcrypt mariadb-connector-c openssl-libs sqlite-libs zlib"
RDEPENDS_bacula-logwatch = "bacula-director logwatch perl-interpreter perl-libs"
RPM_SONAME_REQ_bacula-storage = "ld-linux-aarch64.so.1 libbac-9.0.6.so libbaccats-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libbacsd-9.0.6.so libbacsql-9.0.6.so libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzo2.so.2 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-storage = "bacula-common bacula-libs bacula-libs-sql bash glibc libcap libgcc libstdc++ lzo mt-st mtx openssl-libs systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-client-9.0.6-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-common-9.0.6-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-console-9.0.6-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-director-9.0.6-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-libs-9.0.6-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-libs-sql-9.0.6-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-logwatch-9.0.6-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bacula-storage-9.0.6-6.el8.aarch64.rpm \
          "

SRC_URI[bacula-client.sha256sum] = "5487da4558827fd0a81649944c89f657e7d49ea81e7c923aa85e45ab69489dbb"
SRC_URI[bacula-common.sha256sum] = "773c8b05906a5af1c1927bcaf80fec626d4d1afc9246ccec3185e3775e12726d"
SRC_URI[bacula-console.sha256sum] = "012dad8f5195779b90b24c61337ab49925e03bc2a2e27afd5addf5d96c2cf870"
SRC_URI[bacula-director.sha256sum] = "66454fe73944bba4993873ac8717703aaad16b5204259be442dc281e68865c4d"
SRC_URI[bacula-libs.sha256sum] = "9bc58b684c30e43a4ae847caadfc8ac362ead48f4191090011bdaf1d287a3c3a"
SRC_URI[bacula-libs-sql.sha256sum] = "a292909bba2fd92a35139e1d2aa5dad4cd08d12d0eb544f87cb4c32d2ceee265"
SRC_URI[bacula-logwatch.sha256sum] = "a1580e52ea83a7075b87eb95fa62330a54a356ffc42cc5ea19a894a8179a49bb"
SRC_URI[bacula-storage.sha256sum] = "300044030d5551fe9b7b8d86477a615d11fbcb4a8bfd5b503fa6cd19b265b708"
