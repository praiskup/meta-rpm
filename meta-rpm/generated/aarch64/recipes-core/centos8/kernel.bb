SUMMARY = "generated recipe based on kernel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libbpf libxcrypt ncurses numactl openssl pciutils perl pkgconfig-native platform-python3 slang xz zlib"
RPM_SONAME_REQ_bpftool = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libelf.so.1 libz.so.1"
RDEPENDS_bpftool = "elfutils-libelf glibc zlib"
RDEPENDS_kernel = "kernel-core kernel-modules"
RDEPENDS_kernel-core = "bash coreutils dracut linux-firmware systemd systemd-udev"
RDEPENDS_kernel-debug = "kernel-debug-core kernel-debug-modules"
RDEPENDS_kernel-debug-core = "bash coreutils dracut linux-firmware systemd systemd-udev"
RPROVIDES_kernel-debug-devel = "kernel-debug-dev (= 4.18.0)"
RDEPENDS_kernel-debug-devel = "bash findutils perl-interpreter"
RDEPENDS_kernel-debug-modules = "bash kernel-core"
RDEPENDS_kernel-debug-modules-extra = "bash kernel-core kernel-debug-modules"
RPROVIDES_kernel-devel = "kernel-dev (= 4.18.0)"
RDEPENDS_kernel-devel = "bash findutils perl-interpreter"
RDEPENDS_kernel-modules = "bash kernel-core"
RDEPENDS_kernel-modules-extra = "bash kernel-core kernel-modules"
RPM_SONAME_REQ_kernel-tools = "ld-linux-aarch64.so.1 libc.so.6 libcpupower.so.0 libm.so.6 libncursesw.so.6 libpanelw.so.6 libpci.so.3 libpthread.so.0 librt.so.1 libtinfo.so.6"
RDEPENDS_kernel-tools = "glibc kernel-tools-libs ncurses-libs pciutils-libs platform-python"
RPM_SONAME_PROV_kernel-tools-libs = "libcpupower.so.0"
RPM_SONAME_REQ_kernel-tools-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_kernel-tools-libs = "bash glibc"
RPM_SONAME_REQ_kernel-tools-libs-devel = "libcpupower.so.0"
RPROVIDES_kernel-tools-libs-devel = "kernel-tools-libs-dev (= 4.18.0)"
RDEPENDS_kernel-tools-libs-devel = "kernel-tools kernel-tools-libs"
RPM_SONAME_PROV_perf = "libperf-jvmti.so"
RPM_SONAME_REQ_perf = "ld-linux-aarch64.so.1 libbpf.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libdw.so.1 libelf.so.1 liblzma.so.5 libm.so.6 libnuma.so.1 libperl.so.5.26 libpthread.so.0 libpython3.6m.so.1.0 libresolv.so.2 librt.so.1 libslang.so.2 libutil.so.1 libz.so.1"
RDEPENDS_perf = "bash elfutils-libelf elfutils-libs glibc libbpf libxcrypt numactl-libs openssl-libs perl-Exporter perl-interpreter perl-libs python3-libs slang xz-libs zlib"
RPM_SONAME_REQ_python3-perf = "ld-linux-aarch64.so.1 libc.so.6 libnuma.so.1 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-perf = "glibc numactl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bpftool-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-abi-whitelists-4.18.0-193.28.1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-core-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-cross-headers-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-debug-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-debug-core-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-debug-devel-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-debug-modules-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-debug-modules-extra-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-devel-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-doc-4.18.0-193.28.1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-modules-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-modules-extra-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-tools-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kernel-tools-libs-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perf-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-perf-4.18.0-193.28.1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/kernel-tools-libs-devel-4.18.0-193.28.1.el8_2.aarch64.rpm \
          "

SRC_URI[bpftool.sha256sum] = "95911d9f392c3e0dd353d80dcd2dccd43658ef6c2739fa1313679fc1f636e247"
SRC_URI[kernel.sha256sum] = "6ff66b1cd1cc0882e43c447ea2348d8aa9750d9e28afa8d55051c4bcbdf666b1"
SRC_URI[kernel-abi-whitelists.sha256sum] = "f3926b631a76d0fb965e48edde0580d191eca6ac26aa1849978e1ee268b7b2d9"
SRC_URI[kernel-core.sha256sum] = "86aec8d46d41cd350779424980bd3b53c3130b4f506066a6f842916d57b4a9bf"
SRC_URI[kernel-cross-headers.sha256sum] = "95518eecb39715637cdb07c16072e2b709dd7c6871ba0d353cbe067718220312"
SRC_URI[kernel-debug.sha256sum] = "812f291e97f11e0e6d52b6312737301dfb255034ce27b619a7903a7921018df9"
SRC_URI[kernel-debug-core.sha256sum] = "677336f278fbc1e88d68803ce747b449eafb6bf11815cfba12d16f6375960309"
SRC_URI[kernel-debug-devel.sha256sum] = "4bc45b2e0746fa316df003b02f61d8ece31a525d6fe2edb8040b9ab36f25a7a9"
SRC_URI[kernel-debug-modules.sha256sum] = "dc1989a067fb1044f0afa0369299890f262a78e7dc115ee047bf22eb44e975bd"
SRC_URI[kernel-debug-modules-extra.sha256sum] = "3d56d432add6eea2e2ea9593f20c1abe9307499602657346e73e3771075c1c53"
SRC_URI[kernel-devel.sha256sum] = "5c983d08866e700847eb3ab3f26226b0763b2c648e1faa74e3854d0bcb765f05"
SRC_URI[kernel-doc.sha256sum] = "1164814dc2fcd9e3e3c7798e02a2bc3ad176a259f1dc55dcac6a7d571f1cf625"
SRC_URI[kernel-modules.sha256sum] = "9a1d5efabc8a7769120b6ef3f4b37c6da247fc37fa671a2a66c9839d74131dc0"
SRC_URI[kernel-modules-extra.sha256sum] = "8d3abf9a332309dc133d7380bf4d632a337e992fbbda737e8d9ee5739fabb4f9"
SRC_URI[kernel-tools.sha256sum] = "761e69a6e530767fdabf7cb178ed9fdf6a98a9848b6c910f186fcd15a8946838"
SRC_URI[kernel-tools-libs.sha256sum] = "59018225ee7f5b2d70196ab06ae7e6b47b79cd79d67f5bcb6f86360e141bdea3"
SRC_URI[kernel-tools-libs-devel.sha256sum] = "68d0601eb1b0ec0fb44a5a019ea33988fddcfa3e8d9e3d36f764d33480a0d4f7"
SRC_URI[perf.sha256sum] = "8ff7e3d20da6d5e156ff4e3c8308a5b556da49232e1fb133f6efc5016d8e2d1d"
SRC_URI[python3-perf.sha256sum] = "99b465dd0706dd79e004dbbf96b2198d831d3c59641918b04ae279bd179faacc"
