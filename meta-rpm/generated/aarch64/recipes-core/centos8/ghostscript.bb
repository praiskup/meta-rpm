SUMMARY = "generated recipe based on ghostscript srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs e2fsprogs fontconfig freetype jbig2dec krb5-libs lcms2 libice libidn libijs libjpeg-turbo libpaper libpng libsm libx11 libxcrypt libxext libxt openjpeg2 pkgconfig-native tiff zlib"
RPM_SONAME_REQ_ghostscript = "ld-linux-aarch64.so.1 libc.so.6 libgs.so.9"
RDEPENDS_ghostscript = "bash glibc libgs"
RDEPENDS_ghostscript-doc = "ghostscript"
RDEPENDS_ghostscript-tools-dvipdf = "bash ghostscript texlive-dvips"
RDEPENDS_ghostscript-tools-fonts = "bash ghostscript"
RDEPENDS_ghostscript-tools-printing = "bash ghostscript"
RPM_SONAME_REQ_ghostscript-x11 = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXt.so.6 libc.so.6"
RDEPENDS_ghostscript-x11 = "ghostscript glibc libICE libSM libX11 libXext libXt"
RPM_SONAME_PROV_libgs = "libgs.so.9"
RPM_SONAME_REQ_libgs = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgssapi_krb5.so.2 libidn.so.11 libijs-0.35.so libjbig2dec.so.0 libjpeg.so.62 libk5crypto.so.3 libkrb5.so.3 liblcms2.so.2 libm.so.6 libopenjp2.so.7 libpaper.so.1 libpng16.so.16 libpthread.so.0 libtiff.so.5 libz.so.1"
RDEPENDS_libgs = "adobe-mappings-cmap adobe-mappings-cmap-deprecated adobe-mappings-pdf cups-libs fontconfig freetype glibc google-droid-sans-fonts jbig2dec-libs krb5-libs lcms2 libcom_err libidn libijs libjpeg-turbo libpaper libpng libtiff libxcrypt openjpeg2 urw-base35-fonts zlib"
RPM_SONAME_REQ_libgs-devel = "libgs.so.9"
RPROVIDES_libgs-devel = "libgs-dev (= 9.25)"
RDEPENDS_libgs-devel = "libgs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ghostscript-9.25-5.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgs-9.25-5.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ghostscript-doc-9.25-5.el8_1.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ghostscript-tools-dvipdf-9.25-5.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ghostscript-tools-fonts-9.25-5.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ghostscript-tools-printing-9.25-5.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ghostscript-x11-9.25-5.el8_1.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgs-devel-9.25-5.el8_1.1.aarch64.rpm \
          "

SRC_URI[ghostscript.sha256sum] = "3caa84621b0531d6ddda361b72aa759c27b5f76abcc007d0ff1715526226c9de"
SRC_URI[ghostscript-doc.sha256sum] = "9e17e9613ebc2032aca81729d0a900d112972fbee37cd99980221683a921728d"
SRC_URI[ghostscript-tools-dvipdf.sha256sum] = "3e6ea75dc9a3e1ba916b582159043a2d54d0f59c09889e441b0eed780bca43f1"
SRC_URI[ghostscript-tools-fonts.sha256sum] = "fbb3acd9f9ffda5c32bff0be5a6fbb69bc12cc8711be05038883309197bc908a"
SRC_URI[ghostscript-tools-printing.sha256sum] = "9ff9cea83b68af85b9d120038a39ed46109fc005eb175c31e6ba3f3bd7553b77"
SRC_URI[ghostscript-x11.sha256sum] = "c2eb60a3cd2a268de2e82577c87cc1143f814bd5918f8a2e22c1e67516c98eb4"
SRC_URI[libgs.sha256sum] = "311c829733be1736951eb09d176ff7475ac67e8063b82970baa1e924ffc504a1"
SRC_URI[libgs-devel.sha256sum] = "9ce689735c4097050ee85cf825d245a48d94b910d3ac7ab23f9f92a24e8e8dd4"
