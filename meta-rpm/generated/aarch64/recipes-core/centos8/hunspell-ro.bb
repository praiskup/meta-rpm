SUMMARY = "generated recipe based on hunspell-ro srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ro = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ro-3.3.7-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-ro.sha256sum] = "4652e2a9d606252b4c36adc6e93c33546558748754db1d39a5678ac3c771f378"
