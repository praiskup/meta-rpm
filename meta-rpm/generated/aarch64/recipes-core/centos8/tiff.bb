SUMMARY = "generated recipe based on libtiff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jbigkit libgcc libjpeg-turbo pkgconf pkgconfig-native zlib"
RPM_SONAME_PROV_libtiff = "libtiff.so.5 libtiffxx.so.5"
RPM_SONAME_REQ_libtiff = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libjbig.so.2.1 libjpeg.so.62 libm.so.6 libstdc++.so.6 libtiff.so.5 libz.so.1"
RDEPENDS_libtiff = "glibc jbigkit-libs libgcc libjpeg-turbo libstdc++ zlib"
RPM_SONAME_REQ_libtiff-devel = "libtiff.so.5 libtiffxx.so.5"
RPROVIDES_libtiff-devel = "libtiff-dev (= 4.0.9)"
RDEPENDS_libtiff-devel = "libtiff pkgconf-pkg-config"
RPM_SONAME_REQ_libtiff-tools = "ld-linux-aarch64.so.1 libc.so.6 libjbig.so.2.1 libjpeg.so.62 libm.so.6 libtiff.so.5 libz.so.1"
RDEPENDS_libtiff-tools = "glibc jbigkit-libs libjpeg-turbo libtiff zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtiff-4.0.9-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtiff-devel-4.0.9-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libtiff-tools-4.0.9-17.el8.aarch64.rpm \
          "

SRC_URI[libtiff.sha256sum] = "023f47539b022a04c8a48d995a88d5834f8e5ab84f503ff7ff22bf4ed19ba871"
SRC_URI[libtiff-devel.sha256sum] = "e50af3e20388c8dee87b2fb106656f6d5df91dbf0dcd630c5f9e6e459b0a4283"
SRC_URI[libtiff-tools.sha256sum] = "b9510b37d0df5fb992844aa91acc508379477cd23b854788e33543ba9a596f36"
