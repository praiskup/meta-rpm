SUMMARY = "generated recipe based on qt5-qtquickcontrols srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtquickcontrols = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtquickcontrols = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtquickcontrols-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtquickcontrols-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtquickcontrols"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtquickcontrols-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtquickcontrols-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtquickcontrols.sha256sum] = "b3509bb88dfb2fc3ba30f60e0d1818bd4c3ad02f80884c24fe370d85b1920c09"
SRC_URI[qt5-qtquickcontrols-examples.sha256sum] = "5c2acd2772107f4cf37dd64cb0e3fbfc1f0f99a140a1c0eeb741ce25b5cd890b"
