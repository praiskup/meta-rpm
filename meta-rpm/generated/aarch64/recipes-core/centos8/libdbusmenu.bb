SUMMARY = "generated recipe based on libdbusmenu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-glib-devel gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_PROV_libdbusmenu = "libdbusmenu-glib.so.4"
RPM_SONAME_REQ_libdbusmenu = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_libdbusmenu = "glib2 glibc"
RPM_SONAME_REQ_libdbusmenu-devel = "libdbusmenu-glib.so.4"
RPROVIDES_libdbusmenu-devel = "libdbusmenu-dev (= 16.04.0)"
RDEPENDS_libdbusmenu-devel = "dbus-glib-devel libdbusmenu pkgconf-pkg-config"
RPM_SONAME_PROV_libdbusmenu-gtk3 = "libdbusmenu-gtk3.so.4"
RPM_SONAME_REQ_libdbusmenu-gtk3 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbusmenu-glib.so.4 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_libdbusmenu-gtk3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libdbusmenu pango"
RPM_SONAME_REQ_libdbusmenu-gtk3-devel = "libdbusmenu-gtk3.so.4"
RPROVIDES_libdbusmenu-gtk3-devel = "libdbusmenu-gtk3-dev (= 16.04.0)"
RDEPENDS_libdbusmenu-gtk3-devel = "dbus-glib-devel gdk-pixbuf2-devel gtk3-devel libdbusmenu libdbusmenu-devel libdbusmenu-gtk3 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdbusmenu-16.04.0-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdbusmenu-gtk3-16.04.0-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdbusmenu-devel-16.04.0-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdbusmenu-doc-16.04.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdbusmenu-gtk3-devel-16.04.0-12.el8.aarch64.rpm \
          "

SRC_URI[libdbusmenu.sha256sum] = "a61c8a4177e75d064927592788fc1d4f6b49dc2b99f38354131fb50c18a28b17"
SRC_URI[libdbusmenu-devel.sha256sum] = "622c24b17fbeedd034e0e0b84109d6db7b9473e3cdb33723b057006a8723630a"
SRC_URI[libdbusmenu-doc.sha256sum] = "22ff2d0bdcef0ac950d6bdb843a3316d509d3e242e623e8faae30d0a8327a02c"
SRC_URI[libdbusmenu-gtk3.sha256sum] = "98bbc3fadadf4430aab75bad4f98e848db887c332aa38a5d2db6e82a71a209d0"
SRC_URI[libdbusmenu-gtk3-devel.sha256sum] = "6c6f1c83c02a38c60190e5de84e137ff0318e3ce92706ff0b8250d8906f868ea"
