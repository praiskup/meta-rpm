SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp isl libmpc mpfr pkgconfig-native zlib"
RPM_SONAME_REQ_cpp = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_cpp = "bash filesystem glibc gmp info libmpc mpfr zlib"
RPM_SONAME_PROV_gcc = "liblto_plugin.so.0"
RPM_SONAME_REQ_gcc = "ld-linux-aarch64.so.1 libasan.so.5 libatomic.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgmp.so.10 libgomp.so.1 libisl.so.15 liblto_plugin.so.0 libm.so.6 libmpc.so.3 libmpfr.so.4 libubsan.so.1 libz.so.1"
RDEPENDS_gcc = "bash binutils cpp glibc glibc-devel gmp info isl libasan libatomic libgcc libgomp libmpc libubsan mpfr zlib"
RPM_SONAME_REQ_gcc-c++ = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-c++ = "gcc glibc gmp libmpc libstdc++ libstdc++-devel mpfr zlib"
RPM_SONAME_PROV_gcc-gdb-plugin = "libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0"
RPM_SONAME_REQ_gcc-gdb-plugin = "ld-linux-aarch64.so.1 libc.so.6 libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gcc-gdb-plugin = "gcc glibc libgcc libstdc++"
RPM_SONAME_REQ_gcc-gfortran = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgfortran.so.5 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-gfortran = "bash gcc glibc gmp info libgfortran libmpc mpfr zlib"
RPM_SONAME_REQ_gcc-plugin-devel = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RPROVIDES_gcc-plugin-devel = "gcc-plugin-dev (= 8.3.1)"
RDEPENDS_gcc-plugin-devel = "gcc glibc gmp-devel libmpc-devel mpfr-devel"
RPM_SONAME_PROV_libgfortran = "libgfortran.so.5"
RPM_SONAME_REQ_libgfortran = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libz.so.1"
RDEPENDS_libgfortran = "glibc libgcc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cpp-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-c++-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-gdb-plugin-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-gfortran-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgfortran-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gcc-plugin-devel-8.3.1-5.el8.0.2.aarch64.rpm \
          "

SRC_URI[cpp.sha256sum] = "0ab90347f952814549a3b362ff1493d690fff78e4e05e37c7ad289f21614d1ff"
SRC_URI[gcc.sha256sum] = "172a5ce9d7a268d555547bdbd993234a789a266ef7d3487e4f562cf89c4de496"
SRC_URI[gcc-c++.sha256sum] = "1f3b31cc6619ad7aaee18868a22803d9608af507fda8185994e0abfae4ca0f44"
SRC_URI[gcc-gdb-plugin.sha256sum] = "edaeed1d79eb46d1b9ca404856bba3057d55fafc63895195d6fc9c99eb6a3fe4"
SRC_URI[gcc-gfortran.sha256sum] = "6730495730fc73c50fbf640328cd6599db368b78c4d515190e0ba64806453708"
SRC_URI[gcc-plugin-devel.sha256sum] = "f841f9d9d017bd4a729f46464494ae90efd87a936eacb861d1353d5e6a82530c"
SRC_URI[libgfortran.sha256sum] = "f7ea75039e2c46afe3993ce65ee990b2f63e42ed52b5e9c2f5996d3f6cd5afe9"
