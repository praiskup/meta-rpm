SUMMARY = "generated recipe based on xcb-util srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util = "libxcb-util.so.1"
RPM_SONAME_REQ_xcb-util = "ld-linux-aarch64.so.1 libc.so.6 libxcb.so.1"
RDEPENDS_xcb-util = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-devel = "libxcb-util.so.1"
RPROVIDES_xcb-util-devel = "xcb-util-dev (= 0.4.0)"
RDEPENDS_xcb-util-devel = "libxcb-devel pkgconf-pkg-config xcb-util"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xcb-util-0.4.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xcb-util-devel-0.4.0-10.el8.aarch64.rpm \
          "

SRC_URI[xcb-util.sha256sum] = "04bd17bb4b74e8f5ba83be21f6ee64ca56467bb02fe52d1005653b2dfe3f1d7a"
SRC_URI[xcb-util-devel.sha256sum] = "aa5eeea6e5294b1928b1c1d61ff0c8197d5b20bef718081476338d0fb31b8b64"
