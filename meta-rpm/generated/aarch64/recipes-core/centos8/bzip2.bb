SUMMARY = "generated recipe based on bzip2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_bzip2 = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6"
RDEPENDS_bzip2 = "bash bzip2-libs glibc"
RPM_SONAME_REQ_bzip2-devel = "libbz2.so.1"
RPROVIDES_bzip2-devel = "bzip2-dev (= 1.0.6)"
RDEPENDS_bzip2-devel = "bzip2-libs pkgconf-pkg-config"
RPM_SONAME_PROV_bzip2-libs = "libbz2.so.1"
RPM_SONAME_REQ_bzip2-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_bzip2-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bzip2-1.0.6-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bzip2-devel-1.0.6-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bzip2-libs-1.0.6-26.el8.aarch64.rpm \
          "

SRC_URI[bzip2.sha256sum] = "b18d9f23161d7d5de93fa72a56c645762deefbc0f3e5a095bb8d9e3cf09521e6"
SRC_URI[bzip2-devel.sha256sum] = "4840034c234b69d1ba9fe63fb16cd2013bcc1448459f90ab6df7f741c34c4598"
SRC_URI[bzip2-libs.sha256sum] = "a4451cae0e8a3307228ed8ac7dc9bab7de77fcbf2004141daa7f986f5dc9b381"
