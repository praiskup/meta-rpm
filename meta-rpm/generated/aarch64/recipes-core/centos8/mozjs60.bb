SUMMARY = "generated recipe based on mozjs60 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_mozjs60 = "libmozjs-60.so.0"
RPM_SONAME_REQ_mozjs60 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mozjs60 = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_mozjs60-devel = "libmozjs-60.so.0"
RPROVIDES_mozjs60-devel = "mozjs60-dev (= 60.9.0)"
RDEPENDS_mozjs60-devel = "mozjs60 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mozjs60-60.9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mozjs60-devel-60.9.0-4.el8.aarch64.rpm \
          "

SRC_URI[mozjs60.sha256sum] = "8a1da341e022af37e9861bb2e8f2b045ad0b36cd783547c0dee08b8097e73c80"
SRC_URI[mozjs60-devel.sha256sum] = "0631ab6f27bdfc54d87446f16f44a99e1fdd936292148fe6cd0127f5e49054b2"
