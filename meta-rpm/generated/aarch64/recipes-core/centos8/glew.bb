SUMMARY = "generated recipe based on glew srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libx11 mesa-libglu pkgconfig-native"
RPM_SONAME_REQ_glew-devel = "libGLEW.so.2.0"
RPROVIDES_glew-devel = "glew-dev (= 2.0.0)"
RDEPENDS_glew-devel = "libGLEW mesa-libGLU-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libGLEW = "libGLEW.so.2.0"
RPM_SONAME_REQ_libGLEW = "ld-linux-aarch64.so.1 libGL.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libGLEW = "glibc libX11 libglvnd-glx"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glew-devel-2.0.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libGLEW-2.0.0-6.el8.aarch64.rpm \
          "

SRC_URI[glew-devel.sha256sum] = "ed82c4c35e45acb825ea95cee4789fc10b1acae3e547c0ac23d05d4fb22dfad8"
SRC_URI[libGLEW.sha256sum] = "85d6467cd4b47cb6fbe7dc9a2363c811fea501c0bd9ec91c14a07af4e6927799"
