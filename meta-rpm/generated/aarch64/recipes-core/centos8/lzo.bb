SUMMARY = "generated recipe based on lzo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_lzo = "liblzo2.so.2"
RPM_SONAME_REQ_lzo = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lzo = "glibc"
RPM_SONAME_REQ_lzo-devel = "liblzo2.so.2 libminilzo.so.0"
RPROVIDES_lzo-devel = "lzo-dev (= 2.08)"
RDEPENDS_lzo-devel = "lzo lzo-minilzo zlib-devel"
RPM_SONAME_PROV_lzo-minilzo = "libminilzo.so.0"
RPM_SONAME_REQ_lzo-minilzo = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lzo-minilzo = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lzo-2.08-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lzo-devel-2.08-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lzo-minilzo-2.08-14.el8.aarch64.rpm \
          "

SRC_URI[lzo.sha256sum] = "6809839757bd05082ca1b8d23eac617898eda3ce34844a0d31b0a030c8cc6653"
SRC_URI[lzo-devel.sha256sum] = "71b53381cd11048c6f99797b04ab02b1e1a0ed6b43806827db082364c4c8a4dc"
SRC_URI[lzo-minilzo.sha256sum] = "9c8fed3207ca57508d5a551e92767e326231f7a30adecd89239e2cadef4e3680"
