SUMMARY = "generated recipe based on libucil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib fontconfig freetype glib-2.0 libogg libpng libtheora libunicap libvorbis pango pkgconfig-native zlib"
RPM_SONAME_PROV_libucil = "libucil.so.2"
RPM_SONAME_REQ_libucil = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libogg.so.0 libpango-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 librt.so.1 libtheora.so.0 libunicap.so.2 libvorbis.so.0 libvorbisenc.so.2 libz.so.1"
RDEPENDS_libucil = "alsa-lib fontconfig freetype glib2 glibc libogg libpng libtheora libunicap libvorbis pango zlib"
RPM_SONAME_REQ_libucil-devel = "libucil.so.2"
RPROVIDES_libucil-devel = "libucil-dev (= 0.9.10)"
RDEPENDS_libucil-devel = "glib2-devel libucil libunicap-devel pango-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libucil-0.9.10-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libucil-devel-0.9.10-16.el8.aarch64.rpm \
          "

SRC_URI[libucil.sha256sum] = "83ac50f4501b5889e13761cdc4ced3645a6b1675c7982f248cec2574b9305986"
SRC_URI[libucil-devel.sha256sum] = "4d0d3761599b5de217cef743fba58943544ba4b64a9659663c87e00b95de8d6d"
