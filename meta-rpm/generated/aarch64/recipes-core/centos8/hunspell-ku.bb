SUMMARY = "generated recipe based on hunspell-ku srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ku = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ku-0.21-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-ku.sha256sum] = "b2507c0335eb46735652c16036654f77992bfe39c059dd99807c8962dbad7ccc"
