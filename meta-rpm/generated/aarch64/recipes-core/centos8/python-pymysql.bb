SUMMARY = "generated recipe based on python-PyMySQL srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-PyMySQL = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-PyMySQL-0.8.0-10.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-PyMySQL.sha256sum] = "777298d19fad27c8efac184c608eb58838db1fc6a8595120b527a07ab84ed7f9"
