SUMMARY = "generated recipe based on libxkbcommon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_libxkbcommon = "libxkbcommon.so.0"
RPM_SONAME_REQ_libxkbcommon = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libxkbcommon = "glibc xkeyboard-config"
RPM_SONAME_REQ_libxkbcommon-devel = "libxkbcommon.so.0"
RPROVIDES_libxkbcommon-devel = "libxkbcommon-dev (= 0.9.1)"
RDEPENDS_libxkbcommon-devel = "libxkbcommon pkgconf-pkg-config"
RPM_SONAME_PROV_libxkbcommon-x11 = "libxkbcommon-x11.so.0"
RPM_SONAME_REQ_libxkbcommon-x11 = "ld-linux-aarch64.so.1 libc.so.6 libxcb-xkb.so.1 libxcb.so.1 libxkbcommon.so.0"
RDEPENDS_libxkbcommon-x11 = "glibc libxcb libxkbcommon"
RPM_SONAME_REQ_libxkbcommon-x11-devel = "libxkbcommon-x11.so.0"
RPROVIDES_libxkbcommon-x11-devel = "libxkbcommon-x11-dev (= 0.9.1)"
RDEPENDS_libxkbcommon-x11-devel = "libxcb-devel libxkbcommon-devel libxkbcommon-x11 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxkbcommon-0.9.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxkbcommon-devel-0.9.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxkbcommon-x11-0.9.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libxkbcommon-x11-devel-0.9.1-1.el8.aarch64.rpm \
          "

SRC_URI[libxkbcommon.sha256sum] = "3aca03c788af2ecf8ef39421f246769d7ef7f37260ee9421fc68c1d1cc913600"
SRC_URI[libxkbcommon-devel.sha256sum] = "e5199f5bdd7be04201f1ed5b3a494f5641c4aba0ae3f9482bea426b1fcf21081"
SRC_URI[libxkbcommon-x11.sha256sum] = "e1b191943bd158a2e4ad0988fc66733a34d6fac6d9033a9a370a3f13027bc2ea"
SRC_URI[libxkbcommon-x11-devel.sha256sum] = "390ffb4d648ca546736d386f04c88c8120eefc5d4d7046a18a943fac111d0523"
