SUMMARY = "generated recipe based on json-c srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_json-c = "libjson-c.so.4"
RPM_SONAME_REQ_json-c = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_json-c = "glibc"
RPM_SONAME_REQ_json-c-devel = "libjson-c.so.4"
RPROVIDES_json-c-devel = "json-c-dev (= 0.13.1)"
RDEPENDS_json-c-devel = "json-c pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/json-c-0.13.1-0.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/json-c-devel-0.13.1-0.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/json-c-doc-0.13.1-0.2.el8.noarch.rpm \
          "

SRC_URI[json-c.sha256sum] = "e5f07ebcc0dabd9cc5a460c3fd5db8bbb96276a48132ea52ce2b1ddb7ead6320"
SRC_URI[json-c-devel.sha256sum] = "c695621f99f2b0dd22dcee0d7fd37d3d2d649d0ec8ed5c4ea7d1dab481b8e615"
SRC_URI[json-c-doc.sha256sum] = "11bcee8c836e2108fca403808472fb66a8e3fb37eacff061abab366d4ddd9c65"
