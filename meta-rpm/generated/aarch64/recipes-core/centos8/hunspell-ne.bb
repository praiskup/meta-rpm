SUMMARY = "generated recipe based on hunspell-ne srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ne = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ne-20080425-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-ne.sha256sum] = "3d4c758c72ea711a2f2a6186bbeb76b806a73ec2e656d90a19426ad49ad22853"
