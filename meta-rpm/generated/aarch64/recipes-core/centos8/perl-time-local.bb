SUMMARY = "generated recipe based on perl-Time-Local srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Time-Local = "perl-Carp perl-Exporter perl-constant perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Time-Local-1.280-1.el8.noarch.rpm \
          "

SRC_URI[perl-Time-Local.sha256sum] = "1edcf2b441ddf21417ef2b33e1ab2a30900758819335d7fabafe3b16bb3eab62"
