SUMMARY = "generated recipe based on freeglut srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libice libx11 libxext libxi libxxf86vm mesa mesa-libglu pkgconfig-native"
RPM_SONAME_PROV_freeglut = "libglut.so.3"
RPM_SONAME_REQ_freeglut = "ld-linux-aarch64.so.1 libGL.so.1 libICE.so.6 libX11.so.6 libXext.so.6 libXi.so.6 libXxf86vm.so.1 libc.so.6 libm.so.6"
RDEPENDS_freeglut = "glibc libICE libX11 libXext libXi libXxf86vm libglvnd-glx"
RPM_SONAME_REQ_freeglut-devel = "libglut.so.3"
RPROVIDES_freeglut-devel = "freeglut-dev (= 3.0.0)"
RDEPENDS_freeglut-devel = "freeglut mesa-libGL-devel mesa-libGLU-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/freeglut-3.0.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/freeglut-devel-3.0.0-8.el8.aarch64.rpm \
          "

SRC_URI[freeglut.sha256sum] = "079312e905e7eba0796cd35f5b6391c6a50ce4c7cdcde4415f7a902244c93da8"
SRC_URI[freeglut-devel.sha256sum] = "065d8b704263de6f88cd644636ee50dedcb58042dde3fe1ccd074c6087046c42"
