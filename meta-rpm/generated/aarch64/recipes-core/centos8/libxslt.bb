SUMMARY = "generated recipe based on libxslt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcrypt libgpg-error libxml2 pkgconfig-native"
RPM_SONAME_PROV_libxslt = "libexslt.so.0 libxslt.so.1"
RPM_SONAME_REQ_libxslt = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libxml2.so.2 libxslt.so.1"
RDEPENDS_libxslt = "glibc libgcrypt libgpg-error libxml2"
RPM_SONAME_REQ_libxslt-devel = "libexslt.so.0 libxslt.so.1"
RPROVIDES_libxslt-devel = "libxslt-dev (= 1.1.32)"
RDEPENDS_libxslt-devel = "bash libgcrypt-devel libgpg-error-devel libxml2-devel libxslt pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxslt-devel-1.1.32-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libxslt-1.1.32-4.el8.aarch64.rpm \
          "

SRC_URI[libxslt.sha256sum] = "ad81891bd370ff4a978a62f37a2f6f8da0ba8ac392a48dff3fd158cf17991b88"
SRC_URI[libxslt-devel.sha256sum] = "765b1374b11e1ba8638b3a3e5389b699e9a5c58670a1ccbe026a159a14251550"
