SUMMARY = "generated recipe based on libproxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libmodman pkgconfig-native"
RPM_SONAME_PROV_libproxy = "libproxy.so.1"
RPM_SONAME_REQ_libproxy = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmodman.so.1 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libproxy = "glibc libgcc libmodman libstdc++"
RPM_SONAME_REQ_libproxy-bin = "ld-linux-aarch64.so.1 libc.so.6 libproxy.so.1"
RDEPENDS_libproxy-bin = "glibc libproxy"
RPM_SONAME_REQ_libproxy-devel = "libproxy.so.1"
RPROVIDES_libproxy-devel = "libproxy-dev (= 0.4.15)"
RDEPENDS_libproxy-devel = "libproxy pkgconf-pkg-config"
RDEPENDS_python3-libproxy = "libproxy platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libproxy-bin-0.4.15-5.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libproxy-0.4.15-5.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libproxy-0.4.15-5.2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libproxy-devel-0.4.15-5.2.el8.aarch64.rpm \
          "

SRC_URI[libproxy.sha256sum] = "47df802c2647563bac65f260b56e9385c53062dbdc3f5c1f23a9144ca5902d61"
SRC_URI[libproxy-bin.sha256sum] = "0fcd8031fcee99b6eba8e47f818a6de15bf7191d5c4c1aa35c8876bf4b600a40"
SRC_URI[libproxy-devel.sha256sum] = "ec9ae0159f88fdabb808e899e98a20690418af056ab6e9cae2658e76860a8409"
SRC_URI[python3-libproxy.sha256sum] = "bdb4f750253d06adbf3a44c571068d8248b2a850e0e453712764cbf0e394d236"
