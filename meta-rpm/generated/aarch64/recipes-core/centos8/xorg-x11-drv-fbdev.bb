SUMMARY = "generated recipe based on xorg-x11-drv-fbdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-fbdev = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_xorg-x11-drv-fbdev = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-fbdev-0.5.0-2.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-fbdev.sha256sum] = "2d96af094d649cb30c3d5f8177991120cf05ece0635511c32b8a2091282ddb4c"
