SUMMARY = "generated recipe based on python-pydbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pydbus = "platform-python python3-gobject-base"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pydbus-0.6.0-5.el8.noarch.rpm \
          "

SRC_URI[python3-pydbus.sha256sum] = "bfa39369bd3c36833126b6f46b747de0736a66835d97196195c0810161c24549"
