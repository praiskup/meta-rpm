SUMMARY = "generated recipe based on python-sphinx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sphinx = "chkconfig environment-modules platform-python python-sphinx-locale python3-babel python3-docutils python3-imagesize python3-jinja2 python3-mock python3-packaging python3-pygments python3-requests python3-six python3-snowballstemmer python3-sphinx-theme-alabaster python3-sphinx_rtd_theme python3-sphinxcontrib-websupport"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python-sphinx-locale-1.7.6-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-sphinx-1.7.6-1.el8.noarch.rpm \
          "

SRC_URI[python-sphinx-locale.sha256sum] = "04cd8f266eabfb862c763b8af21db0f27bb7adc6bf63ece5225f0a49f9d1e4dc"
SRC_URI[python3-sphinx.sha256sum] = "5de8480640e3710eb789ff5acd951f0e502f2e82ef735ddb6829230ad668d023"
