SUMMARY = "generated recipe based on iprutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_iprutils = "ld-linux-aarch64.so.1 libc.so.6 libform.so.6 libm.so.6 libmenu.so.6 libncurses.so.6 libpanel.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_iprutils = "bash glibc ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iprutils-2.4.18.1-1.el8.aarch64.rpm \
          "

SRC_URI[iprutils.sha256sum] = "5ece147dd3aad0fdd88d0caedb3e93144cfac40fb548f66cbaba03e6a3ceebaa"
