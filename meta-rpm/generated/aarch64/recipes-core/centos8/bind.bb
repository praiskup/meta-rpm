SUMMARY = "generated recipe based on bind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db e2fsprogs krb5-libs libcap libidn2 libmaxminddb libpq libxml2 mariadb-connector-c openldap openssl pkgconfig-native sqlite3 xz zlib"
RPM_SONAME_REQ_bind = "ld-linux-aarch64.so.1 libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libisccc.so.161 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblwres.so.161 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind = "bash bind-libs bind-libs-lite coreutils glibc glibc-common grep krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs shadow-utils systemd xz-libs zlib"
RDEPENDS_bind-chroot = "bash bind grep"
RPM_SONAME_REQ_bind-devel = "libbind9.so.161 libisccc.so.161 liblwres.so.161"
RPROVIDES_bind-devel = "bind-dev (= 9.11.13)"
RDEPENDS_bind-devel = "bash bind-libs bind-lite-devel"
RPM_SONAME_REQ_bind-export-devel = "libdns-export.so.1107 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163"
RPROVIDES_bind-export-devel = "bind-export-dev (= 9.11.13)"
RDEPENDS_bind-export-devel = "bash bind-export-libs libcap-devel openssl-devel"
RPM_SONAME_PROV_bind-export-libs = "libdns-export.so.1107 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163"
RPM_SONAME_REQ_bind-export-libs = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdns-export.so.1107 libgssapi_krb5.so.2 libisc-export.so.1104 libk5crypto.so.3 libkrb5.so.3"
RDEPENDS_bind-export-libs = "glibc krb5-libs libcap libcom_err openssl-libs"
RPM_SONAME_PROV_bind-libs = "libbind9.so.161 libisccc.so.161 liblwres.so.161"
RPM_SONAME_REQ_bind-libs = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-libs = "bind-libs-lite bind-license glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_PROV_bind-libs-lite = "libdns.so.1107 libirs.so.161 libisc.so.1104 libisccfg.so.163"
RPM_SONAME_REQ_bind-libs-lite = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-libs-lite = "bind-license glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_REQ_bind-lite-devel = "libdns.so.1107 libirs.so.161 libisc.so.1104 libisccfg.so.163"
RPROVIDES_bind-lite-devel = "bind-lite-dev (= 9.11.13)"
RDEPENDS_bind-lite-devel = "bind-libs-lite libmaxminddb-devel libxml2-devel openssl-devel"
RPM_SONAME_REQ_bind-pkcs11 = "ld-linux-aarch64.so.1 libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns-pkcs11.so.1107 libdns.so.1107 libgssapi_krb5.so.2 libisc-pkcs11.so.1104 libisc.so.1104 libisccc.so.161 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblwres.so.161 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-pkcs11 = "bash bind bind-libs bind-libs-lite bind-pkcs11-libs glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs systemd xz-libs zlib"
RPM_SONAME_REQ_bind-pkcs11-devel = "libdns-pkcs11.so.1107 libisc-pkcs11.so.1104"
RPROVIDES_bind-pkcs11-devel = "bind-pkcs11-dev (= 9.11.13)"
RDEPENDS_bind-pkcs11-devel = "bind-lite-devel bind-pkcs11-libs"
RPM_SONAME_PROV_bind-pkcs11-libs = "libdns-pkcs11.so.1107 libisc-pkcs11.so.1104"
RPM_SONAME_REQ_bind-pkcs11-libs = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libisc-pkcs11.so.1104 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-pkcs11-libs = "bind-libs bind-license glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_REQ_bind-pkcs11-utils = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns-pkcs11.so.1107 libgssapi_krb5.so.2 libisc-pkcs11.so.1104 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-pkcs11-utils = "bind-pkcs11-libs glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_REQ_bind-sdb = "ld-linux-aarch64.so.1 libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libisccc.so.161 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 liblwres.so.161 liblzma.so.5 libm.so.6 libmariadb.so.3 libmaxminddb.so.0 libpq.so.5 libpthread.so.0 libsqlite3.so.0 libssl.so.1.1 libxml2.so.2 libz.so.1"
RDEPENDS_bind-sdb = "bash bind bind-libs bind-libs-lite glibc krb5-libs libcap libcom_err libdb libmaxminddb libpq libxml2 mariadb-connector-c openldap openssl-libs sqlite-libs systemd xz-libs zlib"
RDEPENDS_bind-sdb-chroot = "bash bind-sdb grep"
RPM_SONAME_REQ_bind-utils = "ld-linux-aarch64.so.1 libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libidn2.so.0 libirs.so.161 libisc.so.1104 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblwres.so.161 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-utils = "bind-libs bind-libs-lite glibc krb5-libs libcap libcom_err libidn2 libmaxminddb libxml2 openssl-libs platform-python python3-bind xz-libs zlib"
RDEPENDS_python3-bind = "bind-license platform-python python3-ply"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-chroot-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-devel-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-libs-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-libs-lite-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-license-9.11.13-6.el8_2.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-lite-devel-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-pkcs11-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-pkcs11-devel-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-pkcs11-libs-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-pkcs11-utils-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-sdb-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-sdb-chroot-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bind-utils-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-bind-9.11.13-6.el8_2.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bind-export-devel-9.11.13-6.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bind-export-libs-9.11.13-6.el8_2.1.aarch64.rpm \
          "

SRC_URI[bind.sha256sum] = "08ecd9af930eb761f52d211aeb55c32db486f9938823239b75ae5b6c56e640c8"
SRC_URI[bind-chroot.sha256sum] = "afbf97ae75b90003a87f132965a47ef72e6a81c9b1f4a32a870dbef44005a08d"
SRC_URI[bind-devel.sha256sum] = "cb3379515ece6107f824bf702b00aad31331a0c3f93737a51417f06c47037095"
SRC_URI[bind-export-devel.sha256sum] = "3746de38174be8a8fad7f77b974dd1238f960c1467f1cdf156c0f2f1a1c5e24d"
SRC_URI[bind-export-libs.sha256sum] = "8e11aa0c54907a786d64f122fbc9886e16ee96b1c65b5cf795476b9e6dd6d225"
SRC_URI[bind-libs.sha256sum] = "c0e6bb4de2e65fedc461c9d7aa39cc29d92469c6eb4c49e06d211f72e5e4219d"
SRC_URI[bind-libs-lite.sha256sum] = "8b025a58d97b49d530877af0d86b6beb02957837c35e1db9c040bfb473ecb78d"
SRC_URI[bind-license.sha256sum] = "c8e1df6b56b388b06f5c05e01a5dae27086253295918e16d9d64932b9fbe28dc"
SRC_URI[bind-lite-devel.sha256sum] = "0ff6edea8eb2594b9acdec85dc50cddf1a37528005c15248929c34739ca2b332"
SRC_URI[bind-pkcs11.sha256sum] = "dca4749e731ebeba29361d5cc331d830bc66cadc56f14a16031e41fe004f1ae1"
SRC_URI[bind-pkcs11-devel.sha256sum] = "7bd48513c18907fccd6527b704c97cf1bcd63d3ca4dfcab8bccfc76a7250ecbe"
SRC_URI[bind-pkcs11-libs.sha256sum] = "f441794fd854e971946b47a0b74ce3210938e4b871ba24cb98dc7eac403e808c"
SRC_URI[bind-pkcs11-utils.sha256sum] = "9c4ecbddb419b12c58b23e2548182baa8964f82c80b13c8671727ca714036858"
SRC_URI[bind-sdb.sha256sum] = "cf0f0f9dc8c850be78f2fa5f7e29f64fc05bd43753b70e7280a8ab2696b0a345"
SRC_URI[bind-sdb-chroot.sha256sum] = "a4793d169980a10f3e3a5b4ed340ab0fb4c4b103104e8a42e080d55226be8212"
SRC_URI[bind-utils.sha256sum] = "010c87ac3018f2b1983c354b1f042a55d30d9bdcbed17d00d39d54fd04e00aa1"
SRC_URI[python3-bind.sha256sum] = "c157d3483665cc48c6650cc6f16b849a4271ab34acd68768b9dd680f152a7688"
