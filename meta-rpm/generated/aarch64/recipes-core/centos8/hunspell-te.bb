SUMMARY = "generated recipe based on hunspell-te srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-te = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-te-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-te.sha256sum] = "4876181a7778822e5caaae57c0f03d7fa108c69e571e2a676a3138f741b56521"
