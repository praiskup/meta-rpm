SUMMARY = "generated recipe based on xfsprogs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs libblkid libuuid pkgconfig-native readline"
RPM_SONAME_PROV_xfsprogs = "libhandle.so.1"
RPM_SONAME_REQ_xfsprogs = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libdevmapper.so.1.02 libpthread.so.0 libreadline.so.7 librt.so.1 libuuid.so.1"
RDEPENDS_xfsprogs = "bash device-mapper-libs glibc libblkid libuuid readline"
RPM_SONAME_REQ_xfsprogs-devel = "libhandle.so.1"
RPROVIDES_xfsprogs-devel = "xfsprogs-dev (= 5.0.0)"
RDEPENDS_xfsprogs-devel = "libuuid-devel xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xfsprogs-5.0.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xfsprogs-devel-5.0.0-2.el8.aarch64.rpm \
          "

SRC_URI[xfsprogs.sha256sum] = "560bec5e63d6601bc3c820554d34105928de6d5c2c4de8eb15df7e06a912353b"
SRC_URI[xfsprogs-devel.sha256sum] = "f02d845045865d86f9fc8e847ad30bfe6112eceeda6515da55ebe9ca479d0150"
