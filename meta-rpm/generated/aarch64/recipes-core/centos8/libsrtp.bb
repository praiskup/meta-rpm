SUMMARY = "generated recipe based on libsrtp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native zlib"
RPM_SONAME_PROV_libsrtp = "libsrtp.so.1"
RPM_SONAME_REQ_libsrtp = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libssl.so.1.1 libz.so.1"
RDEPENDS_libsrtp = "glibc openssl-libs zlib"
RPM_SONAME_REQ_libsrtp-devel = "libsrtp.so.1"
RPROVIDES_libsrtp-devel = "libsrtp-dev (= 1.5.4)"
RDEPENDS_libsrtp-devel = "libsrtp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsrtp-1.5.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsrtp-devel-1.5.4-8.el8.aarch64.rpm \
          "

SRC_URI[libsrtp.sha256sum] = "eceaef678ae88734d523eff46cc60bb4b73dd67f1bed8b0975eebf451204bdfb"
SRC_URI[libsrtp-devel.sha256sum] = "16efe1648becc285036118e73844fb31a53120847b741a7c8750d18585d38e7b"
