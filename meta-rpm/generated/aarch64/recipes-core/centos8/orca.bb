SUMMARY = "generated recipe based on orca srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_orca = "libwnck3 platform-python python3-brlapi python3-louis python3-pyatspi python3-speechd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/orca-3.28.2-1.el8.noarch.rpm \
          "

SRC_URI[orca.sha256sum] = "ac62a9403ff0118760104033a634878a81524902d871879731aa7f870fb6a6bf"
