SUMMARY = "generated recipe based on rasdaemon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sqlite3"
RPM_SONAME_REQ_rasdaemon = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libsqlite3.so.0"
RDEPENDS_rasdaemon = "glibc hwdata perl-DBD-SQLite perl-Getopt-Long perl-constant perl-interpreter perl-libs sqlite-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rasdaemon-0.6.1-3.el8.aarch64.rpm \
          "

SRC_URI[rasdaemon.sha256sum] = "dfa4620bd8a26b8845e501b5323731b27bd089c9ea7a7b701f7954b85e447cbb"
