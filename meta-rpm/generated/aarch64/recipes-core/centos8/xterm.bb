SUMMARY = "generated recipe based on xterm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libice libutempter libx11 libxaw libxft libxmu libxpm libxt ncurses pkgconfig-native"
RPM_SONAME_REQ_xterm = "ld-linux-aarch64.so.1 libICE.so.6 libX11.so.6 libXaw.so.7 libXft.so.2 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libtinfo.so.6 libutempter.so.0"
RDEPENDS_xterm = "bash fontconfig freetype glibc libICE libX11 libXaw libXft libXmu libXpm libXt libutempter ncurses-libs xterm-resize"
RPM_SONAME_REQ_xterm-resize = "ld-linux-aarch64.so.1 libc.so.6 libtinfo.so.6"
RDEPENDS_xterm-resize = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xterm-331-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xterm-resize-331-1.el8.aarch64.rpm \
          "

SRC_URI[xterm.sha256sum] = "728f7d56e3cf4aa81749484d976f9528f04f531a834154d5c4b40eaa6339eaa9"
SRC_URI[xterm-resize.sha256sum] = "bd4e5b5e1eb3ecdd74fc13ea6eac7ec617874021077818fb4406f14e0237379c"
