SUMMARY = "generated recipe based on python-rpmfluff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-rpmfluff = "createrepo_c platform-python rpm-build"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-rpmfluff-0.5.7.1-2.el8.noarch.rpm \
          "

SRC_URI[python3-rpmfluff.sha256sum] = "962a8a0d98be623926bd55613782d3be7696f9a7e9c2689d40875037731ed86c"
