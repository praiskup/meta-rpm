SUMMARY = "generated recipe based on perl-Unicode-EastAsianWidth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Unicode-EastAsianWidth = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Unicode-EastAsianWidth-1.33-13.el8.noarch.rpm \
          "

SRC_URI[perl-Unicode-EastAsianWidth.sha256sum] = "ccd00c3f5a58084f75e461741350777158ca7e0cc6bd7f387cfe538fc9c7f5ea"
