SUMMARY = "generated recipe based on perl-Devel-LexAlias srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-LexAlias = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-LexAlias = "glibc perl-Devel-Caller perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-LexAlias-0.05-16.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-LexAlias.sha256sum] = "ab3ed847182b3f7d0321bba5692250bd699d731fbde5c09524ebc5168d277d6f"
