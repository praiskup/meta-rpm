SUMMARY = "generated recipe based on libpcap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpcap = "libpcap.so.1"
RPM_SONAME_REQ_libpcap = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libpcap = "glibc"
RPM_SONAME_REQ_libpcap-devel = "libpcap.so.1"
RPROVIDES_libpcap-devel = "libpcap-dev (= 1.9.0)"
RDEPENDS_libpcap-devel = "bash libpcap pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpcap-1.9.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpcap-devel-1.9.0-3.el8.aarch64.rpm \
          "

SRC_URI[libpcap.sha256sum] = "1c945626c4f926e0bb991c26aef4da87aa9a34d9bb4e958ec6090457d4813156"
SRC_URI[libpcap-devel.sha256sum] = "1af7d0eaa44f777626cdfed594f9a10b1f40d23ab0029680391bff0f0ba810be"
