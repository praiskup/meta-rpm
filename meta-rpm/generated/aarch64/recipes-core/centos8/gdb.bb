SUMMARY = "generated recipe based on gdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "babeltrace expat guile libgcc libselinux mpfr ncurses pkgconfig-native platform-python3 readline xz zlib"
RDEPENDS_gdb = "bash gdb-headless"
RPM_SONAME_PROV_gdb-gdbserver = "libinproctrace.so"
RPM_SONAME_REQ_gdb-gdbserver = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libselinux.so.1 libstdc++.so.6"
RDEPENDS_gdb-gdbserver = "glibc libgcc libselinux libstdc++"
RPM_SONAME_REQ_gdb-headless = "ld-linux-aarch64.so.1 libbabeltrace-ctf.so.1 libbabeltrace.so.1 libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libguile-2.0.so.22 liblzma.so.5 libm.so.6 libmpfr.so.4 libncursesw.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreadline.so.7 libselinux.so.1 libstdc++.so.6 libtinfo.so.6 libutil.so.1 libz.so.1"
RDEPENDS_gdb-headless = "bash expat glibc guile libbabeltrace libgcc libselinux libstdc++ mpfr ncurses-libs python3-libs readline xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdb-8.2-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdb-doc-8.2-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdb-gdbserver-8.2-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdb-headless-8.2-11.el8.aarch64.rpm \
          "

SRC_URI[gdb.sha256sum] = "c889d6202e43215690e50aa481c99f5926e18e3fa81665553e9101eccc432cd2"
SRC_URI[gdb-doc.sha256sum] = "3e2f532cf9dcac9f35559bf821545ce1ef378a53be9a76042e28dd63511ae397"
SRC_URI[gdb-gdbserver.sha256sum] = "4214d2819dbe933404f4e28e38e17169a100b73e710d67a849b310d21b17fa05"
SRC_URI[gdb-headless.sha256sum] = "3593c6603ebcfe8cd4579ad99538b5baa1ad66ebd37bd8388bcac47c82c322e9"
