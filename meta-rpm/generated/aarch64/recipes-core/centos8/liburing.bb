SUMMARY = "generated recipe based on liburing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liburing = "liburing.so.1"
RPM_SONAME_REQ_liburing = "libc.so.6"
RDEPENDS_liburing = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liburing-0.2-2.el8.aarch64.rpm \
          "

SRC_URI[liburing.sha256sum] = "fdf07ca5601378a7f2136e4cec0352104388f0b29479152ef137d3c581c2a27f"
