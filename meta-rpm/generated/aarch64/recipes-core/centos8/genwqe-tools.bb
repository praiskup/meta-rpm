SUMMARY = "generated recipe based on genwqe-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_REQ_genwqe-tools = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1 libz.so.1 libzADC.so.4"
RDEPENDS_genwqe-tools = "bash genwqe-zlib glibc zlib"
RPM_SONAME_REQ_genwqe-vpd = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_genwqe-vpd = "glibc"
RPM_SONAME_PROV_genwqe-zlib = "libzADC.so.4"
RPM_SONAME_REQ_genwqe-zlib = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_genwqe-zlib = "glibc"
RPM_SONAME_REQ_genwqe-zlib-devel = "libzADC.so.4"
RPROVIDES_genwqe-zlib-devel = "genwqe-zlib-dev (= 4.0.20)"
RDEPENDS_genwqe-zlib-devel = "genwqe-zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/genwqe-tools-4.0.20-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/genwqe-vpd-4.0.20-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/genwqe-zlib-4.0.20-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/genwqe-zlib-devel-4.0.20-5.el8.aarch64.rpm \
          "

SRC_URI[genwqe-tools.sha256sum] = "2ef3869523bb0f762883323c7ec148e208445c0c6f509333d2a08eb5f1c6bc0d"
SRC_URI[genwqe-vpd.sha256sum] = "4435ddb4611de9bfd701c2d7e7e2ae3f78ede07f7c3035f49610e09f57bf4199"
SRC_URI[genwqe-zlib.sha256sum] = "56eb05e6e7c687770b777b4ee901cf93a127b8e602fddaa48a0e2b2da879f4a2"
SRC_URI[genwqe-zlib-devel.sha256sum] = "96ab4021e54dc9c4439019db76c9960ef064fcfba52befec21261ccee351d681"
