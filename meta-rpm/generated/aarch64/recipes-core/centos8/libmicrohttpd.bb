SUMMARY = "generated recipe based on libmicrohttpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls pkgconfig-native"
RPM_SONAME_PROV_libmicrohttpd = "libmicrohttpd.so.12"
RPM_SONAME_REQ_libmicrohttpd = "ld-linux-aarch64.so.1 libc.so.6 libgnutls.so.30 libpthread.so.0"
RDEPENDS_libmicrohttpd = "glibc gnutls info"
RPM_SONAME_REQ_libmicrohttpd-devel = "libmicrohttpd.so.12"
RPROVIDES_libmicrohttpd-devel = "libmicrohttpd-dev (= 0.9.59)"
RDEPENDS_libmicrohttpd-devel = "gnutls-devel libmicrohttpd pkgconf-pkg-config"
RDEPENDS_libmicrohttpd-doc = "bash libmicrohttpd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmicrohttpd-0.9.59-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmicrohttpd-devel-0.9.59-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmicrohttpd-doc-0.9.59-2.el8.noarch.rpm \
          "

SRC_URI[libmicrohttpd.sha256sum] = "345493ac7c19aade102c22d83ad270d67101f4bc37d8ab7e764d42cb11d7ee82"
SRC_URI[libmicrohttpd-devel.sha256sum] = "ecef1ba3e9c0076c6825c7d70c159e647f0290a9e2db8125570ff64717c07ca4"
SRC_URI[libmicrohttpd-doc.sha256sum] = "4d63c9b1646e7e6d6569c7fb5e801e78ac2e37fbe9c090ae40e9bd2d1e47c434"
