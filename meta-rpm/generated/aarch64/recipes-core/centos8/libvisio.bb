SUMMARY = "generated recipe based on libvisio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc librevenge libxml2 pkgconfig-native"
RPM_SONAME_PROV_libvisio = "libvisio-0.1.so.1"
RPM_SONAME_REQ_libvisio = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicuuc.so.60 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libxml2.so.2"
RDEPENDS_libvisio = "glibc libgcc libicu librevenge libstdc++ libxml2"
RPM_SONAME_REQ_libvisio-devel = "libvisio-0.1.so.1"
RPROVIDES_libvisio-devel = "libvisio-dev (= 0.1.6)"
RDEPENDS_libvisio-devel = "libicu-devel librevenge-devel libvisio libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvisio-0.1.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvisio-devel-0.1.6-2.el8.aarch64.rpm \
          "

SRC_URI[libvisio.sha256sum] = "fdb6b88b4929197d6478b389f09e4ca030cc5497642edbf05a5dfe2567094741"
SRC_URI[libvisio-devel.sha256sum] = "157e2208e38f33df52ca3a36018111b6f6f7b10f6efa9c32ed504af023665048"
