SUMMARY = "generated recipe based on mlocate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mlocate = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mlocate = "bash glibc grep sed shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mlocate-0.26-20.el8.aarch64.rpm \
          "

SRC_URI[mlocate.sha256sum] = "a808c5c2faa66331100a87220808a97e6b8e7fc7d13a08dfe8f51d9ad5a33b47"
