SUMMARY = "generated recipe based on perl-Module-Metadata srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Metadata = "perl-Carp perl-PathTools perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Module-Metadata-1.000033-395.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Metadata.sha256sum] = "0f54bcb83cf9a1a6266b5e6fdf2da88476b988d4aa653c389e06e6a676afacd5"
