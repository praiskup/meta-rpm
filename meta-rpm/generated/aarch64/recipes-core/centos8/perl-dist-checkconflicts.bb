SUMMARY = "generated recipe based on perl-Dist-CheckConflicts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Dist-CheckConflicts = "perl-Carp perl-Exporter perl-Module-Runtime perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Dist-CheckConflicts-0.11-11.el8.noarch.rpm \
          "

SRC_URI[perl-Dist-CheckConflicts.sha256sum] = "7b131dc0a90a373574ec2da75986169834cc4b9e1a488056bf5274b3e8e7740a"
