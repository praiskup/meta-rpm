SUMMARY = "generated recipe based on python-reportlab srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-reportlab = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-reportlab = "dejavu-sans-fonts glibc platform-python python3-libs python3-pillow"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-reportlab-3.4.0-8.el8.aarch64.rpm \
          "

SRC_URI[python3-reportlab.sha256sum] = "ac2ff3d8d5edc77ddad52a3cba371ef3888a9eb53dcfa0df52b60ec09b8588b8"
