SUMMARY = "generated recipe based on tbb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-tbb = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libirml.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6 libtbb.so.2"
RDEPENDS_python3-tbb = "glibc libgcc libstdc++ platform-python python3-libs tbb"
RPM_SONAME_PROV_tbb = "libirml.so.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RPM_SONAME_REQ_tbb = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtbbmalloc.so.2"
RDEPENDS_tbb = "glibc libgcc libstdc++"
RPM_SONAME_REQ_tbb-devel = "libirml.so.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RPROVIDES_tbb-devel = "tbb-dev (= 2018.2)"
RDEPENDS_tbb-devel = "pkgconf-pkg-config tbb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-tbb-2018.2-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tbb-2018.2-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tbb-devel-2018.2-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tbb-doc-2018.2-9.el8.aarch64.rpm \
          "

SRC_URI[python3-tbb.sha256sum] = "4e128c1b684d57f170d495bffca1759fd87906893e642d54562841ead00a6781"
SRC_URI[tbb.sha256sum] = "687d01b304750fb4d5a67a40b187e39128e0a565547ec554a3b29e5acb27fc15"
SRC_URI[tbb-devel.sha256sum] = "50b19fe764fb46e04abc9fcaf4ac7b205398a9dedad0bcecceae663045615d03"
SRC_URI[tbb-doc.sha256sum] = "29ad00370888e82b1f103d8e937659f38d7c206bd9f95612ec6b1312503ebf23"
