SUMMARY = "generated recipe based on libappstream-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 libarchive libgcc libsoup-2.4 libstemmer libuuid pkgconfig-native"
RPM_SONAME_PROV_libappstream-glib = "libappstream-glib.so.8"
RPM_SONAME_REQ_libappstream-glib = "ld-linux-aarch64.so.1 libappstream-glib.so.8 libarchive.so.13 libc.so.6 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libsoup-2.4.so.1 libstemmer.so.0 libuuid.so.1"
RDEPENDS_libappstream-glib = "gdk-pixbuf2 glib2 glibc json-glib libarchive libgcc libsoup libstemmer libuuid"
RPM_SONAME_REQ_libappstream-glib-devel = "libappstream-glib.so.8"
RPROVIDES_libappstream-glib-devel = "libappstream-glib-dev (= 0.7.14)"
RDEPENDS_libappstream-glib-devel = "gdk-pixbuf2-devel glib2-devel libappstream-glib libarchive-devel libuuid-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libappstream-glib-0.7.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libappstream-glib-devel-0.7.14-3.el8.aarch64.rpm \
          "

SRC_URI[libappstream-glib.sha256sum] = "791e9574c613eed49b92680ff89cf786f7f3d7bac52bc5427b72b2ea5058dfab"
SRC_URI[libappstream-glib-devel.sha256sum] = "7624d47c406dc60dd8fa32076d9fceb5177a78ab605aff5d165e0c0ee554c422"
