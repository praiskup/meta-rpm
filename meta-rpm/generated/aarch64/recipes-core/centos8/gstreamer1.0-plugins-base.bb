SUMMARY = "generated recipe based on gstreamer1-plugins-base srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib cairo glib-2.0 gstreamer1.0 libglvnd libogg libpng libtheora libvisual libvorbis libx11 libxcb libxext libxv opus orc pango pkgconfig-native wayland zlib"
RPM_SONAME_PROV_gstreamer1-plugins-base = "libgstadder.so libgstallocators-1.0.so.0 libgstalsa.so libgstapp-1.0.so.0 libgstapp.so libgstaudio-1.0.so.0 libgstaudioconvert.so libgstaudiomixer.so libgstaudiorate.so libgstaudioresample.so libgstaudiotestsrc.so libgstcompositor.so libgstencoding.so libgstfft-1.0.so.0 libgstgio.so libgstgl-1.0.so.0 libgstlibvisual.so libgstogg.so libgstopengl.so libgstopus.so libgstoverlaycomposition.so libgstpango.so libgstpbtypes.so libgstpbutils-1.0.so.0 libgstplayback.so libgstrawparse.so libgstriff-1.0.so.0 libgstrtp-1.0.so.0 libgstrtsp-1.0.so.0 libgstsdp-1.0.so.0 libgstsubparse.so libgsttag-1.0.so.0 libgsttcp.so libgsttheora.so libgsttypefindfunctions.so libgstvideo-1.0.so.0 libgstvideoconvert.so libgstvideorate.so libgstvideoscale.so libgstvideotestsrc.so libgstvolume.so libgstvorbis.so libgstximagesink.so libgstxvimagesink.so"
RPM_SONAME_REQ_gstreamer1-plugins-base = "ld-linux-aarch64.so.1 libEGL.so.1 libGL.so.1 libGLESv2.so.2 libX11-xcb.so.1 libX11.so.6 libXext.so.6 libXv.so.1 libasound.so.2 libc.so.6 libcairo.so.2 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstallocators-1.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstcontroller-1.0.so.0 libgstgl-1.0.so.0 libgstnet-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgstriff-1.0.so.0 libgstrtp-1.0.so.0 libgsttag-1.0.so.0 libgstvideo-1.0.so.0 libm.so.6 libogg.so.0 libopus.so.0 liborc-0.4.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 libtheoradec.so.1 libtheoraenc.so.1 libvisual-0.4.so.0 libvorbis.so.0 libvorbisenc.so.2 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libxcb.so.1 libz.so.1"
RDEPENDS_gstreamer1-plugins-base = "alsa-lib cairo glib2 glibc gstreamer1 iso-codes libX11 libX11-xcb libXext libXv libglvnd-egl libglvnd-gles libglvnd-glx libogg libpng libtheora libvisual libvorbis libwayland-client libwayland-cursor libwayland-egl libxcb opus orc pango zlib"
RPM_SONAME_REQ_gstreamer1-plugins-base-devel = "libgstallocators-1.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstfft-1.0.so.0 libgstgl-1.0.so.0 libgstpbutils-1.0.so.0 libgstriff-1.0.so.0 libgstrtp-1.0.so.0 libgstrtsp-1.0.so.0 libgstsdp-1.0.so.0 libgsttag-1.0.so.0 libgstvideo-1.0.so.0"
RPROVIDES_gstreamer1-plugins-base-devel = "gstreamer1-plugins-base-dev (= 1.16.1)"
RDEPENDS_gstreamer1-plugins-base-devel = "glib2-devel gstreamer1-devel gstreamer1-plugins-base orc-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gstreamer1-plugins-base-1.16.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gstreamer1-plugins-base-devel-1.16.1-1.el8.aarch64.rpm \
          "

SRC_URI[gstreamer1-plugins-base.sha256sum] = "f815cbd4c4606160ae4bd08889db66092f6638c7ef0ff00fe407a82f5fc697c6"
SRC_URI[gstreamer1-plugins-base-devel.sha256sum] = "97c5dc27a36582dd671695d45e6da63d71342f8384a3d2d0eae70e1526412690"
