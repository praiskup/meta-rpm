SUMMARY = "generated recipe based on librepo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 gpgme libgcc libgpg-error libxml2 openssl pkgconfig-native platform-python3"
RPM_SONAME_PROV_librepo = "librepo.so.0"
RPM_SONAME_REQ_librepo = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libglib-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libxml2.so.2"
RDEPENDS_librepo = "glib2 glibc gpgme libcurl libgcc libgpg-error libxml2 openssl-libs"
RPM_SONAME_REQ_python3-librepo = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libglib-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libpython3.6m.so.1.0 librepo.so.0 libxml2.so.2"
RDEPENDS_python3-librepo = "glib2 glibc gpgme libcurl libgcc libgpg-error librepo libxml2 openssl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/librepo-1.11.0-3.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-librepo-1.11.0-3.el8_2.aarch64.rpm \
          "

SRC_URI[librepo.sha256sum] = "420c0643364617c06e00de1a8200346c79c631c2ec0898967a5b6a9435471efb"
SRC_URI[python3-librepo.sha256sum] = "e6b71a89250e8851b1008424e5df211492b2a2a3381b230a5ceaf33c7ad43676"
