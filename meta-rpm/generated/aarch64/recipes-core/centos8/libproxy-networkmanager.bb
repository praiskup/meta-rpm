SUMMARY = "generated recipe based on libproxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs glib-2.0 libgcc networkmanager pkgconfig-native"
RPM_SONAME_REQ_libproxy-networkmanager = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libnm.so.0 libproxy.so.1 libstdc++.so.6"
RDEPENDS_libproxy-networkmanager = "NetworkManager-libnm dbus-libs glib2 glibc libgcc libproxy libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libproxy-networkmanager-0.4.15-5.2.el8.aarch64.rpm \
          "

SRC_URI[libproxy-networkmanager.sha256sum] = "b2063a1b01a9906ddd276112a44e2c0fdd3142ccdfec9aee271f3c31cdef9d06"
