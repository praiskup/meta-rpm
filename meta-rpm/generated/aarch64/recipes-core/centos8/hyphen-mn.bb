SUMMARY = "generated recipe based on hyphen-mn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-mn = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-mn-0.20100531-13.el8.noarch.rpm \
          "

SRC_URI[hyphen-mn.sha256sum] = "ea0a6adcf6e1b285eb59fd48094179298b5a835281ba2f13f5c3366d25965a3b"
