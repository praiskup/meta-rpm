SUMMARY = "generated recipe based on libXpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxt pkgconfig-native"
RPM_SONAME_PROV_libXpm = "libXpm.so.4"
RPM_SONAME_REQ_libXpm = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libXpm = "glibc libX11"
RPM_SONAME_REQ_libXpm-devel = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXpm.so.4 libXt.so.6 libc.so.6"
RPROVIDES_libXpm-devel = "libXpm-dev (= 3.5.12)"
RDEPENDS_libXpm-devel = "glibc libX11 libX11-devel libXext libXpm libXt pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXpm-3.5.12-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXpm-devel-3.5.12-8.el8.aarch64.rpm \
          "

SRC_URI[libXpm.sha256sum] = "d302c33d87658228a1bedfd1b2b9fb3759e1aebcfac691b48a52464ac229712c"
SRC_URI[libXpm-devel.sha256sum] = "e5bf771d4c97cfe4782bc5da904909436c770c5fa99126413fb4c6425e290e86"
