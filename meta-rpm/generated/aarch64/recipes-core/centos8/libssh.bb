SUMMARY = "generated recipe based on libssh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs openssl pkgconfig-native zlib"
RPM_SONAME_PROV_libssh = "libssh.so.4 libssh_threads.so libssh_threads.so.4"
RPM_SONAME_REQ_libssh = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 librt.so.1 libz.so.1"
RDEPENDS_libssh = "crypto-policies glibc krb5-libs libcom_err libssh-config openssl-libs zlib"
RPM_SONAME_REQ_libssh-devel = "libssh.so.4"
RPROVIDES_libssh-devel = "libssh-dev (= 0.9.0)"
RDEPENDS_libssh-devel = "cmake-filesystem libssh pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libssh-devel-0.9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libssh-0.9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libssh-config-0.9.0-4.el8.noarch.rpm \
          "

SRC_URI[libssh.sha256sum] = "c5f58b46af969ce79376f444552cf83880564cd3e53128794dde62ce4effffdf"
SRC_URI[libssh-config.sha256sum] = "de84d1439aba91eb9a6de521a0beb3bc6cbf7686f56a622aa289c398b2b0a28f"
SRC_URI[libssh-devel.sha256sum] = "f1ff2074d9479eebf60c00c6f789c70c691b91f6cbe039a234c63b3fed89458f"
