SUMMARY = "generated recipe based on sonatype-oss-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sonatype-oss-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sonatype-oss-parent-7-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[sonatype-oss-parent.sha256sum] = "47db647744dc0051e8c35b8363bc8eb809dee3dbd1bbfd83dec7d753d49f3b16"
