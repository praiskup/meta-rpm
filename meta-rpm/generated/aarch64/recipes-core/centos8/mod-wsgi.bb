SUMMARY = "generated recipe based on mod_wsgi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-mod_wsgi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_python3-mod_wsgi = "glibc httpd platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-mod_wsgi-4.6.4-4.el8.aarch64.rpm \
          "

SRC_URI[python3-mod_wsgi.sha256sum] = "327f00c71ef031882afb2110b5225fe7271f5643e2b344f9f28baf06c128be5c"
