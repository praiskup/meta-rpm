SUMMARY = "generated recipe based on parted srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs libblkid libselinux libsepol libuuid ncurses pkgconfig-native readline"
RPM_SONAME_PROV_parted = "libparted-fs-resize.so.0 libparted.so.2"
RPM_SONAME_REQ_parted = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libdevmapper.so.1.02 libdl.so.2 libparted.so.2 libreadline.so.7 libselinux.so.1 libsepol.so.1 libtinfo.so.6 libuuid.so.1"
RDEPENDS_parted = "bash device-mapper-libs glibc info libblkid libselinux libsepol libuuid ncurses-libs readline"
RPM_SONAME_REQ_parted-devel = "libparted-fs-resize.so.0 libparted.so.2"
RPROVIDES_parted-devel = "parted-dev (= 3.2)"
RDEPENDS_parted-devel = "parted pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/parted-3.2-38.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/parted-devel-3.2-38.el8.aarch64.rpm \
          "

SRC_URI[parted.sha256sum] = "52e88fe9b9a841735a8ef826592fc11ef7006b3368ff224c4577aad86b5989ea"
SRC_URI[parted-devel.sha256sum] = "4a5e7bda5bf14a08df29f4fb840b9a7cec6641bf402d8e0813a1a2702229eedb"
