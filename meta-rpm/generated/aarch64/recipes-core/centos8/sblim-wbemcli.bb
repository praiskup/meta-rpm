SUMMARY = "generated recipe based on sblim-wbemcli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc pkgconfig-native"
RPM_SONAME_REQ_sblim-wbemcli = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_sblim-wbemcli = "curl glibc libcurl libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sblim-wbemcli-1.6.3-15.el8.aarch64.rpm \
          "

SRC_URI[sblim-wbemcli.sha256sum] = "656dcb6f44dc7c3d542a23c26d0cd30a8e794e5a0ec464dc672219b76e5f9b3c"
