SUMMARY = "generated recipe based on geoclue2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs glib-2.0 json-glib libgcc libnotify libsoup-2.4 modemmanager pkgconfig-native"
RPM_SONAME_REQ_geoclue2 = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libmm-glib.so.0 libnotify.so.4 libsoup-2.4.so.1"
RDEPENDS_geoclue2 = "ModemManager-glib avahi-glib avahi-libs bash dbus glib2 glibc json-glib libgcc libnotify libsoup shadow-utils systemd"
RPM_SONAME_REQ_geoclue2-demos = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgeoclue-2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_geoclue2-demos = "geoclue2 geoclue2-libs glib2 glibc libgcc"
RPM_SONAME_REQ_geoclue2-devel = "libgeoclue-2.so.0"
RPROVIDES_geoclue2-devel = "geoclue2-dev (= 2.5.5)"
RDEPENDS_geoclue2-devel = "geoclue2 geoclue2-libs glib2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_geoclue2-libs = "libgeoclue-2.so.0"
RPM_SONAME_REQ_geoclue2-libs = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_geoclue2-libs = "geoclue2 glib2 glibc libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/geoclue2-2.5.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/geoclue2-demos-2.5.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/geoclue2-libs-2.5.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/geoclue2-devel-2.5.5-1.el8.aarch64.rpm \
          "

SRC_URI[geoclue2.sha256sum] = "83d2051dd094d5e53a40e835db03177481402ddfe0ec1699ff34fc7051fba17c"
SRC_URI[geoclue2-demos.sha256sum] = "5c022db90fc0dc7f76f06d1fd118617f6a7395ebe9d0f90d34c7cad3e24cd26e"
SRC_URI[geoclue2-devel.sha256sum] = "a4c7acf44ffff32e6c495804f25d393228b64ea57eceaf12b0805b13035b55cb"
SRC_URI[geoclue2-libs.sha256sum] = "7654aa76278ac6661f5941a1090b2877a9118a61e0c15e01c66f474653db56a2"
