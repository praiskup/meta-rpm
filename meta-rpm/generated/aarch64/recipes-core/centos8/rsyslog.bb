SUMMARY = "generated recipe based on rsyslog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl gnutls krb5-libs libestr libfastjson libgcc libgcrypt libgpg-error liblognorm libpq librdkafka librelp libuuid mariadb-connector-c net-snmp openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_rsyslog = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libestr.so.0 libfastjson.so.4 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libsystemd.so.0 libuuid.so.1 libz.so.1"
RDEPENDS_rsyslog = "bash glibc libcurl libestr libfastjson libgcc libuuid logrotate systemd systemd-libs zlib"
RPM_SONAME_REQ_rsyslog-crypto = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0"
RDEPENDS_rsyslog-crypto = "glibc libgcrypt libgpg-error rsyslog"
RPM_SONAME_REQ_rsyslog-elasticsearch = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libm.so.6"
RDEPENDS_rsyslog-elasticsearch = "glibc libcurl rsyslog"
RPM_SONAME_REQ_rsyslog-gnutls = "ld-linux-aarch64.so.1 libc.so.6 libgnutls.so.30"
RDEPENDS_rsyslog-gnutls = "glibc gnutls rsyslog"
RPM_SONAME_REQ_rsyslog-gssapi = "ld-linux-aarch64.so.1 libc.so.6 libgssapi_krb5.so.2"
RDEPENDS_rsyslog-gssapi = "glibc krb5-libs rsyslog"
RPM_SONAME_REQ_rsyslog-kafka = "ld-linux-aarch64.so.1 libc.so.6 librdkafka.so.1"
RDEPENDS_rsyslog-kafka = "glibc librdkafka rsyslog"
RPM_SONAME_REQ_rsyslog-mmaudit = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_rsyslog-mmaudit = "glibc rsyslog"
RPM_SONAME_REQ_rsyslog-mmjsonparse = "ld-linux-aarch64.so.1 libc.so.6 libfastjson.so.4"
RDEPENDS_rsyslog-mmjsonparse = "glibc libfastjson rsyslog"
RPM_SONAME_REQ_rsyslog-mmkubernetes = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libfastjson.so.4 liblognorm.so.5"
RDEPENDS_rsyslog-mmkubernetes = "glibc libcurl libfastjson liblognorm rsyslog"
RPM_SONAME_REQ_rsyslog-mmnormalize = "ld-linux-aarch64.so.1 libc.so.6 libfastjson.so.4 liblognorm.so.5"
RDEPENDS_rsyslog-mmnormalize = "glibc libfastjson liblognorm rsyslog"
RPM_SONAME_REQ_rsyslog-mmsnmptrapd = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_rsyslog-mmsnmptrapd = "glibc rsyslog"
RPM_SONAME_REQ_rsyslog-mysql = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_rsyslog-mysql = "glibc mariadb-connector-c openssl-libs rsyslog zlib"
RPM_SONAME_REQ_rsyslog-pgsql = "ld-linux-aarch64.so.1 libc.so.6 libpq.so.5"
RDEPENDS_rsyslog-pgsql = "glibc libpq rsyslog"
RPM_SONAME_REQ_rsyslog-relp = "ld-linux-aarch64.so.1 libc.so.6 librelp.so.0"
RDEPENDS_rsyslog-relp = "glibc librelp rsyslog"
RPM_SONAME_REQ_rsyslog-snmp = "ld-linux-aarch64.so.1 libc.so.6 libnetsnmp.so.35"
RDEPENDS_rsyslog-snmp = "glibc net-snmp-libs rsyslog"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-crypto-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-doc-8.1911.0-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-elasticsearch-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-gnutls-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-gssapi-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-kafka-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-mmaudit-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-mmjsonparse-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-mmkubernetes-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-mmnormalize-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-mmsnmptrapd-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-mysql-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-pgsql-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-relp-8.1911.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rsyslog-snmp-8.1911.0-3.el8.aarch64.rpm \
          "

SRC_URI[rsyslog.sha256sum] = "0eb5e086fdf33366be9a6fa8cdca164d2f4f449fdcd45edb64ce8e8430828186"
SRC_URI[rsyslog-crypto.sha256sum] = "c5605d571be7e205fab3beafe103433fa46f14aa15264e58ef954b181ef5d88d"
SRC_URI[rsyslog-doc.sha256sum] = "36d0e89ef9f5573401ae9d9c1d25c9f71cf4da8f88334fded0be1a5e68ac64ee"
SRC_URI[rsyslog-elasticsearch.sha256sum] = "962445c22a174bd438cebf76c1dc40cf502dc24f14f0e66525873a57f5423420"
SRC_URI[rsyslog-gnutls.sha256sum] = "e1024f4e70ccc6ea54135f9fd03c4a54fc2d51d977bb02f1cb616e422a6f39b7"
SRC_URI[rsyslog-gssapi.sha256sum] = "d1f4be0bae31a15cd9998aa6024f7cc80d9e0903aa955abdb4fc7a2619991d80"
SRC_URI[rsyslog-kafka.sha256sum] = "dd53b0eda38b3bd31887591ea788967791449114be98387ea3d8abbdc30451d1"
SRC_URI[rsyslog-mmaudit.sha256sum] = "1f98b6505cd3a6bf97b6c7985b1aded02972a707ee58dfc9f2a29f6194deda6b"
SRC_URI[rsyslog-mmjsonparse.sha256sum] = "50d1aaea611936d0945f05a0fca24209d17102bb5791e33e1760ca0af7bcfc5a"
SRC_URI[rsyslog-mmkubernetes.sha256sum] = "31581861d5d32e8c41c689e065dde10d12a63ae1fcbd63bfe585b5be47544093"
SRC_URI[rsyslog-mmnormalize.sha256sum] = "4236e854c0fa3e2611bc9528fa32cdd73378c22257210a66eb8b2fc759a9408d"
SRC_URI[rsyslog-mmsnmptrapd.sha256sum] = "912190ef5dde228907cb4a152dc7dcf7c058cb0d6ee369b3bbff29015891fe25"
SRC_URI[rsyslog-mysql.sha256sum] = "8ed19e7a6b7a148878f833db0d680350c982632525e9f0dd5df8c0c1d9dbe08d"
SRC_URI[rsyslog-pgsql.sha256sum] = "b05ded85d660fdc0b4ef2a0e24d6b3e9058cf447977ab6a51755dd5e6220d880"
SRC_URI[rsyslog-relp.sha256sum] = "ff84d8f6118fec0c59280eeb1e291ef3941367809739a1ba7c336b7847d06b87"
SRC_URI[rsyslog-snmp.sha256sum] = "f908da243540841068f446e665ee884dacddcbe6de7153d1bf32b49b04d6e9ba"
