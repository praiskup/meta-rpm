SUMMARY = "generated recipe based on dbus-python srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-glib dbus-libs glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_python3-dbus = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_python3-dbus = "dbus-glib dbus-libs glib2 glibc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-dbus-1.2.4-15.el8.aarch64.rpm \
          "

SRC_URI[python3-dbus.sha256sum] = "116c1d18b0bda6388cde56e4c93f28b2449dc496ea6a9c5e2d4b581065e2e6c4"
