SUMMARY = "generated recipe based on librx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_librx = "librx.so.0"
RPM_SONAME_REQ_librx = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_librx = "glibc"
RPM_SONAME_REQ_librx-devel = "librx.so.0"
RPROVIDES_librx-devel = "librx-dev (= 1.5)"
RDEPENDS_librx-devel = "bash librx"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librx-1.5-31.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librx-devel-1.5-31.el8.aarch64.rpm \
          "

SRC_URI[librx.sha256sum] = "cf2fcff7a2abc39a54c6dd581fd2003edf0c37436d2fdfbded35f976ccf4e940"
SRC_URI[librx-devel.sha256sum] = "100cb46cdf1d8a4aead23a51e952385400dd3e31edc3870dcbd940e54242b709"
