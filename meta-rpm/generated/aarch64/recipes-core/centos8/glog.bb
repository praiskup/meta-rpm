SUMMARY = "generated recipe based on glog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gflags libgcc pkgconfig-native"
RPM_SONAME_PROV_glog = "libglog.so.0"
RPM_SONAME_REQ_glog = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgflags.so.2.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_glog = "gflags gflags-devel glibc libgcc libstdc++"
RPM_SONAME_REQ_glog-devel = "libglog.so.0"
RPROVIDES_glog-devel = "glog-dev (= 0.3.5)"
RDEPENDS_glog-devel = "glog pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glog-0.3.5-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glog-devel-0.3.5-3.el8.aarch64.rpm \
          "

SRC_URI[glog.sha256sum] = "e32d7a3ea5ac75ffbea0f6a097f3a5ff448671847f42e984accf7a38cea1e8df"
SRC_URI[glog-devel.sha256sum] = "f7438eb50292c161f382bcfeddeaa15d97cb1c9d699d4d68b8477e12535617ba"
