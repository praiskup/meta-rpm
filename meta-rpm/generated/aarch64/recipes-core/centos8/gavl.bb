SUMMARY = "generated recipe based on gavl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgdither pkgconfig-native"
RPM_SONAME_PROV_gavl = "libgavl.so.1"
RPM_SONAME_REQ_gavl = "ld-linux-aarch64.so.1 libc.so.6 libgdither.so.1 libm.so.6"
RDEPENDS_gavl = "glibc libgdither"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gavl-1.4.0-12.el8.aarch64.rpm \
          "

SRC_URI[gavl.sha256sum] = "31043eb09af6c64bd8f18db9d1fe8cc273d6b44255f7957c8db42c25411a66fb"
