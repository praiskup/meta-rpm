SUMMARY = "generated recipe based on linuxptp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_linuxptp = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 librt.so.1"
RDEPENDS_linuxptp = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/linuxptp-2.0-4.el8.aarch64.rpm \
          "

SRC_URI[linuxptp.sha256sum] = "fd51c83dc340ac24b07d28afd695ede0e56ba43f507c181387f1b4b7bac8d56e"
