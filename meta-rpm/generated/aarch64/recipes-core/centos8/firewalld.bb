SUMMARY = "generated recipe based on firewalld srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_firewall-applet = "NetworkManager-libnm dbus-x11 firewall-config firewalld hicolor-icon-theme libnotify platform-python python3-gobject python3-qt5-base"
RDEPENDS_firewall-config = "NetworkManager-libnm dbus-x11 firewalld gtk3 hicolor-icon-theme platform-python python3-gobject"
RDEPENDS_firewalld = "bash firewalld-filesystem ipset iptables iptables-ebtables platform-python python3-firewall systemd"
RDEPENDS_python3-firewall = "platform-python python3-dbus python3-decorator python3-gobject-base python3-nftables python3-slip-dbus"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/firewall-applet-0.8.0-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/firewall-config-0.8.0-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/firewalld-0.8.0-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-firewall-0.8.0-4.el8.noarch.rpm \
          "

SRC_URI[firewall-applet.sha256sum] = "8d0b772f948c7a9a8174feb1bacbb2456a06e3167166c3ea0e36a65b9a988d72"
SRC_URI[firewall-config.sha256sum] = "e5eea8758e23168e6536914a100d375714686ea75524e585271b967918be0f29"
SRC_URI[firewalld.sha256sum] = "28e5c5d2b834ebb188d924e17acf1323c7958b1103af7ecb7e9781f355f123a5"
SRC_URI[python3-firewall.sha256sum] = "f70246c0782ff577e7b4c9b1d58d6f810f07ce403fe86713c09622d24ada1b91"
