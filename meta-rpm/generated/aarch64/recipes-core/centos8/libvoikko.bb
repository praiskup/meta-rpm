SUMMARY = "generated recipe based on libvoikko srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libvoikko = "libvoikko.so.1"
RPM_SONAME_REQ_libvoikko = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libvoikko = "glibc libgcc libstdc++ malaga-suomi-voikko"
RPM_SONAME_REQ_voikko-tools = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libvoikko.so.1"
RDEPENDS_voikko-tools = "glibc libgcc libstdc++ libvoikko"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvoikko-4.1.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/voikko-tools-4.1.1-1.el8.aarch64.rpm \
          "

SRC_URI[libvoikko.sha256sum] = "4ae3dff6851433b75649128221d48700b34ae9bbc312d4fae04d46fdbaff4928"
SRC_URI[voikko-tools.sha256sum] = "a238b2d16f78815f9a41a0170604ea127e37a23011bd245445fed8474c1169c8"
