SUMMARY = "generated recipe based on gcc-toolset-9 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gcc-toolset-9 = "gcc-toolset-9-perftools gcc-toolset-9-runtime gcc-toolset-9-toolchain"
RDEPENDS_gcc-toolset-9-build = "gcc-toolset-9-runtime scl-utils-build"
RDEPENDS_gcc-toolset-9-perftools = "gcc-toolset-9-dyninst gcc-toolset-9-runtime gcc-toolset-9-systemtap gcc-toolset-9-valgrind"
RDEPENDS_gcc-toolset-9-runtime = "bash policycoreutils policycoreutils-python-utils scl-utils"
RDEPENDS_gcc-toolset-9-toolchain = "gcc-toolset-9-annobin gcc-toolset-9-binutils gcc-toolset-9-dwz gcc-toolset-9-elfutils gcc-toolset-9-gcc gcc-toolset-9-gcc-c++ gcc-toolset-9-gcc-gfortran gcc-toolset-9-gdb gcc-toolset-9-ltrace gcc-toolset-9-make gcc-toolset-9-runtime gcc-toolset-9-strace"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-build-9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-perftools-9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-runtime-9.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-toolchain-9.0-4.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9.sha256sum] = "5e76085419df20d9a1e6951062aaa00710ff090c608c6edf7cfefb1b0fe57758"
SRC_URI[gcc-toolset-9-build.sha256sum] = "68354b79450af8624f0b02ea32d6f6c165474a819186a923dc0a953ba511c3c6"
SRC_URI[gcc-toolset-9-perftools.sha256sum] = "128bbbb6a268f5b1c822e5798a410da05986655346526f70364686596e8d2c98"
SRC_URI[gcc-toolset-9-runtime.sha256sum] = "2fd92946247775db86dcb9485b489a2ba67d03ccb073b127e1bad769017c2954"
SRC_URI[gcc-toolset-9-toolchain.sha256sum] = "9bcf5069da3d6f83e84c876438435aac26193767a7b951be07364e9e2af0a89f"
