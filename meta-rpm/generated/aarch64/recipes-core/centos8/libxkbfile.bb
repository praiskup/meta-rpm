SUMMARY = "generated recipe based on libxkbfile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libxkbfile = "libxkbfile.so.1"
RPM_SONAME_REQ_libxkbfile = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libxkbfile = "glibc libX11"
RPM_SONAME_REQ_libxkbfile-devel = "libxkbfile.so.1"
RPROVIDES_libxkbfile-devel = "libxkbfile-dev (= 1.0.9)"
RDEPENDS_libxkbfile-devel = "libX11-devel libxkbfile pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxkbfile-1.0.9-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libxkbfile-devel-1.0.9-9.el8.aarch64.rpm \
          "

SRC_URI[libxkbfile.sha256sum] = "81815a54b3c982d8235a11c0c7f4002f9098ac096381ff693bfb0df76db1c6df"
SRC_URI[libxkbfile-devel.sha256sum] = "d605c37e0dd7d22baf7ae9eccd38d9ea152924892512af76b08169c119db6a22"
