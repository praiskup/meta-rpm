SUMMARY = "generated recipe based on vhostmd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt libxml2 pkgconfig-native"
RPM_SONAME_REQ_vhostmd = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_vhostmd = "bash glibc libvirt-libs libxml2 systemd"
RPM_SONAME_PROV_vm-dump-metrics = "libmetrics.so.0"
RPM_SONAME_REQ_vm-dump-metrics = "ld-linux-aarch64.so.1 libc.so.6 libmetrics.so.0 libxml2.so.2"
RDEPENDS_vm-dump-metrics = "glibc libxml2"
RPM_SONAME_REQ_vm-dump-metrics-devel = "libmetrics.so.0"
RPROVIDES_vm-dump-metrics-devel = "vm-dump-metrics-dev (= 1.1)"
RDEPENDS_vm-dump-metrics-devel = "pkgconf-pkg-config vm-dump-metrics"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vhostmd-1.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/vm-dump-metrics-1.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/vm-dump-metrics-devel-1.1-4.el8.aarch64.rpm \
          "

SRC_URI[vhostmd.sha256sum] = "5f790131e6636e05a7063aa5ca5e991c9bee0f35201e835bd2ec8117ab5a654f"
SRC_URI[vm-dump-metrics.sha256sum] = "0f5fc88f1e0b917125eab44c1984f7a44d7886e598b1ea5044d5d6d3581f86d5"
SRC_URI[vm-dump-metrics-devel.sha256sum] = "d4deb3840e12f1a84033821ff6bead5a5e23b9df5b46f34e554b568b275e26ca"
