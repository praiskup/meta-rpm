SUMMARY = "generated recipe based on plymouth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo glib-2.0 libdrm libpng pango pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_plymouth = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply.so.4 librt.so.1 libudev.so.1"
RDEPENDS_plymouth = "bash glibc plymouth-core-libs plymouth-scripts systemd-libs"
RPM_SONAME_PROV_plymouth-core-libs = "libply-boot-client.so.4 libply-splash-core.so.4 libply.so.4"
RPM_SONAME_REQ_plymouth-core-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply.so.4 librt.so.1 libudev.so.1"
RDEPENDS_plymouth-core-libs = "glibc systemd-libs"
RPM_SONAME_PROV_plymouth-graphics-libs = "libply-splash-graphics.so.4"
RPM_SONAME_REQ_plymouth-graphics-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libm.so.6 libply-splash-core.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-graphics-libs = "centos-logos glibc libdrm libpng plymouth-core-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-fade-throbber = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-fade-throbber = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-label = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-label = "cairo glib2 glibc libpng pango plymouth plymouth-core-libs plymouth-graphics-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-script = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-script = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-space-flares = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-space-flares = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs plymouth-plugin-label systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-throbgress = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-throbgress = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs plymouth-plugin-label systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-two-step = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-two-step = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs plymouth-plugin-label systemd-libs zlib"
RDEPENDS_plymouth-scripts = "bash coreutils cpio dracut findutils gzip plymouth"
RDEPENDS_plymouth-system-theme = "plymouth-theme-charge"
RDEPENDS_plymouth-theme-charge = "bash plymouth-plugin-two-step plymouth-scripts"
RDEPENDS_plymouth-theme-fade-in = "bash plymouth-plugin-fade-throbber plymouth-scripts"
RDEPENDS_plymouth-theme-script = "plymouth-plugin-script plymouth-scripts"
RDEPENDS_plymouth-theme-solar = "bash plymouth-plugin-space-flares plymouth-scripts"
RDEPENDS_plymouth-theme-spinfinity = "bash plymouth-plugin-throbgress plymouth-scripts"
RDEPENDS_plymouth-theme-spinner = "bash plymouth-plugin-two-step plymouth-scripts"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-core-libs-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-graphics-libs-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-plugin-fade-throbber-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-plugin-label-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-plugin-script-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-plugin-space-flares-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-plugin-throbgress-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-plugin-two-step-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-scripts-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-system-theme-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-theme-charge-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-theme-fade-in-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-theme-script-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-theme-solar-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-theme-spinfinity-0.9.3-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/plymouth-theme-spinner-0.9.3-16.el8.aarch64.rpm \
          "

SRC_URI[plymouth.sha256sum] = "fe6f3a079aa1f91fc20cb9b327cf02bb695fb427e395448cd17424ec30228208"
SRC_URI[plymouth-core-libs.sha256sum] = "ed32b88153732da3a8d0fc93502e1e7c7bc9cd7850394b430a36b51aa9957fb4"
SRC_URI[plymouth-graphics-libs.sha256sum] = "eff6b16d2869dc45f84a923fcb88fd5e4244b975e647307e6c3a257a70f2cfec"
SRC_URI[plymouth-plugin-fade-throbber.sha256sum] = "1b45a924488f74c156c5630838ebac9536b977bc423a425a5d7ec654959eb087"
SRC_URI[plymouth-plugin-label.sha256sum] = "95f397a618fc116baf929ff6d234dec6f1f37324fe104d27ba1bcfd365235f25"
SRC_URI[plymouth-plugin-script.sha256sum] = "ce70f4ed3863e31f3dab99d7bd66efbee02b784b8b2e07b4b47d8240c4978303"
SRC_URI[plymouth-plugin-space-flares.sha256sum] = "7ad38df41d5a140ae7c5772b7cacaf611c78090a6121f005217f819d124d2efb"
SRC_URI[plymouth-plugin-throbgress.sha256sum] = "2f03e89d3d3f9c4b94c0b609bc1067c3c04f41792c5fc16bf24686242a9fa184"
SRC_URI[plymouth-plugin-two-step.sha256sum] = "ff953bfd1b24e8869871865eac7037779a5a91425534353c7718a52223f9e7c1"
SRC_URI[plymouth-scripts.sha256sum] = "b05e999c9b21fc7339677f7c7e561932cab39c0496b8f674fe98c891d0c57c4d"
SRC_URI[plymouth-system-theme.sha256sum] = "4bb485da51ec7d94cefa6006ae7d0236a0adba7991748511d333ed92b1a2b601"
SRC_URI[plymouth-theme-charge.sha256sum] = "972d4803ada69f687c6986074943732f564f581d193f2c54ef7a8516a5515a86"
SRC_URI[plymouth-theme-fade-in.sha256sum] = "751d346508b39d8af313e266ea3df88abab15fb3db3dc333ca910a415e8b307a"
SRC_URI[plymouth-theme-script.sha256sum] = "89b64e146d94bdd6804d03c2cdeeb6187b5047117d0b055175ccf8c63c064723"
SRC_URI[plymouth-theme-solar.sha256sum] = "f538033c490f088bb90ff022903c50934bf32411c24114c541ec191caa7c7c0d"
SRC_URI[plymouth-theme-spinfinity.sha256sum] = "03aaf88afdd13cc9e549a6497f590d83b3369ee22933e1cbabf4b4468071b2d6"
SRC_URI[plymouth-theme-spinner.sha256sum] = "1a4b5bb8c6c4d298c89478f55a41b6daaf24616d1d3a010fbe5d6bc91760adf2"
