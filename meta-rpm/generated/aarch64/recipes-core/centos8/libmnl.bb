SUMMARY = "generated recipe based on libmnl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmnl = "libmnl.so.0"
RPM_SONAME_REQ_libmnl = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libmnl = "glibc"
RPM_SONAME_REQ_libmnl-devel = "libmnl.so.0"
RPROVIDES_libmnl-devel = "libmnl-dev (= 1.0.4)"
RDEPENDS_libmnl-devel = "libmnl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmnl-1.0.4-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmnl-devel-1.0.4-6.el8.aarch64.rpm \
          "

SRC_URI[libmnl.sha256sum] = "fbe4f2cb2660ebe3cb90a73c7dfbd978059af138356e46c9a93049761c0467ef"
SRC_URI[libmnl-devel.sha256sum] = "470530b232164e3f1d9a5009b3e7b81dcb96b063852fe49e1b1ea9004dbaaaaf"
