SUMMARY = "generated recipe based on python-greenlet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-greenlet = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-greenlet = "glibc platform-python python3-libs"
RPROVIDES_python3-greenlet-devel = "python3-greenlet-dev (= 0.4.13)"
RDEPENDS_python3-greenlet-devel = "python3-greenlet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-greenlet-0.4.13-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-greenlet-devel-0.4.13-4.el8.aarch64.rpm \
          "

SRC_URI[python3-greenlet.sha256sum] = "614c27e1e062d6b5b5921b0ecbd53ddcdc9960599e43ee93319d9a1bc2863669"
SRC_URI[python3-greenlet-devel.sha256sum] = "03b1481798134bd981911e9d1a2f48bb4f9461b23419a2321b84485b5adfc665"
