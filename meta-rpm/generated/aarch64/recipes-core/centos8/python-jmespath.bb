SUMMARY = "generated recipe based on python-jmespath srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jmespath = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-jmespath-0.9.0-11.el8.noarch.rpm \
          "

SRC_URI[python3-jmespath.sha256sum] = "35d8eaea77f07ea30c1550caf613b10f8d69c1ac73b6ff4655a48502de7a2f5b"
