SUMMARY = "generated recipe based on edk2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/edk2-aarch64-20190829git37eef91017ad-9.el8.noarch.rpm \
          "

SRC_URI[edk2-aarch64.sha256sum] = "f916025f920d803499ff86b1a63483edaf2f3d851661e264d3ba270e550849f3"
