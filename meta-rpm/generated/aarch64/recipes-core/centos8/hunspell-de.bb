SUMMARY = "generated recipe based on hunspell-de srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-de = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-de-0.20161207-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-de.sha256sum] = "c2e210b5c103976048454e1d34760cd46e16160637953a0f1c3dd2d0a15b8f9e"
