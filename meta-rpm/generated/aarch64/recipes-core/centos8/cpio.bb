SUMMARY = "generated recipe based on cpio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cpio = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_cpio = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cpio-2.12-8.el8.aarch64.rpm \
          "

SRC_URI[cpio.sha256sum] = "9a200f1cabd5869c7cd6483dc6e143e394bbd83fc6d653d279bc3ec25e1e35cc"
