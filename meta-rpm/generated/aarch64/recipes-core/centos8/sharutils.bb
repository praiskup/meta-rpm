SUMMARY = "generated recipe based on sharutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_sharutils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_sharutils = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sharutils-4.15.2-11.el8.aarch64.rpm \
          "

SRC_URI[sharutils.sha256sum] = "935f6470ae044b0a56bd3c2fc71ff4c938bd48167c2f970c0f5496ba54505492"
