SUMMARY = "generated recipe based on perl-Module-ScanDeps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-ScanDeps = "perl-Data-Dumper perl-Encode perl-Exporter perl-ExtUtils-MakeMaker perl-File-Path perl-File-Temp perl-Getopt-Long perl-Module-Metadata perl-PathTools perl-Text-ParseWords perl-constant perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Module-ScanDeps-1.24-3.el8.noarch.rpm \
          "

SRC_URI[perl-Module-ScanDeps.sha256sum] = "64934ab9696dadf92ed779d31dd17a563746a0edf23dcc716b2d278a225b5e06"
