SUMMARY = "generated recipe based on redhat-rpm-config srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_kernel-rpm-macros = "bash findutils perl-Getopt-Long perl-interpreter redhat-rpm-config"
RDEPENDS_redhat-rpm-config = "bash coreutils dwz efi-srpm-macros file findutils ghc-srpm-macros go-srpm-macros grep ocaml-srpm-macros openblas-srpm-macros perl-srpm-macros python-srpm-macros python3-rpm-macros qt5-srpm-macros rpm rust-srpm-macros sed zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/kernel-rpm-macros-122-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-rpm-config-122-1.el8.noarch.rpm \
          "

SRC_URI[kernel-rpm-macros.sha256sum] = "e49d4c2fb055573196ca19762413774ffbbc91cd93128acf040f580a32983043"
SRC_URI[redhat-rpm-config.sha256sum] = "2e3f2fb0c44b38aeae9df75bae5ca03c74968d8f8dd9fd2a0af9ca57dfa4152a"
