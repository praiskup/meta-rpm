SUMMARY = "generated recipe based on compat-libgfortran-48 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_compat-libgfortran-48 = "libgfortran.so.3"
RPM_SONAME_REQ_compat-libgfortran-48 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6"
RDEPENDS_compat-libgfortran-48 = "binutils glibc glibc-devel libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/compat-libgfortran-48-4.8.5-36.1.el8.aarch64.rpm \
          "

SRC_URI[compat-libgfortran-48.sha256sum] = "4edcb392edee908a9eb422ab24aeb787d7f2115afc14a154c7f86019a67c0037"
