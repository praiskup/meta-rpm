SUMMARY = "generated recipe based on libproxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native webkit2gtk3"
RPM_SONAME_REQ_libproxy-webkitgtk4 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libjavascriptcoregtk-4.0.so.18 libm.so.6 libproxy.so.1 libstdc++.so.6"
RDEPENDS_libproxy-webkitgtk4 = "glib2 glibc libgcc libproxy libstdc++ webkit2gtk3-jsc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libproxy-webkitgtk4-0.4.15-5.2.el8.aarch64.rpm \
          "

SRC_URI[libproxy-webkitgtk4.sha256sum] = "aa7b5c287148030ca252b970e1a3af3be3c0e667f4969618938bceb8bcc6f005"
