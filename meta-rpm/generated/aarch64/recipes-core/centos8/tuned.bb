SUMMARY = "generated recipe based on tuned srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_tuned = "bash dbus ethtool gawk hdparm platform-python polkit python3-configobj python3-dbus python3-decorator python3-gobject-base python3-linux-procfs python3-perf python3-pyudev python3-schedutils python3-syspurpose systemd util-linux virt-what"
RDEPENDS_tuned-profiles-atomic = "tuned"
RDEPENDS_tuned-profiles-compat = "bash tuned"
RDEPENDS_tuned-profiles-cpu-partitioning = "bash tuned"
RDEPENDS_tuned-profiles-mssql = "tuned"
RDEPENDS_tuned-profiles-oracle = "tuned"
RDEPENDS_tuned-utils = "platform-python powertop tuned"
RDEPENDS_tuned-utils-systemtap = "platform-python systemtap systemtap-devel tuned"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tuned-utils-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tuned-utils-systemtap-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tuned-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tuned-profiles-atomic-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tuned-profiles-compat-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tuned-profiles-cpu-partitioning-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tuned-profiles-mssql-2.13.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tuned-profiles-oracle-2.13.0-6.el8.noarch.rpm \
          "

SRC_URI[tuned.sha256sum] = "6a04f539fb9c714dce79e31d0f011970025ebce6f628c7bb5365688512c20d6a"
SRC_URI[tuned-profiles-atomic.sha256sum] = "135e47d6572fc871369d84bdf891ac5132c3b82865189048de284a6a1d9c5f12"
SRC_URI[tuned-profiles-compat.sha256sum] = "f2122af6fcb6f2dc90d3554394ad982033bd21afa4a4e7b72149fb8a1b913e95"
SRC_URI[tuned-profiles-cpu-partitioning.sha256sum] = "dd57143336a840498879f7be8d3f5e5f0eceac73268f733f997f45c12c270629"
SRC_URI[tuned-profiles-mssql.sha256sum] = "b80ee42a1908fd0d51810d9c2a622aeb7eaf708fbb5498631b795f376fb37ca8"
SRC_URI[tuned-profiles-oracle.sha256sum] = "fc84184bc899cc215e8afefd671bf904623706043bac41df3d4dd7e7811e5bcd"
SRC_URI[tuned-utils.sha256sum] = "6bde5ab3d028e54690c43f9dd31cd53d08c5ba34474cf9a66255899b15f87ca1"
SRC_URI[tuned-utils-systemtap.sha256sum] = "9ce91ac0100b9de255297ac5beba98526a193d1b72fb90b48b0727f6368c3bd2"
