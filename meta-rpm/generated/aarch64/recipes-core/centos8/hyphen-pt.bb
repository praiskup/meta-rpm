SUMMARY = "generated recipe based on hyphen-pt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-pt = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-pt-0.20021021-17.el8.noarch.rpm \
          "

SRC_URI[hyphen-pt.sha256sum] = "96a43dbb37393a10f3b7c36063f1f7a64504f824796ee805bf00bcaba000aa3f"
