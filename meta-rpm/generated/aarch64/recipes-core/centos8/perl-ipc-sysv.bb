SUMMARY = "generated recipe based on perl-IPC-SysV srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-IPC-SysV = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-IPC-SysV = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-IPC-SysV-2.07-397.el8.aarch64.rpm \
          "

SRC_URI[perl-IPC-SysV.sha256sum] = "5b3cc6677b8f7d85b75f75bb0d86b1f881024e91a536b26485e619d8000b827c"
