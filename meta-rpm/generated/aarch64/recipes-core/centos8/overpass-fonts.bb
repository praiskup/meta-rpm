SUMMARY = "generated recipe based on overpass-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_overpass-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/overpass-fonts-3.0.2-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/overpass-mono-fonts-3.0.2-3.el8.noarch.rpm \
          "

SRC_URI[overpass-fonts.sha256sum] = "6a009ec3a564951470219467679b66d8c38faca2c665849ca575881b9f907c13"
SRC_URI[overpass-mono-fonts.sha256sum] = "032006d2a85649ee9513a08454dfadde0c7c7522d6065774a45b673b5e058161"
