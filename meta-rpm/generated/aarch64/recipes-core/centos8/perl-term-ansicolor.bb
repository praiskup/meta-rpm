SUMMARY = "generated recipe based on perl-Term-ANSIColor srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-ANSIColor = "perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Term-ANSIColor-4.06-396.el8.noarch.rpm \
          "

SRC_URI[perl-Term-ANSIColor.sha256sum] = "f4e3607f242bbca7ec2379822ca961860e6d9c276da51c6e2dfd17a29469ec78"
