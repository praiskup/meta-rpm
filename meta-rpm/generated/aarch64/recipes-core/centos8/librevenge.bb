SUMMARY = "generated recipe based on librevenge srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_librevenge = "librevenge-0.0.so.0 librevenge-generators-0.0.so.0 librevenge-stream-0.0.so.0"
RPM_SONAME_REQ_librevenge = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_librevenge = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_librevenge-devel = "librevenge-0.0.so.0 librevenge-generators-0.0.so.0 librevenge-stream-0.0.so.0"
RPROVIDES_librevenge-devel = "librevenge-dev (= 0.0.4)"
RDEPENDS_librevenge-devel = "librevenge pkgconf-pkg-config"
RDEPENDS_librevenge-gdb = "librevenge python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librevenge-0.0.4-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librevenge-gdb-0.0.4-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librevenge-devel-0.0.4-12.el8.aarch64.rpm \
          "

SRC_URI[librevenge.sha256sum] = "d8e6dd6c0d85f28c34bbe78e2b11654b57dca365bf51a12317c9ab7b93a4f25b"
SRC_URI[librevenge-devel.sha256sum] = "d4aaccbd9365b366838ea6ab70db2dab1c7ed6ef6d5ff3fd1f88cd15b891c42c"
SRC_URI[librevenge-gdb.sha256sum] = "989eeabcb8019987eceabd753eace5311b3d0000e0354fdba88a525f6ef6ae8d"
