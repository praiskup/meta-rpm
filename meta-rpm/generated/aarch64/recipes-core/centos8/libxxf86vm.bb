SUMMARY = "generated recipe based on libXxf86vm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXxf86vm = "libXxf86vm.so.1"
RPM_SONAME_REQ_libXxf86vm = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXxf86vm = "glibc libX11 libXext"
RPM_SONAME_REQ_libXxf86vm-devel = "libXxf86vm.so.1"
RPROVIDES_libXxf86vm-devel = "libXxf86vm-dev (= 1.1.4)"
RDEPENDS_libXxf86vm-devel = "libX11-devel libXext-devel libXxf86vm pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXxf86vm-1.1.4-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXxf86vm-devel-1.1.4-9.el8.aarch64.rpm \
          "

SRC_URI[libXxf86vm.sha256sum] = "85780656a9cbd6b39c1c97660af353766cd3de6c23461bb81ae562ac8c2907fc"
SRC_URI[libXxf86vm-devel.sha256sum] = "abdbe453413d8b58b3d50fd24b1cb3c306e7d9925e43a842bf692bc726d759b8"
