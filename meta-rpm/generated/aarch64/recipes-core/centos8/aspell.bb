SUMMARY = "generated recipe based on aspell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc ncurses pkgconfig-native"
RPM_SONAME_PROV_aspell = "libaspell.so.15 libpspell.so.15"
RPM_SONAME_REQ_aspell = "ld-linux-aarch64.so.1 libaspell.so.15 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libncursesw.so.6 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_aspell = "bash glibc info libgcc libstdc++ ncurses-libs"
RPM_SONAME_REQ_aspell-devel = "libaspell.so.15 libpspell.so.15"
RPROVIDES_aspell-devel = "aspell-dev (= 0.60.6.1)"
RDEPENDS_aspell-devel = "aspell bash info pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/aspell-0.60.6.1-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/aspell-devel-0.60.6.1-21.el8.aarch64.rpm \
          "

SRC_URI[aspell.sha256sum] = "4bcd405db31085058aad8a190d0874a8e8103a8b2287616bfc0ab1ef9bddbeca"
SRC_URI[aspell-devel.sha256sum] = "91812f4903fb29bd56b24e3aa082fd4df00808849b1264bcf14fdf788a6be1df"
