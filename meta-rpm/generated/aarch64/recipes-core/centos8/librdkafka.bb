SUMMARY = "generated recipe based on librdkafka srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib libgcc lz4 openssl pkgconfig-native zlib"
RPM_SONAME_PROV_librdkafka = "librdkafka++.so.1 librdkafka.so.1"
RPM_SONAME_REQ_librdkafka = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblz4.so.1 libpthread.so.0 librdkafka.so.1 librt.so.1 libsasl2.so.3 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_librdkafka = "cyrus-sasl-lib glibc libgcc libstdc++ lz4-libs openssl-libs zlib"
RPM_SONAME_REQ_librdkafka-devel = "librdkafka++.so.1 librdkafka.so.1"
RPROVIDES_librdkafka-devel = "librdkafka-dev (= 0.11.4)"
RDEPENDS_librdkafka-devel = "librdkafka pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librdkafka-0.11.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/librdkafka-devel-0.11.4-1.el8.aarch64.rpm \
          "

SRC_URI[librdkafka.sha256sum] = "f7b1e48d505965af488e279f71494870404540d99cf01b8e6bb02e75afea8995"
SRC_URI[librdkafka-devel.sha256sum] = "a94350c1217b2f80f216d8f7610864730a14c904334a5aa161f888d5d88c952a"
