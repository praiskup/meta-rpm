SUMMARY = "generated recipe based on gsl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_gsl = "libgsl.so.23 libgslcblas.so.0"
RPM_SONAME_REQ_gsl = "ld-linux-aarch64.so.1 libc.so.6 libgsl.so.23 libgslcblas.so.0 libm.so.6"
RDEPENDS_gsl = "glibc"
RPM_SONAME_REQ_gsl-devel = "libgsl.so.23 libgslcblas.so.0"
RPROVIDES_gsl-devel = "gsl-dev (= 2.5)"
RDEPENDS_gsl-devel = "automake bash gsl info pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gsl-2.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gsl-devel-2.5-1.el8.aarch64.rpm \
          "

SRC_URI[gsl.sha256sum] = "6d4df17af5b266f62276ee2c325948aca9db54ce7d8791089b7b9b8b7a9413b6"
SRC_URI[gsl-devel.sha256sum] = "71d739bf183e01a44a33141805e50660b0feed0afecdd43e70ee90f4a74de772"
