SUMMARY = "generated recipe based on libarchive srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl bzip2 libxml2 lz4 openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_bsdtar = "ld-linux-aarch64.so.1 libacl.so.1 libarchive.so.13 libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblz4.so.1 liblzma.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_bsdtar = "bzip2-libs glibc libacl libarchive libxml2 lz4-libs openssl-libs xz-libs zlib"
RPM_SONAME_PROV_libarchive = "libarchive.so.13"
RPM_SONAME_REQ_libarchive = "ld-linux-aarch64.so.1 libacl.so.1 libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblz4.so.1 liblzma.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_libarchive = "bzip2-libs glibc libacl libxml2 lz4-libs openssl-libs xz-libs zlib"
RPM_SONAME_REQ_libarchive-devel = "libarchive.so.13"
RPROVIDES_libarchive-devel = "libarchive-dev (= 3.3.2)"
RDEPENDS_libarchive-devel = "libarchive pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bsdtar-3.3.2-8.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libarchive-3.3.2-8.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libarchive-devel-3.3.2-8.el8_1.aarch64.rpm \
          "

SRC_URI[bsdtar.sha256sum] = "4139ed37a7fba2f78a81b28965004e02a6cb7ace2b71cd319382731a2958f7e4"
SRC_URI[libarchive.sha256sum] = "bcaf73c3ca5ef466bdb61cc5f8db55fb082a6fdcab98bcaa6f44c91ddddec2d1"
SRC_URI[libarchive-devel.sha256sum] = "7bc0bef82b1d49a7c7b94eaebdbee6b277c9ed34c2593746888ffd7e971ee814"
