SUMMARY = "generated recipe based on gimp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib atk babl bzip2 cairo dbus-glib dbus-libs fontconfig freetype gdk-pixbuf gegl ghostscript glib-2.0 gtk2 jasper lcms2 libexif libgudev libice libjpeg-turbo libmng libpng librsvg libsm libwmf libx11 libxcursor libxext libxfixes libxml2 libxmu libxpm libxt pango pkgconfig-native poppler tiff xz zlib"
RPM_SONAME_PROV_gimp = "libcolor-selector-cmyk.so libcolor-selector-water.so libcolor-selector-wheel.so libcontroller-linux-input.so libcontroller-midi.so libdisplay-filter-color-blind.so libdisplay-filter-gamma.so libdisplay-filter-high-contrast.so libdisplay-filter-lcms.so libdisplay-filter-proof.so"
RPM_SONAME_REQ_gimp = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXcursor.so.1 libXext.so.6 libXfixes.so.3 libXmu.so.6 libXpm.so.4 libXt.so.6 libasound.so.2 libatk-1.0.so.0 libbabl-0.1.so.0 libbz2.so.1 libc.so.6 libcairo.so.2 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libexif.so.12 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgegl-0.2.so.0 libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpthumb-2.0.so.0 libgimpui-2.0.so.0 libgimpwidgets-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgs.so.9 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libgudev-1.0.so.0 libjasper.so.4 libjpeg.so.62 liblcms2.so.2 liblzma.so.5 libm.so.6 libmng.so.2 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 libpoppler-glib.so.8 libpthread.so.0 librsvg-2.so.2 libtiff.so.5 libwmf-0.2.so.7 libwmflite-0.2.so.7 libxml2.so.2 libz.so.1"
RDEPENDS_gimp = "alsa-lib atk babl bzip2-libs cairo dbus-glib dbus-libs fontconfig freetype gdk-pixbuf2 gegl gimp-libs glib2 glibc gtk2 hicolor-icon-theme jasper-libs lcms2 libICE libSM libX11 libXcursor libXext libXfixes libXmu libXpm libXt libexif libgs libgudev libjpeg-turbo libmng libpng librsvg2 libtiff libwmf libwmf-lite libxml2 pango poppler-glib pygtk2 xdg-utils xz-libs zlib"
RPM_SONAME_REQ_gimp-devel = "libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpthumb-2.0.so.0 libgimpui-2.0.so.0 libgimpwidgets-2.0.so.0"
RPROVIDES_gimp-devel = "gimp-dev (= 2.8.22)"
RDEPENDS_gimp-devel = "cairo-devel gdk-pixbuf2-devel gimp-devel-tools gimp-libs glib2-devel gtk2-devel pkgconf-pkg-config rpm"
RPM_SONAME_REQ_gimp-devel-tools = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgimpbase-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gimp-devel-tools = "atk cairo fontconfig freetype gdk-pixbuf2 gimp-devel gimp-libs glib2 glibc gtk2 pango"
RPM_SONAME_PROV_gimp-libs = "libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpthumb-2.0.so.0 libgimpui-2.0.so.0 libgimpwidgets-2.0.so.0"
RPM_SONAME_REQ_gimp-libs = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpwidgets-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gimp-libs = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gimp-2.8.22-15.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gimp-devel-2.8.22-15.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gimp-devel-tools-2.8.22-15.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gimp-libs-2.8.22-15.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
          "

SRC_URI[gimp.sha256sum] = "1125422b65aada9e0adf177944af2e86e9fb7d0f47216afca2c9bae97a6c1fe9"
SRC_URI[gimp-devel.sha256sum] = "5f0572c1fd7d3477c720e2a5b4bb52a5cce34fb5665b3c41c0d343fd149cb7ae"
SRC_URI[gimp-devel-tools.sha256sum] = "95473d6012883bd267b1e170466755930dbde2c7fab044a464a500df8d04c3e0"
SRC_URI[gimp-libs.sha256sum] = "522b625836f3021791d04168503cf3eac4567cda7f217e7ed132bce920429397"
