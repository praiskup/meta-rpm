SUMMARY = "generated recipe based on libev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libev = "libev.so.4"
RPM_SONAME_REQ_libev = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libev = "glibc"
RPM_SONAME_REQ_libev-devel = "libev.so.4"
RPROVIDES_libev-devel = "libev-dev (= 4.24)"
RDEPENDS_libev-devel = "libev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libev-4.24-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libev-devel-4.24-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libev-source-4.24-6.el8.noarch.rpm \
          "

SRC_URI[libev.sha256sum] = "8ba84e4c6b7a20a26e56a84bd5df7a836683a4d3378eecd59af0d7d808df647c"
SRC_URI[libev-devel.sha256sum] = "ed003bf6551a7602c73eea3abdfde5e0b72c2b14fe5db5927c8de6f2d3e76a2f"
SRC_URI[libev-source.sha256sum] = "c27cd56149df04c3af07a8f06a6a722986613bd7775f6ae6324de9ef70c63113"
