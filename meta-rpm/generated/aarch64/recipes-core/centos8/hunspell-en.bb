SUMMARY = "generated recipe based on hunspell-en srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-en = "hunspell hunspell-en-GB hunspell-en-US"
RDEPENDS_hunspell-en-GB = "hunspell"
RDEPENDS_hunspell-en-US = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-en-0.20140811.1-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-en-GB-0.20140811.1-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-en-US-0.20140811.1-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-en.sha256sum] = "8af4665b7468b8124bac5f17d9be10045157e3b93b0c12a5eb54c2d8d6ea9b67"
SRC_URI[hunspell-en-GB.sha256sum] = "6a6211d3005556122fb8596f2a3f7b987fbea24c7e986d3434ac4d0fd26c0ac4"
SRC_URI[hunspell-en-US.sha256sum] = "4b46a9f55fa04a02127dd120bb436041509fed9ef4d227960240bfbc28ec6e85"
