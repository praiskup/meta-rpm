SUMMARY = "generated recipe based on dovecot srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 clucene e2fsprogs expat krb5-libs libcap libgcc libpq libxcrypt lz4 mariadb-connector-c openldap openssl pam pkgconfig-native sqlite3 xz zlib"
RPM_SONAME_PROV_dovecot = "lib01_acl_plugin.so lib02_imap_acl_plugin.so lib02_lazy_expunge_plugin.so lib05_mail_crypt_acl_plugin.so lib05_pop3_migration_plugin.so lib05_snarf_plugin.so lib10_doveadm_acl_plugin.so lib10_doveadm_expire_plugin.so lib10_doveadm_quota_plugin.so lib10_last_login_plugin.so lib10_mail_crypt_plugin.so lib10_mail_filter_plugin.so lib10_quota_plugin.so lib11_imap_quota_plugin.so lib11_trash_plugin.so lib15_notify_plugin.so lib20_auth_var_expand_crypt.so lib20_autocreate_plugin.so lib20_charset_alias_plugin.so lib20_doveadm_fts_lucene_plugin.so lib20_doveadm_fts_plugin.so lib20_expire_plugin.so lib20_fts_plugin.so lib20_listescape_plugin.so lib20_mail_log_plugin.so lib20_mailbox_alias_plugin.so lib20_notify_status_plugin.so lib20_push_notification_plugin.so lib20_quota_clone_plugin.so lib20_replication_plugin.so lib20_var_expand_crypt.so lib20_virtual_plugin.so lib20_zlib_plugin.so lib21_fts_lucene_plugin.so lib21_fts_solr_plugin.so lib21_fts_squat_plugin.so lib30_imap_zlib_plugin.so lib90_old_stats_plugin.so lib95_imap_old_stats_plugin.so lib99_welcome_plugin.so libauthdb_imap.so libauthdb_ldap.so libdcrypt_openssl.so libdict_ldap.so libdoveadm_mail_crypt_plugin.so libdovecot-compression.so.0 libdovecot-dsync.so.0 libdovecot-fts.so.0 libdovecot-lda.so.0 libdovecot-ldap.so.0 libdovecot-login.so.0 libdovecot-sieve.so.0 libdovecot-sql.so.0 libdovecot-storage.so.0 libdovecot.so.0 libdriver_sqlite.so libfs_compress.so libfs_crypt.so libfs_mail_crypt.so libmech_gssapi.so libold_stats_mail.so libssl_iostream_openssl.so libstats_auth.so"
RPM_SONAME_REQ_dovecot = "ld-linux-aarch64.so.1 lib01_acl_plugin.so lib10_mail_crypt_plugin.so lib10_quota_plugin.so lib15_notify_plugin.so lib20_fts_plugin.so lib90_old_stats_plugin.so libbz2.so.1 libc.so.6 libcap.so.2 libclucene-core.so.1 libclucene-shared.so.1 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libdovecot-compression.so.0 libdovecot-lda.so.0 libdovecot-ldap.so.0 libdovecot-login.so.0 libdovecot-storage.so.0 libdovecot.so.0 libexpat.so.1 libgcc_s.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 liblz4.so.1 liblzma.so.5 libm.so.6 libpam.so.0 libsqlite3.so.0 libssl.so.1.1 libssl_iostream_openssl.so libstats_auth.so libstdc++.so.6 libz.so.1"
RDEPENDS_dovecot = "bash bzip2-libs clucene-core expat glibc krb5-libs libcap libcom_err libgcc libstdc++ libxcrypt lz4-libs openldap openssl openssl-libs pam shadow-utils sqlite-libs systemd xz-libs zlib"
RPM_SONAME_REQ_dovecot-devel = "libdovecot-compression.so.0 libdovecot-dsync.so.0 libdovecot-fts.so.0 libdovecot-lda.so.0 libdovecot-ldap.so.0 libdovecot-login.so.0 libdovecot-sieve.so.0 libdovecot-sql.so.0 libdovecot-storage.so.0 libdovecot.so.0"
RPROVIDES_dovecot-devel = "dovecot-dev (= 2.3.8)"
RDEPENDS_dovecot-devel = "dovecot"
RPM_SONAME_PROV_dovecot-mysql = "libdriver_mysql.so"
RPM_SONAME_REQ_dovecot-mysql = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_dovecot-mysql = "dovecot glibc mariadb-connector-c openssl-libs zlib"
RPM_SONAME_PROV_dovecot-pgsql = "libdriver_pgsql.so"
RPM_SONAME_REQ_dovecot-pgsql = "ld-linux-aarch64.so.1 libc.so.6 libpq.so.5"
RDEPENDS_dovecot-pgsql = "dovecot glibc libpq"
RPM_SONAME_PROV_dovecot-pigeonhole = "lib10_doveadm_sieve_plugin.so lib90_sieve_extprograms_plugin.so lib90_sieve_imapsieve_plugin.so lib90_sieve_plugin.so lib95_imap_filter_sieve_plugin.so lib95_imap_sieve_plugin.so libmanagesieve_login_settings.so libmanagesieve_settings.so libpigeonhole_settings.so"
RPM_SONAME_REQ_dovecot-pigeonhole = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdovecot-lda.so.0 libdovecot-login.so.0 libdovecot-sieve.so.0 libdovecot-storage.so.0 libdovecot.so.0 libssl.so.1.1"
RDEPENDS_dovecot-pigeonhole = "dovecot glibc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dovecot-2.3.8-2.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dovecot-mysql-2.3.8-2.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dovecot-pgsql-2.3.8-2.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dovecot-pigeonhole-2.3.8-2.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dovecot-devel-2.3.8-2.el8_2.2.aarch64.rpm \
          "

SRC_URI[dovecot.sha256sum] = "7e5e20b4c4f597ddea979d1f50389dea37f99bfeb249b62f3242f718e602ec71"
SRC_URI[dovecot-devel.sha256sum] = "9ea49272fab3cfd8048d4c24ff1276b58ea73cdfe827ae81336d480a3c095c2a"
SRC_URI[dovecot-mysql.sha256sum] = "fccf4318533e813c79fd51f23d655eefd9ca9e1da6036fd4000b46347fe25cfa"
SRC_URI[dovecot-pgsql.sha256sum] = "8ed8d1e2dff96410f41656d20942fb98c3c30ec508753ee37eb8dffa20638332"
SRC_URI[dovecot-pigeonhole.sha256sum] = "e5d0aadc8f821ec04e3fa2790da95a719dd53c8af09614d0f9a3e3ef31fc76b2"
