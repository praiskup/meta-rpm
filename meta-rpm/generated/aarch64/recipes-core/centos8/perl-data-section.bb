SUMMARY = "generated recipe based on perl-Data-Section srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Data-Section = "perl-Encode perl-MRO-Compat perl-Sub-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Data-Section-0.200007-3.el8.noarch.rpm \
          "

SRC_URI[perl-Data-Section.sha256sum] = "9a7a97485356940549ed2a190d46767baae634f641197aa25b0a7019f505c84a"
