SUMMARY = "generated recipe based on nspr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_nspr = "libnspr4.so libplc4.so libplds4.so"
RPM_SONAME_REQ_nspr = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnspr4.so libpthread.so.0 librt.so.1"
RDEPENDS_nspr = "glibc"
RPROVIDES_nspr-devel = "nspr-dev (= 4.25.0)"
RDEPENDS_nspr-devel = "bash nspr pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nspr-4.25.0-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nspr-devel-4.25.0-2.el8_2.aarch64.rpm \
          "

SRC_URI[nspr.sha256sum] = "7e2dedab30c1a87a8e23ae1693245fe0ba742d63b7a750c23a0b9946b303856c"
SRC_URI[nspr-devel.sha256sum] = "39a331cae5156ffb0e7048bc8da31fb58e784c45b21cca045bd7de402403d70e"
