SUMMARY = "generated recipe based on vulkan-validation-layers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native spirv-tools"
RPM_SONAME_PROV_vulkan-validation-layers = "libVkLayer_khronos_validation.so libVkLayer_utils.so"
RPM_SONAME_REQ_vulkan-validation-layers = "ld-linux-aarch64.so.1 libSPIRV-Tools-opt.so libSPIRV-Tools.so libVkLayer_utils.so libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_vulkan-validation-layers = "glibc libgcc libstdc++ spirv-tools-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vulkan-validation-layers-1.2.135.0-1.el8.aarch64.rpm \
          "

SRC_URI[vulkan-validation-layers.sha256sum] = "8d60a5a66046660c1c75fcf2eb8cae9eb28416be24a65a9b01c3b0710fa04d58"
