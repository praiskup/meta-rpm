SUMMARY = "generated recipe based on hwloc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpciaccess libtool libx11 libxext libxml2 libxnvctrl ncurses numactl pkgconfig-native rdma-core"
RPM_SONAME_REQ_hwloc = "ld-linux-aarch64.so.1 libc.so.6 libhwloc.so.5 libltdl.so.7 libm.so.6 libnuma.so.1 libtinfo.so.6"
RDEPENDS_hwloc = "bash glibc hwloc-libs libtool-ltdl ncurses-libs numactl-libs"
RPM_SONAME_REQ_hwloc-devel = "libhwloc.so.5"
RPROVIDES_hwloc-devel = "hwloc-dev (= 1.11.9)"
RDEPENDS_hwloc-devel = "hwloc-libs pkgconf-pkg-config rdma-core-devel"
RPM_SONAME_PROV_hwloc-libs = "libhwloc.so.5"
RPM_SONAME_REQ_hwloc-libs = "ld-linux-aarch64.so.1 libc.so.6 libltdl.so.7 libm.so.6 libnuma.so.1"
RDEPENDS_hwloc-libs = "glibc libtool-ltdl numactl-libs"
RPM_SONAME_REQ_hwloc-plugins = "ld-linux-aarch64.so.1 libX11.so.6 libXNVCtrl.so.0 libXext.so.6 libc.so.6 libltdl.so.7 libpciaccess.so.0 libxml2.so.2"
RDEPENDS_hwloc-plugins = "glibc libX11 libXNVCtrl libXext libpciaccess libtool-ltdl libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hwloc-plugins-1.11.9-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/hwloc-1.11.9-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/hwloc-libs-1.11.9-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hwloc-devel-1.11.9-3.el8.aarch64.rpm \
          "

SRC_URI[hwloc.sha256sum] = "eb06a4393de1b0d6a1d62ee738d2622700c219d2509f336d3366cf24cf6e9528"
SRC_URI[hwloc-devel.sha256sum] = "53935d0d9a8fe27dc00ded930d0ffc53cd39436d046e162887c969faae083136"
SRC_URI[hwloc-libs.sha256sum] = "09adc5e5fcaf2ee4789e8154074b26a33223e6326e4f3d9b5e99e4b00f217269"
SRC_URI[hwloc-plugins.sha256sum] = "b7ae32f4b373ee18553b4dcb752efa43b0cb67e86027f3d51fb101c18edcacf9"
