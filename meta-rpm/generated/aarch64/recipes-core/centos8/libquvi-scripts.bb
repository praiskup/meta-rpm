SUMMARY = "generated recipe based on libquvi-scripts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_libquvi-scripts = "lua-expat lua-json lua-socket pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libquvi-scripts-0.9.20131130-9.el8.noarch.rpm \
          "

SRC_URI[libquvi-scripts.sha256sum] = "5f33cc40bb635b297b22b1b6987001897545d9d3076333768deb4f3c02c66561"
