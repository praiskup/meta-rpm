SUMMARY = "generated recipe based on libnetfilter_cthelper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_cthelper = "libnetfilter_cthelper.so.0"
RPM_SONAME_REQ_libnetfilter_cthelper = "ld-linux-aarch64.so.1 libc.so.6 libmnl.so.0"
RDEPENDS_libnetfilter_cthelper = "glibc libmnl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnetfilter_cthelper-1.0.0-15.el8.aarch64.rpm \
          "

SRC_URI[libnetfilter_cthelper.sha256sum] = "349c22e24c7164c5edab478f4c49d33b188b96747d5fb518e3c556479184a912"
