SUMMARY = "generated recipe based on cyrus-imapd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib e2fsprogs glib-2.0 icu jansson krb5-libs libgcc libical libnghttp2 libpcre libpq libxml2 lm-sensors mariadb-connector-c net-snmp openldap openssl perl pkgconfig-native rpm sqlite3 zlib"
RPM_SONAME_PROV_cyrus-imapd = "libcyrus.so.0 libcyrus_imap.so.0 libcyrus_min.so.0 libcyrus_sieve.so.0"
RPM_SONAME_REQ_cyrus-imapd = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcyrus.so.0 libcyrus_imap.so.0 libcyrus_min.so.0 libcyrus_sieve.so.0 libdl.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 libical.so.3 libicalss.so.3 libicalvcal.so.3 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libjansson.so.4 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 liblber-2.4.so.2 libldap-2.4.so.2 libm.so.6 libmariadb.so.3 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libnghttp2.so.14 libpcre.so.1 libpcreposix.so.0 libpq.so.5 libpthread.so.0 librpm.so.8 librpmio.so.8 libsasl2.so.3 libsensors.so.4 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_cyrus-imapd = "bash cyrus-imapd-utils cyrus-sasl-lib file glibc jansson krb5-libs libcom_err libdb-utils libgcc libical libicu libnghttp2 libpq libstdc++ libxml2 lm_sensors-libs mariadb-connector-c net-snmp-agent-libs net-snmp-libs openldap openssl-libs pcre perl-interpreter perl-libs rpm-libs shadow-utils sqlite-libs sscg systemd zlib"
RPM_SONAME_REQ_cyrus-imapd-utils = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcyrus.so.0 libcyrus_imap.so.0 libcyrus_min.so.0 libcyrus_sieve.so.0 libdl.so.2 libgssapi_krb5.so.2 libical.so.3 libicalss.so.3 libicalvcal.so.3 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libjansson.so.4 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 libm.so.6 libmariadb.so.3 libnghttp2.so.14 libpcre.so.1 libpcreposix.so.0 libperl.so.5.26 libpq.so.5 libpthread.so.0 libsasl2.so.3 libsqlite3.so.0 libssl.so.1.1 libxml2.so.2 libz.so.1"
RDEPENDS_cyrus-imapd-utils = "bash cyrus-imapd cyrus-sasl-lib glibc jansson krb5-libs libcom_err libical libicu libnghttp2 libpq libxml2 mariadb-connector-c openssl-libs pcre perl-Carp perl-Encode perl-Exporter perl-File-Path perl-Getopt-Long perl-IO perl-JSON perl-MIME-Base64 perl-Net-Server perl-URI perl-Unix-Syslog perl-constant perl-interpreter perl-libs sqlite-libs zlib"
RPM_SONAME_REQ_cyrus-imapd-vzic = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0"
RDEPENDS_cyrus-imapd-vzic = "cyrus-imapd glib2 glibc perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cyrus-imapd-3.0.7-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cyrus-imapd-utils-3.0.7-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cyrus-imapd-vzic-3.0.7-16.el8.aarch64.rpm \
          "

SRC_URI[cyrus-imapd.sha256sum] = "bc0fb3c325ef3886a35005cb6277924b86fc46437cc3d536718773ed47844f11"
SRC_URI[cyrus-imapd-utils.sha256sum] = "9870a62e48161c6595e0e59fa3585184b98d4edada8b9195a0d39f4ec8106cca"
SRC_URI[cyrus-imapd-vzic.sha256sum] = "1105e036d89baccfbdd908e7fb12378840d7d1b0af4f322fae8561dcd76812a1"
