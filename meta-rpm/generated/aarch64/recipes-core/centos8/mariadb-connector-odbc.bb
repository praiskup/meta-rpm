SUMMARY = "generated recipe based on mariadb-connector-odbc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "mariadb-connector-c pkgconfig-native unixodbc"
RPM_SONAME_PROV_mariadb-connector-odbc = "libmaodbc.so"
RPM_SONAME_REQ_mariadb-connector-odbc = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libmariadb.so.3 libodbcinst.so.2"
RDEPENDS_mariadb-connector-odbc = "glibc mariadb-connector-c unixODBC"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mariadb-connector-odbc-3.0.7-1.el8.aarch64.rpm \
          "

SRC_URI[mariadb-connector-odbc.sha256sum] = "cc24881aeabf23fe93322134266aa0e890d72db20042dbb6c1b9fca60bf0ce96"
