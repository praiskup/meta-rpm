SUMMARY = "generated recipe based on pcre2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "pcre2"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_pcre2 = "libpcre2-8.so.0 libpcre2-posix.so.2"
RPM_SONAME_REQ_pcre2 = "ld-linux-aarch64.so.1 libc.so.6 libpcre2-8.so.0 libpthread.so.0"
RDEPENDS_pcre2 = "glibc"
RPM_SONAME_REQ_pcre2-devel = "libpcre2-16.so.0 libpcre2-32.so.0 libpcre2-8.so.0 libpcre2-posix.so.2"
RPROVIDES_pcre2-devel = "pcre2-dev (= 10.32)"
RDEPENDS_pcre2-devel = "bash pcre2 pcre2-utf16 pcre2-utf32 pkgconf-pkg-config"
RPM_SONAME_PROV_pcre2-utf16 = "libpcre2-16.so.0"
RPM_SONAME_REQ_pcre2-utf16 = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_pcre2-utf16 = "glibc"
RPM_SONAME_PROV_pcre2-utf32 = "libpcre2-32.so.0"
RPM_SONAME_REQ_pcre2-utf32 = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_pcre2-utf32 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre2-10.32-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre2-devel-10.32-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre2-utf16-10.32-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre2-utf32-10.32-1.el8.aarch64.rpm \
          "
SRC_URI = "file://pcre2-10.32-1.el8.patch"

SRC_URI[pcre2.sha256sum] = "4bc79516dafe99ec1157d1733d6f5873f1add668f022051794d89f92de5616ee"
SRC_URI[pcre2-devel.sha256sum] = "28b5e82f7ef26e08020fdc2dff4377865374f9e4acae311a5bde428195c69b15"
SRC_URI[pcre2-utf16.sha256sum] = "3a32d08f8686a0a163c906dd12a3b3d11bd144745dbb1d867dad34643884daf4"
SRC_URI[pcre2-utf32.sha256sum] = "3e31cf5a4b8bc716aa31e4bd47cc8c22258bcc5d8cf32ef5dd0d4e16e464acb5"
