SUMMARY = "generated recipe based on python-humanize srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-humanize = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-humanize-0.5.1-13.el8.noarch.rpm \
          "

SRC_URI[python3-humanize.sha256sum] = "bb0532493add26389aadab718c9cd3e751716aea34e33a515d0fee6bab341de4"
