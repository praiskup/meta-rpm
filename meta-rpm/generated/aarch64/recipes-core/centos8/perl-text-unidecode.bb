SUMMARY = "generated recipe based on perl-Text-Unidecode srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Unidecode = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Text-Unidecode-1.30-5.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Unidecode.sha256sum] = "a00ebe93cd3bafef19af5c1fdd5dbd4d17c723a3e8d000b993551905f4878233"
