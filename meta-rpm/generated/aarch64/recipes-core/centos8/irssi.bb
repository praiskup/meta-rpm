SUMMARY = "generated recipe based on irssi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libxcrypt ncurses openssl perl pkgconfig-native"
RPM_SONAME_PROV_irssi = "libirc_proxy.so"
RPM_SONAME_REQ_irssi = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libglib-2.0.so.0 libgmodule-2.0.so.0 libm.so.6 libperl.so.5.26 libpthread.so.0 libresolv.so.2 libssl.so.1.1 libtinfo.so.6 libutil.so.1"
RDEPENDS_irssi = "glib2 glibc libxcrypt ncurses-libs openssl-libs perl-Carp perl-Exporter perl-interpreter perl-libs"
RPROVIDES_irssi-devel = "irssi-dev (= 1.1.1)"
RDEPENDS_irssi-devel = "irssi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/irssi-1.1.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/irssi-devel-1.1.1-3.el8.aarch64.rpm \
          "

SRC_URI[irssi.sha256sum] = "be2a7d3f36706577dc521913e838ebe9996be6775780a5efdb154aecf0c49746"
SRC_URI[irssi-devel.sha256sum] = "eb0ba7bbf1c329b79c62ace463eaa18d3c78c10fa9894ad1da2a04c2e1589f5a"
