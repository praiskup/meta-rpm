SUMMARY = "generated recipe based on yasm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_yasm = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_yasm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/yasm-1.3.0-7.el8.aarch64.rpm \
          "

SRC_URI[yasm.sha256sum] = "f902f6d7902c1a199864b3349a78f25da186b4940bc4b6969ce09fbe23ae2860"
