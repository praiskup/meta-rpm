SUMMARY = "generated recipe based on xorg-x11-drv-evdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevdev mtdev pkgconfig-native systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-evdev = "ld-linux-aarch64.so.1 libc.so.6 libevdev.so.2 libmtdev.so.1 libudev.so.1"
RDEPENDS_xorg-x11-drv-evdev = "glibc libevdev mtdev systemd-libs xkeyboard-config xorg-x11-server-Xorg"
RPROVIDES_xorg-x11-drv-evdev-devel = "xorg-x11-drv-evdev-dev (= 2.10.6)"
RDEPENDS_xorg-x11-drv-evdev-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-evdev-2.10.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-evdev-devel-2.10.6-2.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-evdev.sha256sum] = "e567a5fffc786ec7ac431a6c92efb98ae743ca2938b454c768aff706b292f1aa"
SRC_URI[xorg-x11-drv-evdev-devel.sha256sum] = "67c026058cd93d2424f21585ae2085a77fab3eddd69bac722af04724d8cbc8ba"
