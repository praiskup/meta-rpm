SUMMARY = "generated recipe based on python-hypothesis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-hypothesis = "platform-python platform-python-coverage python3-attrs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-hypothesis-3.44.24-6.el8.noarch.rpm \
          "

SRC_URI[python3-hypothesis.sha256sum] = "af01595aa7e8de1fef8a0397635be6bc27a270088e6890ffa859a51683df9970"
