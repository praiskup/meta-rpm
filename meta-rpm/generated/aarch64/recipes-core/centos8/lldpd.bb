SUMMARY = "generated recipe based on lldpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevent libxml2 lm-sensors net-snmp openssl pkgconfig-native readline rpm"
RPM_SONAME_PROV_lldpd = "liblldpctl.so.4"
RPM_SONAME_REQ_lldpd = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libevent-2.1.so.6 liblldpctl.so.4 libm.so.6 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libreadline.so.7 librpm.so.8 librpmio.so.8 libsensors.so.4 libssl.so.1.1 libxml2.so.2"
RDEPENDS_lldpd = "bash glibc libevent libxml2 lm_sensors-libs net-snmp-agent-libs net-snmp-libs openssl-libs readline rpm-libs shadow-utils systemd"
RPM_SONAME_REQ_lldpd-devel = "liblldpctl.so.4"
RPROVIDES_lldpd-devel = "lldpd-dev (= 1.0.1)"
RDEPENDS_lldpd-devel = "lldpd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lldpd-1.0.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lldpd-devel-1.0.1-2.el8.aarch64.rpm \
          "

SRC_URI[lldpd.sha256sum] = "59596be9aa1d933a502db8cc4092caa53e065e1ecb224d3edde77be5877284b8"
SRC_URI[lldpd-devel.sha256sum] = "139994551e783edf06fe1858f1e963c7ccb46718eb6de52dc13758ff16b4accd"
