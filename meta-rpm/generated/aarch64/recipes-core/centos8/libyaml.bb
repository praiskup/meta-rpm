SUMMARY = "generated recipe based on libyaml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libyaml = "libyaml-0.so.2"
RPM_SONAME_REQ_libyaml = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libyaml = "glibc"
RPROVIDES_libyaml-devel = "libyaml-dev (= 0.1.7)"
RDEPENDS_libyaml-devel = "libyaml pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libyaml-0.1.7-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libyaml-devel-0.1.7-5.el8.aarch64.rpm \
          "

SRC_URI[libyaml.sha256sum] = "30327c94b9729602f0b4dd73ff67edc2b7269af782182a2c02f44246ffe7f10f"
SRC_URI[libyaml-devel.sha256sum] = "3399d15d85a5655308fa0f05713afb2b0268ca43fd059c2f5ad411d4cc82875a"
