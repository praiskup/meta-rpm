SUMMARY = "generated recipe based on perl-Taint-Runtime srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Taint-Runtime = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Taint-Runtime = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Taint-Runtime-0.03-32.el8.aarch64.rpm \
          "

SRC_URI[perl-Taint-Runtime.sha256sum] = "395c130fa48a9684323eb200e30f8270101591b34828f90a2811b247abca9226"
