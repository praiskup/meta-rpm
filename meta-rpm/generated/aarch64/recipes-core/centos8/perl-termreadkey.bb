SUMMARY = "generated recipe based on perl-TermReadKey srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-TermReadKey = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-TermReadKey = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-TermReadKey-2.37-7.el8.aarch64.rpm \
          "

SRC_URI[perl-TermReadKey.sha256sum] = "22c5761a5337e189c654e577ab4a8882969ddb56df8d4bed20a48be21086c58c"
