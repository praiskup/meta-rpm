SUMMARY = "generated recipe based on perl-Data-OptList srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Data-OptList = "perl-Params-Util perl-Scalar-List-Utils perl-Sub-Install perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Data-OptList-0.110-6.el8.noarch.rpm \
          "

SRC_URI[perl-Data-OptList.sha256sum] = "59a8dc1f9c4e129fac3b62c9fe0ed95bdb1d98480444015c9bc5da5ee1cc9733"
