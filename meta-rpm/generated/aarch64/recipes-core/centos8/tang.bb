SUMMARY = "generated recipe based on tang srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "http-parser jansson jose libgcc pkgconfig-native"
RPM_SONAME_REQ_tang = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libhttp_parser.so.2 libjansson.so.4 libjose.so.0"
RDEPENDS_tang = "bash coreutils glibc grep http-parser jansson jose libgcc libjose sed shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tang-7-5.el8.aarch64.rpm \
          "

SRC_URI[tang.sha256sum] = "9a831a7634a2c480307838fc4b16d956f9c898714c61df077bfa1b1a81141f56"
