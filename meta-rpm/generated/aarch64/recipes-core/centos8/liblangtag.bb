SUMMARY = "generated recipe based on liblangtag srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_PROV_liblangtag = "liblangtag-ext-ldml-t.so liblangtag-ext-ldml-u.so liblangtag.so.1"
RPM_SONAME_REQ_liblangtag = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblangtag.so.1 libxml2.so.2"
RDEPENDS_liblangtag = "glibc liblangtag-data libxml2"
RPM_SONAME_REQ_liblangtag-devel = "liblangtag-gobject.so.0 liblangtag.so.1"
RPROVIDES_liblangtag-devel = "liblangtag-dev (= 0.6.2)"
RDEPENDS_liblangtag-devel = "glib2-devel liblangtag liblangtag-gobject libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_liblangtag-gobject = "liblangtag-gobject.so.0"
RPM_SONAME_REQ_liblangtag-gobject = "libc.so.6 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 liblangtag.so.1 libxml2.so.2"
RDEPENDS_liblangtag-gobject = "glib2 glibc liblangtag libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liblangtag-0.6.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liblangtag-data-0.6.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liblangtag-devel-0.6.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liblangtag-doc-0.6.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liblangtag-gobject-0.6.2-5.el8.aarch64.rpm \
          "

SRC_URI[liblangtag.sha256sum] = "806413c67379484a22e7ebe317eb80bcc8ab1a066c249c466216e58a4ef1c4f9"
SRC_URI[liblangtag-data.sha256sum] = "ae50b9fd0e6b204c8c8f3ac0ede8985f550f3e368fa1ed8c5d607007f256191b"
SRC_URI[liblangtag-devel.sha256sum] = "333f79ce27cc0ff10317e3037a8432785dcc35314c51f759d989aba9233825aa"
SRC_URI[liblangtag-doc.sha256sum] = "5a5942091765b2c01b88537a9b6aa9e5c86c8cef3256ea7ce1a2cb300d8aa989"
SRC_URI[liblangtag-gobject.sha256sum] = "774ecc238771ce3e5a09c9dac1905e14ffd9efe5c190188e3c74e7fe74a53243"
