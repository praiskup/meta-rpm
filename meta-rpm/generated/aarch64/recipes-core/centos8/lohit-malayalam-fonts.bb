SUMMARY = "generated recipe based on lohit-malayalam-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-malayalam-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-malayalam-fonts-2.92.2-3.el8.noarch.rpm \
          "

SRC_URI[lohit-malayalam-fonts.sha256sum] = "9670d20e77599b55cc495faf6d5e2be0ab056b1d34a4966f16381c4bb18ea5f9"
