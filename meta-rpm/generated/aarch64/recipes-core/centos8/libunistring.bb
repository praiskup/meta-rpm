SUMMARY = "generated recipe based on libunistring srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libunistring = "libunistring.so.2"
RPM_SONAME_REQ_libunistring = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libunistring = "glibc info"
RPM_SONAME_REQ_libunistring-devel = "libunistring.so.2"
RPROVIDES_libunistring-devel = "libunistring-dev (= 0.9.9)"
RDEPENDS_libunistring-devel = "bash libunistring"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libunistring-0.9.9-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libunistring-devel-0.9.9-3.el8.aarch64.rpm \
          "

SRC_URI[libunistring.sha256sum] = "707429ccb3223628d55097a162cd0d3de1bd00b48800677c1099931b0f019e80"
SRC_URI[libunistring-devel.sha256sum] = "ad1b3daf8d25aa16fd4dd7133065f81aeca7ccc63843bcfd85552dbe1a4c0902"
