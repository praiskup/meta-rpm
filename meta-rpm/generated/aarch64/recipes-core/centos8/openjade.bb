SUMMARY = "generated recipe based on openjade srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc opensp pkgconfig-native"
RPM_SONAME_PROV_openjade = "libogrove.so.0 libospgrove.so.0 libostyle.so.0"
RPM_SONAME_REQ_openjade = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libogrove.so.0 libosp.so.5 libospgrove.so.0 libostyle.so.0 libstdc++.so.6"
RDEPENDS_openjade = "bash glibc libgcc libstdc++ opensp sgml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openjade-1.3.2-57.el8.aarch64.rpm \
          "

SRC_URI[openjade.sha256sum] = "8cfec0fe66718f248bbe97f0fb9163d177e09f76b7cdc12de11db4385b5b9185"
