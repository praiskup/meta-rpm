SUMMARY = "generated recipe based on libmspack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmspack = "libmspack.so.0"
RPM_SONAME_REQ_libmspack = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libmspack = "glibc"
RPM_SONAME_REQ_libmspack-devel = "libmspack.so.0"
RPROVIDES_libmspack-devel = "libmspack-dev (= 0.7)"
RDEPENDS_libmspack-devel = "libmspack pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmspack-0.7-0.3.alpha.el8.4.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmspack-devel-0.7-0.3.alpha.el8.4.aarch64.rpm \
          "

SRC_URI[libmspack.sha256sum] = "4a95c50b13249e60b72af6f7fc8866d3a1cfbdcfed77818a90a321f0c44ae714"
SRC_URI[libmspack-devel.sha256sum] = "a8f397d17a11ec5ca09eca3131bd8d82e16d37f63c5f4a89e9d05a39183b4caf"
