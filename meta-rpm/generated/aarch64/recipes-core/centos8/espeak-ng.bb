SUMMARY = "generated recipe based on espeak-ng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pcaudiolib pkgconfig-native"
RPM_SONAME_PROV_espeak-ng = "libespeak-ng.so.1"
RPM_SONAME_REQ_espeak-ng = "ld-linux-aarch64.so.1 libc.so.6 libespeak-ng.so.1 libm.so.6 libpcaudio.so.0 libpthread.so.0"
RDEPENDS_espeak-ng = "glibc pcaudiolib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/espeak-ng-1.49.2-4.el8.aarch64.rpm \
          "

SRC_URI[espeak-ng.sha256sum] = "0074294ef2177876e6a5ed38d08f75a4a0e0a593652ceed5dc4b110e1fb91ba7"
