SUMMARY = "generated recipe based on plexus-interpolation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-interpolation = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_plexus-interpolation-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-interpolation-1.22-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-interpolation-javadoc-1.22-9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-interpolation.sha256sum] = "ec858ddcf0cf78bcb418d461b810a024fe601e7639357e8cbb9b59d92388684b"
SRC_URI[plexus-interpolation-javadoc.sha256sum] = "a2135102d38f44aeec1cb3727741f0904491d91a724430b40a1d15b1b7d7c4d6"
