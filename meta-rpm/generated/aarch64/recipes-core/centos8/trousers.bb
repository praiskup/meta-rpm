SUMMARY = "generated recipe based on trousers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_trousers = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_trousers = "bash glibc openssl-libs shadow-utils systemd trousers-lib"
RPM_SONAME_REQ_trousers-devel = "libtspi.so.1"
RPROVIDES_trousers-devel = "trousers-dev (= 0.3.14)"
RDEPENDS_trousers-devel = "trousers-lib"
RPM_SONAME_PROV_trousers-lib = "libtspi.so.1"
RPM_SONAME_REQ_trousers-lib = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libssl.so.1.1"
RDEPENDS_trousers-lib = "glibc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/trousers-0.3.14-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/trousers-lib-0.3.14-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/trousers-devel-0.3.14-4.el8.aarch64.rpm \
          "

SRC_URI[trousers.sha256sum] = "b9d9438d1fd54ff9a63190eb3c24f87eaf250502cf40f8ec263cdef443f400f4"
SRC_URI[trousers-devel.sha256sum] = "f796d47131dc5ab324d50bd9ddffb20d0a53f96163aed8d7ef91b91c701b5c3e"
SRC_URI[trousers-lib.sha256sum] = "37cc33bca2dce51b6188742303f91c037aec09586fa1e6d239f654186212f4ab"
