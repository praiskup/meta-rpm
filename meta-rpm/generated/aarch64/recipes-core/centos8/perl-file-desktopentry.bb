SUMMARY = "generated recipe based on perl-File-DesktopEntry srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-DesktopEntry = "perl-Carp perl-Encode perl-File-BaseDir perl-File-Path perl-PathTools perl-URI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-DesktopEntry-0.22-7.el8.noarch.rpm \
          "

SRC_URI[perl-File-DesktopEntry.sha256sum] = "6dbb4d629a5779a42a5e2218375fe4e85f7db99566fc1a057ae687bb952c0f18"
