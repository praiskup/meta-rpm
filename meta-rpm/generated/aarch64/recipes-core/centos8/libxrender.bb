SUMMARY = "generated recipe based on libXrender srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXrender = "libXrender.so.1"
RPM_SONAME_REQ_libXrender = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libXrender = "glibc libX11"
RPM_SONAME_REQ_libXrender-devel = "libXrender.so.1"
RPROVIDES_libXrender-devel = "libXrender-dev (= 0.9.10)"
RDEPENDS_libXrender-devel = "libX11-devel libXrender pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXrender-0.9.10-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXrender-devel-0.9.10-7.el8.aarch64.rpm \
          "

SRC_URI[libXrender.sha256sum] = "0a7cb86aeedf8d6665a37a3515238c04f8780caaab303d1a7263b66b9a6cb7ed"
SRC_URI[libXrender-devel.sha256sum] = "3cabdcb025c96028eba9a734f56ecf9b1c235e6552f02465e7c723f8e7d24326"
