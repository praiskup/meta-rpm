SUMMARY = "generated recipe based on libdap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc libtirpc libuuid libxml2 openssl pkgconfig-native"
RPM_SONAME_PROV_libdap = "libdap.so.25 libdapclient.so.6 libdapserver.so.7"
RPM_SONAME_REQ_libdap = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdap.so.25 libdapclient.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libtirpc.so.3 libuuid.so.1 libxml2.so.2"
RDEPENDS_libdap = "glibc libcurl libgcc libstdc++ libtirpc libuuid libxml2 openssl-libs"
RPM_SONAME_REQ_libdap-devel = "libdap.so.25 libdapclient.so.6 libdapserver.so.7"
RPROVIDES_libdap-devel = "libdap-dev (= 3.19.1)"
RDEPENDS_libdap-devel = "automake bash libcurl-devel libdap libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdap-3.19.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdap-devel-3.19.1-2.el8.aarch64.rpm \
          "

SRC_URI[libdap.sha256sum] = "d57a5f6ef1b70a448bdc09c0a6debd7dc687731f6612e5576c9f57b7246f227b"
SRC_URI[libdap-devel.sha256sum] = "b9512721271230cdc2d8e4072429739fcacf195914ee632af1f3ac5248b4b190"
