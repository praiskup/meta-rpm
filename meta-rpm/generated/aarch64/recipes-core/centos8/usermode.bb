SUMMARY = "generated recipe based on usermode srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 libblkid libice libselinux libsm libuser pam pango pkgconfig-native startup-notification"
RPM_SONAME_REQ_usermode = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libpam.so.0 libpam_misc.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_usermode = "glib2 glibc libselinux libuser pam passwd util-linux"
RPM_SONAME_REQ_usermode-gtk = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libatk-1.0.so.0 libblkid.so.1 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libstartup-notification-1.so.0"
RDEPENDS_usermode-gtk = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libICE libSM libblkid pango startup-notification usermode"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/usermode-gtk-1.113-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/usermode-1.113-1.el8.aarch64.rpm \
          "

SRC_URI[usermode.sha256sum] = "1865d83df30869bb74fde21b6c31b5440c34b36323a0001fcf140698a9746c40"
SRC_URI[usermode-gtk.sha256sum] = "9d1d04c936d099ee3a4456837a5a9a169b8164102ac280784c3a6115ef8d576c"
