SUMMARY = "generated recipe based on jtidy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jtidy = "java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools xml-commons-apis"
RDEPENDS_jtidy-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jtidy-1.0-0.28.20100930svn1125.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jtidy-javadoc-1.0-0.28.20100930svn1125.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jtidy.sha256sum] = "be36b2afb68cc0b130931aec8b65fc7d4a078f8553871677931400dc04c49365"
SRC_URI[jtidy-javadoc.sha256sum] = "6b8a3e1f41916f1e3f1110f128c0f7d220cbd0fd3cd88b2e001c0288b4bccaf8"
