SUMMARY = "generated recipe based on python-distro srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-distro = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-distro-1.4.0-2.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-distro.sha256sum] = "889be1e35217912c6b77551500f01850a589c5b7811aa5aceda16847677c8fd9"
