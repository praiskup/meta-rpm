SUMMARY = "generated recipe based on alsa-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native"
RPM_SONAME_REQ_alsa-tools-firmware = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_alsa-tools-firmware = "alsa-firmware alsa-lib bash fxload glibc systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-tools-firmware-1.1.6-1.el8.aarch64.rpm \
          "

SRC_URI[alsa-tools-firmware.sha256sum] = "660b3971d95ad7491589c0149082a90e51b25e4efbe6b07fbbf7e37e17a7e452"
