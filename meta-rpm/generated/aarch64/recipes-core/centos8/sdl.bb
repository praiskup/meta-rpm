SUMMARY = "generated recipe based on SDL srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib libx11 libxext libxrandr libxrender mesa mesa-libglu pkgconfig-native"
RPM_SONAME_PROV_SDL = "libSDL-1.2.so.0"
RPM_SONAME_REQ_SDL = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_SDL = "glibc"
RPM_SONAME_REQ_SDL-devel = "libSDL-1.2.so.0"
RPROVIDES_SDL-devel = "SDL-dev (= 1.2.15)"
RDEPENDS_SDL-devel = "SDL alsa-lib-devel bash libX11-devel libXext-devel libXrandr-devel libXrender-devel mesa-libGL-devel mesa-libGLU-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/SDL-1.2.15-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/SDL-devel-1.2.15-37.el8.aarch64.rpm \
          "

SRC_URI[SDL.sha256sum] = "3b923913ad4a2f629eb9d276b9e298ae91f5827f25ea5dc64d9c9a009e82c3c0"
SRC_URI[SDL-devel.sha256sum] = "f761eda72205a335de6054fe63e730f55f8c7399d203256239b5d8b27654a38c"
