SUMMARY = "generated recipe based on xcb-util-keysyms srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util-keysyms = "libxcb-keysyms.so.1"
RPM_SONAME_REQ_xcb-util-keysyms = "libc.so.6 libxcb.so.1"
RDEPENDS_xcb-util-keysyms = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-keysyms-devel = "libxcb-keysyms.so.1"
RPROVIDES_xcb-util-keysyms-devel = "xcb-util-keysyms-dev (= 0.4.0)"
RDEPENDS_xcb-util-keysyms-devel = "libxcb-devel pkgconf-pkg-config xcb-util-keysyms"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xcb-util-keysyms-0.4.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xcb-util-keysyms-devel-0.4.0-7.el8.aarch64.rpm \
          "

SRC_URI[xcb-util-keysyms.sha256sum] = "35ecbba5f262f0b768fb8b10a4d3ad6533cb10b289404ac48937493817abf112"
SRC_URI[xcb-util-keysyms-devel.sha256sum] = "387e8fbeb8046cedc1214f7295fb615e106fb2dbf361275ed14585473e978b4a"
