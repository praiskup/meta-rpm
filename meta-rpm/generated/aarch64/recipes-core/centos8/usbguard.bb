SUMMARY = "generated recipe based on usbguard srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-glib dbus-libs glib-2.0 libgcc libgcrypt libgpg-error libqb pkgconfig-native protobuf"
RPM_SONAME_PROV_usbguard = "libusbguard.so.0"
RPM_SONAME_REQ_usbguard = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libprotobuf.so.15 libpthread.so.0 libqb.so.0 libstdc++.so.6 libusbguard.so.0"
RDEPENDS_usbguard = "audit-libs bash glibc libgcc libgcrypt libgpg-error libqb libstdc++ protobuf systemd"
RPM_SONAME_REQ_usbguard-dbus = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libm.so.6 libprotobuf.so.15 libpthread.so.0 libqb.so.0 libstdc++.so.6 libusbguard.so.0"
RDEPENDS_usbguard-dbus = "bash dbus dbus-glib dbus-libs glib2 glibc libgcc libgcrypt libgpg-error libqb libstdc++ polkit protobuf usbguard"
RPM_SONAME_REQ_usbguard-tools = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libprotobuf.so.15 libpthread.so.0 libqb.so.0 libstdc++.so.6 libusbguard.so.0"
RDEPENDS_usbguard-tools = "glibc libgcc libgcrypt libgpg-error libqb libstdc++ protobuf usbguard"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/usbguard-0.7.4-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/usbguard-dbus-0.7.4-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/usbguard-tools-0.7.4-4.el8.aarch64.rpm \
          "

SRC_URI[usbguard.sha256sum] = "1300962ee71ffe3199f3d666db827aed44f2ca32f54cb556d5141fbe5f52d23d"
SRC_URI[usbguard-dbus.sha256sum] = "ae5f1d7e26b18d0725708c1790578345634c2d591cae1517fd14bd80a9d0721e"
SRC_URI[usbguard-tools.sha256sum] = "50c5e2989169dcc1796f33e659f339cbc3f6cc8667820a1a159bf8b5817f0d4b"
