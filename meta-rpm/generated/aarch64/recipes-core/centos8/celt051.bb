SUMMARY = "generated recipe based on celt051 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libogg pkgconfig-native"
RPM_SONAME_PROV_celt051 = "libcelt051.so.0"
RPM_SONAME_REQ_celt051 = "ld-linux-aarch64.so.1 libc.so.6 libcelt051.so.0 libm.so.6 libogg.so.0"
RDEPENDS_celt051 = "glibc libogg"
RPM_SONAME_REQ_celt051-devel = "libcelt051.so.0"
RPROVIDES_celt051-devel = "celt051-dev (= 0.5.1.3)"
RDEPENDS_celt051-devel = "celt051 libogg-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/celt051-0.5.1.3-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/celt051-devel-0.5.1.3-15.el8.aarch64.rpm \
          "

SRC_URI[celt051.sha256sum] = "b8eadec2737f3bd6eaa924901820229d4ca308b72039c0885afc6040d7bf08ec"
SRC_URI[celt051-devel.sha256sum] = "88d98d1488b38bd8ba1ce51c91c481006c5971906c9c999c65f826b23302705f"
