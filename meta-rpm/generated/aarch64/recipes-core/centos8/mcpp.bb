SUMMARY = "generated recipe based on mcpp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmcpp = "libmcpp.so.0"
RPM_SONAME_REQ_libmcpp = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libmcpp = "glibc"
RPM_SONAME_REQ_mcpp = "libc.so.6 libmcpp.so.0"
RDEPENDS_mcpp = "glibc libmcpp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmcpp-2.7.2-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mcpp-2.7.2-20.el8.aarch64.rpm \
          "

SRC_URI[libmcpp.sha256sum] = "c39c15205b506daa46180f2bebf8700af2ae8c43aecdbe8276a98edda1cd9854"
SRC_URI[mcpp.sha256sum] = "378e079746b7d9d31db73785ad6b116a1b8dfe11a4c9e3d9f0d558b984c98772"
