SUMMARY = "generated recipe based on pcsc-lite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_pcsc-lite = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 librt.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_pcsc-lite = "bash glib2 glibc pcsc-lite-ccid pcsc-lite-libs polkit polkit-libs systemd systemd-libs"
RPM_SONAME_PROV_pcsc-lite-devel = "libpcscspy.so.0"
RPM_SONAME_REQ_pcsc-lite-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcsclite.so.1 libpcscspy.so.0 librt.so.1"
RPROVIDES_pcsc-lite-devel = "pcsc-lite-dev (= 1.8.23)"
RDEPENDS_pcsc-lite-devel = "glibc pcsc-lite-libs pkgconf-pkg-config platform-python"
RDEPENDS_pcsc-lite-doc = "pcsc-lite-libs"
RPM_SONAME_PROV_pcsc-lite-libs = "libpcsclite.so.1"
RPM_SONAME_REQ_pcsc-lite-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_pcsc-lite-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcsc-lite-1.8.23-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcsc-lite-doc-1.8.23-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcsc-lite-libs-1.8.23-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pcsc-lite-devel-1.8.23-3.el8.aarch64.rpm \
          "

SRC_URI[pcsc-lite.sha256sum] = "21eadf1353eed372cd5acc8d6b3d64af605273f8167ac9e963b141a8c3bcbfc1"
SRC_URI[pcsc-lite-devel.sha256sum] = "2ffa7ea7b07e85f3510c3ff70de05a3ce2764d081c3f6b87108865ca958ab749"
SRC_URI[pcsc-lite-doc.sha256sum] = "683982b7e6db44d488bb46b878771d9274ecad9fe3e31305abdeb2fb3335aae7"
SRC_URI[pcsc-lite-libs.sha256sum] = "eb0b7752cfed14f5be4478865a584546573c51dd59f21738eefbb912adbbcdf4"
