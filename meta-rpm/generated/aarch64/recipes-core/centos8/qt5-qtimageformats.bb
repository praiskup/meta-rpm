SUMMARY = "generated recipe based on qt5-qtimageformats srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jasper libgcc libglvnd libmng libwebp pkgconfig-native qt5-qtbase tiff"
RPM_SONAME_REQ_qt5-qtimageformats = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libc.so.6 libgcc_s.so.1 libjasper.so.4 libm.so.6 libmng.so.2 libpthread.so.0 libstdc++.so.6 libtiff.so.5 libwebp.so.7 libwebpdemux.so.2"
RDEPENDS_qt5-qtimageformats = "glibc jasper-libs libgcc libglvnd-glx libmng libstdc++ libtiff libwebp qt5-qtbase qt5-qtbase-gui"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtimageformats-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtimageformats.sha256sum] = "f3b83c3a54ee6a292fbd8ab50a24a0d9378205e58a8bdb5fd529b8e349c3b7e2"
