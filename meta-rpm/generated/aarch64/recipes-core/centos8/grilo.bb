SUMMARY = "generated recipe based on grilo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gtk+3 liboauth libsoup-2.4 libxml2 pkgconfig-native totem-pl-parser"
RPM_SONAME_PROV_grilo = "libgrilo-0.3.so.0 libgrlnet-0.3.so.0 libgrlpls-0.3.so.0"
RPM_SONAME_REQ_grilo = "ld-linux-aarch64.so.1 libc.so.6 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgrilo-0.3.so.0 libgtk-3.so.0 liboauth.so.0 libpthread.so.0 libsoup-2.4.so.1 libtotem-plparser.so.18"
RDEPENDS_grilo = "glib2 glibc gtk3 liboauth libsoup totem-pl-parser"
RPM_SONAME_REQ_grilo-devel = "libgrilo-0.3.so.0 libgrlnet-0.3.so.0 libgrlpls-0.3.so.0"
RPROVIDES_grilo-devel = "grilo-dev (= 0.3.6)"
RDEPENDS_grilo-devel = "glib2-devel grilo libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grilo-0.3.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/grilo-devel-0.3.6-2.el8.aarch64.rpm \
          "

SRC_URI[grilo.sha256sum] = "141cb66b7bdfcd45d24d5860ca9f0c0716f50180a03928a7a781d2b5a7d423b9"
SRC_URI[grilo-devel.sha256sum] = "cef1c9053d28d07d9241b0d463f0330c73023865495c3bfb23f62d677aa70806"
