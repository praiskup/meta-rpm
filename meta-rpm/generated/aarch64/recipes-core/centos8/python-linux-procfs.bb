SUMMARY = "generated recipe based on python-linux-procfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-linux-procfs = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-linux-procfs-0.6-7.el8.noarch.rpm \
          "

SRC_URI[python3-linux-procfs.sha256sum] = "5226e65270b5a04b649ce2b7a74ed57ee390a8120a6057fe77fa535740992e27"
