SUMMARY = "generated recipe based on vte291 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gnutls gtk+3 libgcc libpcre2 pango pkgconfig-native zlib"
RPM_SONAME_PROV_vte291 = "libvte-2.91.so.0"
RPM_SONAME_REQ_vte291 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpcre2-8.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_vte291 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnutls gtk3 libgcc libstdc++ pango pcre2 vte-profile zlib"
RPM_SONAME_REQ_vte291-devel = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libstdc++.so.6 libvte-2.91.so.0"
RPROVIDES_vte291-devel = "vte291-dev (= 0.52.2)"
RDEPENDS_vte291-devel = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glib2-devel glibc gnutls-devel gtk3 gtk3-devel libgcc libstdc++ pango pango-devel pcre2-devel pkgconf-pkg-config vte291 zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vte-profile-0.52.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vte291-0.52.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/vte291-devel-0.52.2-2.el8.aarch64.rpm \
          "

SRC_URI[vte-profile.sha256sum] = "0bbdb54f346dd40dc7c38e2a2ec5ac72d0b2f47dc64a9d60265fc2fc83225817"
SRC_URI[vte291.sha256sum] = "2e409d39aa8c488ddf9b3a1fc147cd419cc3a72b23e47b0aa5e9de1a1ff08bfb"
SRC_URI[vte291-devel.sha256sum] = "a3b32e3c7cd9f28f8b45210920277d54a2a5b506b08e588c2b10bbd17583ce1f"
