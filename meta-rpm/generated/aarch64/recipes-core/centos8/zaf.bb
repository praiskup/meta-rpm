SUMMARY = "generated recipe based on zaf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-af = "hyphen"
RDEPENDS_hyphen-zu = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-af-0-0.16.20080714svn.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-zu-0-0.16.20080714svn.el8.noarch.rpm \
          "

SRC_URI[hyphen-af.sha256sum] = "57ba04629a553f4ff23f541a205ede3b4ca7b76db2c5966157d5e822550ea37d"
SRC_URI[hyphen-zu.sha256sum] = "fe1ac0a6b5717b0ef259d2a5124562a17a0b808a547560c1859f8c2c2df164ae"
