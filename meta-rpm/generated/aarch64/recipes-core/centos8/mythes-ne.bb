SUMMARY = "generated recipe based on mythes-ne srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-ne = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-ne-1.1-14.el8.noarch.rpm \
          "

SRC_URI[mythes-ne.sha256sum] = "0745ab669e6f78ea053707261d151ef7a3ed1542898c9eaf16d45a35aa9af2a1"
