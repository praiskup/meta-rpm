SUMMARY = "generated recipe based on hunspell-om srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-om = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-om-0.04-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-om.sha256sum] = "dd7694d00e5952385d38a3a126e3872ce4c81145dbe7fadb2c47937c07cd2ebe"
