SUMMARY = "generated recipe based on powermock srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_powermock-api-easymock = "cglib java-1.8.0-openjdk-headless javapackages-filesystem powermock-api-support powermock-common"
RDEPENDS_powermock-api-mockito = "hamcrest-core java-1.8.0-openjdk-headless javapackages-filesystem mockito powermock-api-support powermock-common"
RDEPENDS_powermock-api-support = "java-1.8.0-openjdk-headless javapackages-filesystem powermock-common powermock-core powermock-reflect"
RDEPENDS_powermock-core = "java-1.8.0-openjdk-headless javapackages-filesystem javassist powermock-api-support powermock-common powermock-reflect"
RDEPENDS_powermock-javadoc = "javapackages-filesystem"
RDEPENDS_powermock-junit4 = "java-1.8.0-openjdk-headless javapackages-filesystem junit powermock-common powermock-core powermock-reflect"
RDEPENDS_powermock-reflect = "java-1.8.0-openjdk-headless javapackages-filesystem objenesis powermock-common"
RDEPENDS_powermock-testng = "java-1.8.0-openjdk-headless javapackages-filesystem powermock-common powermock-core powermock-reflect testng"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-api-easymock-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-api-mockito-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-api-support-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-common-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-core-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-javadoc-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-junit4-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-reflect-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/powermock-testng-1.6.5-9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[powermock-api-easymock.sha256sum] = "7ae062d75ae67fd47d4b389fdb21dd961a499f69f443202e61b05f439dad5344"
SRC_URI[powermock-api-mockito.sha256sum] = "6142002d4545d9c618431825480b512e1fbbf9fc912fc80b7807536c844069da"
SRC_URI[powermock-api-support.sha256sum] = "b9e0d12e1dbc4e24ec3518c5d28ba67819306f31e9514f3aac1fcf6a218addb4"
SRC_URI[powermock-common.sha256sum] = "a8f6bd140de914dbccc6343a32890455d7cd2ab8d4581ac249475707963b6b91"
SRC_URI[powermock-core.sha256sum] = "e7da6107faff1747a5a19a8e14cdc904c9d4b5d662f20dc7d29aafc90f7f3c82"
SRC_URI[powermock-javadoc.sha256sum] = "42a4f391dd8377195b747b30db387ee541bdd1b138ffae8b43f5d8b565973e55"
SRC_URI[powermock-junit4.sha256sum] = "e513b82bf6f3b9f3a872dd4a15bd1abdd6c04bef62dfdac740e553905804e1db"
SRC_URI[powermock-reflect.sha256sum] = "4bea4c7cba421948cb11add3edfc82514c9ab421754a85fb4f03406a06784b9c"
SRC_URI[powermock-testng.sha256sum] = "197043a02a980441052f62a7843f2075c99bc15926d91b4a2efe8d77e922612f"
