SUMMARY = "generated recipe based on libidn2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libunistring pkgconfig-native"
RPM_SONAME_REQ_idn2 = "ld-linux-aarch64.so.1 libc.so.6 libidn2.so.0 libunistring.so.2"
RDEPENDS_idn2 = "glibc libidn2 libunistring"
RPM_SONAME_PROV_libidn2 = "libidn2.so.0"
RPM_SONAME_REQ_libidn2 = "ld-linux-aarch64.so.1 libc.so.6 libunistring.so.2"
RDEPENDS_libidn2 = "glibc libunistring"
RPM_SONAME_REQ_libidn2-devel = "libidn2.so.0"
RPROVIDES_libidn2-devel = "libidn2-dev (= 2.2.0)"
RDEPENDS_libidn2-devel = "libidn2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/idn2-2.2.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libidn2-2.2.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libidn2-devel-2.2.0-1.el8.aarch64.rpm \
          "

SRC_URI[idn2.sha256sum] = "b39c5e30ce6fef669f6313eeb286db3bc53a7b9ab30f4fcf6d692c7c9a1e9dc6"
SRC_URI[libidn2.sha256sum] = "b62589101a60a365ef34447cae78f62e6dba560d403dc56c87036709ea00ad88"
SRC_URI[libidn2-devel.sha256sum] = "5fbbae847ff32d6ccc3e7eda37e7964caf68b708f4760c36fb9b2dad23a55d12"
