SUMMARY = "generated recipe based on gc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libatomic-ops libgcc pkgconfig-native"
RPM_SONAME_PROV_gc = "libcord.so.1 libgc.so.1 libgccpp.so.1"
RPM_SONAME_REQ_gc = "ld-linux-aarch64.so.1 libatomic_ops.so.1 libc.so.6 libdl.so.2 libgc.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gc = "glibc libatomic_ops libgcc libstdc++"
RPM_SONAME_REQ_gc-devel = "libcord.so.1 libgc.so.1 libgccpp.so.1"
RPROVIDES_gc-devel = "gc-dev (= 7.6.4)"
RDEPENDS_gc-devel = "gc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gc-7.6.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gc-devel-7.6.4-3.el8.aarch64.rpm \
          "

SRC_URI[gc.sha256sum] = "b912f8d66d2da8247e5d21f74e33192af7ac670f9be6c6f677b2e0503fc566a4"
SRC_URI[gc-devel.sha256sum] = "974fd6df6a12722d74d7d82dd78f1e488383b95f48ff40bbc9e842112d3306cd"
