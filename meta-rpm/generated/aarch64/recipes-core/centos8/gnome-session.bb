SUMMARY = "generated recipe based on gnome-session srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gnome-desktop3 gtk+3 json-glib libepoxy libgcc libglvnd libice libsm libx11 libxcomposite pkgconfig-native systemd-libs"
RPM_SONAME_REQ_gnome-session = "ld-linux-aarch64.so.1 libEGL.so.1 libGL.so.1 libGLESv2.so.2 libICE.so.6 libSM.so.6 libX11.so.6 libXcomposite.so.1 libc.so.6 libepoxy.so.0 libgcc_s.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libsystemd.so.0"
RDEPENDS_gnome-session = "bash centos-backgrounds centos-logos dbus-x11 dconf glib2 glibc gnome-control-center-filesystem gnome-desktop3 gsettings-desktop-schemas gtk3 json-glib libICE libSM libX11 libXcomposite libepoxy libgcc libglvnd-egl libglvnd-gles libglvnd-glx systemd-libs"
RDEPENDS_gnome-session-wayland-session = "gnome-session xorg-x11-server-Xwayland"
RDEPENDS_gnome-session-xsession = "gnome-session"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-session-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-session-wayland-session-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-session-xsession-3.28.1-8.el8.aarch64.rpm \
          "

SRC_URI[gnome-session.sha256sum] = "4c6ba4e9f99786f682790c24d9ae9dcd489984da541caf36b746f2aeb65df5e9"
SRC_URI[gnome-session-wayland-session.sha256sum] = "eeb22b9ba7290efd8949f12a35629401795ff664e3e99b1274ff7b6835474e36"
SRC_URI[gnome-session-xsession.sha256sum] = "6c10ae158b91ed3732c42fa40f686c1dd01819615ca1060ebc19ce0d2dde0d42"
