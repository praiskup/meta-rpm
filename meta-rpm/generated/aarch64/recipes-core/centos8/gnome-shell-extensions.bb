SUMMARY = "generated recipe based on gnome-shell-extensions srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-classic-session = "gnome-shell-extension-apps-menu gnome-shell-extension-desktop-icons gnome-shell-extension-horizontal-workspaces gnome-shell-extension-launch-new-instance gnome-shell-extension-places-menu gnome-shell-extension-window-list nautilus"
RDEPENDS_gnome-shell-extension-apps-menu = "gnome-menus gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-common = "gnome-shell"
RDEPENDS_gnome-shell-extension-desktop-icons = "gjs gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-disable-screenshield = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-horizontal-workspaces = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-launch-new-instance = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-places-menu = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-window-grouper = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-window-list = "gnome-shell-extension-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-classic-session-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-apps-menu-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-common-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-desktop-icons-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-disable-screenshield-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-horizontal-workspaces-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-launch-new-instance-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-places-menu-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-window-grouper-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-shell-extension-window-list-3.32.1-10.el8.noarch.rpm \
          "

SRC_URI[gnome-classic-session.sha256sum] = "1330cb41c0f8cbac4c5ae8a63fac8d98a1d90859e3b4024409aad55879a43f9f"
SRC_URI[gnome-shell-extension-apps-menu.sha256sum] = "cb0eede68d3b9a6a6831afe1e6414237b01cb4c84571fdabdcb829b9d8ec1f42"
SRC_URI[gnome-shell-extension-common.sha256sum] = "a187c07e86d9a3cfc4665ed1651107eb50e0014d249abb8b344448e2bfd439fb"
SRC_URI[gnome-shell-extension-desktop-icons.sha256sum] = "ea9cdff1b57ed49fd8d928b979f9cf51a201bf9253eea3abf13de7180a09bd4a"
SRC_URI[gnome-shell-extension-disable-screenshield.sha256sum] = "26230e9ac39dacaf544f73c34f5599a150d4ac26ca37bc90e6da3b701c49eec8"
SRC_URI[gnome-shell-extension-horizontal-workspaces.sha256sum] = "de0884ec797b5072896eea2902566afaecf5d6d5bf68c38559688139758b6446"
SRC_URI[gnome-shell-extension-launch-new-instance.sha256sum] = "c2046394a4a0b5fcfe918b572c5c03b3912a6f716b3da3d6033372e3bb71fc0f"
SRC_URI[gnome-shell-extension-places-menu.sha256sum] = "f3a255a0dcaf805589ce288d738b90392cf867761c55b3f9d81cbaba1edb5b2c"
SRC_URI[gnome-shell-extension-window-grouper.sha256sum] = "6472be905b3a4a956fb73c236aec635f76c28dcc108470f112b634a12f3cdadf"
SRC_URI[gnome-shell-extension-window-list.sha256sum] = "cbe02e9c9a3f7bd5442f1bc3712857b0dcc1e924f9fc7445ca650e324c72fac4"
