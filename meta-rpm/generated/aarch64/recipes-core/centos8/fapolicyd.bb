SUMMARY = "generated recipe based on fapolicyd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "file libcap-ng libgcrypt libseccomp lmdb pkgconfig-native rpm systemd-libs"
RPM_SONAME_REQ_fapolicyd = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libgcrypt.so.20 liblmdb.so.0.0.0 libmagic.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libseccomp.so.2 libudev.so.1"
RDEPENDS_fapolicyd = "bash file-libs glibc libcap-ng libgcrypt libseccomp lmdb-libs platform-python rpm-libs shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fapolicyd-0.9.1-4.el8.aarch64.rpm \
          "

SRC_URI[fapolicyd.sha256sum] = "498a5c84a9a3765bf34073b492c372f03c42f5904f30740b9cb252bfe2592b48"
