SUMMARY = "generated recipe based on libsndfile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "flac gsm libogg libvorbis pkgconfig-native"
RPM_SONAME_PROV_libsndfile = "libsndfile.so.1"
RPM_SONAME_REQ_libsndfile = "ld-linux-aarch64.so.1 libFLAC.so.8 libc.so.6 libgsm.so.1 libm.so.6 libogg.so.0 libvorbis.so.0 libvorbisenc.so.2"
RDEPENDS_libsndfile = "flac-libs glibc gsm libogg libvorbis"
RPM_SONAME_REQ_libsndfile-devel = "libsndfile.so.1"
RPROVIDES_libsndfile-devel = "libsndfile-dev (= 1.0.28)"
RDEPENDS_libsndfile-devel = "libsndfile pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsndfile-1.0.28-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsndfile-devel-1.0.28-10.el8.aarch64.rpm \
          "

SRC_URI[libsndfile.sha256sum] = "4d86b9bdf0a19b692f417a1828c35369c75d9403bc6f2f78a9f242ca3ddcb70d"
SRC_URI[libsndfile-devel.sha256sum] = "ab41f2292f43271d61007a4c2f92994e19430a2940fb05c917b9ac4712f9da6e"
