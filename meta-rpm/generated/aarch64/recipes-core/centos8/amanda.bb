SUMMARY = "generated recipe based on amanda srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl e2fsprogs glib-2.0 krb5-libs libtirpc openssl pkgconfig-native readline"
RPM_SONAME_REQ_amanda = "ld-linux-aarch64.so.1 libamanda-3.5.1.so libamandad-3.5.1.so libamar-3.5.1.so libamclient-3.5.1.so libamdevice-3.5.1.so libamglue-3.5.1.so libamserver-3.5.1.so libamxfer-3.5.1.so libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgthread-2.0.so.0 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 libm.so.6 libndmjob-3.5.1.so libndmlib-3.5.1.so libpthread.so.0 libresolv.so.2 librt.so.1 libssl.so.1.1 libtirpc.so.3"
RDEPENDS_amanda = "amanda-libs bash glib2 glibc grep krb5-libs libcom_err libcurl libtirpc mailx openssl-libs perl-Carp perl-Data-Dumper perl-Encode perl-Encode-Locale perl-Errno perl-Exporter perl-File-Path perl-File-Temp perl-Getopt-Long perl-IO perl-IO-Socket-SSL perl-JSON perl-MIME-Base64 perl-Math-BigInt perl-PathTools perl-Scalar-List-Utils perl-Socket perl-Text-ParseWords perl-Time-Local perl-URI perl-XML-Simple perl-constant perl-interpreter perl-libs perl-parent systemd tar"
RPM_SONAME_REQ_amanda-client = "ld-linux-aarch64.so.1 libamanda-3.5.1.so libamandad-3.5.1.so libamclient-3.5.1.so libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcurl.so.4 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgthread-2.0.so.0 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 libm.so.6 libpthread.so.0 libreadline.so.7 libresolv.so.2 librt.so.1 libssl.so.1.1 libtirpc.so.3"
RDEPENDS_amanda-client = "amanda amanda-libs bash glib2 glibc grep krb5-libs libcom_err libcurl libtirpc openssl-libs perl-Carp perl-File-Path perl-File-Temp perl-Getopt-Long perl-IO perl-JSON perl-MIME-Base64 perl-XML-Simple perl-interpreter perl-libs readline"
RPM_SONAME_PROV_amanda-libs = "libamanda-3.5.1.so libamandad-3.5.1.so libamar-3.5.1.so libamclient-3.5.1.so libamdevice-3.5.1.so libamglue-3.5.1.so libamserver-3.5.1.so libamxfer-3.5.1.so libndmjob-3.5.1.so libndmlib-3.5.1.so"
RPM_SONAME_REQ_amanda-libs = "ld-linux-aarch64.so.1 libamanda-3.5.1.so libamdevice-3.5.1.so libamxfer-3.5.1.so libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgthread-2.0.so.0 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 libm.so.6 libndmlib-3.5.1.so libpthread.so.0 libresolv.so.2 librt.so.1 libssl.so.1.1 libtirpc.so.3"
RDEPENDS_amanda-libs = "glib2 glibc grep krb5-libs libcom_err libcurl libtirpc openssl-libs"
RPM_SONAME_REQ_amanda-server = "ld-linux-aarch64.so.1 libamanda-3.5.1.so libamandad-3.5.1.so libamdevice-3.5.1.so libamserver-3.5.1.so libamxfer-3.5.1.so libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgthread-2.0.so.0 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 libm.so.6 libndmlib-3.5.1.so libpthread.so.0 libresolv.so.2 librt.so.1 libssl.so.1.1 libtirpc.so.3"
RDEPENDS_amanda-server = "amanda amanda-libs bash glib2 glibc grep krb5-libs libcom_err libcurl libtirpc openssl-libs perl-Carp perl-Data-Dumper perl-Encode perl-Encode-Locale perl-File-Path perl-Getopt-Long perl-JSON perl-MIME-Base64 perl-Math-BigInt perl-PathTools perl-Socket perl-Text-ParseWords perl-Text-Tabs+Wrap perl-Time-Local perl-XML-Simple perl-constant perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/amanda-3.5.1-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/amanda-client-3.5.1-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/amanda-libs-3.5.1-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/amanda-server-3.5.1-13.el8.aarch64.rpm \
          "

SRC_URI[amanda.sha256sum] = "32bc1fbc7e49013833edc6f387cdc6666b740a49628e656213fa2f54f383d3e8"
SRC_URI[amanda-client.sha256sum] = "1127c76b98fe805bf7059ca34b4de5099c859a98faebc8c3c6c6f9d1f215f859"
SRC_URI[amanda-libs.sha256sum] = "e9d831533a6f81c88da1d7869e55e70a576b27eda5c6718dbd0fcf2f476f062d"
SRC_URI[amanda-server.sha256sum] = "e93686380c3c835d1dc22e1c7d02266c2bc0c1e6a0be5f2ceebe5ddf39ed0911"
