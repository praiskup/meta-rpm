SUMMARY = "generated recipe based on zziplib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sdl zlib"
RPM_SONAME_PROV_zziplib = "libzzip-0.so.13 libzzipfseeko-0.so.13 libzzipmmapped-0.so.13 libzzipwrap-0.so.13"
RPM_SONAME_REQ_zziplib = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1 libzzip-0.so.13"
RDEPENDS_zziplib = "glibc zlib"
RPM_SONAME_REQ_zziplib-devel = "libzzip-0.so.13 libzzipfseeko-0.so.13 libzzipmmapped-0.so.13 libzzipwrap-0.so.13"
RPROVIDES_zziplib-devel = "zziplib-dev (= 0.13.68)"
RDEPENDS_zziplib-devel = "SDL-devel pkgconf-pkg-config zlib-devel zziplib"
RPM_SONAME_REQ_zziplib-utils = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1 libzzip-0.so.13 libzzipfseeko-0.so.13 libzzipmmapped-0.so.13"
RDEPENDS_zziplib-utils = "glibc zlib zziplib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/zziplib-0.13.68-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/zziplib-utils-0.13.68-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/zziplib-devel-0.13.68-8.el8.aarch64.rpm \
          "

SRC_URI[zziplib.sha256sum] = "34ea61ef6db843b94d7855d31ae606e2070b46a83dfc8acce7b0ecf71c57e7d0"
SRC_URI[zziplib-devel.sha256sum] = "08cd035b4772b0cd603c1cf12f1fecdfd2c607d28935ae4b28f00b2681ed19ff"
SRC_URI[zziplib-utils.sha256sum] = "0de0452f3ff8bfb38d2d2e02906cee60d1b6aec378b83a404e2a71c6b48e3e98"
