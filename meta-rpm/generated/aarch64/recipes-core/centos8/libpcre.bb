SUMMARY = "generated recipe based on pcre srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_pcre = "libpcre.so.1 libpcreposix.so.0"
RPM_SONAME_REQ_pcre = "ld-linux-aarch64.so.1 libc.so.6 libpcre.so.1 libpthread.so.0"
RDEPENDS_pcre = "glibc"
RPM_SONAME_PROV_pcre-cpp = "libpcrecpp.so.0"
RPM_SONAME_REQ_pcre-cpp = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpcre.so.1 libstdc++.so.6"
RDEPENDS_pcre-cpp = "glibc libgcc libstdc++ pcre"
RPM_SONAME_REQ_pcre-devel = "libpcre.so.1 libpcre16.so.0 libpcre32.so.0 libpcrecpp.so.0 libpcreposix.so.0"
RPROVIDES_pcre-devel = "pcre-dev (= 8.42)"
RDEPENDS_pcre-devel = "bash pcre pcre-cpp pcre-utf16 pcre-utf32 pkgconf-pkg-config"
RDEPENDS_pcre-static = "pcre-devel"
RPM_SONAME_PROV_pcre-utf16 = "libpcre16.so.0"
RPM_SONAME_REQ_pcre-utf16 = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_pcre-utf16 = "glibc"
RPM_SONAME_PROV_pcre-utf32 = "libpcre32.so.0"
RPM_SONAME_REQ_pcre-utf32 = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_pcre-utf32 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre-8.42-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre-cpp-8.42-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre-devel-8.42-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre-utf16-8.42-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcre-utf32-8.42-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pcre-static-8.42-4.el8.aarch64.rpm \
          "
SRC_URI = "file://pcre-8.42-4.el8.patch"

SRC_URI[pcre.sha256sum] = "a8053346f758919a8e6f177d767d6372352b2b37e1c86195ab87a891ce55e692"
SRC_URI[pcre-cpp.sha256sum] = "51e8c06521516a7949dedd2ce7d41519c2703d0c53a9df2ea4b135810a17c29d"
SRC_URI[pcre-devel.sha256sum] = "28d4be9b0986949e02a23b6c45f87e0f1b3afc8bd48bf0ecdb7c3d0d8698d443"
SRC_URI[pcre-static.sha256sum] = "6969c9244d6d4e292c62455f113ac544f8a579a5af5a8b7893a499144720dc97"
SRC_URI[pcre-utf16.sha256sum] = "13637775252aa796e7c7de234e6a6b1f5b08ddb0227c1b4066abd810a9885f89"
SRC_URI[pcre-utf32.sha256sum] = "3e3eba84b8414544a8cbf6bab19ff5d69f19393cf7aca77aa1f8281ee5d5729f"
