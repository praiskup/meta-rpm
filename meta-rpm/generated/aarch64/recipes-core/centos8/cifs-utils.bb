SUMMARY = "generated recipe based on cifs-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "keyutils krb5-libs libcap-ng libtalloc pam pkgconfig-native samba"
RPM_SONAME_REQ_cifs-utils = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libdl.so.2 libkeyutils.so.1 libkrb5.so.3 libtalloc.so.2 libwbclient.so.0"
RDEPENDS_cifs-utils = "bash chkconfig glibc keyutils keyutils-libs krb5-libs libcap-ng libtalloc libwbclient"
RPM_SONAME_REQ_pam_cifscreds = "ld-linux-aarch64.so.1 libc.so.6 libkeyutils.so.1 libpam.so.0"
RDEPENDS_pam_cifscreds = "glibc keyutils-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cifs-utils-6.8-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pam_cifscreds-6.8-3.el8.aarch64.rpm \
          "

SRC_URI[cifs-utils.sha256sum] = "de4b315dd44a1f98d2a45b795a2f9d5909636ab14270e24610419e2246b3acf2"
SRC_URI[pam_cifscreds.sha256sum] = "743de167cb8fc71b8312998c30f5cba5957c4e258ca00cf2de234d485395e91d"
