SUMMARY = "generated recipe based on libXmu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xmu"
DEPENDS = "libx11 libxext libxt pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXmu = "libXmu.so.6 libXmuu.so.1"
RPM_SONAME_REQ_libXmu = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXt.so.6 libc.so.6"
RDEPENDS_libXmu = "glibc libX11 libXext libXt"
RPM_SONAME_REQ_libXmu-devel = "libXmu.so.6 libXmuu.so.1"
RPROVIDES_libXmu-devel = "libXmu-dev (= 1.1.2)"
RDEPENDS_libXmu-devel = "libX11-devel libXext-devel libXmu libXt-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXmu-1.1.2-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXmu-devel-1.1.2-12.el8.aarch64.rpm \
          "

SRC_URI[libXmu.sha256sum] = "68a9edc1a362d83b966c243120e107b7e58a9c292b099d8ecbe6f87ba8856ecc"
SRC_URI[libXmu-devel.sha256sum] = "c2b4d888683a4fe94eb3212f1604c22a664bd012a0eba8ae61349aa1d40aa2e9"
