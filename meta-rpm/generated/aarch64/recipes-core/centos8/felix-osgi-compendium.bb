SUMMARY = "generated recipe based on felix-osgi-compendium srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_felix-osgi-compendium = "felix-osgi-core felix-osgi-foundation glassfish-servlet-api java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_felix-osgi-compendium-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-osgi-compendium-1.4.0-26.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-osgi-compendium-javadoc-1.4.0-26.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[felix-osgi-compendium.sha256sum] = "0d5ee5ff50dbf2ff61de5655ac115ed8fadd1404473b612ea461528a1247e3bd"
SRC_URI[felix-osgi-compendium-javadoc.sha256sum] = "fd35f1f06f2fe1cc4dcfcf112fc6b9992151e7098aa486d9ae9652ed1ab22db6"
