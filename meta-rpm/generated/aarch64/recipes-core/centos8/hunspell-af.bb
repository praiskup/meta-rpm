SUMMARY = "generated recipe based on hunspell-af srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-af = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-af-0.20080825-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-af.sha256sum] = "94c07749804e7e1e8b25be0e4352c9b43260fd77f39969883da9bd8476081815"
