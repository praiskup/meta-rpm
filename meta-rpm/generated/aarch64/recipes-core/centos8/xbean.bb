SUMMARY = "generated recipe based on xbean srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xbean = "apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem log4j12 objectweb-asm slf4j"
RDEPENDS_xbean-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xbean-4.8-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xbean-javadoc-4.8-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xbean.sha256sum] = "9dfd935610a0200f7ccfdae13f229b508fd1a7587777e8a371714ae8660265b2"
SRC_URI[xbean-javadoc.sha256sum] = "43d75cfeff0ac8134e126c0c19442a9537888282f09ab9af861f42b27b70990b"
