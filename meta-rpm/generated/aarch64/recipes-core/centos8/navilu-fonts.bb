SUMMARY = "generated recipe based on navilu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_navilu-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/navilu-fonts-1.2-11.el8.noarch.rpm \
          "

SRC_URI[navilu-fonts.sha256sum] = "2ebaa8202d37eaf0e604e42c6afb3b0d15d63edf332b1ba96f7cd4f2b21c6d0e"
