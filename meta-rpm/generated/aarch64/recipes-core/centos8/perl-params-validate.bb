SUMMARY = "generated recipe based on perl-Params-Validate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Validate = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Validate = "glibc perl-Carp perl-Exporter perl-Module-Implementation perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Params-Validate-1.29-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Params-Validate.sha256sum] = "2804c950875cf7fc7c0a951cadb7bae3be486589aba08f6627836fe1cb56ea5f"
