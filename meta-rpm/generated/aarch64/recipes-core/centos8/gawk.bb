SUMMARY = "generated recipe based on gawk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libsigsegv mpfr pkgconfig-native readline"
RPM_SONAME_REQ_gawk = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpfr.so.4 libreadline.so.7 libsigsegv.so.2"
RDEPENDS_gawk = "filesystem glibc gmp libsigsegv mpfr readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gawk-4.2.1-1.el8.aarch64.rpm \
          "

SRC_URI[gawk.sha256sum] = "481c7df1492512f2a1d0afb42f041e4ac500cdf7d1f6e2f20c81222c7fc54e35"
