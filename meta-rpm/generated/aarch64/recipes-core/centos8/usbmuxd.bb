SUMMARY = "generated recipe based on usbmuxd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libimobiledevice libplist libusb1 pkgconfig-native"
RPM_SONAME_REQ_usbmuxd = "ld-linux-aarch64.so.1 libc.so.6 libimobiledevice.so.6 libplist.so.3 libpthread.so.0 libusb-1.0.so.0"
RDEPENDS_usbmuxd = "bash glibc libimobiledevice libplist libusbx shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/usbmuxd-1.1.0-13.el8.aarch64.rpm \
          "

SRC_URI[usbmuxd.sha256sum] = "096627b7f60821b5b89fe9ef1d968e92c71a175070dcdd553f006ec8de99e99c"
