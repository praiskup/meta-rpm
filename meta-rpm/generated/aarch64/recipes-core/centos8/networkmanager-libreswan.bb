SUMMARY = "generated recipe based on NetworkManager-libreswan srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libnl networkmanager pkgconfig-native"
RPM_SONAME_PROV_NetworkManager-libreswan = "libnm-vpn-plugin-libreswan.so"
RPM_SONAME_REQ_NetworkManager-libreswan = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libnl-3.so.200 libnm.so.0 libpthread.so.0 libutil.so.1"
RDEPENDS_NetworkManager-libreswan = "NetworkManager NetworkManager-libnm bash dbus glib2 glibc libgcc libnl3 libreswan"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/NetworkManager-libreswan-1.2.10-4.el8.aarch64.rpm \
          "

SRC_URI[NetworkManager-libreswan.sha256sum] = "ed15ff1ca8e6c2ff1fb044a6e153123f011887fa75039550a21a0e5b741b960d"
