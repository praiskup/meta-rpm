SUMMARY = "generated recipe based on libXp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxau libxext pkgconfig-native"
RPM_SONAME_PROV_libXp = "libXp.so.6"
RPM_SONAME_REQ_libXp = "ld-linux-aarch64.so.1 libX11.so.6 libXau.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXp = "glibc libX11 libXau libXext"
RPM_SONAME_REQ_libXp-devel = "libXp.so.6"
RPROVIDES_libXp-devel = "libXp-dev (= 1.0.3)"
RDEPENDS_libXp-devel = "libX11-devel libXau-devel libXext-devel libXp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXp-1.0.3-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXp-devel-1.0.3-3.el8.aarch64.rpm \
          "

SRC_URI[libXp.sha256sum] = "999abf3a44bcc6284d2873cf395e91056642d6aac389ee9982a24ea99d213161"
SRC_URI[libXp-devel.sha256sum] = "01d436725427caef8c80a77f037ee8836531d42243b41e3f5d637f8711ac1ea8"
