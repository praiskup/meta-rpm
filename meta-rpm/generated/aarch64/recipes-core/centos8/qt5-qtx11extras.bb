SUMMARY = "generated recipe based on qt5-qtx11extras srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qt5-qtx11extras = "libQt5X11Extras.so.5"
RPM_SONAME_REQ_qt5-qtx11extras = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtx11extras = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_qt5-qtx11extras-devel = "libQt5X11Extras.so.5"
RPROVIDES_qt5-qtx11extras-devel = "qt5-qtx11extras-dev (= 5.12.5)"
RDEPENDS_qt5-qtx11extras-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtx11extras"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtx11extras-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtx11extras-devel-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtx11extras.sha256sum] = "2c6905562c0af5d3e206f7bb8ad7784a3a8410ce1c2a53a81b9568d684a8e887"
SRC_URI[qt5-qtx11extras-devel.sha256sum] = "2e2e3df7f406d16d821c9b768ee266f167d4e66b28db10764359ce862ce36d31"
