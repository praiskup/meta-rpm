SUMMARY = "generated recipe based on perl-Perl-Destruct-Level srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Perl-Destruct-Level = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Perl-Destruct-Level = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Perl-Destruct-Level-0.02-20.el8.aarch64.rpm \
          "

SRC_URI[perl-Perl-Destruct-Level.sha256sum] = "881366fc612d7cdff36d4367fc356174addf35298df37226b10879b77bd781d9"
