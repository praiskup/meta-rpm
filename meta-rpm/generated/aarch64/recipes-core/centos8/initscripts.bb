SUMMARY = "generated recipe based on initscripts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native popt"
RPM_SONAME_REQ_initscripts = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libpopt.so.0"
RDEPENDS_initscripts = "bash coreutils filesystem findutils gawk glib2 glibc grep popt procps-ng setup shadow-utils systemd util-linux"
RDEPENDS_netconsole-service = "bash coreutils filesystem gawk glibc-common initscripts iproute iputils kmod sed util-linux"
RPM_SONAME_REQ_network-scripts = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_network-scripts = "bash chkconfig coreutils dbus filesystem gawk glibc grep hostname initscripts ipcalc iproute kmod procps-ng sed systemd"
RDEPENDS_readonly-root = "bash coreutils cpio filesystem findutils gawk hostname initscripts ipcalc iproute util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/initscripts-10.00.6-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/netconsole-service-10.00.6-1.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/network-scripts-10.00.6-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/readonly-root-10.00.6-1.el8_2.2.noarch.rpm \
          "

SRC_URI[initscripts.sha256sum] = "afa4808bf4b56dbb896f24b72bec4c8eb433d5cdd74fc3460c9bd2b36a7676c3"
SRC_URI[netconsole-service.sha256sum] = "6014cc61b7b137768b81982bde91ba79978fc234f4a7f1a29f73ba710a284856"
SRC_URI[network-scripts.sha256sum] = "e4e1f6cc15357cdb7d029f39d28e1f0078145d84dc1d77a136665e5255548bfc"
SRC_URI[readonly-root.sha256sum] = "d88137051e13445a3542152c01bc244cd5b5c91fc0ff7ad0b710fde550e5b129"
