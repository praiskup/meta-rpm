SUMMARY = "generated recipe based on pyxdg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyxdg = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyxdg-0.25-16.el8.noarch.rpm \
          "

SRC_URI[python3-pyxdg.sha256sum] = "74d57e615d6dcf92d4c174f5219f3f396278304ab40985b51745d07608e875e6"
