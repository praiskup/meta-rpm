SUMMARY = "generated recipe based on augeas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libselinux libxml2 pkgconfig-native readline"
RPM_SONAME_REQ_augeas = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libfa.so.1 libgcc_s.so.1 libreadline.so.7 libselinux.so.1 libxml2.so.2"
RDEPENDS_augeas = "augeas-libs glibc libgcc libselinux libxml2 readline"
RPM_SONAME_REQ_augeas-devel = "libaugeas.so.0 libfa.so.1"
RPROVIDES_augeas-devel = "augeas-dev (= 1.12.0)"
RDEPENDS_augeas-devel = "augeas-libs libselinux-devel libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_augeas-libs = "libaugeas.so.0 libfa.so.1"
RPM_SONAME_REQ_augeas-libs = "ld-linux-aarch64.so.1 libc.so.6 libfa.so.1 libselinux.so.1 libxml2.so.2"
RDEPENDS_augeas-libs = "glibc libselinux libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/augeas-1.12.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/augeas-libs-1.12.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/augeas-devel-1.12.0-5.el8.aarch64.rpm \
          "

SRC_URI[augeas.sha256sum] = "a112848c37aa92fdd46a37ce7d2bf6e107fd4ec60b985c3d4f1425cb97cca7e4"
SRC_URI[augeas-devel.sha256sum] = "2c667b6093ba5a393d892e7787cc9accc8348e169c10456819ee45e26d8b2514"
SRC_URI[augeas-libs.sha256sum] = "94941050edf2d316038d3309f53992849f8e73e83eaa66079c9919997d431baa"
