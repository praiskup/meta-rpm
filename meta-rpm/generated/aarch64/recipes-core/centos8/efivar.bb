SUMMARY = "generated recipe based on efivar srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_efivar = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libefivar.so.1"
RDEPENDS_efivar = "efivar-libs glibc"
RPM_SONAME_PROV_efivar-libs = "libefiboot.so.1 libefivar.so.1"
RPM_SONAME_REQ_efivar-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libefivar.so.1"
RDEPENDS_efivar-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/efivar-36-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/efivar-libs-36-1.el8.aarch64.rpm \
          "

SRC_URI[efivar.sha256sum] = "9702573194d4d11974707180e6f1e5e0e8ed7f2361088afc71af0450536492f9"
SRC_URI[efivar-libs.sha256sum] = "7095af6b92597a3f8ddb3129edace55f25892fff2b5d12a9f6e6378020f318e3"
