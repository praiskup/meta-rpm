SUMMARY = "generated recipe based on libgit2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl http-parser openssl pkgconfig-native zlib"
RPM_SONAME_PROV_libgit2 = "libgit2.so.26"
RPM_SONAME_REQ_libgit2 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libhttp_parser.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1 libz.so.1"
RDEPENDS_libgit2 = "glibc http-parser libcurl openssl-libs zlib"
RPM_SONAME_REQ_libgit2-devel = "libgit2.so.26"
RPROVIDES_libgit2-devel = "libgit2-dev (= 0.26.8)"
RDEPENDS_libgit2-devel = "libgit2 openssl-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgit2-0.26.8-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgit2-devel-0.26.8-1.el8.aarch64.rpm \
          "

SRC_URI[libgit2.sha256sum] = "10a39eec2887b8bc61734e674e60701b91289b386141090d9f75870dc6543a04"
SRC_URI[libgit2-devel.sha256sum] = "d8d7dceff65448c858471d2b33b7da55c442e9e1b7610011edce8afd27689029"
