SUMMARY = "generated recipe based on python-wheel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-wheel = "bash platform-python python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-wheel-0.31.1-2.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-wheel-wheel-0.31.1-2.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-wheel.sha256sum] = "549ca39766d7b17f6ed17bdeb53270325d8fa4221470867dbe3a63fcd0a9416f"
SRC_URI[python3-wheel-wheel.sha256sum] = "eed2f0ee8c7bc2f23aa31280b15dad019cd7c17ad5ddf882331c7be1af6968fe"
