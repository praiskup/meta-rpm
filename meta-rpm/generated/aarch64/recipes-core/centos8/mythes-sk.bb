SUMMARY = "generated recipe based on mythes-sk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-sk = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-sk-0.20130130-11.el8.noarch.rpm \
          "

SRC_URI[mythes-sk.sha256sum] = "01bb5fcbb00c41bba68990bf39c1674daf52b9b7c376990b60a53cadc59f2620"
