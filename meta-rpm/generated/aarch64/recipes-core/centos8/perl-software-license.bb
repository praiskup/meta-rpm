SUMMARY = "generated recipe based on perl-Software-License srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Software-License = "perl-Carp perl-Data-Section perl-IO perl-Module-Load perl-PathTools perl-Text-Template perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Software-License-0.103013-2.el8.noarch.rpm \
          "

SRC_URI[perl-Software-License.sha256sum] = "900ed454b75df55db808055467d9e8eb6687013f530f6470c367ebeae62cf99e"
