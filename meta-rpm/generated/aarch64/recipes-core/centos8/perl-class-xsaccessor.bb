SUMMARY = "generated recipe based on perl-Class-XSAccessor srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Class-XSAccessor = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Class-XSAccessor = "glibc perl-Carp perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Class-XSAccessor-1.19-14.el8.aarch64.rpm \
          "

SRC_URI[perl-Class-XSAccessor.sha256sum] = "251b8d77e27aeeb2fa4e37e26e41a10c13e8838862af7313850f14657816faf7"
