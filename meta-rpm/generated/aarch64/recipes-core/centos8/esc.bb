SUMMARY = "generated recipe based on esc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_esc = "dbus gjs glib2 gobject-introspection gtk3 nspr nss opensc pcsc-lite"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/esc-1.1.2-11.el8.aarch64.rpm \
          "

SRC_URI[esc.sha256sum] = "23d47362171bf71853ff7c3f367e5fa2a3e1939e57e8faeb73e58d384c2e3148"
