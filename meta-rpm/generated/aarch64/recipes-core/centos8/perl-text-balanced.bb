SUMMARY = "generated recipe based on perl-Text-Balanced srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Balanced = "perl-Carp perl-Exporter perl-SelfLoader perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Text-Balanced-2.03-395.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Balanced.sha256sum] = "3c92c857f5d07fd4eb07d6c01e728281ef6c938a054b4edcda90e2b86fd458e9"
