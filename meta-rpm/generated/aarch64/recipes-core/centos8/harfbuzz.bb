SUMMARY = "generated recipe based on harfbuzz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo freetype glib-2.0 graphite2 icu libgcc pkgconfig-native"
RPM_SONAME_PROV_harfbuzz = "libharfbuzz.so.0"
RPM_SONAME_REQ_harfbuzz = "ld-linux-aarch64.so.1 libc.so.6 libfreetype.so.6 libglib-2.0.so.0 libgraphite2.so.3 libm.so.6"
RDEPENDS_harfbuzz = "freetype glib2 glibc graphite2"
RPM_SONAME_REQ_harfbuzz-devel = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 libglib-2.0.so.0 libharfbuzz-icu.so.0 libharfbuzz.so.0 libm.so.6 libstdc++.so.6"
RPROVIDES_harfbuzz-devel = "harfbuzz-dev (= 1.7.5)"
RDEPENDS_harfbuzz-devel = "cairo freetype glib2 glib2-devel glibc graphite2-devel harfbuzz harfbuzz-icu libgcc libicu-devel libstdc++ pkgconf-pkg-config"
RPM_SONAME_PROV_harfbuzz-icu = "libharfbuzz-icu.so.0"
RPM_SONAME_REQ_harfbuzz-icu = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libharfbuzz.so.0 libicudata.so.60 libicuuc.so.60 libm.so.6 libstdc++.so.6"
RDEPENDS_harfbuzz-icu = "glibc harfbuzz libgcc libicu libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/harfbuzz-1.7.5-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/harfbuzz-devel-1.7.5-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/harfbuzz-icu-1.7.5-3.el8.aarch64.rpm \
          "

SRC_URI[harfbuzz.sha256sum] = "463c80fb6b28a0bc8dbe50e9880d810b705198721f8e397599e6e4a1512277d1"
SRC_URI[harfbuzz-devel.sha256sum] = "818437839c634ca196e3c377136fe8e40ee320f4745c47e8fe5dd6063fc700ec"
SRC_URI[harfbuzz-icu.sha256sum] = "0286408a3347b873a6dcd48a57cf6a8e22f63511e79663e691894dce04414323"
