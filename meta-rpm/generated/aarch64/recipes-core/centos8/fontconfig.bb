SUMMARY = "generated recipe based on fontconfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat freetype libuuid pkgconfig-native"
RPM_SONAME_PROV_fontconfig = "libfontconfig.so.1"
RPM_SONAME_REQ_fontconfig = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libfontconfig.so.1 libfreetype.so.6 libpthread.so.0 libuuid.so.1"
RDEPENDS_fontconfig = "bash coreutils expat fontpackages-filesystem freetype glibc grep libuuid"
RPM_SONAME_REQ_fontconfig-devel = "libfontconfig.so.1"
RPROVIDES_fontconfig-devel = "fontconfig-dev (= 2.13.1)"
RDEPENDS_fontconfig-devel = "expat-devel fontconfig freetype-devel gettext libuuid-devel pkgconf-pkg-config"
RDEPENDS_fontconfig-devel-doc = "fontconfig-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fontconfig-2.13.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fontconfig-devel-2.13.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/fontconfig-devel-doc-2.13.1-3.el8.noarch.rpm \
          "

SRC_URI[fontconfig.sha256sum] = "e4dc102f15c1e6104bdeae343e54fcc7929b922b93077f6b50216087e7125a2d"
SRC_URI[fontconfig-devel.sha256sum] = "81fa6f39d00c3537a7662d6fec9ef6da89b2e953b6eac369844b9a30ffb4ca00"
SRC_URI[fontconfig-devel-doc.sha256sum] = "dae94a7165fa5d4004ce478c37226bb316e79c2ae1e3df8d5275d19e7f39569d"
