SUMMARY = "generated recipe based on cdrdao srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lame libao libgcc libmad libvorbis pkgconfig-native"
RPM_SONAME_REQ_cdrdao = "ld-linux-aarch64.so.1 libao.so.4 libc.so.6 libgcc_s.so.1 libm.so.6 libmad.so.0 libmp3lame.so.0 libpthread.so.0 libstdc++.so.6 libvorbisfile.so.3"
RDEPENDS_cdrdao = "glibc lame-libs libao libgcc libmad libstdc++ libvorbis"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cdrdao-1.2.3-32.el8.aarch64.rpm \
          "

SRC_URI[cdrdao.sha256sum] = "3706feb8cb711f2762c3c86f695d146abaccf362661409fb7be0d986eed9ca79"
