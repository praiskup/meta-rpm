SUMMARY = "generated recipe based on ibus-sayura srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 ibus pkgconfig-native"
RPM_SONAME_REQ_ibus-sayura = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 libpthread.so.0"
RDEPENDS_ibus-sayura = "glib2 glibc ibus ibus-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-sayura-1.3.2-13.el8.aarch64.rpm \
          "

SRC_URI[ibus-sayura.sha256sum] = "74b20de7ed0ace77c78ad466201372018cc7839ef81040db74d291249fd97990"
