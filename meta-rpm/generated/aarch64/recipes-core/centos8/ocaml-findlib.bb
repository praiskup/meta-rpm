SUMMARY = "generated recipe based on ocaml-findlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-findlib = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-findlib = "bash glibc ocaml ocaml-compiler-libs ocaml-runtime"
RPROVIDES_ocaml-findlib-devel = "ocaml-findlib-dev (= 1.8.0)"
RDEPENDS_ocaml-findlib-devel = "ocaml-findlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-findlib-1.8.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-findlib-devel-1.8.0-4.el8.aarch64.rpm \
          "

SRC_URI[ocaml-findlib.sha256sum] = "ca7a3478e07c9b859702f1c9fb604fe501db2bd686954db04e70cb66a4a8cb7c"
SRC_URI[ocaml-findlib-devel.sha256sum] = "89297330e076164a9100d79b912fae67577c74ee298f01ee15545b57b2248309"
