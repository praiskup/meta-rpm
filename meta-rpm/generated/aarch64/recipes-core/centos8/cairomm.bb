SUMMARY = "generated recipe based on cairomm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo freetype libgcc libpng libsigc++20 libx11 libxext libxrender pkgconfig-native zlib"
RPM_SONAME_PROV_cairomm = "libcairomm-1.0.so.1"
RPM_SONAME_REQ_cairomm = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 libm.so.6 libpng16.so.16 libsigc-2.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_cairomm = "cairo freetype glibc libX11 libXext libXrender libgcc libpng libsigc++20 libstdc++ zlib"
RPM_SONAME_REQ_cairomm-devel = "libcairomm-1.0.so.1"
RPROVIDES_cairomm-devel = "cairomm-dev (= 1.12.0)"
RDEPENDS_cairomm-devel = "cairo-devel cairomm libsigc++20-devel pkgconf-pkg-config"
RDEPENDS_cairomm-doc = "cairomm libsigc++20-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cairomm-1.12.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cairomm-devel-1.12.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cairomm-doc-1.12.0-7.el8.noarch.rpm \
          "

SRC_URI[cairomm.sha256sum] = "136ae2fd02f18177f83c0d7ada97c16a4a920bc749744a4042073bb7793a8e89"
SRC_URI[cairomm-devel.sha256sum] = "53a649d52058bde0974578d7fe6562a039463db00ea970a9333d324d697e7f9c"
SRC_URI[cairomm-doc.sha256sum] = "0f6eab26a8ca223190f1ea4ad0321807b37c9805f2f1aa2633323aa78d47a531"
