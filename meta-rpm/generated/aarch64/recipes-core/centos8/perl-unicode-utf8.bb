SUMMARY = "generated recipe based on perl-Unicode-UTF8 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-UTF8 = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-UTF8 = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Unicode-UTF8-0.62-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Unicode-UTF8.sha256sum] = "c5f941373f5867c9e2a9bf279f02d19b6f6030c7a17a2ce9fe4d3d303371ae58"
