SUMMARY = "generated recipe based on perl-Digest-CRC srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-CRC = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-CRC = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Digest-CRC-0.22.2-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Digest-CRC.sha256sum] = "e0f563049305078b8d316ca3cfd845c4d13016371dc46a642240a5b15d576cbd"
