SUMMARY = "generated recipe based on multilib-rpm-config srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_multilib-rpm-config = "bash redhat-rpm-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/multilib-rpm-config-1-10.el8.noarch.rpm \
          "

SRC_URI[multilib-rpm-config.sha256sum] = "937bdbba99e119d5f8d0c6d252f058565539a552ecce68a348fb84286c674f01"
