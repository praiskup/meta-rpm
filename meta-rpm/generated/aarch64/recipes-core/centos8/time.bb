SUMMARY = "generated recipe based on time srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_time = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_time = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/time-1.9-3.el8.aarch64.rpm \
          "

SRC_URI[time.sha256sum] = "1887b9b2e3c5bdb843cfd06421191c0580412bb22fdc8f3930aca9e03d102b4c"
