SUMMARY = "generated recipe based on dracut srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "kmod libgcc pkgconfig-native"
RPM_SONAME_REQ_dracut = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libkmod.so.2"
RDEPENDS_dracut = "bash coreutils cpio filesystem findutils glibc grep gzip kmod kmod-libs libgcc libkcapi-hmaccalc procps-ng sed systemd systemd-udev util-linux xz"
RDEPENDS_dracut-caps = "bash dracut libcap"
RDEPENDS_dracut-config-generic = "dracut"
RDEPENDS_dracut-config-rescue = "bash dracut"
RDEPENDS_dracut-live = "bash coreutils curl device-mapper dracut dracut-network gzip tar"
RDEPENDS_dracut-network = "bash dhcp-client dracut iproute iputils"
RDEPENDS_dracut-squash = "bash dracut squashfs-tools"
RDEPENDS_dracut-tools = "bash dracut"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-caps-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-config-generic-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-config-rescue-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-live-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-network-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-squash-049-70.git20200228.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dracut-tools-049-70.git20200228.el8.aarch64.rpm \
          "

SRC_URI[dracut.sha256sum] = "c4dfc2912e7c144dd3cb72135e6e9d60a3b4df285891c39f4978ff80744c285c"
SRC_URI[dracut-caps.sha256sum] = "f935599c96976ddc5d6cb8979ddf424a922804e92be2f5ba1dce831317ae4bc3"
SRC_URI[dracut-config-generic.sha256sum] = "3f02146ddfa6ed6be37e42e0dcc0e43409094840edba95488a5b4e8ae5961fbe"
SRC_URI[dracut-config-rescue.sha256sum] = "160480597c36506ef5e1fec6f01983506bfe149a512b330aa834efc2063e839c"
SRC_URI[dracut-live.sha256sum] = "360186aad27ce0776360b2025499a8828d6d751c6f6fb521479dcadc36422424"
SRC_URI[dracut-network.sha256sum] = "e990c923623c24de703b7e0e39bcb25aa13be87ef6a31deed65d794671944686"
SRC_URI[dracut-squash.sha256sum] = "688806514f644c13caa49614a6c0e5cefc8507e5bc6de93af5cab76755f4ef33"
SRC_URI[dracut-tools.sha256sum] = "493d778818b3e6c1bcf4507805d541b188200390c43257f2cd07988f7f53f7bf"
