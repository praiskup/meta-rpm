SUMMARY = "generated recipe based on rubygem-diff-lcs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-diff-lcs = "ruby rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rubygem-diff-lcs-1.3-4.el8.noarch.rpm \
          "

SRC_URI[rubygem-diff-lcs.sha256sum] = "e92aae92b214f425199a0cced41e4b583afe40261496590399e94985eb501580"
