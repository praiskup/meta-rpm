SUMMARY = "generated recipe based on hunspell-haw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-haw = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-haw-0.03-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-haw.sha256sum] = "f6dcaa865c2a509b9da29a47c00eccbe21137865d095a909b029fdbdd4b04b90"
