SUMMARY = "generated recipe based on ocaml-extlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-extlib = "libc.so.6"
RDEPENDS_ocaml-extlib = "glibc ocaml-runtime"
RPROVIDES_ocaml-extlib-devel = "ocaml-extlib-dev (= 1.7.5)"
RDEPENDS_ocaml-extlib-devel = "ocaml-extlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-extlib-1.7.5-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-extlib-devel-1.7.5-3.el8.aarch64.rpm \
          "

SRC_URI[ocaml-extlib.sha256sum] = "83b138c7bf3e42cad4c8c789808e0e93627adb8c0018bf08420de18e49faf7be"
SRC_URI[ocaml-extlib-devel.sha256sum] = "293bdacca8cf018e31014d6883987c0efe1fd189047c99bf9f2db9f5755943e8"
