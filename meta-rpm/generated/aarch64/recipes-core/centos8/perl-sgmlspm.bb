SUMMARY = "generated recipe based on perl-SGMLSpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-SGMLSpm = "openjade perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-SGMLSpm-1.03ii-42.el8.noarch.rpm \
          "

SRC_URI[perl-SGMLSpm.sha256sum] = "cb805d5dcc47f666c22fd57c3a1af0c2c6d3bbd17e7907b469b1baa3636427d4"
