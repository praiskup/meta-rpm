SUMMARY = "generated recipe based on opensc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native readline zlib"
RPM_SONAME_PROV_opensc = "libopensc.so.6 libsmm-local.so.6"
RPM_SONAME_REQ_opensc = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libopensc.so.6 libpthread.so.0 libreadline.so.7 libz.so.1"
RDEPENDS_opensc = "bash glibc openssl-libs pcsc-lite pcsc-lite-libs pkgconf-pkg-config readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opensc-0.19.0-7.el8.aarch64.rpm \
          "

SRC_URI[opensc.sha256sum] = "5700d441016c40a38541b4599291a9c610bcf11980aeb08572ec78ee541cac3f"
