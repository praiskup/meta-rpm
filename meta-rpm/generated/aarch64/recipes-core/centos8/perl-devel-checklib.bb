SUMMARY = "generated recipe based on perl-Devel-CheckLib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-CheckLib = "perl-Exporter perl-File-Temp perl-PathTools perl-Text-ParseWords perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-CheckLib-1.11-5.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-CheckLib.sha256sum] = "a1d99c3ef4ea454ad9d736bf5c89c61c8bcecc9f0ab25947d7c93288cec45df2"
