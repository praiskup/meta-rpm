SUMMARY = "generated recipe based on yp-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnsl2 libtirpc libxcrypt pkgconfig-native"
RPM_SONAME_REQ_yp-tools = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libnsl.so.2 libtirpc.so.3"
RDEPENDS_yp-tools = "glibc libnsl2 libtirpc libxcrypt ypbind"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/yp-tools-4.2.3-1.el8.aarch64.rpm \
          "

SRC_URI[yp-tools.sha256sum] = "0f1eb7a29f571ee5ca19d21d88b685498eea79fd109a82f1ae30671cfa5ba0ae"
