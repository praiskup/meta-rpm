SUMMARY = "generated recipe based on shim-unsigned-aarch64 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/shim-unsigned-aarch64-15-4.el8.aarch64.rpm \
          "

SRC_URI[shim-unsigned-aarch64.sha256sum] = "f87600ac90d8c830b3d66bc80e783b5fa6227a9acbe2eef81949f75de09d6da0"
