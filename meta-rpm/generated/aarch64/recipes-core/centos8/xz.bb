SUMMARY = "generated recipe based on xz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xz = "ld-linux-aarch64.so.1 libc.so.6 liblzma.so.5 libpthread.so.0"
RDEPENDS_xz = "bash glibc grep xz-libs"
RPM_SONAME_REQ_xz-devel = "liblzma.so.5"
RPROVIDES_xz-devel = "xz-dev (= 5.2.4)"
RDEPENDS_xz-devel = "pkgconf-pkg-config xz-libs"
RPM_SONAME_PROV_xz-libs = "liblzma.so.5"
RPM_SONAME_REQ_xz-libs = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_xz-libs = "glibc"
RPM_SONAME_REQ_xz-lzma-compat = "ld-linux-aarch64.so.1 libc.so.6 liblzma.so.5 libpthread.so.0"
RDEPENDS_xz-lzma-compat = "glibc xz xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xz-5.2.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xz-devel-5.2.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xz-libs-5.2.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xz-lzma-compat-5.2.4-3.el8.aarch64.rpm \
          "

SRC_URI[xz.sha256sum] = "b9a899e715019e7002600005bcb2a9dd7b089eaef9c55c3764c326d745ad681f"
SRC_URI[xz-devel.sha256sum] = "2fcd905c343bed5eb3332d24804fa84975f459960bf0530b82f7680a48d7ca8d"
SRC_URI[xz-libs.sha256sum] = "8f141db26834b1ec60028790b130d00b14b7fda256db0df1e51b7ba8d3d40c7b"
SRC_URI[xz-lzma-compat.sha256sum] = "68efa01de7acccdd243e0e592a1f47a4a0b2be8001454f1d166d0e2a22fbc721"
