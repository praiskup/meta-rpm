SUMMARY = "generated recipe based on rpcbind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtirpc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_rpcbind = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libsystemd.so.0 libtirpc.so.3"
RDEPENDS_rpcbind = "bash chkconfig coreutils glibc glibc-common libtirpc policycoreutils setup shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpcbind-1.2.5-7.el8.aarch64.rpm \
          "

SRC_URI[rpcbind.sha256sum] = "7de8bfdda9407f31d43c7c9c4bfa593419b7273edd6f48cec167c01de87977ed"
