SUMMARY = "generated recipe based on speex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_speex = "libspeex.so.1"
RPM_SONAME_REQ_speex = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_speex = "glibc"
RPM_SONAME_REQ_speex-devel = "libspeex.so.1"
RPROVIDES_speex-devel = "speex-dev (= 1.2.0)"
RDEPENDS_speex-devel = "pkgconf-pkg-config speex"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/speex-1.2.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/speex-devel-1.2.0-1.el8.aarch64.rpm \
          "

SRC_URI[speex.sha256sum] = "7b8b6b775e96c46a12e61b0c02e2fc74b3de5f735eea00f97f22a3fd0c7fb694"
SRC_URI[speex-devel.sha256sum] = "0a2bfe2c08d995537d229bad0f6a1d062902d3172f92a43af9cf7a8838d61c5a"
