SUMMARY = "generated recipe based on libSM srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libice libuuid pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libSM = "libSM.so.6"
RPM_SONAME_REQ_libSM = "ld-linux-aarch64.so.1 libICE.so.6 libc.so.6 libuuid.so.1"
RDEPENDS_libSM = "glibc libICE libuuid"
RPM_SONAME_REQ_libSM-devel = "libSM.so.6"
RPROVIDES_libSM-devel = "libSM-dev (= 1.2.3)"
RDEPENDS_libSM-devel = "libICE-devel libSM pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libSM-1.2.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libSM-devel-1.2.3-1.el8.aarch64.rpm \
          "

SRC_URI[libSM.sha256sum] = "d95cc40774040bbd0aada1b0d38a5e1c036b3bdc331d8758d661332fa3f0aab0"
SRC_URI[libSM-devel.sha256sum] = "f4b51e7241d8a27e2c4ade27128fc652a600e6e6dc518963ff1ee3b7f209f9e0"
