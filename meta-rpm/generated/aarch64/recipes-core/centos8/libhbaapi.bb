SUMMARY = "generated recipe based on libhbaapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libhbaapi = "libHBAAPI.so.2"
RPM_SONAME_REQ_libhbaapi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libhbaapi = "glibc"
RPM_SONAME_REQ_libhbaapi-devel = "libHBAAPI.so.2"
RPROVIDES_libhbaapi-devel = "libhbaapi-dev (= 2.2.9)"
RDEPENDS_libhbaapi-devel = "libhbaapi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libhbaapi-2.2.9-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libhbaapi-devel-2.2.9-13.el8.aarch64.rpm \
          "

SRC_URI[libhbaapi.sha256sum] = "cc8777238aa130af583b707ff4f9af58a2e8cbabbeb3de4aa245aa1579e1b2c4"
SRC_URI[libhbaapi-devel.sha256sum] = "23edfea9e06d3086c6e8db99299a2fe2591f6e716474bab8008e3f7b7afcfc11"
