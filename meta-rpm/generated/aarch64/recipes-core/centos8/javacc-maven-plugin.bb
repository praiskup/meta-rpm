SUMMARY = "generated recipe based on javacc-maven-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_javacc-maven-plugin = "java-1.8.0-openjdk-headless javacc javapackages-filesystem maven-doxia-sink-api maven-doxia-sitetools maven-lib maven-project maven-reporting-api maven-reporting-impl plexus-utils"
RDEPENDS_javacc-maven-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javacc-maven-plugin-2.6-25.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javacc-maven-plugin-javadoc-2.6-25.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[javacc-maven-plugin.sha256sum] = "98ab9c07c6c043a8b7d056ac2e8f990a49edfe1c91f53a8e88e9c051c9305c83"
SRC_URI[javacc-maven-plugin-javadoc.sha256sum] = "0d2589386c983d5630573066796deeeecb5a1904f05d8a3b4b092c71a55579b1"
