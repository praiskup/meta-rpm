SUMMARY = "generated recipe based on perl-Params-ValidationCompiler srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Params-ValidationCompiler = "perl-Carp perl-Eval-Closure perl-Exception-Class perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Params-ValidationCompiler-0.27-1.el8.noarch.rpm \
          "

SRC_URI[perl-Params-ValidationCompiler.sha256sum] = "e5f776a979e71b0cb6866aece89fbe3dcc6043cc4d0902dd3f9b8294565b445e"
