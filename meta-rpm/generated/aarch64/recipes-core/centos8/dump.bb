SUMMARY = "generated recipe based on dump srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 e2fsprogs libselinux lzo ncurses pkgconfig-native readline zlib"
RPM_SONAME_REQ_dump = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libcom_err.so.2 libe2p.so.2 libext2fs.so.2 liblzo2.so.2 libreadline.so.7 libselinux.so.1 libtinfo.so.6 libz.so.1"
RDEPENDS_dump = "bzip2-libs e2fsprogs-libs glibc libcom_err libselinux lzo ncurses-libs readline rmt setup zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dump-0.4-0.36.b46.el8.aarch64.rpm \
          "

SRC_URI[dump.sha256sum] = "32c7a31c3ca6c7880fb3e699b44a4483b790e35f6c2717b8a1f66779acc10d00"
