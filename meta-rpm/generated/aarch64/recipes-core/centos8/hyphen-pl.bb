SUMMARY = "generated recipe based on hyphen-pl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-pl = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-pl-0.20060726-17.el8.noarch.rpm \
          "

SRC_URI[hyphen-pl.sha256sum] = "e3901309c033ee0d9e9ae9e126e84cc747b22c9486a645a06a86133f839114d7"
