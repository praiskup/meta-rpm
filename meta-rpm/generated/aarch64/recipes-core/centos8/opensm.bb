SUMMARY = "generated recipe based on opensm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native rdma-core"
RPM_SONAME_REQ_opensm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libibumad.so.3 libopensm.so.9 libosmcomp.so.5 libosmvendor.so.5 libpthread.so.0"
RDEPENDS_opensm = "bash glibc libibumad logrotate opensm-libs rdma-core systemd"
RPM_SONAME_REQ_opensm-devel = "libopensm.so.9 libosmcomp.so.5 libosmvendor.so.5"
RPROVIDES_opensm-devel = "opensm-dev (= 3.3.22)"
RDEPENDS_opensm-devel = "opensm-libs"
RPM_SONAME_PROV_opensm-libs = "libopensm.so.9 libosmcomp.so.5 libosmvendor.so.5"
RPM_SONAME_REQ_opensm-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libibumad.so.3 libopensm.so.9 libosmcomp.so.5 libpthread.so.0"
RDEPENDS_opensm-libs = "glibc libgcc libibumad"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opensm-3.3.22-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opensm-libs-3.3.22-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/opensm-devel-3.3.22-2.el8.aarch64.rpm \
          "

SRC_URI[opensm.sha256sum] = "c4c6c08e0b682337765c30ab2040960841336f9377ce2829e1d8040f1ed8257b"
SRC_URI[opensm-devel.sha256sum] = "78bf0f52f01432f6e5eaca2eba91527b627e594ac3a1e36276636b23dad41fe4"
SRC_URI[opensm-libs.sha256sum] = "56c1a996650022de8a34ea099da2af1ce3b56428d72b7397079d04fa99b1cc9e"
