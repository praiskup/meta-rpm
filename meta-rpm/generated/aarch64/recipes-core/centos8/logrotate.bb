SUMMARY = "generated recipe based on logrotate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl libselinux pkgconfig-native popt"
RPM_SONAME_REQ_logrotate = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libpopt.so.0 libselinux.so.1"
RDEPENDS_logrotate = "bash coreutils glibc libacl libselinux popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/logrotate-3.14.0-3.el8.aarch64.rpm \
          "

SRC_URI[logrotate.sha256sum] = "c28aee1e2d4f0c2e20f5ca9096895bd36657c3438fe9b5a2a0d97e490a62109e"
