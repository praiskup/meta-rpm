SUMMARY = "generated recipe based on hunspell-et srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-et = "hunspell"
RDEPENDS_hyphen-et = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-et-0.20030606-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-et-0.20030606-19.el8.noarch.rpm \
          "

SRC_URI[hunspell-et.sha256sum] = "7b2038c7122ab8348c4a00ec0acaf49467e3174de6d724bb29c5ff2aa47d1570"
SRC_URI[hyphen-et.sha256sum] = "ed5d5f31354386ab74ff2f931d90a740fd1cc6636abdbc193ae3f863e98febd9"
