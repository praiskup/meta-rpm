SUMMARY = "generated recipe based on tpm2-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl openssl pkgconfig-native tpm2-tss"
RPM_SONAME_REQ_tpm2-tools = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libtss2-mu.so.0 libtss2-sys.so.0"
RDEPENDS_tpm2-tools = "glibc libcurl openssl-libs tpm2-tss"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm2-tools-3.2.1-1.el8.aarch64.rpm \
          "

SRC_URI[tpm2-tools.sha256sum] = "f3efa04c032f1f3afa5d51a066457d1dfb9243023af376d477373bed85e1c6ab"
