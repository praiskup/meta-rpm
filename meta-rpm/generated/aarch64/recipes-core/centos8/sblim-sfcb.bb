SUMMARY = "generated recipe based on sblim-sfcb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl openslp openssl pam pkgconfig-native sblim-sfccommon zlib"
RPM_SONAME_PROV_sblim-sfcb = "libcimcClientSfcbLocal.so.0 libsfcBasicAuthentication.so.0 libsfcBasicPAMAuthentication.so.0 libsfcBrokerCore.so.0 libsfcCertificateAuthentication.so.0 libsfcCimXmlCodec.so.0 libsfcClassProvider.so.0 libsfcClassProviderGz.so.0 libsfcClassProviderMem.so.0 libsfcClassProviderSf.so.0 libsfcCustomLib.so.0 libsfcElementCapabilitiesProvider.so.0 libsfcFileRepository.so.0 libsfcHttpAdapter.so.0 libsfcIndCIMXMLHandler.so.0 libsfcInternalProvider.so.0 libsfcInteropProvider.so.0 libsfcInteropServerProvider.so.0 libsfcObjectImplSwapI32toP32.so.0 libsfcProfileProvider.so.0 libsfcQualifierProvider.so.0"
RPM_SONAME_REQ_sblim-sfcb = "ld-linux-aarch64.so.1 libc.so.6 libcimcClientSfcbLocal.so.0 libcurl.so.4 libdl.so.2 libpam.so.0 libpthread.so.0 libsfcBasicAuthentication.so.0 libsfcBasicPAMAuthentication.so.0 libsfcBrokerCore.so.0 libsfcCertificateAuthentication.so.0 libsfcCimXmlCodec.so.0 libsfcClassProvider.so.0 libsfcClassProviderGz.so.0 libsfcClassProviderMem.so.0 libsfcClassProviderSf.so.0 libsfcCustomLib.so.0 libsfcElementCapabilitiesProvider.so.0 libsfcFileRepository.so.0 libsfcHttpAdapter.so.0 libsfcIndCIMXMLHandler.so.0 libsfcInternalProvider.so.0 libsfcInteropProvider.so.0 libsfcInteropServerProvider.so.0 libsfcObjectImplSwapI32toP32.so.0 libsfcProfileProvider.so.0 libsfcQualifierProvider.so.0 libsfcUtil.so.0 libslp.so.1 libssl.so.1.1 libz.so.1"
RDEPENDS_sblim-sfcb = "bash cim-schema glibc libcurl openslp openssl-libs pam perl-Getopt-Long perl-URI perl-interpreter perl-libs perl-libwww-perl sblim-sfcCommon systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sblim-sfcb-1.4.9-16.el8.aarch64.rpm \
          "

SRC_URI[sblim-sfcb.sha256sum] = "2b147ba8fedd50e34c922d08133348fba1b3511e4c565f8f7cbe5c280599ae50"
