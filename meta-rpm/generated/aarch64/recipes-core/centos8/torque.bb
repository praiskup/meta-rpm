SUMMARY = "generated recipe based on torque srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libxml2 munge openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_torque = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libmunge.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libtorque.so.2 libxml2.so.2 libz.so.1"
RDEPENDS_torque = "bash coreutils glibc grep libgcc libstdc++ libxml2 munge munge-libs openssl-libs setup systemd torque-libs xz-libs zlib"
RPM_SONAME_REQ_torque-devel = "libtorque.so.2"
RPROVIDES_torque-devel = "torque-dev (= 4.2.10)"
RDEPENDS_torque-devel = "bash torque-libs"
RPM_SONAME_PROV_torque-libs = "libtorque.so.2"
RPM_SONAME_REQ_torque-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libmunge.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_torque-libs = "glibc libgcc libstdc++ libxml2 munge munge-libs openssl-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/torque-libs-4.2.10-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/torque-4.2.10-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/torque-devel-4.2.10-17.el8.aarch64.rpm \
          "

SRC_URI[torque.sha256sum] = "4a04e508be43f883b0bfeadbd87621e53fe09d2e016559641cb582352c730397"
SRC_URI[torque-devel.sha256sum] = "268b50e6c5f7effaf4ffa45cd1d58e8356af2b32f3bc57b0087442bcdc9ea807"
SRC_URI[torque-libs.sha256sum] = "437f3c2082b7a1a3a960944bc4377c64149cc272b50992ccf63c4d8cf82ed92c"
