SUMMARY = "generated recipe based on perl-Time-HiRes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Time-HiRes = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 librt.so.1"
RDEPENDS_perl-Time-HiRes = "glibc perl-Carp perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Time-HiRes-1.9758-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Time-HiRes.sha256sum] = "8936c9f00f4a6579969e7408691244e765f4f31856b3eaf7650d51cd6006b9e9"
