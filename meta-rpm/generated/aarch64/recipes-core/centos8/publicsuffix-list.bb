SUMMARY = "generated recipe based on publicsuffix-list srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/publicsuffix-list-20180723-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/publicsuffix-list-dafsa-20180723-1.el8.noarch.rpm \
          "

SRC_URI[publicsuffix-list.sha256sum] = "4d1ffd3edba8c19303505b1e24d7dcb78e7216a49cc532077d9741fb5f557313"
SRC_URI[publicsuffix-list-dafsa.sha256sum] = "65ecf1e479dc2b2f7f09c2e86d7457043b3470b3eab3c4ee8909b50b0a669fc2"
