SUMMARY = "generated recipe based on libgudev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libgudev = "libgudev-1.0.so.0"
RPM_SONAME_REQ_libgudev = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libudev.so.1"
RDEPENDS_libgudev = "glib2 glibc systemd-libs"
RPM_SONAME_REQ_libgudev-devel = "libgudev-1.0.so.0"
RPROVIDES_libgudev-devel = "libgudev-dev (= 232)"
RDEPENDS_libgudev-devel = "glib2-devel libgudev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgudev-232-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgudev-devel-232-4.el8.aarch64.rpm \
          "

SRC_URI[libgudev.sha256sum] = "3580edd85564275b4f99bc0045d795f9d792e53215560ff955122abf67f83c21"
SRC_URI[libgudev-devel.sha256sum] = "674f249a4484d29c7b232faa520c7168273890a7d33874b7bffd7279e90552dc"
