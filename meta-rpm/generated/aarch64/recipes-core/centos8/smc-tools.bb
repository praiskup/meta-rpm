SUMMARY = "generated recipe based on smc-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native"
RPM_SONAME_PROV_smc-tools = "libsmc-preload.so.1"
RPM_SONAME_REQ_smc-tools = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnl-3.so.200 libnl-genl-3.so.200"
RDEPENDS_smc-tools = "bash glibc libnl3"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/smc-tools-1.2.2-3.el8.aarch64.rpm \
          "

SRC_URI[smc-tools.sha256sum] = "84a5d9573e8e53613fbf07c7993f7d72386a4b41f82b0012d79e20ab3c5c65b2"
