SUMMARY = "generated recipe based on systemd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "udev"
DEPENDS = "acl audit bzip2 cryptsetup-libs curl dbus-libs elfutils glib-2.0 gnutls iptables kmod libblkid libcap libgcc libgcrypt libgpg-error libidn2 libmicrohttpd libmount libpcre2 libseccomp libselinux libxcrypt lz4 pam pkgconfig-native systemd-libs xz zlib"
RPM_SONAME_PROV_systemd = "libsystemd-shared-239.so"
RPM_SONAME_REQ_systemd = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypt.so.1 libcryptsetup.so.12 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libgcrypt.so.20 libgnutls.so.30 libgpg-error.so.0 libidn2.so.0 libip4tc.so.2 libkmod.so.2 liblz4.so.1 liblzma.so.5 libm.so.6 libmount.so.1 libpam.so.0 libpcre2-8.so.0 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsystemd-shared-239.so libz.so.1"
RDEPENDS_systemd = "acl audit-libs bash bzip2-libs coreutils cryptsetup-libs dbus elfutils-libelf elfutils-libs glibc glibc-common gnutls grep iptables-libs kmod-libs libacl libblkid libcap libgcc libgcrypt libgpg-error libidn2 libmount libseccomp libselinux libxcrypt lz4-libs pam pcre2 sed shadow-utils systemd-libs systemd-pam util-linux xz-libs zlib"
RPM_SONAME_PROV_systemd-container = "libnss_mymachines.so.2"
RPM_SONAME_REQ_systemd-container = "ld-linux-aarch64.so.1 libacl.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libgcc_s.so.1 libgcrypt.so.20 liblzma.so.5 libmount.so.1 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsystemd-shared-239.so libz.so.1"
RDEPENDS_systemd-container = "bzip2-libs glibc libacl libcap libcurl libgcc libgcrypt libmount libseccomp libselinux systemd xz-libs zlib"
RPM_SONAME_REQ_systemd-devel = "libsystemd.so.0 libudev.so.1"
RPROVIDES_systemd-devel = "systemd-dev (= 239)"
RDEPENDS_systemd-devel = "systemd-libs systemd-pam"
RPM_SONAME_REQ_systemd-journal-remote = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libgcc_s.so.1 libgnutls.so.30 libmicrohttpd.so.12 libpthread.so.0 libsystemd-shared-239.so"
RDEPENDS_systemd-journal-remote = "bash firewalld-filesystem glibc glibc-common gnutls libcurl libgcc libmicrohttpd systemd"
RPM_SONAME_REQ_systemd-pam = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libgcc_s.so.1 libmount.so.1 libpam.so.0 libpam_misc.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_systemd-pam = "glibc libcap libgcc libmount pam systemd"
RPM_SONAME_REQ_systemd-tests = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libblkid.so.1 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libkmod.so.2 liblz4.so.1 libm.so.6 libmount.so.1 libpam.so.0 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsystemd-shared-239.so libsystemd.so.0 libudev.so.1 libz.so.1"
RDEPENDS_systemd-tests = "audit-libs dbus-libs glib2 glibc kmod-libs libacl libblkid libcap libgcc libgcrypt libgpg-error libmount libseccomp libselinux lz4-libs pam systemd systemd-libs zlib"
RPM_SONAME_REQ_systemd-udev = "ld-linux-aarch64.so.1 libacl.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libgcc_s.so.1 libkmod.so.2 libpthread.so.0 libsystemd-shared-239.so"
RPROVIDES_systemd-udev = "udev (= 239)"
RDEPENDS_systemd-udev = "bash cryptsetup-libs glibc grep kmod kmod-libs libacl libblkid libgcc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-239-31.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-container-239-31.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-devel-239-31.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-journal-remote-239-31.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-pam-239-31.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-tests-239-31.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-udev-239-31.el8_2.2.aarch64.rpm \
          "

SRC_URI[systemd.sha256sum] = "526919d1b449d0c586f5887d7359970c8fbcaa3490c657a4f101f7905835fe2a"
SRC_URI[systemd-container.sha256sum] = "2591acad8d15f6836bacc4c5fa0c8fdd426d527b804f9856c3af33b9a537ba79"
SRC_URI[systemd-devel.sha256sum] = "04111996724d55bb74f5c0313cffc288ea33d9117c5794e6b785db99eff300aa"
SRC_URI[systemd-journal-remote.sha256sum] = "8e4bba4e425115f14528a97e84bbe298874b542b911fb0cfdb9283a6c2288dde"
SRC_URI[systemd-pam.sha256sum] = "c611c3373a10c86bbf61d3b3e161b472f45b3fce72d5d76cee7d51a89da5f8c2"
SRC_URI[systemd-tests.sha256sum] = "573c4a5b4ffc75bb159ee6048e535f14b5372acb33ea4fa10439f1652d7516b0"
SRC_URI[systemd-udev.sha256sum] = "32ae9d6e950618de98e740716a613e99e0e1f6ef85ee60c19e1e1cc6148174a9"
