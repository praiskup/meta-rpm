SUMMARY = "generated recipe based on hunspell-sl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-sl-0.20070127-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-sl.sha256sum] = "3d807e4f0e387cd49b4337fa8034cd7002a19aa5f87413658fc72bbd9cb04dad"
