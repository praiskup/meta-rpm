SUMMARY = "generated recipe based on libbytesize srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libpcre mpfr pkgconfig-native"
RPM_SONAME_PROV_libbytesize = "libbytesize.so.1"
RPM_SONAME_REQ_libbytesize = "ld-linux-aarch64.so.1 libc.so.6 libgmp.so.10 libmpfr.so.4 libpcre.so.1"
RDEPENDS_libbytesize = "glibc gmp mpfr pcre"
RDEPENDS_python3-bytesize = "libbytesize platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libbytesize-1.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-bytesize-1.4-3.el8.aarch64.rpm \
          "

SRC_URI[libbytesize.sha256sum] = "a22ef4dc8fe6cb39b4d8d87eb2aa2378ae2868276c9dee632a3514788531c57b"
SRC_URI[python3-bytesize.sha256sum] = "54d228d1da27f95a7b1a25dd0133d486c153512eb53ef54dca06e58fca805dd3"
