SUMMARY = "generated recipe based on hyphen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_hyphen = "libhyphen.so.0"
RPM_SONAME_REQ_hyphen = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_hyphen = "glibc"
RPM_SONAME_REQ_hyphen-devel = "libhyphen.so.0"
RPROVIDES_hyphen-devel = "hyphen-dev (= 2.8.8)"
RDEPENDS_hyphen-devel = "hyphen perl-interpreter"
RDEPENDS_hyphen-en = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-2.8.8-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-en-2.8.8-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hyphen-devel-2.8.8-9.el8.aarch64.rpm \
          "

SRC_URI[hyphen.sha256sum] = "d3d9be00d46a720cd98ffcff5513d96059c4842534a2195cb020c7740a705d23"
SRC_URI[hyphen-devel.sha256sum] = "38d70ded9ae3a1c494dbce92f21e4cee4c1610a6cfce17bede614bfa1665c0d4"
SRC_URI[hyphen-en.sha256sum] = "a00d285ed333b46d8e1fd44498c7ca079d5fc1e1e31b709a405bee28d424afe3"
