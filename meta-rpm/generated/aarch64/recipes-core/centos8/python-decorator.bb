SUMMARY = "generated recipe based on python-decorator srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-decorator = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-decorator-4.2.1-2.el8.noarch.rpm \
          "

SRC_URI[python3-decorator.sha256sum] = "0e9a8a0f823bf0ef948862f0ccb62353460bd07931e756c98f23908c1c526578"
