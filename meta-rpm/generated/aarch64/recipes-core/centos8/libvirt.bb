SUMMARY = "generated recipe based on libvirt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl audit avahi-libs ceph curl cyrus-sasl-lib dbus-libs device-mapper-libs glusterfs gnutls libblkid libcap-ng libgcc libnl libpcap libpciaccess libselinux libssh libtirpc libuuid libxml2 netcf numactl parted pkgconfig-native readline sanlock systemd-libs yajl"
RDEPENDS_libvirt = "bash libvirt-client libvirt-daemon libvirt-daemon-config-network libvirt-daemon-config-nwfilter libvirt-daemon-driver-interface libvirt-daemon-driver-network libvirt-daemon-driver-nodedev libvirt-daemon-driver-nwfilter libvirt-daemon-driver-qemu libvirt-daemon-driver-secret libvirt-daemon-driver-storage libvirt-libs"
RPM_SONAME_REQ_libvirt-admin = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libreadline.so.7 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt-admin.so.0 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-admin = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-bash-completion libvirt-libs libxml2 numactl-libs readline yajl"
RPM_SONAME_REQ_libvirt-client = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libreadline.so.7 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-client = "audit-libs avahi-libs bash cyrus-sasl-lib dbus-libs device-mapper-libs gettext glibc gnutls gnutls-utils libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-bash-completion libvirt-libs libxml2 ncurses numactl-libs readline yajl"
RPM_SONAME_REQ_libvirt-daemon = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon = "audit-libs avahi-libs bash cyrus-sasl-lib dbus dbus-libs device-mapper-libs glibc gnutls iproute iproute-tc kmod libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-libs libxml2 numactl-libs numad polkit shadow-utils systemd yajl"
RDEPENDS_libvirt-daemon-config-network = "bash libvirt-daemon libvirt-daemon-driver-network"
RDEPENDS_libvirt-daemon-config-nwfilter = "bash libvirt-daemon libvirt-daemon-driver-nwfilter"
RPM_SONAME_PROV_libvirt-daemon-driver-interface = "libvirt_driver_interface.so"
RPM_SONAME_REQ_libvirt-daemon-driver-interface = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnetcf.so.1 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libudev.so.1 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-interface = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 netcf-libs numactl-libs systemd-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-network = "libvirt_driver_network.so"
RPM_SONAME_REQ_libvirt-daemon-driver-network = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-network = "audit-libs avahi-libs bash cyrus-sasl-lib dbus-libs device-mapper-libs dnsmasq glibc gnutls iptables libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs radvd yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-nodedev = "libvirt_driver_nodedev.so"
RPM_SONAME_REQ_libvirt-daemon-driver-nodedev = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpciaccess.so.0 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libudev.so.1 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-nodedev = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libpciaccess libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs systemd systemd-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-nwfilter = "libvirt_driver_nwfilter.so"
RPM_SONAME_REQ_libvirt-daemon-driver-nwfilter = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpcap.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-nwfilter = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls iptables iptables-ebtables libacl libcap-ng libcurl libnl3 libpcap libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-qemu = "libvirt_driver_qemu.so"
RPM_SONAME_REQ_libvirt-daemon-driver-qemu = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-qemu = "audit-libs avahi-libs bash bzip2 cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls gzip libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-daemon-driver-network libvirt-daemon-driver-storage-core libvirt-libs libxml2 lzop numactl-libs qemu-img systemd-container xz yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-secret = "libvirt_driver_secret.so"
RPM_SONAME_REQ_libvirt-daemon-driver-secret = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-secret = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs yajl"
RDEPENDS_libvirt-daemon-driver-storage = "libvirt-daemon-driver-storage-core libvirt-daemon-driver-storage-disk libvirt-daemon-driver-storage-gluster libvirt-daemon-driver-storage-iscsi libvirt-daemon-driver-storage-logical libvirt-daemon-driver-storage-mpath libvirt-daemon-driver-storage-rbd libvirt-daemon-driver-storage-scsi"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-core = "libvirt_driver_storage.so libvirt_storage_backend_fs.so libvirt_storage_file_fs.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-core = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libblkid.so.1 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libparted.so.2 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-core = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libblkid libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 nfs-utils numactl-libs parted qemu-img util-linux yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-disk = "libvirt_storage_backend_disk.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-disk = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-disk = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs parted yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-gluster = "libvirt_storage_backend_gluster.so libvirt_storage_file_gluster.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-gluster = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgfapi.so.0 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libuuid.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-gluster = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc glusterfs-api glusterfs-cli glusterfs-libs gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libuuid libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-iscsi = "libvirt_storage_backend_iscsi.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-iscsi = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-iscsi = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls iscsi-initiator-utils libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-logical = "libvirt_storage_backend_logical.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-logical = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-logical = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 lvm2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-mpath = "libvirt_storage_backend_mpath.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-mpath = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-mpath = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-rbd = "libvirt_storage_backend_rbd.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-rbd = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 librados.so.2 librbd.so.1 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-rbd = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 librados2 librbd1 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-scsi = "libvirt_storage_backend_scsi.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-scsi = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-scsi = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RDEPENDS_libvirt-daemon-kvm = "libvirt-daemon libvirt-daemon-driver-interface libvirt-daemon-driver-network libvirt-daemon-driver-nodedev libvirt-daemon-driver-nwfilter libvirt-daemon-driver-qemu libvirt-daemon-driver-secret libvirt-daemon-driver-storage qemu-kvm"
RPM_SONAME_REQ_libvirt-devel = "libvirt-admin.so.0 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0"
RPROVIDES_libvirt-devel = "libvirt-dev (= 4.5.0)"
RDEPENDS_libvirt-devel = "libvirt-libs pkgconf-pkg-config"
RPM_SONAME_PROV_libvirt-libs = "libvirt-admin.so.0 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0"
RPM_SONAME_REQ_libvirt-libs = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-libs = "audit-libs avahi-libs cyrus-sasl cyrus-sasl-gssapi cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libxml2 nmap-ncat numactl-libs yajl"
RPM_SONAME_REQ_libvirt-lock-sanlock = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsanlock_client.so.1 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-lock-sanlock = "audit-libs augeas avahi-libs bash cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs sanlock sanlock-lib yajl"
RPM_SONAME_PROV_libvirt-nss = "libnss_libvirt.so.2 libnss_libvirt_guest.so.2"
RPM_SONAME_REQ_libvirt-nss = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 libtirpc.so.3 libutil.so.1 libyajl.so.2"
RDEPENDS_libvirt-nss = "glibc libgcc libtirpc libvirt-daemon-driver-network yajl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-admin-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-bash-completion-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-client-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-config-network-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-config-nwfilter-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-interface-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-network-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-nodedev-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-nwfilter-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-qemu-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-secret-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-core-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-disk-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-gluster-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-iscsi-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-logical-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-mpath-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-rbd-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-driver-storage-scsi-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-daemon-kvm-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-devel-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-docs-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-libs-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-lock-sanlock-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-nss-4.5.0-42.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[libvirt.sha256sum] = "7f389605d6f318059e8cde6f997aaa1358f934bceaa6973bf1898214337bb34d"
SRC_URI[libvirt-admin.sha256sum] = "7fa3a4d71d719babb582232e729f83571b7d07c3196d6996be2db2f255e55f4d"
SRC_URI[libvirt-bash-completion.sha256sum] = "1499266b1b47565157454eb47f3251a5a2e24d907afac54151f6cb5f26e51440"
SRC_URI[libvirt-client.sha256sum] = "04a61acf1c192e43541020b093a37c3f35e1b76e8074842d9a0aa58138c3b4a4"
SRC_URI[libvirt-daemon.sha256sum] = "1c0cf86a217a3e5fa4b06565782c03dd0ac944b83d1a346b3eec21a67ff8cc54"
SRC_URI[libvirt-daemon-config-network.sha256sum] = "fbc3eaac0ceb129349e347180633b0a4e53a5c492769115d0f63c722054ea2bd"
SRC_URI[libvirt-daemon-config-nwfilter.sha256sum] = "0e6a56f3bbd897c3737e690cf1d4717a30365ccfea741fc878fb283360d93f8d"
SRC_URI[libvirt-daemon-driver-interface.sha256sum] = "1bf3a92703dd86ec9fa40fa28e6f20b7f9d41ee0b3af966c3a3379b845407398"
SRC_URI[libvirt-daemon-driver-network.sha256sum] = "ae8a110593860bf6094c3228f81986df2c2758f8b1aa71647f3b1ab565468536"
SRC_URI[libvirt-daemon-driver-nodedev.sha256sum] = "96aee65c7cd4521922e5ac89fd2ef116c582203cbcd290e8c183648d97fdc240"
SRC_URI[libvirt-daemon-driver-nwfilter.sha256sum] = "eaa2f24db1fbb94fa69205b5ad5d14b0dbf5a5d55af97540d3a6a7e923fff764"
SRC_URI[libvirt-daemon-driver-qemu.sha256sum] = "d533ff4d8a72c322f9c3cf7fb781dda956bd4a72111e65bfcbe046e91ce213c7"
SRC_URI[libvirt-daemon-driver-secret.sha256sum] = "e4217677e9e3497ab96c609d4417e56adf71c22e251982d38ffebc8d5f50e314"
SRC_URI[libvirt-daemon-driver-storage.sha256sum] = "7b397a92f0da76f528e4f7f22a74feb172103bd5a879b8eb16bc4182ff5455f1"
SRC_URI[libvirt-daemon-driver-storage-core.sha256sum] = "faf98b0a701a36adb7799309ce8384a66b063aa4c8e7e4aeee45df643d9f71bd"
SRC_URI[libvirt-daemon-driver-storage-disk.sha256sum] = "6a2e674610f4e37442e73a6404346e190aaeef8ea905788253fa43ace13bf3a4"
SRC_URI[libvirt-daemon-driver-storage-gluster.sha256sum] = "0205d58d14cbc3a2119ce3cbba87105c1ac9b1e4c9b549ef713fab8781ff335c"
SRC_URI[libvirt-daemon-driver-storage-iscsi.sha256sum] = "fbc083fae09087a59ec66ed9e7237db4741c988e859d8fce5d8b854e2cbb7264"
SRC_URI[libvirt-daemon-driver-storage-logical.sha256sum] = "c6567e461169124085849ea164cd0aa735a9a8dcc547a8179642886f2098920b"
SRC_URI[libvirt-daemon-driver-storage-mpath.sha256sum] = "2a4dda59ad507128aea48e56962432d6e26331a92fe80c298de7c4b21e59b9d6"
SRC_URI[libvirt-daemon-driver-storage-rbd.sha256sum] = "4078a7cd002ecff760154b2615b20d37f2633d1890fb18ad47de50f37957d662"
SRC_URI[libvirt-daemon-driver-storage-scsi.sha256sum] = "fb1cf1ae31aab2cc0e32cfa55a9bec8dc64b56270efb387e7cd8df722689597d"
SRC_URI[libvirt-daemon-kvm.sha256sum] = "bf54131fcb32a6a2cb28c553a8e63326194fd7c9fd4e88ea9ce1a71c3530f5d8"
SRC_URI[libvirt-devel.sha256sum] = "ae9395b2894ed991bfa249182895cf3cbd22cef3bfde294ea7693a3aba3a71d9"
SRC_URI[libvirt-docs.sha256sum] = "5f4377da2a5a4faa9e27133f1d731d98c83972f6a03694a5bd57be26a9b476d1"
SRC_URI[libvirt-libs.sha256sum] = "f5667bcc57136183d4c02506d501aeb8e1c3798abe422ce2361463d96bf226e9"
SRC_URI[libvirt-lock-sanlock.sha256sum] = "34e1cc8f1c1bfb10e2d492f42e79223e407faf6cef43f75e347cb29ea18d0fb9"
SRC_URI[libvirt-nss.sha256sum] = "718ab8eb6981c513416a515b23cd5924d46adceb7a0e306cbdd97a636a162f03"
