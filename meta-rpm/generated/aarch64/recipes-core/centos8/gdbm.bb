SUMMARY = "generated recipe based on gdbm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_gdbm = "ld-linux-aarch64.so.1 libc.so.6 libgdbm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_gdbm = "gdbm-libs glibc ncurses-libs readline"
RPM_SONAME_REQ_gdbm-devel = "libgdbm.so.6 libgdbm_compat.so.4"
RPROVIDES_gdbm-devel = "gdbm-dev (= 1.18)"
RDEPENDS_gdbm-devel = "bash gdbm gdbm-libs info"
RPM_SONAME_PROV_gdbm-libs = "libgdbm.so.6 libgdbm_compat.so.4"
RPM_SONAME_REQ_gdbm-libs = "ld-linux-aarch64.so.1 libc.so.6 libgdbm.so.6"
RDEPENDS_gdbm-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gdbm-1.18-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gdbm-devel-1.18-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gdbm-libs-1.18-1.el8.aarch64.rpm \
          "

SRC_URI[gdbm.sha256sum] = "b7d0b4b922429354ffe7ddac90c8cd448229571b8d8e4c342110edadfe809f99"
SRC_URI[gdbm-devel.sha256sum] = "5120279815eabe953cc2ad4b4a039ab9d09f3050d32c25995f95b0c3dd8a0a35"
SRC_URI[gdbm-libs.sha256sum] = "a7d04ae40ad91ba0ea93e4971a35585638f6adf8dbe1ed4849f643b6b64a5871"
