SUMMARY = "generated recipe based on perl-File-Temp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Temp = "perl-Carp perl-Errno perl-Exporter perl-File-Path perl-IO perl-PathTools perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-File-Temp-0.230.600-1.el8.noarch.rpm \
          "

SRC_URI[perl-File-Temp.sha256sum] = "e269f7d33abbb790311ffa95fa7df9766cac8bf31ace24fce6ed732ba0db19ae"
