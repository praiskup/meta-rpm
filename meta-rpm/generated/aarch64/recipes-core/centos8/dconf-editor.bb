SUMMARY = "generated recipe based on dconf-editor srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dconf glib-2.0 gtk+3 pkgconfig-native"
RPM_SONAME_REQ_dconf-editor = "ld-linux-aarch64.so.1 libc.so.6 libdconf.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpthread.so.0"
RDEPENDS_dconf-editor = "dconf glib2 glibc gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dconf-editor-3.28.0-1.el8.aarch64.rpm \
          "

SRC_URI[dconf-editor.sha256sum] = "c7a8e56a04fd3c99be1eb080a3c6a7501ab07be99cb1255e8faeada9f4483582"
