SUMMARY = "generated recipe based on virt-manager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_virt-install = "bash libvirt-client platform-python virt-manager-common"
RDEPENDS_virt-manager = "bash dconf gtk-vnc2 gtk3 gtksourceview3 libvirt-glib platform-python python3-gobject spice-gtk3 virt-manager-common vte291"
RDEPENDS_virt-manager-common = "genisoimage libosinfo python3-argcomplete python3-gobject-base python3-libvirt python3-libxml2 python3-requests"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/virt-install-2.2.1-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/virt-manager-2.2.1-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/virt-manager-common-2.2.1-3.el8.noarch.rpm \
          "

SRC_URI[virt-install.sha256sum] = "5f2aada1445eb161b49c270213bf21bb2164961e35309a6261164aedf8a00fec"
SRC_URI[virt-manager.sha256sum] = "67cab8df97b933c11f9a2755c642bdc32b46d8f4387c9a5310c5c128c6d64f2f"
SRC_URI[virt-manager-common.sha256sum] = "3e50d01daae8f92134b936ef709d099cb4f61edf1b12cbba0dfa253fa280b6c2"
