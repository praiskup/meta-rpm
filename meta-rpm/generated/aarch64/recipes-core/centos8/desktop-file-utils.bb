SUMMARY = "generated recipe based on desktop-file-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_desktop-file-utils = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0"
RDEPENDS_desktop-file-utils = "bash emacs-filesystem glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/desktop-file-utils-0.23-8.el8.aarch64.rpm \
          "

SRC_URI[desktop-file-utils.sha256sum] = "2ad0395b94f9cb92040fed4377dc381719f209318759f8c346db0210ce502fb1"
