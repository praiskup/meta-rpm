SUMMARY = "generated recipe based on perl-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-srpm-macros-1-25.el8.noarch.rpm \
          "

SRC_URI[perl-srpm-macros.sha256sum] = "486f391da85c02eaffba41bb2d661e3af52c07ed6b614ec1ebb1eb192dd78784"
