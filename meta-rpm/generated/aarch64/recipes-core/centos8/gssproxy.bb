SUMMARY = "generated recipe based on gssproxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ding-libs e2fsprogs krb5-libs libselinux libverto pkgconfig-native popt"
RPM_SONAME_REQ_gssproxy = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libgssapi_krb5.so.2 libgssrpc.so.4 libini_config.so.5 libk5crypto.so.3 libkrb5.so.3 libpopt.so.0 libpthread.so.0 libref_array.so.1 libselinux.so.1 libverto.so.1"
RDEPENDS_gssproxy = "bash glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libini_config libref_array libselinux libverto libverto-libevent popt systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gssproxy-0.8.0-15.el8.aarch64.rpm \
          "

SRC_URI[gssproxy.sha256sum] = "a56497f75efd054c12876b8d3d1344a2173357489ad9f193f52525eb40b00679"
