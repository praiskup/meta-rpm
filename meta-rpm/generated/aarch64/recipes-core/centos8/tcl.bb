SUMMARY = "generated recipe based on tcl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_tcl = "libtcl8.6.so"
RPM_SONAME_REQ_tcl = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libtcl8.6.so libz.so.1"
RDEPENDS_tcl = "glibc zlib"
RPROVIDES_tcl-devel = "tcl-dev (= 8.6.8)"
RDEPENDS_tcl-devel = "pkgconf-pkg-config tcl zlib-devel"
RDEPENDS_tcl-doc = "tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tcl-8.6.8-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tcl-devel-8.6.8-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tcl-doc-8.6.8-2.el8.noarch.rpm \
          "

SRC_URI[tcl.sha256sum] = "db7ecf9220877234c1acca290a1c1875c593cdf99b02aac6db3497b531bad59c"
SRC_URI[tcl-devel.sha256sum] = "a5ac8b3d4f32c255853d627eaa3faca16959a708fc771c97b8064fcd38c6aa61"
SRC_URI[tcl-doc.sha256sum] = "b7804f68ff5391ec2132c2a46e3fd2ca8ebc287271c2c6aa827b9f6ac5bc7204"
