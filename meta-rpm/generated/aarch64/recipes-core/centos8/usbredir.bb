SUMMARY = "generated recipe based on usbredir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_PROV_usbredir = "libusbredirhost.so.1 libusbredirparser.so.1"
RPM_SONAME_REQ_usbredir = "ld-linux-aarch64.so.1 libc.so.6 libusb-1.0.so.0 libusbredirparser.so.1"
RDEPENDS_usbredir = "glibc libusbx"
RPM_SONAME_REQ_usbredir-devel = "libusbredirhost.so.1 libusbredirparser.so.1"
RPROVIDES_usbredir-devel = "usbredir-dev (= 0.8.0)"
RDEPENDS_usbredir-devel = "libusbx-devel pkgconf-pkg-config usbredir"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/usbredir-0.8.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/usbredir-devel-0.8.0-1.el8.aarch64.rpm \
          "

SRC_URI[usbredir.sha256sum] = "21fe893ac05f3f0608bd2f6a419b7268bfd2b4ed0b97086d994c9aead8eb075d"
SRC_URI[usbredir-devel.sha256sum] = "8ff5bca7dc418d9a8008c2a2383185c5e19c3f5df105047ddeb9c21f566ae244"
