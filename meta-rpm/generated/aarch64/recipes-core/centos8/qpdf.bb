SUMMARY = "generated recipe based on qpdf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcc libjpeg-turbo pkgconfig-native zlib"
RPM_SONAME_REQ_qpdf = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libqpdf.so.18 libstdc++.so.6 libz.so.1"
RDEPENDS_qpdf = "glibc gnutls libgcc libjpeg-turbo libstdc++ perl-interpreter perl-libs qpdf-libs zlib"
RDEPENDS_qpdf-doc = "qpdf-libs"
RPM_SONAME_PROV_qpdf-libs = "libqpdf.so.18"
RPM_SONAME_REQ_qpdf-libs = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_qpdf-libs = "glibc gnutls libgcc libjpeg-turbo libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qpdf-7.1.1-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qpdf-doc-7.1.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qpdf-libs-7.1.1-10.el8.aarch64.rpm \
          "

SRC_URI[qpdf.sha256sum] = "06e24f06ba35852553a006391be3e11e8026f827966163fcf7c01016f5466de8"
SRC_URI[qpdf-doc.sha256sum] = "521fabf0e78fb33b4d1a482fc70eac7af2e506c161813324cc2b0ab513623c26"
SRC_URI[qpdf-libs.sha256sum] = "e6d68c42a628c54efb1526a387c73d26b91c86fc39bfd87f96a5adb5ec7bd344"
