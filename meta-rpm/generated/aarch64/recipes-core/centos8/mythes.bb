SUMMARY = "generated recipe based on mythes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_mythes = "libmythes-1.2.so.0"
RPM_SONAME_REQ_mythes = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_mythes = "glibc libgcc libstdc++"
RPM_SONAME_REQ_mythes-devel = "libmythes-1.2.so.0"
RPROVIDES_mythes-devel = "mythes-dev (= 1.2.4)"
RDEPENDS_mythes-devel = "mythes perl-interpreter pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-1.2.4-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mythes-devel-1.2.4-9.el8.aarch64.rpm \
          "

SRC_URI[mythes.sha256sum] = "c60c9d37f10491247f5731254adf92afaafae31cba8e85784db42c7d1b3e2ed3"
SRC_URI[mythes-devel.sha256sum] = "536ef9a4f0931438612d4978839114b53db5e07d621c99173562be89ebddb8e0"
