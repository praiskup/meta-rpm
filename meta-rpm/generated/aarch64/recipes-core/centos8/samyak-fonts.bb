SUMMARY = "generated recipe based on samyak-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_samyak-devanagari-fonts = "samyak-fonts-common"
RDEPENDS_samyak-fonts-common = "fontpackages-filesystem"
RDEPENDS_samyak-gujarati-fonts = "samyak-fonts-common"
RDEPENDS_samyak-malayalam-fonts = "samyak-fonts-common"
RDEPENDS_samyak-odia-fonts = "samyak-fonts-common"
RDEPENDS_samyak-tamil-fonts = "samyak-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/samyak-devanagari-fonts-1.2.2-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/samyak-fonts-common-1.2.2-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/samyak-gujarati-fonts-1.2.2-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/samyak-malayalam-fonts-1.2.2-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/samyak-odia-fonts-1.2.2-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/samyak-tamil-fonts-1.2.2-19.el8.noarch.rpm \
          "

SRC_URI[samyak-devanagari-fonts.sha256sum] = "adbbee09c755fe2c31b412742313053253e3a55f29c1cd9453331e0af8e71a98"
SRC_URI[samyak-fonts-common.sha256sum] = "ffee3d7e49932c10a75bf28916d47835e865dab128ab8f87b8acb4e403ffe6e3"
SRC_URI[samyak-gujarati-fonts.sha256sum] = "8cdc93f94b05ac32a5669b5b40d2abdcb83dacb2fc0d2a10a70aca02b68bb474"
SRC_URI[samyak-malayalam-fonts.sha256sum] = "8277230f3e335f5101e24dc709a53f91ce63faa1f1767f4dd253cba51d9ae8af"
SRC_URI[samyak-odia-fonts.sha256sum] = "3efba91855ca65fd5a3f402e9c630e5163a382ec0a293ef75117001c0cbfae99"
SRC_URI[samyak-tamil-fonts.sha256sum] = "086437f7c5efbef51025151908d304df3ecae48e98536d8d177750871a999626"
