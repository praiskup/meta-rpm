SUMMARY = "generated recipe based on netpbm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jasper jbigkit libjpeg-turbo libpng libx11 libxml2 pkgconfig-native tiff zlib"
RPM_SONAME_PROV_netpbm = "libnetpbm.so.11"
RPM_SONAME_REQ_netpbm = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_netpbm = "glibc"
RPROVIDES_netpbm-devel = "netpbm-dev (= 10.82.00)"
RDEPENDS_netpbm-devel = "netpbm"
RDEPENDS_netpbm-doc = "netpbm-progs"
RPM_SONAME_REQ_netpbm-progs = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libjasper.so.4 libjbig.so.2.1 libjpeg.so.62 libm.so.6 libnetpbm.so.11 libpng16.so.16 libtiff.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_netpbm-progs = "bash ghostscript glibc jasper-libs jbigkit-libs libX11 libjpeg-turbo libpng libtiff libxml2 netpbm perl-Errno perl-File-Temp perl-Getopt-Long perl-PathTools perl-interpreter perl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/netpbm-10.82.00-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/netpbm-progs-10.82.00-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/netpbm-devel-10.82.00-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/netpbm-doc-10.82.00-6.el8.aarch64.rpm \
          "

SRC_URI[netpbm.sha256sum] = "a660c8132676f5e314e8902b237dcb632df53c72d31dd93adbe4e3a3b8f3c1e5"
SRC_URI[netpbm-devel.sha256sum] = "a56a80c5d06b3eb3fbc62e7366044bcd1fdef23365db4bab4c49ad5bcc519020"
SRC_URI[netpbm-doc.sha256sum] = "33a6fb237cd1e439efd0995ec12d62c6ef0b3712479bcee1914f6afb139475b3"
SRC_URI[netpbm-progs.sha256sum] = "427fd888e4c8acb521ac2e5e5a5aae4962286f6ba424df47662724989ac96575"
