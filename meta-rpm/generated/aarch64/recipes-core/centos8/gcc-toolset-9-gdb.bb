SUMMARY = "generated recipe based on gcc-toolset-9-gdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "babeltrace boost expat gmp guile libgcc libselinux mpfr ncurses pkgconfig-native platform-python3 readline source-highlight xz zlib"
RPM_SONAME_REQ_gcc-toolset-9-gdb = "ld-linux-aarch64.so.1 libbabeltrace-ctf.so.1 libbabeltrace.so.1 libboost_regex.so.1.66.0 libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libgmp.so.10 libguile-2.0.so.22 liblzma.so.5 libm.so.6 libmpfr.so.4 libncursesw.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreadline.so.7 libselinux.so.1 libsource-highlight.so.4 libstdc++.so.6 libtinfo.so.6 libutil.so.1 libz.so.1"
RDEPENDS_gcc-toolset-9-gdb = "bash boost-regex coreutils expat gcc-toolset-9-runtime glibc gmp guile libbabeltrace libgcc libselinux libstdc++ mpfr ncurses-libs python3-libs readline source-highlight xz-libs zlib"
RDEPENDS_gcc-toolset-9-gdb-doc = "bash gcc-toolset-9-runtime info"
RPM_SONAME_PROV_gcc-toolset-9-gdb-gdbserver = "libinproctrace.so"
RPM_SONAME_REQ_gcc-toolset-9-gdb-gdbserver = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libselinux.so.1 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-gdb-gdbserver = "gcc-toolset-9-runtime glibc libgcc libselinux libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gdb-8.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gdb-doc-8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gdb-gdbserver-8.3-1.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-gdb.sha256sum] = "495291f08268046ffcb098d76b97bab512d6ca68a6b953d42cf6c65f71129a8d"
SRC_URI[gcc-toolset-9-gdb-doc.sha256sum] = "4d2ffa1e5efc6d9f80200f531f6b15bb66a0fe95637917a8afaae3117c023f6c"
SRC_URI[gcc-toolset-9-gdb-gdbserver.sha256sum] = "6c2f7cd093f3b67e8d42f51c62db4495c5c69e68de90cb2d4cc0614d351091f7"
