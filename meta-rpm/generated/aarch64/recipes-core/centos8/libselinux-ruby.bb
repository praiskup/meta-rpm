SUMMARY = "generated recipe based on libselinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native ruby"
RPM_SONAME_REQ_libselinux-ruby = "ld-linux-aarch64.so.1 libc.so.6 libruby.so.2.5 libselinux.so.1"
RDEPENDS_libselinux-ruby = "glibc libselinux ruby-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libselinux-ruby-2.9-3.el8.aarch64.rpm \
          "

SRC_URI[libselinux-ruby.sha256sum] = "b1c8fd39187b9632845e1e7f91e6157a1a2f888743a1c62a3634b9df1ddba236"
