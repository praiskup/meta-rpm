SUMMARY = "generated recipe based on pcaudiolib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native pulseaudio"
RPM_SONAME_PROV_pcaudiolib = "libpcaudio.so.0"
RPM_SONAME_REQ_pcaudiolib = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libpulse-simple.so.0 libpulse.so.0"
RDEPENDS_pcaudiolib = "alsa-lib glibc pulseaudio-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcaudiolib-1.1-2.el8.aarch64.rpm \
          "

SRC_URI[pcaudiolib.sha256sum] = "762119b4250c64a61c5a4424d293c377574da8e4709362e49b3fa91064de0783"
