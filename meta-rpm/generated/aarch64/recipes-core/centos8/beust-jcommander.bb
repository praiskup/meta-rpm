SUMMARY = "generated recipe based on beust-jcommander srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_beust-jcommander = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_beust-jcommander-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/beust-jcommander-1.71-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/beust-jcommander-javadoc-1.71-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[beust-jcommander.sha256sum] = "f2a147c776b16b1cb233552f350fad6026a446fcb46db6f7cea0d1e8e8565c8b"
SRC_URI[beust-jcommander-javadoc.sha256sum] = "ca40024c8b67282db017e97cf78a81f2115dbe0049b373f1e2701c68e58b1197"
