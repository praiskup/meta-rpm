SUMMARY = "generated recipe based on libestr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libestr = "libestr.so.0"
RPM_SONAME_REQ_libestr = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libestr = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libestr-0.1.10-1.el8.aarch64.rpm \
          "

SRC_URI[libestr.sha256sum] = "d44031d4e8ad270fd07b56002667c5dcdca056dda326e69baf0326c76a3147e2"
