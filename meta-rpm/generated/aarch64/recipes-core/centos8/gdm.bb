SUMMARY = "generated recipe based on gdm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "accountsservice atk audit cairo gdk-pixbuf glib-2.0 gtk+3 keyutils libgcc libselinux libx11 libxau libxcb libxdmcp pam pango pkgconfig-native systemd-libs"
RPM_SONAME_PROV_gdm = "libgdm.so.1"
RPM_SONAME_REQ_gdm = "ld-linux-aarch64.so.1 libX11.so.6 libXau.so.6 libXdmcp.so.6 libaccountsservice.so.0 libatk-1.0.so.0 libaudit.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgdm.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libkeyutils.so.1 libpam.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libutil.so.1 libxcb.so.1"
RDEPENDS_gdm = "accountsservice accountsservice-libs atk audit-libs bash cairo cairo-gobject centos-logos dconf gdk-pixbuf2 glib2 glibc gnome-keyring-pam gnome-session gnome-session-wayland-session gnome-settings-daemon gnome-shell gtk3 iso-codes keyutils-libs libX11 libXau libXdmcp libgcc libselinux libxcb pam pango shadow-utils systemd systemd-libs util-linux xorg-x11-server-utils xorg-x11-xinit xorg-x11-xkb-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdm-3.28.3-29.el8.aarch64.rpm \
          "

SRC_URI[gdm.sha256sum] = "7c08020b8f09b85e82002de74ac1ddd513e07c3fd63e037722d0bfb5f18c91f4"
