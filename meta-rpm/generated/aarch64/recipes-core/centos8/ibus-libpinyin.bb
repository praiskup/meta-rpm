SUMMARY = "generated recipe based on ibus-libpinyin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 ibus libgcc libpinyin lua pkgconfig-native sqlite3"
RPM_SONAME_REQ_ibus-libpinyin = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 liblua-5.3.so libm.so.6 libpinyin.so.13 libpthread.so.0 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_ibus-libpinyin = "bash glib2 glibc ibus ibus-libs libgcc libpinyin libpinyin-data libstdc++ lua-libs python3-gobject sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-libpinyin-1.10.0-1.el8.aarch64.rpm \
          "

SRC_URI[ibus-libpinyin.sha256sum] = "66efccbc3475b20715c3cd08ca416886aac343253591c257e49a1170f7cf88ee"
