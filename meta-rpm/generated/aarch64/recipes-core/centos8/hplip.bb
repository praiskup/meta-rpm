SUMMARY = "generated recipe based on hplip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs dbus-libs libgcc libjpeg-turbo libusb1 net-snmp openssl pkgconfig-native sane-backends zlib"
RPM_SONAME_REQ_hplip = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcups.so.2 libcupsimage.so.2 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libhpdiscovery.so.0 libhpip.so.0 libhpmud.so.0 libjpeg.so.62 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libstdc++.so.6 libusb-1.0.so.0 libz.so.1"
RDEPENDS_hplip = "bash cups cups-libs dbus-libs glibc gnupg2 hplip-libs libgcc libjpeg-turbo libstdc++ libusbx net-snmp-libs openssl-libs platform-python python3-dbus python3-pillow systemd wget zlib"
RDEPENDS_hplip-gui = "hplip libsane-hpaio platform-python python3-gobject python3-qt5 python3-reportlab"
RPM_SONAME_PROV_hplip-libs = "libhpdiscovery.so.0 libhpip.so.0 libhpipp.so.0 libhpmud.so.0"
RPM_SONAME_REQ_hplip-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcups.so.2 libdl.so.2 libhpdiscovery.so.0 libhpipp.so.0 libhpmud.so.0 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libsane.so.1 libusb-1.0.so.0"
RDEPENDS_hplip-libs = "cups-libs glibc hplip-common libusbx net-snmp-libs openssl-libs platform-python sane-backends-libs"
RPM_SONAME_PROV_libsane-hpaio = "libsane-hpaio.so.1"
RPM_SONAME_REQ_libsane-hpaio = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcups.so.2 libdbus-1.so.3 libdl.so.2 libhpdiscovery.so.0 libhpip.so.0 libhpipp.so.0 libhpmud.so.0 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libsane-hpaio.so.1 libusb-1.0.so.0"
RDEPENDS_libsane-hpaio = "cups-libs dbus-libs glibc hplip-libs libusbx net-snmp-libs openssl-libs sane-backends"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hplip-3.18.4-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hplip-common-3.18.4-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hplip-gui-3.18.4-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hplip-libs-3.18.4-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsane-hpaio-3.18.4-9.el8.aarch64.rpm \
          "

SRC_URI[hplip.sha256sum] = "5ac1695e1b437c870eea648dc1e7548e602f7d3b4ebe8d87a49125a9b3fd0518"
SRC_URI[hplip-common.sha256sum] = "1c2d79f6f3a8342ea0f916ee229d53ce0b1b1e4ef2fe6c4be544e9066e202010"
SRC_URI[hplip-gui.sha256sum] = "5cf7516a66279c56dd4b1e88f53b7a03db08904d3fddaf7262bbeb27f3b5ca07"
SRC_URI[hplip-libs.sha256sum] = "cb1ffc6687eee4fcb58d5f0733cde9ceb155f9337d31eb12e296594cbad40b8a"
SRC_URI[libsane-hpaio.sha256sum] = "8d981036b9a687ec89ebab2b35781803a6ff38cd0884e48bce0e7700a6c4deb0"
