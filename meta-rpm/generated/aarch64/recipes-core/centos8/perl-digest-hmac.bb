SUMMARY = "generated recipe based on perl-Digest-HMAC srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Digest-HMAC = "perl-Digest-MD5 perl-Digest-SHA perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Digest-HMAC-1.03-17.el8.noarch.rpm \
          "

SRC_URI[perl-Digest-HMAC.sha256sum] = "3452f53c4c9b161fa240a70037ac76961b4b176903ec7dd633bcc90c61c7e88c"
