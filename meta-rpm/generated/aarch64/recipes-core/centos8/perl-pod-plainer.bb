SUMMARY = "generated recipe based on perl-Pod-Plainer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Plainer = "perl-Pod-Parser perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Pod-Plainer-1.04-7.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Plainer.sha256sum] = "ce5a3b4ee2d8aaca15a59ca873bcf5aeb4ea5e8f2d8be537aa0248997f9fa76f"
