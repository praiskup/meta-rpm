SUMMARY = "generated recipe based on libatasmart srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libatasmart = "libatasmart.so.4"
RPM_SONAME_REQ_libatasmart = "ld-linux-aarch64.so.1 libatasmart.so.4 libc.so.6 libudev.so.1"
RDEPENDS_libatasmart = "glibc systemd-libs"
RPM_SONAME_REQ_libatasmart-devel = "libatasmart.so.4"
RPROVIDES_libatasmart-devel = "libatasmart-dev (= 0.19)"
RDEPENDS_libatasmart-devel = "libatasmart pkgconf-pkg-config vala"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libatasmart-0.19-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libatasmart-devel-0.19-14.el8.aarch64.rpm \
          "

SRC_URI[libatasmart.sha256sum] = "2cb6c88868446219d7cb277e3592f10aa233c9f8cecda16594a21ef35e212565"
SRC_URI[libatasmart-devel.sha256sum] = "3c8fc0cb61b70611d50779e676c1683f4b93f1675e726db6eb8cc89efce2e264"
