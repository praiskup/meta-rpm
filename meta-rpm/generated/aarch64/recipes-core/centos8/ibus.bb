SUMMARY = "generated recipe based on ibus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus dbus-libs dconf fontconfig freetype gdk-pixbuf glib-2.0 gobject-introspection gtk+3 gtk2 libnotify libx11 libxi libxkbcommon pango pkgconfig-native wayland"
RPM_SONAME_REQ_ibus = "ld-linux-aarch64.so.1 libX11.so.6 libXi.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdconf.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libibus-1.0.so.5 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus = "atk bash cairo cairo-gobject chkconfig dbus-x11 dconf desktop-file-utils gdk-pixbuf2 glib2 glibc gtk3 ibus-gtk2 ibus-gtk3 ibus-libs ibus-setup iso-codes libX11 libXi libnotify pango platform-python python3-gobject xorg-x11-xinit xorg-x11-xkb-utils"
RPM_SONAME_REQ_ibus-devel = "libibus-1.0.so.5"
RPROVIDES_ibus-devel = "ibus-dev (= 1.5.19)"
RDEPENDS_ibus-devel = "dbus-devel glib2-devel gobject-introspection-devel ibus-libs pkgconf-pkg-config vala"
RPM_SONAME_REQ_ibus-gtk2 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdbus-1.so.3 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libibus-1.0.so.5 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus-gtk2 = "atk cairo dbus-libs fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 ibus-libs pango"
RPM_SONAME_REQ_ibus-gtk3 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libibus-1.0.so.5 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus-gtk3 = "atk cairo cairo-gobject dbus-libs gdk-pixbuf2 glib2 glibc gtk3 ibus-libs pango"
RPM_SONAME_PROV_ibus-libs = "libibus-1.0.so.5"
RPM_SONAME_REQ_ibus-libs = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_ibus-libs = "dbus glib2 glibc gobject-introspection"
RDEPENDS_ibus-setup = "bash ibus platform-python python3-gobject"
RPM_SONAME_REQ_ibus-wayland = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 libpthread.so.0 libwayland-client.so.0 libxkbcommon.so.0"
RDEPENDS_ibus-wayland = "glib2 glibc ibus-libs libwayland-client libxkbcommon"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-1.5.19-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-gtk2-1.5.19-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-gtk3-1.5.19-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-libs-1.5.19-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-setup-1.5.19-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-wayland-1.5.19-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ibus-devel-1.5.19-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ibus-devel-docs-1.5.19-11.el8.noarch.rpm \
          "

SRC_URI[ibus.sha256sum] = "d28a14672bf29baeec9e3280aa91fa516da178923530e221df05834730c3af46"
SRC_URI[ibus-devel.sha256sum] = "7cc7ffe8dd9be6abd252cbf45f94fd075febb1c86f58f90292e314c00ee3a462"
SRC_URI[ibus-devel-docs.sha256sum] = "162d8e3ff2e630b982eb9e1f8da5dabef099e46f05bf93c5cc98cbce129d1e1a"
SRC_URI[ibus-gtk2.sha256sum] = "e31bd627e0d8d364e2f9ae3c59a8284d9c61596f2f5eb51fc08cb26fdbb13f92"
SRC_URI[ibus-gtk3.sha256sum] = "f50f0d6b3afe164817210a54e4f5a69f1805be32f2a195738756358c1761f2a4"
SRC_URI[ibus-libs.sha256sum] = "eb301576929267fe54bb8f36aa1980c5ddc2f731ce6399ae1c08c5c5c2bb5fcb"
SRC_URI[ibus-setup.sha256sum] = "53f2f2be07182f9b09ba0c6967a3d2d217278ac62075cda20eada6e808f451ea"
SRC_URI[ibus-wayland.sha256sum] = "c3a75ca92009e9f264c4070d2e9598caf3ef5057e1667ba134b0803002b044cd"
