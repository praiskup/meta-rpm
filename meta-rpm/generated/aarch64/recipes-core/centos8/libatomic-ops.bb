SUMMARY = "generated recipe based on libatomic_ops srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libatomic_ops = "libatomic_ops.so.1 libatomic_ops_gpl.so.1"
RPM_SONAME_REQ_libatomic_ops = "ld-linux-aarch64.so.1 libatomic_ops.so.1 libc.so.6"
RDEPENDS_libatomic_ops = "glibc"
RPM_SONAME_REQ_libatomic_ops-devel = "libatomic_ops.so.1 libatomic_ops_gpl.so.1"
RPROVIDES_libatomic_ops-devel = "libatomic_ops-dev (= 7.6.2)"
RDEPENDS_libatomic_ops-devel = "libatomic_ops pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libatomic_ops-7.6.2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libatomic_ops-devel-7.6.2-3.el8.aarch64.rpm \
          "

SRC_URI[libatomic_ops.sha256sum] = "0aa33acddbb76f3271bf595d887f3a614d82cb73841c1c2074d79755eb7fb7ab"
SRC_URI[libatomic_ops-devel.sha256sum] = "a1a2671b445da614560c82b48b6739d0cf53425c84acc01bc1aece5a889d679f"
