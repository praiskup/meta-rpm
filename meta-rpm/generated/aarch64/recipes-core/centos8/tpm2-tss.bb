SUMMARY = "generated recipe based on tpm2-tss srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcrypt pkgconfig-native"
RPM_SONAME_PROV_tpm2-tss = "libtss2-esys.so.0 libtss2-mu.so.0 libtss2-sys.so.0 libtss2-tcti-device.so.0 libtss2-tcti-mssim.so.0"
RPM_SONAME_REQ_tpm2-tss = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libtss2-mu.so.0 libtss2-sys.so.0 libtss2-tcti-device.so.0 libtss2-tcti-mssim.so.0"
RDEPENDS_tpm2-tss = "glibc libgcrypt"
RPM_SONAME_REQ_tpm2-tss-devel = "libtss2-esys.so.0 libtss2-mu.so.0 libtss2-sys.so.0 libtss2-tcti-device.so.0 libtss2-tcti-mssim.so.0"
RPROVIDES_tpm2-tss-devel = "tpm2-tss-dev (= 2.0.0)"
RDEPENDS_tpm2-tss-devel = "pkgconf-pkg-config tpm2-tss"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm2-tss-2.0.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm2-tss-devel-2.0.0-4.el8.aarch64.rpm \
          "

SRC_URI[tpm2-tss.sha256sum] = "da687d3d174271f484f9c292dc812c853574cd1e405ab96b9bb607a9e703a6c9"
SRC_URI[tpm2-tss-devel.sha256sum] = "9efdb6bad2ddd70d2a148aa32b58f940c574548a28b1406e5395d5abb0860fe6"
