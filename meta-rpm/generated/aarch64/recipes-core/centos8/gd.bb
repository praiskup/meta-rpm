SUMMARY = "generated recipe based on gd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libjpeg-turbo libpng libwebp libx11 libxpm pkgconfig-native tiff zlib"
RPM_SONAME_PROV_gd = "libgd.so.3"
RPM_SONAME_REQ_gd = "ld-linux-aarch64.so.1 libX11.so.6 libXpm.so.4 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libjpeg.so.62 libm.so.6 libpng16.so.16 libtiff.so.5 libwebp.so.7 libz.so.1"
RDEPENDS_gd = "fontconfig freetype glibc libX11 libXpm libjpeg-turbo libpng libtiff libwebp zlib"
RPM_SONAME_REQ_gd-devel = "libgd.so.3"
RPROVIDES_gd-devel = "gd-dev (= 2.2.5)"
RDEPENDS_gd-devel = "bash fontconfig-devel freetype-devel gd libX11-devel libXpm-devel libjpeg-turbo-devel libpng-devel libtiff-devel libwebp-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gd-2.2.5-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gd-devel-2.2.5-6.el8.aarch64.rpm \
          "

SRC_URI[gd.sha256sum] = "736beb966a376b32e4c58e18c229af48d68b760d147489547c17817bbc81b5a7"
SRC_URI[gd-devel.sha256sum] = "d5bf4ff7c4c886d378d94ddd0d2d098b2c05d613fe46f4c0e3b93c42cef1798b"
