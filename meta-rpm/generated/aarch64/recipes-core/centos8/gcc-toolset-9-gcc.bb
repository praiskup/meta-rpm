SUMMARY = "generated recipe based on gcc-toolset-9-gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libgcc libmpc mpfr pkgconfig-native zlib"
RPM_SONAME_PROV_gcc-toolset-9-gcc = "liblto_plugin.so.0"
RPM_SONAME_REQ_gcc-toolset-9-gcc = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 liblto_plugin.so.0 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-toolset-9-gcc = "binutils gcc-toolset-9-runtime glibc glibc-devel gmp libgcc libgomp libmpc mpfr zlib"
RPM_SONAME_REQ_gcc-toolset-9-gcc-c++ = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-toolset-9-gcc-c++ = "gcc-toolset-9-gcc gcc-toolset-9-libstdc++-devel gcc-toolset-9-runtime glibc gmp libmpc libstdc++ mpfr zlib"
RPM_SONAME_PROV_gcc-toolset-9-gcc-gdb-plugin = "libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0"
RPM_SONAME_REQ_gcc-toolset-9-gcc-gdb-plugin = "ld-linux-aarch64.so.1 libc.so.6 libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-gcc-gdb-plugin = "gcc-toolset-9-gcc gcc-toolset-9-runtime glibc libgcc libstdc++"
RPM_SONAME_REQ_gcc-toolset-9-gcc-gfortran = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-toolset-9-gcc-gfortran = "bash gcc-toolset-9-gcc gcc-toolset-9-runtime glibc gmp libgfortran libmpc mpfr zlib"
RPM_SONAME_REQ_gcc-toolset-9-gcc-plugin-devel = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RPROVIDES_gcc-toolset-9-gcc-plugin-devel = "gcc-toolset-9-gcc-plugin-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-gcc-plugin-devel = "gcc-toolset-9-gcc gcc-toolset-9-runtime glibc gmp-devel libmpc-devel mpfr-devel"
RPROVIDES_gcc-toolset-9-libasan-devel = "gcc-toolset-9-libasan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libasan-devel = "gcc-toolset-9-runtime libasan"
RPROVIDES_gcc-toolset-9-libatomic-devel = "gcc-toolset-9-libatomic-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libatomic-devel = "gcc-toolset-9-runtime libatomic"
RPROVIDES_gcc-toolset-9-libitm-devel = "gcc-toolset-9-libitm-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libitm-devel = "gcc-toolset-9-gcc gcc-toolset-9-runtime libitm"
RPROVIDES_gcc-toolset-9-liblsan-devel = "gcc-toolset-9-liblsan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-liblsan-devel = "gcc-toolset-9-runtime liblsan"
RPROVIDES_gcc-toolset-9-libstdc++-devel = "gcc-toolset-9-libstdc++-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libstdc++-devel = "gcc-toolset-9-runtime libstdc++"
RDEPENDS_gcc-toolset-9-libstdc++-docs = "gcc-toolset-9-runtime"
RPROVIDES_gcc-toolset-9-libtsan-devel = "gcc-toolset-9-libtsan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libtsan-devel = "gcc-toolset-9-runtime libtsan"
RPROVIDES_gcc-toolset-9-libubsan-devel = "gcc-toolset-9-libubsan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libubsan-devel = "gcc-toolset-9-runtime libubsan"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gcc-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gcc-c++-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gcc-gdb-plugin-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-gcc-gfortran-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libasan-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libatomic-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libitm-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-liblsan-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libstdc++-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libstdc++-docs-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libtsan-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-libubsan-devel-9.2.1-2.2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gcc-toolset-9-gcc-plugin-devel-9.2.1-2.2.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-gcc.sha256sum] = "41014f3fc45c229229699f92bdec76399bd9fbc30ddd9972697823b97e63be08"
SRC_URI[gcc-toolset-9-gcc-c++.sha256sum] = "c7cf34f55c95aa58ee1a33c77fbe6d53bd69451b562fa0f956faa3526a26ed57"
SRC_URI[gcc-toolset-9-gcc-gdb-plugin.sha256sum] = "26a2c2cbfe0c983c4c50d2b4ffc0fad2a5545b26a9d1381895807c10e8162bd6"
SRC_URI[gcc-toolset-9-gcc-gfortran.sha256sum] = "b829d2c61d376ca0a3e373491dd04c15ac93b3c3c84221307b30f545c88d7f57"
SRC_URI[gcc-toolset-9-gcc-plugin-devel.sha256sum] = "fb4e4ee7d69679a22ef7889ff5a4ab08368317c37d0d0c1f7ec3c4349b99bbc1"
SRC_URI[gcc-toolset-9-libasan-devel.sha256sum] = "a0ea5f5c86b54ae8f90972a1e0482f9bb2f5f822cc39cb767e965b5477b4fac6"
SRC_URI[gcc-toolset-9-libatomic-devel.sha256sum] = "3c24fb279547260b804a5c61f19f92478e0db7d7bf8570bf8c1505a339240166"
SRC_URI[gcc-toolset-9-libitm-devel.sha256sum] = "9ba252d2d1b7e4a8b88795993b069c7e8f8c826eace4dcfcb92dbf4427b2fb47"
SRC_URI[gcc-toolset-9-liblsan-devel.sha256sum] = "7310612bbd91fce6cb476deb2c87f4e5bd9c37bc869a7ba0782d90397ee6479d"
SRC_URI[gcc-toolset-9-libstdc++-devel.sha256sum] = "6530e9b138dd2ee96428afb79417c3125db8959dc51889e36bb73284c2893e9b"
SRC_URI[gcc-toolset-9-libstdc++-docs.sha256sum] = "71bb1718e950ce0e5f341df398f8ead176779371db9fda7dfc22dcbdc4a4852a"
SRC_URI[gcc-toolset-9-libtsan-devel.sha256sum] = "c4f58e1bafbec3141db7b3a630aad9e664b08a9dd49046c75c82a3cb1f501a89"
SRC_URI[gcc-toolset-9-libubsan-devel.sha256sum] = "1cb3cda3ee73f74c3cfd0be70a6c5b3d3cd10cc6f34d4e5fe038c9b3c347de5f"
