SUMMARY = "generated recipe based on libverto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libverto = "libverto.so.1"
RPM_SONAME_REQ_libverto = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_libverto = "glibc"
RPM_SONAME_REQ_libverto-devel = "libverto.so.1"
RPROVIDES_libverto-devel = "libverto-dev (= 0.3.0)"
RDEPENDS_libverto-devel = "libverto pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libverto-0.3.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libverto-devel-0.3.0-5.el8.aarch64.rpm \
          "

SRC_URI[libverto.sha256sum] = "446f45706d78e80d4057d9d55dda32ce1cb823b2ca4dfe50f0ca5b515238130d"
SRC_URI[libverto-devel.sha256sum] = "c43e180d06240dc8d007a1502dfe7e4dc4ed3aa11d65067d3327d1fff86d14dc"
