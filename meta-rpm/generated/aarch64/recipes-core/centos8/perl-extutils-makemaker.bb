SUMMARY = "generated recipe based on perl-ExtUtils-MakeMaker srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-ExtUtils-Command = "perl-Carp perl-Exporter perl-File-Path perl-interpreter perl-libs"
RDEPENDS_perl-ExtUtils-MM-Utils = "perl-interpreter perl-libs"
RDEPENDS_perl-ExtUtils-MakeMaker = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-ExtUtils-Command perl-ExtUtils-Install perl-ExtUtils-Manifest perl-ExtUtils-ParseXS perl-File-Path perl-Getopt-Long perl-IO perl-PathTools perl-Test-Harness perl-devel perl-interpreter perl-libs perl-podlators perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-ExtUtils-Command-7.34-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-ExtUtils-MM-Utils-7.34-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-ExtUtils-MakeMaker-7.34-1.el8.noarch.rpm \
          "

SRC_URI[perl-ExtUtils-Command.sha256sum] = "a29ef96d4e4cb75d272e8ba52690ba9a7d735181891ed24cd19f443fb4a32970"
SRC_URI[perl-ExtUtils-MM-Utils.sha256sum] = "dbc72f176a80f98d22b68b35d0101ac49dd5f1d4bcb6e5888637236d233e493c"
SRC_URI[perl-ExtUtils-MakeMaker.sha256sum] = "24f5c889c2fd7c26287a9669614257a1f3a85bb5e4542198a82c44ff195ac861"
