SUMMARY = "generated recipe based on hunspell-el srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-el = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-el-0.9-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-el.sha256sum] = "e8a132f528690977d13b654c2cce23bac37b1e77f243b4d4e8fa389945d7af43"
