SUMMARY = "generated recipe based on perl-IPC-Run srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-Run = "perl-Carp perl-Data-Dumper perl-Errno perl-Exporter perl-IO perl-IO-Tty perl-PathTools perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-IPC-Run-0.99-1.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-Run.sha256sum] = "20865ad642dc00b874b06fc75def97389168c41f054874b2334506b56a45a997"
