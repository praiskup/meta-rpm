SUMMARY = "generated recipe based on perl-Ref-Util srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Ref-Util = "perl-Carp perl-Exporter perl-Ref-Util-XS perl-Scalar-List-Utils perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Ref-Util-0.203-4.el8.noarch.rpm \
          "

SRC_URI[perl-Ref-Util.sha256sum] = "d6ef65546ae0e220ed0c66c546bd1e548e0f74cb3357aa374128fb019207d499"
