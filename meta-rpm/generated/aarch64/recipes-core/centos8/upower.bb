SUMMARY = "generated recipe based on upower srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgudev libimobiledevice libplist libusb1 pkgconfig-native"
RPM_SONAME_PROV_upower = "libupower-glib.so.3"
RPM_SONAME_REQ_upower = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libimobiledevice.so.6 libplist.so.3 libupower-glib.so.3 libusb-1.0.so.0"
RDEPENDS_upower = "glib2 glibc gobject-introspection libgudev libimobiledevice libplist libusbx systemd-udev"
RPM_SONAME_REQ_upower-devel = "libupower-glib.so.3"
RPROVIDES_upower-devel = "upower-dev (= 0.99.7)"
RDEPENDS_upower-devel = "glib2-devel pkgconf-pkg-config upower"
RDEPENDS_upower-devel-docs = "upower"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/upower-0.99.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/upower-devel-0.99.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/upower-devel-docs-0.99.7-3.el8.noarch.rpm \
          "

SRC_URI[upower.sha256sum] = "6f88bbe06cb8316904646c288d62be0759872a7be267484164778bf2508fe631"
SRC_URI[upower-devel.sha256sum] = "36e0e92a109c8850b024a714f24b6879994b0ff4f1d2129de1db9e48396f4e08"
SRC_URI[upower-devel-docs.sha256sum] = "fa0448182dffc5756fcd95bd49b1e732fc01f94c655119f8e0144347f43dcc51"
