SUMMARY = "generated recipe based on arpwatch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcap pkgconfig-native"
RPM_SONAME_REQ_arpwatch = "ld-linux-aarch64.so.1 libc.so.6 libpcap.so.1"
RDEPENDS_arpwatch = "bash glibc libpcap postfix shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/arpwatch-2.1a15-44.el8.aarch64.rpm \
          "

SRC_URI[arpwatch.sha256sum] = "bd785678b84b31c7a791d8516f21bb647980d2ae03a76f23ede4f6adf66c7125"
