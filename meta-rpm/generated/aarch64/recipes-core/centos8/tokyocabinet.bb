SUMMARY = "generated recipe based on tokyocabinet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 pkgconfig-native zlib"
RPM_SONAME_PROV_tokyocabinet = "libtokyocabinet.so.9"
RPM_SONAME_REQ_tokyocabinet = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libm.so.6 libpthread.so.0 librt.so.1 libtokyocabinet.so.9 libz.so.1"
RDEPENDS_tokyocabinet = "bzip2-libs glibc zlib"
RPM_SONAME_REQ_tokyocabinet-devel = "libtokyocabinet.so.9"
RPROVIDES_tokyocabinet-devel = "tokyocabinet-dev (= 1.4.48)"
RDEPENDS_tokyocabinet-devel = "pkgconf-pkg-config tokyocabinet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tokyocabinet-1.4.48-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tokyocabinet-devel-1.4.48-10.el8.aarch64.rpm \
          "

SRC_URI[tokyocabinet.sha256sum] = "26ae328f7615abfa74faddb78d59e874a401281653f4c454688220a8cbe7468b"
SRC_URI[tokyocabinet-devel.sha256sum] = "ce8abb9c2c8495893a4db0b5e509034b39263a6056e55f7ce0eb390cd367cb32"
