SUMMARY = "generated recipe based on libpipeline srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpipeline = "libpipeline.so.1"
RPM_SONAME_REQ_libpipeline = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libpipeline = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpipeline-1.5.0-2.el8.aarch64.rpm \
          "

SRC_URI[libpipeline.sha256sum] = "f5cf9c5fbec4050a8314d47bf7005504c324ca653b6ed3576859d5ecf880b5be"
