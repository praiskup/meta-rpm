SUMMARY = "generated recipe based on supermin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs pkgconfig-native rpm"
RPM_SONAME_REQ_supermin = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libdl.so.2 libext2fs.so.2 libm.so.6 librpm.so.8 librpmio.so.8"
RDEPENDS_supermin = "cpio dnf dnf-plugins-core e2fsprogs e2fsprogs-libs findutils glibc libcom_err rpm rpm-libs tar util-linux"
RPROVIDES_supermin-devel = "supermin-dev (= 5.1.19)"
RDEPENDS_supermin-devel = "bash rpm-build supermin"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/supermin-5.1.19-9.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/supermin-devel-5.1.19-9.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[supermin.sha256sum] = "99c5cd969f6285f69f1de71e5e2a62788f258d82bd0882ecf4d7d61deea993bd"
SRC_URI[supermin-devel.sha256sum] = "ad31dabe094409ab69e587cf2f355ddb95c22fb60199661c4d91f616a684870d"
