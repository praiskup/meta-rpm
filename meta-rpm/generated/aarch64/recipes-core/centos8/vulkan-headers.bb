SUMMARY = "generated recipe based on vulkan-headers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vulkan-headers-1.2.135.0-1.el8.noarch.rpm \
          "

SRC_URI[vulkan-headers.sha256sum] = "260227c9e7c238f2d07965e2b47dcd2663b8114da06ff008b42298a03d5531de"
