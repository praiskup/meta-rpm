SUMMARY = "generated recipe based on libwacom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgudev pkgconfig-native"
RPM_SONAME_PROV_libwacom = "libwacom.so.2"
RPM_SONAME_REQ_libwacom = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libwacom.so.2"
RDEPENDS_libwacom = "glib2 glibc libgudev libwacom-data"
RPM_SONAME_REQ_libwacom-devel = "libwacom.so.2"
RPROVIDES_libwacom-devel = "libwacom-dev (= 1.1)"
RDEPENDS_libwacom-devel = "libwacom pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwacom-1.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwacom-data-1.1-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwacom-devel-1.1-2.el8.aarch64.rpm \
          "

SRC_URI[libwacom.sha256sum] = "395d71ec6a327223b4bef9754b25099dacccbe635ead515759b39b1bacdcf5e8"
SRC_URI[libwacom-data.sha256sum] = "caafa89e90e752529e1d9b43306cd783bc0fe0d05f0188185ac6e480db2a1caf"
SRC_URI[libwacom-devel.sha256sum] = "133df56e9d8223944f96e53a6f8baea3e201daf5b743c5ce7034e2ad80e8cc17"
