SUMMARY = "generated recipe based on environment-modules srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_environment-modules = "bash chkconfig less man-db platform-python procps-ng sed tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/environment-modules-4.1.4-4.el8.aarch64.rpm \
          "

SRC_URI[environment-modules.sha256sum] = "73a9c1f7813d7866efc55493d578d83516416c6c0d304154a72eb5ea370c4f5e"
