SUMMARY = "generated recipe based on systemtap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_systemtap-sdt-devel = "systemtap-sdt-dev (= 4.2)"
RDEPENDS_systemtap-sdt-devel = "platform-python python3-pyparsing"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-sdt-devel-4.2-6.el8.aarch64.rpm \
          "

SRC_URI[systemtap-sdt-devel.sha256sum] = "53d6a84b9b70bcd9b06bff7ed416651a81ddf8c7e4f4c9c863b0809fb2d9efec"
