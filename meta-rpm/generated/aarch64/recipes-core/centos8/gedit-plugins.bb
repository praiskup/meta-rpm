SUMMARY = "generated recipe based on gedit-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gobject-introspection gtk+3 gtksourceview3 libpeas pango pkgconfig-native"
RPM_SONAME_PROV_gedit-plugin-bookmarks = "libbookmarks.so"
RPM_SONAME_REQ_gedit-plugin-bookmarks = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-bookmarks = "atk cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RDEPENDS_gedit-plugin-bracketcompletion = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-codecomment = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-colorpicker = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-colorschemer = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-commander = "gedit-plugins-data libpeas-loader-python3"
RPM_SONAME_PROV_gedit-plugin-drawspaces = "libdrawspaces.so"
RPM_SONAME_REQ_gedit-plugin-drawspaces = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-drawspaces = "atk bash cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RPM_SONAME_PROV_gedit-plugin-findinfiles = "libfindinfiles.so"
RPM_SONAME_REQ_gedit-plugin-findinfiles = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-findinfiles = "atk cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RDEPENDS_gedit-plugin-joinlines = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-multiedit = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-smartspaces = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-terminal = "bash gedit-plugins-data libpeas-loader-python3 vte291"
RDEPENDS_gedit-plugin-textsize = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-translate = "gedit-plugins-data libpeas-loader-python3"
RPM_SONAME_PROV_gedit-plugin-wordcompletion = "libwordcompletion.so"
RPM_SONAME_REQ_gedit-plugin-wordcompletion = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-wordcompletion = "atk bash cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RDEPENDS_gedit-plugins = "gedit-plugin-bookmarks gedit-plugin-bracketcompletion gedit-plugin-codecomment gedit-plugin-colorpicker gedit-plugin-colorschemer gedit-plugin-commander gedit-plugin-drawspaces gedit-plugin-findinfiles gedit-plugin-joinlines gedit-plugin-multiedit gedit-plugin-smartspaces gedit-plugin-terminal gedit-plugin-textsize gedit-plugin-translate gedit-plugin-wordcompletion"
RDEPENDS_gedit-plugins-data = "gedit python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-bookmarks-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-bracketcompletion-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-codecomment-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-colorpicker-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-colorschemer-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-commander-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-drawspaces-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-findinfiles-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-joinlines-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-multiedit-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-smartspaces-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-terminal-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-textsize-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-translate-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugin-wordcompletion-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugins-3.28.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gedit-plugins-data-3.28.1-8.el8.aarch64.rpm \
          "

SRC_URI[gedit-plugin-bookmarks.sha256sum] = "f98c68679c5acb4a0bbfd696c93f2c52201c38e68856a33343d61eed7da79362"
SRC_URI[gedit-plugin-bracketcompletion.sha256sum] = "614b6db25de0a28649f184a556e78400a9096174c29ddb2f9a3a157f8a936b31"
SRC_URI[gedit-plugin-codecomment.sha256sum] = "43b65b6b7c750547da6d3991fa19a7dbfad9ca3f9bda0ad09af33965784c86bb"
SRC_URI[gedit-plugin-colorpicker.sha256sum] = "c3584a35cbacdc4cdc62ea6956120339c652062298cc23bdcb4cb3174b6631fd"
SRC_URI[gedit-plugin-colorschemer.sha256sum] = "f807c7f16859b9ef71b7de361abeec00dfb3f25cec18de72cc44c1bececc72dd"
SRC_URI[gedit-plugin-commander.sha256sum] = "69078eb22e5bb387ccc254967232745c616fc160450a0ede7a129315e01526cf"
SRC_URI[gedit-plugin-drawspaces.sha256sum] = "2122c7bf6e2233da9cb3a5ecace1a352c75f5ef4c96057d11fd4d97a7568fe28"
SRC_URI[gedit-plugin-findinfiles.sha256sum] = "27887b7296aa0084ccd55d459c85d0a63ae82afddce1283ed8be8ebf60bdfe88"
SRC_URI[gedit-plugin-joinlines.sha256sum] = "54f94f84535096cfe0eb465f00577109cacddf472583b308a64df2a9c418d88d"
SRC_URI[gedit-plugin-multiedit.sha256sum] = "41573a26ddb5db47e9f3292501c51a4886d8bd5a4bc173cf2dd685486ea73363"
SRC_URI[gedit-plugin-smartspaces.sha256sum] = "b546c0a91ea835a3f09498b0091c0b2adc67c90845843a8b99c4bf9614c5a861"
SRC_URI[gedit-plugin-terminal.sha256sum] = "a4f2d2fa7e16f4fb85cb3172f425a524035526202562f1e44540226ba883cc1a"
SRC_URI[gedit-plugin-textsize.sha256sum] = "b0f80ae1e529410a49e08b5d9f522dc7ed78876bf9895bb8798ad209ca28f245"
SRC_URI[gedit-plugin-translate.sha256sum] = "26044ce11039b5ed597556747af58f7b847dfac6f18aef73fed4eaa6ee6aeb07"
SRC_URI[gedit-plugin-wordcompletion.sha256sum] = "224419d40325670cd5226e9fa39caab08671e87e77246130a03c5258f2eea83d"
SRC_URI[gedit-plugins.sha256sum] = "e48ca06184eb12bddc4eb3952f66aa92a65c469d010e3b548414eae8bfc9d044"
SRC_URI[gedit-plugins-data.sha256sum] = "fa0fb7c18b631d3cb837e523d3837cf504be668c45b972ff8cba04ce21525aaa"
