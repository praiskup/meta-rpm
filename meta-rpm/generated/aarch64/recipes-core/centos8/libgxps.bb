SUMMARY = "generated recipe based on libgxps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo freetype glib-2.0 lcms2 libarchive libjpeg-turbo libpng pkgconfig-native tiff"
RPM_SONAME_PROV_libgxps = "libgxps.so.2"
RPM_SONAME_REQ_libgxps = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcairo.so.2 libfreetype.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpng16.so.16 libtiff.so.5"
RDEPENDS_libgxps = "cairo freetype glib2 glibc lcms2 libarchive libjpeg-turbo libpng libtiff"
RPM_SONAME_REQ_libgxps-devel = "libgxps.so.2"
RPROVIDES_libgxps-devel = "libgxps-dev (= 0.3.0)"
RDEPENDS_libgxps-devel = "cairo-devel glib2-devel libarchive-devel libgxps pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgxps-0.3.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgxps-devel-0.3.0-5.el8.aarch64.rpm \
          "

SRC_URI[libgxps.sha256sum] = "984734c9f54a9f7a358670553c1e8059cf87aa2c0766928d8040cacc03df40bf"
SRC_URI[libgxps-devel.sha256sum] = "841ebfbba0dd71aba18b34700a997779841827c23930ac24759dfbc8a0aff053"
