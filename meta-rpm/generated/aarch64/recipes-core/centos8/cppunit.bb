SUMMARY = "generated recipe based on cppunit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_cppunit = "libcppunit-1.14.so.0"
RPM_SONAME_REQ_cppunit = "ld-linux-aarch64.so.1 libc.so.6 libcppunit-1.14.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_cppunit = "glibc libgcc libstdc++"
RPM_SONAME_REQ_cppunit-devel = "libcppunit-1.14.so.0"
RPROVIDES_cppunit-devel = "cppunit-dev (= 1.14.0)"
RDEPENDS_cppunit-devel = "cppunit pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cppunit-1.14.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cppunit-devel-1.14.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cppunit-doc-1.14.0-4.el8.aarch64.rpm \
          "

SRC_URI[cppunit.sha256sum] = "3d289013cff586c0c2e723db2a77bb6812fafa27ef850b7e03e2ce99cf44c550"
SRC_URI[cppunit-devel.sha256sum] = "6d4832efa1dd7c5f6ba5e7088993ac0b013ee0e8988410d5f46756b2a2250e47"
SRC_URI[cppunit-doc.sha256sum] = "7594cccd59e61bdba56632d63d3578512d1e0071c6e0c97f3e0e712ce7145b20"
