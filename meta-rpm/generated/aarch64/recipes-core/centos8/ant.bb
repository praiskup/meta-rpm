SUMMARY = "generated recipe based on ant srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ant = "ant-lib java-1.8.0-openjdk-devel javapackages-tools"
RDEPENDS_ant-antlr = "ant ant-lib antlr-tool java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-apache-bcel = "ant ant-lib bcel java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-apache-bsf = "ant ant-lib bsf java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-apache-log4j = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem log4j12"
RDEPENDS_ant-apache-oro = "ant ant-lib jakarta-oro java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-apache-regexp = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem regexp"
RDEPENDS_ant-apache-resolver = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem xml-commons-resolver"
RDEPENDS_ant-apache-xalan2 = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem xalan-j2"
RDEPENDS_ant-commons-logging = "ant ant-lib apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-commons-net = "ant ant-lib apache-commons-net java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-javadoc = "javapackages-filesystem"
RDEPENDS_ant-javamail = "ant ant-lib java-1.8.0-openjdk-headless javamail javapackages-filesystem"
RDEPENDS_ant-jdepend = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem jdepend"
RDEPENDS_ant-jmf = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-jsch = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem jsch"
RDEPENDS_ant-junit = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem junit"
RDEPENDS_ant-lib = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-swing = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ant-testutil = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem junit"
RDEPENDS_ant-xz = "ant ant-lib java-1.8.0-openjdk-headless javapackages-filesystem xz-java"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-antlr-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-bcel-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-bsf-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-log4j-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-oro-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-regexp-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-resolver-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-apache-xalan2-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-commons-logging-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-commons-net-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-javadoc-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-javamail-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-jdepend-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-jmf-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-jsch-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-junit-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-lib-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-manual-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-swing-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-testutil-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-xz-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[ant.sha256sum] = "eeb35d64e140a00018f1f086c61043b25d05a1944d250783ce9e6a2bd6b874d6"
SRC_URI[ant-antlr.sha256sum] = "bcc7042f9ee2dcc319de27a4dd526b44ef1fd78c35315f4eb6b0187d11fad544"
SRC_URI[ant-apache-bcel.sha256sum] = "cf10d6cbf651597e6510385e3a78ad64185e53652a009ffb47f4fbf13f5f7aaf"
SRC_URI[ant-apache-bsf.sha256sum] = "0f165d95ad2f19c496e3aa857c1c595b910c95115a61626a1749ae9ffdc51fcd"
SRC_URI[ant-apache-log4j.sha256sum] = "dd361fad111b5f9c9db87f0b29a10ee5d851bd03b70ab41f1f5cff085c43323f"
SRC_URI[ant-apache-oro.sha256sum] = "499d51ab9126215fc943e53efd61f9b4b8fe8efe5d2558582c6723479157ac3b"
SRC_URI[ant-apache-regexp.sha256sum] = "f9814d6756b38d75243d248b08f27f2101e36ed83e7ed359615e2674bb845551"
SRC_URI[ant-apache-resolver.sha256sum] = "5dc3d5bac657710d702062553e048f76d9baf704359897da1fc090df5950beec"
SRC_URI[ant-apache-xalan2.sha256sum] = "ce89815948d3d4cfb407c1968e11a5b0ef421f9b8875b6e49e970909ec9a78ea"
SRC_URI[ant-commons-logging.sha256sum] = "53574d1af0a173091a9ad40b46f341ed34605e6be166b255e104ec4eb44d5ffe"
SRC_URI[ant-commons-net.sha256sum] = "11d9c3ee49de27148d425c3316b7821b0d08b3e9689517cd743b28372935665b"
SRC_URI[ant-javadoc.sha256sum] = "b165f75806e72615f11afd74e2578f928767e144e0f63ef7afd6eeeb880afa44"
SRC_URI[ant-javamail.sha256sum] = "af305c554ba8006f8460c4cb69e817e75d7f05769a64a1beab1bf4f4b8e333e4"
SRC_URI[ant-jdepend.sha256sum] = "b18042aafdf57e7339cac4ad2725a5348a3988089dadce75f6fc12b3cca3405a"
SRC_URI[ant-jmf.sha256sum] = "319b9913ff5b834ae665bf40a1ff4b1724b320520b66029bbed6915f47bf6421"
SRC_URI[ant-jsch.sha256sum] = "73b7eeb86ac7831eea95fec7e12ce6f85a52ee48a33fc4da58353f8de0d01556"
SRC_URI[ant-junit.sha256sum] = "e6191f29efb2f2a435f8114c0240a5810ab71751cb9f186c09d4b79667149616"
SRC_URI[ant-lib.sha256sum] = "ceeb1d11162a18c833dbfbd21a79708634d5d95d8e88545f879500ca9585e094"
SRC_URI[ant-manual.sha256sum] = "dfacb0397f4617bb213f9af15af780dbfab04a92eef8cb9de115059dd25e50e9"
SRC_URI[ant-swing.sha256sum] = "a427a44411882bbc83cb4bb37fe413a9cb2b396b8a989a207d706e91fd0cab10"
SRC_URI[ant-testutil.sha256sum] = "bfcc802ca966375aabafc49698fa57a5fb958c8ac2d86606b7e1ba553e2c72a2"
SRC_URI[ant-xz.sha256sum] = "bbce9a7c408afa58b6492dc25ab00db677ef4657943b477a713a25165ae9c0f9"
