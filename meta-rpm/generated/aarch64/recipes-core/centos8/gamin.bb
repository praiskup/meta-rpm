SUMMARY = "generated recipe based on gamin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_gamin = "libfam.so.0 libgamin-1.so.0"
RPM_SONAME_REQ_gamin = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0"
RDEPENDS_gamin = "glib2 glibc"
RPM_SONAME_REQ_gamin-devel = "libfam.so.0 libgamin-1.so.0"
RPROVIDES_gamin-devel = "gamin-dev (= 0.1.10)"
RDEPENDS_gamin-devel = "gamin pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gamin-0.1.10-31.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gamin-devel-0.1.10-31.el8.aarch64.rpm \
          "

SRC_URI[gamin.sha256sum] = "f8f392397ad4748e0b51671efc65c56b74e1db7b85c006a1ee038ae8d7e936fb"
SRC_URI[gamin-devel.sha256sum] = "5f3dccedb68ce641e7c78f37e8ce98580ef319c8230a850080ccedcd91b1add2"
