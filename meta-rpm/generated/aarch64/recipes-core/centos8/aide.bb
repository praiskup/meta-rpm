SUMMARY = "generated recipe based on aide srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr audit curl e2fsprogs libgcrypt libgpg-error libpcre libselinux pkgconfig-native zlib"
RPM_SONAME_REQ_aide = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libaudit.so.1 libc.so.6 libcurl.so.4 libe2p.so.2 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libpcre.so.1 libselinux.so.1 libz.so.1"
RDEPENDS_aide = "audit-libs e2fsprogs-libs glibc libacl libattr libcurl libgcrypt libgpg-error libselinux pcre zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/aide-0.16-11.el8.aarch64.rpm \
          "

SRC_URI[aide.sha256sum] = "a7ca52d91fbde85fe7603bf5b22f2f840e691db3f33c33089c395183ad482646"
