SUMMARY = "generated recipe based on python-ethtool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-ethtool = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-ethtool = "glibc libnl3 platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-ethtool-0.14-3.el8.aarch64.rpm \
          "

SRC_URI[python3-ethtool.sha256sum] = "caa73f06b13aaef91c4115e0af1689821cdf5130f9f75b416e2f0d2b0c3a6cf8"
