SUMMARY = "generated recipe based on ladspa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_ladspa = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_ladspa = "glibc libgcc libstdc++"
RPROVIDES_ladspa-devel = "ladspa-dev (= 1.13)"
RDEPENDS_ladspa-devel = "ladspa"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ladspa-1.13-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ladspa-devel-1.13-20.el8.aarch64.rpm \
          "

SRC_URI[ladspa.sha256sum] = "59f53842692673a0aec634c72e764a943a35f881a32d3712ab7f96189f0cba13"
SRC_URI[ladspa-devel.sha256sum] = "1387d994a24259aa26b74be67911b2ec2de32ba86887c10bd8ea6e89c608b0a9"
