SUMMARY = "generated recipe based on gnu-free-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gnu-free-fonts-common = "fontpackages-filesystem"
RDEPENDS_gnu-free-mono-fonts = "gnu-free-fonts-common"
RDEPENDS_gnu-free-sans-fonts = "gnu-free-fonts-common"
RDEPENDS_gnu-free-serif-fonts = "gnu-free-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnu-free-fonts-common-20120503-18.el8.0.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnu-free-mono-fonts-20120503-18.el8.0.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnu-free-sans-fonts-20120503-18.el8.0.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnu-free-serif-fonts-20120503-18.el8.0.1.noarch.rpm \
          "

SRC_URI[gnu-free-fonts-common.sha256sum] = "7a84cdd3e6f8ffa630ab65d5575aef0abe0f10939803b34d854107e8aa499954"
SRC_URI[gnu-free-mono-fonts.sha256sum] = "f34450ee98301944bcc830d471053c5f80f5e62a625d20f108835184464cdf0f"
SRC_URI[gnu-free-sans-fonts.sha256sum] = "328d29b2cdeb13f9bb3d3afbaaf9716a5f8db8083f99e425b712ed5f10d14938"
SRC_URI[gnu-free-serif-fonts.sha256sum] = "4c8a65ef3371821489e015ac306f4ccb8ec40cb82186ed5bdc10037dd20e63c0"
