SUMMARY = "generated recipe based on python-netifaces srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-netifaces = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-netifaces = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-netifaces-0.10.6-4.el8.aarch64.rpm \
          "

SRC_URI[python3-netifaces.sha256sum] = "c805709ad155fa3a798350106f664aa3f239879560269146484bcf36c5d6f0d5"
