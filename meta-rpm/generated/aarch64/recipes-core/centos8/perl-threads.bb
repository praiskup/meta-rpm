SUMMARY = "generated recipe based on perl-threads srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-threads = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-threads = "glibc perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-threads-2.21-2.el8.aarch64.rpm \
          "

SRC_URI[perl-threads.sha256sum] = "e515b0ff943643163957e6a6f09eb7eb6abf0deb64260e57fbde9233969033bb"
