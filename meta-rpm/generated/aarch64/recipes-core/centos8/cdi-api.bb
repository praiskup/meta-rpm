SUMMARY = "generated recipe based on cdi-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_cdi-api = "atinject glassfish-el-api java-1.8.0-openjdk-headless javapackages-filesystem jboss-interceptors-1.2-api"
RDEPENDS_cdi-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cdi-api-1.2-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cdi-api-javadoc-1.2-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[cdi-api.sha256sum] = "e82f4269983c8f365bc9f7324f7d4abdf86c5d810e006bdc266d3eb71312eaf9"
SRC_URI[cdi-api-javadoc.sha256sum] = "f686d51642e0c49a976b28e615bc19fda7e215896256331f3a8723515fbc1c06"
