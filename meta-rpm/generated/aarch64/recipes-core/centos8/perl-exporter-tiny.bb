SUMMARY = "generated recipe based on perl-Exporter-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Exporter-Tiny = "perl-Carp perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Exporter-Tiny-1.000000-4.el8.noarch.rpm \
          "

SRC_URI[perl-Exporter-Tiny.sha256sum] = "89c82f1fa45474a675ebc4baa042539b80dd758f2722a6a063b3a1d3e7bea501"
