SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libasan = "libasan.so.5"
RPM_SONAME_REQ_libasan = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_libasan = "glibc info libgcc libstdc++"
RPM_SONAME_PROV_liblsan = "liblsan.so.0"
RPM_SONAME_REQ_liblsan = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_liblsan = "glibc info libgcc libstdc++"
RPM_SONAME_PROV_libtsan = "libtsan.so.0"
RPM_SONAME_REQ_libtsan = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_libtsan = "glibc info libgcc libstdc++"
RPM_SONAME_PROV_libubsan = "libubsan.so.1"
RPM_SONAME_REQ_libubsan = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_libubsan = "glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liblsan-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libasan-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtsan-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libubsan-8.3.1-5.el8.0.2.aarch64.rpm \
          "

SRC_URI[libasan.sha256sum] = "c37e6ae809aa60ae51fe322b7348861f0029a7c82591fd8f9dbfd651de495dca"
SRC_URI[liblsan.sha256sum] = "e5e5fa607d4b1eeaff48fff60797e7e5c2c6e8cdab4371452a4531d13a8a520a"
SRC_URI[libtsan.sha256sum] = "a550d894ce5e3e3ce536c790008a92e68e41886bbf04047c00d7476021c06677"
SRC_URI[libubsan.sha256sum] = "fa3fd7e12ca895840b496adff195405d21249011f1d61df6bfbc18a7203d33f4"
