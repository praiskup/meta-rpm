SUMMARY = "generated recipe based on mksh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mksh = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mksh = "bash chkconfig glibc grep"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mksh-56c-5.el8.aarch64.rpm \
          "

SRC_URI[mksh.sha256sum] = "68b091fbed40e5484345d88e2e353d8b0ed476f0c955d4308b2cec5bc871726b"
