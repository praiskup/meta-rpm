SUMMARY = "generated recipe based on python-blivet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_blivet-data = "platform-python"
RDEPENDS_python3-blivet = "blivet-data lsof parted platform-python python3-blockdev python3-bytesize python3-gobject-base python3-libselinux python3-pyparted python3-pyudev python3-six systemd-udev util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/blivet-data-3.1.0-21.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-blivet-3.1.0-21.el8_2.noarch.rpm \
          "

SRC_URI[blivet-data.sha256sum] = "2d6b226a859af003eeff7e3089ceb6a8784d975518c5ebdf2df927ddf579a13c"
SRC_URI[python3-blivet.sha256sum] = "ce6b964d944efc783b0b4ee636c53a0cda9f6156b87cf005c8967022f77f97d8"
