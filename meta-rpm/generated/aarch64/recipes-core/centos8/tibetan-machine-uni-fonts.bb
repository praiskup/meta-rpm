SUMMARY = "generated recipe based on tibetan-machine-uni-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_tibetan-machine-uni-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tibetan-machine-uni-fonts-1.901-20.el8.noarch.rpm \
          "

SRC_URI[tibetan-machine-uni-fonts.sha256sum] = "d79358beeb5b14b9104b956313f8d402782f31e540c7db4a212d5dd44f294d5c"
