SUMMARY = "generated recipe based on numpy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openblas pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-numpy = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libopenblas.so.0 libopenblasp.so.0 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-numpy = "glibc openblas openblas-threads platform-python python3-libs"
RDEPENDS_python3-numpy-f2py = "bash platform-python platform-python-devel python3-numpy python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-numpy-1.14.3-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-numpy-f2py-1.14.3-9.el8.aarch64.rpm \
          "

SRC_URI[python3-numpy.sha256sum] = "b8f4f735b6a10e932c99bc038163600d33eff538acbab176c21eb0cb015f6f66"
SRC_URI[python3-numpy-f2py.sha256sum] = "2dd9d5378d4c0dac33e0a415b14be23cd0381eaeb234506e72bd43238e67fcb1"
