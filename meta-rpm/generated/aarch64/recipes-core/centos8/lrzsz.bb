SUMMARY = "generated recipe based on lrzsz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lrzsz = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lrzsz = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lrzsz-0.12.20-43.el8.aarch64.rpm \
          "

SRC_URI[lrzsz.sha256sum] = "f3dcc495fbd4aaf8398d2974b86479dcfa5e52f58b5216a46cdd4cb3e28fa103"
