SUMMARY = "generated recipe based on sblim-indication_helper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sblim-indication_helper = "libind_helper.so.0"
RPM_SONAME_REQ_sblim-indication_helper = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_sblim-indication_helper = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sblim-indication_helper-0.5.0-2.el8.aarch64.rpm \
          "

SRC_URI[sblim-indication_helper.sha256sum] = "bcd4e4be3f4120ed83fea06605070542f82daf65b274e12ba4f9fc59aa569a0b"
