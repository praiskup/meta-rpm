SUMMARY = "generated recipe based on hunspell-mai srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mai = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-mai-1.0.1-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-mai.sha256sum] = "b1a91834b9f8a73b7ad984d0ab98b8ff1150c607e1d59bb866d0832cf317d57d"
