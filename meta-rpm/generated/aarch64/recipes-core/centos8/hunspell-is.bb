SUMMARY = "generated recipe based on hunspell-is srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-is = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-is-0.20090823-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-is.sha256sum] = "d74d1b3e20e73767ae7b7591680398e16c449a60a58dbbce931a2beb90971ebf"
