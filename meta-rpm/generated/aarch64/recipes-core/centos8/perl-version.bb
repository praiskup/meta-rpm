SUMMARY = "generated recipe based on perl-version srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-version = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-version = "glibc perl-Carp perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-version-0.99.24-1.el8.aarch64.rpm \
          "

SRC_URI[perl-version.sha256sum] = "1d0f1e9ed34ce31e0c9bd25cd87ab6bff3d0f451617dff87ae35fe874028f353"
