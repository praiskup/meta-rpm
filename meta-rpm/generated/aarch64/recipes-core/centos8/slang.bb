SUMMARY = "generated recipe based on slang srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_slang = "libslang.so.2"
RPM_SONAME_REQ_slang = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_slang = "glibc"
RPM_SONAME_REQ_slang-devel = "libslang.so.2"
RPROVIDES_slang-devel = "slang-dev (= 2.3.2)"
RDEPENDS_slang-devel = "pkgconf-pkg-config slang"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/slang-devel-2.3.2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/slang-2.3.2-3.el8.aarch64.rpm \
          "

SRC_URI[slang.sha256sum] = "121ddf8c1e31a9f6958659daa77254ca991da1d25609bc17eb7c261aa32d6176"
SRC_URI[slang-devel.sha256sum] = "c61dd9b3b970d14208adc3f038d0dbfe01e1904e5ce5b9234b59bbc6bd84661f"
