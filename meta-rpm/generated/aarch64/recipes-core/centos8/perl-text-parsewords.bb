SUMMARY = "generated recipe based on perl-Text-ParseWords srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-ParseWords = "perl-Carp perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Text-ParseWords-3.30-395.el8.noarch.rpm \
          "

SRC_URI[perl-Text-ParseWords.sha256sum] = "2975de6545b4ca7907ae368a1716c531764e4afccbf27fb0a694d90e983c38e2"
