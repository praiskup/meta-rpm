SUMMARY = "generated recipe based on xorg-x11-drv-v4l srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-v4l = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_xorg-x11-drv-v4l = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-v4l-0.3.0-2.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-v4l.sha256sum] = "805073c03e33661e952b305305afe2879de8687175189df18d0563c95f14b108"
