SUMMARY = "generated recipe based on urlview srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_urlview = "ld-linux-aarch64.so.1 libc.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_urlview = "bash glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urlview-0.9-23.20131022git08767a.el8.aarch64.rpm \
          "

SRC_URI[urlview.sha256sum] = "18540d5a0fd975b11d3f43cfd46b0813a99962e9a6885c1a366e754b49aa8c1a"
