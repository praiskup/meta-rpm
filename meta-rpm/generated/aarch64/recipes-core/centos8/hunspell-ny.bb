SUMMARY = "generated recipe based on hunspell-ny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ny = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ny-0.01-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-ny.sha256sum] = "d6fef0e7ce06d5d5cf388979e4cfa07eb9d4e9eb3cfd05009dedc39d77f2c19b"
