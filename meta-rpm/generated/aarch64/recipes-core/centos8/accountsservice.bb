SUMMARY = "generated recipe based on accountsservice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libxcrypt pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_accountsservice = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0"
RDEPENDS_accountsservice = "bash glib2 glibc libgcc polkit polkit-libs shadow-utils systemd"
RPM_SONAME_REQ_accountsservice-devel = "libaccountsservice.so.0"
RPROVIDES_accountsservice-devel = "accountsservice-dev (= 0.6.50)"
RDEPENDS_accountsservice-devel = "accountsservice-libs pkgconf-pkg-config"
RPM_SONAME_PROV_accountsservice-libs = "libaccountsservice.so.0"
RPM_SONAME_REQ_accountsservice-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_accountsservice-libs = "accountsservice glib2 glibc libgcc libxcrypt systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/accountsservice-0.6.50-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/accountsservice-libs-0.6.50-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/accountsservice-devel-0.6.50-8.el8.aarch64.rpm \
          "

SRC_URI[accountsservice.sha256sum] = "55de2c9c3cf71d5a751631e2b6f82b50d8172480e5f18a40aff782067bd9e19a"
SRC_URI[accountsservice-devel.sha256sum] = "ffd0dcbe329709666030291a26c2bbbbc0e1c8b1fa00ab990c0ded4b189c51c7"
SRC_URI[accountsservice-libs.sha256sum] = "9dd79cb14557925b3b6e1f5bfb9a5b11fd026ecd4584530749ee694b3201f0df"
