SUMMARY = "generated recipe based on hostname srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_hostname = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_hostname = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/hostname-3.20-6.el8.aarch64.rpm \
          "

SRC_URI[hostname.sha256sum] = "656b3b23230b9c17461b5cde860d46bb84074a406f71840697fca8dffcdf280c"
