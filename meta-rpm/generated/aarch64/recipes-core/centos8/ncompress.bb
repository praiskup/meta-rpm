SUMMARY = "generated recipe based on ncompress srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ncompress = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_ncompress = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ncompress-4.2.4.4-12.el8.aarch64.rpm \
          "

SRC_URI[ncompress.sha256sum] = "7ffb6772b38472e46bbdcfd0bcad833a14cf4d453241a83d2c8fe48b79f73cdc"
