SUMMARY = "generated recipe based on libao srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native pulseaudio"
RPM_SONAME_PROV_libao = "libalsa.so libao.so.4 liboss.so libpulse.so"
RPM_SONAME_REQ_libao = "ld-linux-aarch64.so.1 libao.so.4 libasound.so.2 libc.so.6 libdl.so.2 libpthread.so.0 libpulse-simple.so.0 libpulse.so.0"
RDEPENDS_libao = "alsa-lib glibc pulseaudio-libs"
RPM_SONAME_REQ_libao-devel = "libao.so.4"
RPROVIDES_libao-devel = "libao-dev (= 1.2.0)"
RDEPENDS_libao-devel = "libao pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libao-1.2.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libao-devel-1.2.0-10.el8.aarch64.rpm \
          "

SRC_URI[libao.sha256sum] = "c734cb600427d6a109915c8e69269774271a06f67096ef0eed054d71c93e8a7a"
SRC_URI[libao-devel.sha256sum] = "2a12a356b495647d13a3e56ebdc4f0cc7e4234f8139f5dbb9e1af8c3882f80e0"
