SUMMARY = "generated recipe based on maven-plugins-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-plugins-pom = "java-1.8.0-openjdk-headless javapackages-filesystem maven-parent maven-plugin-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugins-pom-28-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-plugins-pom.sha256sum] = "01bd1c9f1e2d7a649e5e85203278b99c94e7df86a37f368d48bb7ac50eda8eec"
