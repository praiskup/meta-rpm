SUMMARY = "generated recipe based on subscription-manager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-subscription-manager-rhsm = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_python3-subscription-manager-rhsm = "glibc openssl-libs platform-python python3-dateutil python3-iniparse python3-libs python3-rpm python3-six subscription-manager-rhsm-certificates"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-subscription-manager-rhsm-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/subscription-manager-rhsm-certificates-1.26.20-1.el8_2.aarch64.rpm \
          "

SRC_URI[python3-subscription-manager-rhsm.sha256sum] = "2fdc8083e03994b6cbb92503b0836d46d27f446199b4092112b49119dc88e3e7"
SRC_URI[subscription-manager-rhsm-certificates.sha256sum] = "75cbbcd7b8aca2133f955a66a13fe7e42a56a03927678c742575585fabb0bdea"
