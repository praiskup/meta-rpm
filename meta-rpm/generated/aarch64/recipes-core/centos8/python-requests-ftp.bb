SUMMARY = "generated recipe based on python-requests-ftp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-requests-ftp = "platform-python python3-requests"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-requests-ftp-0.3.1-11.el8.noarch.rpm \
          "

SRC_URI[python3-requests-ftp.sha256sum] = "e6a45883cdae3075330cc32871a018784ad85db3065e895107113c8e464fb2d3"
