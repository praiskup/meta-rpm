SUMMARY = "generated recipe based on perl-Config-AutoConf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Config-AutoConf = "perl-Capture-Tiny perl-Carp perl-Exporter perl-File-Temp perl-PathTools perl-Text-ParseWords perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Config-AutoConf-0.315-2.el8.noarch.rpm \
          "

SRC_URI[perl-Config-AutoConf.sha256sum] = "4317a7d70b556c03f57eea07b4fd350d6e2f863866db41eceebe8107ac7c5c3b"
