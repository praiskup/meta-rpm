SUMMARY = "generated recipe based on pam srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit cracklib db libnsl2 libselinux libtirpc libxcrypt pkgconfig-native"
RPM_SONAME_PROV_pam = "libpam.so.0 libpam_misc.so.0 libpamc.so.0"
RPM_SONAME_REQ_pam = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcrack.so.2 libcrypt.so.1 libdb-5.3.so libdl.so.2 libnsl.so.2 libpam.so.0 libselinux.so.1 libtirpc.so.3 libutil.so.1"
RDEPENDS_pam = "audit-libs bash coreutils cracklib glibc libdb libnsl2 libpwquality libselinux libtirpc libxcrypt"
RPM_SONAME_REQ_pam-devel = "libpam.so.0 libpam_misc.so.0 libpamc.so.0"
RPROVIDES_pam-devel = "pam-dev (= 1.3.1)"
RDEPENDS_pam-devel = "pam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pam-1.3.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pam-devel-1.3.1-8.el8.aarch64.rpm \
          "

SRC_URI[pam.sha256sum] = "3e28658058d0d26e10b788642a7cd131a5cc59b2b7ca56465033054e6ad91d84"
SRC_URI[pam-devel.sha256sum] = "71bd3bf95eaa838b5373ac6205c5dec9076389449c82595caf866d8cf1e6033d"
