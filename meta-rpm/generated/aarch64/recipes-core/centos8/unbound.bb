SUMMARY = "generated recipe based on unbound srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libevent openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-unbound = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libevent-2.1.so.6 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1 libunbound.so.2"
RDEPENDS_python3-unbound = "glibc libevent openssl-libs platform-python python3-libs unbound-libs"
RPM_SONAME_REQ_unbound = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libevent-2.1.so.6 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1 libunbound.so.2"
RDEPENDS_unbound = "bash glibc libevent openssl-libs python3-libs systemd unbound-libs"
RPM_SONAME_REQ_unbound-devel = "libunbound.so.2"
RPROVIDES_unbound-devel = "unbound-dev (= 1.7.3)"
RDEPENDS_unbound-devel = "libevent-devel openssl-devel pkgconf-pkg-config platform-python-devel unbound-libs"
RPM_SONAME_PROV_unbound-libs = "libunbound.so.2"
RPM_SONAME_REQ_unbound-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libevent-2.1.so.6 libexpat.so.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1 libunbound.so.2"
RDEPENDS_unbound-libs = "bash expat glibc libevent openssl-libs python3-libs shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-unbound-1.7.3-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/unbound-1.7.3-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/unbound-devel-1.7.3-11.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/unbound-libs-1.7.3-11.el8_2.aarch64.rpm \
          "

SRC_URI[python3-unbound.sha256sum] = "adb5296856b5e1365a4e4d7dbb9e4ac79d16232c4b0149b048b69e784616d545"
SRC_URI[unbound.sha256sum] = "791cdd8e75561b5d3455f624303a35719a26a377763f40083d7dc6dad1b6bd1b"
SRC_URI[unbound-devel.sha256sum] = "302957b9761adcb8ff087c6f2560fa884c574ad934724c158670f6590fced205"
SRC_URI[unbound-libs.sha256sum] = "7762686c940dba1257f03f66812054ca97cbeb2dc92289b101ff9f7fbac276da"
