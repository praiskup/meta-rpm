SUMMARY = "generated recipe based on jolokia-jvm-agent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jolokia-jvm-agent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jolokia-jvm-agent-1.6.2-3.el8.noarch.rpm \
          "

SRC_URI[jolokia-jvm-agent.sha256sum] = "f1d7007e40ff14864e48b4acd7f9526117f6e658479c2034e8b0c3104da71ec2"
