SUMMARY = "generated recipe based on libtevent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtalloc libxcrypt pkgconfig-native platform-python3"
RPM_SONAME_PROV_libtevent = "libtevent.so.0"
RPM_SONAME_REQ_libtevent = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0 libtalloc.so.2"
RDEPENDS_libtevent = "glibc libtalloc libxcrypt"
RPM_SONAME_REQ_libtevent-devel = "libtevent.so.0"
RPROVIDES_libtevent-devel = "libtevent-dev (= 0.10.0)"
RDEPENDS_libtevent-devel = "libtalloc-devel libtevent pkgconf-pkg-config"
RPM_SONAME_REQ_python3-tevent = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libtalloc.so.2 libtevent.so.0 libutil.so.1"
RDEPENDS_python3-tevent = "glibc libtalloc libtevent libxcrypt platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtevent-0.10.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtevent-devel-0.10.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-tevent-0.10.0-2.el8.aarch64.rpm \
          "

SRC_URI[libtevent.sha256sum] = "abb0e7063ef6b2b85b0ebcaae460a57260681ee34764739f88365b2a93cde256"
SRC_URI[libtevent-devel.sha256sum] = "fa4df30af34fa34a47be5d11c67e76b70bed3cbefe023364707f95405a326c9f"
SRC_URI[python3-tevent.sha256sum] = "25fd79f3a7fc307e4f9c7244dca0fa95e9799273a63e114d6c34f272d7eb3f60"
