SUMMARY = "generated recipe based on egl-wayland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native wayland"
RPM_SONAME_PROV_egl-wayland = "libnvidia-egl-wayland.so.1"
RPM_SONAME_REQ_egl-wayland = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 libwayland-client.so.0 libwayland-server.so.0"
RDEPENDS_egl-wayland = "glibc libglvnd-egl libwayland-client libwayland-server"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/egl-wayland-1.1.4-1.el8.aarch64.rpm \
          "

SRC_URI[egl-wayland.sha256sum] = "73ab3cc7ded90099693dc1bfe6dcc1035ed4836436d9e028a2c177117b509046"
