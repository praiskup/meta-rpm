SUMMARY = "generated recipe based on perl-Module-Load-Conditional srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Load-Conditional = "perl-Carp perl-Exporter perl-Locale-Maketext-Simple perl-Module-CoreList perl-Module-Load perl-Module-Metadata perl-Params-Check perl-PathTools perl-constant perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Module-Load-Conditional-0.68-395.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Load-Conditional.sha256sum] = "26a31dcd676b940ed99d575b6ae8c4a869ef17f32b610641441316c50831eeec"
