SUMMARY = "generated recipe based on libcanberra srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gstreamer1.0 gtk+3 gtk2 libtdb libtool libvorbis libx11 pango pkgconfig-native pulseaudio systemd-libs"
RPM_SONAME_PROV_libcanberra = "libcanberra-alsa.so libcanberra-gstreamer.so libcanberra-multi.so libcanberra-null.so libcanberra-pulse.so libcanberra.so.0"
RPM_SONAME_REQ_libcanberra = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libcanberra.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstreamer-1.0.so.0 libltdl.so.7 libm.so.6 libpthread.so.0 libpulse.so.0 libtdb.so.1 libudev.so.1 libvorbisfile.so.3"
RDEPENDS_libcanberra = "alsa-lib bash glib2 glibc gstreamer1 libtdb libtool-ltdl libvorbis pulseaudio-libs sound-theme-freedesktop systemd systemd-libs"
RPM_SONAME_REQ_libcanberra-devel = "libcanberra-gtk.so.0 libcanberra-gtk3.so.0 libcanberra.so.0"
RPROVIDES_libcanberra-devel = "libcanberra-dev (= 0.30)"
RDEPENDS_libcanberra-devel = "gtk2-devel gtk3-devel libcanberra libcanberra-gtk2 libcanberra-gtk3 pkgconf-pkg-config"
RPM_SONAME_PROV_libcanberra-gtk2 = "libcanberra-gtk-module.so libcanberra-gtk.so.0"
RPM_SONAME_REQ_libcanberra-gtk2 = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcanberra-gtk.so.0 libcanberra.so.0 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libltdl.so.7 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libtdb.so.1 libvorbisfile.so.3"
RDEPENDS_libcanberra-gtk2 = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libX11 libcanberra libcanberra-gtk3 libtdb libtool-ltdl libvorbis pango"
RPM_SONAME_PROV_libcanberra-gtk3 = "libcanberra-gtk3-module.so libcanberra-gtk3.so.0"
RPM_SONAME_REQ_libcanberra-gtk3 = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libltdl.so.7 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libtdb.so.1 libvorbisfile.so.3"
RDEPENDS_libcanberra-gtk3 = "atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libcanberra libtdb libtool-ltdl libvorbis pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcanberra-0.30-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcanberra-devel-0.30-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcanberra-gtk2-0.30-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcanberra-gtk3-0.30-16.el8.aarch64.rpm \
          "

SRC_URI[libcanberra.sha256sum] = "03bc66e4b1c91e4717734d0c3b35cbdfd5514b0da18d5a253e8cc8c0fbbb5385"
SRC_URI[libcanberra-devel.sha256sum] = "3dfeaebad94a8b3ebb7d6876d99a95164d15e33eb56b71f92de03fbb2d4064fe"
SRC_URI[libcanberra-gtk2.sha256sum] = "700ed564d8758c18e9a6c74c7edd4b873e5615fbb2b422f8249e7f9ef537e104"
SRC_URI[libcanberra-gtk3.sha256sum] = "a5f768e67cb08b626412225d9a2842faae87dd266c7af8c67578f2ed2407aea3"
