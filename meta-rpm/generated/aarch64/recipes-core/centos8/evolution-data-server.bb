SUMMARY = "generated recipe based on evolution-data-server srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo db e2fsprogs gcr gdk-pixbuf glib-2.0 gnome-online-accounts gtk+3 icu json-glib krb5-libs libgcc libgdata libgweather libical libsecret libsoup-2.4 libxml2 nspr nss openldap p11-kit pango pkgconfig-native sqlite3 webkit2gtk3 zlib"
RPM_SONAME_PROV_evolution-data-server = "libcamel-1.2.so.61 libcamelimapx.so libcamellocal.so libcamelnntp.so libcamelpop3.so libcamelsendmail.so libcamelsmtp.so libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libebookbackendfile.so libebookbackendgoogle.so libebookbackendldap.so libebookbackendwebdav.so libecal-1.2.so.19 libecalbackendcaldav.so libecalbackendcontacts.so libecalbackendfile.so libecalbackendgtasks.so libecalbackendhttp.so libecalbackendweather.so libedata-book-1.2.so.25 libedata-cal-1.2.so.28 libedataserver-1.2.so.23 libedataserverui-1.2.so.2 libedbus-private.so"
RPM_SONAME_REQ_evolution-data-server = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcamel-1.2.so.61 libcom_err.so.2 libdb-5.3.so libdl.so.2 libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libecal-1.2.so.19 libedata-book-1.2.so.25 libedata-cal-1.2.so.28 libedataserver-1.2.so.23 libedbus-private.so libgcc_s.so.1 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgdata.so.22 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgtk-3.so.0 libgweather-3.so.15 libical.so.3 libicalss.so.3 libicalvcal.so.3 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libjavascriptcoregtk-4.0.so.18 libjson-glib-1.0.so.0 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libplc4.so libplds4.so libpthread.so.0 libresolv.so.2 libsecret-1.so.0 libsmime3.so libsoup-2.4.so.1 libsqlite3.so.0 libssl3.so libstdc++.so.6 libwebkit2gtk-4.0.so.37 libxml2.so.2 libz.so.1"
RDEPENDS_evolution-data-server = "atk bash cairo cairo-gobject dconf evolution-data-server-langpacks gcr gdk-pixbuf2 glib2 glibc gnome-online-accounts gtk3 json-glib krb5-libs libcom_err libdb libgcc libgdata libgweather libical libicu libsecret libsoup libstdc++ libxml2 nspr nss nss-util openldap p11-kit pango sqlite-libs webkit2gtk3 webkit2gtk3-jsc zlib"
RPM_SONAME_REQ_evolution-data-server-devel = "libcamel-1.2.so.61 libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libecal-1.2.so.19 libedata-book-1.2.so.25 libedata-cal-1.2.so.28 libedataserver-1.2.so.23 libedataserverui-1.2.so.2"
RPROVIDES_evolution-data-server-devel = "evolution-data-server-dev (= 3.28.5)"
RDEPENDS_evolution-data-server-devel = "evolution-data-server glib2-devel gnome-online-accounts-devel gtk3-devel json-glib-devel libgdata-devel libgweather-devel libical-devel libsecret-devel libsoup-devel libxml2-devel nspr-devel nss-devel pkgconf-pkg-config sqlite-devel webkit2gtk3-devel"
RDEPENDS_evolution-data-server-langpacks = "evolution-data-server"
RDEPENDS_evolution-data-server-perl = "evolution-data-server perl-Text-ParseWords perl-interpreter perl-libs"
RPM_SONAME_PROV_evolution-data-server-tests = "libetestserverutils.so.0"
RPM_SONAME_REQ_evolution-data-server-tests = "ld-linux-aarch64.so.1 libc.so.6 libcamel-1.2.so.61 libcom_err.so.2 libdb-5.3.so libdl.so.2 libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libecal-1.2.so.19 libedata-book-1.2.so.25 libedata-cal-1.2.so.28 libedataserver-1.2.so.23 libedbus-private.so libetestserverutils.so.0 libgcc_s.so.1 libgck-1.so.0 libgcr-base-3.so.1 libgdata.so.22 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libical.so.3 libicalss.so.3 libicalvcal.so.3 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libjson-glib-1.0.so.0 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libp11-kit.so.0 libplc4.so libplds4.so libpthread.so.0 libsecret-1.so.0 libsmime3.so libsoup-2.4.so.1 libsqlite3.so.0 libssl3.so libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_evolution-data-server-tests = "evolution-data-server gcr glib2 glibc json-glib krb5-libs libcom_err libdb libgcc libgdata libical libicu libsecret libsoup libstdc++ libxml2 nspr nss nss-util p11-kit sqlite-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-data-server-3.28.5-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-data-server-devel-3.28.5-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-data-server-langpacks-3.28.5-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/evolution-data-server-doc-3.28.5-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/evolution-data-server-perl-3.28.5-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/evolution-data-server-tests-3.28.5-13.el8.aarch64.rpm \
          "

SRC_URI[evolution-data-server.sha256sum] = "7cb73224aff2eb2c99f406115f6feb02e1d3937cf90c6228addfc213660b4ff4"
SRC_URI[evolution-data-server-devel.sha256sum] = "a3839790e9ada5abd43f2708813f22aa121282f678c3b314971855c9a5f2009b"
SRC_URI[evolution-data-server-doc.sha256sum] = "282c3d603b02fa8bd29c20d7134ac8c6e0675cdbfe693b48a1feb7b037cd3dfe"
SRC_URI[evolution-data-server-langpacks.sha256sum] = "1ef332cceec4973a4850294c3c6af34b69e9cbb7ae65bca7e9afaa87b8ae79c0"
SRC_URI[evolution-data-server-perl.sha256sum] = "26c3758d5992ce3b1d6b104f2f22abf29864b5878445970a5cc93215a4c97054"
SRC_URI[evolution-data-server-tests.sha256sum] = "a11b7fe817d89020229686b00ca2eb56ab571a1f05b943c6534daab1fb0fcc96"
