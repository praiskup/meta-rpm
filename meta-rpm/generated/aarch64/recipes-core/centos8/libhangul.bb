SUMMARY = "generated recipe based on libhangul srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libhangul = "libhangul.so.1"
RPM_SONAME_REQ_libhangul = "ld-linux-aarch64.so.1 libc.so.6 libhangul.so.1"
RDEPENDS_libhangul = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libhangul-0.1.0-16.el8.aarch64.rpm \
          "

SRC_URI[libhangul.sha256sum] = "e6018c80de22b4498397668e141fd8bea360377883b88dc51709014b0c55b859"
