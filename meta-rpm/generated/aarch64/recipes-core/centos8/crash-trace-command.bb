SUMMARY = "generated recipe based on crash-trace-command srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-trace-command = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_crash-trace-command = "crash glibc trace-cmd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/crash-trace-command-2.0-15.el8.aarch64.rpm \
          "

SRC_URI[crash-trace-command.sha256sum] = "e02f9d7a0e976d6c15af2b1305715c21c6712b62980f33064173f714c807c785"
