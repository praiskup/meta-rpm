SUMMARY = "generated recipe based on tigervnc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fltk fontconfig gnutls libgcc libglvnd libice libjpeg-turbo libsm libx11 libxau libxdamage libxdmcp libxext libxfixes libxfont2 libxft libxinerama libxrender libxshmfence libxtst nettle pam pixman pkgconfig-native zlib"
RPM_SONAME_REQ_tigervnc = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXfixes.so.3 libXft.so.2 libXinerama.so.1 libXrender.so.1 libc.so.6 libfltk.so.1.3 libfltk_images.so.1.3 libfontconfig.so.1 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libpam.so.0 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_tigervnc = "coreutils fltk fontconfig glibc gnutls hicolor-icon-theme libICE libSM libX11 libXext libXfixes libXft libXinerama libXrender libgcc libjpeg-turbo libstdc++ pam tigervnc-icons tigervnc-license zlib"
RPM_SONAME_REQ_tigervnc-server = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXtst.so.6 libc.so.6 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libpam.so.0 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_tigervnc-server = "bash glibc gnutls libICE libSM libX11 libXdamage libXext libXfixes libXtst libgcc libjpeg-turbo libstdc++ pam perl-interpreter systemd tigervnc-server-minimal xorg-x11-xauth xorg-x11-xinit zlib"
RDEPENDS_tigervnc-server-applet = "java-1.8.0-openjdk javapackages-tools tigervnc-server"
RPM_SONAME_REQ_tigervnc-server-minimal = "ld-linux-aarch64.so.1 libGL.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXau.so.6 libXdmcp.so.6 libXext.so.6 libXfont2.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libnettle.so.6 libpam.so.0 libpixman-1.so.0 libpthread.so.0 libstdc++.so.6 libxshmfence.so.1 libz.so.1"
RDEPENDS_tigervnc-server-minimal = "chkconfig glibc gnutls libICE libSM libX11 libXau libXdmcp libXext libXfont2 libgcc libglvnd-glx libjpeg-turbo libstdc++ libxshmfence mesa-dri-drivers nettle pam pixman tigervnc-license xkeyboard-config xorg-x11-xkb-utils zlib"
RPM_SONAME_PROV_tigervnc-server-module = "libvnc.so"
RPM_SONAME_REQ_tigervnc-server-module = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libpam.so.0 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_tigervnc-server-module = "glibc gnutls libgcc libjpeg-turbo libstdc++ pam tigervnc-license xorg-x11-server-Xorg zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-1.9.0-15.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-icons-1.9.0-15.el8_1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-license-1.9.0-15.el8_1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-server-1.9.0-15.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-server-applet-1.9.0-15.el8_1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-server-minimal-1.9.0-15.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tigervnc-server-module-1.9.0-15.el8_1.aarch64.rpm \
          "

SRC_URI[tigervnc.sha256sum] = "b525d5f79477637476fdf38546801c0ecfb420ff911541906b3b36215bd2ecd9"
SRC_URI[tigervnc-icons.sha256sum] = "bf57475d9a64c8965bf3388f0a6c3c5b79b2766763b76242194c9e2d7b858c98"
SRC_URI[tigervnc-license.sha256sum] = "bad19a2339bb43af93f1c7c37cb592041c7049d853b4da795f8ebcb4da53a5c2"
SRC_URI[tigervnc-server.sha256sum] = "bece82c265d5a1fec6f6aa1b6b06f2ae7157edec0a74ae7891bdc12ed38c60d1"
SRC_URI[tigervnc-server-applet.sha256sum] = "b17d4c4b5c1742f082b744aa709787ad3ca26715c0195dc6491951ec0909a3d0"
SRC_URI[tigervnc-server-minimal.sha256sum] = "84b61772ff8be3ff5b6eeb6f2506e59a26ec5c3f1c1f77932697a8caa69a6e29"
SRC_URI[tigervnc-server-module.sha256sum] = "763bd37551bb4920049cca144c6ab6dd26188de187075207af6e90dec2959589"
