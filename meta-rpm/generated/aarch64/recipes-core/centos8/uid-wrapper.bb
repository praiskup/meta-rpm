SUMMARY = "generated recipe based on uid_wrapper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_uid_wrapper = "libuid_wrapper.so.0"
RPM_SONAME_REQ_uid_wrapper = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 libuid_wrapper.so.0"
RDEPENDS_uid_wrapper = "cmake-filesystem glibc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/uid_wrapper-1.2.4-4.el8.aarch64.rpm \
          "

SRC_URI[uid_wrapper.sha256sum] = "e52c4b4fa7562cfeaf36fd713f539ae7f212aa69d39a814bd9c01780dfdd0a33"
