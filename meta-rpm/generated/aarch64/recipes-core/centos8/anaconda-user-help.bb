SUMMARY = "generated recipe based on anaconda-user-help srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-user-help-8.2.3-1.el8.noarch.rpm \
          "

SRC_URI[anaconda-user-help.sha256sum] = "759494579a7e5dfe707afb484ae31da778c1560958956fb0776af9f2a2b8f961"
