SUMMARY = "generated recipe based on iotop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_iotop = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iotop-0.6-16.el8.noarch.rpm \
          "

SRC_URI[iotop.sha256sum] = "b8c4dae9af478df8c501f099bdd98d0140df59c45d3f734b8031fa7451366158"
