SUMMARY = "generated recipe based on libXv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXv = "libXv.so.1"
RPM_SONAME_REQ_libXv = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXv = "glibc libX11 libXext"
RPM_SONAME_REQ_libXv-devel = "libXv.so.1"
RPROVIDES_libXv-devel = "libXv-dev (= 1.0.11)"
RDEPENDS_libXv-devel = "libX11-devel libXext-devel libXv pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXv-1.0.11-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXv-devel-1.0.11-7.el8.aarch64.rpm \
          "

SRC_URI[libXv.sha256sum] = "4fa819d1cf002b2dc339beb7649709d5f977ff8ae8ca4e34350055663e7a54b8"
SRC_URI[libXv-devel.sha256sum] = "8b8d414ceeb2ca127eea795526d292ed630a26e02969d95f40fe088636fdf8bd"
