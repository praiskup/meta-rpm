SUMMARY = "generated recipe based on media-player-info srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_media-player-info = "systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/media-player-info-23-2.el8.noarch.rpm \
          "

SRC_URI[media-player-info.sha256sum] = "8ae106b691b0dfcbb9c0217fb4209c90960bf372b4f23f9790b6be52e3e2f29f"
