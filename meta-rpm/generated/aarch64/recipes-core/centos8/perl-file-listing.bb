SUMMARY = "generated recipe based on perl-File-Listing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Listing = "perl-Carp perl-Exporter perl-HTTP-Date perl-Time-Local perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-File-Listing-6.04-17.el8.noarch.rpm \
          "

SRC_URI[perl-File-Listing.sha256sum] = "0f502eabf9df61c657c39c7e3ea037c2b24c03b6637a4dd63acd9b9bc8b0390e"
