SUMMARY = "generated recipe based on dos2unix srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dos2unix = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_dos2unix = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dos2unix-7.4.0-3.el8.aarch64.rpm \
          "

SRC_URI[dos2unix.sha256sum] = "4cd3826b467bbbac18ee1acaffdedceb4dfb481cec723588f387d7ae26787f3e"
