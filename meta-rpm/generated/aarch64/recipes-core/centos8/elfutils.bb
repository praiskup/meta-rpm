SUMMARY = "generated recipe based on elfutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 curl gcc-runtime pkgconfig-native xz zlib"
RPM_SONAME_REQ_elfutils = "ld-linux-aarch64.so.1 libasm.so.1 libc.so.6 libdw.so.1 libelf.so.1 libstdc++.so.6"
RDEPENDS_elfutils = "bash elfutils-libelf elfutils-libs glibc libstdc++"
RPM_SONAME_PROV_elfutils-debuginfod-client = "libdebuginfod.so.1"
RPM_SONAME_REQ_elfutils-debuginfod-client = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdebuginfod.so.1"
RDEPENDS_elfutils-debuginfod-client = "glibc libcurl"
RPM_SONAME_REQ_elfutils-debuginfod-client-devel = "libdebuginfod.so.1"
RPROVIDES_elfutils-debuginfod-client-devel = "elfutils-debuginfod-client-dev (= 0.178)"
RDEPENDS_elfutils-debuginfod-client-devel = "elfutils-debuginfod-client pkgconf-pkg-config"
RDEPENDS_elfutils-default-yama-scope = "bash"
RPM_SONAME_REQ_elfutils-devel = "libasm.so.1 libdw.so.1"
RPROVIDES_elfutils-devel = "elfutils-dev (= 0.178)"
RDEPENDS_elfutils-devel = "elfutils-libelf-devel elfutils-libs pkgconf-pkg-config xz-devel zlib-devel"
RDEPENDS_elfutils-devel-static = "elfutils-devel elfutils-libelf-devel-static"
RPM_SONAME_PROV_elfutils-libelf = "libelf.so.1"
RPM_SONAME_REQ_elfutils-libelf = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1"
RDEPENDS_elfutils-libelf = "glibc zlib"
RPM_SONAME_REQ_elfutils-libelf-devel = "libelf.so.1"
RPROVIDES_elfutils-libelf-devel = "elfutils-libelf-dev (= 0.178)"
RDEPENDS_elfutils-libelf-devel = "elfutils-libelf pkgconf-pkg-config zlib-devel"
RDEPENDS_elfutils-libelf-devel-static = "elfutils-libelf-devel"
RPM_SONAME_PROV_elfutils-libs = "libasm.so.1 libdw.so.1"
RPM_SONAME_REQ_elfutils-libs = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 liblzma.so.5 libpthread.so.0 libz.so.1"
RDEPENDS_elfutils-libs = "bzip2-libs elfutils-default-yama-scope elfutils-libelf glibc xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/elfutils-debuginfod-client-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/elfutils-debuginfod-client-devel-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/elfutils-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/elfutils-default-yama-scope-0.178-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/elfutils-devel-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/elfutils-libelf-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/elfutils-libelf-devel-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/elfutils-libs-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/elfutils-devel-static-0.178-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/elfutils-libelf-devel-static-0.178-7.el8.aarch64.rpm \
          "

SRC_URI[elfutils.sha256sum] = "0a784af5352d523b40427455791939e125abf2cc9ceeb7f6d63f9c96d747bd83"
SRC_URI[elfutils-debuginfod-client.sha256sum] = "20c3aa4dea7a422a3b104658cce64bc985bec92094f7afd394fa7fb8e4b9b396"
SRC_URI[elfutils-debuginfod-client-devel.sha256sum] = "229dc525310be77d4f2dccaebe804bfe692cb115f52c79235b568341ff4fc26a"
SRC_URI[elfutils-default-yama-scope.sha256sum] = "25157a7103a91e2c40ff8b4bf43dfd4cf2faf37c98698dc2ff032c240babe4e2"
SRC_URI[elfutils-devel.sha256sum] = "e7204d661c55a1a46487effa2d26be7548ce2249af315eb83439649b4cf2cb46"
SRC_URI[elfutils-devel-static.sha256sum] = "6ec181f4039bebca57ec0d9a9f3e477bdb116f690b460c7a20554d4309a7cc0b"
SRC_URI[elfutils-libelf.sha256sum] = "4c5372ed4b1d944c5d572afbbee6cef8c3c7084635ddef4dc5880c16c7dba659"
SRC_URI[elfutils-libelf-devel.sha256sum] = "c148a5c0a791c694292c2075fc046f435a5ff57ab1bfe070962fdd4f3e6e3fdb"
SRC_URI[elfutils-libelf-devel-static.sha256sum] = "4dffa85d8324c2b706cf5d1106f643500769e87753eb001107ff09262fa62caf"
SRC_URI[elfutils-libs.sha256sum] = "b3b68bbbbec6eaecad845f6bffa58784b201ef94b723737d67103ff60e6185b8"
