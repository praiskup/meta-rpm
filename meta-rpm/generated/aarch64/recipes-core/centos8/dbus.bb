SUMMARY = "generated recipe based on dbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-libs expat libcap-ng libselinux libx11 pkgconfig-native systemd-libs"
RDEPENDS_dbus = "dbus-daemon systemd"
RPM_SONAME_REQ_dbus-daemon = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcap-ng.so.0 libdbus-1.so.3 libexpat.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0"
RDEPENDS_dbus-daemon = "audit-libs bash dbus-common dbus-libs dbus-tools expat glibc libcap-ng libselinux shadow-utils systemd-libs"
RPM_SONAME_REQ_dbus-devel = "libdbus-1.so.3"
RPROVIDES_dbus-devel = "dbus-dev (= 1.12.8)"
RDEPENDS_dbus-devel = "cmake-filesystem dbus-daemon dbus-libs pkgconf-pkg-config xml-common"
RPM_SONAME_REQ_dbus-tools = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libpthread.so.0 libsystemd.so.0"
RDEPENDS_dbus-tools = "dbus-libs glibc systemd-libs"
RPM_SONAME_REQ_dbus-x11 = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libdbus-1.so.3 libpthread.so.0 libsystemd.so.0"
RDEPENDS_dbus-x11 = "bash dbus-daemon dbus-libs glibc libX11 systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dbus-devel-1.12.8-10.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dbus-x11-1.12.8-10.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbus-1.12.8-10.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbus-common-1.12.8-10.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbus-daemon-1.12.8-10.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbus-tools-1.12.8-10.el8_2.aarch64.rpm \
          "

SRC_URI[dbus.sha256sum] = "7e70d6e8bc90dccd4c3388d506ad696820490c2051ab3e58802ea6cc38ef714a"
SRC_URI[dbus-common.sha256sum] = "488c91557b55bcd1ef629d1a7ed7a6eaba939a05a319e07166f310d79fe39184"
SRC_URI[dbus-daemon.sha256sum] = "fb61b8661ba66e560cdb8c618c01263750ee47fde8313a699854852232dcc4be"
SRC_URI[dbus-devel.sha256sum] = "e8506cf0f2e5d8027028d46b243589d6cd479eaa5f25f7f7e1190c0c9639f44c"
SRC_URI[dbus-tools.sha256sum] = "e5d76cb86a030c10df349bd11b892190af441004424d02af028966d10e3d1fce"
SRC_URI[dbus-x11.sha256sum] = "e7c049634ae80965646fb64633dce557fee7ffeaf2b81b658b5c7d3419cf25dc"
