SUMMARY = "generated recipe based on hunspell-csb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-csb = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-csb-0.20050311-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-csb.sha256sum] = "dc07c43440855bf9e53f99c2559224402e57bb15cdba5ee9378443ddf032f8c9"
