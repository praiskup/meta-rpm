SUMMARY = "generated recipe based on at-spi2-atk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "at-spi2-core atk dbus-libs glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_at-spi2-atk = "libatk-bridge-2.0.so.0 libatk-bridge.so"
RPM_SONAME_REQ_at-spi2-atk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libatk-bridge-2.0.so.0 libatspi.so.0 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_at-spi2-atk = "at-spi2-core atk dbus-libs glib2 glibc"
RPM_SONAME_REQ_at-spi2-atk-devel = "libatk-bridge-2.0.so.0"
RPROVIDES_at-spi2-atk-devel = "at-spi2-atk-dev (= 2.26.2)"
RDEPENDS_at-spi2-atk-devel = "at-spi2-atk at-spi2-core-devel glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/at-spi2-atk-2.26.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/at-spi2-atk-devel-2.26.2-1.el8.aarch64.rpm \
          "

SRC_URI[at-spi2-atk.sha256sum] = "132cba5915b7a4192824cf81f68c8f7ab33c0ac0d8fd4b1c1752f57f5bda8e6d"
SRC_URI[at-spi2-atk-devel.sha256sum] = "538da6d71a2214f4250a57226f5aa8009b438e5ac4f9e5b1f138df7521644e7c"
