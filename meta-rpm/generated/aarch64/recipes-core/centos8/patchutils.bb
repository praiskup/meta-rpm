SUMMARY = "generated recipe based on patchutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_patchutils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_patchutils = "bash glibc perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/patchutils-0.3.4-10.el8.aarch64.rpm \
          "

SRC_URI[patchutils.sha256sum] = "1ba43e0dbb6a71504366d01d1211f7666acf6914c4fca57123564c247c243e1e"
