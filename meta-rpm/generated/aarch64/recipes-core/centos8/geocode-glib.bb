SUMMARY = "generated recipe based on geocode-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib libgcc libsoup-2.4 pkgconfig-native"
RPM_SONAME_PROV_geocode-glib = "libgeocode-glib.so.0"
RPM_SONAME_REQ_geocode-glib = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libsoup-2.4.so.1"
RDEPENDS_geocode-glib = "glib2 glibc json-glib libgcc libsoup"
RPM_SONAME_REQ_geocode-glib-devel = "libgeocode-glib.so.0"
RPROVIDES_geocode-glib-devel = "geocode-glib-dev (= 3.26.0)"
RDEPENDS_geocode-glib-devel = "geocode-glib glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/geocode-glib-3.26.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/geocode-glib-devel-3.26.0-1.el8.aarch64.rpm \
          "

SRC_URI[geocode-glib.sha256sum] = "ec08fd8aa87f58997c2118fbb80775b4d1c31672d9f45ba2c0bc408c4abce775"
SRC_URI[geocode-glib-devel.sha256sum] = "f78611d2f7898cd40741fac4d60617bd4cbbb81e1ec47b9157c3bbacf5a870da"
