SUMMARY = "generated recipe based on haproxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre libxcrypt lua openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_haproxy = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 liblua-5.3.so libm.so.6 libpcre.so.1 libpcreposix.so.0 libpthread.so.0 libssl.so.1.1 libsystemd.so.0 libz.so.1"
RDEPENDS_haproxy = "bash glibc libxcrypt lua-libs openssl-libs pcre shadow-utils systemd systemd-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/haproxy-1.8.23-3.el8.aarch64.rpm \
          "

SRC_URI[haproxy.sha256sum] = "50806be5ee5706b32b89a3df140ce0a8b29feecdf17d3e6ca0c8119c24afc02e"
