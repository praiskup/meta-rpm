SUMMARY = "generated recipe based on perl-Digest-SHA1 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-SHA1 = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-SHA1 = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Digest-SHA1-2.13-23.el8.aarch64.rpm \
          "

SRC_URI[perl-Digest-SHA1.sha256sum] = "48da52c714b9e490446b4e5990c36b1f1936af90486f40bbccbd82135d945811"
