SUMMARY = "generated recipe based on hunspell-smj srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-smj = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-smj-1.0-0.14.beta7.el8.noarch.rpm \
          "

SRC_URI[hunspell-smj.sha256sum] = "97ea759ea0ceba19c9cee6bc7cc8f7252888b7fbfd2395d1ce7f735012ca89c2"
