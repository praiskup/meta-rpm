SUMMARY = "generated recipe based on kdump-anaconda-addon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_kdump-anaconda-addon = "anaconda hicolor-icon-theme"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/kdump-anaconda-addon-003-2.20181107git443d7ed.el8.noarch.rpm \
          "

SRC_URI[kdump-anaconda-addon.sha256sum] = "6b1610bc0acde6c17c91d5ebe4d3dfba2e19ff554ed6aae0b6e8b5e40070a500"
