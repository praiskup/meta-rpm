SUMMARY = "generated recipe based on libreport srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "augeas curl dbus-libs glib-2.0 json-c libnewt libproxy libtar libxml2 pkgconfig-native platform-python3 satyr systemd-libs xmlrpc-c"
RPM_SONAME_PROV_libreport = "libabrt_dbus.so.0 libreport.so.0"
RPM_SONAME_REQ_libreport = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport = "augeas-libs dbus-libs glib2 glibc libreport-filesystem libtar lz4 satyr systemd-libs xz"
RDEPENDS_libreport-anaconda = "libreport libreport-plugin-reportuploader libreport-plugin-rhtsupport"
RPM_SONAME_REQ_libreport-cli = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-cli = "augeas-libs glib2 glibc libreport libtar satyr systemd-libs"
RPM_SONAME_REQ_libreport-newt = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libnewt.so.0.52 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-newt = "augeas-libs glib2 glibc libreport libtar newt satyr systemd-libs"
RPM_SONAME_REQ_libreport-plugin-bugzilla = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-bugzilla = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-kerneloops = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-kerneloops = "augeas-libs curl glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-logger = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-plugin-logger = "augeas-libs glib2 glibc libreport libtar satyr systemd-libs"
RPM_SONAME_REQ_libreport-plugin-mailx = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-plugin-mailx = "augeas-libs glib2 glibc libreport libtar mailx satyr systemd-libs"
RPM_SONAME_REQ_libreport-plugin-reportuploader = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-reportuploader = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-rhtsupport = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-rhtsupport = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-ureport = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-ureport = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 python3-subscription-manager-rhsm satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RDEPENDS_libreport-rhel = "libreport"
RDEPENDS_libreport-rhel-anaconda-bugzilla = "libreport libreport-plugin-bugzilla"
RDEPENDS_libreport-rhel-bugzilla = "libreport libreport-plugin-bugzilla libreport-plugin-ureport"
RPM_SONAME_PROV_libreport-web = "libreport-web.so.0"
RPM_SONAME_REQ_libreport-web = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-web = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_python3-libreport = "ld-linux-aarch64.so.1 libaugeas.so.0 libc.so.6 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_python3-libreport = "augeas-libs glib2 glibc libreport libtar platform-python python3-dnf python3-libs satyr systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-anaconda-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-cli-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-newt-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-bugzilla-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-kerneloops-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-logger-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-mailx-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-reportuploader-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-rhtsupport-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-plugin-ureport-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-rhel-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-rhel-anaconda-bugzilla-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-rhel-bugzilla-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreport-web-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-libreport-2.9.5-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libreport-filesystem-2.9.5-10.el8.aarch64.rpm \
          "

SRC_URI[libreport.sha256sum] = "637cbb5773322d51a416aab8ffb397f31444ccd72d2d9ec23baeedec98ca214a"
SRC_URI[libreport-anaconda.sha256sum] = "683fb0475b81110e4f17c65888ae65021749d5023a33989b2f6df8bb7015a909"
SRC_URI[libreport-cli.sha256sum] = "929ad24b8f296acd906efd0ffb6265e77963d4246337745aa6da8ecdfb30e199"
SRC_URI[libreport-filesystem.sha256sum] = "7f970aefb46a2c43057f46aaf66d1e4db637ea00d142bb7a9c03ad09ae4004b3"
SRC_URI[libreport-newt.sha256sum] = "dec6f58bf2c8d6d20e800519762f79c3d5f5503b90d4b3bb359e97a50bbd8139"
SRC_URI[libreport-plugin-bugzilla.sha256sum] = "36d0d705bc66b905b88ba38ebb97fd6623f4fb5a5f5e297cfb214dcbb561d02f"
SRC_URI[libreport-plugin-kerneloops.sha256sum] = "1f3feebe652dde017049c07f9a8f6add9d47eaa9b61bb57e4798c00ca570591c"
SRC_URI[libreport-plugin-logger.sha256sum] = "205229fa2ec9cae9038ee1e2f3a287911c9767bf137cca675731c4b4e0ca95e0"
SRC_URI[libreport-plugin-mailx.sha256sum] = "f1389be3ce31f02b3f4737ea989368d8f2bf724f0c34ab6b800312b55fde4ec8"
SRC_URI[libreport-plugin-reportuploader.sha256sum] = "19e17a6ab1e23f2fad8bc57587261705d780a4d361012b2ce01e273bc603e5a2"
SRC_URI[libreport-plugin-rhtsupport.sha256sum] = "1264836f16ee9df6cd5ccee47f3ac57106d984d45be5dabfd4e596c6ffa767be"
SRC_URI[libreport-plugin-ureport.sha256sum] = "8722d1be3bb48ba3b78b9d61bb37faaa2cb502d6c4344ec668a8652620d45fc7"
SRC_URI[libreport-rhel.sha256sum] = "69cc317eb169a57c6332fb68df9cd28418a8e3dc932d909bccecfcd473f6541d"
SRC_URI[libreport-rhel-anaconda-bugzilla.sha256sum] = "26ae4a277e1a36da94ba998764535a0b04ec4aa21968ac823897fb111d7d29a9"
SRC_URI[libreport-rhel-bugzilla.sha256sum] = "5a5a9224a6a3badc8aa87883e08f99fe73d3081069a3950477f50e89068c58fa"
SRC_URI[libreport-web.sha256sum] = "3b11600e912ae5ea77429ab007a9b91ad81bd04bea4946a7f7a12214b3b5b36c"
SRC_URI[python3-libreport.sha256sum] = "aa75d9e6e8c64c1a1f4ac34d8c67b84d30fa102477e2f094aa7decd645c75979"
