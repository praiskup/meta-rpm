SUMMARY = "generated recipe based on fxload srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_fxload = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_fxload = "glibc systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fxload-2008_10_13-10.el8.aarch64.rpm \
          "

SRC_URI[fxload.sha256sum] = "d9226141451f7e2f5688b3b9d59a27c7c15d75a98d36b77a33be3f462601f0cf"
