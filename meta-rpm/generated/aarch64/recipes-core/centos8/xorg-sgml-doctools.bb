SUMMARY = "generated recipe based on xorg-sgml-doctools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xorg-sgml-doctools = "pkgconf-pkg-config xml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-sgml-doctools-1.11-6.el8.noarch.rpm \
          "

SRC_URI[xorg-sgml-doctools.sha256sum] = "2248e5daa4a3a7a85bb1ce50b1e3f0acd55c1fd860cd1c5f7679b450749d2f98"
