SUMMARY = "generated recipe based on jboss-interceptors-1.2-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-interceptors-1.2-api = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jboss-interceptors-1.2-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jboss-interceptors-1.2-api-1.0.0-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jboss-interceptors-1.2-api-javadoc-1.0.0-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jboss-interceptors-1.2-api.sha256sum] = "5ec5c307394f1ff8ab37aa30eb93e6ba3681eeb2c6dba7dd10554bd74e3288af"
SRC_URI[jboss-interceptors-1.2-api-javadoc.sha256sum] = "534c217887eb850cbd8639129d639a7390db39fa9b7cb44aa642f77756ed39e2"
