SUMMARY = "generated recipe based on cockpit-composer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_cockpit-composer = "cockpit lorax-composer"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cockpit-composer-12.1-1.el8.noarch.rpm \
          "

SRC_URI[cockpit-composer.sha256sum] = "bf723464f82e3de113a924b5e5b5244ac95bf52d82d5daa071e7cd9497182eb8"
