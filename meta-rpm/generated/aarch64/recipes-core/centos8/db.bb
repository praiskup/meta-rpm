SUMMARY = "generated recipe based on libdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc openssl pkgconfig-native"
RPM_SONAME_PROV_libdb = "libdb-5.3.so"
RPM_SONAME_REQ_libdb = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_libdb = "glibc openssl-libs"
RPM_SONAME_PROV_libdb-cxx = "libdb_cxx-5.3.so"
RPM_SONAME_REQ_libdb-cxx = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libdb-cxx = "glibc libdb libgcc libstdc++ openssl-libs"
RPROVIDES_libdb-cxx-devel = "libdb-cxx-dev (= 5.3.28)"
RDEPENDS_libdb-cxx-devel = "libdb-cxx libdb-devel"
RPROVIDES_libdb-devel = "libdb-dev (= 5.3.28)"
RDEPENDS_libdb-devel = "libdb"
RDEPENDS_libdb-devel-doc = "libdb libdb-devel"
RPM_SONAME_PROV_libdb-sql = "libdb_sql-5.3.so"
RPM_SONAME_REQ_libdb-sql = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_libdb-sql = "glibc libdb openssl-libs"
RPM_SONAME_REQ_libdb-sql-devel = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdb_sql-5.3.so libdl.so.2 libpthread.so.0"
RPROVIDES_libdb-sql-devel = "libdb-sql-dev (= 5.3.28)"
RDEPENDS_libdb-sql-devel = "glibc libdb-sql openssl-libs"
RPM_SONAME_REQ_libdb-utils = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdb-5.3.so libpthread.so.0"
RDEPENDS_libdb-utils = "glibc libdb openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdb-devel-5.3.28-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libdb-5.3.28-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libdb-utils-5.3.28-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdb-cxx-5.3.28-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdb-cxx-devel-5.3.28-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdb-devel-doc-5.3.28-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdb-sql-5.3.28-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdb-sql-devel-5.3.28-37.el8.aarch64.rpm \
          "

SRC_URI[libdb.sha256sum] = "ad06d1753bb443403e544c02837579cb0b7836d380ca59c88cd0e89894870615"
SRC_URI[libdb-cxx.sha256sum] = "d0247212605fb7b11d10817d3d4ad51a36586a86ce9ba3a71704f2dee5545766"
SRC_URI[libdb-cxx-devel.sha256sum] = "e8c2c21ded67738aae6174b17743d6d2fda1ce178b5cf9db48ff5500d3a91f1d"
SRC_URI[libdb-devel.sha256sum] = "1824ce0db81cd1c496ea0d4b87cecc5c831b2a77128b0de26a14fe1b0d736531"
SRC_URI[libdb-devel-doc.sha256sum] = "9766e7612e003692b61a686c7bd53a25ebadc467ae3bc8e01dca0a0b64fa891c"
SRC_URI[libdb-sql.sha256sum] = "5bb7096a4995c1215b40dc6e9651e57d71f9284d30983025c2420fc1e3d7740f"
SRC_URI[libdb-sql-devel.sha256sum] = "42e05f5f5e8fb618d32a5e7915b839d0b54140862096c775ffdf72ddf886063f"
SRC_URI[libdb-utils.sha256sum] = "9c174435e558ebc58198efd824803a01275831c69e8969fc567a34b6f9d07f6d"
