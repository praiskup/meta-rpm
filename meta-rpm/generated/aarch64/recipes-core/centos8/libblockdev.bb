SUMMARY = "generated recipe based on libblockdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cryptsetup-libs device-mapper-libs glib-2.0 kmod libblkid libbytesize libgcc libmount libuuid libyaml ndctl nspr nss parted pkgconfig-native systemd-libs volume-key"
RPM_SONAME_PROV_libblockdev = "libblockdev.so.2"
RPM_SONAME_REQ_libblockdev = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev = "glib2 glibc kmod-libs libblockdev-utils libgcc systemd-libs"
RPM_SONAME_PROV_libblockdev-crypto = "libbd_crypto.so.2"
RPM_SONAME_REQ_libblockdev-crypto = "ld-linux-aarch64.so.1 libbd_utils.so.2 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libudev.so.1 libvolume_key.so.1"
RDEPENDS_libblockdev-crypto = "cryptsetup-libs glib2 glibc kmod-libs libblkid libblockdev-utils nspr nss nss-util systemd-libs volume_key-libs"
RPM_SONAME_PROV_libblockdev-dm = "libbd_dm.so.2"
RPM_SONAME_REQ_libblockdev-dm = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-dm = "device-mapper device-mapper-libs glib2 glibc kmod-libs libblockdev-utils systemd-libs"
RPM_SONAME_PROV_libblockdev-fs = "libbd_fs.so.2"
RPM_SONAME_REQ_libblockdev-fs = "ld-linux-aarch64.so.1 libbd_part_err.so.2 libbd_utils.so.2 libblkid.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libmount.so.1 libparted-fs-resize.so.0 libparted.so.2 libudev.so.1"
RDEPENDS_libblockdev-fs = "glib2 glibc kmod-libs libblkid libblockdev-utils libgcc libmount parted systemd-libs"
RPM_SONAME_PROV_libblockdev-kbd = "libbd_kbd.so.2"
RPM_SONAME_REQ_libblockdev-kbd = "ld-linux-aarch64.so.1 libbd_utils.so.2 libbytesize.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-kbd = "glib2 glibc kmod-libs libblockdev-utils libbytesize systemd-libs"
RPM_SONAME_PROV_libblockdev-loop = "libbd_loop.so.2"
RPM_SONAME_REQ_libblockdev-loop = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-loop = "glib2 glibc kmod-libs libblockdev-utils systemd-libs"
RPM_SONAME_PROV_libblockdev-lvm = "libbd_lvm.so.2"
RPM_SONAME_REQ_libblockdev-lvm = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-lvm = "device-mapper-libs device-mapper-persistent-data glib2 glibc kmod-libs libblockdev-utils lvm2 systemd-libs"
RPM_SONAME_PROV_libblockdev-lvm-dbus = "libbd_lvm-dbus.so.2"
RPM_SONAME_REQ_libblockdev-lvm-dbus = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libpthread.so.0 libudev.so.1"
RDEPENDS_libblockdev-lvm-dbus = "device-mapper-libs device-mapper-persistent-data glib2 glibc kmod-libs libblockdev-utils lvm2-dbusd systemd-libs"
RPM_SONAME_PROV_libblockdev-mdraid = "libbd_mdraid.so.2"
RPM_SONAME_REQ_libblockdev-mdraid = "ld-linux-aarch64.so.1 libbd_utils.so.2 libbytesize.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-mdraid = "glib2 glibc kmod-libs libblockdev-utils libbytesize mdadm systemd-libs"
RPM_SONAME_PROV_libblockdev-mpath = "libbd_mpath.so.2"
RPM_SONAME_REQ_libblockdev-mpath = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-mpath = "device-mapper-libs device-mapper-multipath glib2 glibc kmod-libs libblockdev-utils systemd-libs"
RPM_SONAME_PROV_libblockdev-nvdimm = "libbd_nvdimm.so.2"
RPM_SONAME_REQ_libblockdev-nvdimm = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libndctl.so.6 libudev.so.1 libuuid.so.1"
RDEPENDS_libblockdev-nvdimm = "glib2 glibc kmod-libs libblockdev-utils libuuid ndctl ndctl-libs systemd-libs"
RPM_SONAME_PROV_libblockdev-part = "libbd_part.so.2"
RPM_SONAME_REQ_libblockdev-part = "ld-linux-aarch64.so.1 libbd_part_err.so.2 libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libparted-fs-resize.so.0 libparted.so.2 libudev.so.1"
RDEPENDS_libblockdev-part = "gdisk glib2 glibc kmod-libs libblockdev-utils parted systemd-libs util-linux"
RDEPENDS_libblockdev-plugins-all = "libblockdev libblockdev-crypto libblockdev-dm libblockdev-fs libblockdev-kbd libblockdev-loop libblockdev-lvm libblockdev-mdraid libblockdev-mpath libblockdev-nvdimm libblockdev-part libblockdev-swap libblockdev-vdo"
RPM_SONAME_PROV_libblockdev-swap = "libbd_swap.so.2"
RPM_SONAME_REQ_libblockdev-swap = "ld-linux-aarch64.so.1 libbd_utils.so.2 libblkid.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-swap = "glib2 glibc kmod-libs libblkid libblockdev-utils systemd-libs util-linux"
RPM_SONAME_PROV_libblockdev-utils = "libbd_part_err.so.2 libbd_utils.so.2"
RPM_SONAME_REQ_libblockdev-utils = "ld-linux-aarch64.so.1 libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libparted-fs-resize.so.0 libparted.so.2 libudev.so.1"
RDEPENDS_libblockdev-utils = "glib2 glibc kmod-libs parted systemd-libs"
RPM_SONAME_PROV_libblockdev-vdo = "libbd_vdo.so.2"
RPM_SONAME_REQ_libblockdev-vdo = "ld-linux-aarch64.so.1 libbd_utils.so.2 libbytesize.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1 libyaml-0.so.2"
RDEPENDS_libblockdev-vdo = "glib2 glibc kmod-kvdo kmod-libs libblockdev-utils libbytesize libyaml systemd-libs vdo"
RDEPENDS_python3-blockdev = "libblockdev platform-python python3-gobject-base"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-crypto-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-dm-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-fs-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-kbd-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-loop-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-lvm-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-lvm-dbus-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-mdraid-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-mpath-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-nvdimm-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-part-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-plugins-all-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-swap-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-utils-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libblockdev-vdo-2.19-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-blockdev-2.19-12.el8.aarch64.rpm \
          "

SRC_URI[libblockdev.sha256sum] = "b592bbdb78f8faba7e856b3a11d7fe30cf3c8e124e5ad1db862ca5c7f9911cfd"
SRC_URI[libblockdev-crypto.sha256sum] = "9ec0423906ae0aecd43a0246527f0541997a0d3ecf56c9d3f8460701d1ec2f81"
SRC_URI[libblockdev-dm.sha256sum] = "0d14c37019f1948da46ce56d91b510ec2196ba665902b2f518ff0704264304c0"
SRC_URI[libblockdev-fs.sha256sum] = "c23928aa3b732218a39895a1e891603dc459254404df66d1c7d92c262465d34a"
SRC_URI[libblockdev-kbd.sha256sum] = "26c0bc854eb732f79be4a4479a33efd9fa596c781f2134670eeb571d456ac95d"
SRC_URI[libblockdev-loop.sha256sum] = "ac64d35639ba7c09289d32fd94bf6d792541e688fe62def5f591d3ef7ca67a96"
SRC_URI[libblockdev-lvm.sha256sum] = "988b43541e05d16c25851fe3c595f1895d1980ad6551b6b87029b4522f60e662"
SRC_URI[libblockdev-lvm-dbus.sha256sum] = "c4b9bd8630ddebf646c8c9402e8993da8a299c98ffc56d8c2281410495e4e2a8"
SRC_URI[libblockdev-mdraid.sha256sum] = "34eb28d44a9199f9fc4b13f4b14ec939cb9f326d8a8b46832124f92c5984b662"
SRC_URI[libblockdev-mpath.sha256sum] = "ad08805108b7e4a350d02b0b87a74a585afeb19a246e6ab46c3ba09433ad34a6"
SRC_URI[libblockdev-nvdimm.sha256sum] = "3648c2a4d734c368067d9e4ca41e06dafd132309d5051cd9adbc6412902a640e"
SRC_URI[libblockdev-part.sha256sum] = "6522ef7c420751416dedc591f9a50416a47ec841aa917858da75f8b81d1ade0b"
SRC_URI[libblockdev-plugins-all.sha256sum] = "8c7b56f23113d13d2529f6cffc5238ef2cf91985ec67a2ce9b627396e60332de"
SRC_URI[libblockdev-swap.sha256sum] = "ab5cc5526dd962d5a34372e45273f26ec5bd35799b2279b29f9a4728ad21b2cd"
SRC_URI[libblockdev-utils.sha256sum] = "a9445515f211d922d33fbcfbd75729e0e8b8cdab3142cb93a38d7819bfda94d8"
SRC_URI[libblockdev-vdo.sha256sum] = "5a721b7c4536c4e20260108c4885c62441d0e8390d89f658b0fe13b21d145424"
SRC_URI[python3-blockdev.sha256sum] = "dd6d645736ae6ec2ba09639f5d2ca56fe189fc8807ad0123a488639f2072a8c5"
