SUMMARY = "generated recipe based on autotrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng pkgconfig-native pstoedit zlib"
RPM_SONAME_PROV_autotrace = "libautotrace.so.3"
RPM_SONAME_REQ_autotrace = "ld-linux-aarch64.so.1 libautotrace.so.3 libc.so.6 libdl.so.2 libm.so.6 libpng16.so.16 libpstoedit.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_autotrace = "glibc libpng libstdc++ pstoedit zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/autotrace-0.31.1-52.el8.aarch64.rpm \
          "

SRC_URI[autotrace.sha256sum] = "da636b05053ee564785501239c057cc07b009cd1d55068c8a9097b26d5c2fa92"
