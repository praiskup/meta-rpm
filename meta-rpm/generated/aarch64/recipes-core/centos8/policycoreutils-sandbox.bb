SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap-ng libselinux pkgconfig-native"
RPM_SONAME_REQ_policycoreutils-sandbox = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libselinux.so.1"
RDEPENDS_policycoreutils-sandbox = "bash glibc libcap-ng libselinux matchbox-window-manager platform-python python3-policycoreutils rsync xorg-x11-server-Xephyr xorg-x11-server-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/policycoreutils-sandbox-2.9-9.el8.aarch64.rpm \
          "

SRC_URI[policycoreutils-sandbox.sha256sum] = "f097912886336ae800bc56de93a0ebe34df2012213c38397ba72de70628cbc44"
