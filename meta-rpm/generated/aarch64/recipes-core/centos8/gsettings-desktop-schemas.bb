SUMMARY = "generated recipe based on gsettings-desktop-schemas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gsettings-desktop-schemas = "glib2"
RPROVIDES_gsettings-desktop-schemas-devel = "gsettings-desktop-schemas-dev (= 3.32.0)"
RDEPENDS_gsettings-desktop-schemas-devel = "gsettings-desktop-schemas pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gsettings-desktop-schemas-devel-3.32.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gsettings-desktop-schemas-3.32.0-4.el8.aarch64.rpm \
          "

SRC_URI[gsettings-desktop-schemas.sha256sum] = "ed9ce8ce2fca4a134f2e49373a84f3e02c54d499279cb89861ca3919b25add41"
SRC_URI[gsettings-desktop-schemas-devel.sha256sum] = "d2f85432d7545f63244bb0fc58b6db4b764565e1622b39485098c1a744836cef"
