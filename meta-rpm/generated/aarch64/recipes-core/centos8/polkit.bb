SUMMARY = "generated recipe based on polkit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat glib-2.0 libgcc mozjs60 pam pkgconfig-native systemd-libs"
RPM_SONAME_REQ_polkit = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libmozjs-60.so.0 libpam.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libstdc++.so.6 libsystemd.so.0"
RDEPENDS_polkit = "bash dbus expat glib2 glibc libgcc libstdc++ mozjs60 pam polkit-libs polkit-pkla-compat shadow-utils systemd systemd-libs"
RPM_SONAME_REQ_polkit-devel = "libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0"
RPROVIDES_polkit-devel = "polkit-dev (= 0.115)"
RDEPENDS_polkit-devel = "glib2-devel pkgconf-pkg-config polkit-docs polkit-libs"
RDEPENDS_polkit-docs = "polkit-devel"
RPM_SONAME_PROV_polkit-libs = "libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0"
RPM_SONAME_REQ_polkit-libs = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_polkit-libs = "expat glib2 glibc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/polkit-0.115-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/polkit-devel-0.115-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/polkit-docs-0.115-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/polkit-libs-0.115-11.el8.aarch64.rpm \
          "

SRC_URI[polkit.sha256sum] = "eac1e232eca5c21531c74e6ee646a0afa96b0a9f2a79016abfe9892898f0d05a"
SRC_URI[polkit-devel.sha256sum] = "10ec4cae4b887a77b63ed0c1f72d273304413085a2f79d309bdd9aa85453cc42"
SRC_URI[polkit-docs.sha256sum] = "a0aae6dff05fa061ffadc9ed59ecf45b4f05716c9c9bf5569c152af85f539e92"
SRC_URI[polkit-libs.sha256sum] = "36ef3e3aa546eaf01b011d9ff763a8bcaceb98fe4050f345bc140720a03db5e7"
