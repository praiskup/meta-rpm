SUMMARY = "generated recipe based on hyphen-ml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ml = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-ml-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-ml.sha256sum] = "fa60ba739957ad16be108d73159d6ddfa8bee0168673eb91b508f2840bdb9cb1"
