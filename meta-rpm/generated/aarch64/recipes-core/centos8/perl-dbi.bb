SUMMARY = "generated recipe based on perl-DBI srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-DBI = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-DBI = "glibc perl-Carp perl-Data-Dumper perl-Errno perl-Exporter perl-Getopt-Long perl-IO perl-Math-BigInt perl-PathTools perl-Scalar-List-Utils perl-Storable perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-DBI-1.641-1.el8.aarch64.rpm \
          "

SRC_URI[perl-DBI.sha256sum] = "28ae1acb8e285483d037426cf62b0f7a802ce3111735eed886300f996667fba3"
