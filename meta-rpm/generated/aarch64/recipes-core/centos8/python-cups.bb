SUMMARY = "generated recipe based on python-cups srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cups = "ld-linux-aarch64.so.1 libc.so.6 libcups.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-cups = "cups-libs glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-cups-1.9.72-21.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python-cups-doc-1.9.72-21.el8.0.1.aarch64.rpm \
          "

SRC_URI[python-cups-doc.sha256sum] = "eaf9f5168f0290ba9c84d4e1d253270ec705d7f7c29cef8b75a68b50021557db"
SRC_URI[python3-cups.sha256sum] = "c9edfb1ca7b036e9dafeda526c4e8463b51919328778abd789842fa889445ed4"
