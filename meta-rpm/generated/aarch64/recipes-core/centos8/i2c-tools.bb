SUMMARY = "generated recipe based on i2c-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_i2c-tools = "ld-linux-aarch64.so.1 libc.so.6 libi2c.so.0"
RDEPENDS_i2c-tools = "bash glibc kmod libi2c systemd-udev"
RDEPENDS_i2c-tools-perl = "libi2c perl-constant perl-interpreter perl-libs"
RPM_SONAME_PROV_libi2c = "libi2c.so.0"
RPM_SONAME_REQ_libi2c = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libi2c = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/i2c-tools-4.0-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/i2c-tools-perl-4.0-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libi2c-4.0-12.el8.aarch64.rpm \
          "

SRC_URI[i2c-tools.sha256sum] = "fbcebf1da0a39cfe429c5fb7a7f1bbb1b412e4a931adec4c43d437c3caedbc9d"
SRC_URI[i2c-tools-perl.sha256sum] = "f1966f80913c48621766776729965f690afd9e19d412a6eefff8747689d96c72"
SRC_URI[libi2c.sha256sum] = "fba30f9207f5d5a071145eceb0a5b4d6c068cd4a9f80e00788a0b0e4ceca021e"
