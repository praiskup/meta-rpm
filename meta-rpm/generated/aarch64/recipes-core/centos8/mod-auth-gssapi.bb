SUMMARY = "generated recipe based on mod_auth_gssapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs openssl pkgconfig-native"
RPM_SONAME_REQ_mod_auth_gssapi = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0 libssl.so.1.1"
RDEPENDS_mod_auth_gssapi = "glibc httpd krb5-libs libcom_err openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_auth_gssapi-1.6.1-6.el8.aarch64.rpm \
          "

SRC_URI[mod_auth_gssapi.sha256sum] = "71bb6da2508b18f42919d49fcfffa19213b8aefdef583edcd83deab1d6c4601a"
