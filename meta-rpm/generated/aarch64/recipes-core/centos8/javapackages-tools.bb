SUMMARY = "generated recipe based on javapackages-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ivy-local = "apache-ivy javapackages-local javapackages-tools xmvn-connector-ivy"
RDEPENDS_javapackages-local = "java-1.8.0-openjdk-devel javapackages-tools platform-python python3-javapackages xmvn-install xmvn-resolve xmvn-subst"
RDEPENDS_javapackages-tools = "coreutils findutils java-1.8.0-openjdk-headless javapackages-filesystem which"
RDEPENDS_maven-local = "javapackages-local javapackages-tools junit maven-compiler-plugin maven-jar-plugin maven-resources-plugin maven-surefire-plugin maven-surefire-provider-junit maven-surefire-provider-testng xmvn-connector-aether xmvn-minimal xmvn-mojo"
RDEPENDS_python3-javapackages = "platform-python python3-lxml python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/javapackages-filesystem-5.3.0-1.module_el8.0.0+11+5b8c10bd.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/javapackages-tools-5.3.0-1.module_el8.0.0+11+5b8c10bd.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ivy-local-5.3.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javapackages-local-5.3.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-local-5.3.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-javapackages-5.3.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[ivy-local.sha256sum] = "2ab3a5375cbc944e5d620150c714eb3edcd43ea39647db10fb33b641345ff8e0"
SRC_URI[javapackages-filesystem.sha256sum] = "64ab9dd5e8024a1148b2f902cad82434b3afa42d86f63b06f09cb8d98f0836c1"
SRC_URI[javapackages-local.sha256sum] = "58fcbf1ad89c831572f3492f0ebf6c7f8c16a63146366f0d99fe91af588a4433"
SRC_URI[javapackages-tools.sha256sum] = "dd85efe8ae36a9b0abb3544e93a57cc37237286f56e1c2a3b1367a9397f02f60"
SRC_URI[maven-local.sha256sum] = "f323df12a3f23000df051c3acdcd4d857e89fba7a3b6e77d12cf96e054972eee"
SRC_URI[python3-javapackages.sha256sum] = "36c85c785127a3aa84c677e4c47d794615a3d6e95a7b7698c9b46719536ab391"
