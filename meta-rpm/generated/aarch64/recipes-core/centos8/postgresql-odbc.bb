SUMMARY = "generated recipe based on postgresql-odbc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpq pkgconfig-native unixodbc"
RPM_SONAME_REQ_postgresql-odbc = "ld-linux-aarch64.so.1 libc.so.6 libodbcinst.so.2 libpq.so.5 libpthread.so.0"
RDEPENDS_postgresql-odbc = "glibc libpq unixODBC"
RDEPENDS_postgresql-odbc-tests = "bash gcc make postgresql-odbc unixODBC-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postgresql-odbc-10.03.0000-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/postgresql-odbc-tests-10.03.0000-2.el8.aarch64.rpm \
          "

SRC_URI[postgresql-odbc.sha256sum] = "c6e443be3f03454db07ec459814571f1d8ad3b6dd5b4600ce799d676075f8cc4"
SRC_URI[postgresql-odbc-tests.sha256sum] = "fabfaad446c22691cc120b0b9894f69df3486f9c4157cbcdcd8f3714ed2f8bac"
