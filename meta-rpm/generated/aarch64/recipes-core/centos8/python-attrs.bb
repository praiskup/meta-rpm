SUMMARY = "generated recipe based on python-attrs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-attrs = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-attrs-17.4.0-6.el8.noarch.rpm \
          "

SRC_URI[python3-attrs.sha256sum] = "ff726c23fb57657988784d8421bc23283501757a5b973372d13d2c2484545b30"
