SUMMARY = "generated recipe based on aspell-en srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_aspell-en = "aspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/aspell-en-2017.08.24-2.el8.aarch64.rpm \
          "

SRC_URI[aspell-en.sha256sum] = "cd93d30dfdf3cb4ee77cb39843083937c18bd76a0bdd850b85eaf694fbb323d0"
