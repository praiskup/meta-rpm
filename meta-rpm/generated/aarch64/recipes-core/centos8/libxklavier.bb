SUMMARY = "generated recipe based on libxklavier srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libx11 libxi libxkbfile libxml2 pkgconfig-native"
RPM_SONAME_PROV_libxklavier = "libxklavier.so.16"
RPM_SONAME_REQ_libxklavier = "ld-linux-aarch64.so.1 libX11.so.6 libXi.so.6 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libxkbfile.so.1 libxml2.so.2"
RDEPENDS_libxklavier = "glib2 glibc iso-codes libX11 libXi libxkbfile libxml2"
RPM_SONAME_REQ_libxklavier-devel = "libxklavier.so.16"
RPROVIDES_libxklavier-devel = "libxklavier-dev (= 5.4)"
RDEPENDS_libxklavier-devel = "glib2-devel libxklavier libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxklavier-5.4-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libxklavier-devel-5.4-11.el8.aarch64.rpm \
          "

SRC_URI[libxklavier.sha256sum] = "cdc87dfd7af40b5b1599b698c94a2aad6cbab9c0e45269e6e4f2aca1f1a41e3e"
SRC_URI[libxklavier-devel.sha256sum] = "f043dbab224e86fb3e0694d753826794807b7641f81386c3db87cf4b40f7b014"
