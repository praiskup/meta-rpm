SUMMARY = "generated recipe based on libgusb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libusb1 pkgconfig-native"
RPM_SONAME_PROV_libgusb = "libgusb.so.2"
RPM_SONAME_REQ_libgusb = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libusb-1.0.so.0"
RDEPENDS_libgusb = "glib2 glibc libusbx"
RPM_SONAME_REQ_libgusb-devel = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgusb.so.2"
RPROVIDES_libgusb-devel = "libgusb-dev (= 0.3.0)"
RDEPENDS_libgusb-devel = "glib2 glib2-devel glibc libgusb libusbx-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgusb-0.3.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgusb-devel-0.3.0-1.el8.aarch64.rpm \
          "

SRC_URI[libgusb.sha256sum] = "995a24fdcc789353e5f6670c3b14f317391aead116e782099895f44fe4a1bcd7"
SRC_URI[libgusb-devel.sha256sum] = "85b7908f1c3abb023f0ef887a5ef7949ff7187f562cbcd3a9d8ea4aabae78888"
