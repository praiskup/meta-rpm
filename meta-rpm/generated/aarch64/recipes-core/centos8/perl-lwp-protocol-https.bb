SUMMARY = "generated recipe based on perl-LWP-Protocol-https srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-LWP-Protocol-https = "perl-IO-Socket-SSL perl-Mozilla-CA perl-Net-HTTP perl-libs perl-libwww-perl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-LWP-Protocol-https-6.07-4.el8.noarch.rpm \
          "

SRC_URI[perl-LWP-Protocol-https.sha256sum] = "bfb791d057bd1707edc7e6dfda9d498e3952a80ef5928246b53571498af135e7"
