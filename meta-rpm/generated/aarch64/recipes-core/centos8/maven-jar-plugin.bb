SUMMARY = "generated recipe based on maven-jar-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-jar-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-archiver maven-lib plexus-archiver plexus-utils"
RDEPENDS_maven-jar-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-jar-plugin-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-jar-plugin-javadoc-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-jar-plugin.sha256sum] = "be8bb0f5605b6dce70eb37081b8c347320a2f96c188cbd9eec066e79be85d597"
SRC_URI[maven-jar-plugin-javadoc.sha256sum] = "e7d94c2c2eed2e785782c3aa71b33906dba364dabc235533dcd333dd2f5d2a4c"
