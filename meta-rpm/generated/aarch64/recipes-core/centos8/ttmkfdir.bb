SUMMARY = "generated recipe based on ttmkfdir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_ttmkfdir = "ld-linux-aarch64.so.1 libc.so.6 libfreetype.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_ttmkfdir = "freetype glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ttmkfdir-3.0.9-54.el8.aarch64.rpm \
          "

SRC_URI[ttmkfdir.sha256sum] = "d25072e50ca8153f7b4b25a06638e63dd8893521f516019502055a36836c75b3"
