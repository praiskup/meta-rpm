SUMMARY = "generated recipe based on ipvsadm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native popt"
RPM_SONAME_REQ_ipvsadm = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libnl-genl-3.so.200 libpopt.so.0"
RDEPENDS_ipvsadm = "bash glibc libnl3 popt systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ipvsadm-1.31-1.el8.aarch64.rpm \
          "

SRC_URI[ipvsadm.sha256sum] = "24a6d457398054a8b75ed8d418988dabeabb1c81722d112cd195363680e75d01"
