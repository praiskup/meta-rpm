SUMMARY = "generated recipe based on geoipupdate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl pkgconfig-native zlib"
RPM_SONAME_REQ_geoipupdate = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libz.so.1"
RDEPENDS_geoipupdate = "glibc libcurl zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/geoipupdate-2.5.0-2.el8.aarch64.rpm \
          "

SRC_URI[geoipupdate.sha256sum] = "e6fb3414468ae06d07a55e9834e6b507d2df6f925c3b171448a41d3505c6cf59"
