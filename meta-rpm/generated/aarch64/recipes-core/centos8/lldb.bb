SUMMARY = "generated recipe based on lldb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "clang libedit libgcc libxml2 llvm ncurses pkgconfig-native platform-python3"
RPM_SONAME_PROV_lldb = "liblldb.so.9 liblldbIntelFeatures.so.9"
RPM_SONAME_REQ_lldb = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libclangAST.so.9 libclangBasic.so.9 libclangCodeGen.so.9 libclangDriver.so.9 libclangEdit.so.9 libclangFrontend.so.9 libclangLex.so.9 libclangParse.so.9 libclangRewrite.so.9 libclangRewriteFrontend.so.9 libclangSema.so.9 libclangSerialization.so.9 libclangTooling.so.9 libdl.so.2 libedit.so.0 libform.so.6 libgcc_s.so.1 liblldb.so.9 libm.so.6 libncurses.so.6 libpanel.so.6 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6 libtinfo.so.6 libxml2.so.2"
RDEPENDS_lldb = "clang-libs glibc libedit libgcc libstdc++ libxml2 llvm-libs ncurses-libs python3-libs python3-lldb"
RPM_SONAME_REQ_lldb-devel = "liblldb.so.9 liblldbIntelFeatures.so.9"
RPROVIDES_lldb-devel = "lldb-dev (= 9.0.1)"
RDEPENDS_lldb-devel = "lldb"
RPM_SONAME_REQ_python3-lldb = "liblldb.so.9"
RDEPENDS_python3-lldb = "lldb platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lldb-9.0.1-2.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lldb-devel-9.0.1-2.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-lldb-9.0.1-2.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
          "

SRC_URI[lldb.sha256sum] = "59362ca1cd801998d854561c2df04ea060d88abe475b08b84b2555ef18a41354"
SRC_URI[lldb-devel.sha256sum] = "11fa6907f7d5d1a27287a9576e23a2f1dd29879f156d27b238c0990af3fd4e4a"
SRC_URI[python3-lldb.sha256sum] = "55e7b55f24381a633ff23aa1702e2b95e79d8c450cc17e5cc47e20e1772217cf"
