SUMMARY = "generated recipe based on glassfish-jsp-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_glassfish-jsp-api = "glassfish-el-api glassfish-servlet-api java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_glassfish-jsp-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glassfish-jsp-api-2.3.2-0.9.b01.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glassfish-jsp-api-javadoc-2.3.2-0.9.b01.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[glassfish-jsp-api.sha256sum] = "c6bdd008ce0060d0ab9a2ac8f49b349f0ccd0e63b66140045e83ce6c9d2826da"
SRC_URI[glassfish-jsp-api-javadoc.sha256sum] = "33ec50f0e598f01075f8632d138062350d1f14585ceb34f87dc1a0cd9ccc84f9"
