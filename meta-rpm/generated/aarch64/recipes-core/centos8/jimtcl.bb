SUMMARY = "generated recipe based on jimtcl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_jimtcl = "libjim.so.0.77"
RPM_SONAME_REQ_jimtcl = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libjim.so.0.77 libm.so.6"
RDEPENDS_jimtcl = "glibc"
RPM_SONAME_REQ_jimtcl-devel = "libjim.so.0.77"
RPROVIDES_jimtcl-devel = "jimtcl-dev (= 0.77)"
RDEPENDS_jimtcl-devel = "jimtcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/jimtcl-0.77-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jimtcl-devel-0.77-5.el8.aarch64.rpm \
          "

SRC_URI[jimtcl.sha256sum] = "23f20e501f6889cd7148b1bc4ce1b690a468b99c7bec2e816e711f06f7177a81"
SRC_URI[jimtcl-devel.sha256sum] = "9121832a853dfbb3992a8dcbb38a65493cdb1fcad6dfcc4a0540580695ccb3d3"
