SUMMARY = "generated recipe based on hunspell-sk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-sk-0.20110228-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-sk.sha256sum] = "4e81e07b527e39f5be57c7f5435ea67f598400dba7e44ea1dd37ddb58c59afc3"
