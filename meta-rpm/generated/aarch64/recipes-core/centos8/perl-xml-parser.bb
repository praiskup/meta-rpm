SUMMARY = "generated recipe based on perl-XML-Parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat perl pkgconfig-native"
RPM_SONAME_REQ_perl-XML-Parser = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-XML-Parser = "expat glibc perl-Carp perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-Parser-2.44-11.el8.aarch64.rpm \
          "

SRC_URI[perl-XML-Parser.sha256sum] = "f198fcbe2a4ad7cabb3020dcf205b85ac5fbdf3ccf6839edfd96053bf948d838"
