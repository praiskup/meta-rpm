SUMMARY = "generated recipe based on setroubleshoot srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-libs libcap-ng libselinux pkgconfig-native"
RDEPENDS_setroubleshoot = "dbus desktop-file-utils gtk3 libnotify libreport-gtk platform-python python3-gobject python3-libreport python3-pydbus setroubleshoot-server xdg-utils"
RPM_SONAME_REQ_setroubleshoot-server = "ld-linux-aarch64.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libdbus-1.so.3 libselinux.so.1"
RDEPENDS_setroubleshoot-server = "audit audit-libs bash dbus dbus-libs glibc initscripts libcap-ng libselinux platform-python policycoreutils-python-utils polkit python3-audit python3-dbus python3-gobject python3-libselinux python3-libxml2 python3-rpm python3-slip-dbus python3-systemd setroubleshoot-plugins shadow-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/setroubleshoot-3.3.22-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/setroubleshoot-server-3.3.22-2.el8.aarch64.rpm \
          "

SRC_URI[setroubleshoot.sha256sum] = "a8290a8ef4717e94aff07c03a31dc9264cb320f13f5bb112ddaa53d5c49bba3d"
SRC_URI[setroubleshoot-server.sha256sum] = "5e7873915383f82db6bd5484d938ff8551f71808487d97b547080605cb202a8d"
