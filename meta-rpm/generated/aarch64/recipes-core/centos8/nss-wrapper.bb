SUMMARY = "generated recipe based on nss_wrapper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_nss_wrapper = "libnss_wrapper.so.0"
RPM_SONAME_REQ_nss_wrapper = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnss_wrapper.so.0 libpthread.so.0"
RDEPENDS_nss_wrapper = "cmake-filesystem glibc perl-Getopt-Long perl-PathTools perl-interpreter perl-libs pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss_wrapper-1.1.5-3.el8.aarch64.rpm \
          "

SRC_URI[nss_wrapper.sha256sum] = "f20abdd7568b076986b8490babdc3309edf8076133761234d3d7a7841eb0b8c3"
