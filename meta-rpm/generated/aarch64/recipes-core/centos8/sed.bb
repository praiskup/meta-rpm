SUMMARY = "generated recipe based on sed srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl libselinux pkgconfig-native"
RPM_SONAME_REQ_sed = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_sed = "bash glibc info libacl libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sed-4.5-1.el8.aarch64.rpm \
          "

SRC_URI[sed.sha256sum] = "fbecd3eaccc7aed98b42e88280d2ee9104c52d3329f9139b291527fc30d5fd02"
