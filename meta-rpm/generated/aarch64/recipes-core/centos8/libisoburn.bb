SUMMARY = "generated recipe based on libisoburn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl libburn libisofs pkgconfig-native readline zlib"
RPM_SONAME_PROV_libisoburn = "libisoburn.so.1"
RPM_SONAME_REQ_libisoburn = "ld-linux-aarch64.so.1 libacl.so.1 libburn.so.4 libc.so.6 libisofs.so.6 libpthread.so.0 libreadline.so.7 libz.so.1"
RDEPENDS_libisoburn = "glibc libacl libburn libisofs readline zlib"
RPM_SONAME_REQ_libisoburn-devel = "libisoburn.so.1"
RPROVIDES_libisoburn-devel = "libisoburn-dev (= 1.4.8)"
RDEPENDS_libisoburn-devel = "libisoburn pkgconf-pkg-config"
RPM_SONAME_REQ_xorriso = "ld-linux-aarch64.so.1 libacl.so.1 libburn.so.4 libc.so.6 libisoburn.so.1 libisofs.so.6 libpthread.so.0 libreadline.so.7 libz.so.1"
RDEPENDS_xorriso = "bash chkconfig coreutils glibc info libacl libburn libisoburn libisofs readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libisoburn-1.4.8-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorriso-1.4.8-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libisoburn-devel-1.4.8-4.el8.aarch64.rpm \
          "

SRC_URI[libisoburn.sha256sum] = "3ff828ef16f6033227d71207bc1b00983b826172fe7c575cd7590a72d846d831"
SRC_URI[libisoburn-devel.sha256sum] = "21d092dca76897bc9ce7880fa3dc0f4905bc5a2d131467257fcd2507a3771e38"
SRC_URI[xorriso.sha256sum] = "4280064ab658525b486d7b8c2ca5f87aeef90002361a0925f2819fd7a7909500"
