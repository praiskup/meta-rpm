SUMMARY = "generated recipe based on orc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_orc = "liborc-0.4.so.0 liborc-test-0.4.so.0"
RPM_SONAME_REQ_orc = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 liborc-0.4.so.0 liborc-test-0.4.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_orc = "glibc"
RPM_SONAME_REQ_orc-compiler = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 liborc-0.4.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_orc-compiler = "glibc orc pkgconf-pkg-config"
RPM_SONAME_REQ_orc-devel = "liborc-0.4.so.0 liborc-test-0.4.so.0"
RPROVIDES_orc-devel = "orc-dev (= 0.4.28)"
RDEPENDS_orc-devel = "orc orc-compiler pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/orc-0.4.28-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/orc-compiler-0.4.28-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/orc-devel-0.4.28-3.el8.aarch64.rpm \
          "

SRC_URI[orc.sha256sum] = "2bf4c2726822d64e1dc7f31afc4cc6b1c7aee935cddeeb3337cf873efb6abbdb"
SRC_URI[orc-compiler.sha256sum] = "77d90b93963df76bce4125e1d1a21937a23f70ef586d67d5d4caa8432182d435"
SRC_URI[orc-devel.sha256sum] = "b5fdda535c638dd22b301186b03f29d5e2225dc04564df48be5b13028228fb80"
