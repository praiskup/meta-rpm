SUMMARY = "generated recipe based on file srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_REQ_file = "ld-linux-aarch64.so.1 libc.so.6 libmagic.so.1 libz.so.1"
RDEPENDS_file = "file-libs glibc zlib"
RPM_SONAME_REQ_file-devel = "libmagic.so.1"
RPROVIDES_file-devel = "file-dev (= 5.33)"
RDEPENDS_file-devel = "file file-libs"
RPM_SONAME_PROV_file-libs = "libmagic.so.1"
RPM_SONAME_REQ_file-libs = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1"
RDEPENDS_file-libs = "glibc zlib"
RDEPENDS_python3-magic = "file platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/file-5.33-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/file-libs-5.33-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-magic-5.33-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/file-devel-5.33-13.el8.aarch64.rpm \
          "

SRC_URI[file.sha256sum] = "9d62c5bb74a7ccd10b277295e700b8f3a1b6bb024d333650b4a0fab7635b360d"
SRC_URI[file-devel.sha256sum] = "d10b4f05ba31aae576f5da2ee68098d963bd15038efdd93f534ac4cb64478755"
SRC_URI[file-libs.sha256sum] = "9897d30307f398109e580f0440d7ec0bf484d4c7f248a1018a0b0f848be4ed00"
SRC_URI[python3-magic.sha256sum] = "a42993ea23df5011cab9b18c8c6c148394b6523f2797292303ac99f8f6411d72"
