SUMMARY = "generated recipe based on perl-DBD-SQLite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native sqlite3"
RPM_SONAME_REQ_perl-DBD-SQLite = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 libsqlite3.so.0"
RDEPENDS_perl-DBD-SQLite = "glibc perl-DBI perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-DBD-SQLite-1.58-2.el8.aarch64.rpm \
          "

SRC_URI[perl-DBD-SQLite.sha256sum] = "cf8a956e75a26e3a2a1da9bbf1dfc3befc776d5dc9fc0e1b122fbcc1c48736c1"
