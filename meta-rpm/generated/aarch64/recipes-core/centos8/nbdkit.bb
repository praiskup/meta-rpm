SUMMARY = "generated recipe based on nbdkit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcc libselinux pkgconfig-native platform-python3 xz zlib"
RPM_SONAME_REQ_nbdkit = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libpthread.so.0 libselinux.so.1"
RDEPENDS_nbdkit = "glibc gnutls libgcc libselinux"
RDEPENDS_nbdkit-bash-completion = "bash-completion nbdkit"
RPM_SONAME_REQ_nbdkit-basic-plugins = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_nbdkit-basic-plugins = "glibc libselinux nbdkit"
RPROVIDES_nbdkit-devel = "nbdkit-dev (= 1.4.2)"
RDEPENDS_nbdkit-devel = "nbdkit pkgconf-pkg-config"
RPM_SONAME_REQ_nbdkit-example-plugins = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_nbdkit-example-plugins = "glibc libselinux nbdkit"
RPM_SONAME_REQ_nbdkit-plugin-gzip = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1 libz.so.1"
RDEPENDS_nbdkit-plugin-gzip = "glibc libselinux nbdkit zlib"
RDEPENDS_nbdkit-plugin-python-common = "nbdkit"
RPM_SONAME_REQ_nbdkit-plugin-python3 = "ld-linux-aarch64.so.1 libc.so.6 libpython3.6m.so.1.0 libselinux.so.1"
RDEPENDS_nbdkit-plugin-python3 = "glibc libselinux nbdkit nbdkit-plugin-python-common python3-libs"
RPM_SONAME_REQ_nbdkit-plugin-xz = "ld-linux-aarch64.so.1 libc.so.6 liblzma.so.5 libselinux.so.1"
RDEPENDS_nbdkit-plugin-xz = "glibc libselinux nbdkit xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-bash-completion-1.4.2-5.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-basic-plugins-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-devel-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-example-plugins-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-plugin-gzip-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-plugin-python-common-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-plugin-python3-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nbdkit-plugin-xz-1.4.2-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[nbdkit.sha256sum] = "7953fc337fb7648a03a5a2dfcb85c1f59eaa44aaf9e26f3ec17ad683ca3b0ac0"
SRC_URI[nbdkit-bash-completion.sha256sum] = "57cb9d10781f50348ce36f5c858ad77079382011c7e4ac8300da1fada17eadaf"
SRC_URI[nbdkit-basic-plugins.sha256sum] = "eaf08c511159a41ad98d0e467089261a5d36355559a54a3393016c8c88bbb600"
SRC_URI[nbdkit-devel.sha256sum] = "b7a845b4afe718f397b5acba6e03501732b8cfca5f2a48e58352bab3481477c4"
SRC_URI[nbdkit-example-plugins.sha256sum] = "11c8dd750f23c8f9c6f3455605506494265a4f3c05e081c42fd76896ddc142e0"
SRC_URI[nbdkit-plugin-gzip.sha256sum] = "acc5aa0523f43afabeab7f362e2f318812a8bb644bf63665961cac26146e98a8"
SRC_URI[nbdkit-plugin-python-common.sha256sum] = "c002d338a55449194fd3d9f405a141493334dddaf467dfe2aeede40728d16adb"
SRC_URI[nbdkit-plugin-python3.sha256sum] = "aab6fdeeb1d63c8b0acc8545eebc6f36675f9b01bbf58c7c8c760da05c27e078"
SRC_URI[nbdkit-plugin-xz.sha256sum] = "8fb6d5e6b693007ea059d9a4205251a1fd8c5a6b2d3b699f58c2564b99d79455"
