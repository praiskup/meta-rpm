SUMMARY = "generated recipe based on python-simpleline srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-simpleline = "platform-python python3-rpm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-simpleline-1.1-2.el8.noarch.rpm \
          "

SRC_URI[python3-simpleline.sha256sum] = "7f0dcd1ee04fe8f51026fb43e337b4037a8d83878fb167f29e18b917687d42f2"
