SUMMARY = "generated recipe based on perl-Pod-Checker srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Checker = "perl-Carp perl-Exporter perl-Getopt-Long perl-Pod-Simple perl-Pod-Usage perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Pod-Checker-1.73-395.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Checker.sha256sum] = "657b23dbef2e635b2a638aa51a6a078fbccf7f72939660803d39ccfd8c7655a7"
