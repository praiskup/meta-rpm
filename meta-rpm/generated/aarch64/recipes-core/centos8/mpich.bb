SUMMARY = "generated recipe based on mpich srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "hwloc libgcc pkgconfig-native"
RPM_SONAME_PROV_mpich = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPM_SONAME_REQ_mpich = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libhwloc.so.5 libmpi.so.12 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_mpich = "environment-modules glibc hwloc-libs libgcc libstdc++ perl-interpreter"
RPM_SONAME_REQ_mpich-devel = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPROVIDES_mpich-devel = "mpich-dev (= 3.2.1)"
RDEPENDS_mpich-devel = "bash gcc-gfortran mpich pkgconf-pkg-config rpm-mpi-hooks"
RDEPENDS_python3-mpich = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpich-3.2.1-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpich-devel-3.2.1-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-mpich-3.2.1-9.el8.aarch64.rpm \
          "

SRC_URI[mpich.sha256sum] = "3231b4de7c48de134bcee8fc28eac24a1dde0b486c0ec7807b0e482e71db00b5"
SRC_URI[mpich-devel.sha256sum] = "58ef3810307a903bd316296011561c931f514f13aa87f54c5175977007b350f6"
SRC_URI[python3-mpich.sha256sum] = "aaeabeb74d79bc500aac6d3d082724b792f83d8964d916e7b405fa0e28e8e6aa"
