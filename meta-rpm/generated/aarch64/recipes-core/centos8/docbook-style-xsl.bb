SUMMARY = "generated recipe based on docbook-style-xsl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_docbook-style-xsl = "bash docbook-dtds libxml2 xml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/docbook-style-xsl-1.79.2-7.el8.noarch.rpm \
          "

SRC_URI[docbook-style-xsl.sha256sum] = "df6f4ed26d9465f49f40b1fd615aff9a2a2353d6afd60564cbc0a4a683b442ea"
