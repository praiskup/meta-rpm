SUMMARY = "generated recipe based on itstool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_itstool = "platform-python python3-libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/itstool-2.0.4-2.el8.noarch.rpm \
          "

SRC_URI[itstool.sha256sum] = "0119ba8aee44bce4b97d7e07f5d636697dadc6f0ffb0a42228368f3fc8d851c2"
