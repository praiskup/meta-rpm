SUMMARY = "generated recipe based on perl-Carp-Clan srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Carp-Clan = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Carp-Clan-6.06-6.el8.noarch.rpm \
          "

SRC_URI[perl-Carp-Clan.sha256sum] = "eb9074981bd361a4e1fd389c9bbde50155f07a48cb365b8502e73f42ab9d19a8"
