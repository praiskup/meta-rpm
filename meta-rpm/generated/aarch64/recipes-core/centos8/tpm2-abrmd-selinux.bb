SUMMARY = "generated recipe based on tpm2-abrmd-selinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_tpm2-abrmd-selinux = "bash libselinux-utils policycoreutils policycoreutils-python-utils selinux-policy selinux-policy-targeted"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm2-abrmd-selinux-2.0.0-3.el8.noarch.rpm \
          "

SRC_URI[tpm2-abrmd-selinux.sha256sum] = "d8441e55a74bedfafb9bb047aac12bda6c1bbd0ff3e51a947b09888c3a0466cb"
