SUMMARY = "generated recipe based on perl-Parse-Yapp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Parse-Yapp = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Parse-Yapp-1.21-2.el8.noarch.rpm \
          "

SRC_URI[perl-Parse-Yapp.sha256sum] = "b43fe7a5ae6bd0ef3dd3fb37cf7d736ed15e65a299c15ddc3990de24f9b5e11c"
