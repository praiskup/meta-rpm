SUMMARY = "generated recipe based on perl-Text-CharWidth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Text-CharWidth = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Text-CharWidth = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Text-CharWidth-0.04-32.el8.aarch64.rpm \
          "

SRC_URI[perl-Text-CharWidth.sha256sum] = "269e56119ee082168d2f18df17e1c0ebc10bd7863ead4c561589e69e5a8930f3"
