SUMMARY = "generated recipe based on perl-Text-Soundex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Text-Soundex = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Text-Soundex = "glibc perl-Carp perl-Exporter perl-Text-Unidecode perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Text-Soundex-3.05-8.el8.aarch64.rpm \
          "

SRC_URI[perl-Text-Soundex.sha256sum] = "f39d5a52480db87a0af4703405180a126f6f48465b227c4148345186edb78fe6"
