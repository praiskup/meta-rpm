SUMMARY = "generated recipe based on plexus-io srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-io = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem plexus-utils"
RDEPENDS_plexus-io-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-io-3.0.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-io-javadoc-3.0.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-io.sha256sum] = "9a41603339ed2b5449f0d7244f9091602a8237c55e8daa899de192aa5dfaf1a5"
SRC_URI[plexus-io-javadoc.sha256sum] = "a75ac2cc05cb64e831727fdd9591924cfec47acb4967a16308df1ff86dfba121"
