SUMMARY = "generated recipe based on perl-Variable-Magic srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Variable-Magic = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Variable-Magic = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Variable-Magic-0.62-3.el8.aarch64.rpm \
          "

SRC_URI[perl-Variable-Magic.sha256sum] = "ef5196c10ddb50fec1347b81823177ad0724c8bf9e94fe901002dbeb06d0ccc4"
