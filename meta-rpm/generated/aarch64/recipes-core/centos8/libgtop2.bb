SUMMARY = "generated recipe based on libgtop2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libgtop2 = "libgtop-2.0.so.11"
RPM_SONAME_REQ_libgtop2 = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libgtop2 = "glib2 glibc"
RPM_SONAME_REQ_libgtop2-devel = "libgtop-2.0.so.11"
RPROVIDES_libgtop2-devel = "libgtop2-dev (= 2.38.0)"
RDEPENDS_libgtop2-devel = "glib2-devel libgtop2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgtop2-2.38.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgtop2-devel-2.38.0-3.el8.aarch64.rpm \
          "

SRC_URI[libgtop2.sha256sum] = "64b8408cd997009bb5be18a95527fc3d44c1c41a0f51aeb94fc0cccc56c8390a"
SRC_URI[libgtop2-devel.sha256sum] = "da3f2cf732d864b59c02e136246a9cda9fe1782d9d1b6c8e49408f8e7d2dc935"
