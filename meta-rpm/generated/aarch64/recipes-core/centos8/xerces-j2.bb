SUMMARY = "generated recipe based on xerces-j2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xerces-j2 = "bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools xalan-j2 xml-commons-apis xml-commons-resolver"
RDEPENDS_xerces-j2-demo = "xerces-j2"
RDEPENDS_xerces-j2-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xerces-j2-2.11.0-34.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xerces-j2-demo-2.11.0-34.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xerces-j2-javadoc-2.11.0-34.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xerces-j2.sha256sum] = "b7d98be45058413f3ee38de5ceeb304f3e0270bddb9276b35255b48887f0dcc9"
SRC_URI[xerces-j2-demo.sha256sum] = "6db9f37e9b8edfc02e8ab88f0189c9d1231fb527fb164f99c5923b9cca3ebb58"
SRC_URI[xerces-j2-javadoc.sha256sum] = "54709839b6ed9c8a04f46384e431744d806ed48c9d54916e83e4d62991c6afae"
