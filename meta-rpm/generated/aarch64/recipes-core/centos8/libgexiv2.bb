SUMMARY = "generated recipe based on libgexiv2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "compat-exiv2-026 exiv2 glib-2.0 libgcc pkgconfig-native"
RPM_SONAME_PROV_libgexiv2 = "libgexiv2.so.2"
RPM_SONAME_REQ_libgexiv2 = "ld-linux-aarch64.so.1 libc.so.6 libexiv2.so.26 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libstdc++.so.6"
RDEPENDS_libgexiv2 = "compat-exiv2-026 glib2 glibc libgcc libstdc++"
RPM_SONAME_REQ_libgexiv2-devel = "libgexiv2.so.2"
RPROVIDES_libgexiv2-devel = "libgexiv2-dev (= 0.10.8)"
RDEPENDS_libgexiv2-devel = "exiv2-devel glib2-devel libgexiv2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgexiv2-0.10.8-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgexiv2-devel-0.10.8-4.el8.aarch64.rpm \
          "

SRC_URI[libgexiv2.sha256sum] = "42a2bddc0cffe11ea6c65492c169cea2ab9804f530bf2dff679353fc4303ec35"
SRC_URI[libgexiv2-devel.sha256sum] = "b2644ba0657e145d2bd63ab6154c227051767e60c78684c5f765a0243a54dfff"
