SUMMARY = "generated recipe based on dbxtool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "efivar pkgconfig-native popt"
RPM_SONAME_REQ_dbxtool = "ld-linux-aarch64.so.1 libc.so.6 libefivar.so.1 libpopt.so.0 libpthread.so.0"
RDEPENDS_dbxtool = "bash efivar efivar-libs glibc popt systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbxtool-8-5.el8.aarch64.rpm \
          "

SRC_URI[dbxtool.sha256sum] = "9d21cef2038fc9ea3009fff44f9045283d321a2e50ca4b45fbb29f0427ad5756"
