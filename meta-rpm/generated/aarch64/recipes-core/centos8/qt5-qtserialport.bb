SUMMARY = "generated recipe based on qt5-qtserialport srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase systemd-libs"
RPM_SONAME_PROV_qt5-qtserialport = "libQt5SerialPort.so.5"
RPM_SONAME_REQ_qt5-qtserialport = "ld-linux-aarch64.so.1 libQt5Core.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libudev.so.1"
RDEPENDS_qt5-qtserialport = "glibc libgcc libstdc++ qt5-qtbase systemd-libs"
RPM_SONAME_REQ_qt5-qtserialport-devel = "libQt5SerialPort.so.5"
RPROVIDES_qt5-qtserialport-devel = "qt5-qtserialport-dev (= 5.12.5)"
RDEPENDS_qt5-qtserialport-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtserialport"
RPM_SONAME_REQ_qt5-qtserialport-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5SerialPort.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtserialport-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtserialport"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtserialport-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtserialport-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtserialport-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtserialport.sha256sum] = "935cfcc8c00c044cda5df238940583fedb5263f0844e2059d3a5a565ff0e0d87"
SRC_URI[qt5-qtserialport-devel.sha256sum] = "95a3559ba484335a34659cef085869a70c74958e12910b2ed0a79d1d291e1923"
SRC_URI[qt5-qtserialport-examples.sha256sum] = "37083d60a090cb81f5b4da39cf9a6d0c07f6b5a3c60a2e0d0d8aa63c8079065d"
