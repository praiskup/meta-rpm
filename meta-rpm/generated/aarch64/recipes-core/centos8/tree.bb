SUMMARY = "generated recipe based on tree srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_tree = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_tree = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tree-1.7.0-15.el8.aarch64.rpm \
          "

SRC_URI[tree.sha256sum] = "335c02d243e4c615564f6533a611f84f4874e2b06fc4155efafeb85f8b538636"
