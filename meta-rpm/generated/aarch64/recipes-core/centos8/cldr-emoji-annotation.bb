SUMMARY = "generated recipe based on cldr-emoji-annotation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cldr-emoji-annotation-33.1.0_0-1.el8.noarch.rpm \
          "

SRC_URI[cldr-emoji-annotation.sha256sum] = "a055ed7669856cacf31c2c3d57d1500076b8e95de008854d8878c9afbcd458c1"
