SUMMARY = "generated recipe based on exiv2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_exiv2 = "ld-linux-aarch64.so.1 libc.so.6 libexiv2.so.27 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_exiv2 = "exiv2-libs glibc libgcc libstdc++"
RPM_SONAME_REQ_exiv2-devel = "libexiv2.so.27"
RPROVIDES_exiv2-devel = "exiv2-dev (= 0.27.2)"
RDEPENDS_exiv2-devel = "cmake-filesystem exiv2-libs pkgconf-pkg-config"
RPM_SONAME_PROV_exiv2-libs = "libexiv2.so.27"
RPM_SONAME_REQ_exiv2-libs = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_exiv2-libs = "expat glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/exiv2-0.27.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/exiv2-devel-0.27.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/exiv2-doc-0.27.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/exiv2-libs-0.27.2-5.el8.aarch64.rpm \
          "

SRC_URI[exiv2.sha256sum] = "78ccdad1cf37cbd6a7e0f924ec491acc7c5df7c80163908694d07e2e2e4ab80f"
SRC_URI[exiv2-devel.sha256sum] = "78cab0ef05ef7ed8480ce73958b06e37f5c7f94340846d53b9644f66bfe89e22"
SRC_URI[exiv2-doc.sha256sum] = "7f2bb71efcca211e098c92d56d16b9ae439eb1d883bd433eef085bfe521ecf20"
SRC_URI[exiv2-libs.sha256sum] = "c928454985a82d5bbabd7f5921f3158e5dca388af5ef23262196dc6d7be008c8"
