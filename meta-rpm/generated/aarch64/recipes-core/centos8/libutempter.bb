SUMMARY = "generated recipe based on libutempter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libutempter = "libutempter.so.0"
RPM_SONAME_REQ_libutempter = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libutempter = "bash glibc shadow-utils"
RPM_SONAME_REQ_libutempter-devel = "libutempter.so.0"
RPROVIDES_libutempter-devel = "libutempter-dev (= 1.1.6)"
RDEPENDS_libutempter-devel = "libutempter"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libutempter-1.1.6-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libutempter-devel-1.1.6-14.el8.aarch64.rpm \
          "

SRC_URI[libutempter.sha256sum] = "8f6d9839a758fdacfdb4b4b0731e8023b8bbb0b633bd32dbf21c2ce85a933a8a"
SRC_URI[libutempter-devel.sha256sum] = "59acc47caf0af934373c0b080cd70a0f09769b75794d86da3d4df3afbdc77949"
