SUMMARY = "generated recipe based on binutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
RPM_SONAME_PROV_binutils = "libbfd-2.30-73.el8.so libopcodes-2.30-73.el8.so"
RPM_SONAME_REQ_binutils = "ld-linux-aarch64.so.1 libbfd-2.30-73.el8.so libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libopcodes-2.30-73.el8.so libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_binutils = "bash chkconfig coreutils glibc info libgcc libstdc++ zlib"
RPROVIDES_binutils-devel = "binutils-dev (= 2.30)"
RDEPENDS_binutils-devel = "binutils coreutils info zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/binutils-devel-2.30-73.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/binutils-2.30-73.el8.aarch64.rpm \
          "

SRC_URI[binutils.sha256sum] = "d43aa00845479e64692e717dc5b2e1b3d8c409d7e981ed8e29b1ec3ba65c3bfc"
SRC_URI[binutils-devel.sha256sum] = "47b2a437c2178caad31042b4afea7777415517cb00c185a827bb0e9472bca322"
