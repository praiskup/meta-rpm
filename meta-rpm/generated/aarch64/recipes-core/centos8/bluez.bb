SUMMARY = "generated recipe based on bluez srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs glib-2.0 libical pkgconfig-native readline systemd-libs"
RPM_SONAME_REQ_bluez = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libreadline.so.7 librt.so.1 libudev.so.1"
RDEPENDS_bluez = "bash dbus dbus-libs glib2 glibc readline systemd systemd-libs"
RPM_SONAME_REQ_bluez-cups = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0"
RDEPENDS_bluez-cups = "bluez cups dbus-libs glib2 glibc"
RPM_SONAME_REQ_bluez-hid2hci = "ld-linux-aarch64.so.1 libc.so.6 libudev.so.1"
RDEPENDS_bluez-hid2hci = "bash bluez glibc systemd-libs"
RPM_SONAME_PROV_bluez-libs = "libbluetooth.so.3"
RPM_SONAME_REQ_bluez-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_bluez-libs = "glibc"
RPM_SONAME_REQ_bluez-libs-devel = "libbluetooth.so.3"
RPROVIDES_bluez-libs-devel = "bluez-libs-dev (= 5.50)"
RDEPENDS_bluez-libs-devel = "bluez-libs pkgconf-pkg-config"
RPM_SONAME_REQ_bluez-obexd = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libical.so.3 libicalss.so.3 libicalvcal.so.3"
RDEPENDS_bluez-obexd = "bash bluez bluez-libs dbus-libs glib2 glibc libical"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bluez-cups-5.50-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bluez-5.50-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bluez-hid2hci-5.50-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bluez-libs-5.50-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bluez-obexd-5.50-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bluez-libs-devel-5.50-3.el8.aarch64.rpm \
          "

SRC_URI[bluez.sha256sum] = "b559e283a338cfbd20ad23d43f861c29a608d68322dca5d3285610e894abdcd8"
SRC_URI[bluez-cups.sha256sum] = "b183b70c76ec625f347e55cc2d7e2be8a7610309f846056f8bf3eb9eba2bda28"
SRC_URI[bluez-hid2hci.sha256sum] = "81f34c5ed7273fa83158f7eff10c2702c221455380c2257a1c66f0842f71fc83"
SRC_URI[bluez-libs.sha256sum] = "aa472a64a9c71d6a6266c43b467f0e4f26ff1c177d2c702af1f6502028ecb4a1"
SRC_URI[bluez-libs-devel.sha256sum] = "c4e2d6ebee153aafd087f16317037ab913eae99a9b6be37157ff84d0f95fc1dc"
SRC_URI[bluez-obexd.sha256sum] = "87f7621c0ff7ab8136ec7e3c3e484f9d938c1d9ed265981ec4bb96a8d8defb11"
