SUMMARY = "generated recipe based on fonts-tweak-tool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_fonts-tweak-tool = "libfontstweak-resources.so.0"
RPM_SONAME_REQ_fonts-tweak-tool = "libc.so.6 libpthread.so.0"
RDEPENDS_fonts-tweak-tool = "glibc gtk3 hicolor-icon-theme libeasyfc-gobject platform-python python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fonts-tweak-tool-0.4.5-3.el8.aarch64.rpm \
          "

SRC_URI[fonts-tweak-tool.sha256sum] = "c2c3f7a5810835b57cd44bac8e8ba0c9e52519d6fc4779afaa03514b03cdfea3"
