SUMMARY = "generated recipe based on perl-Data-Dumper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Data-Dumper = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Data-Dumper = "glibc perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Data-Dumper-2.167-399.el8.aarch64.rpm \
          "

SRC_URI[perl-Data-Dumper.sha256sum] = "24a7297b8f6db905749311728b2afee907149000b4502af8b86930f31a07bda0"
