SUMMARY = "generated recipe based on lohit-marathi-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-marathi-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-marathi-fonts-2.94.2-5.el8.noarch.rpm \
          "

SRC_URI[lohit-marathi-fonts.sha256sum] = "16e40b940473ad7c996177b57909f4fe667a57bc50e3c10c5b8dc5136d9b88de"
