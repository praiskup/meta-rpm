SUMMARY = "generated recipe based on perl-Tk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig libjpeg-turbo libpng libx11 libxft perl pkgconfig-native zlib"
RPM_SONAME_REQ_perl-Tk = "ld-linux-aarch64.so.1 libX11.so.6 libXft.so.2 libc.so.6 libfontconfig.so.1 libjpeg.so.62 libm.so.6 libperl.so.5.26 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_perl-Tk = "fontconfig glibc libX11 libXft libjpeg-turbo libpng perl-Carp perl-Encode perl-Exporter perl-Getopt-Long perl-IO perl-PathTools perl-Socket perl-Text-Tabs+Wrap perl-interpreter perl-libs perl-open zlib"
RPROVIDES_perl-Tk-devel = "perl-Tk-dev (= 804.034)"
RDEPENDS_perl-Tk-devel = "perl-Carp perl-Exporter perl-ExtUtils-MakeMaker perl-PathTools perl-Tk perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Tk-804.034-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Tk-devel-804.034-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Tk.sha256sum] = "e1ca19c74e7c2683cb441deba7f622db6e23b8378ba8cc2d04552072c8eab626"
SRC_URI[perl-Tk-devel.sha256sum] = "91ad48b904a69eda160833e91937762bfface63fb5389cdb9f83c2788f620ab2"
