SUMMARY = "generated recipe based on autoconf-archive srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_autoconf-archive = "autoconf bash info"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/autoconf-archive-2018.03.13-1.el8.noarch.rpm \
          "

SRC_URI[autoconf-archive.sha256sum] = "b18a037d3df87d1ec80249fe1ba7d2d2c62aba1dbbbc69f30ca7845706ed55b0"
