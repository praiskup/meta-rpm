SUMMARY = "generated recipe based on dotconf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_dotconf = "libdotconf.so.0"
RPM_SONAME_REQ_dotconf = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_dotconf = "glibc"
RPM_SONAME_REQ_dotconf-devel = "libdotconf.so.0"
RPROVIDES_dotconf-devel = "dotconf-dev (= 1.3)"
RDEPENDS_dotconf-devel = "dotconf pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dotconf-1.3-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dotconf-devel-1.3-18.el8.aarch64.rpm \
          "

SRC_URI[dotconf.sha256sum] = "31eded3ecc759b6bd80d3981b265d9f3b0f2964e1dae483f1a319191bad9ca0d"
SRC_URI[dotconf-devel.sha256sum] = "b91992bf63df1e9fc7aeae5902edd55513c24a49d88205108e25c9eff109ca1f"
