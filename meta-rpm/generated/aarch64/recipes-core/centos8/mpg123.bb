SUMMARY = "generated recipe based on mpg123 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native"
RPM_SONAME_REQ_mpg123 = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libdl.so.2 libm.so.6 libmpg123.so.0 libout123.so.0"
RDEPENDS_mpg123 = "alsa-lib glibc mpg123-libs"
RPM_SONAME_REQ_mpg123-devel = "libmpg123.so.0 libout123.so.0"
RPROVIDES_mpg123-devel = "mpg123-dev (= 1.25.10)"
RDEPENDS_mpg123-devel = "mpg123-libs pkgconf-pkg-config"
RPM_SONAME_PROV_mpg123-libs = "libmpg123.so.0 libout123.so.0"
RPM_SONAME_REQ_mpg123-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mpg123-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpg123-1.25.10-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpg123-libs-1.25.10-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mpg123-devel-1.25.10-2.el8.aarch64.rpm \
          "

SRC_URI[mpg123.sha256sum] = "d53f71c51c2057c3887d274469fddcf5b687415a54a9f81d02a6802f6e595b9c"
SRC_URI[mpg123-devel.sha256sum] = "6d06ff3901c01eb8d8e47ba94691aceb1afd4a4d14de72222658115a55121fd6"
SRC_URI[mpg123-libs.sha256sum] = "02f5743d36d110878e23f3c547785404ac5f1dc27b425e3f4d81a717a2c7a029"
