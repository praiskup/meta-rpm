SUMMARY = "generated recipe based on dropwatch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native readline zlib"
RPM_SONAME_REQ_dropwatch = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnl-3.so.200 libnl-genl-3.so.200 libreadline.so.7 libz.so.1"
RDEPENDS_dropwatch = "glibc libnl3 readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dropwatch-1.5-1.el8.aarch64.rpm \
          "

SRC_URI[dropwatch.sha256sum] = "1b81beba2537541d880fa80abc0aaf7cfc598bbffc87320b8328179ddcf3c626"
