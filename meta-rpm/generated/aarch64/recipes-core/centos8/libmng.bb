SUMMARY = "generated recipe based on libmng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lcms2 libjpeg-turbo pkgconfig-native zlib"
RPM_SONAME_PROV_libmng = "libmng.so.2"
RPM_SONAME_REQ_libmng = "ld-linux-aarch64.so.1 libc.so.6 libjpeg.so.62 liblcms2.so.2 libm.so.6 libz.so.1"
RDEPENDS_libmng = "glibc lcms2 libjpeg-turbo zlib"
RPM_SONAME_REQ_libmng-devel = "libmng.so.2"
RPROVIDES_libmng-devel = "libmng-dev (= 2.0.3)"
RDEPENDS_libmng-devel = "libjpeg-turbo-devel libmng pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmng-2.0.3-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmng-devel-2.0.3-7.el8.aarch64.rpm \
          "

SRC_URI[libmng.sha256sum] = "fef8865accfbae36789955970271c452d63a2c0f21efc888e45b27d67606bc01"
SRC_URI[libmng-devel.sha256sum] = "dee11b5a3cb7a02e987906e97f96dfcfbc8844cc04b96fe185f464d0855c6b70"
