SUMMARY = "generated recipe based on wayland-protocols srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_wayland-protocols-devel = "wayland-protocols (= 1.18) wayland-protocols-dev (= 1.18)"
RDEPENDS_wayland-protocols-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/wayland-protocols-devel-1.18-1.el8.noarch.rpm \
          "

SRC_URI[wayland-protocols-devel.sha256sum] = "d82e78cf3aec49f51b1fc9050162eeeeca163b303000610e1ed091e880566e8d"
