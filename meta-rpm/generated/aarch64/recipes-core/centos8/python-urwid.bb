SUMMARY = "generated recipe based on python-urwid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-urwid = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-urwid = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-urwid-1.3.1-4.el8.aarch64.rpm \
          "

SRC_URI[python3-urwid.sha256sum] = "5a88edcd0abfd401ef917164200524d3bf56982d5af8c914fe12f253593b98b3"
