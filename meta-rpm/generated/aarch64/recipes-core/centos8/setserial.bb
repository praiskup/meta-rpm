SUMMARY = "generated recipe based on setserial srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_setserial = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_setserial = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/setserial-2.17-45.el8.aarch64.rpm \
          "

SRC_URI[setserial.sha256sum] = "84ced85845b744ac9af92d119f86ac71cb4bb2c33c6c483057e15bdb4efed91e"
