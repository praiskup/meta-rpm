SUMMARY = "generated recipe based on hyphen-id srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-id = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-id-0.20040812-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-id.sha256sum] = "225b7d05482e6d33cffa0a6fd5c0fb0a8d010854cb1459b02b5172d2a3970414"
