SUMMARY = "generated recipe based on xorg-x11-drv-wacom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxi libxinerama libxrandr pkgconfig-native systemd-libs xorg-x11-server"
RPM_SONAME_REQ_xorg-x11-drv-wacom = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libc.so.6 libm.so.6"
RDEPENDS_xorg-x11-drv-wacom = "glibc libX11 libXext libXi libXinerama libXrandr xorg-x11-drv-wacom-serial-support xorg-x11-server-Xorg"
RPM_SONAME_REQ_xorg-x11-drv-wacom-devel = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RPROVIDES_xorg-x11-drv-wacom-devel = "xorg-x11-drv-wacom-dev (= 0.38.0)"
RDEPENDS_xorg-x11-drv-wacom-devel = "glibc pkgconf-pkg-config xorg-x11-server-devel"
RPM_SONAME_REQ_xorg-x11-drv-wacom-serial-support = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libudev.so.1"
RDEPENDS_xorg-x11-drv-wacom-serial-support = "glibc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-wacom-0.38.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-wacom-serial-support-0.38.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xorg-x11-drv-wacom-devel-0.38.0-1.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-wacom.sha256sum] = "f1f67e2c20229a9d8c0af0dbd95a2e868e0f3d0caad656a352006a19ea9a1712"
SRC_URI[xorg-x11-drv-wacom-devel.sha256sum] = "e1941fbe2c487a04d94f80fdfa1b83b2367c4b0dda555b3bf031f3955d9cdfa4"
SRC_URI[xorg-x11-drv-wacom-serial-support.sha256sum] = "2774a7d3e31b8483ff0b1e401ee5387e8a790e866ebfd565823981d6aba87ae7"
