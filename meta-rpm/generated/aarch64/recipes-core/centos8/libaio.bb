SUMMARY = "generated recipe based on libaio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libaio = "libaio.so.1 libaio.so.1.0.0"
RPM_SONAME_REQ_libaio = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libaio = "glibc"
RPM_SONAME_REQ_libaio-devel = "libaio.so.1"
RPROVIDES_libaio-devel = "libaio-dev (= 0.3.112)"
RDEPENDS_libaio-devel = "libaio"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libaio-0.3.112-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libaio-devel-0.3.112-1.el8.aarch64.rpm \
          "

SRC_URI[libaio.sha256sum] = "3bcb1ade26c217ead2da81c92b7ef78026c4a78383d28b6e825a7b840cae97fa"
SRC_URI[libaio-devel.sha256sum] = "cb232422c9002584bd233a5b55fa700bcc44e5a2aaa36f80395c6071aecb2f2a"
