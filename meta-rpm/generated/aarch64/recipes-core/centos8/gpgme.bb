SUMMARY = "generated recipe based on gpgme srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libassuan libgcc libgpg-error pkgconfig-native platform-python3"
RPM_SONAME_PROV_gpgme = "libgpgme.so.11"
RPM_SONAME_REQ_gpgme = "ld-linux-aarch64.so.1 libassuan.so.0 libc.so.6 libgpg-error.so.0"
RDEPENDS_gpgme = "glibc gnupg2 libassuan libgpg-error"
RPM_SONAME_REQ_gpgme-devel = "ld-linux-aarch64.so.1 libassuan.so.0 libc.so.6 libgpg-error.so.0 libgpgme.so.11"
RPROVIDES_gpgme-devel = "gpgme-dev (= 1.10.0)"
RDEPENDS_gpgme-devel = "bash glibc gpgme info libassuan libgpg-error libgpg-error-devel"
RPM_SONAME_PROV_gpgmepp = "libgpgmepp.so.6"
RPM_SONAME_REQ_gpgmepp = "ld-linux-aarch64.so.1 libassuan.so.0 libc.so.6 libgcc_s.so.1 libgpg-error.so.0 libgpgme.so.11 libm.so.6 libstdc++.so.6"
RDEPENDS_gpgmepp = "glibc gpgme libassuan libgcc libgpg-error libstdc++"
RPM_SONAME_REQ_gpgmepp-devel = "libgpgmepp.so.6"
RPROVIDES_gpgmepp-devel = "gpgmepp-dev (= 1.10.0)"
RDEPENDS_gpgmepp-devel = "cmake-filesystem gpgme-devel gpgmepp"
RPM_SONAME_REQ_python3-gpg = "ld-linux-aarch64.so.1 libc.so.6 libgpgme.so.11 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-gpg = "glibc gpgme platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gpgme-1.10.0-6.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gpgmepp-1.10.0-6.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-gpg-1.10.0-6.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gpgme-devel-1.10.0-6.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gpgmepp-devel-1.10.0-6.el8.0.1.aarch64.rpm \
          "

SRC_URI[gpgme.sha256sum] = "8113abba5e1552707c4bb808b9fbbd42e8837046e7a815fea170eaa032ade3e7"
SRC_URI[gpgme-devel.sha256sum] = "9489a53102c378996ff5236c165a7908621c03e9fb79c1bba98338ced798b97a"
SRC_URI[gpgmepp.sha256sum] = "4fe1e8b7165bb772448b57ec86ec8bf65559f9fadcf24bda7eaa500895dbde68"
SRC_URI[gpgmepp-devel.sha256sum] = "e7764440d5b54ed8458e5d3c408df4175a58cdfbe2f77a7afc83b707ca5c3470"
SRC_URI[python3-gpg.sha256sum] = "cd7c23cccf987fc8a7d3a3508508377e030eef22513de56062b2eaf49e855819"
