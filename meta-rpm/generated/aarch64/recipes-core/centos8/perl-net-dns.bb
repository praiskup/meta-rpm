SUMMARY = "generated recipe based on perl-Net-DNS srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Net-DNS = "perl-Carp perl-Data-Dumper perl-Digest-HMAC perl-Digest-MD5 perl-Digest-SHA perl-Encode perl-Exporter perl-IO perl-MIME-Base64 perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Net-DNS-1.15-1.el8.noarch.rpm \
          "

SRC_URI[perl-Net-DNS.sha256sum] = "2c0c55297ab15537c140e25b468c66dc87e8b62a75ffd87703e023d921f7ea81"
