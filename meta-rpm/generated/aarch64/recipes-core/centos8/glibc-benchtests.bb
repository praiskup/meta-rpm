SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_glibc-benchtests = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_glibc-benchtests = "bash glibc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glibc-benchtests-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-benchtests.sha256sum] = "4f60efca9ab82569546765f2b64f91445af808f8b0ce33691c06c22275ac5ae8"
