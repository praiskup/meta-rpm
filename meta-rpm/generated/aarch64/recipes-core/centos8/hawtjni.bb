SUMMARY = "generated recipe based on hawtjni srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hawtjni = "apache-commons-cli autoconf automake hawtjni-runtime java-1.8.0-openjdk-headless javapackages-filesystem libtool make objectweb-asm xbean"
RDEPENDS_hawtjni-javadoc = "javapackages-filesystem"
RDEPENDS_hawtjni-runtime = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_maven-hawtjni-plugin = "hawtjni java-1.8.0-openjdk-headless javapackages-filesystem maven-archiver maven-artifact-manager maven-lib maven-project plexus-archiver plexus-interpolation plexus-io plexus-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hawtjni-1.16-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hawtjni-javadoc-1.16-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hawtjni-runtime-1.16-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-hawtjni-plugin-1.16-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[hawtjni.sha256sum] = "c0e6301186d72f93efa1f2f0c6fc03513b4b3fa859c91fef1dfe3668814165de"
SRC_URI[hawtjni-javadoc.sha256sum] = "44d1baefefe22eafae60df9c75b82fec24e6c062c35ba5bd0f62c6eef7674339"
SRC_URI[hawtjni-runtime.sha256sum] = "21a065f0bb0588e581b8ce4d7dac33c80cd3334bdf7cc085fb4c80a3ec5f4bb6"
SRC_URI[maven-hawtjni-plugin.sha256sum] = "08867d016a4cb928e2e3b6e0849b4554b699af0c4571ad0647b05cf8990e2d25"
