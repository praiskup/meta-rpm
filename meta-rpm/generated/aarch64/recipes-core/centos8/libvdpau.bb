SUMMARY = "generated recipe based on libvdpau srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libvdpau = "libvdpau.so.1 libvdpau_trace.so.1"
RPM_SONAME_REQ_libvdpau = "ld-linux-aarch64.so.1 libXext.so.6 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libvdpau_trace.so.1"
RDEPENDS_libvdpau = "glibc libXext libgcc libstdc++"
RPM_SONAME_REQ_libvdpau-devel = "libvdpau.so.1"
RPROVIDES_libvdpau-devel = "libvdpau-dev (= 1.1.1)"
RDEPENDS_libvdpau-devel = "libX11-devel libvdpau pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvdpau-1.1.1-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvdpau-devel-1.1.1-7.el8.aarch64.rpm \
          "

SRC_URI[libvdpau.sha256sum] = "c2fa65545b97a37780b2594e815f37fc2ca7c77d02b76c261a35634d2c9a01d1"
SRC_URI[libvdpau-devel.sha256sum] = "6e25a578c31facd8f73d55427da2c7641d7f38d4d0432ce8605b6ca1aa33c33b"
