SUMMARY = "generated recipe based on perl-Class-Singleton srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Singleton = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Class-Singleton-1.5-9.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Singleton.sha256sum] = "0e997e8aa482676677e10ba08f7e96729f592210688c5d2f6387e83ba1130bdb"
