SUMMARY = "generated recipe based on nghttp2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libnghttp2 = "libnghttp2.so.14"
RPM_SONAME_REQ_libnghttp2 = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libnghttp2 = "glibc"
RPM_SONAME_REQ_libnghttp2-devel = "libnghttp2.so.14"
RPROVIDES_libnghttp2-devel = "libnghttp2-dev (= 1.33.0)"
RDEPENDS_libnghttp2-devel = "libnghttp2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnghttp2-1.33.0-3.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnghttp2-devel-1.33.0-3.el8_2.1.aarch64.rpm \
          "

SRC_URI[libnghttp2.sha256sum] = "23e9ff009c2316652c3bcd96a8b69b5bc26f2acd46214f652a7ce26a572cbabb"
SRC_URI[libnghttp2-devel.sha256sum] = "b9fe6b3816ec91f1fe62dd10747517ddf8552257726843d7788b6920a08a9ef2"
