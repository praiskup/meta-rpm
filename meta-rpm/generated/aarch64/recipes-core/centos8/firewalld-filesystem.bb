SUMMARY = "generated recipe based on firewalld srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/firewalld-filesystem-0.8.0-4.el8.noarch.rpm \
          "

SRC_URI[firewalld-filesystem.sha256sum] = "9e749e7e75dd8fdbbe45db6debc19553dad962b447ab86fd10ddd96517ad65d3"
