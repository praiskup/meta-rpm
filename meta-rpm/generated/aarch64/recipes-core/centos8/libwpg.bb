SUMMARY = "generated recipe based on libwpg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge libwpd pkgconfig-native"
RPM_SONAME_PROV_libwpg = "libwpg-0.3.so.3"
RPM_SONAME_REQ_libwpg = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libwpd-0.10.so.10"
RDEPENDS_libwpg = "glibc libgcc librevenge libstdc++ libwpd"
RPM_SONAME_REQ_libwpg-devel = "libwpg-0.3.so.3"
RPROVIDES_libwpg-devel = "libwpg-dev (= 0.3.2)"
RDEPENDS_libwpg-devel = "librevenge-devel libwpd-devel libwpg pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwpg-0.3.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwpg-devel-0.3.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwpg-doc-0.3.2-1.el8.noarch.rpm \
          "

SRC_URI[libwpg.sha256sum] = "dfa067f18c99a5eb64ceb3808239d557b3f2bc70ecf2cf00a1a3a7985f9bb174"
SRC_URI[libwpg-devel.sha256sum] = "eb347fd8ce1189fc323d5397b9074a075acd5c2ad93b3df01b900ec0a0df0d2b"
SRC_URI[libwpg-doc.sha256sum] = "c869a5cdf6239106e93083a331972d943968cfff9ab7691f4b3aa52e35163bb2"
