SUMMARY = "generated recipe based on hyphen-sk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-sk = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-sk-0.20031227-18.el8.noarch.rpm \
          "

SRC_URI[hyphen-sk.sha256sum] = "442281eaca617169c26a61cd837bd0e151e3e11397d68a21f228d50b883419d0"
