SUMMARY = "generated recipe based on iptstate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libnetfilter-conntrack ncurses pkgconfig-native"
RPM_SONAME_REQ_iptstate = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libncurses.so.6 libnetfilter_conntrack.so.3 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_iptstate = "glibc iptables libgcc libnetfilter_conntrack libstdc++ ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptstate-2.2.6-6.el8.aarch64.rpm \
          "

SRC_URI[iptstate.sha256sum] = "1fbc77b83cfe51aac82aa6f34673b97a377eb08c33e603f530935e540d8afd02"
