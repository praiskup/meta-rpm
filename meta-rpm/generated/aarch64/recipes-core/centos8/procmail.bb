SUMMARY = "generated recipe based on procmail srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_procmail = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_procmail = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/procmail-3.22-47.el8.aarch64.rpm \
          "

SRC_URI[procmail.sha256sum] = "8ca7a88cf2da08a7224aec8bc41ed672e7476a66e963f9d10dbaf98d9204a010"
