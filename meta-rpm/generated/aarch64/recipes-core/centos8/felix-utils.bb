SUMMARY = "generated recipe based on felix-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_felix-utils = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_felix-utils-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-utils-1.10.4-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-utils-javadoc-1.10.4-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[felix-utils.sha256sum] = "619b9ecba3cec4e7b0449d203efad3aafb92fedec485fb1c5b06c797457e7de0"
SRC_URI[felix-utils-javadoc.sha256sum] = "3f9927bfdeef26da00721cdbedc8ff4079d090e6c1b61bc884d3558f908453e4"
