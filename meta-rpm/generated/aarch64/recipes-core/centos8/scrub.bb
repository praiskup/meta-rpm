SUMMARY = "generated recipe based on scrub srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcrypt libgpg-error pkgconfig-native"
RPM_SONAME_REQ_scrub = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libpthread.so.0"
RDEPENDS_scrub = "glibc libgcrypt libgpg-error"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/scrub-2.5.2-14.el8.aarch64.rpm \
          "

SRC_URI[scrub.sha256sum] = "8ff9c90201cd271bee67cf3e87190fe2bdfa856aa37f16b3428c6744ee7f8051"
