SUMMARY = "generated recipe based on coreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr gmp libcap libselinux openssl pkgconfig-native"
RPM_SONAME_PROV_coreutils = "libstdbuf.so"
RPM_SONAME_REQ_coreutils = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libgmp.so.10 libpthread.so.0 librt.so.1 libselinux.so.1"
RDEPENDS_coreutils = "coreutils-common glibc gmp libacl libattr libcap libselinux ncurses openssl-libs"
RDEPENDS_coreutils-common = "bash info"
RPM_SONAME_PROV_coreutils-single = "libstdbuf.so.single"
RPM_SONAME_REQ_coreutils-single = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libc.so.6 libcap.so.2 libpthread.so.0 librt.so.1 libselinux.so.1"
RDEPENDS_coreutils-single = "glibc libacl libattr libcap libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/coreutils-8.30-7.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/coreutils-common-8.30-7.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/coreutils-single-8.30-7.el8_2.1.aarch64.rpm \
          "

SRC_URI[coreutils.sha256sum] = "be1c5b6282a894c7e0bc6783c1c66c9179b2e0be6125e9b7948970a5bb786f21"
SRC_URI[coreutils-common.sha256sum] = "e5e04326d2277178ee7fa7cd6c54452f827cb1f96cd0040e5c4f9c710a4b9399"
SRC_URI[coreutils-single.sha256sum] = "c6645104131d64c598037acb5489c4725e30d0fc62965662ee20fcf810f165c5"
