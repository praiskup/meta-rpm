SUMMARY = "generated recipe based on irqbalance srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libcap-ng ncurses numactl pkgconfig-native"
RPM_SONAME_REQ_irqbalance = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libglib-2.0.so.0 libm.so.6 libncursesw.so.6 libnuma.so.1 libtinfo.so.6"
RDEPENDS_irqbalance = "bash glib2 glibc libcap-ng ncurses-libs numactl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/irqbalance-1.4.0-4.el8.aarch64.rpm \
          "

SRC_URI[irqbalance.sha256sum] = "f232d059308997b356dc4efdc249c4171bc0ead65d1baf8319ba3f2141d5c3c6"
