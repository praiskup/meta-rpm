SUMMARY = "generated recipe based on crash srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lzo ncurses pkgconfig-native snappy zlib"
RPM_SONAME_REQ_crash = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblzo2.so.2 libm.so.6 libncurses.so.6 libsnappy.so.1 libtinfo.so.6 libz.so.1"
RDEPENDS_crash = "binutils glibc lzo ncurses-libs snappy zlib"
RPROVIDES_crash-devel = "crash-dev (= 7.2.7)"
RDEPENDS_crash-devel = "crash lzo-devel snappy-devel zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/crash-7.2.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/crash-devel-7.2.7-3.el8.aarch64.rpm \
          "

SRC_URI[crash.sha256sum] = "49b43085a0bd63dfe83312bed216bebdc5cdd931437368eb4c141d59a16dc6ca"
SRC_URI[crash-devel.sha256sum] = "7d46b97debd58032227df612b226ac160dde56d763e465960254aca646887d34"
