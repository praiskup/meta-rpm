SUMMARY = "generated recipe based on perl-Mozilla-CA srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Mozilla-CA = "ca-certificates perl-PathTools perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Mozilla-CA-20160104-7.el8.noarch.rpm \
          "

SRC_URI[perl-Mozilla-CA.sha256sum] = "6e56db648a944682053e34f6da8fb27305422458de0dd666855fcdc8c896bb7b"
