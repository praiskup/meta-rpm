SUMMARY = "generated recipe based on apache-commons-cli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-cli = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-cli-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-cli-1.4-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-cli-javadoc-1.4-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-cli.sha256sum] = "6988c98ab669fb10fbed75e8bd3cee32ad3c1ebfe89afc0a472c9692cb845606"
SRC_URI[apache-commons-cli-javadoc.sha256sum] = "9174f4bdb43692298cbc7b5a19c99d146d567157b4c44ec977870cf078546e83"
