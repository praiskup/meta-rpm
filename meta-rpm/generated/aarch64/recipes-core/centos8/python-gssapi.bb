SUMMARY = "generated recipe based on python-gssapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-gssapi = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-gssapi = "glibc krb5-libs libcom_err platform-python python3-decorator python3-libs python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-gssapi-1.5.1-5.el8.aarch64.rpm \
          "

SRC_URI[python3-gssapi.sha256sum] = "33106bc3ab980caca740aff339f3401ac25a9aa1fb614d9bda872bf424aeee2a"
