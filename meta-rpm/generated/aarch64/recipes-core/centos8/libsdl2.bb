SUMMARY = "generated recipe based on SDL2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libx11 mesa pkgconfig-native"
RPM_SONAME_PROV_SDL2 = "libSDL2-2.0.so.0"
RPM_SONAME_REQ_SDL2 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_SDL2 = "glibc"
RPM_SONAME_REQ_SDL2-devel = "libSDL2-2.0.so.0"
RPROVIDES_SDL2-devel = "SDL2-dev (= 2.0.10)"
RDEPENDS_SDL2-devel = "SDL2 bash libX11-devel libglvnd-devel mesa-libEGL-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/SDL2-2.0.10-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/SDL2-devel-2.0.10-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/SDL2-static-2.0.10-2.el8.aarch64.rpm \
          "

SRC_URI[SDL2.sha256sum] = "ce954e256868442bd4fc9792b8caf7708aede429ea32b02b89c087f2181f08cc"
SRC_URI[SDL2-devel.sha256sum] = "93ad6eac052366a22e40fe8e8e78622572a7d65e7dba590346f3a6d79a4d1126"
SRC_URI[SDL2-static.sha256sum] = "c7e4ffbd6eb4e969d9e6b4c2d91c1697165c9bcecd49db4456349d29cc731a48"
