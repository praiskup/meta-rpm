SUMMARY = "generated recipe based on dhcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bind e2fsprogs krb5-libs libcap libcap-ng openldap openssl pkgconfig-native systemd-libs"
RPM_SONAME_REQ_dhcp-client = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdns-export.so.1107 libgssapi_krb5.so.2 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163 libk5crypto.so.3 libkrb5.so.3 libomapi.so.0 libsystemd.so.0"
RDEPENDS_dhcp-client = "bash bind-export-libs coreutils dhcp-common dhcp-libs gawk glibc grep ipcalc iproute iputils krb5-libs libcap libcap-ng libcom_err openssl-libs sed systemd systemd-libs"
RPM_SONAME_PROV_dhcp-libs = "libdhcpctl.so.0 libomapi.so.0"
RPM_SONAME_REQ_dhcp-libs = "ld-linux-aarch64.so.1 libc.so.6 libsystemd.so.0"
RDEPENDS_dhcp-libs = "glibc systemd-libs"
RPM_SONAME_REQ_dhcp-relay = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdns-export.so.1107 libgssapi_krb5.so.2 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163 libk5crypto.so.3 libkrb5.so.3 libomapi.so.0 libsystemd.so.0"
RDEPENDS_dhcp-relay = "bash bind-export-libs dhcp-common dhcp-libs glibc grep krb5-libs libcap libcap-ng libcom_err openssl-libs sed systemd systemd-libs"
RPM_SONAME_REQ_dhcp-server = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdhcpctl.so.0 libdns-export.so.1107 libgssapi_krb5.so.2 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libomapi.so.0 libsystemd.so.0"
RDEPENDS_dhcp-server = "bash bind-export-libs coreutils dhcp-common dhcp-libs glibc grep krb5-libs libcap libcom_err openldap openssl-libs sed shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dhcp-client-4.3.6-40.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dhcp-common-4.3.6-40.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dhcp-libs-4.3.6-40.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dhcp-relay-4.3.6-40.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dhcp-server-4.3.6-40.el8.aarch64.rpm \
          "

SRC_URI[dhcp-client.sha256sum] = "c6f5df4ee10d9160bdb85e7b7e94ffdb1d7f671c90ef0c365be2508cdb2ffb96"
SRC_URI[dhcp-common.sha256sum] = "9ab0e4ecf16648b4271db6486c5a3d8f7ece0e552317b5bac2bc008876acffcb"
SRC_URI[dhcp-libs.sha256sum] = "32492ad760329c6e876064bb02874f8657f1779352f041a177704ba67ab75e42"
SRC_URI[dhcp-relay.sha256sum] = "45129baae5a27f7d697323538c570746650446175aee87dae181c6d46190947a"
SRC_URI[dhcp-server.sha256sum] = "93a792b3557b9b7efb2d31e1e47e7e2a99013a1b814fb29dbc95f73d4b38fc09"
