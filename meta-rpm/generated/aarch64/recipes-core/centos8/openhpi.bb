SUMMARY = "generated recipe based on openhpi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 json-c libgcc libgcrypt librabbitmq libuuid libxml2 net-snmp openssl pkgconfig-native sysfsutils"
RPM_SONAME_PROV_openhpi = "libdyn_simulator.so.3 libilo2_ribcl.so.3 libipmidirect.so.3 liboa_soap.so.3 libov_rest.so.3 libsimulator.so.3 libslave.so.3 libsnmp_bc.so.3 libsysfs2hpi.so.3 libtest_agent.so.3 libwatchdog.so.3"
RPM_SONAME_REQ_openhpi = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdyn_simulator.so.3 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgthread-2.0.so.0 libilo2_ribcl.so.3 libipmidirect.so.3 libjson-c.so.4 libm.so.6 libnetsnmp.so.35 liboa_soap.so.3 libopenhpi.so.3 libopenhpi_snmp.so.3 libopenhpi_ssl.so.3 libopenhpimarshal.so.3 libopenhpitransport.so.3 libopenhpiutils.so.3 libov_rest.so.3 libpthread.so.0 librabbitmq.so.4 libsimulator.so.3 libslave.so.3 libsnmp_bc.so.3 libssl.so.1.1 libstdc++.so.6 libsysfs.so.2 libsysfs2hpi.so.3 libtest_agent.so.3 libuuid.so.1 libwatchdog.so.3 libxml2.so.2"
RDEPENDS_openhpi = "bash glib2 glibc json-c libcurl libgcc libgcrypt librabbitmq libstdc++ libsysfs libuuid libxml2 net-snmp-libs openhpi-libs openssl-libs systemd"
RPM_SONAME_PROV_openhpi-libs = "libopenhpi.so.3 libopenhpi_snmp.so.3 libopenhpi_ssl.so.3 libopenhpimarshal.so.3 libopenhpitransport.so.3 libopenhpiutils.so.3"
RPM_SONAME_REQ_openhpi-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libnetsnmp.so.35 libopenhpimarshal.so.3 libopenhpitransport.so.3 libopenhpiutils.so.3 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libuuid.so.1"
RDEPENDS_openhpi-libs = "glib2 glibc libgcc libgcrypt libstdc++ libuuid net-snmp-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openhpi-3.8.0-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openhpi-libs-3.8.0-9.el8.aarch64.rpm \
          "

SRC_URI[openhpi.sha256sum] = "832589e5422aad52cb7367587385c34acbf4e10e4e58aff250205a34dcc7a651"
SRC_URI[openhpi-libs.sha256sum] = "22c1cc3cdb05c3a7585f5c5bb1b973004f1c462a3004c70bb4768eca298ecb2c"
