SUMMARY = "generated recipe based on dvd+rw-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_dvd+rw-tools = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dvd+rw-tools = "glibc libgcc libstdc++ xorriso"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dvd+rw-tools-7.1-27.el8.aarch64.rpm \
          "

SRC_URI[dvd+rw-tools.sha256sum] = "1dd9833b3336e5011f955209daa676b4cf46b5b43a5baec65d9bafd57597be9c"
