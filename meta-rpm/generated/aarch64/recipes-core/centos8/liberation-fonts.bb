SUMMARY = "generated recipe based on liberation-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_liberation-fonts = "liberation-mono-fonts liberation-sans-fonts liberation-serif-fonts"
RDEPENDS_liberation-fonts-common = "fontpackages-filesystem"
RDEPENDS_liberation-mono-fonts = "liberation-fonts-common"
RDEPENDS_liberation-sans-fonts = "liberation-fonts-common"
RDEPENDS_liberation-serif-fonts = "liberation-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/liberation-fonts-2.00.3-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/liberation-fonts-common-2.00.3-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/liberation-mono-fonts-2.00.3-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/liberation-sans-fonts-2.00.3-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/liberation-serif-fonts-2.00.3-7.el8.noarch.rpm \
          "

SRC_URI[liberation-fonts.sha256sum] = "d5ab64f5cd84e5301493da0f138aa7d4f3e8cfaa30dc95a4f23618059aa7d915"
SRC_URI[liberation-fonts-common.sha256sum] = "7e48c1ce8c9a48c0b7ca7795aafe83fe9871bd1ae88dfd31435bf318baac8f64"
SRC_URI[liberation-mono-fonts.sha256sum] = "fdbff1e8b002da671142b914c8f3750e51933f3de2ef2eeba2ef457e60d5e289"
SRC_URI[liberation-sans-fonts.sha256sum] = "4be854b40ec2de83247c86d501d314fe85ce64bdcbe48e93673f890d8e6af449"
SRC_URI[liberation-serif-fonts.sha256sum] = "a03cdb1554814e391e992d7b7d08cfe9ec9b7d2c858212e90f9cb4a368adf22e"
