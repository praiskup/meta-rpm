SUMMARY = "generated recipe based on strace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_strace = "ld-linux-aarch64.so.1 libc.so.6 libdw.so.1 librt.so.1"
RDEPENDS_strace = "bash elfutils-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/strace-4.24-9.el8.aarch64.rpm \
          "

SRC_URI[strace.sha256sum] = "02589576db12e6415b9f76099e91f5d47a97d3fc22feaeb5a0355ec1a8808440"
