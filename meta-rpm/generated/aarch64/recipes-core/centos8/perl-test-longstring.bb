SUMMARY = "generated recipe based on perl-Test-LongString srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-LongString = "perl-Exporter perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-LongString-0.17-10.el8.noarch.rpm \
          "

SRC_URI[perl-Test-LongString.sha256sum] = "a78f7f542690e94bdd287e9b51f7d2325b573dbe6164c8435efc383ba97ec422"
