SUMMARY = "generated recipe based on lensfun srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native"
RPM_SONAME_PROV_lensfun = "liblensfun.so.1"
RPM_SONAME_REQ_lensfun = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libm.so.6 libstdc++.so.6"
RDEPENDS_lensfun = "glib2 glibc libgcc libstdc++"
RPM_SONAME_REQ_lensfun-devel = "liblensfun.so.1"
RPROVIDES_lensfun-devel = "lensfun-dev (= 0.3.2)"
RDEPENDS_lensfun-devel = "glib2-devel lensfun pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lensfun-0.3.2-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lensfun-devel-0.3.2-14.el8.aarch64.rpm \
          "

SRC_URI[lensfun.sha256sum] = "58e40fb6afa2781067a92971f0724a7a02ea167c4286fa4849a61edf38b0ab55"
SRC_URI[lensfun-devel.sha256sum] = "05c93ec3434fd73fd33dedac66da7413b0278b2d2a2adc38137e3136bdcf94dd"
