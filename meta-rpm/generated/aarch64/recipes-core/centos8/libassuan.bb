SUMMARY = "generated recipe based on libassuan srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libassuan = "libassuan.so.0"
RPM_SONAME_REQ_libassuan = "ld-linux-aarch64.so.1 libc.so.6 libgpg-error.so.0"
RDEPENDS_libassuan = "glibc libgpg-error"
RPM_SONAME_REQ_libassuan-devel = "libassuan.so.0"
RPROVIDES_libassuan-devel = "libassuan-dev (= 2.5.1)"
RDEPENDS_libassuan-devel = "bash info libassuan"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libassuan-2.5.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libassuan-devel-2.5.1-3.el8.aarch64.rpm \
          "

SRC_URI[libassuan.sha256sum] = "cf4d477f18ecd97470d1bc50c0e442de6f7d5db74829221d0f9b1ddfc9a71dab"
SRC_URI[libassuan-devel.sha256sum] = "6ff43e7b4049a03b25aaf21d6f38bc61ba848b3beab319166262b3b5926d3f50"
