SUMMARY = "generated recipe based on libseccomp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libseccomp = "libseccomp.so.2"
RPM_SONAME_REQ_libseccomp = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libseccomp = "glibc"
RPM_SONAME_REQ_libseccomp-devel = "ld-linux-aarch64.so.1 libc.so.6 libseccomp.so.2"
RPROVIDES_libseccomp-devel = "libseccomp-dev (= 2.4.1)"
RDEPENDS_libseccomp-devel = "glibc libseccomp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libseccomp-devel-2.4.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libseccomp-2.4.1-1.el8.aarch64.rpm \
          "

SRC_URI[libseccomp.sha256sum] = "9e370acca52660ec634507b4ab9cdd63931141ba79f850febb063adb902e71a8"
SRC_URI[libseccomp-devel.sha256sum] = "b85ec8e14f21ee7f4ff64c1a3adaaf2ee308fea379bfcbd3189d490ba4d8d7dc"
