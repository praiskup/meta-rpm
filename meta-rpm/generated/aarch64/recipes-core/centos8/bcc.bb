SUMMARY = "generated recipe based on bcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "clang elfutils libgcc ncurses pkgconfig-native zlib"
RPM_SONAME_PROV_bcc = "libbcc.so.0 libbcc_bpf.so.0"
RPM_SONAME_REQ_bcc = "ld-linux-aarch64.so.1 libc.so.6 libclangAST.so.9 libclangASTMatchers.so.9 libclangAnalysis.so.9 libclangBasic.so.9 libclangCodeGen.so.9 libclangDriver.so.9 libclangEdit.so.9 libclangFrontend.so.9 libclangLex.so.9 libclangParse.so.9 libclangRewrite.so.9 libclangSema.so.9 libclangSerialization.so.9 libdl.so.2 libelf.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_bcc = "bcc-tools clang-libs elfutils-libelf glibc libgcc libstdc++ ncurses-libs zlib"
RPM_SONAME_REQ_bcc-devel = "libbcc.so.0 libbcc_bpf.so.0"
RPROVIDES_bcc-devel = "bcc-dev (= 0.11.0)"
RDEPENDS_bcc-devel = "bcc pkgconf-pkg-config"
RDEPENDS_bcc-doc = "bash platform-python"
RPM_SONAME_REQ_bcc-tools = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_bcc-tools = "bash glibc kernel-devel platform-python python3-bcc python3-netaddr"
RDEPENDS_python3-bcc = "bcc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bcc-0.11.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bcc-tools-0.11.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-bcc-0.11.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bcc-devel-0.11.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bcc-doc-0.11.0-2.el8.noarch.rpm \
          "

SRC_URI[bcc.sha256sum] = "793840f129658df81f85b70bfca948985e61045fb9844f65f954c49d0b8de25b"
SRC_URI[bcc-devel.sha256sum] = "9aaf963d387fdeff693daedbb3fc8d4cf8c955ba01f9e5a522c7a0d87c0c19b0"
SRC_URI[bcc-doc.sha256sum] = "e350590b3619c65ff6197d6db83f7e760101756150b080e4ec44673db136a314"
SRC_URI[bcc-tools.sha256sum] = "12143d178e0dfc5718f3ff8a5fd035cf4d330383f206de0194558cae97f4e7f2"
SRC_URI[python3-bcc.sha256sum] = "aeadb9e3fa9139e9e7d1bc9868fa56f1e05ebef6df9bd42dbc671572521ff2b8"
