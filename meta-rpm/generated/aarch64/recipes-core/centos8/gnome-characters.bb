SUMMARY = "generated recipe based on gnome-characters srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gtk+3 libunistring pango pkgconfig-native"
RPM_SONAME_PROV_gnome-characters = "libgc.so"
RPM_SONAME_REQ_gnome-characters = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libunistring.so.2"
RDEPENDS_gnome-characters = "gjs glib2 glibc gtk3 libunistring pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-characters-3.28.2-1.el8.aarch64.rpm \
          "

SRC_URI[gnome-characters.sha256sum] = "2764f49bc7c94b00bad0d741e2f64f50e1eb58c8198204618c79f071f0c88119"
