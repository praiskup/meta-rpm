SUMMARY = "generated recipe based on libpaper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpaper = "libpaper.so.1"
RPM_SONAME_REQ_libpaper = "ld-linux-aarch64.so.1 libc.so.6 libpaper.so.1"
RDEPENDS_libpaper = "bash glibc"
RPM_SONAME_REQ_libpaper-devel = "libpaper.so.1"
RPROVIDES_libpaper-devel = "libpaper-dev (= 1.1.24)"
RDEPENDS_libpaper-devel = "libpaper"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpaper-1.1.24-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpaper-devel-1.1.24-22.el8.aarch64.rpm \
          "

SRC_URI[libpaper.sha256sum] = "5e93518451541cd7f7ad1e4b3b61ef7d6523e9dcf3fc15f18f4c35c5939fe1f8"
SRC_URI[libpaper-devel.sha256sum] = "6659191b416d28ebfccc924a284627f0ddc3353485f357525346eba0fdc27a4c"
