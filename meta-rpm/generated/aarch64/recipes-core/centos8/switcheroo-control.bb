SUMMARY = "generated recipe based on switcheroo-control srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_switcheroo-control = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_switcheroo-control = "bash glib2 glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/switcheroo-control-1.1-5.el8.aarch64.rpm \
          "

SRC_URI[switcheroo-control.sha256sum] = "39f59d5fdeb8c8df5064fe5be3ea44da70b360e9e34450d98fe19a4101166c5a"
