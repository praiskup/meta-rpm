SUMMARY = "generated recipe based on libdaemon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdaemon = "libdaemon.so.0"
RPM_SONAME_REQ_libdaemon = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libdaemon = "glibc"
RPM_SONAME_REQ_libdaemon-devel = "libdaemon.so.0"
RPROVIDES_libdaemon-devel = "libdaemon-dev (= 0.14)"
RDEPENDS_libdaemon-devel = "libdaemon pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libdaemon-0.14-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdaemon-devel-0.14-15.el8.aarch64.rpm \
          "

SRC_URI[libdaemon.sha256sum] = "752995ca0b46a767a114cd55cc620a3188d68d35747b3e0dc995fb0b8d9dc241"
SRC_URI[libdaemon-devel.sha256sum] = "a9b3cdd5a99a05469a8240dedf0fc0d5d21e250e248c769676da48d54ed0e622"
