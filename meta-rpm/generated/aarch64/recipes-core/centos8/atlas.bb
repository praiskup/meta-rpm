SUMMARY = "generated recipe based on atlas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc pkgconfig-native"
RPM_SONAME_PROV_atlas = "libsatlas.so.3 libtatlas.so.3"
RPM_SONAME_REQ_atlas = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_atlas = "glibc libgfortran"
RPM_SONAME_REQ_atlas-devel = "libsatlas.so.3 libtatlas.so.3"
RPROVIDES_atlas-devel = "atlas-dev (= 3.10.3)"
RDEPENDS_atlas-devel = "atlas bash chkconfig pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/atlas-3.10.3-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/atlas-devel-3.10.3-7.el8.aarch64.rpm \
          "

SRC_URI[atlas.sha256sum] = "bc58ae1fc4849cdbd974ff6af32a82a02d18a4e5fc0a6809263b771142dc1290"
SRC_URI[atlas-devel.sha256sum] = "db1ea8350f197db392884e99f823a960f7af60e23f19ee06e049956bb7216b3c"
