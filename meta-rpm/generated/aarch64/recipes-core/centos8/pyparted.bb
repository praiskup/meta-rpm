SUMMARY = "generated recipe based on pyparted srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "parted pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pyparted = "ld-linux-aarch64.so.1 libc.so.6 libparted.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pyparted = "glibc parted platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyparted-3.11.0-13.el8.aarch64.rpm \
          "

SRC_URI[python3-pyparted.sha256sum] = "6881462f38a97e3f93a297906618866d261a6ae6d2b1ecac270c3274e5376d72"
