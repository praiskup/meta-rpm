SUMMARY = "generated recipe based on mpfr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp pkgconfig-native"
RPM_SONAME_PROV_mpfr = "libmpfr.so.4"
RPM_SONAME_REQ_mpfr = "ld-linux-aarch64.so.1 libc.so.6 libgmp.so.10"
RDEPENDS_mpfr = "glibc gmp"
RPM_SONAME_REQ_mpfr-devel = "libmpfr.so.4"
RPROVIDES_mpfr-devel = "mpfr-dev (= 3.1.6)"
RDEPENDS_mpfr-devel = "bash gmp-devel info mpfr"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpfr-devel-3.1.6-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mpfr-3.1.6-1.el8.aarch64.rpm \
          "

SRC_URI[mpfr.sha256sum] = "97a998a1b93c21bf070f9a9a1dbb525234b00fccedfe67de8967cd9ec7132eb1"
SRC_URI[mpfr-devel.sha256sum] = "b76a3128f8c99247c8bb8d968589642468ff79e39cd122ca29ce89af6f467f79"
