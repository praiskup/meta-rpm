SUMMARY = "generated recipe based on libwmf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype gdk-pixbuf glib-2.0 gtk2 libjpeg-turbo libpng libx11 libxml2 pkgconfig-native xz zlib"
RPM_SONAME_PROV_libwmf = "libwmf-0.2.so.7"
RPM_SONAME_REQ_libwmf = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libdl.so.2 libfreetype.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 liblzma.so.5 libm.so.6 libpng16.so.16 libpthread.so.0 libwmf-0.2.so.7 libwmflite-0.2.so.7 libxml2.so.2 libz.so.1"
RDEPENDS_libwmf = "bash freetype gdk-pixbuf2 glib2 glibc libX11 libjpeg-turbo libpng libwmf-lite libxml2 urw-base35-fonts xz-libs zlib"
RPM_SONAME_REQ_libwmf-devel = "libwmf-0.2.so.7 libwmflite-0.2.so.7"
RPROVIDES_libwmf-devel = "libwmf-dev (= 0.2.9)"
RDEPENDS_libwmf-devel = "bash gtk2-devel libjpeg-turbo-devel libwmf libwmf-lite libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libwmf-lite = "libwmflite-0.2.so.7"
RPM_SONAME_REQ_libwmf-lite = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_libwmf-lite = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwmf-0.2.9-8.el8_0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwmf-lite-0.2.9-8.el8_0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwmf-devel-0.2.9-8.el8_0.aarch64.rpm \
          "

SRC_URI[libwmf.sha256sum] = "689161b5c043205ad2c414158d7ca150335e3fa32f8e6ef02518b0b5dcb9a8db"
SRC_URI[libwmf-devel.sha256sum] = "77f22949bb39615db54382a2a066fea063191114a065cc5eee2d5087635e72a5"
SRC_URI[libwmf-lite.sha256sum] = "af6731e2b1cc59189654f56d2b9f48c334362f469ca3da16b4830b26a911c0bb"
