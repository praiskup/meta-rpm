SUMMARY = "generated recipe based on openblas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc libgcc pkgconfig-native"
RPM_SONAME_PROV_openblas = "libopenblas.so.0"
RPM_SONAME_REQ_openblas = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas = "glibc libgfortran"
RPM_SONAME_PROV_openblas-Rblas = "libRblas.so"
RPM_SONAME_REQ_openblas-Rblas = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas-Rblas = "glibc libgfortran"
RPM_SONAME_REQ_openblas-devel = "libopenblaso.so.0 libopenblaso64.so.0 libopenblaso64_.so.0"
RPROVIDES_openblas-devel = "openblas-dev (= 0.3.3)"
RDEPENDS_openblas-devel = "openblas openblas-openmp openblas-openmp64 openblas-openmp64_ openblas-serial64 openblas-serial64_ openblas-srpm-macros openblas-threads openblas-threads64 openblas-threads64_"
RPM_SONAME_PROV_openblas-openmp = "libopenblaso.so.0"
RPM_SONAME_REQ_openblas-openmp = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-openmp = "glibc libgcc libgfortran libgomp"
RPM_SONAME_PROV_openblas-openmp64 = "libopenblaso64.so.0"
RPM_SONAME_REQ_openblas-openmp64 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-openmp64 = "glibc libgcc libgfortran libgomp"
RPM_SONAME_PROV_openblas-openmp64_ = "libopenblaso64_.so.0"
RPM_SONAME_REQ_openblas-openmp64_ = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-openmp64_ = "glibc libgcc libgfortran libgomp"
RPM_SONAME_PROV_openblas-serial64 = "libopenblas64.so.0"
RPM_SONAME_REQ_openblas-serial64 = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas-serial64 = "glibc libgfortran"
RPM_SONAME_PROV_openblas-serial64_ = "libopenblas64_.so.0"
RPM_SONAME_REQ_openblas-serial64_ = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas-serial64_ = "glibc libgfortran"
RDEPENDS_openblas-static = "openblas-devel"
RPM_SONAME_PROV_openblas-threads = "libopenblasp.so.0"
RPM_SONAME_REQ_openblas-threads = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-threads = "glibc libgfortran"
RPM_SONAME_PROV_openblas-threads64 = "libopenblasp64.so.0"
RPM_SONAME_REQ_openblas-threads64 = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-threads64 = "glibc libgfortran"
RPM_SONAME_PROV_openblas-threads64_ = "libopenblasp64_.so.0"
RPM_SONAME_REQ_openblas-threads64_ = "ld-linux-aarch64.so.1 libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-threads64_ = "glibc libgfortran"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openblas-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openblas-threads-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-Rblas-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-devel-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-openmp-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-openmp64-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-openmp64_-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-serial64-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-serial64_-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-static-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-threads64-0.3.3-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openblas-threads64_-0.3.3-5.el8.aarch64.rpm \
          "

SRC_URI[openblas.sha256sum] = "5c67cad11112bee0eacfd35973f41e2114372db16f858b3d4a61229311503f0f"
SRC_URI[openblas-Rblas.sha256sum] = "a177d27ef8a567f0a0e85812b75b00e2fab4c37ee07e036e0d9f04fc60e24e29"
SRC_URI[openblas-devel.sha256sum] = "b3318c451c2771005a1dcc62b5f778051bcc3a814ff828cca7be69d1c9e8c782"
SRC_URI[openblas-openmp.sha256sum] = "ab50f8be91b18b57bc9ed7cd24b39fd7161c93df54300195ec679cb7abec4ad0"
SRC_URI[openblas-openmp64.sha256sum] = "e6d0b6ace45e6fe8b0165f8881e3e20cbc47efdf013a4bd86762de79f5c1e2c3"
SRC_URI[openblas-openmp64_.sha256sum] = "9d0b7941fba9202ce91a221f4ea842d8f640de45411c74fb7296f4b6807ea033"
SRC_URI[openblas-serial64.sha256sum] = "4d40f2ac605d13443b239590e8e3e667d48c284c9bf01c6382314723f0b2483d"
SRC_URI[openblas-serial64_.sha256sum] = "11e59d4c8c5b1509c802058dc39f15986338dffc8b8284b2491469d0e19d28ec"
SRC_URI[openblas-static.sha256sum] = "5e60bad41abf22ae43fa728b400eac92e048672577deae9bf8bcac1a864acb5b"
SRC_URI[openblas-threads.sha256sum] = "99089f4bd0ec4a3dd4ce1a5b425f89f35215b954e39a0b1f86ad0bca50374f08"
SRC_URI[openblas-threads64.sha256sum] = "31dd73ae45096209df9f317e186e72474dd3fa4cf259170f02e7161c557115be"
SRC_URI[openblas-threads64_.sha256sum] = "246af715b793555894eeba6412fce715edbdc53feae77dd1b47be464439bb478"
