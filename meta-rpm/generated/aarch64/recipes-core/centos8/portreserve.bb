SUMMARY = "generated recipe based on portreserve srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_portreserve = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_portreserve = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/portreserve-0.0.5-19.el8.aarch64.rpm \
          "

SRC_URI[portreserve.sha256sum] = "77978fb2a171ea2bccc1a69c78115e9e4382233c003f702263f5d7e72a5e4506"
