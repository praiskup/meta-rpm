SUMMARY = "generated recipe based on gcr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcrypt libgpg-error p11-kit pango pkgconfig-native"
RPM_SONAME_PROV_gcr = "libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1"
RPM_SONAME_REQ_gcr = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gcr = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcrypt libgpg-error p11-kit pango"
RPM_SONAME_REQ_gcr-devel = "libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1"
RPROVIDES_gcr-devel = "gcr-dev (= 3.28.0)"
RDEPENDS_gcr-devel = "gcr glib2-devel gtk3-devel p11-kit-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcr-3.28.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcr-devel-3.28.0-1.el8.aarch64.rpm \
          "

SRC_URI[gcr.sha256sum] = "d5cd670af74d73ce95bccd4a8dc19910c73f26fe527fb6478bb4078ea9326a10"
SRC_URI[gcr-devel.sha256sum] = "fd4136c8119ce497c8e553e2e546691220b85066fd9ed31b9c11a1c0b3a60de3"
