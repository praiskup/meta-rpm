SUMMARY = "generated recipe based on hunspell-gv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-gv = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-gv-0.20040505-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-gv.sha256sum] = "23ce336db42aacc6ecd7f301da5f42e8dcb143784b926c01f9c7963c2247b5b1"
