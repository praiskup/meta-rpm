SUMMARY = "generated recipe based on javacc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_javacc = "bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"
RDEPENDS_javacc-demo = "javacc"
RDEPENDS_javacc-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javacc-7.0.2-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javacc-demo-7.0.2-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javacc-javadoc-7.0.2-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javacc-manual-7.0.2-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[javacc.sha256sum] = "f568c585e084ce3ce65798a0d9ab247ed383cb28201310da0611f3832ec57e66"
SRC_URI[javacc-demo.sha256sum] = "65353d84ad2d3f226b26e4e66071dcd851efdfbf5fb937488d4e6b9388f06c8f"
SRC_URI[javacc-javadoc.sha256sum] = "d444950f3c553d7b4c4ba87eef963e99bd133548abf3170e23688599edba69a4"
SRC_URI[javacc-manual.sha256sum] = "94db6cfbb30feaf1bdad48cffef726215845ba8d3c7e94de6dee4f26095db991"
