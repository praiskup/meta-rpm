SUMMARY = "generated recipe based on maven-install-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-install-plugin = "apache-commons-codec java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact-manager maven-lib maven-project maven-shared-utils plexus-utils"
RDEPENDS_maven-install-plugin-javadoc = "javapackages-filesystem javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-install-plugin-2.5.2-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-install-plugin-javadoc-2.5.2-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-install-plugin.sha256sum] = "0349c7d09db773c9a174cdd7160ff0344c773530bcd0f6be69dc61aea6c0b65c"
SRC_URI[maven-install-plugin-javadoc.sha256sum] = "ae2fae2966149498878c78edb29005c2add4c1af36b48f00b8bb31028c97cade"
