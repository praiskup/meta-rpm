SUMMARY = "generated recipe based on xorg-x11-docs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-docs-1.7.1-7.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-docs.sha256sum] = "f706026925c7f4b7348017cc575f58fa7636bace90c026c92b67e5acd405d1cf"
