SUMMARY = "generated recipe based on star srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr libselinux pkgconfig-native"
RPM_SONAME_REQ_rmt = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_rmt = "glibc"
RPM_SONAME_REQ_spax = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_spax = "bash chkconfig glibc libacl libattr libselinux"
RPM_SONAME_REQ_star = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_star = "glibc libacl libattr libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rmt-1.5.3-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/spax-1.5.3-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/star-1.5.3-13.el8.aarch64.rpm \
          "

SRC_URI[rmt.sha256sum] = "6168b6a3a22f992144a6bee25924e4d68163f4d0971fddee482078de7fa421b4"
SRC_URI[spax.sha256sum] = "4804f9e8e6faeeedf871c0543209cd79b0f95ad80b15a4ca56ce4aafca5e7d38"
SRC_URI[star.sha256sum] = "c2fe860c85bc9e48032e203d2fbe448eb1ce19e0c934313d1857dad10291630e"
