SUMMARY = "generated recipe based on texi2html srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_texi2html = "bash info latex2html perl-Data-Dumper perl-Exporter perl-Getopt-Long perl-PathTools perl-Text-Unidecode perl-Unicode-EastAsianWidth perl-interpreter perl-libintl-perl perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/texi2html-5.0-8.el8.noarch.rpm \
          "

SRC_URI[texi2html.sha256sum] = "9a687037dc92254a7d5c5468039ca1883c83d63fed71756318ded1cd90a50b16"
