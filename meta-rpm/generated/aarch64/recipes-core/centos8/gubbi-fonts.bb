SUMMARY = "generated recipe based on gubbi-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gubbi-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gubbi-fonts-1.3-2.el8.noarch.rpm \
          "

SRC_URI[gubbi-fonts.sha256sum] = "419cdbd4453a6ae8080482293139c63eac61075e9f09bff6078f48154efc7359"
