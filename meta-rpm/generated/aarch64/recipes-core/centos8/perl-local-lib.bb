SUMMARY = "generated recipe based on perl-local-lib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-local-lib = "perl-Carp perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-local-lib-2.000024-2.el8.noarch.rpm \
          "

SRC_URI[perl-local-lib.sha256sum] = "87bc777cf3f02b721141ab89c9d140233b6b5d04572d5786d52e47a5bb13e4a4"
