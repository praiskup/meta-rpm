SUMMARY = "generated recipe based on shadow-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr audit libselinux libsemanage libxcrypt pkgconfig-native"
RPM_SONAME_REQ_shadow-utils = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libaudit.so.1 libc.so.6 libcrypt.so.1 libselinux.so.1 libsemanage.so.1"
RDEPENDS_shadow-utils = "audit-libs coreutils glibc libacl libattr libselinux libsemanage libxcrypt setup"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/shadow-utils-4.6-8.el8.aarch64.rpm \
          "

SRC_URI[shadow-utils.sha256sum] = "4ec8423570d1cb923acc9e03a87c02065eb26e2aa67048240223fc72cdb861c5"
