SUMMARY = "generated recipe based on pcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi-libs cyrus-sasl-lib device-mapper-libs libgcc libglvnd libpfm libuv libvarlink ncurses nspr nss openssl perl pkgconfig-native platform-python3 qt5-qtbase qt5-qtsvg rdma-core readline rpm systemd-libs xz"
RPM_SONAME_REQ_pcp = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpcp.so.3 libpcp_gui.so.2 libpcp_mmv.so.1 libpcp_pmda.so.3 libpcp_trace.so.2 libpcp_web.so.1 libpthread.so.0 libreadline.so.7 librt.so.1 libssl.so.1.1 libuv.so.1"
RDEPENDS_pcp = "bash findutils gawk glibc grep hostname libuv openssl-libs pcp-libs pcp-selinux readline sed which xz"
RPM_SONAME_REQ_pcp-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RPROVIDES_pcp-devel = "pcp-dev (= 5.0.2)"
RDEPENDS_pcp-devel = "bash glibc pcp pcp-libs pcp-libs-devel perl-PCP-PMDA perl-interpreter perl-libs"
RDEPENDS_pcp-export-pcp2elasticsearch = "pcp pcp-libs python3-pcp python3-requests"
RDEPENDS_pcp-export-pcp2graphite = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2influxdb = "pcp pcp-libs python3-pcp python3-requests"
RDEPENDS_pcp-export-pcp2json = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2spark = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2xml = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2zabbix = "pcp pcp-libs python3-pcp"
RPM_SONAME_REQ_pcp-export-zabbix-agent = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3"
RDEPENDS_pcp-export-zabbix-agent = "glibc pcp-libs"
RPM_SONAME_REQ_pcp-gui = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpcp.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_pcp-gui = "bash glibc liberation-sans-fonts libgcc libglvnd-glx libstdc++ pcp pcp-libs qt5-qtbase qt5-qtbase-gui qt5-qtsvg"
RPM_SONAME_REQ_pcp-import-collectl2pcp = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_import.so.1"
RDEPENDS_pcp-import-collectl2pcp = "glibc pcp-libs"
RDEPENDS_pcp-import-ganglia2pcp = "pcp-libs perl-PCP-LogImport perl-TimeDate perl-interpreter perl-libs rrdtool-perl"
RDEPENDS_pcp-import-iostat2pcp = "pcp-libs perl-PCP-LogImport perl-TimeDate perl-interpreter perl-libs"
RDEPENDS_pcp-import-mrtg2pcp = "pcp-libs perl-PCP-LogImport perl-interpreter perl-libs"
RDEPENDS_pcp-import-sar2pcp = "pcp-libs perl-PCP-LogImport perl-TimeDate perl-XML-TokeParser perl-interpreter perl-libs"
RPM_SONAME_PROV_pcp-libs = "libpcp.so.3 libpcp_gui.so.2 libpcp_import.so.1 libpcp_mmv.so.1 libpcp_pmda.so.3 libpcp_trace.so.2 libpcp_web.so.1"
RPM_SONAME_REQ_pcp-libs = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcrypto.so.1.1 libdl.so.2 liblzma.so.5 libm.so.6 libnspr4.so libnss3.so libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0 librt.so.1 libsasl2.so.3 libssl.so.1.1 libssl3.so libuv.so.1"
RDEPENDS_pcp-libs = "avahi-libs cyrus-sasl-lib glibc libuv nspr nss openssl-libs pcp-conf xz-libs"
RPM_SONAME_REQ_pcp-libs-devel = "libpcp.so.3 libpcp_gui.so.2 libpcp_import.so.1 libpcp_mmv.so.1 libpcp_pmda.so.3 libpcp_trace.so.2 libpcp_web.so.1"
RPROVIDES_pcp-libs-devel = "pcp-libs-dev (= 5.0.2)"
RDEPENDS_pcp-libs-devel = "pcp pcp-libs pkgconf-pkg-config"
RPM_SONAME_REQ_pcp-manager = "ld-linux-aarch64.so.1 libatomic.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpcp.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_pcp-manager = "bash glibc libatomic libgcc libstdc++ pcp pcp-libs"
RDEPENDS_pcp-pmda-activemq = "bash perl-Digest-MD5 perl-JSON perl-PCP-PMDA perl-Time-HiRes perl-libs perl-libwww-perl"
RPM_SONAME_REQ_pcp-pmda-apache = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libpcp_web.so.1 libpthread.so.0 librt.so.1 libuv.so.1"
RDEPENDS_pcp-pmda-apache = "bash glibc libuv pcp-libs"
RPM_SONAME_REQ_pcp-pmda-bash = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-bash = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-bcc = "bash pcp python3-bcc python3-pcp"
RDEPENDS_pcp-pmda-bind2 = "bash perl-File-Slurp perl-PCP-PMDA perl-XML-LibXML perl-libwww-perl"
RDEPENDS_pcp-pmda-bonding = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-bpftrace = "bash bpftrace platform-python python3-pcp python36"
RPM_SONAME_REQ_pcp-pmda-cifs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-cifs = "bash glibc pcp-libs"
RPM_SONAME_REQ_pcp-pmda-cisco = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0"
RDEPENDS_pcp-pmda-cisco = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-dbping = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-dm = "ld-linux-aarch64.so.1 libc.so.6 libdevmapper.so.1.02 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-dm = "bash device-mapper-libs glibc pcp-libs"
RPM_SONAME_REQ_pcp-pmda-docker = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libpcp_web.so.1 libpthread.so.0 librt.so.1 libssl.so.1.1 libuv.so.1"
RDEPENDS_pcp-pmda-docker = "bash glibc libuv openssl-libs pcp pcp-libs"
RDEPENDS_pcp-pmda-ds389 = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-ds389log = "bash perl-Date-Manip perl-PCP-PMDA"
RDEPENDS_pcp-pmda-elasticsearch = "bash pcp python3-pcp"
RPM_SONAME_REQ_pcp-pmda-gfs2 = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-gfs2 = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-gluster = "bash pcp python3-pcp"
RDEPENDS_pcp-pmda-gpfs = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-gpsd = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-haproxy = "bash pcp python3-pcp"
RPM_SONAME_REQ_pcp-pmda-infiniband = "ld-linux-aarch64.so.1 libc.so.6 libibmad.so.5 libibumad.so.3 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-infiniband = "bash glibc infiniband-diags libibumad pcp pcp-libs"
RDEPENDS_pcp-pmda-json = "bash pcp python3-jsonpointer python3-pcp python3-six"
RDEPENDS_pcp-pmda-libvirt = "bash pcp python3-libvirt python3-lxml python3-pcp"
RDEPENDS_pcp-pmda-lio = "bash pcp python3-pcp python3-rtslib"
RDEPENDS_pcp-pmda-lmsensors = "bash pcp pcp-libs python3-pcp"
RPM_SONAME_REQ_pcp-pmda-logger = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-logger = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-lustre = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-lustrecomm = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-lustrecomm = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-mailq = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-mailq = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-memcache = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-mic = "bash pcp python3-pcp"
RPM_SONAME_REQ_pcp-pmda-mounts = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-mounts = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-mysql = "bash perl-DBI perl-PCP-PMDA"
RDEPENDS_pcp-pmda-named = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-netcheck = "bash pcp pcp-libs python3-pcp"
RDEPENDS_pcp-pmda-netfilter = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-news = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-nfsclient = "bash pcp python3-pcp"
RDEPENDS_pcp-pmda-nginx = "bash perl-PCP-PMDA perl-libwww-perl"
RPM_SONAME_REQ_pcp-pmda-nvidia-gpu = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-nvidia-gpu = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-openmetrics = "bash pcp pcp-libs python3-pcp python3-requests"
RDEPENDS_pcp-pmda-oracle = "bash perl-DBI perl-PCP-PMDA perl-interpreter perl-libs"
RDEPENDS_pcp-pmda-pdns = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-perfevent = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpcp.so.3 libpcp_pmda.so.3 libpfm.so.4 libpthread.so.0"
RDEPENDS_pcp-pmda-perfevent = "bash glibc libpfm pcp pcp-libs perl-interpreter perl-libs"
RPM_SONAME_REQ_pcp-pmda-podman = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libvarlink.so.0"
RDEPENDS_pcp-pmda-podman = "bash glibc libvarlink pcp pcp-libs"
RDEPENDS_pcp-pmda-postfix = "bash perl-PCP-PMDA perl-Time-HiRes postfix-perl-scripts"
RDEPENDS_pcp-pmda-postgresql = "bash pcp python3-pcp python3-psycopg2"
RDEPENDS_pcp-pmda-redis = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-roomtemp = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-roomtemp = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-rpm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0 librpm.so.8"
RDEPENDS_pcp-pmda-rpm = "bash glibc pcp pcp-libs rpm-libs"
RDEPENDS_pcp-pmda-rsyslog = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-samba = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-sendmail = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-sendmail = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-shping = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0"
RDEPENDS_pcp-pmda-shping = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-slurm = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-smart = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-smart = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-snmp = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-summary = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-summary = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-systemd = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libsystemd.so.0"
RDEPENDS_pcp-pmda-systemd = "bash glibc pcp-libs systemd-libs"
RPM_SONAME_REQ_pcp-pmda-trace = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpcp_trace.so.2"
RDEPENDS_pcp-pmda-trace = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-unbound = "bash pcp python3-pcp"
RDEPENDS_pcp-pmda-vmware = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-weblog = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0"
RDEPENDS_pcp-pmda-weblog = "bash glibc pcp pcp-libs"
RDEPENDS_pcp-pmda-zimbra = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-zswap = "bash pcp python3-pcp"
RDEPENDS_pcp-selinux = "bash policycoreutils selinux-policy-targeted"
RPM_SONAME_REQ_pcp-system-tools = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libncurses.so.6 libpcp.so.3 libpcp_gui.so.2 libtinfo.so.6"
RDEPENDS_pcp-system-tools = "bash glibc ncurses-libs pcp pcp-libs python3-pcp"
RPM_SONAME_REQ_pcp-testsuite = "ld-linux-aarch64.so.1 libQt5Core.so.5 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_pcp-testsuite = "bash bc bzip2 gcc glibc gzip libgcc libstdc++ pcp pcp-devel pcp-gui pcp-libs pcp-libs-devel pcp-pmda-activemq pcp-pmda-apache pcp-pmda-bash pcp-pmda-bcc pcp-pmda-bind2 pcp-pmda-bonding pcp-pmda-bpftrace pcp-pmda-cisco pcp-pmda-dbping pcp-pmda-dm pcp-pmda-docker pcp-pmda-ds389 pcp-pmda-ds389log pcp-pmda-elasticsearch pcp-pmda-gfs2 pcp-pmda-gluster pcp-pmda-gpfs pcp-pmda-gpsd pcp-pmda-haproxy pcp-pmda-json pcp-pmda-libvirt pcp-pmda-lio pcp-pmda-lmsensors pcp-pmda-logger pcp-pmda-lustre pcp-pmda-lustrecomm pcp-pmda-mailq pcp-pmda-memcache pcp-pmda-mic pcp-pmda-mounts pcp-pmda-mysql pcp-pmda-named pcp-pmda-netcheck pcp-pmda-netfilter pcp-pmda-news pcp-pmda-nfsclient pcp-pmda-nginx pcp-pmda-nvidia-gpu pcp-pmda-openmetrics pcp-pmda-oracle pcp-pmda-pdns pcp-pmda-podman pcp-pmda-postfix pcp-pmda-postgresql pcp-pmda-roomtemp pcp-pmda-rpm pcp-pmda-samba pcp-pmda-sendmail pcp-pmda-shping pcp-pmda-slurm pcp-pmda-smart pcp-pmda-snmp pcp-pmda-summary pcp-pmda-trace pcp-pmda-unbound pcp-pmda-vmware pcp-pmda-weblog pcp-pmda-zimbra pcp-pmda-zswap pcp-system-tools perl-Getopt-Long perl-IO perl-PCP-LogImport perl-TimeDate perl-interpreter perl-libnet perl-libs platform-python qt5-qtbase redhat-rpm-config"
RDEPENDS_pcp-zeroconf = "bash pcp pcp-doc pcp-pmda-dm pcp-pmda-nfsclient pcp-system-tools"
RPM_SONAME_REQ_perl-PCP-LogImport = "libc.so.6 libpcp.so.3 libpcp_import.so.1 libperl.so.5.26"
RDEPENDS_perl-PCP-LogImport = "glibc pcp-libs perl-Exporter perl-interpreter perl-libs"
RDEPENDS_perl-PCP-LogSummary = "pcp-libs perl-Exporter perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-PCP-MMV = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_mmv.so.1 libperl.so.5.26"
RDEPENDS_perl-PCP-MMV = "glibc pcp-libs perl-Exporter perl-Time-HiRes perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-PCP-PMDA = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libperl.so.5.26"
RDEPENDS_perl-PCP-PMDA = "glibc pcp-libs perl-Exporter perl-interpreter perl-libs"
RPM_SONAME_REQ_python3-pcp = "ld-linux-aarch64.so.1 libc.so.6 libpcp.so.3 libpcp_gui.so.2 libpcp_import.so.1 libpcp_mmv.so.1 libpcp_pmda.so.3 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pcp = "glibc pcp pcp-libs platform-python python3-libs python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-conf-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-devel-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-doc-5.0.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2elasticsearch-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2graphite-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2influxdb-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2json-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2spark-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2xml-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-pcp2zabbix-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-export-zabbix-agent-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-gui-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-import-collectl2pcp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-import-ganglia2pcp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-import-iostat2pcp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-import-mrtg2pcp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-import-sar2pcp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-libs-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-libs-devel-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-manager-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-activemq-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-apache-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-bash-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-bcc-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-bind2-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-bonding-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-bpftrace-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-cifs-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-cisco-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-dbping-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-dm-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-docker-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-ds389-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-ds389log-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-elasticsearch-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-gfs2-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-gluster-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-gpfs-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-gpsd-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-haproxy-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-infiniband-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-json-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-libvirt-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-lio-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-lmsensors-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-logger-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-lustre-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-lustrecomm-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-mailq-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-memcache-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-mic-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-mounts-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-mysql-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-named-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-netcheck-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-netfilter-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-news-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-nfsclient-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-nginx-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-nvidia-gpu-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-openmetrics-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-oracle-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-pdns-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-perfevent-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-podman-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-postfix-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-postgresql-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-redis-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-roomtemp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-rpm-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-rsyslog-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-samba-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-sendmail-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-shping-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-slurm-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-smart-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-snmp-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-summary-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-systemd-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-trace-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-unbound-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-vmware-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-weblog-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-zimbra-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-pmda-zswap-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-selinux-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-system-tools-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-testsuite-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pcp-zeroconf-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-PCP-LogImport-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-PCP-LogSummary-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-PCP-MMV-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-PCP-PMDA-5.0.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pcp-5.0.2-5.el8.aarch64.rpm \
          "

SRC_URI[pcp.sha256sum] = "8243c12ce8779ebf5208066d3dd0c94d654b2aedaf8994ce9ce33aa9658332d4"
SRC_URI[pcp-conf.sha256sum] = "c91cefc0d7cdc97be196bd4edd5ee80f05c56b7d646a2a493d6cfc6737e87d67"
SRC_URI[pcp-devel.sha256sum] = "4ca1c3ee25eff9c8510a0da8443bf4c2d1c56cfc5711da90809b9448671f0d2b"
SRC_URI[pcp-doc.sha256sum] = "d1f44417b64b8397b541623d4686824f52434cea11692782be93ae803ef05e2a"
SRC_URI[pcp-export-pcp2elasticsearch.sha256sum] = "e4de8740677b6a89864298dafe38e589efc753fd378ccffc7269d59a34e01672"
SRC_URI[pcp-export-pcp2graphite.sha256sum] = "6f162f55fc5ec7f63a84f8876f0c112001a41e10637e3dafe12ac110853150cd"
SRC_URI[pcp-export-pcp2influxdb.sha256sum] = "a510c12d13b719d408f30377f396741928228945cb03688cad8fcc318c79a6d5"
SRC_URI[pcp-export-pcp2json.sha256sum] = "428608630604fb07c04f5c32d854603afb4254457bd004b5ad770ef607a293bc"
SRC_URI[pcp-export-pcp2spark.sha256sum] = "9dc1c8f7ec06ee0f8c5a559d1b2488166766ebaca92506635fbcfbd7470d55e6"
SRC_URI[pcp-export-pcp2xml.sha256sum] = "3dcdc3089f19ac6e51aa7f4887a83275169bf5cebd05ec2b03f626440ce01879"
SRC_URI[pcp-export-pcp2zabbix.sha256sum] = "8694d3788389884b370cff96c4cd4ea97eb52dcf1bd6814130540055395336ed"
SRC_URI[pcp-export-zabbix-agent.sha256sum] = "b76874f1c0d7d71d75a33f9bec56b7611107e6cc85d2b1001d693b6c4ac93527"
SRC_URI[pcp-gui.sha256sum] = "f587eec7d54a008abb263c0f1c36aa1b3562246b27f41e1387aab98da31b7882"
SRC_URI[pcp-import-collectl2pcp.sha256sum] = "f4806892cde606117a28547e2eb2df13fd112ea5f3477121060168b776ceecfd"
SRC_URI[pcp-import-ganglia2pcp.sha256sum] = "8468ae01abc712264901abf21621144a2f0f09c96d13cabd0defbe9ad4af59d4"
SRC_URI[pcp-import-iostat2pcp.sha256sum] = "ed21124b327ea7bb42a679bc51cce1809982465190dabfbc17d8e208574e8048"
SRC_URI[pcp-import-mrtg2pcp.sha256sum] = "a58ddd0f3e5fcc2a5fbaf19281d46fadab9ffbd674ac47b296bdad42be6674ef"
SRC_URI[pcp-import-sar2pcp.sha256sum] = "e782320f81e0b667ddc34657381bdc56fd62556718963241267a768191e4cab3"
SRC_URI[pcp-libs.sha256sum] = "de02b0b271ed88d90f6a0be9b71ffe3fc5164e50416c8bc6d95483da87de55f3"
SRC_URI[pcp-libs-devel.sha256sum] = "aba73cbd6ccc9c3ba45c73fae8866dc33117a8723f67b8ce55671d77cbd36afa"
SRC_URI[pcp-manager.sha256sum] = "25ca4fc944c5780dae432031fc5af1d4420b1808b010894f0bcc05d945baab49"
SRC_URI[pcp-pmda-activemq.sha256sum] = "01b46323f0e71c77cf78097af7aae49546a2ed685c79a82389cc994de3f658b4"
SRC_URI[pcp-pmda-apache.sha256sum] = "181035f7fd6989af52178e3240ec79507f9a518c74e6b8da59d235184a52871f"
SRC_URI[pcp-pmda-bash.sha256sum] = "497dfb1634f571398a9a1c0207377ec1a25c59cd5a144aff5697a20b3d986dff"
SRC_URI[pcp-pmda-bcc.sha256sum] = "b8b5a77657045bdf8b9403d3a76ab7f17072397794c36174bde3b0f0ef1de3e8"
SRC_URI[pcp-pmda-bind2.sha256sum] = "0d76108ffd41953c05affb241ffe1e9fa6def3b99638dc123fd81f16143dad83"
SRC_URI[pcp-pmda-bonding.sha256sum] = "d3cf821a57717ea3ba0c8218a01ab1d0b7d37a26f98bf5b34e74b6270169f1ca"
SRC_URI[pcp-pmda-bpftrace.sha256sum] = "358481606f0c4a90eb7a0bc92cc6b846b0532049f78eef9599c943225007106b"
SRC_URI[pcp-pmda-cifs.sha256sum] = "ba9077738f1f30bc3a4f5d8d9b0ff4bc28b2ee1506d79bdc34b422ca9947575b"
SRC_URI[pcp-pmda-cisco.sha256sum] = "206b53320a50025a165cec48a7ec1b8a31874fcad1546ee54e7c6f9d6436a212"
SRC_URI[pcp-pmda-dbping.sha256sum] = "d9eb97fd9a44e5e7dc3b5ce46dd3476698a2b8507b0a8c1d02ffcab62266d6b9"
SRC_URI[pcp-pmda-dm.sha256sum] = "89e81d0b92c20ddeb11beb74a782d076105064f933f255f4ad80a44d6e7e61dc"
SRC_URI[pcp-pmda-docker.sha256sum] = "22fe09e7221663f9200c31552d8f4977ed16a8f996a19f4fb4568e0935dda067"
SRC_URI[pcp-pmda-ds389.sha256sum] = "d97e19f9b5da94e90e41641ce30daa22587548efb4485d5c4f558c5f4861cb07"
SRC_URI[pcp-pmda-ds389log.sha256sum] = "3086b3e8820acb4d159162ee7f3569f7e6c3d63ca9c943a8891818aa3082d58a"
SRC_URI[pcp-pmda-elasticsearch.sha256sum] = "63c7ddfffd4f00f16300ad8ef64e2a1dc2f93f2420f4b071ef5c9e003644ec0e"
SRC_URI[pcp-pmda-gfs2.sha256sum] = "4a0af564f1591a7f2b8dac91b6db3c003a7b491059820f35dac8a4eb40a81bd1"
SRC_URI[pcp-pmda-gluster.sha256sum] = "cd18c34db5e97996568dbf8a52a4992dce582e5b6dab3fef71327bb4f4eedb20"
SRC_URI[pcp-pmda-gpfs.sha256sum] = "4a039f7fa71d00b986d927fbbb66d5e585045b2a8b8a9bbf250a08a7671a1ad8"
SRC_URI[pcp-pmda-gpsd.sha256sum] = "29cdd5582ec11b83fd41867a561ba077363d616598dbcd4fc644d05c15aa0a6f"
SRC_URI[pcp-pmda-haproxy.sha256sum] = "d19b4f1723a84dde3eee182ab50d261837a6ee8e6e9222b25e64029d2cafd78c"
SRC_URI[pcp-pmda-infiniband.sha256sum] = "23e417c3372755905aec0d0222d17f6587720c58b0ea201bbbac8baa055696a5"
SRC_URI[pcp-pmda-json.sha256sum] = "79dd8dc6b3458d7e7265d9ea530469ad4e393f3b40eedbf145ea9860377d43cb"
SRC_URI[pcp-pmda-libvirt.sha256sum] = "327cb079b3abfa0e41dcc914f91a608b280ae87140f4d255ceeef174804dadc2"
SRC_URI[pcp-pmda-lio.sha256sum] = "5ac39ca9658ddba3f4a53ad7c5150937f560208d3386a5bd5561e192ccc79d8d"
SRC_URI[pcp-pmda-lmsensors.sha256sum] = "42d583037ef3c767a8d6661fac57afaa66131e4ea0be3446c0f623f0c752ec5b"
SRC_URI[pcp-pmda-logger.sha256sum] = "818b6b99019aec039100aea6598160cf0eb7343cd4994c76cd71366321edc5ce"
SRC_URI[pcp-pmda-lustre.sha256sum] = "dd91bdc6c4f101323b50dd1f28b808bb65b78cf10e276a2ea0ee5b8ca77333c0"
SRC_URI[pcp-pmda-lustrecomm.sha256sum] = "fd9041184fa084dd15c2434dd87af524e1723decff17c7ae4e20d0070996dfd8"
SRC_URI[pcp-pmda-mailq.sha256sum] = "17fe2786ba6c79e4b9d4e46f34e3178579b835217c13862fa175a250bb8f5b83"
SRC_URI[pcp-pmda-memcache.sha256sum] = "682f462e25e65b0af0a59b0703ef95a3b9e5296989f50a0e23028bcf2b208d3f"
SRC_URI[pcp-pmda-mic.sha256sum] = "c82201b1cf7b23c028556cc94b7a6db50b0d6307b1990701abb778da27cb9601"
SRC_URI[pcp-pmda-mounts.sha256sum] = "5d75958435a2f4d96603fd972790f8590b08d583ea45d18ed247f4be178a08fd"
SRC_URI[pcp-pmda-mysql.sha256sum] = "4488d97676e80220bfffab8b6385dfe061ae68a7e8771ee9897292043962d941"
SRC_URI[pcp-pmda-named.sha256sum] = "29240f491153ff87f013933e237583545a50079462b1dce1ce72046c8bbfa01c"
SRC_URI[pcp-pmda-netcheck.sha256sum] = "3055251719a1329f42de5a6dd84b487d49c00a15f4ab6402d67a3966ac448f4d"
SRC_URI[pcp-pmda-netfilter.sha256sum] = "50ec03dd4462102af0e2d853166bedc9b4512524d8daa351787366a449377861"
SRC_URI[pcp-pmda-news.sha256sum] = "c1bee83ef8b924284a39d50177ed47b7d9f1197f74ae7ecdfdd12644a47ad5ac"
SRC_URI[pcp-pmda-nfsclient.sha256sum] = "351a79b8554f3a263ca9d3475f794265723758b37948113d430be3957334a406"
SRC_URI[pcp-pmda-nginx.sha256sum] = "77a51e0860a19a6dd2a2c2111c3613eb88db94147f5ba913f858ab8d7d8e3b1c"
SRC_URI[pcp-pmda-nvidia-gpu.sha256sum] = "8af6ca312c8e1455f48a358f587576c2e4281a0a235d65e5dea518b073705ad7"
SRC_URI[pcp-pmda-openmetrics.sha256sum] = "ad672fb74a3e761c0abbd83f54d8d049cb87bd8b6934cda682f8ddae80c78370"
SRC_URI[pcp-pmda-oracle.sha256sum] = "7e4df4a3e719e830af3c2b38102744e94dc17b6b0ab5c76a303c696c47879165"
SRC_URI[pcp-pmda-pdns.sha256sum] = "61674173845e8de08dae9fac0fee9ec9bd241eaea13fb43ec91bbbf4ed49f2a4"
SRC_URI[pcp-pmda-perfevent.sha256sum] = "cff3d6ac1f5e0a4fad07de7e4a22ac1948482f370e74dd8a4e62360d933c878e"
SRC_URI[pcp-pmda-podman.sha256sum] = "a3f787bd7864de31fd7d6414b10bc81ff6417cdf1f9753f9af5c58c2945bd853"
SRC_URI[pcp-pmda-postfix.sha256sum] = "5571a0b181f812f51cfd232723c894360f14df4d26432650182eb888b482ca26"
SRC_URI[pcp-pmda-postgresql.sha256sum] = "82b11dd7e0b57c8fb155a96ec0d632b5ac653fad882e159e97235a4b40125432"
SRC_URI[pcp-pmda-redis.sha256sum] = "4ad610917082fb754297d25a76bb7ca0fe1ad7e073d6648d9c098e5c42747e1d"
SRC_URI[pcp-pmda-roomtemp.sha256sum] = "8299e5abf6a1790946928e924d39a7d93baf645380063160686f11080d57077f"
SRC_URI[pcp-pmda-rpm.sha256sum] = "cf0f152e08247b0286121b32c99bb8284656d87250c8660aafbbcb7c36e8f017"
SRC_URI[pcp-pmda-rsyslog.sha256sum] = "65d9bb9c48bc31e28770dee4a004167fb31dde4120bf6c86f3f08b69a74326a4"
SRC_URI[pcp-pmda-samba.sha256sum] = "b5584b4706554c5d96959d3867aded4e4a7ffd98d7fd23c0c0716a4539380b95"
SRC_URI[pcp-pmda-sendmail.sha256sum] = "672ba5240b36dec21d31f8cb21b64c69b1aeb6588eca4eb914fd3ec4f3468f5c"
SRC_URI[pcp-pmda-shping.sha256sum] = "6bec2915f1b7cfd200f89ffb7cd3ba1d997680d653a4fd00a4897da33cdc463c"
SRC_URI[pcp-pmda-slurm.sha256sum] = "a2bdad768567203cd817a67f849122201e14c724aeffe0f710bf3d613fffe6df"
SRC_URI[pcp-pmda-smart.sha256sum] = "86179244b9e53b05e332d3fb32aa662160e880770923937c4696a2f701334815"
SRC_URI[pcp-pmda-snmp.sha256sum] = "c781d4be18649564f5dd52033217bb8fa821cbc15b8ed80b8c32e5a594d9c2b2"
SRC_URI[pcp-pmda-summary.sha256sum] = "2c0dd01d6498ff52b45cbe8900c247b577bc649044dcddcfcda89fd9c79bb1d8"
SRC_URI[pcp-pmda-systemd.sha256sum] = "d6c9f64e66878fe4b8b8d79fe5a2f0353fefafe295d8f6b4deeb212880edde2a"
SRC_URI[pcp-pmda-trace.sha256sum] = "06cd218c0f1116a57f44e3b2b8e87e4a3b15a4a5fc82f45b29bd1e63bb90ee99"
SRC_URI[pcp-pmda-unbound.sha256sum] = "5f491c33fe6a66a0501a38053a8bd3d869d3b6ec1815c4025fd0503dfcb66984"
SRC_URI[pcp-pmda-vmware.sha256sum] = "27aa10a204fa125302d56e99e0616f35beb376d4dcb75bfd70b45012020b03aa"
SRC_URI[pcp-pmda-weblog.sha256sum] = "9dcd6f4fa3ad85f2c1fbec5c31d6883e63a0c37525a67560b563ef874858ed54"
SRC_URI[pcp-pmda-zimbra.sha256sum] = "738efd8be5c9d3eddaafe63c4b4a8eaff29aa54f75b924914ada8251b4469d21"
SRC_URI[pcp-pmda-zswap.sha256sum] = "e85fb33869e440514e7c0b4113b4d6f16e1a3426babc8effd2b51a86a53acd70"
SRC_URI[pcp-selinux.sha256sum] = "8b0489eb2e88c7f010f188a2ac456c9ce9ddb5cbffa0a2ca0d82ba7ab38ee097"
SRC_URI[pcp-system-tools.sha256sum] = "413ef30624419d35c19d1a8b44f38bb5c5ab3f330c5f6dace0996c32e86cb2e5"
SRC_URI[pcp-testsuite.sha256sum] = "2e8c23c9492f51541f0eb6225d2a56374ca896fa4ab80c0ea15bf45ed26d07b6"
SRC_URI[pcp-zeroconf.sha256sum] = "71fb8faf57be08e20cf8db8c2719d880ac1448346778ac042cac0c7b93e75f49"
SRC_URI[perl-PCP-LogImport.sha256sum] = "ff9e35a6dc590ea7af5b17af5366c3ba22935015e9d9fb6178cd51c97cbefab5"
SRC_URI[perl-PCP-LogSummary.sha256sum] = "bba78bb5887cbe60a7fb26d92eeec06bc8c1979a2978efae5ed22124322f4eef"
SRC_URI[perl-PCP-MMV.sha256sum] = "d94811508a8b0d14b11492ba825005ea0b01867f1f4b0bb3e88ad5890a677427"
SRC_URI[perl-PCP-PMDA.sha256sum] = "a86145346754c525e9fb3ce76fae1dd9e9a98e6fa14b712822d8fc116bb86319"
SRC_URI[python3-pcp.sha256sum] = "46cab5969a7ff0a117b3b7be0be81f9e59a558616cb544081a799a8fd5e0a03e"
