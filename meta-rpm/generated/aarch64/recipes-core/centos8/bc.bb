SUMMARY = "generated recipe based on bc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_bc = "ld-linux-aarch64.so.1 libc.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_bc = "bash glibc info ncurses-libs readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bc-1.07.1-5.el8.aarch64.rpm \
          "

SRC_URI[bc.sha256sum] = "c7b35e022cd8163bce181a18b373d0b6c151cc7fad4970bee29152a0bfcbc16e"
