SUMMARY = "generated recipe based on python-pyasn1 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyasn1 = "platform-python"
RDEPENDS_python3-pyasn1-modules = "platform-python python3-pyasn1"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyasn1-0.3.7-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyasn1-modules-0.3.7-6.el8.noarch.rpm \
          "

SRC_URI[python3-pyasn1.sha256sum] = "16bb90f4105692da54369c59e5d4aea2e2e6ef7ab53a485b984f780c3c34d916"
SRC_URI[python3-pyasn1-modules.sha256sum] = "69fe47cd8ee483c0a816ac5f23df0b4d8d507f646a9d29740f019b1e79c0b2d9"
