SUMMARY = "generated recipe based on nfs-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs e2fsprogs keyutils krb5-libs libblkid libcap libevent libmount libtirpc libxml2 openldap pkgconfig-native sqlite3"
RPM_SONAME_PROV_libnfsidmap = "libnfsidmap.so.1"
RPM_SONAME_REQ_libnfsidmap = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libldap-2.4.so.2 libresolv.so.2 libtirpc.so.3"
RDEPENDS_libnfsidmap = "glibc libtirpc openldap"
RPM_SONAME_REQ_libnfsidmap-devel = "libnfsidmap.so.1"
RPROVIDES_libnfsidmap-devel = "libnfsidmap-dev (= 2.3.3)"
RDEPENDS_libnfsidmap-devel = "libnfsidmap pkgconf-pkg-config"
RPM_SONAME_REQ_nfs-utils = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libdevmapper.so.1.02 libdl.so.2 libevent-2.1.so.6 libgssapi_krb5.so.2 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 libmount.so.1 libnfsidmap.so.1 libpthread.so.0 libresolv.so.2 libsqlite3.so.0 libtirpc.so.3 libxml2.so.2"
RDEPENDS_nfs-utils = "bash coreutils device-mapper-libs gawk glibc grep gssproxy keyutils keyutils-libs kmod krb5-libs libblkid libcap libcom_err libevent libmount libnfsidmap libtirpc libxml2 platform-python quota rpcbind sed shadow-utils sqlite-libs systemd util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnfsidmap-2.3.3-31.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nfs-utils-2.3.3-31.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnfsidmap-devel-2.3.3-31.el8.aarch64.rpm \
          "

SRC_URI[libnfsidmap.sha256sum] = "f0170bbe7443c0851d7f698fe49af3f1741040f4ae82dd01163aadfed0cd799f"
SRC_URI[libnfsidmap-devel.sha256sum] = "7027673a74c0310148b56a6c28ddf38bfb6767de0dfdd7268e51b9ac86da28ac"
SRC_URI[nfs-utils.sha256sum] = "22aed5dede07d9aae72fb9233672bb9f19ea61686931b4246c473993979b97ad"
