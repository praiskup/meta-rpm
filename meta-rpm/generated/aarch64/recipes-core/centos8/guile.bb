SUMMARY = "generated recipe based on guile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gc gmp libffi libtool libunistring libxcrypt ncurses pkgconfig-native readline"
RPM_SONAME_PROV_guile = "libguile-2.0.so.22 libguilereadline-v-18.so.18"
RPM_SONAME_REQ_guile = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libffi.so.6 libgc.so.1 libgmp.so.10 libguile-2.0.so.22 libguilereadline-v-18.so.18 libltdl.so.7 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libtinfo.so.6 libunistring.so.2"
RDEPENDS_guile = "bash coreutils gc glibc gmp info libffi libtool-ltdl libunistring libxcrypt ncurses-libs readline"
RPM_SONAME_REQ_guile-devel = "libguile-2.0.so.22"
RPROVIDES_guile-devel = "guile-dev (= 2.0.14)"
RDEPENDS_guile-devel = "bash gc-devel gmp-devel guile pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/guile-2.0.14-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guile-devel-2.0.14-7.el8.aarch64.rpm \
          "

SRC_URI[guile.sha256sum] = "b399912b3f3cd4fe6575650309212e9f577681694fb4879720028070d64e24b7"
SRC_URI[guile-devel.sha256sum] = "9500189ff1999741de9a5ce859ead8ca0b15273c9419797522cfe38ea4030eff"
