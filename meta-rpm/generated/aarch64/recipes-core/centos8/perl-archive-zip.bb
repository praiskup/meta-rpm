SUMMARY = "generated recipe based on perl-Archive-Zip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Archive-Zip = "perl-Carp perl-Compress-Raw-Zlib perl-Data-Dumper perl-Encode perl-Exporter perl-File-Path perl-File-Temp perl-IO perl-PathTools perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Archive-Zip-1.60-3.el8.noarch.rpm \
          "

SRC_URI[perl-Archive-Zip.sha256sum] = "e2a7aa697857f021a644d566b81b75f04892e77ddcb821ff34bae531b45b2959"
