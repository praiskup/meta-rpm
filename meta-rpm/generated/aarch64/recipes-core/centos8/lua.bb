SUMMARY = "generated recipe based on lua srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_lua = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblua-5.3.so libm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_lua = "glibc lua-libs ncurses-libs readline"
RPROVIDES_lua-devel = "lua-dev (= 5.3.4)"
RDEPENDS_lua-devel = "lua pkgconf-pkg-config"
RPM_SONAME_PROV_lua-libs = "liblua-5.3.so"
RPM_SONAME_REQ_lua-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_lua-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lua-5.3.4-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lua-libs-5.3.4-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lua-devel-5.3.4-11.el8.aarch64.rpm \
          "

SRC_URI[lua.sha256sum] = "2d5fb54643d534114994d192b310531e88b8795f71e61e5a28c1c9360ca79b6e"
SRC_URI[lua-devel.sha256sum] = "2ef04e4a41ba713f793246ad400160d6d98f495ff6899871f5490df913017953"
SRC_URI[lua-libs.sha256sum] = "914f1d8cf5385ec874ac88b00f5ae99e77be48aa6c7157a2e0c1c5355c415c94"
