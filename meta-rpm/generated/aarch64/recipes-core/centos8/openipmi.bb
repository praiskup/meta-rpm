SUMMARY = "generated recipe based on OpenIPMI srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm glib-2.0 ncurses net-snmp openssl pkgconfig-native popt readline"
RPM_SONAME_REQ_OpenIPMI = "ld-linux-aarch64.so.1 libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIposix.so.0 libOpenIPMIui.so.1 libOpenIPMIutils.so.0 libc.so.6 libgdbm.so.6 libglib-2.0.so.0 libgthread-2.0.so.0 libncurses.so.6 libnetsnmp.so.35 libpthread.so.0 libreadline.so.7"
RDEPENDS_OpenIPMI = "OpenIPMI-libs bash gdbm-libs glib2 glibc ncurses-libs net-snmp-libs readline systemd"
RPM_SONAME_REQ_OpenIPMI-devel = "libIPMIlanserv.so.0 libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIposix.so.0 libOpenIPMIpthread.so.0 libOpenIPMIui.so.1 libOpenIPMIutils.so.0"
RPROVIDES_OpenIPMI-devel = "OpenIPMI-dev (= 2.0.27)"
RDEPENDS_OpenIPMI-devel = "OpenIPMI OpenIPMI-lanserv OpenIPMI-libs ncurses-devel pkgconf-pkg-config"
RPM_SONAME_PROV_OpenIPMI-lanserv = "libIPMIlanserv.so.0"
RPM_SONAME_REQ_OpenIPMI-lanserv = "ld-linux-aarch64.so.1 libIPMIlanserv.so.0 libOpenIPMIposix.so.0 libOpenIPMIutils.so.0 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpopt.so.0 libpthread.so.0"
RDEPENDS_OpenIPMI-lanserv = "OpenIPMI OpenIPMI-libs glibc openssl-libs popt"
RPM_SONAME_PROV_OpenIPMI-libs = "libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIposix.so.0 libOpenIPMIpthread.so.0 libOpenIPMIui.so.1 libOpenIPMIutils.so.0"
RPM_SONAME_REQ_OpenIPMI-libs = "ld-linux-aarch64.so.1 libOpenIPMI.so.0 libOpenIPMIutils.so.0 libc.so.6 libcrypto.so.1.1 libgdbm.so.6 libglib-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0"
RDEPENDS_OpenIPMI-libs = "gdbm-libs glib2 glibc openssl-libs"
RPM_SONAME_PROV_OpenIPMI-perl = "libOpenIPMI.so.0"
RPM_SONAME_REQ_OpenIPMI-perl = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_OpenIPMI-perl = "OpenIPMI-libs glibc perl-Exporter perl-interpreter perl-libs"
RPM_SONAME_REQ_python3-openipmi = "ld-linux-aarch64.so.1 libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIpthread.so.0 libOpenIPMIutils.so.0 libc.so.6 libpthread.so.0"
RDEPENDS_python3-openipmi = "OpenIPMI-libs glibc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/OpenIPMI-2.0.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/OpenIPMI-lanserv-2.0.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/OpenIPMI-libs-2.0.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/OpenIPMI-perl-2.0.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-openipmi-2.0.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/OpenIPMI-devel-2.0.27-1.el8.aarch64.rpm \
          "

SRC_URI[OpenIPMI.sha256sum] = "cce7583af335f6d9d16afed56d0a152d3db378996fe1781a017b29d0c32a5a5e"
SRC_URI[OpenIPMI-devel.sha256sum] = "8f9beb50737b904f4df429100702f484888bf6d487d5966b4c2fb52ecb4da8d0"
SRC_URI[OpenIPMI-lanserv.sha256sum] = "25853ca5059b8067da334b329dd5d203fa623bec0f509455497ec72a0b634c94"
SRC_URI[OpenIPMI-libs.sha256sum] = "8c5b07a019d61c31d486b78f21fc504b297986ec0d4d9ce1bfc7ab50e3b7a8b6"
SRC_URI[OpenIPMI-perl.sha256sum] = "260c8fc0892438d6c6bd8a36210b0dfead8fd2dfd0b7881f2eef5ade271ced99"
SRC_URI[python3-openipmi.sha256sum] = "61e722395f939705df7a72ba17e15bc50cefd4a2adde33c73f0cbf141122cce9"
