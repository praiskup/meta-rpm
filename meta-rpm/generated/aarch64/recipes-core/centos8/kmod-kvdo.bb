SUMMARY = "generated recipe based on kmod-kvdo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_kmod-kvdo = "bash kernel kmod"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kmod-kvdo-6.2.2.117-65.el8.aarch64.rpm \
          "

SRC_URI[kmod-kvdo.sha256sum] = "ea3116f2cc94526fe0135568de9e9866fbe8e62ea94d6bdf2556630a3163ecb6"
