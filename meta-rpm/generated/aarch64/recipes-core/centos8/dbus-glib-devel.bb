SUMMARY = "generated recipe based on dbus-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus dbus-libs expat glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_dbus-glib-devel = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libexpat.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RPROVIDES_dbus-glib-devel = "dbus-glib-dev (= 0.110)"
RDEPENDS_dbus-glib-devel = "dbus-devel dbus-glib dbus-libs expat glib2 glib2-devel glibc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dbus-glib-devel-0.110-2.el8.aarch64.rpm \
          "

SRC_URI[dbus-glib-devel.sha256sum] = "2648d3efda8ee0adf35820c35aa24467e2facf9254d45156dda0f718c91dfa62"
