SUMMARY = "generated recipe based on perl-Net-SSLeay srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Net-SSLeay = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0 libssl.so.1.1"
RDEPENDS_perl-Net-SSLeay = "glibc openssl-libs perl-Carp perl-Errno perl-Exporter perl-MIME-Base64 perl-Socket perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Net-SSLeay-1.88-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Net-SSLeay.sha256sum] = "cf870e29cf9736171f3934d5fb3cd2e872b5946975d4d790a763d3b1ec0ff1c5"
