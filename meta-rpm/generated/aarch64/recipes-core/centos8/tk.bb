SUMMARY = "generated recipe based on tk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libx11 libxft pkgconfig-native tcl zlib"
RPM_SONAME_PROV_tk = "libtk8.6.so"
RPM_SONAME_REQ_tk = "ld-linux-aarch64.so.1 libX11.so.6 libXft.so.2 libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libm.so.6 libpthread.so.0 libtcl8.6.so libtk8.6.so libz.so.1"
RDEPENDS_tk = "bash fontconfig freetype glibc libX11 libXft tcl zlib"
RPROVIDES_tk-devel = "tk-dev (= 8.6.8)"
RDEPENDS_tk-devel = "libX11-devel libXft-devel pkgconf-pkg-config tcl-devel tk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tk-8.6.8-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tk-devel-8.6.8-1.el8.aarch64.rpm \
          "

SRC_URI[tk.sha256sum] = "240fffc8568f94904e18e88b4a01b9f1bad1a4c484817714a28f006cc674fab5"
SRC_URI[tk-devel.sha256sum] = "cf580255576506eda4c0fa2f16127b5ef494b04c79fee34b146273a313e35d58"
