SUMMARY = "generated recipe based on hunspell-rw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-rw = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-rw-0.20050109-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-rw.sha256sum] = "b1490a22e17b1c6856b48b2fa2151c4526d958333dde625949dc419134a5aa85"
