SUMMARY = "generated recipe based on enca srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_enca = "libenca.so.0"
RPM_SONAME_REQ_enca = "ld-linux-aarch64.so.1 libc.so.6 libenca.so.0 libm.so.6"
RDEPENDS_enca = "glibc"
RPM_SONAME_REQ_enca-devel = "libenca.so.0"
RPROVIDES_enca-devel = "enca-dev (= 1.19)"
RDEPENDS_enca-devel = "enca pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/enca-1.19-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/enca-devel-1.19-1.el8.aarch64.rpm \
          "

SRC_URI[enca.sha256sum] = "1d2240de0e77e23a226fabe971075b7b72c58d08175985ba5565f3514f5d3b15"
SRC_URI[enca-devel.sha256sum] = "b20773438e175ebf40b993256f9d518a069cb315ee307b7fbce9de2b93f0ce0c"
