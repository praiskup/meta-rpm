SUMMARY = "generated recipe based on libndp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libndp = "libndp.so.0"
RPM_SONAME_REQ_libndp = "ld-linux-aarch64.so.1 libc.so.6 libndp.so.0"
RDEPENDS_libndp = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libndp-1.7-3.el8.aarch64.rpm \
          "

SRC_URI[libndp.sha256sum] = "8a234875b8e2ff7ad0b88bc4493cf6281ce01870b957826ce70d031e1c9405fa"
