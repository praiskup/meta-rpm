SUMMARY = "generated recipe based on python-pillow srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype lcms2 libjpeg-turbo libwebp openjpeg2 pkgconfig-native platform-python3 tiff zlib"
RPM_SONAME_REQ_python3-pillow = "ld-linux-aarch64.so.1 libc.so.6 libfreetype.so.6 libjpeg.so.62 liblcms2.so.2 libopenjp2.so.7 libpthread.so.0 libpython3.6m.so.1.0 libtiff.so.5 libwebp.so.7 libwebpdemux.so.2 libwebpmux.so.3 libz.so.1"
RDEPENDS_python3-pillow = "freetype glibc lcms2 libjpeg-turbo libtiff libwebp openjpeg2 platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pillow-5.1.1-12.el8_2.aarch64.rpm \
          "

SRC_URI[python3-pillow.sha256sum] = "e0f19583b77cc00b482c9bc561b3841657735776d4cddca61a6cab8834d9335c"
