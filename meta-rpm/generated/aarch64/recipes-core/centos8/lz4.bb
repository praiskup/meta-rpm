SUMMARY = "generated recipe based on lz4 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lz4 = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lz4 = "glibc"
RPM_SONAME_REQ_lz4-devel = "liblz4.so.1"
RPROVIDES_lz4-devel = "lz4-dev (= 1.8.1.2)"
RDEPENDS_lz4-devel = "lz4-libs pkgconf-pkg-config"
RPM_SONAME_PROV_lz4-libs = "liblz4.so.1"
RPM_SONAME_REQ_lz4-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lz4-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lz4-1.8.1.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lz4-devel-1.8.1.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lz4-libs-1.8.1.2-4.el8.aarch64.rpm \
          "

SRC_URI[lz4.sha256sum] = "562931c4dadffcafed66c65f72627faad944ed9ba1f668f56842f4dd474aa8c7"
SRC_URI[lz4-devel.sha256sum] = "9d91a0e4163bdbfc409615a904eeee6cd2d4246a66a0108b0f2aea3aa4e7b8f9"
SRC_URI[lz4-libs.sha256sum] = "b646ce7481e0c17b0b5ae21d606e96496a56bf782e94247968061e746d97b4ce"
