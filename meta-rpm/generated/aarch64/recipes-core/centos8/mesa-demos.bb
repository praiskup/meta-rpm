SUMMARY = "generated recipe based on mesa-demos srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libx11 pkgconfig-native"
RPM_SONAME_REQ_glx-utils = "ld-linux-aarch64.so.1 libGL.so.1 libX11.so.6 libc.so.6 libm.so.6"
RDEPENDS_glx-utils = "glibc libX11 libglvnd-glx"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/glx-utils-8.4.0-4.20181118git1830dcb.el8.aarch64.rpm \
          "

SRC_URI[glx-utils.sha256sum] = "fe2e9e7e70319351feacbca2c06fb964bb715bdc6311e4c9c98f8caaaf478254"
