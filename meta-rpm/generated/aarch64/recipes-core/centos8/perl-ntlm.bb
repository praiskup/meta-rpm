SUMMARY = "generated recipe based on perl-NTLM srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-NTLM = "perl-Digest-HMAC perl-Exporter perl-MIME-Base64 perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-NTLM-1.09-17.el8.noarch.rpm \
          "

SRC_URI[perl-NTLM.sha256sum] = "304f32c764d463d88e312018db685217782084a42093492f73e6f9236b938b10"
