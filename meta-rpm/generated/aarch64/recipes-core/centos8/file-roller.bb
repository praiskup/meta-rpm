SUMMARY = "generated recipe based on file-roller srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo file gdk-pixbuf glib-2.0 gtk+3 json-glib libarchive libnotify pango pkgconfig-native"
RPM_SONAME_REQ_file-roller = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libmagic.so.1 libnotify.so.4 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_file-roller = "binutils bzip2 cairo cpio desktop-file-utils file-libs gdk-pixbuf2 genisoimage glib2 glibc gtk3 gzip json-glib libarchive libnotify lzop ncompress pango tar unzip xz zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/file-roller-3.28.1-2.el8.aarch64.rpm \
          "

SRC_URI[file-roller.sha256sum] = "e2db70eb2f7c68a3bd81197616aa6c58b2e5678f81b8a88c56a49cf514027f66"
