SUMMARY = "generated recipe based on libimobiledevice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcrypt libplist libtasn1 libusbmuxd pkgconfig-native"
RPM_SONAME_PROV_libimobiledevice = "libimobiledevice.so.6"
RPM_SONAME_REQ_libimobiledevice = "ld-linux-aarch64.so.1 libc.so.6 libgcrypt.so.20 libgnutls.so.30 libplist.so.3 libpthread.so.0 libtasn1.so.6 libusbmuxd.so.4"
RDEPENDS_libimobiledevice = "glibc gnutls libgcrypt libplist libtasn1 libusbmuxd"
RPM_SONAME_REQ_libimobiledevice-devel = "libimobiledevice.so.6"
RPROVIDES_libimobiledevice-devel = "libimobiledevice-dev (= 1.2.0)"
RDEPENDS_libimobiledevice-devel = "gnutls-devel libimobiledevice libplist-devel libtasn1-devel libusbmuxd-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libimobiledevice-1.2.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libimobiledevice-devel-1.2.0-16.el8.aarch64.rpm \
          "

SRC_URI[libimobiledevice.sha256sum] = "673f6fb2eac4f07427eeceacc4561e4d704b1c926ce7b8d61a0f49183015efac"
SRC_URI[libimobiledevice-devel.sha256sum] = "488a8e8e8587297c1003935630b791b399f8d9eca40540202b4a653f3758dc58"
