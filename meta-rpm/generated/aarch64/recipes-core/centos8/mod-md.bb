SUMMARY = "generated recipe based on mod_md srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "apr apr-util curl expat jansson libxcrypt openssl pkgconfig-native"
RPM_SONAME_REQ_mod_md = "ld-linux-aarch64.so.1 libapr-1.so.0 libaprutil-1.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libexpat.so.1 libjansson.so.4 libpthread.so.0 libssl.so.1.1"
RDEPENDS_mod_md = "apr apr-util expat glibc httpd jansson libcurl libxcrypt mod_ssl openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_md-2.0.8-7.module_el8.2.0+307+4d18d695.aarch64.rpm \
          "

SRC_URI[mod_md.sha256sum] = "9c85c0164b5308ef03e9e1f99dfc9a06d4e192503b3f3826bc90332fe230fafd"
