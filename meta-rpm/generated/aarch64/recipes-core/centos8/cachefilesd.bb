SUMMARY = "generated recipe based on cachefilesd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cachefilesd = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_cachefilesd = "bash glibc selinux-policy-minimum systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cachefilesd-0.10.10-4.el8.aarch64.rpm \
          "

SRC_URI[cachefilesd.sha256sum] = "6cd24a9c5fb1faad6bad59187819f126eb1ba746ea17cd34c3a1593a6cf3de6f"
