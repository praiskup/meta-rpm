SUMMARY = "generated recipe based on ding-libs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libbasicobjects = "libbasicobjects.so.0"
RPM_SONAME_REQ_libbasicobjects = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libbasicobjects = "glibc"
RPM_SONAME_REQ_libbasicobjects-devel = "libbasicobjects.so.0"
RPROVIDES_libbasicobjects-devel = "libbasicobjects-dev (= 0.1.1)"
RDEPENDS_libbasicobjects-devel = "libbasicobjects pkgconf-pkg-config"
RPM_SONAME_PROV_libcollection = "libcollection.so.4"
RPM_SONAME_REQ_libcollection = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libcollection = "glibc"
RPM_SONAME_REQ_libcollection-devel = "libcollection.so.4"
RPROVIDES_libcollection-devel = "libcollection-dev (= 0.7.0)"
RDEPENDS_libcollection-devel = "libcollection pkgconf-pkg-config"
RPM_SONAME_PROV_libdhash = "libdhash.so.1"
RPM_SONAME_REQ_libdhash = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libdhash = "glibc"
RPM_SONAME_PROV_libini_config = "libini_config.so.5"
RPM_SONAME_REQ_libini_config = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libpath_utils.so.1 libref_array.so.1"
RDEPENDS_libini_config = "glibc libbasicobjects libcollection libpath_utils libref_array"
RPM_SONAME_REQ_libini_config-devel = "libini_config.so.5"
RPROVIDES_libini_config-devel = "libini_config-dev (= 1.3.1)"
RDEPENDS_libini_config-devel = "libbasicobjects-devel libcollection-devel libini_config libref_array-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libpath_utils = "libpath_utils.so.1"
RPM_SONAME_REQ_libpath_utils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libpath_utils = "glibc"
RPM_SONAME_REQ_libpath_utils-devel = "libpath_utils.so.1"
RPROVIDES_libpath_utils-devel = "libpath_utils-dev (= 0.2.1)"
RDEPENDS_libpath_utils-devel = "libpath_utils pkgconf-pkg-config"
RPM_SONAME_PROV_libref_array = "libref_array.so.1"
RPM_SONAME_REQ_libref_array = "libc.so.6"
RDEPENDS_libref_array = "glibc"
RPM_SONAME_REQ_libref_array-devel = "libref_array.so.1"
RPROVIDES_libref_array-devel = "libref_array-dev (= 0.1.5)"
RDEPENDS_libref_array-devel = "libref_array pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libbasicobjects-0.1.1-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcollection-0.7.0-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libdhash-0.5.0-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libini_config-1.3.1-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpath_utils-0.2.1-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libref_array-0.1.5-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libbasicobjects-devel-0.1.1-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcollection-devel-0.7.0-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libini_config-devel-1.3.1-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpath_utils-devel-0.2.1-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libref_array-devel-0.1.5-39.el8.aarch64.rpm \
          "

SRC_URI[libbasicobjects.sha256sum] = "fb0c3ae20294a9f49668a4152b6906f399e8856246c0c29458fc682e22c1edbe"
SRC_URI[libbasicobjects-devel.sha256sum] = "0e8f365107d57e3aa1921fa365cc79bc8fcb84dbd3300b36caac6254fac296f3"
SRC_URI[libcollection.sha256sum] = "97e11df69bf2ed6eeb3c93b04d5276216bee58543b5e7ef149d7d82d186f67b5"
SRC_URI[libcollection-devel.sha256sum] = "29e5ef888fca4aad8c4ef7b3da83900b712229233e9c68a459ba1ef0cbeac2b9"
SRC_URI[libdhash.sha256sum] = "84af0a7d5811252b51025e77aba787704793106b1c4c3fc6f281bafe72d9a586"
SRC_URI[libini_config.sha256sum] = "4156413385dce1692e46bc8bc42c74bc6b4f9748230bbd4a2134a17e69ea79d9"
SRC_URI[libini_config-devel.sha256sum] = "a4d208cd41149b06b518766935f1f5e20a9c7517bcc22ad8c22d434d68eb8321"
SRC_URI[libpath_utils.sha256sum] = "6cc569defd57bfde537b5a727ae2413a72e7ed8584be0df30b1613bbf8af1e0a"
SRC_URI[libpath_utils-devel.sha256sum] = "3e7230b4e427bbb5adf68fac2094ddbc1a0b016f66908ed258f8ad9c8c8d27bc"
SRC_URI[libref_array.sha256sum] = "73276d31fe59e80654a4cfbda40edf01a8cfdeffbdfc1a5b4a5ff5fdb898dfca"
SRC_URI[libref_array-devel.sha256sum] = "c809f27197046a0f31165d12b9d3caf0da9151ab4482396a23e3e1174de2785f"
