SUMMARY = "generated recipe based on lohit-odia-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-odia-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-odia-fonts-2.91.2-3.el8.noarch.rpm \
          "

SRC_URI[lohit-odia-fonts.sha256sum] = "f44bc36f2ab0406f0410a7d6013d34c6afb6192e6d89d2ff5a07d1f516b5eccd"
