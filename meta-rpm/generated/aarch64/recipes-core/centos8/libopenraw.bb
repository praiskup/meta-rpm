SUMMARY = "generated recipe based on libopenraw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 libgcc libjpeg-turbo pkgconfig-native"
RPM_SONAME_PROV_libopenraw = "libopenraw.so.7"
RPM_SONAME_REQ_libopenraw = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libjpeg.so.62 libstdc++.so.6"
RDEPENDS_libopenraw = "glibc libgcc libjpeg-turbo libstdc++"
RPM_SONAME_REQ_libopenraw-devel = "libopenraw.so.7"
RPROVIDES_libopenraw-devel = "libopenraw-dev (= 0.1.2)"
RDEPENDS_libopenraw-devel = "libopenraw pkgconf-pkg-config"
RPM_SONAME_PROV_libopenraw-gnome = "libopenrawgnome.so.7"
RPM_SONAME_REQ_libopenraw-gnome = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libopenraw.so.7"
RDEPENDS_libopenraw-gnome = "gdk-pixbuf2 glib2 glibc libopenraw"
RPM_SONAME_REQ_libopenraw-gnome-devel = "libopenrawgnome.so.7"
RPROVIDES_libopenraw-gnome-devel = "libopenraw-gnome-dev (= 0.1.2)"
RDEPENDS_libopenraw-gnome-devel = "libopenraw-devel libopenraw-gnome pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libopenraw-0.1.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libopenraw-devel-0.1.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libopenraw-gnome-0.1.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libopenraw-gnome-devel-0.1.2-4.el8.aarch64.rpm \
          "

SRC_URI[libopenraw.sha256sum] = "da7684bf24c02d6b78d0c0c252f60ff8c3d69b5e857df25daf4a608c71d5d182"
SRC_URI[libopenraw-devel.sha256sum] = "48d54879f61680c9a37a896ce2db6fb4c05259d38a7dafe4180119eec84787ea"
SRC_URI[libopenraw-gnome.sha256sum] = "ca45bc08c12d4646d5ecf0d0bd25b32ae6bdade20c62f19e5da71b0fcac42950"
SRC_URI[libopenraw-gnome-devel.sha256sum] = "f898fcdfc2b5e497a6a0544599e3013b8f002c71aff5f17b5bbda99f5b4046bd"
