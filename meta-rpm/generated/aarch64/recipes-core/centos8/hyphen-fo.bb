SUMMARY = "generated recipe based on hyphen-fo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-fo = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-fo-0.20040420-13.el8.noarch.rpm \
          "

SRC_URI[hyphen-fo.sha256sum] = "81ce5ee7f7574bdf5d87c52a813ef71e92c8851ad7542ab3a5e6b870e08faecb"
