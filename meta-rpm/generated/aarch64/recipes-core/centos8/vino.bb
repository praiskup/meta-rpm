SUMMARY = "generated recipe based on vino srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk avahi avahi-libs cairo gdk-pixbuf glib-2.0 gnutls gtk+3 libgcrypt libgpg-error libice libnotify libsecret libsm libx11 libxdamage libxext libxfixes libxtst pango pkgconfig-native zlib"
RPM_SONAME_REQ_vino = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXtst.so.6 libatk-1.0.so.0 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libresolv.so.2 libsecret-1.so.0 libz.so.1"
RDEPENDS_vino = "atk avahi-glib avahi-libs bash cairo cairo-gobject dbus gdk-pixbuf2 glib2 glibc gnutls gtk3 libICE libSM libX11 libXdamage libXext libXfixes libXtst libgcrypt libgpg-error libnotify libsecret pango systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vino-3.22.0-10.el8.aarch64.rpm \
          "

SRC_URI[vino.sha256sum] = "e4b1ab03b8b9c0d73d9007a1b668a864d5c23e16ca772dd0c1ab28627d18a8f7"
