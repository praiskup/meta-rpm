SUMMARY = "generated recipe based on prefixdevname srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_prefixdevname = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 libudev.so.1"
RDEPENDS_prefixdevname = "bash glibc libgcc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/prefixdevname-0.1.0-6.el8.aarch64.rpm \
          "

SRC_URI[prefixdevname.sha256sum] = "a8bc6c8653eaede6f46620da42f99ee5d511f161e78f2719aa231c75cf06cd15"
