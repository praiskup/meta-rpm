SUMMARY = "generated recipe based on libEMF srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libEMF = "libEMF.so.1"
RPM_SONAME_REQ_libEMF = "ld-linux-aarch64.so.1 libEMF.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libEMF = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libEMF-devel = "libEMF.so.1"
RPROVIDES_libEMF-devel = "libEMF-dev (= 1.0.9)"
RDEPENDS_libEMF-devel = "libEMF libstdc++-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libEMF-1.0.9-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libEMF-devel-1.0.9-5.el8.aarch64.rpm \
          "

SRC_URI[libEMF.sha256sum] = "506e21986338a93b07706b097e0495e0882311a76459b23865b9f186d81933bb"
SRC_URI[libEMF-devel.sha256sum] = "fd19f50682937c696c9258b2cacdec48f677ce25d024ce4cf07c4aeaf41eca20"
