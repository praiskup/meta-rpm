SUMMARY = "generated recipe based on hunspell-sw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sw = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-sw-0.20050819-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-sw.sha256sum] = "a0ce326b0b02bd1760c1dd7315c439ce0de9ec036230c71260454e7e9951e671"
