SUMMARY = "generated recipe based on adwaita-qt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native qt5-qtbase"
RPM_SONAME_REQ_adwaita-qt = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_adwaita-qt = "glibc libgcc libstdc++ qt5-qtbase qt5-qtbase-gui"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/adwaita-qt-1.0-5.el8.aarch64.rpm \
          "

SRC_URI[adwaita-qt.sha256sum] = "21c3a84cb1a2c62e5265691066aefb726fff3767fdf78199ebd790a1dc2fb348"
