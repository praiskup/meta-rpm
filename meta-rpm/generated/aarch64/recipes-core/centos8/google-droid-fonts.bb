SUMMARY = "generated recipe based on google-droid-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_google-droid-kufi-fonts = "fontpackages-filesystem"
RDEPENDS_google-droid-sans-fonts = "fontpackages-filesystem"
RDEPENDS_google-droid-sans-mono-fonts = "fontpackages-filesystem"
RDEPENDS_google-droid-serif-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-droid-kufi-fonts-20120715-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-droid-sans-fonts-20120715-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-droid-sans-mono-fonts-20120715-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-droid-serif-fonts-20120715-13.el8.noarch.rpm \
          "

SRC_URI[google-droid-kufi-fonts.sha256sum] = "4edfc0368dc8044cc022e0c5fe0dd8dc16207478359b75ebd1e6d76a194bc1b4"
SRC_URI[google-droid-sans-fonts.sha256sum] = "16c7e9eab0e217ab274d02c20138f981f821cf57ee1d6b19bcd8f9a5b50b6798"
SRC_URI[google-droid-sans-mono-fonts.sha256sum] = "6c8ecdbfbd3115ee563584b1a15b3ce4f9fbf1dc5efb145699256778bfb32a49"
SRC_URI[google-droid-serif-fonts.sha256sum] = "a33948878c771f7ce53cf1abb41b08b98bbb1795b2a57cfa921ff6f5880a5430"
