SUMMARY = "generated recipe based on gnome-themes-standard srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 pango pkgconfig-native"
RPM_SONAME_PROV_adwaita-gtk2-theme = "libadwaita.so"
RPM_SONAME_REQ_adwaita-gtk2-theme = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_adwaita-gtk2-theme = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 pango"
RDEPENDS_gnome-themes-standard = "abattis-cantarell-fonts adwaita-gtk2-theme adwaita-icon-theme bash"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/adwaita-gtk2-theme-3.22.3-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-themes-standard-3.22.3-4.el8.aarch64.rpm \
          "

SRC_URI[adwaita-gtk2-theme.sha256sum] = "88ca509b8a88bab58a88bea82a4a023689b75d69d96408f7c15beb7a0e93b04a"
SRC_URI[gnome-themes-standard.sha256sum] = "c84d2f7bd0d333f52e19b6b020e0313fedb26a6ee2044f10adf1a0db4b88acf7"
