SUMMARY = "generated recipe based on redfish-finder srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_redfish-finder = "NetworkManager bash dmidecode platform-python python36 systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redfish-finder-0.3-4.el8.noarch.rpm \
          "

SRC_URI[redfish-finder.sha256sum] = "201f1d7d180a72b7064b8bd38c71ea31d31b1b45ce66a1bafdad14c6b5348410"
