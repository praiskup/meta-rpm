SUMMARY = "generated recipe based on diffstat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_diffstat = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_diffstat = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/diffstat-1.61-7.el8.aarch64.rpm \
          "

SRC_URI[diffstat.sha256sum] = "9924e478fe4ec42838b0aaedf48a1162d642b8277cd0e7aa497ab2aa4d88de7d"
