SUMMARY = "generated recipe based on iw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native"
RPM_SONAME_REQ_iw = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libnl-genl-3.so.200"
RDEPENDS_iw = "glibc libnl3"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iw-4.14-5.el8.aarch64.rpm \
          "

SRC_URI[iw.sha256sum] = "13b52668bcb9ddefe9b0bde121824362167bd6e0c3dc2beed398e6e8e65aa08f"
