SUMMARY = "generated recipe based on hunspell-kk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-kk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-kk-1.1-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-kk.sha256sum] = "a6c68941c8aa9242aa9271e9c95c689391c16809c576504127b106208d11d28a"
