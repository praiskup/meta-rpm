SUMMARY = "generated recipe based on man-pages-overrides srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/man-pages-overrides-8.2.0.2-1.el8.noarch.rpm \
          "

SRC_URI[man-pages-overrides.sha256sum] = "bd95e3803014c672a0bcf022745dc36bab477e78f154e8a49eac72aa6589b0ab"
