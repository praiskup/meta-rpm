SUMMARY = "generated recipe based on libpng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconf pkgconfig-native zlib"
RPM_SONAME_PROV_libpng = "libpng16.so.16"
RPM_SONAME_REQ_libpng = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libz.so.1"
RDEPENDS_libpng = "glibc zlib"
RPM_SONAME_REQ_libpng-devel = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpng16.so.16 libz.so.1"
RPROVIDES_libpng-devel = "libpng-dev (= 1.6.34)"
RDEPENDS_libpng-devel = "bash glibc libpng pkgconf-pkg-config zlib zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpng-1.6.34-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpng-devel-1.6.34-5.el8.aarch64.rpm \
          "

SRC_URI[libpng.sha256sum] = "d7bd4e7a7ff4424266c0f6030bf444de0bea88d0540ff4caf4f7f6c2bac175f6"
SRC_URI[libpng-devel.sha256sum] = "86b0707915c617b9e3ca3f432530701ddbd46331cb6d5d4ee9d726ca98a82be8"
