SUMMARY = "generated recipe based on qt5-qtsvg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase zlib"
RPM_SONAME_PROV_qt5-qtsvg = "libQt5Svg.so.5 libqsvg.so libqsvgicon.so"
RPM_SONAME_REQ_qt5-qtsvg = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qtsvg = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui zlib"
RPM_SONAME_REQ_qt5-qtsvg-devel = "libQt5Svg.so.5"
RPROVIDES_qt5-qtsvg-devel = "qt5-qtsvg-dev (= 5.12.5)"
RDEPENDS_qt5-qtsvg-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtsvg"
RPM_SONAME_REQ_qt5-qtsvg-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtsvg-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtsvg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtsvg-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtsvg-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtsvg-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtsvg.sha256sum] = "8614a07ec69fb92a0d03b4e0697ea193c87d6685ad0a26e84841d925b0c5312f"
SRC_URI[qt5-qtsvg-devel.sha256sum] = "9a4394fcc717bb9e981b4ab6ae034052f9c9e458c3c6735592f96432ccc4192a"
SRC_URI[qt5-qtsvg-examples.sha256sum] = "761d6d4c21f1b848b75a932fd3cab2d00d5d481d9318b80ca4b417a1e6c25dc1"
