SUMMARY = "generated recipe based on perl-Storable srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Storable = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Storable = "glibc perl-Carp perl-Exporter perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Storable-3.11-3.el8.aarch64.rpm \
          "

SRC_URI[perl-Storable.sha256sum] = "96b57a47850079d963284eabd72b9d8fc034a072cda2d0b396f36aa7955bf766"
