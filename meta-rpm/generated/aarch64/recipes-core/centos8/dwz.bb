SUMMARY = "generated recipe based on dwz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_dwz = "ld-linux-aarch64.so.1 libc.so.6 libelf.so.1"
RDEPENDS_dwz = "elfutils-libelf glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dwz-0.12-9.el8.aarch64.rpm \
          "

SRC_URI[dwz.sha256sum] = "9f49b975cc9cb2326bf6315369fd8ae0aaad79d6954f2825b5a6e31b79302ae4"
