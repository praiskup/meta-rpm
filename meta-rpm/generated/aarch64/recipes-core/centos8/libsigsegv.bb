SUMMARY = "generated recipe based on libsigsegv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsigsegv = "libsigsegv.so.2"
RPM_SONAME_REQ_libsigsegv = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsigsegv = "glibc"
RPM_SONAME_REQ_libsigsegv-devel = "libsigsegv.so.2"
RPROVIDES_libsigsegv-devel = "libsigsegv-dev (= 2.11)"
RDEPENDS_libsigsegv-devel = "libsigsegv"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsigsegv-2.11-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsigsegv-devel-2.11-5.el8.aarch64.rpm \
          "

SRC_URI[libsigsegv.sha256sum] = "b377f4e8bcdc750ed0be94f97bdbfbb12843c458fbc1d5d507f92ad04aaf592b"
SRC_URI[libsigsegv-devel.sha256sum] = "29f0e9ec85fd8fc8f1bfb005e5f525d5bcfc1ded7350fbf8c27404ab823e225d"
