SUMMARY = "generated recipe based on jaxen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jaxen = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jaxen-demo = "jaxen"
RDEPENDS_jaxen-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jaxen-1.1.6-18.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jaxen-demo-1.1.6-18.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jaxen-javadoc-1.1.6-18.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jaxen.sha256sum] = "426e92292340a8d5d6a3c85b71a4eb69899aa7b00029ff32422dc92168871c33"
SRC_URI[jaxen-demo.sha256sum] = "28f1798a35a6c7a2ad475aa12f8d4d448e76b83f1dcfc25d99111df3d96cdd32"
SRC_URI[jaxen-javadoc.sha256sum] = "9e9a8402c686400a789c61bc243304f6443e1798f648cef4e4a027a98c4a7eb0"
