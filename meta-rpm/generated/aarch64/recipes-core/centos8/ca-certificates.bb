SUMMARY = "generated recipe based on ca-certificates srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ca-certificates = "bash coreutils grep p11-kit p11-kit-trust sed"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ca-certificates-2020.2.41-80.0.el8_2.noarch.rpm \
          "

SRC_URI[ca-certificates.sha256sum] = "dc984aefb28c2d11ff6f6f1a794d04d300744ea0cfc9b869368f2f1acfc419be"
