SUMMARY = "generated recipe based on ocl-icd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_ocl-icd = "libOpenCL.so.1"
RPM_SONAME_REQ_ocl-icd = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_ocl-icd = "glibc"
RPM_SONAME_REQ_ocl-icd-devel = "libOpenCL.so.1"
RPROVIDES_ocl-icd-devel = "ocl-icd-dev (= 2.2.12)"
RDEPENDS_ocl-icd-devel = "ocl-icd opencl-headers pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ocl-icd-2.2.12-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocl-icd-devel-2.2.12-1.el8.aarch64.rpm \
          "

SRC_URI[ocl-icd.sha256sum] = "2b1c7d55eaf95363ca9cd84464c19f727ca06432be60812900314e84d3a5954b"
SRC_URI[ocl-icd-devel.sha256sum] = "24f11478093d3dc635ef5bd0475db6552fd1743f25eb4b4fc662d683d8df1291"
