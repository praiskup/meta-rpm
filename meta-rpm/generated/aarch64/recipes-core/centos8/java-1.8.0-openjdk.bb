SUMMARY = "generated recipe based on java-1.8.0-openjdk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib freetype giflib libgcc libjpeg-turbo libpng libx11 libxext libxi libxrender libxtst pkgconfig-native zlib"
RPM_SONAME_PROV_java-1.8.0-openjdk = "libjawt.so"
RPM_SONAME_REQ_java-1.8.0-openjdk = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXi.so.6 libXrender.so.1 libXtst.so.6 libasound.so.2 libc.so.6 libdl.so.2 libgif.so.7 libjava.so libjpeg.so.62 libjvm.so libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_java-1.8.0-openjdk = "alsa-lib bash fontconfig giflib glibc java-1.8.0-openjdk-headless libX11 libXcomposite libXext libXi libXrender libXtst libjpeg-turbo libpng xorg-x11-fonts-Type1 zlib"
RDEPENDS_java-1.8.0-openjdk-accessibility = "java-1.8.0-openjdk java-atk-wrapper"
RPM_SONAME_PROV_java-1.8.0-openjdk-demo = "libcompiledMethodLoad.so libgctest.so libheapTracker.so libheapViewer.so libminst.so libmtrace.so libversionCheck.so libwaiters.so"
RPM_SONAME_REQ_java-1.8.0-openjdk-demo = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_java-1.8.0-openjdk-demo = "bash glibc java-1.8.0-openjdk libgcc libstdc++"
RPM_SONAME_PROV_java-1.8.0-openjdk-devel = "libjawt.so"
RPM_SONAME_REQ_java-1.8.0-openjdk-devel = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libdl.so.2 libjava.so libjvm.so libpthread.so.0 libstdc++.so.6 libz.so.1"
RPROVIDES_java-1.8.0-openjdk-devel = "java-1.8.0-openjdk-dev (= 1.8.0.272.b10)"
RDEPENDS_java-1.8.0-openjdk-devel = "bash chkconfig glibc java-1.8.0-openjdk java-1.8.0-openjdk-headless libX11 libstdc++ zlib"
RPM_SONAME_PROV_java-1.8.0-openjdk-headless = "libjava.so libjsig.so libjvm.so libverify.so"
RPM_SONAME_REQ_java-1.8.0-openjdk-headless = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libfreetype.so.6 libgcc_s.so.1 libjava.so libjpeg.so.62 libjvm.so libm.so.6 libpthread.so.0 libstdc++.so.6 libthread_db.so.1 libverify.so libz.so.1"
RDEPENDS_java-1.8.0-openjdk-headless = "bash ca-certificates chkconfig copy-jdk-configs cups-libs freetype glibc javapackages-filesystem libgcc libjpeg-turbo libstdc++ lksctp-tools tzdata-java zlib"
RDEPENDS_java-1.8.0-openjdk-javadoc = "bash chkconfig javapackages-filesystem"
RDEPENDS_java-1.8.0-openjdk-javadoc-zip = "bash chkconfig javapackages-filesystem"
RDEPENDS_java-1.8.0-openjdk-src = "java-1.8.0-openjdk-headless"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-1.8.0.272.b10-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-accessibility-1.8.0.272.b10-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-demo-1.8.0.272.b10-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-devel-1.8.0.272.b10-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-headless-1.8.0.272.b10-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-javadoc-1.8.0.272.b10-1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-javadoc-zip-1.8.0.272.b10-1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/java-1.8.0-openjdk-src-1.8.0.272.b10-1.el8_2.aarch64.rpm \
          "

SRC_URI[java-1.8.0-openjdk.sha256sum] = "a647b9a27d3919f443997930c6acbd69a26b441d10c2a2ceb16cc06be0cfc94f"
SRC_URI[java-1.8.0-openjdk-accessibility.sha256sum] = "b70a9a8e775235dd241aa15645c3f3db74cfa91ad4db62b0a4c1d7c4798b77be"
SRC_URI[java-1.8.0-openjdk-demo.sha256sum] = "a285a2c0eed557482ac79185e8e6f07f45b7b238e3a0f86177beb5453665342e"
SRC_URI[java-1.8.0-openjdk-devel.sha256sum] = "3dc4e49358f14b6b767c6b27d89f17ded2669d23615225ece7eb5bb19e3f53a6"
SRC_URI[java-1.8.0-openjdk-headless.sha256sum] = "2ec40bbb9bb1735c4f61852562eae056d0a63c2d1245534f544f684f6d65db86"
SRC_URI[java-1.8.0-openjdk-javadoc.sha256sum] = "843dc8aa7d833b8dd2288f84af54f41c98efdaeea53f66f4c308d09a03c518fd"
SRC_URI[java-1.8.0-openjdk-javadoc-zip.sha256sum] = "1a0ba17e904a79911620fb58cffc344c3a65c16cb848c9a00251847982658a8a"
SRC_URI[java-1.8.0-openjdk-src.sha256sum] = "853a4c558a32a4f274c2573f0258c7364e27de0874c05769a0010fa0066108ca"
