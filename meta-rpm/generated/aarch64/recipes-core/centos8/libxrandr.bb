SUMMARY = "generated recipe based on libXrandr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxrender pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXrandr = "libXrandr.so.2"
RPM_SONAME_REQ_libXrandr = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6"
RDEPENDS_libXrandr = "glibc libX11 libXext libXrender"
RPM_SONAME_REQ_libXrandr-devel = "libXrandr.so.2"
RPROVIDES_libXrandr-devel = "libXrandr-dev (= 1.5.1)"
RDEPENDS_libXrandr-devel = "libX11-devel libXext-devel libXrandr libXrender-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXrandr-1.5.1-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXrandr-devel-1.5.1-7.el8.aarch64.rpm \
          "

SRC_URI[libXrandr.sha256sum] = "a82383fa6f2c462fe20374bebae2b62e58e7840961f89d0991d32da1b8f2392e"
SRC_URI[libXrandr-devel.sha256sum] = "47cac3211a5a19595a3f2969e2cf0091e73f1763c2ea5496da83d86abf7bfda0"
