SUMMARY = "generated recipe based on subscription-manager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-c libdnf librepo openssl pkgconfig-native rpm sqlite3 zlib"
RPM_SONAME_REQ_dnf-plugin-subscription-manager = "libc.so.6 libcrypto.so.1.1 libdnf.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 librepo.so.0 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_dnf-plugin-subscription-manager = "dnf glib2 glibc json-c libdnf librepo openssl-libs platform-python python3-dnf-plugins-core python3-librepo rpm-libs sqlite-libs zlib"
RDEPENDS_python3-syspurpose = "platform-python"
RDEPENDS_rhsm-gtk = "gtk3 librsvg2 platform-python python3-gobject rarian-compat rhsm-icons usermode-gtk"
RPM_SONAME_REQ_subscription-manager = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0"
RDEPENDS_subscription-manager = "bash dnf-plugin-subscription-manager glib2 glibc platform-python platform-python-setuptools python3-dateutil python3-dbus python3-decorator python3-ethtool python3-gobject-base python3-iniparse python3-inotify python3-six python3-subscription-manager-rhsm python3-syspurpose systemd usermode virt-what"
RDEPENDS_subscription-manager-cockpit = "cockpit-bridge cockpit-system cockpit-ws rhsm-icons subscription-manager"
RDEPENDS_subscription-manager-initial-setup-addon = "initial-setup-gui rhsm-gtk"
RDEPENDS_subscription-manager-migration = "platform-python subscription-manager subscription-manager-migration-data"
RDEPENDS_subscription-manager-plugin-ostree = "platform-python python3-gobject-base python3-iniparse subscription-manager"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rhsm-gtk-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/subscription-manager-initial-setup-addon-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/subscription-manager-migration-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dnf-plugin-subscription-manager-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-syspurpose-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rhsm-icons-1.26.20-1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/subscription-manager-1.26.20-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/subscription-manager-cockpit-1.26.20-1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/subscription-manager-plugin-ostree-1.26.20-1.el8_2.aarch64.rpm \
          "

SRC_URI[dnf-plugin-subscription-manager.sha256sum] = "8a5c1daea8beddabe630dc8c3ae93b1f303e11ccd3340219a5744ae36104a2d8"
SRC_URI[python3-syspurpose.sha256sum] = "31f6fef28be04653a683409832b99f779a95f6df89406612fc1221e805eb1c2f"
SRC_URI[rhsm-gtk.sha256sum] = "7f876d060065afa11c610bf343c217e534599367cebf789c5cd7a0d4a0e059bb"
SRC_URI[rhsm-icons.sha256sum] = "d9b35dfb9ed2cba066c711c522758b83372c8b47c09cb26498c411d35ea3b7a6"
SRC_URI[subscription-manager.sha256sum] = "cce2a110a69c048c3481c3d66f0ccc9ddaaa834de89a5ba0d172fcc14b148da8"
SRC_URI[subscription-manager-cockpit.sha256sum] = "c63098a1ee8701758f070df20fd3ca899e820605a9e5c481df5f5f9fcd5da41e"
SRC_URI[subscription-manager-initial-setup-addon.sha256sum] = "a75fded78860ed664bca44b3f7712e0186a7e162f00df028da900da446ecc07f"
SRC_URI[subscription-manager-migration.sha256sum] = "80e4b3016e202709ec8fbc8a16a71ae988f70a59c1a733a9ea10519e8c7e90df"
SRC_URI[subscription-manager-plugin-ostree.sha256sum] = "0ce6919a49c969a7de498a528c3c92323094c858dfedf1ae10f5cef9d6756b15"
