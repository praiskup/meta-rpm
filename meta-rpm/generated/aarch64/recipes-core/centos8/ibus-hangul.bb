SUMMARY = "generated recipe based on ibus-hangul srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 ibus libhangul pkgconfig-native"
RPM_SONAME_REQ_ibus-hangul = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libhangul.so.1 libibus-1.0.so.5 libpthread.so.0"
RDEPENDS_ibus-hangul = "bash glib2 glibc ibus ibus-libs libhangul platform-python python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-hangul-1.5.1-4.el8.aarch64.rpm \
          "

SRC_URI[ibus-hangul.sha256sum] = "cdbf432ea199f5df3e3a8a184550b71b478e7c243f786cbcd70487a3694e8b4f"
