SUMMARY = "generated recipe based on libxshmfence srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libxshmfence = "libxshmfence.so.1"
RPM_SONAME_REQ_libxshmfence = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libxshmfence = "glibc"
RPM_SONAME_REQ_libxshmfence-devel = "libxshmfence.so.1"
RPROVIDES_libxshmfence-devel = "libxshmfence-dev (= 1.3)"
RDEPENDS_libxshmfence-devel = "libxshmfence pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxshmfence-1.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxshmfence-devel-1.3-2.el8.aarch64.rpm \
          "

SRC_URI[libxshmfence.sha256sum] = "2accc06ffb9511a1e54e05263847fc7e29f1b0b58997bc58d28318be1ee448f1"
SRC_URI[libxshmfence-devel.sha256sum] = "0c65d58d673fe49d6323ed9cb844b272918494944ec665c2304f3acb224cf8ae"
