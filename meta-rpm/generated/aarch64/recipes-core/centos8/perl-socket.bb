SUMMARY = "generated recipe based on perl-Socket srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Socket = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Socket = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Socket-2.027-3.el8.aarch64.rpm \
          "

SRC_URI[perl-Socket.sha256sum] = "1dc66066521354428e0da1a9b93b3db292ac1f77b0efc060c33f3c5760440e47"
