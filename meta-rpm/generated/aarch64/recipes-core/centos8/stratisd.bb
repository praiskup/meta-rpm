SUMMARY = "generated recipe based on stratisd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs libgcc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_stratisd = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libudev.so.1"
RDEPENDS_stratisd = "bash dbus-libs device-mapper-persistent-data glibc libgcc systemd-libs xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/stratisd-2.0.0-4.el8.aarch64.rpm \
          "

SRC_URI[stratisd.sha256sum] = "cb6a53aace57e1a69bb353291c49cb7bd8121db297431b16a57c462017bd111d"
