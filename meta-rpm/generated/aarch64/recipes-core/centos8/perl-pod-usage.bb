SUMMARY = "generated recipe based on perl-Pod-Usage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Usage = "perl-Carp perl-Exporter perl-Getopt-Long perl-PathTools perl-Pod-Perldoc perl-interpreter perl-libs perl-podlators"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Pod-Usage-1.69-395.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Usage.sha256sum] = "794f970f498af07b37f914c19ad5dedc6b6c2f89d343af9dd1768d17232555de"
