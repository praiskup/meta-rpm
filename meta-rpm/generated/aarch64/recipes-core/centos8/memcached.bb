SUMMARY = "generated recipe based on memcached srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib libevent pkgconfig-native"
RPM_SONAME_REQ_memcached = "ld-linux-aarch64.so.1 libc.so.6 libevent-2.1.so.6 libpthread.so.0 libsasl2.so.3"
RDEPENDS_memcached = "bash cyrus-sasl-lib glibc libevent perl-IO perl-interpreter perl-libs shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/memcached-1.5.9-3.el8.aarch64.rpm \
          "

SRC_URI[memcached.sha256sum] = "8754d4d5bd3934a884a3990580f041b80e35dbf93ad98541a4561b5dd732b0be"
