SUMMARY = "generated recipe based on jbigkit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_jbigkit-devel = "libjbig.so.2.1 libjbig85.so.2.1"
RPROVIDES_jbigkit-devel = "jbigkit-dev (= 2.1)"
RDEPENDS_jbigkit-devel = "jbigkit-libs"
RPM_SONAME_PROV_jbigkit-libs = "libjbig.so.2.1 libjbig85.so.2.1"
RPM_SONAME_REQ_jbigkit-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_jbigkit-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jbigkit-libs-2.1-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jbigkit-devel-2.1-14.el8.aarch64.rpm \
          "

SRC_URI[jbigkit-devel.sha256sum] = "cda585dbb261a3c8b772ec3dc5cc6eafb053e8860d02ad20919efec8874b3eaf"
SRC_URI[jbigkit-libs.sha256sum] = "3b96c1d19aa0ccf4153f26cc4969906daa06c99b8af62df18b85999b05004b90"
