SUMMARY = "generated recipe based on libfastjson srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libfastjson = "libfastjson.so.4"
RPM_SONAME_REQ_libfastjson = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libfastjson = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libfastjson-0.99.8-2.el8.aarch64.rpm \
          "

SRC_URI[libfastjson.sha256sum] = "413890b3d1d8535c08a8aab6afc1437283867f88c0f945ff9d1e664c0855d700"
