SUMMARY = "generated recipe based on mailx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "krb5-libs openssl pkgconfig-native"
RPM_SONAME_REQ_mailx = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgssapi_krb5.so.2 libssl.so.1.1"
RDEPENDS_mailx = "bash glibc krb5-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mailx-12.5-29.el8.aarch64.rpm \
          "

SRC_URI[mailx.sha256sum] = "d694aea8513fc57a647ba47d4840c7eaea710ff07448c08b95a3ebd573edde86"
