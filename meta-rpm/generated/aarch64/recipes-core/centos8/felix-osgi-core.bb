SUMMARY = "generated recipe based on felix-osgi-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_felix-osgi-core = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_felix-osgi-core-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-osgi-core-1.4.0-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-osgi-core-javadoc-1.4.0-23.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[felix-osgi-core.sha256sum] = "98fc87a5a76387c24ba46e41ec6eed0b907aeaee52b2833afd4fdf188ff7fcd1"
SRC_URI[felix-osgi-core-javadoc.sha256sum] = "d604c3cb8bcf2748549b134a9fdf229403a41a7d444a56c6e05cc90b8389aa8f"
