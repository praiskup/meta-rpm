SUMMARY = "generated recipe based on pinentry srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gcr gdk-pixbuf glib-2.0 gtk+3 gtk2 libassuan libgpg-error libsecret ncurses p11-kit pango pkgconfig-native"
RPM_SONAME_REQ_pinentry = "ld-linux-aarch64.so.1 libassuan.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libncursesw.so.6 libsecret-1.so.0 libtinfo.so.6"
RDEPENDS_pinentry = "bash chkconfig glib2 glibc info libassuan libgpg-error libsecret ncurses-libs"
RPM_SONAME_REQ_pinentry-emacs = "ld-linux-aarch64.so.1 libassuan.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libsecret-1.so.0"
RDEPENDS_pinentry-emacs = "glib2 glibc libassuan libgpg-error libsecret pinentry"
RPM_SONAME_REQ_pinentry-gnome3 = "ld-linux-aarch64.so.1 libassuan.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libncursesw.so.6 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libsecret-1.so.0 libtinfo.so.6"
RDEPENDS_pinentry-gnome3 = "atk cairo cairo-gobject gcr gdk-pixbuf2 glib2 glibc gtk3 libassuan libgpg-error libsecret ncurses-libs p11-kit pango pinentry"
RPM_SONAME_REQ_pinentry-gtk = "ld-linux-aarch64.so.1 libassuan.so.0 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-x11-2.0.so.0 libncursesw.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libsecret-1.so.0 libtinfo.so.6"
RDEPENDS_pinentry-gtk = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libassuan libgpg-error libsecret ncurses-libs pango pinentry"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pinentry-1.1.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pinentry-emacs-1.1.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pinentry-gnome3-1.1.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pinentry-gtk-1.1.0-2.el8.aarch64.rpm \
          "

SRC_URI[pinentry.sha256sum] = "cd55ba5d97f3de548d5fa7e14b9ff2924300f620dfc8013d55d8b2e031c36d36"
SRC_URI[pinentry-emacs.sha256sum] = "aca851ef0f963bff29bfeb3a3ab83b02d8d09d30cda65c2656b6200ef173e4c0"
SRC_URI[pinentry-gnome3.sha256sum] = "7d575b5f4e218e04e6a72479d09447e0ed14fb3790df767615406c5420aa926b"
SRC_URI[pinentry-gtk.sha256sum] = "8fce3450aaae3497a70dbcd9f0f5fa5b6dfd1e48afff74a97a99e0b1436ab1c7"
