SUMMARY = "generated recipe based on libgnomekbd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libx11 libxklavier pango pkgconfig-native"
RPM_SONAME_PROV_libgnomekbd = "libgnomekbd.so.8 libgnomekbdui.so.8"
RPM_SONAME_REQ_libgnomekbd = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnomekbd.so.8 libgnomekbdui.so.8 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libxklavier.so.16"
RDEPENDS_libgnomekbd = "GConf2 atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libxklavier pango"
RPM_SONAME_REQ_libgnomekbd-devel = "libgnomekbd.so.8 libgnomekbdui.so.8"
RPROVIDES_libgnomekbd-devel = "libgnomekbd-dev (= 3.26.0)"
RDEPENDS_libgnomekbd-devel = "glib2-devel gtk3-devel libgnomekbd libxklavier-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgnomekbd-3.26.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgnomekbd-devel-3.26.0-4.el8.aarch64.rpm \
          "

SRC_URI[libgnomekbd.sha256sum] = "3d4eb137bed676a552a4c35bfe2d23d5d36b1dae655ef1725b1d244281acca12"
SRC_URI[libgnomekbd-devel.sha256sum] = "309153bbb87fda28c11154c0fc376d0d70ded3a4a628ee064396a1aa8671065e"
