SUMMARY = "generated recipe based on mtr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 ncurses pango pkgconfig-native"
RPM_SONAME_REQ_mtr = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libncurses.so.6 libresolv.so.2 libtinfo.so.6"
RDEPENDS_mtr = "glibc ncurses-libs"
RPM_SONAME_REQ_mtr-gtk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libncurses.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libresolv.so.2 libtinfo.so.6"
RDEPENDS_mtr-gtk = "atk bash cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 mtr ncurses-libs pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mtr-gtk-0.92-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mtr-0.92-3.el8.aarch64.rpm \
          "

SRC_URI[mtr.sha256sum] = "ae3803683598b8fc584feeaa595e916d20fe69c1f90a676eeae261252a5101ea"
SRC_URI[mtr-gtk.sha256sum] = "4c751d74a035eeca7fce291602d4135c67bc2df64bb0c619d4e54965c9998e51"
