SUMMARY = "generated recipe based on cyrus-sasl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db e2fsprogs krb5-libs libxcrypt pkgconfig-native"
RPM_SONAME_REQ_cyrus-sasl-devel = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libsasl2.so.3"
RPROVIDES_cyrus-sasl-devel = "cyrus-sasl-dev (= 2.1.27)"
RDEPENDS_cyrus-sasl-devel = "cyrus-sasl cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt pkgconf-pkg-config"
RPM_SONAME_PROV_cyrus-sasl-lib = "libanonymous.so.3 libsasl2.so.3 libsasldb.so.3"
RPM_SONAME_REQ_cyrus-sasl-lib = "ld-linux-aarch64.so.1 libanonymous.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libdb-5.3.so libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libsasl2.so.3 libsasldb.so.3"
RDEPENDS_cyrus-sasl-lib = "glibc krb5-libs libcom_err libdb libxcrypt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-devel-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-lib-2.1.27-1.el8.aarch64.rpm \
          "

SRC_URI[cyrus-sasl-devel.sha256sum] = "7c53b8d66222778b7558c1fb814b51439f759a6f54a906878587bb43c3297ce5"
SRC_URI[cyrus-sasl-lib.sha256sum] = "fbf3b3a288f9fd9787280a467199e9ffe188e1d1927ec7de04dc4c57eb6c28c8"
