SUMMARY = "generated recipe based on ltrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libselinux pkgconfig-native"
RPM_SONAME_REQ_ltrace = "ld-linux-aarch64.so.1 libc.so.6 libdw.so.1 libelf.so.1 libselinux.so.1 libstdc++.so.6"
RDEPENDS_ltrace = "elfutils-libelf elfutils-libs glibc libselinux libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ltrace-0.7.91-28.el8.aarch64.rpm \
          "

SRC_URI[ltrace.sha256sum] = "f83e3c5f789c1eef67f73c3963f85b36bc000caab61cbb5850a8daaa3af6dbed"
