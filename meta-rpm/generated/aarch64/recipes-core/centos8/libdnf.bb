SUMMARY = "generated recipe based on libdnf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gpgme json-c libgcc libgpg-error libmodulemd librepo libsolv openssl pkgconfig-native platform-python3 rpm sqlite3 util-linux"
RPM_SONAME_PROV_libdnf = "libdnf.so.2"
RPM_SONAME_REQ_libdnf = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libm.so.6 libmodulemd.so.1 librepo.so.0 librpm.so.8 librpmio.so.8 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_libdnf = "glib2 glibc gpgme json-c libgcc libgpg-error libmodulemd libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs rpm-libs sqlite-libs"
RPM_SONAME_REQ_python3-hawkey = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdnf.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libm.so.6 libmodulemd.so.1 libpython3.6m.so.1.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_python3-hawkey = "glib2 glibc gpgme json-c libdnf libgcc libgpg-error libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs platform-python python3-libdnf python3-libs rpm-libs sqlite-libs"
RPM_SONAME_REQ_python3-libdnf = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdnf.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libm.so.6 libmodulemd.so.1 libpython3.6m.so.1.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_python3-libdnf = "glib2 glibc gpgme json-c libdnf libgcc libgpg-error libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs platform-python python3-libs rpm-libs sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libdnf-0.39.1-6.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-hawkey-0.39.1-6.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libdnf-0.39.1-6.el8_2.aarch64.rpm \
          "

SRC_URI[libdnf.sha256sum] = "5db5f176cf01caa4b9948df3b2b83db8f89b34760505e866e1d5e39fc9fb9ff1"
SRC_URI[python3-hawkey.sha256sum] = "4943cfc08b10a503063e7682fec83cf32e4a5a278bcbe64698a6d54c7e18cffb"
SRC_URI[python3-libdnf.sha256sum] = "cb0338a2a33370288678f834118803fd50514a05af095d62361bbcda4fd9d751"
