SUMMARY = "generated recipe based on pacemaker srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 corosync dbus-libs glib-2.0 gnutls libqb libuuid libxml2 libxslt ncurses pam pkgconfig-native"
RPM_SONAME_PROV_pacemaker-cluster-libs = "libcrmcluster.so.29"
RPM_SONAME_REQ_pacemaker-cluster-libs = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libcfg.so.7 libcmap.so.4 libcorosync_common.so.4 libcpg.so.4 libcrmcommon.so.34 libcrmservice.so.28 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libgnutls.so.30 libpam.so.0 libqb.so.0 libquorum.so.5 librt.so.1 libstonithd.so.26 libuuid.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_pacemaker-cluster-libs = "bzip2-libs corosynclib dbus-libs glib2 glibc gnutls libqb libuuid libxml2 libxslt pacemaker-libs pam"
RPM_SONAME_PROV_pacemaker-libs = "libcib.so.27 libcrmcommon.so.34 libcrmservice.so.28 liblrmd.so.28 libpacemaker.so.1 libpe_rules.so.26 libpe_status.so.28 libstonithd.so.26"
RPM_SONAME_REQ_pacemaker-libs = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libcib.so.27 libcrmcommon.so.34 libcrmservice.so.28 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libgnutls.so.30 liblrmd.so.28 libncurses.so.6 libpam.so.0 libpe_rules.so.26 libpe_status.so.28 libqb.so.0 librt.so.1 libstonithd.so.26 libtinfo.so.6 libuuid.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_pacemaker-libs = "bash bzip2-libs dbus-libs glib2 glibc gnutls libqb libuuid libxml2 libxslt ncurses-libs pacemaker-schemas pam shadow-utils"
RDEPENDS_pacemaker-schemas = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pacemaker-cluster-libs-2.0.3-5.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pacemaker-libs-2.0.3-5.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pacemaker-schemas-2.0.3-5.el8_2.1.noarch.rpm \
          "

SRC_URI[pacemaker-cluster-libs.sha256sum] = "6268e504dec3de3dd010cf8d9a85bc5b8b963410f75b4f4081eafc22ce575b79"
SRC_URI[pacemaker-libs.sha256sum] = "242e8699377abea41441b41e4749379d4916cf3eeefd8d11c26e480b42b658c0"
SRC_URI[pacemaker-schemas.sha256sum] = "553c7049b0294990e7f70fdae27d867cc7277f3729c6520cf3c5cb1fb02d74e8"
