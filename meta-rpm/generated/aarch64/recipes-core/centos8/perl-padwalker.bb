SUMMARY = "generated recipe based on perl-PadWalker srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-PadWalker = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-PadWalker = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-PadWalker-2.3-2.el8.aarch64.rpm \
          "

SRC_URI[perl-PadWalker.sha256sum] = "d4cc94cf4f7e0e05a5114e663e617f659b890e78bdd1b39cad254f53556f52af"
