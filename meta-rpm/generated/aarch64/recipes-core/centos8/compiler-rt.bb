SUMMARY = "generated recipe based on compiler-rt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_compiler-rt = "libclang_rt.asan-aarch64.so libclang_rt.hwasan-aarch64.so libclang_rt.scudo-aarch64.so libclang_rt.scudo_minimal-aarch64.so libclang_rt.ubsan_minimal-aarch64.so libclang_rt.ubsan_standalone-aarch64.so"
RPM_SONAME_REQ_compiler-rt = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_compiler-rt = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/compiler-rt-9.0.1-2.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
          "

SRC_URI[compiler-rt.sha256sum] = "bbf0537f1cd7683e21a30a66cf0689ad3f0f62c469bdd9df3e5e2b316435e331"
