SUMMARY = "generated recipe based on iproute srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db elfutils iptables libmnl libselinux pkgconfig-native"
RPM_SONAME_REQ_iproute = "ld-linux-aarch64.so.1 libc.so.6 libdb-5.3.so libdl.so.2 libelf.so.1 libm.so.6 libmnl.so.0 libselinux.so.1"
RDEPENDS_iproute = "bash elfutils-libelf glibc libdb libmnl libselinux"
RPROVIDES_iproute-devel = "iproute-dev (= 5.3.0)"
RDEPENDS_iproute-devel = "iproute"
RPM_SONAME_REQ_iproute-tc = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libelf.so.1 libm.so.6 libmnl.so.0 libselinux.so.1 libxtables.so.12"
RDEPENDS_iproute-tc = "bash elfutils-libelf glibc iproute iptables-libs libmnl libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iproute-5.3.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iproute-tc-5.3.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/iproute-devel-5.3.0-1.el8.aarch64.rpm \
          "

SRC_URI[iproute.sha256sum] = "dea582ce420386d66a80aa316accfa9f3e9b5628d87b6c30272f7e80e2dce222"
SRC_URI[iproute-devel.sha256sum] = "6b75bbbd611bec96e850146c6067d953b800b7e284d4573e12b0b73a8411e392"
SRC_URI[iproute-tc.sha256sum] = "308b3f154906363be245c9c5f50339847530f0d36ed5edfeea46b7996b0d3faa"
