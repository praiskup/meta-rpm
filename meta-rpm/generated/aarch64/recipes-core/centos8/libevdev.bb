SUMMARY = "generated recipe based on libevdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libevdev = "libevdev.so.2"
RPM_SONAME_REQ_libevdev = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libevdev = "glibc"
RPM_SONAME_REQ_libevdev-devel = "libevdev.so.2"
RPROVIDES_libevdev-devel = "libevdev-dev (= 1.8.0)"
RDEPENDS_libevdev-devel = "libevdev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libevdev-1.8.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libevdev-devel-1.8.0-1.el8.aarch64.rpm \
          "

SRC_URI[libevdev.sha256sum] = "859423ca03ae01d62c2f8769a453ce1ad22f00de35a707b24899c6671e0330bf"
SRC_URI[libevdev-devel.sha256sum] = "de50fcc17270968edfa25b5b74e1ceb38b4384c5794b5fb78f92d603c23e7d25"
