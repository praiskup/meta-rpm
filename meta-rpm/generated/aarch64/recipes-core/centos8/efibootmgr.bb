SUMMARY = "generated recipe based on efibootmgr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "efivar pkgconfig-native popt"
RPM_SONAME_REQ_efibootmgr = "ld-linux-aarch64.so.1 libc.so.6 libefiboot.so.1 libefivar.so.1 libpopt.so.0"
RDEPENDS_efibootmgr = "efivar-libs glibc popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/efibootmgr-16-1.el8.aarch64.rpm \
          "

SRC_URI[efibootmgr.sha256sum] = "7d604604382cb1ec72fb7534cdff4371e5b8ceb84be26ebb3ea6cfbc613f82d9"
