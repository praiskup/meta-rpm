SUMMARY = "generated recipe based on zip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 pkgconfig-native"
RPM_SONAME_REQ_zip = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6"
RDEPENDS_zip = "bzip2-libs glibc unzip"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/zip-3.0-23.el8.aarch64.rpm \
          "

SRC_URI[zip.sha256sum] = "e36dcc50cea1c63c3f5a4249a3e9f9833115684bf485b0637605930ef5810977"
