SUMMARY = "generated recipe based on psmisc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux ncurses pkgconfig-native"
RPM_SONAME_REQ_psmisc = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1 libtinfo.so.6"
RDEPENDS_psmisc = "glibc libselinux ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/psmisc-23.1-4.el8.aarch64.rpm \
          "

SRC_URI[psmisc.sha256sum] = "bf8e27d481a727a7d1571a1988f07d056696a08c3eebc0a5c763950c556be21e"
