SUMMARY = "generated recipe based on mc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gpm pkgconfig-native slang"
RPM_SONAME_REQ_mc = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgmodule-2.0.so.0 libgpm.so.2 libpthread.so.0 libslang.so.2"
RDEPENDS_mc = "bash glib2 glibc gpm-libs perl-File-Temp perl-interpreter perl-libs slang"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mc-4.8.19-9.el8.aarch64.rpm \
          "

SRC_URI[mc.sha256sum] = "894f0e09bd2ef2f163d8a0bd366bc7a3f27463772d6e477602e9c5f9d39c85c1"
