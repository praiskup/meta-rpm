SUMMARY = "generated recipe based on openal-soft srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_openal-soft = "libopenal.so.1"
RPM_SONAME_REQ_openal-soft = "ld-linux-aarch64.so.1 libatomic.so.1 libc.so.6 libdl.so.2 libm.so.6 libopenal.so.1 libpthread.so.0 librt.so.1"
RDEPENDS_openal-soft = "glibc libatomic"
RPM_SONAME_REQ_openal-soft-devel = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libopenal.so.1"
RPROVIDES_openal-soft-devel = "openal-soft-dev (= 1.18.2)"
RDEPENDS_openal-soft-devel = "cmake-filesystem glibc openal-soft pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openal-soft-1.18.2-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openal-soft-devel-1.18.2-7.el8.aarch64.rpm \
          "

SRC_URI[openal-soft.sha256sum] = "2d68e268c35465f0ad9b538d72bb3ddf26e1efa1b5b6ffe7b53624fb31474080"
SRC_URI[openal-soft-devel.sha256sum] = "26601bcb09d3be1b05e9cac349d143ff09c77dd2a9d185d9f2ddb5835eec6e93"
