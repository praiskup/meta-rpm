SUMMARY = "generated recipe based on gnu-efi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_gnu-efi-devel = "gnu-efi-dev (= 3.0.8)"
RDEPENDS_gnu-efi-devel = "gnu-efi"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gnu-efi-3.0.8-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gnu-efi-devel-3.0.8-4.el8.aarch64.rpm \
          "

SRC_URI[gnu-efi.sha256sum] = "213d82edbc14f105c8d0a363648a7f0ea4ebab2e4727df56705ab1e0a4e1c6f8"
SRC_URI[gnu-efi-devel.sha256sum] = "079e8f5352d5f5a320ce8ffa5a6607833ee3736c56b3df5a18db6bae487bb9a8"
