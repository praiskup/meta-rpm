SUMMARY = "generated recipe based on openssl-ibmpkcs11 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_openssl-ibmpkcs11 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0"
RDEPENDS_openssl-ibmpkcs11 = "glibc opencryptoki-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-ibmpkcs11-1.0.2-1.el8.aarch64.rpm \
          "

SRC_URI[openssl-ibmpkcs11.sha256sum] = "fe48e5414bbaf6ee4aee9557e0851701387e3e75d621aa8f79bd0b36f876afed"
