SUMMARY = "generated recipe based on sip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native python3"
RPM_SONAME_REQ_python3-pyqt5-sip = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_python3-pyqt5-sip = "glibc libgcc libstdc++ platform-python"
RPROVIDES_python3-sip-devel = "python3-sip-dev (= 4.19.19)"
RDEPENDS_python3-sip-devel = "platform-python python36-devel sip"
RPM_SONAME_REQ_sip = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_sip = "bash glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyqt5-sip-4.19.19-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-sip-devel-4.19.19-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sip-4.19.19-1.el8.aarch64.rpm \
          "

SRC_URI[python3-pyqt5-sip.sha256sum] = "49872aaaccc648c8e78110467d95c4e5b634870358d9c722344a5b81a0f899b1"
SRC_URI[python3-sip-devel.sha256sum] = "caf477c36e3f688a6a3c6a3630bd4f2f2ff085138818d76c057a626a8331c537"
SRC_URI[sip.sha256sum] = "122f55c614cb7d8c9b170f0fa6873019f07525fc1bfd066460f7096523cca9b8"
