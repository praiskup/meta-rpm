SUMMARY = "generated recipe based on libmemcached srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib libevent libgcc pkgconfig-native"
RPM_SONAME_REQ_libmemcached = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libevent-2.1.so.6 libgcc_s.so.1 libm.so.6 libmemcached.so.11 libmemcachedutil.so.2 libpthread.so.0 libsasl2.so.3 libstdc++.so.6"
RDEPENDS_libmemcached = "cyrus-sasl-lib glibc libevent libgcc libmemcached-libs libstdc++"
RPM_SONAME_REQ_libmemcached-devel = "libhashkit.so.2 libmemcached.so.11 libmemcachedprotocol.so.0 libmemcachedutil.so.2"
RPROVIDES_libmemcached-devel = "libmemcached-dev (= 1.0.18)"
RDEPENDS_libmemcached-devel = "cyrus-sasl-devel libmemcached libmemcached-libs pkgconf-pkg-config"
RPM_SONAME_PROV_libmemcached-libs = "libhashkit.so.2 libmemcached.so.11 libmemcachedprotocol.so.0 libmemcachedutil.so.2"
RPM_SONAME_REQ_libmemcached-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libevent-2.1.so.6 libgcc_s.so.1 libm.so.6 libmemcached.so.11 libpthread.so.0 libsasl2.so.3 libstdc++.so.6"
RDEPENDS_libmemcached-libs = "cyrus-sasl-lib glibc libevent libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmemcached-1.0.18-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmemcached-libs-1.0.18-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmemcached-devel-1.0.18-15.el8.aarch64.rpm \
          "

SRC_URI[libmemcached.sha256sum] = "7ed6b9048971c71ad182975ee50ad620727090a52609030d0ef9a18b6db3bc1d"
SRC_URI[libmemcached-devel.sha256sum] = "49ffe90d9d75773718d452983deb554920d849dab231b53fd9e6902f1e5dd6d2"
SRC_URI[libmemcached-libs.sha256sum] = "2f95b492dc4a9e2cbbfc96ca45798e542c95846d1857852dbd313dcb0d514a09"
