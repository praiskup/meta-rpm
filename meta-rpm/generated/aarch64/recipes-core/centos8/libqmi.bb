SUMMARY = "generated recipe based on libqmi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgudev libmbim pkgconfig-native"
RPM_SONAME_PROV_libqmi = "libqmi-glib.so.5"
RPM_SONAME_REQ_libqmi = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libmbim-glib.so.4"
RDEPENDS_libqmi = "glib2 glibc libmbim"
RPM_SONAME_REQ_libqmi-utils = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmbim-glib.so.4 libqmi-glib.so.5"
RDEPENDS_libqmi-utils = "bash glib2 glibc libgudev libmbim libqmi"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libqmi-1.24.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libqmi-utils-1.24.0-1.el8.aarch64.rpm \
          "

SRC_URI[libqmi.sha256sum] = "ae46383205e15ba9b4fb8ec6ff2f360146a2118e534658894be80653aec40e6a"
SRC_URI[libqmi-utils.sha256sum] = "557cce66062f59cafd2b324b8ae1023ad409e71e535991a08ce96c2ee1e046b6"
