SUMMARY = "generated recipe based on libgee srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libgee = "libgee-0.8.so.2"
RPM_SONAME_REQ_libgee = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libgee = "glib2 glibc"
RPM_SONAME_REQ_libgee-devel = "libgee-0.8.so.2"
RPROVIDES_libgee-devel = "libgee-dev (= 0.20.1)"
RDEPENDS_libgee-devel = "glib2-devel libgee pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgee-0.20.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgee-devel-0.20.1-1.el8.aarch64.rpm \
          "

SRC_URI[libgee.sha256sum] = "1a35a98ec90f099bd11def01357bb67e9bf24f51f3b1fbb7013b682f86f20704"
SRC_URI[libgee-devel.sha256sum] = "0cffcd07ac78fad7c063a85e78e1250860ef3e6c5d057235d95de43adc0ee370"
