SUMMARY = "generated recipe based on perl-CPAN-Meta srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-CPAN-Meta = "perl-CPAN-Meta-Requirements perl-CPAN-Meta-YAML perl-Carp perl-Encode perl-Exporter perl-JSON-PP perl-Scalar-List-Utils perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-CPAN-Meta-2.150010-396.el8.noarch.rpm \
          "

SRC_URI[perl-CPAN-Meta.sha256sum] = "cc1ad1f38602e92325040215ed2017b93eceac18823d9b5b1a1cbf1da292f587"
