SUMMARY = "generated recipe based on lorax srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_composer-cli = "platform-python python3-pytoml python3-urllib3"
RDEPENDS_lorax = "GConf2 cpio device-mapper dosfstools dracut e2fsprogs findutils gawk genisoimage glib2 glibc glibc-common gzip isomd5sum kmod kpartx lorax-templates-generic lorax-templates-rhel parted pigz platform-python python3-dnf python3-kickstart python3-librepo python3-libselinux python3-mako squashfs-tools util-linux xz"
RDEPENDS_lorax-composer = "anaconda-tui bash createrepo_c git glibc-common libgit2 libgit2-glib lorax platform-python python3-flask python3-gevent python3-pytoml python3-rpmfluff python3-semantic_version qemu-img shadow-utils systemd tar xz"
RDEPENDS_lorax-lmc-novirt = "anaconda-core anaconda-tui centos-logos lorax"
RDEPENDS_lorax-lmc-virt = "lorax qemu-kvm"
RDEPENDS_lorax-templates-generic = "lorax"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/composer-cli-28.14.42-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lorax-28.14.42-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lorax-composer-28.14.42-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lorax-lmc-novirt-28.14.42-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lorax-lmc-virt-28.14.42-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lorax-templates-generic-28.14.42-2.el8_2.aarch64.rpm \
          "

SRC_URI[composer-cli.sha256sum] = "d471d48432ebff2b9f9178bddb05ff5d2358271c6cf8cec7eb77b4fbc43d3629"
SRC_URI[lorax.sha256sum] = "faf08f25c464c0238f4a2482c6745fd2684b9134f996fb9f0a70b51c5b53d1ca"
SRC_URI[lorax-composer.sha256sum] = "3ca491b7d65ceac422471cc3d44b5e7b670588a978619abbc7339bca7162c556"
SRC_URI[lorax-lmc-novirt.sha256sum] = "f72a887e9dc0520203f0f650cff866ae90c1594439442fce4e06659e1c015a1c"
SRC_URI[lorax-lmc-virt.sha256sum] = "6d3feef08f2f0622ee94b35d08ca3ced077981066b63a6f84d7c4d9f54250746"
SRC_URI[lorax-templates-generic.sha256sum] = "65b177615c5aaafb49533a2d35ab2e38668573fe0a3ca0b2baabeedc71c06673"
