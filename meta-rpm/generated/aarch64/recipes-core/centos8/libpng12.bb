SUMMARY = "generated recipe based on libpng12 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libpng12 = "libpng12.so.0"
RPM_SONAME_REQ_libpng12 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libz.so.1"
RDEPENDS_libpng12 = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpng12-1.2.57-5.el8.aarch64.rpm \
          "

SRC_URI[libpng12.sha256sum] = "8ebd98c8b80dd3758d916db1937f0c5737580d8bf7d9f1b0735c93eb30446015"
