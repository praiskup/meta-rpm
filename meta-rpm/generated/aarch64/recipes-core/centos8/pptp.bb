SUMMARY = "generated recipe based on pptp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_pptp = "ld-linux-aarch64.so.1 libc.so.6 libutil.so.1"
RDEPENDS_pptp = "glibc iproute ppp systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pptp-1.10.0-3.el8.aarch64.rpm \
          "

SRC_URI[pptp.sha256sum] = "3a8a584ac1455e4979a964bee7b1bd8782a80ac2be2a063c3155e7869b7652d4"
