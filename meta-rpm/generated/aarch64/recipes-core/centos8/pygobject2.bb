SUMMARY = "generated recipe based on pygobject2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_pygobject2 = "libpyglib-2.0-python2.so.0"
RPM_SONAME_REQ_pygobject2 = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libpyglib-2.0-python2.so.0"
RDEPENDS_pygobject2 = "glib2 glibc platform-python"
RDEPENDS_pygobject2-codegen = "bash"
RPROVIDES_pygobject2-devel = "pygobject2-dev (= 2.28.7)"
RDEPENDS_pygobject2-devel = "glib2-devel pkgconf-pkg-config pygobject2 pygobject2-codegen pygobject2-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygobject2-2.28.7-4.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygobject2-codegen-2.28.7-4.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygobject2-devel-2.28.7-4.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygobject2-doc-2.28.7-4.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
          "

SRC_URI[pygobject2.sha256sum] = "56eaa527ba2620c48b7d64811dea2dd42f9e6809d3364faaf3c416587a2ed4f4"
SRC_URI[pygobject2-codegen.sha256sum] = "f487ad7bded7c4ce4f9763d64ff59b1347811391430dc6aaa530efadcf2867ec"
SRC_URI[pygobject2-devel.sha256sum] = "d71276911232b5ee986bd8fc08b77f6ced14125a6ac7b52afe2c6b471a1b82bf"
SRC_URI[pygobject2-doc.sha256sum] = "c51020a5f8fca24da7a26c8d02e309e62e023c2bef8ba7356bbbed8546a186c4"
