SUMMARY = "generated recipe based on cdrkit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 cdparanoia file libcap pkgconfig-native zlib"
RDEPENDS_dirsplit = "genisoimage perl-File-Path perl-Getopt-Long perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs"
RPM_SONAME_REQ_genisoimage = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libmagic.so.1 libpthread.so.0 librols.so.0 libusal.so.0 libz.so.1"
RDEPENDS_genisoimage = "bash bzip2-libs chkconfig coreutils file-libs glibc libusal zlib"
RPM_SONAME_REQ_icedax = "ld-linux-aarch64.so.1 libc.so.6 libcdda_interface.so.0 libcdda_paranoia.so.0 librols.so.0 libusal.so.0"
RDEPENDS_icedax = "bash cdparanoia cdparanoia-libs chkconfig coreutils glibc libusal vorbis-tools"
RPM_SONAME_PROV_libusal = "librols.so.0 libusal.so.0"
RPM_SONAME_REQ_libusal = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libusal = "glibc"
RPM_SONAME_REQ_wodim = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libpthread.so.0 librols.so.0 libusal.so.0"
RDEPENDS_wodim = "bash chkconfig coreutils glibc libcap libusal"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dirsplit-1.1.11-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/genisoimage-1.1.11-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/icedax-1.1.11-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libusal-1.1.11-39.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/wodim-1.1.11-39.el8.aarch64.rpm \
          "

SRC_URI[dirsplit.sha256sum] = "113fae13a537ddb688d68fd1779c9800da3c25c4b0fb741649cd86112ce4489f"
SRC_URI[genisoimage.sha256sum] = "308643cc67fae11c206b579fff4d22f6c4c5d771b8f82bfc160b2037ad851723"
SRC_URI[icedax.sha256sum] = "1800d4d65028413d7ebd0ae923b7429302330013395f0c5e30c5a29b793ac655"
SRC_URI[libusal.sha256sum] = "74bf56261c41a515bc53cbc95f447e2d03adbe74b0ec060eec13c5ef3d8c3d32"
SRC_URI[wodim.sha256sum] = "afbcbae12e5b221133e4eb7a134188ac12a63dbfa77fbfc567fe106701a37e28"
