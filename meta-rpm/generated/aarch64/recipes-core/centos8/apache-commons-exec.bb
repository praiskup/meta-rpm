SUMMARY = "generated recipe based on apache-commons-exec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-exec = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-exec-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-exec-1.3-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-exec-javadoc-1.3-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-exec.sha256sum] = "85158e507c49a4ffd867bf2eaf097f54f56c5de1c790874eae3d07239dbac2b3"
SRC_URI[apache-commons-exec-javadoc.sha256sum] = "96219a4e7a8227b35e9ad144075a4e77982ab9acc3ad465da808d0bd890957f7"
