SUMMARY = "generated recipe based on symlinks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_symlinks = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_symlinks = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/symlinks-1.4-19.el8.aarch64.rpm \
          "

SRC_URI[symlinks.sha256sum] = "2e58af0cdab070b23439f00fba4daadd344b58273b65e48634c6097e5c296900"
