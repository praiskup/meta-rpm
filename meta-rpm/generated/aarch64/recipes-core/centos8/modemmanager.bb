SUMMARY = "generated recipe based on ModemManager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libgudev libmbim libqmi pkgconfig-native systemd-libs"
RPM_SONAME_REQ_ModemManager = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmbim-glib.so.4 libmm-glib.so.0 libpthread.so.0 libqmi-glib.so.5 libsystemd.so.0"
RDEPENDS_ModemManager = "ModemManager-glib bash glib2 glibc libgcc libgudev libmbim libmbim-utils libqmi libqmi-utils systemd systemd-libs"
RPROVIDES_ModemManager-devel = "ModemManager-dev (= 1.10.8)"
RDEPENDS_ModemManager-devel = "ModemManager pkgconf-pkg-config"
RPM_SONAME_PROV_ModemManager-glib = "libmm-glib.so.0"
RPM_SONAME_REQ_ModemManager-glib = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_ModemManager-glib = "glib2 glibc"
RPM_SONAME_REQ_ModemManager-glib-devel = "libmm-glib.so.0"
RPROVIDES_ModemManager-glib-devel = "ModemManager-glib-dev (= 1.10.8)"
RDEPENDS_ModemManager-glib-devel = "ModemManager ModemManager-devel ModemManager-glib glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ModemManager-1.10.8-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ModemManager-glib-1.10.8-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ModemManager-devel-1.10.8-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ModemManager-glib-devel-1.10.8-2.el8.aarch64.rpm \
          "

SRC_URI[ModemManager.sha256sum] = "3079d4fb66a166e1f5913b7bcace54dc9603cc7626315bd61e0025eb20ec5042"
SRC_URI[ModemManager-devel.sha256sum] = "3b9fe291da8ac48d5437a246696f95a9065059c58a118087a418949e1a6cb339"
SRC_URI[ModemManager-glib.sha256sum] = "dbed55a96bd89a636f21a71a6e21a1f0282be7dc9cafdbbba44214a03d4f796e"
SRC_URI[ModemManager-glib-devel.sha256sum] = "b402f6d44d68ae45d21fce9588e3a8a23eff96530615a1c1e915a01928f41f5f"
