SUMMARY = "generated recipe based on less srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_less = "ld-linux-aarch64.so.1 libc.so.6 libtinfo.so.6"
RDEPENDS_less = "bash glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/less-530-1.el8.aarch64.rpm \
          "

SRC_URI[less.sha256sum] = "2057a073ae0bb0149a8093447fad2577f900577605576b3ca22b2af76e970c5f"
