SUMMARY = "generated recipe based on httpcomponents-client srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_httpcomponents-client = "apache-commons-codec apache-commons-logging httpcomponents-core java-1.8.0-openjdk-headless javapackages-filesystem publicsuffix-list"
RDEPENDS_httpcomponents-client-cache = "apache-commons-logging httpcomponents-client java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_httpcomponents-client-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/httpcomponents-client-4.5.5-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/httpcomponents-client-cache-4.5.5-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/httpcomponents-client-javadoc-4.5.5-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[httpcomponents-client.sha256sum] = "5d265d24001ff68ab307bb611b94ab1680115c6c4dc78f29e473afc2c79d6b17"
SRC_URI[httpcomponents-client-cache.sha256sum] = "4c2fd8b82be152d88f5ed9f6db68c29783a45069600b0248154c203d0699335d"
SRC_URI[httpcomponents-client-javadoc.sha256sum] = "aa63906cbd8c53b4481b982f60849acc64d6b42c438e0f307a75b0f8d7defdde"
