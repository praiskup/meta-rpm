SUMMARY = "generated recipe based on libxcb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxau pkgconfig-native"
RPM_SONAME_PROV_libxcb = "libxcb-composite.so.0 libxcb-damage.so.0 libxcb-dpms.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-glx.so.0 libxcb-present.so.0 libxcb-randr.so.0 libxcb-record.so.0 libxcb-render.so.0 libxcb-res.so.0 libxcb-screensaver.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-sync.so.1 libxcb-xf86dri.so.0 libxcb-xfixes.so.0 libxcb-xinerama.so.0 libxcb-xinput.so.0 libxcb-xkb.so.1 libxcb-xselinux.so.0 libxcb-xtest.so.0 libxcb-xv.so.0 libxcb-xvmc.so.0 libxcb.so.1"
RPM_SONAME_REQ_libxcb = "ld-linux-aarch64.so.1 libXau.so.6 libc.so.6 libxcb.so.1"
RDEPENDS_libxcb = "glibc libXau"
RPM_SONAME_REQ_libxcb-devel = "libxcb-composite.so.0 libxcb-damage.so.0 libxcb-dpms.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-glx.so.0 libxcb-present.so.0 libxcb-randr.so.0 libxcb-record.so.0 libxcb-render.so.0 libxcb-res.so.0 libxcb-screensaver.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-sync.so.1 libxcb-xf86dri.so.0 libxcb-xfixes.so.0 libxcb-xinerama.so.0 libxcb-xinput.so.0 libxcb-xkb.so.1 libxcb-xselinux.so.0 libxcb-xtest.so.0 libxcb-xv.so.0 libxcb-xvmc.so.0 libxcb.so.1"
RPROVIDES_libxcb-devel = "libxcb-dev (= 1.13.1)"
RDEPENDS_libxcb-devel = "libXau-devel libxcb pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxcb-1.13.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxcb-devel-1.13.1-1.el8.aarch64.rpm \
          "

SRC_URI[libxcb.sha256sum] = "47861fc8dd36847f9b1c74cde10e62968a7a38210737d91cfba42a64b65c6112"
SRC_URI[libxcb-devel.sha256sum] = "d776162f312da2f42e2f0ce1a53a676614ed1744750378b2c514a8bdcd1d78d3"
