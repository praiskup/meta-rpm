SUMMARY = "generated recipe based on shim srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_shim-aa64 = "dbxtool efi-filesystem mokutil"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/shim-aa64-15-15.el8_2.aarch64.rpm \
          "

SRC_URI[shim-aa64.sha256sum] = "6404c2bf11a3f1b5b8215807cb4a5c95d6b283dccfe2e5798ec51918337afdcb"
