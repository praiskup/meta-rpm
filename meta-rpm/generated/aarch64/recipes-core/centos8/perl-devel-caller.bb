SUMMARY = "generated recipe based on perl-Devel-Caller srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-Caller = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Caller = "glibc perl-Exporter perl-PadWalker perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-Caller-2.06-15.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-Caller.sha256sum] = "0a4d160601080cc27fa36253957c61d37e98cc193f1bd128a6157ad750e6421c"
