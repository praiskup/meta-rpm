SUMMARY = "generated recipe based on redhat-support-lib-python srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_redhat-support-lib-python = "ca-certificates platform-python python3-dateutil python3-lxml python3-rpm python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/redhat-support-lib-python-0.11.2-1.el8.noarch.rpm \
          "

SRC_URI[redhat-support-lib-python.sha256sum] = "9deadf4403a39de590855af141a9cc82cc19693d78c684714728ab225c89846d"
