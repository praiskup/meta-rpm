SUMMARY = "generated recipe based on rtkit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs libcap pkgconfig-native"
RPM_SONAME_REQ_rtkit = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libdbus-1.so.3 libpthread.so.0 librt.so.1"
RDEPENDS_rtkit = "bash dbus dbus-libs glibc libcap polkit systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rtkit-0.11-19.el8.aarch64.rpm \
          "

SRC_URI[rtkit.sha256sum] = "553fd2dd54ecae2b90d715eb0da1f9f4644d5ecb94878d2732d53ace8b92a00c"
