SUMMARY = "generated recipe based on libgdata srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcr glib-2.0 gnome-online-accounts json-glib libgcc liboauth libsoup-2.4 libxml2 p11-kit pkgconfig-native"
RPM_SONAME_PROV_libgdata = "libgdata.so.22"
RPM_SONAME_REQ_libgdata = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libjson-glib-1.0.so.0 liboauth.so.0 libp11-kit.so.0 libpthread.so.0 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_libgdata = "gcr glib2 glibc gnome-online-accounts json-glib libgcc liboauth libsoup libxml2 p11-kit"
RPM_SONAME_REQ_libgdata-devel = "libgdata.so.22"
RPROVIDES_libgdata-devel = "libgdata-dev (= 0.17.9)"
RDEPENDS_libgdata-devel = "gcr-devel glib2-devel gnome-online-accounts-devel json-glib-devel libgdata liboauth-devel libsoup-devel libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgdata-0.17.9-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgdata-devel-0.17.9-2.el8.aarch64.rpm \
          "

SRC_URI[libgdata.sha256sum] = "6bc3abcce8732e7567ea0db4ff4769c702e4af05d55512ebeb4e9ae7606d645a"
SRC_URI[libgdata-devel.sha256sum] = "7c12a804a26ff57b9b3b9692a90a83d407e27dea3d4f8b7210ec2707bdd179bf"
