SUMMARY = "generated recipe based on isomd5sum srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native popt"
RPM_SONAME_REQ_isomd5sum = "ld-linux-aarch64.so.1 libc.so.6 libpopt.so.0"
RDEPENDS_isomd5sum = "glibc popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/isomd5sum-1.2.3-3.el8.aarch64.rpm \
          "

SRC_URI[isomd5sum.sha256sum] = "34ff9dc23a4ee66c312291b0e180a6df10454cf0cd98709157887d6e0a29ff8b"
