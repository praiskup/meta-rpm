SUMMARY = "generated recipe based on hexedit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_hexedit = "ld-linux-aarch64.so.1 libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_hexedit = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hexedit-1.2.13-12.el8.aarch64.rpm \
          "

SRC_URI[hexedit.sha256sum] = "9ebd0a82c0e6d8ff41e1fc27a7fbf9c4620cb9e316e6ea67c221509c4b5337fa"
