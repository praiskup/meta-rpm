SUMMARY = "generated recipe based on http-parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_http-parser = "libhttp_parser.so.2 libhttp_parser_strict.so.2"
RPM_SONAME_REQ_http-parser = "libc.so.6"
RDEPENDS_http-parser = "glibc"
RPM_SONAME_REQ_http-parser-devel = "libhttp_parser.so.2 libhttp_parser_strict.so.2"
RPROVIDES_http-parser-devel = "http-parser-dev (= 2.8.0)"
RDEPENDS_http-parser-devel = "http-parser"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/http-parser-2.8.0-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/http-parser-devel-2.8.0-9.el8.aarch64.rpm \
          "

SRC_URI[http-parser.sha256sum] = "88b785c1bda2fee725bddfb0386ff6e9dc1d440a91943a0c7e05f06c93442257"
SRC_URI[http-parser-devel.sha256sum] = "963e8bbd3ffc5a0904af5841d6d570843976ef131eacb0d81e8532e0fbe3f2dd"
