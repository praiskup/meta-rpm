SUMMARY = "generated recipe based on mousetweaks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libx11 libxcursor libxfixes libxtst pango pkgconfig-native"
RPM_SONAME_REQ_mousetweaks = "ld-linux-aarch64.so.1 libX11.so.6 libXcursor.so.1 libXfixes.so.3 libXtst.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_mousetweaks = "GConf2 atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libXcursor libXfixes libXtst pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mousetweaks-3.12.0-11.el8.aarch64.rpm \
          "

SRC_URI[mousetweaks.sha256sum] = "2d807632db684e3451a303008222e9d77df55e912675f7247395a6fbbaf4f240"
