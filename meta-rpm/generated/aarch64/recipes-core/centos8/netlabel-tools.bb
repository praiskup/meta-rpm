SUMMARY = "generated recipe based on netlabel_tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native"
RPM_SONAME_REQ_netlabel_tools = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libnl-genl-3.so.200"
RDEPENDS_netlabel_tools = "bash glibc kernel libnl3 systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/netlabel_tools-0.30.0-3.el8.aarch64.rpm \
          "

SRC_URI[netlabel_tools.sha256sum] = "e94a3f12fa021ed2e5f9c08e966fd942f1af8af291084c4f1edb8758406df3ce"
