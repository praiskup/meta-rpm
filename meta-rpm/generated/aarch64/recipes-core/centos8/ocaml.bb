SUMMARY = "generated recipe based on ocaml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native zlib"
RPM_SONAME_PROV_ocaml = "libasmrun_shared.so libcamlrun_shared.so"
RPM_SONAME_REQ_ocaml = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_ocaml = "bash gawk gcc glibc libX11 ocaml-runtime zlib"
RDEPENDS_ocaml-compiler-libs = "ocaml ocaml-runtime"
RPM_SONAME_REQ_ocaml-ocamldoc = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-ocamldoc = "glibc ocaml ocaml-compiler-libs ocaml-runtime"
RPM_SONAME_REQ_ocaml-runtime = "libX11.so.6 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_ocaml-runtime = "glibc libX11 ocaml-compiler-libs util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-4.07.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-compiler-libs-4.07.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-ocamldoc-4.07.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-runtime-4.07.0-3.el8.aarch64.rpm \
          "

SRC_URI[ocaml.sha256sum] = "591e8006d6fa978b9256876d1930c581b90e52a17037be4169159e83e0877c2a"
SRC_URI[ocaml-compiler-libs.sha256sum] = "632f5f5ae5e7b8da675240ff0367868abe715594e442ae3a5e1e4b075befa1bf"
SRC_URI[ocaml-ocamldoc.sha256sum] = "5e201f73ade8b9256e246a90dff57c691a25883e21622ef208c447a0a88eb550"
SRC_URI[ocaml-runtime.sha256sum] = "359dc7fa6e685ab1b2a9e05ebebf13f025037812c23080d16709df54b4abacdd"
