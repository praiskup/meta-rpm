SUMMARY = "generated recipe based on perl-Module-Install-AuthorTests srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Install-AuthorTests = "perl-Carp perl-Module-Install perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Module-Install-AuthorTests-0.002-16.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Install-AuthorTests.sha256sum] = "31005bd3a129e71badf2d5962ab36243d774dd10592796bea4024742077476a4"
