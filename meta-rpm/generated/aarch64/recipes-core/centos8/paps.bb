SUMMARY = "generated recipe based on paps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs e2fsprogs fontconfig freetype glib-2.0 krb5-libs libxcrypt pango pkgconfig-native zlib"
RPM_SONAME_REQ_paps = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpango-1.0.so.0 libpangoft2-1.0.so.0 libpaps.so.0 libpthread.so.0 libz.so.1"
RDEPENDS_paps = "cups-filesystem cups-libs fontconfig fontpackages-filesystem freetype glib2 glibc krb5-libs libcom_err libxcrypt pango paps-libs zlib"
RPM_SONAME_PROV_paps-libs = "libpaps.so.0"
RPM_SONAME_REQ_paps-libs = "ld-linux-aarch64.so.1 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libpango-1.0.so.0 libpangoft2-1.0.so.0"
RDEPENDS_paps-libs = "fontconfig freetype glib2 glibc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/paps-0.6.8-41.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/paps-libs-0.6.8-41.el8.aarch64.rpm \
          "

SRC_URI[paps.sha256sum] = "4984ca00890bd947a6163cd05947031de7d7d10bc0d8ba337ee8dc539f29424f"
SRC_URI[paps-libs.sha256sum] = "ed66d17500a11cf2b763e17acaff49801ddf5573e59610bf589b10d40a16d04c"
