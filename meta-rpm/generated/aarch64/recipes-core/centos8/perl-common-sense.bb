SUMMARY = "generated recipe based on perl-common-sense srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-common-sense = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-common-sense-3.7.4-8.el8.aarch64.rpm \
          "

SRC_URI[perl-common-sense.sha256sum] = "8898f10d20bbc8dd5e90cf39be799a526f1f72a79d99df2ed9e7d3ab18382a38"
