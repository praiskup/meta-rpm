SUMMARY = "generated recipe based on system-storage-manager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_system-storage-manager = "e2fsprogs platform-python python3-pwquality util-linux which xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/system-storage-manager-1.4-1.el8.noarch.rpm \
          "

SRC_URI[system-storage-manager.sha256sum] = "ea0a2c4e7f4a4b0a65c1fc3cdecfad4d951e9d1b1e01336ea044008b28f98de2"
