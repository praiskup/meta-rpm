SUMMARY = "generated recipe based on perl-File-CheckTree srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-CheckTree = "perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-File-CheckTree-4.42-303.el8.noarch.rpm \
          "

SRC_URI[perl-File-CheckTree.sha256sum] = "c22fc3653184b050d5270cf8dd10e41285e52c7e5cd5d42ed48ece2965cf6e4f"
