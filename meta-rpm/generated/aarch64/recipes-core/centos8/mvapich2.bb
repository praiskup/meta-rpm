SUMMARY = "generated recipe based on mvapich2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc hwloc libgcc pkgconfig-native rdma-core"
RPM_SONAME_PROV_mvapich2 = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPM_SONAME_REQ_mvapich2 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgfortran.so.5 libhwloc.so.5 libibmad.so.5 libibumad.so.3 libibverbs.so.1 libm.so.6 libmpi.so.12 libpthread.so.0 librdmacm.so.1 librt.so.1 libstdc++.so.6"
RDEPENDS_mvapich2 = "environment-modules glibc hwloc-libs infiniband-diags libgcc libgfortran libibumad libibverbs librdmacm libstdc++ perl-interpreter platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mvapich2-2.3.2-2.el8.aarch64.rpm \
          "

SRC_URI[mvapich2.sha256sum] = "91a693f62ee2191850c1b0db21e8f5411d5ba7ba075bf59363185b0edd79aa41"
