SUMMARY = "generated recipe based on perl-Devel-Size srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-Size = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Size = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Devel-Size-0.81-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-Size.sha256sum] = "3dfafbe8cecb86b936925962d9297a3183e910c36e01ca51f2a36951a11011f9"
