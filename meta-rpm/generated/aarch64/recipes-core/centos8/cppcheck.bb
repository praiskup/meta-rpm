SUMMARY = "generated recipe based on cppcheck srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libpcre pkgconfig-native tinyxml2"
RPM_SONAME_REQ_cppcheck = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpcre.so.1 libstdc++.so.6 libtinyxml2.so.6"
RDEPENDS_cppcheck = "glibc libgcc libstdc++ pcre tinyxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/cppcheck-1.83-1.el8.aarch64.rpm \
          "

SRC_URI[cppcheck.sha256sum] = "6b0d9854aac5fc16b08129e17a97bd11f107eb9a8ad791b51b9f8cf105cdfe4f"
