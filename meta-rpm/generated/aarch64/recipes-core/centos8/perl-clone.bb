SUMMARY = "generated recipe based on perl-Clone srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Clone = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Clone = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Clone-0.39-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Clone.sha256sum] = "61f4cae76bc1ba5d37b7efa97b20fd2c6cd0ddfb61ce7bc357da5329ec6e7160"
