SUMMARY = "generated recipe based on vim srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl cairo gdk-pixbuf glib-2.0 gpm gtk+3 libice libselinux libsm libx11 libxt ncurses pango pkgconfig-native"
RPM_SONAME_REQ_vim-X11 = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXt.so.6 libacl.so.1 libc.so.6 libcairo.so.2 libdl.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpm.so.2 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libselinux.so.1 libtinfo.so.6"
RDEPENDS_vim-X11 = "bash cairo gdk-pixbuf2 glib2 glibc gpm-libs gtk3 hicolor-icon-theme libICE libSM libX11 libXt libacl libattr libselinux ncurses-libs pango perl-libs vim-common"
RPM_SONAME_REQ_vim-common = "libc.so.6"
RDEPENDS_vim-common = "bash glibc vim-filesystem"
RPM_SONAME_REQ_vim-enhanced = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libdl.so.2 libgpm.so.2 libm.so.6 libpthread.so.0 libselinux.so.1 libtinfo.so.6"
RDEPENDS_vim-enhanced = "bash glibc gpm-libs libacl libselinux ncurses-libs vim-common which"
RPM_SONAME_REQ_vim-minimal = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libselinux.so.1 libtinfo.so.6"
RDEPENDS_vim-minimal = "glibc libacl libselinux ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vim-X11-8.0.1763-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vim-common-8.0.1763-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vim-enhanced-8.0.1763-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vim-filesystem-8.0.1763-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/vim-minimal-8.0.1763-13.el8.aarch64.rpm \
          "

SRC_URI[vim-X11.sha256sum] = "92ddc530a1aaba1cd49931ae41186919ba7e3ce0be217a1120c01d839c1e80eb"
SRC_URI[vim-common.sha256sum] = "8755fb0dfb35cac19db4e321fb2c2ebdfaad4040bcacc7bd6223fc105ea9bdf0"
SRC_URI[vim-enhanced.sha256sum] = "d7b00774601f6b8fad4b5a3ff83639eb0ec2ab5cacbe14ed237a027de426215b"
SRC_URI[vim-filesystem.sha256sum] = "e61f185f6fcce2740e2f92f963b069695dd0120b4aa82343458c43cf4ba95faf"
SRC_URI[vim-minimal.sha256sum] = "064c38a01fb786acf8ff0f8657d74077d2d6091908c20a4c41156d161893e6a4"
