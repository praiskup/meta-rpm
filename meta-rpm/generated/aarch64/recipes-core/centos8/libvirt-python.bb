SUMMARY = "generated recipe based on libvirt-python srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-libvirt = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0"
RDEPENDS_python3-libvirt = "glibc libvirt-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-libvirt-4.5.0-2.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[python3-libvirt.sha256sum] = "04e23b5976c60cc80db9a4c6c3d44127abffbd39d3dd5410fcae1e174606e060"
