SUMMARY = "generated recipe based on maven-invoker-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-invoker-plugin = "ant-lib apache-commons-io bsh java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-doxia-sink-api maven-doxia-sitetools maven-invoker maven-lib maven-model maven-project maven-reporting-api maven-reporting-impl maven-script-interpreter maven-settings plexus-i18n plexus-interpolation plexus-utils"
RDEPENDS_maven-invoker-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-invoker-plugin-1.10-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-invoker-plugin-javadoc-1.10-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-invoker-plugin.sha256sum] = "2015ff4acdbac5bc60174b13bd797a60ef6c18f526e40e1da63516749aae76b1"
SRC_URI[maven-invoker-plugin-javadoc.sha256sum] = "8b973b04b0d75530a5a9c5187f3aa7649cf4b54c40d255670c1c6bacefdec987"
