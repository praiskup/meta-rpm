SUMMARY = "generated recipe based on pcsc-lite-ccid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_PROV_pcsc-lite-ccid = "libccid.so libccidtwin.so"
RPM_SONAME_REQ_pcsc-lite-ccid = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libusb-1.0.so.0"
RDEPENDS_pcsc-lite-ccid = "bash glibc libusbx pcsc-lite systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pcsc-lite-ccid-1.4.29-3.el8.aarch64.rpm \
          "

SRC_URI[pcsc-lite-ccid.sha256sum] = "b3b299fdd16ddd594aaa5409a175101d22c25b9e84831a52e106bc79a85726d8"
