SUMMARY = "generated recipe based on libyang srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre pkgconfig-native"
RPM_SONAME_PROV_libyang = "libyang.so.0.16"
RPM_SONAME_REQ_libyang = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpcre.so.1 libpthread.so.0 libyang.so.0.16"
RDEPENDS_libyang = "glibc pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libyang-0.16.105-3.el8_1.2.aarch64.rpm \
          "

SRC_URI[libyang.sha256sum] = "d1592db62ad11da88e93a114948ed9bfc04c00cccc6ca9d5b27cfa01448db98b"
