SUMMARY = "generated recipe based on libusb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_PROV_libusb = "libusb-0.1.so.4"
RPM_SONAME_REQ_libusb = "ld-linux-aarch64.so.1 libc.so.6 libusb-1.0.so.0"
RDEPENDS_libusb = "glibc libusbx"
RPM_SONAME_REQ_libusb-devel = "libusb-0.1.so.4"
RPROVIDES_libusb-devel = "libusb-dev (= 0.1.5)"
RDEPENDS_libusb-devel = "bash libusb libusbx-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libusb-0.1.5-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libusb-devel-0.1.5-12.el8.aarch64.rpm \
          "

SRC_URI[libusb.sha256sum] = "c58b8c403554503c1fc9561c3c15fc1dd493821c282c5717867e3231649c4648"
SRC_URI[libusb-devel.sha256sum] = "8c10e5b4cb877908ba03c689b69c6b6b44fd55d69ef1b0f5610f061d8fe4207e"
