SUMMARY = "generated recipe based on gtest srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_gmock = "libgmock.so.1.8.0 libgmock_main.so.1.8.0"
RPM_SONAME_REQ_gmock = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgmock.so.1.8.0 libgtest.so.1.8.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gmock = "glibc gtest libgcc libstdc++"
RPM_SONAME_REQ_gmock-devel = "libgmock.so.1.8.0 libgmock_main.so.1.8.0"
RPROVIDES_gmock-devel = "gmock-dev (= 1.8.0)"
RDEPENDS_gmock-devel = "gmock"
RPM_SONAME_PROV_gtest = "libgtest.so.1.8.0 libgtest_main.so.1.8.0"
RPM_SONAME_REQ_gtest = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgtest.so.1.8.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gtest = "glibc libgcc libstdc++"
RPM_SONAME_REQ_gtest-devel = "libgtest.so.1.8.0 libgtest_main.so.1.8.0"
RPROVIDES_gtest-devel = "gtest-dev (= 1.8.0)"
RDEPENDS_gtest-devel = "gtest"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gmock-1.8.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gmock-devel-1.8.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtest-1.8.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtest-devel-1.8.0-5.el8.aarch64.rpm \
          "

SRC_URI[gmock.sha256sum] = "4fa7732a80ca37c4ccc4a4694649cfa8f89e2dbb3c8ae205b920ab42092e52cc"
SRC_URI[gmock-devel.sha256sum] = "dbd37e13fe2d801d8fec359f7b79e5fc6e9a876a709341bdd2090da97172b8f9"
SRC_URI[gtest.sha256sum] = "0509adde1be4655548e3550b872a41c8be2083ccdd3eff86915442394b16d93e"
SRC_URI[gtest-devel.sha256sum] = "b7753710c00b50bc5d51be2a97b99bea8db42a6bccf839ddc2a132377d539ff0"
