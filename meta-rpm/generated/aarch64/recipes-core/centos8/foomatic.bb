SUMMARY = "generated recipe based on foomatic srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxml2 pkgconfig-native xz zlib"
RPM_SONAME_REQ_foomatic = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_foomatic = "bash colord coreutils cups cups-filters dbus foomatic-db ghostscript glibc libxml2 perl-Data-Dumper perl-Encode perl-Exporter perl-Getopt-Long perl-PathTools perl-interpreter perl-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/foomatic-4.0.12-23.el8.aarch64.rpm \
          "

SRC_URI[foomatic.sha256sum] = "25e753449bbd5b9a0cf6885120820e1b3210a2fa12f1c795ba510b3a2fe21853"
