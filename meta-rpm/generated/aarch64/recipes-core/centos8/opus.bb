SUMMARY = "generated recipe based on opus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libogg pkgconfig-native"
RPM_SONAME_PROV_opus = "libopus.so.0"
RPM_SONAME_REQ_opus = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6"
RDEPENDS_opus = "glibc libgcc"
RPM_SONAME_REQ_opus-devel = "libopus.so.0"
RPROVIDES_opus-devel = "opus-dev (= 1.3)"
RDEPENDS_opus-devel = "libogg-devel opus pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/opus-1.3-0.4.beta.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/opus-devel-1.3-0.4.beta.el8.aarch64.rpm \
          "

SRC_URI[opus.sha256sum] = "f07c9cb723eaf6779022c5893a4705deab765a48d8f3e5dcd1670b9fb3d253d7"
SRC_URI[opus-devel.sha256sum] = "c60da32dea95a2ab63b138dced1654368c29070a74ea700f294a6ba5747cc6fd"
