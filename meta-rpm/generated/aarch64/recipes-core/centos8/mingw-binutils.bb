SUMMARY = "generated recipe based on mingw-binutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mingw-binutils-generic = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_mingw-binutils-generic = "glibc"
RPM_SONAME_REQ_mingw32-binutils = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mingw32-binutils = "glibc mingw-binutils-generic mingw32-filesystem"
RPM_SONAME_REQ_mingw64-binutils = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mingw64-binutils = "glibc mingw-binutils-generic mingw64-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw-binutils-generic-2.30-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw32-binutils-2.30-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw64-binutils-2.30-1.el8.aarch64.rpm \
          "

SRC_URI[mingw-binutils-generic.sha256sum] = "fa687182345095bbb894e9283c07ec51d68b6c03e517cc7d6104c912c7458ea0"
SRC_URI[mingw32-binutils.sha256sum] = "3f6b23d089587370da1b2899c05de119de8435cee50614250c760ae5c25710c2"
SRC_URI[mingw64-binutils.sha256sum] = "afe6dcc59792b1f3c5eb383ded752b36a507553d06e42ec229ea155286825ccc"
