SUMMARY = "generated recipe based on xdg-user-dirs-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_REQ_xdg-user-dirs-gtk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_xdg-user-dirs-gtk = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 pango xdg-user-dirs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xdg-user-dirs-gtk-0.10-13.el8.aarch64.rpm \
          "

SRC_URI[xdg-user-dirs-gtk.sha256sum] = "4a81747eeb83d8d8cce0a388f19fb617381cdeabbf3f0cd38bdaf7c2c68d9a44"
