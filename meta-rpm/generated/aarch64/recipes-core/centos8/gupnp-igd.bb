SUMMARY = "generated recipe based on gupnp-igd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gssdp gupnp pkgconfig-native"
RPM_SONAME_PROV_gupnp-igd = "libgupnp-igd-1.0.so.4"
RPM_SONAME_REQ_gupnp-igd = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgssdp-1.0.so.3 libgupnp-1.0.so.4 libpthread.so.0"
RDEPENDS_gupnp-igd = "glib2 glibc gssdp gupnp"
RPM_SONAME_REQ_gupnp-igd-devel = "libgupnp-igd-1.0.so.4"
RPROVIDES_gupnp-igd-devel = "gupnp-igd-dev (= 0.2.5)"
RDEPENDS_gupnp-igd-devel = "gupnp-devel gupnp-igd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gupnp-igd-0.2.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gupnp-igd-devel-0.2.5-4.el8.aarch64.rpm \
          "

SRC_URI[gupnp-igd.sha256sum] = "ad2f3f9f5bdf103afc3b000880242f9309ef4f3df11ff95446669e614af1e59f"
SRC_URI[gupnp-igd-devel.sha256sum] = "6f4578de886ccc16575ba985dfa8bfc481cf6c09362d940e696bc8fa0337de4a"
