SUMMARY = "generated recipe based on perl-Specio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Specio = "perl-Carp perl-Devel-StackTrace perl-Eval-Closure perl-Exporter perl-MRO-Compat perl-Module-Runtime perl-Ref-Util perl-Role-Tiny perl-Scalar-List-Utils perl-Storable perl-interpreter perl-libs perl-parent perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Specio-0.42-2.el8.noarch.rpm \
          "

SRC_URI[perl-Specio.sha256sum] = "fde93c93f91ee46f9233ac887b805c95015a32ff46c84d880bea5d85698dae94"
