SUMMARY = "generated recipe based on openblas-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openblas-srpm-macros-2-2.el8.noarch.rpm \
          "

SRC_URI[openblas-srpm-macros.sha256sum] = "e551afb45f48dcd047181fea948b9a643c10de10b8869688171d4572794cd4d1"
