SUMMARY = "generated recipe based on ledmon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sg3-utils systemd-libs"
RPM_SONAME_REQ_ledmon = "ld-linux-aarch64.so.1 libc.so.6 librt.so.1 libsgutils2.so.2 libudev.so.1"
RDEPENDS_ledmon = "glibc sg3_utils-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ledmon-0.93-1.el8.aarch64.rpm \
          "

SRC_URI[ledmon.sha256sum] = "fe7fbd7317343d22288bffe1d1b2b15e03cfc031171d3a63011832bfc17e9ed8"
