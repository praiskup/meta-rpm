SUMMARY = "generated recipe based on libexif srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libexif = "libexif.so.12"
RPM_SONAME_REQ_libexif = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libexif = "glibc"
RPM_SONAME_REQ_libexif-devel = "libexif.so.12"
RPROVIDES_libexif-devel = "libexif-dev (= 0.6.21)"
RDEPENDS_libexif-devel = "libexif pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libexif-0.6.21-17.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libexif-devel-0.6.21-17.el8_2.aarch64.rpm \
          "

SRC_URI[libexif.sha256sum] = "2f368048ff2efd4377d846b2add1bfcce842bda85ea493c927153c24ffb782ac"
SRC_URI[libexif-devel.sha256sum] = "87d20e41458be747e8046e66e5cd807976780b293fbe2122e48eba164c9a3cec"
