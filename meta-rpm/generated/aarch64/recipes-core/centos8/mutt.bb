SUMMARY = "generated recipe based on mutt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib e2fsprogs gnutls gpgme krb5-libs libidn2 ncurses pkgconfig-native tokyocabinet"
RPM_SONAME_REQ_mutt = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libgnutls.so.30 libgpgme.so.11 libgssapi_krb5.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 libncursesw.so.6 libsasl2.so.3 libtinfo.so.6 libtokyocabinet.so.9"
RDEPENDS_mutt = "cyrus-sasl-lib glibc gnutls gpgme krb5-libs libcom_err libidn2 mailcap ncurses-libs perl-File-Temp perl-Time-Local perl-interpreter perl-libs tokyocabinet urlview"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mutt-1.10.1-2.el8.aarch64.rpm \
          "

SRC_URI[mutt.sha256sum] = "c02fc333b7203b63246c9fe757c8cefbe9dbaed67c7c5fc2647fda1bd8cbd369"
