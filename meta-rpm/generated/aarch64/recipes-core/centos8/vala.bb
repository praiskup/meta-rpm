SUMMARY = "generated recipe based on vala srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_vala = "libvala-0.40.so.0 libvalaccodegen.so"
RPM_SONAME_REQ_vala = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libvala-0.40.so.0 libvalaccodegen.so"
RDEPENDS_vala = "bash chkconfig glib2 glibc gobject-introspection-devel pkgconf-pkg-config"
RPM_SONAME_REQ_vala-devel = "libvala-0.40.so.0"
RPROVIDES_vala-devel = "vala-dev (= 0.40.19)"
RDEPENDS_vala-devel = "glib2-devel pkgconf-pkg-config vala"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/vala-0.40.19-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/vala-devel-0.40.19-1.el8.aarch64.rpm \
          "

SRC_URI[vala.sha256sum] = "c1813922ea9e4e931f37396fb2a06efe148450df6895bfa5907602de4bc6a12c"
SRC_URI[vala-devel.sha256sum] = "f17b8ca74b6cedb68401bae2eacbc951e7e7f3d7c76f695431aebb620e9a7d6f"
