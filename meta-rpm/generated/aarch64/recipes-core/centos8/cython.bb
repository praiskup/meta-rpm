SUMMARY = "generated recipe based on Cython srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-Cython = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-Cython = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-Cython-0.28.1-3.el8.aarch64.rpm \
          "

SRC_URI[python3-Cython.sha256sum] = "378394f6a3b8f74cdc3759334cac107a5eb15d060b1504185090824ec11f0b4a"
