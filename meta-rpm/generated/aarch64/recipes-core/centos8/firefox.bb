SUMMARY = "generated recipe based on firefox srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-glib dbus-libs fontconfig freetype gdk-pixbuf glib-2.0 gtk+3 gtk2 libffi libgcc libx11 libxcb libxext libxrender libxt nspr nss pango pkgconfig-native zlib"
RPM_SONAME_PROV_firefox = "libclearkey.so liblgpllibs.so libmozavcodec.so libmozavutil.so libmozgtk.so libmozsqlite3.so libmozwayland.so libxul.so"
RPM_SONAME_REQ_firefox = "ld-linux-aarch64.so.1 libX11-xcb.so.1 libX11.so.6 libXext.so.6 libXrender.so.1 libXt.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libffi.so.6 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgtk-x11-2.0.so.0 liblgpllibs.so libm.so.6 libmozavutil.so libmozgtk.so libmozsqlite3.so libmozwayland.so libnspr4.so libnss3.so libnssutil3.so libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libplc4.so libplds4.so libpthread.so.0 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6 libxcb-shm.so.0 libxcb.so.1 libxul.so libz.so.1"
RDEPENDS_firefox = "atk bash cairo cairo-gobject centos-indexhtml dbus-glib dbus-libs fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 gtk3 libX11 libX11-xcb libXext libXrender libXt liberation-fonts-common liberation-sans-fonts libffi libgcc libstdc++ libxcb mozilla-filesystem nspr nss nss-util p11-kit-trust pango zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/firefox-78.4.0-1.el8_2.aarch64.rpm \
          "

SRC_URI[firefox.sha256sum] = "ba891131ecd9e99df19bd4ca89376b946480db512f986d39899d46b0ea694124"
