SUMMARY = "generated recipe based on openssh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk audit cairo e2fsprogs fipscheck fontconfig freetype gdk-pixbuf glib-2.0 gtk2 krb5-libs libedit libselinux libx11 libxcrypt openldap openssl pam pango pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_openssh = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh = "audit-libs bash fipscheck-lib glibc libselinux libxcrypt openssl-libs util-linux zlib"
RPM_SONAME_REQ_openssh-askpass = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_openssh-askpass = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libX11 openssh pango"
RPM_SONAME_REQ_openssh-cavs = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-cavs = "fipscheck-lib glibc libselinux libxcrypt openssh openssl-libs perl-interpreter perl-libs zlib"
RPM_SONAME_REQ_openssh-clients = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libedit.so.0 libfipscheck.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-clients = "bash crypto-policies fipscheck-lib glibc krb5-libs libcom_err libedit libselinux libxcrypt openssh openssl-libs zlib"
RPM_SONAME_REQ_openssh-keycat = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libpam.so.0 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-keycat = "fipscheck-lib glibc libselinux libxcrypt openssh openssl-libs pam zlib"
RPM_SONAME_REQ_openssh-ldap = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-ldap = "bash fipscheck-lib glibc libselinux libxcrypt openldap openssh openssl-libs zlib"
RPM_SONAME_REQ_openssh-server = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpam.so.0 libresolv.so.2 libselinux.so.1 libsystemd.so.0 libutil.so.1 libz.so.1"
RDEPENDS_openssh-server = "audit-libs bash crypto-policies fipscheck-lib glibc krb5-libs libcom_err libselinux libxcrypt openssh openssl-libs pam shadow-utils systemd systemd-libs zlib"
RPM_SONAME_REQ_pam_ssh_agent_auth = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libpam.so.0 libresolv.so.2 libselinux.so.1 libutil.so.1"
RDEPENDS_pam_ssh_agent_auth = "glibc libselinux libxcrypt openssl-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openssh-askpass-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssh-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssh-cavs-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssh-clients-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssh-keycat-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssh-ldap-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssh-server-8.0p1-4.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pam_ssh_agent_auth-0.10.3-7.4.el8_1.aarch64.rpm \
          "

SRC_URI[openssh.sha256sum] = "44e0c98c773c8add8258c83d6bd493d87423b9ea48dd443b14c28a353095043b"
SRC_URI[openssh-askpass.sha256sum] = "3d0383907468e3907ef7e6ccbe2ee6f712dc201014a044f3b5d23cb28bf4170a"
SRC_URI[openssh-cavs.sha256sum] = "d3302790122f1848814de08793274510fbc10f6a4f7859bb80d1f144db17ff5a"
SRC_URI[openssh-clients.sha256sum] = "b74064f46423f3c1b74c43ba0b1b3396108beaf67943764f6a0fe79427b72874"
SRC_URI[openssh-keycat.sha256sum] = "c76aad707251d985f8b31fbb60b5d9e08d93455744ed81a3312acedcb6e0626d"
SRC_URI[openssh-ldap.sha256sum] = "5687f1f2409ea787454fb3314d5ae139584f80e784755c65a4261c8ba0cac18e"
SRC_URI[openssh-server.sha256sum] = "e388fdd85791e5fc157a2af880e3ae78c5a2d786a4eab48d7c186b6be5a5ea25"
SRC_URI[pam_ssh_agent_auth.sha256sum] = "1ea1027d6506e36e5295968ea2194b4bcd6e23999a197022fe650d76a12df9d4"
