SUMMARY = "generated recipe based on cheese srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo clutter clutter-gst3 clutter-gtk cogl gdk-pixbuf glib-2.0 gnome-desktop3 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 json-glib libcanberra libdrm libglvnd libx11 libxcomposite libxdamage libxext libxfixes libxi libxkbcommon libxrandr mesa pango pkgconfig-native wayland"
RPM_SONAME_PROV_cheese-libs = "libcheese-gtk.so.25 libcheese.so.8"
RPM_SONAME_REQ_cheese-libs = "ld-linux-aarch64.so.1 libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libcheese.so.8 libclutter-1.0.so.0 libclutter-gst-3.0.so.0 libclutter-gtk-1.0.so.0 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libdrm.so.2 libgbm.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxkbcommon.so.0"
RDEPENDS_cheese-libs = "atk cairo cairo-gobject clutter clutter-gst3 clutter-gtk cogl gdk-pixbuf2 glib2 glibc gnome-desktop3 gstreamer1 gstreamer1-plugins-base gtk3 json-glib libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libcanberra libcanberra-gtk3 libdrm libglvnd-egl libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon mesa-libgbm pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cheese-libs-3.28.0-1.el8.aarch64.rpm \
          "

SRC_URI[cheese-libs.sha256sum] = "ea035c21792b5c2421754d22df592262a4ab01dd9f9669e608e9be882596c346"
