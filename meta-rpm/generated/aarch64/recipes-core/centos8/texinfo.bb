SUMMARY = "generated recipe based on texinfo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_info = "ld-linux-aarch64.so.1 libc.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_info = "bash glibc ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/info-6.5-6.el8.aarch64.rpm \
          "

SRC_URI[info.sha256sum] = "187a1fbb7e2992dfa777c7ca5c2f7369ecb85e4be4a483e6c0c6036e02bacf95"
