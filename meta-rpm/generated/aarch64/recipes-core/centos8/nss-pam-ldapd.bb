SUMMARY = "generated recipe based on nss-pam-ldapd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "krb5-libs libgcc openldap pam pkgconfig-native"
RPM_SONAME_PROV_nss-pam-ldapd = "libnss_ldap.so.2"
RPM_SONAME_REQ_nss-pam-ldapd = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 liblber-2.4.so.2 libldap_r-2.4.so.2 libnss_ldap.so.2 libpam.so.0 libpthread.so.0"
RDEPENDS_nss-pam-ldapd = "bash glibc krb5-libs libgcc openldap pam systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nss-pam-ldapd-0.9.9-3.el8.aarch64.rpm \
          "

SRC_URI[nss-pam-ldapd.sha256sum] = "cbcab7193fb45044a95820d71033b40feda38fe03d9529db255082ed0de5c193"
