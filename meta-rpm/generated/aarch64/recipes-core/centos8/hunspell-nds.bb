SUMMARY = "generated recipe based on hunspell-nds srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-nds = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-nds-0.1-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-nds.sha256sum] = "1ae7b1aa95734dade85dac7fdf947b79867ee1f194488ecdf2a0b45ce8b326a2"
