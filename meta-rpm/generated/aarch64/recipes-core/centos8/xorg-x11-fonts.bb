SUMMARY = "generated recipe based on xorg-x11-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xorg-x11-fonts-100dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-75dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-1-100dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-1-75dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-14-100dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-14-75dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-15-100dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-15-75dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-2-100dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-2-75dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-9-100dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ISO8859-9-75dpi = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-Type1 = "bash fontconfig ttmkfdir xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-cyrillic = "bash xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-ethiopic = "bash fontconfig ttmkfdir xorg-x11-font-utils"
RDEPENDS_xorg-x11-fonts-misc = "bash fontconfig xorg-x11-font-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-100dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-75dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-1-100dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-1-75dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-14-100dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-14-75dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-15-100dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-15-75dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-2-100dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-2-75dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-9-100dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ISO8859-9-75dpi-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-Type1-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-cyrillic-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-ethiopic-7.5-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-fonts-misc-7.5-19.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-fonts-100dpi.sha256sum] = "93fac3086a46a430da13c0709529f6b908a72cd3d3c36e44fd2f9ddba48738f6"
SRC_URI[xorg-x11-fonts-75dpi.sha256sum] = "f5d5540aa3cffaf3c051e3e54fc4a8845172cf87db13f162a139628695768874"
SRC_URI[xorg-x11-fonts-ISO8859-1-100dpi.sha256sum] = "723c0424ce3ec12586d390f2c6d01a2075e9f91543eb5b9a5a998636d49cb3b0"
SRC_URI[xorg-x11-fonts-ISO8859-1-75dpi.sha256sum] = "9e002b438e0082aa691522b508848e28bc8daeddf5d0976450b39f376d09e7af"
SRC_URI[xorg-x11-fonts-ISO8859-14-100dpi.sha256sum] = "43ca8667bfc1dc0c560e62b34f248c2089b39a642cec9859e947d864299751e1"
SRC_URI[xorg-x11-fonts-ISO8859-14-75dpi.sha256sum] = "c4a83caf3fc9798ba284763f9bf3f63a8475ecd57dd8009f508556bf736260c0"
SRC_URI[xorg-x11-fonts-ISO8859-15-100dpi.sha256sum] = "fc98193dc882dd8cdcb74aee12f65880de3736bcd04b09c3555e6b82b7347d2a"
SRC_URI[xorg-x11-fonts-ISO8859-15-75dpi.sha256sum] = "3283f08a8bd78a9f03043a67e9b8f473cf6f25e5c4ff76d00bdcd70a4239b452"
SRC_URI[xorg-x11-fonts-ISO8859-2-100dpi.sha256sum] = "9d6188913d76afdbff19508f8372ccf0f5926736c9eadeaeba6e56997e33a0f0"
SRC_URI[xorg-x11-fonts-ISO8859-2-75dpi.sha256sum] = "4cae2964896e5f399a4f8afd86a72f37be92154716924856ac9591b04d7ee54f"
SRC_URI[xorg-x11-fonts-ISO8859-9-100dpi.sha256sum] = "d8d157873ef3ef523b39ed28059e1386c78dec960372446e4e6600842eb8599a"
SRC_URI[xorg-x11-fonts-ISO8859-9-75dpi.sha256sum] = "b01604d7dbe338c3a9860158a08ab2c694cc91bc835978025a55b0b4efd0dfcd"
SRC_URI[xorg-x11-fonts-Type1.sha256sum] = "7903fa72df1c523b607a0d70c0e8c96aa3fd16f11e124f06d7ea0072c88a3933"
SRC_URI[xorg-x11-fonts-cyrillic.sha256sum] = "d5aa84f4a12ad980129c49e967a5705c7c1fcb54e22c0a436c0be555afc856ee"
SRC_URI[xorg-x11-fonts-ethiopic.sha256sum] = "f133b654c267322145d3806d90cbc41adb0f43ab4e43a75d9cf4125c95042a64"
SRC_URI[xorg-x11-fonts-misc.sha256sum] = "c572d43ec3ebbee731e3327b6be4f0ba84c043ba9f408c60a5f46e32346ca006"
