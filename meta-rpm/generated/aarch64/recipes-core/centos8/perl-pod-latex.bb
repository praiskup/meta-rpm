SUMMARY = "generated recipe based on perl-Pod-LaTeX srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-LaTeX = "perl-Carp perl-Getopt-Long perl-Pod-Parser perl-Pod-Usage perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Pod-LaTeX-0.61-302.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-LaTeX.sha256sum] = "69187f2fe63ecbd7ea3bd699bf340ce09cdf98f018b36b888a05b967055837d3"
