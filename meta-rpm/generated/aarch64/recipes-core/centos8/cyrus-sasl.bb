SUMMARY = "generated recipe based on cyrus-sasl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib db e2fsprogs krb5-libs libpq libxcrypt mariadb-connector-c openldap openssl pam pkgconfig-native"
RPM_SONAME_REQ_cyrus-sasl = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libpam.so.0 libresolv.so.2 libsasl2.so.3"
RDEPENDS_cyrus-sasl = "bash chkconfig cyrus-sasl-lib glibc krb5-libs libcom_err libdb libxcrypt openldap openssl-libs pam shadow-utils systemd util-linux"
RPM_SONAME_PROV_cyrus-sasl-gs2 = "libgs2.so.3"
RPM_SONAME_REQ_cyrus-sasl-gs2 = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libgs2.so.3 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-gs2 = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt"
RPM_SONAME_PROV_cyrus-sasl-gssapi = "libgssapiv2.so.3"
RPM_SONAME_REQ_cyrus-sasl-gssapi = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libgssapiv2.so.3 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-gssapi = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt"
RPM_SONAME_PROV_cyrus-sasl-ldap = "libldapdb.so.3"
RPM_SONAME_REQ_cyrus-sasl-ldap = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldapdb.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-ldap = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openldap"
RPM_SONAME_PROV_cyrus-sasl-md5 = "libcrammd5.so.3 libdigestmd5.so.3"
RPM_SONAME_REQ_cyrus-sasl-md5 = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrammd5.so.3 libcrypt.so.1 libcrypto.so.1.1 libdigestmd5.so.3 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-md5 = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openssl-libs"
RPM_SONAME_PROV_cyrus-sasl-ntlm = "libntlm.so.3"
RPM_SONAME_REQ_cyrus-sasl-ntlm = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libntlm.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-ntlm = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openssl-libs"
RPM_SONAME_PROV_cyrus-sasl-plain = "liblogin.so.3 libplain.so.3"
RPM_SONAME_REQ_cyrus-sasl-plain = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblogin.so.3 libplain.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-plain = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt"
RPM_SONAME_PROV_cyrus-sasl-scram = "libscram.so.3"
RPM_SONAME_REQ_cyrus-sasl-scram = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libscram.so.3"
RDEPENDS_cyrus-sasl-scram = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openssl-libs"
RPM_SONAME_PROV_cyrus-sasl-sql = "libsql.so.3"
RPM_SONAME_REQ_cyrus-sasl-sql = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libmariadb.so.3 libpq.so.5 libresolv.so.2 libsql.so.3"
RDEPENDS_cyrus-sasl-sql = "cyrus-sasl-lib glibc krb5-libs libcom_err libpq libxcrypt mariadb-connector-c"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cyrus-sasl-sql-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-gs2-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-gssapi-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-ldap-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-md5-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-ntlm-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-plain-2.1.27-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cyrus-sasl-scram-2.1.27-1.el8.aarch64.rpm \
          "

SRC_URI[cyrus-sasl.sha256sum] = "2e51af20f4431b2bb70f6a295e45734c9975727c75ca495da4fc7b028fa8df1f"
SRC_URI[cyrus-sasl-gs2.sha256sum] = "884348410d0de5daff16adc4f7f71be3498cc2b885342596b8b8c82b050d26bd"
SRC_URI[cyrus-sasl-gssapi.sha256sum] = "758ceca2037715059b751535650afd9aca4dfeea2fc8062d534d737d70fe69b2"
SRC_URI[cyrus-sasl-ldap.sha256sum] = "279be8539e96302b3a51110cb190e17e1f8b7a88c77d5ed680fb037972db2975"
SRC_URI[cyrus-sasl-md5.sha256sum] = "a570d5d291e5d7b00290f60add4330d3032b131764337899f68f5bb8cda5180b"
SRC_URI[cyrus-sasl-ntlm.sha256sum] = "fc228cad3869b8d8d32fa86e31e2f971ab105c12d01ea0f38bda620fdccb8e86"
SRC_URI[cyrus-sasl-plain.sha256sum] = "44b093c3af5c59afb0d6dabf9e5cb2811f40cc641d1628ae662e24569b3666b9"
SRC_URI[cyrus-sasl-scram.sha256sum] = "aaeae22c2f18d1395af29a925c10c199db73ee25375a599d357f2e0e7ba840f8"
SRC_URI[cyrus-sasl-sql.sha256sum] = "62a383159ca9254eb1954a38ca5e97d5521f2c40e516217d06670942d8cf6be2"
