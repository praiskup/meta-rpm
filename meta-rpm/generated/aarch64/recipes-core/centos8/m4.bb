SUMMARY = "generated recipe based on m4 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_m4 = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_m4 = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/m4-1.4.18-7.el8.aarch64.rpm \
          "

SRC_URI[m4.sha256sum] = "8b4245a3c270c890f0264219abf38a1460de472588c3fa5a47bdb77f45eb60ae"
