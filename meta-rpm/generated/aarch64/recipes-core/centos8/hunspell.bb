SUMMARY = "generated recipe based on hunspell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc ncurses pkgconfig-native"
RPM_SONAME_PROV_hunspell = "libhunspell-1.6.so.0"
RPM_SONAME_REQ_hunspell = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libhunspell-1.6.so.0 libm.so.6 libncursesw.so.6 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_hunspell = "glibc hunspell-en-US libgcc libstdc++ ncurses-libs"
RPM_SONAME_REQ_hunspell-devel = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libhunspell-1.6.so.0 libm.so.6 libstdc++.so.6"
RPROVIDES_hunspell-devel = "hunspell-dev (= 1.6.2)"
RDEPENDS_hunspell-devel = "bash glibc hunspell libgcc libstdc++ perl-Getopt-Long perl-interpreter pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-1.6.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-devel-1.6.2-1.el8.aarch64.rpm \
          "

SRC_URI[hunspell.sha256sum] = "2e1b37fa6da1045f3efb4e71b58d53951ae8c0e15f4973048fd66bf46f5d1dbf"
SRC_URI[hunspell-devel.sha256sum] = "b34b9057695fcc74918c6872dd6579db37db8a437b968b63b6d52b64f9d9e7da"
