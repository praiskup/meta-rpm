SUMMARY = "generated recipe based on make srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/make"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_make = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_make = "bash glibc info"
RPROVIDES_make-devel = "make-dev (= 4.2.1)"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/make-4.2.1-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/make-devel-4.2.1-10.el8.aarch64.rpm \
          "

SRC_URI[make.sha256sum] = "5260db4306bbed3f2cf77f76eda0bd77d5f06e365c1833a2423285d6997ade1e"
SRC_URI[make-devel.sha256sum] = "62f728243c0f27431f6b42f70fae15ad02b0ec4946aa4fe1a02c7c28f80cca71"
