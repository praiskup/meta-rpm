SUMMARY = "generated recipe based on fribidi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_fribidi = "libfribidi.so.0"
RPM_SONAME_REQ_fribidi = "ld-linux-aarch64.so.1 libc.so.6 libfribidi.so.0"
RDEPENDS_fribidi = "glibc"
RPM_SONAME_REQ_fribidi-devel = "libfribidi.so.0"
RPROVIDES_fribidi-devel = "fribidi-dev (= 1.0.4)"
RDEPENDS_fribidi-devel = "fribidi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fribidi-1.0.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fribidi-devel-1.0.4-8.el8.aarch64.rpm \
          "

SRC_URI[fribidi.sha256sum] = "58c7fb4c6d4302ed345752e4e3caea2e878afc660e9210c317053fafe73806bf"
SRC_URI[fribidi-devel.sha256sum] = "3dacfda770181f98e1820ae932e660790561d1a712f816ac21144d76b6316845"
