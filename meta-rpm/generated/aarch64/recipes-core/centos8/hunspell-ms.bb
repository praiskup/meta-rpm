SUMMARY = "generated recipe based on hunspell-ms srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ms = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ms-0.20050117-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-ms.sha256sum] = "75920fb9fae649ce4818b3399faf688758b3253a9ae8d8c98637bf0c9deb9842"
