SUMMARY = "generated recipe based on rubygem-bson srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native ruby"
RPM_SONAME_REQ_rubygem-bson = "ld-linux-aarch64.so.1 libc.so.6 libruby.so.2.5"
RDEPENDS_rubygem-bson = "glibc ruby-libs rubygem-bigdecimal rubygems"
RDEPENDS_rubygem-bson-doc = "rubygem-bson"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-bson-4.3.0-2.module_el8.1.0+214+9be47fd7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-bson-doc-4.3.0-2.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-bson.sha256sum] = "f6b5609c89b621f8120d5faaecc1cb4ec786fb690a21930bd5b4d338c4ae898e"
SRC_URI[rubygem-bson-doc.sha256sum] = "61225ffccd81a172428af5c13495c64e2484e9d88109e52eea82b625750c54ec"
