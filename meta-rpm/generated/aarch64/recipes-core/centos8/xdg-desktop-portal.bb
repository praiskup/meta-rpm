SUMMARY = "generated recipe based on xdg-desktop-portal srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype fuse geoclue2 glib-2.0 json-glib libgcc pipewire pkgconfig-native"
RPM_SONAME_REQ_xdg-desktop-portal = "ld-linux-aarch64.so.1 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libfuse.so.2 libgcc_s.so.1 libgeoclue-2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpipewire-0.2.so.1 libpthread.so.0"
RDEPENDS_xdg-desktop-portal = "bash dbus fontconfig freetype fuse fuse-libs geoclue2 geoclue2-libs glib2 glibc json-glib libgcc pipewire-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xdg-desktop-portal-1.4.2-1.el8.aarch64.rpm \
          "

SRC_URI[xdg-desktop-portal.sha256sum] = "5b932bed67032ccf580ab7e733df8c60a206a414243c3c92653a96ba5279245d"
