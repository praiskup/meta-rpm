SUMMARY = "generated recipe based on libgit2-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgit2 pkgconfig-native"
RPM_SONAME_PROV_libgit2-glib = "libgit2-glib-1.0.so.0"
RPM_SONAME_REQ_libgit2-glib = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libgit2.so.26 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libgit2-glib = "glib2 glibc libgit2 platform-python python3-gobject"
RPM_SONAME_REQ_libgit2-glib-devel = "libgit2-glib-1.0.so.0"
RPROVIDES_libgit2-glib-devel = "libgit2-glib-dev (= 0.26.4)"
RDEPENDS_libgit2-glib-devel = "glib2-devel libgit2-devel libgit2-glib pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgit2-glib-0.26.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgit2-glib-devel-0.26.4-2.el8.aarch64.rpm \
          "

SRC_URI[libgit2-glib.sha256sum] = "9dbef85e27d9e2aa41dbc8a3e61c2ccd647c315fb129020c6825ab40b596898d"
SRC_URI[libgit2-glib-devel.sha256sum] = "dcac85aa3982a52335568aecde4fbf583197c561ea6e21eaaaf445ddadeb70a1"
