SUMMARY = "generated recipe based on freetype srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 libpng pkgconfig-native zlib"
RPM_SONAME_PROV_freetype = "libfreetype.so.6"
RPM_SONAME_REQ_freetype = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_freetype = "bash bzip2-libs glibc libpng zlib"
RPM_SONAME_REQ_freetype-devel = "libfreetype.so.6"
RPROVIDES_freetype-devel = "freetype-dev (= 2.9.1)"
RDEPENDS_freetype-devel = "bash bzip2-devel freetype libpng-devel pkgconf pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/freetype-2.9.1-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/freetype-devel-2.9.1-4.el8.aarch64.rpm \
          "

SRC_URI[freetype.sha256sum] = "b07f15c1664aebecde998ae5c704e86f7869171d79726f3748ed8e5c60d9b145"
SRC_URI[freetype-devel.sha256sum] = "6f64563df703c7767bbf7cec84d6fa872ca0388fad3d8aaba26b989a70986eec"
