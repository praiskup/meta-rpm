SUMMARY = "generated recipe based on hyphen-hi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-hi = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-hi-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-hi.sha256sum] = "4f9030d8fbb11e56eb54dace95385fbc9edfa00f4b9a01d6ce840768b29cc795"
