SUMMARY = "generated recipe based on python-sphinx-theme-alabaster srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sphinx-theme-alabaster = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-sphinx-theme-alabaster-0.7.9-7.el8.noarch.rpm \
          "

SRC_URI[python3-sphinx-theme-alabaster.sha256sum] = "e7b1e8c31a95318548cc80efa9c054972d3de9b82716d594b74c2a077e5f3f2f"
