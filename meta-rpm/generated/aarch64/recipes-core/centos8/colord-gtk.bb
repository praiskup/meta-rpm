SUMMARY = "generated recipe based on colord-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo colord gdk-pixbuf glib-2.0 gtk+3 lcms2 pango pkgconfig-native"
RPM_SONAME_PROV_colord-gtk = "libcolord-gtk.so.1"
RPM_SONAME_REQ_colord-gtk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcolord-gtk.so.1 libcolord.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 liblcms2.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_colord-gtk = "atk cairo cairo-gobject colord-libs gdk-pixbuf2 glib2 glibc gtk3 lcms2 pango"
RPM_SONAME_REQ_colord-gtk-devel = "libcolord-gtk.so.1"
RPROVIDES_colord-gtk-devel = "colord-gtk-dev (= 0.1.26)"
RDEPENDS_colord-gtk-devel = "colord-devel colord-gtk gtk3-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/colord-gtk-0.1.26-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/colord-gtk-devel-0.1.26-8.el8.aarch64.rpm \
          "

SRC_URI[colord-gtk.sha256sum] = "b2c193e9683c6042800e34afeae7b9f79ecd85ca0dee7006e00713db9286d30a"
SRC_URI[colord-gtk-devel.sha256sum] = "77e85f7e1f734defb38747742c42849a9029aaa06d7d114ed5620791ac737ad5"
