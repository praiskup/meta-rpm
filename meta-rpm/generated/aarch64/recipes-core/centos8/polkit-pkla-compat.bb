SUMMARY = "generated recipe based on polkit-pkla-compat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native polkit"
RPM_SONAME_REQ_polkit-pkla-compat = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_polkit-pkla-compat = "glib2 glibc polkit polkit-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/polkit-pkla-compat-0.1-12.el8.aarch64.rpm \
          "

SRC_URI[polkit-pkla-compat.sha256sum] = "d25d562fe77f391458903ebf0d9078b6d38af6d9ced39d902b9afc7e717d2234"
