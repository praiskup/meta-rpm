SUMMARY = "generated recipe based on nvme-cli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libuuid pkgconfig-native"
RPM_SONAME_REQ_nvme-cli = "ld-linux-aarch64.so.1 libc.so.6 libuuid.so.1"
RDEPENDS_nvme-cli = "bash glibc libuuid"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nvme-cli-1.9-7.el8_2.aarch64.rpm \
          "

SRC_URI[nvme-cli.sha256sum] = "e9053dd6ff854e67644fe39d44e82cefbe4f048b57c1e81b8cfd34cfbe8c4bf8"
