SUMMARY = "generated recipe based on hunspell-qu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-qu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-qu-0.9-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-qu.sha256sum] = "3e05ad8914e1af345cb3b958d9522e8b5f24fe6b9b15ab22bda3b70203f6510a"
