SUMMARY = "generated recipe based on perl-Digest-SHA srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-SHA = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-SHA = "glibc perl-Carp perl-Digest perl-Exporter perl-Getopt-Long perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Digest-SHA-6.02-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Digest-SHA.sha256sum] = "f593041ff505c3968884423224c15b296711abfc7255998d347d1ffcfd920ed9"
