SUMMARY = "generated recipe based on sblim-sfcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl pkgconfig-native"
RPM_SONAME_PROV_sblim-sfcc = "libcimcClientXML.so.0 libcimcclient.so.0 libcmpisfcc.so.1"
RPM_SONAME_REQ_sblim-sfcc = "ld-linux-aarch64.so.1 libc.so.6 libcimcClientXML.so.0 libcimcclient.so.0 libcurl.so.4 libdl.so.2 libpthread.so.0"
RDEPENDS_sblim-sfcc = "glibc libcurl"
RPM_SONAME_REQ_sblim-sfcc-devel = "libcimcclient.so.0 libcmpisfcc.so.1"
RPROVIDES_sblim-sfcc-devel = "sblim-sfcc-dev (= 2.2.8)"
RDEPENDS_sblim-sfcc-devel = "sblim-sfcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sblim-sfcc-2.2.8-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sblim-sfcc-devel-2.2.8-9.el8.aarch64.rpm \
          "

SRC_URI[sblim-sfcc.sha256sum] = "5469ab28a43f3b26b005b4e7712b6dd05ab904f5d26edcb470e1043e2fa52086"
SRC_URI[sblim-sfcc-devel.sha256sum] = "19cf0197830c0566c440459a683daa1ca49997789b5d11e707e2f7d597f987a1"
