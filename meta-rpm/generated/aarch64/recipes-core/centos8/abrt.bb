SUMMARY = "generated recipe based on abrt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-libs gdk-pixbuf glib-2.0 gtk+3 json-c libcap libnotify libreport libreport-gtk libselinux pango pkgconfig-native platform-python3 polkit rpm satyr systemd-libs"
RPM_SONAME_REQ_abrt = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libreport.so.0 librpm.so.8 librpmio.so.8 libsatyr.so.3"
RDEPENDS_abrt = "abrt-libs bash dmidecode glib2 glibc json-c libreport libreport-plugin-rhtsupport libreport-plugin-ureport platform-python python3-abrt python3-augeas python3-dbus rpm-libs satyr shadow-utils systemd"
RPM_SONAME_REQ_abrt-addon-ccpp = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-addon-ccpp = "abrt abrt-addon-coredump-helper abrt-libs bash cpio elfutils gdb-headless glib2 glibc libreport platform-python python3-libreport rpm satyr systemd-libs"
RPM_SONAME_REQ_abrt-addon-coredump-helper = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libcap.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libselinux.so.1"
RDEPENDS_abrt-addon-coredump-helper = "abrt-libs bash glib2 glibc libcap libreport libselinux satyr"
RPM_SONAME_REQ_abrt-addon-kerneloops = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-addon-kerneloops = "abrt abrt-libs bash curl glib2 glibc libreport satyr systemd-libs"
RPM_SONAME_REQ_abrt-addon-pstoreoops = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-addon-pstoreoops = "abrt abrt-addon-kerneloops abrt-libs bash glib2 glibc libreport platform-python satyr"
RDEPENDS_abrt-addon-vmcore = "abrt abrt-addon-kerneloops bash kexec-tools platform-python python3-abrt python3-augeas util-linux"
RPM_SONAME_REQ_abrt-addon-xorg = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-addon-xorg = "abrt abrt-libs bash curl glib2 glibc libreport satyr systemd-libs"
RDEPENDS_abrt-cli = "abrt abrt-addon-ccpp abrt-addon-kerneloops abrt-addon-pstoreoops abrt-addon-vmcore abrt-addon-xorg abrt-tui libreport-plugin-rhtsupport libreport-rhel python3-abrt-addon"
RDEPENDS_abrt-cli-ng = "abrt abrt-addon-ccpp abrt-dbus abrt-libs libreport-cli platform-python python3-abrt python3-argcomplete python3-argh python3-humanize"
RDEPENDS_abrt-console-notification = "abrt abrt-cli"
RPM_SONAME_REQ_abrt-dbus = "ld-linux-aarch64.so.1 libabrt.so.0 libabrt_dbus.so.0 libc.so.6 libdbus-1.so.3 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-dbus = "abrt abrt-libs bash dbus-libs glib2 glibc libreport polkit-libs satyr"
RDEPENDS_abrt-desktop = "abrt abrt-addon-ccpp abrt-addon-kerneloops abrt-addon-pstoreoops abrt-addon-vmcore abrt-addon-xorg abrt-gui gdb-headless gnome-abrt libreport-plugin-rhtsupport libreport-rhel python3-abrt-addon"
RPM_SONAME_REQ_abrt-gui = "ld-linux-aarch64.so.1 libabrt.so.0 libabrt_dbus.so.0 libabrt_gui.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpolkit-gobject-1.so.0 libreport-gtk.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-gui = "abrt abrt-dbus abrt-gui-libs abrt-libs atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnome-abrt gsettings-desktop-schemas gtk3 libnotify libreport libreport-gtk pango polkit-libs satyr"
RPM_SONAME_PROV_abrt-gui-libs = "libabrt_gui.so.0"
RPM_SONAME_REQ_abrt-gui-libs = "ld-linux-aarch64.so.1 libabrt.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-gui-libs = "abrt-libs atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libreport pango satyr"
RPM_SONAME_PROV_abrt-libs = "libabrt.so.0"
RPM_SONAME_REQ_abrt-libs = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-libs = "glib2 glibc libreport satyr"
RDEPENDS_abrt-plugin-machine-id = "abrt platform-python"
RDEPENDS_abrt-plugin-sosreport = "abrt sos"
RPM_SONAME_REQ_abrt-tui = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-tui = "abrt abrt-dbus abrt-libs glib2 glibc libreport libreport-cli polkit-libs satyr"
RPM_SONAME_REQ_python3-abrt = "ld-linux-aarch64.so.1 libabrt.so.0 libc.so.6 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreport.so.0 libsatyr.so.3 libutil.so.1"
RDEPENDS_python3-abrt = "abrt abrt-dbus abrt-libs glib2 glibc libreport platform-python python3-dbus python3-gobject-base python3-libreport python3-libs satyr"
RDEPENDS_python3-abrt-addon = "abrt bash platform-python python3-abrt python3-systemd"
RDEPENDS_python3-abrt-container-addon = "container-exception-logger platform-python"
RDEPENDS_python3-abrt-doc = "abrt platform-python python3-abrt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-addon-ccpp-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-addon-coredump-helper-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-addon-kerneloops-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-addon-pstoreoops-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-addon-vmcore-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-addon-xorg-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-cli-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-cli-ng-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-console-notification-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-dbus-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-desktop-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-gui-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-gui-libs-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-libs-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-plugin-machine-id-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-plugin-sosreport-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/abrt-tui-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-abrt-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-abrt-addon-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-abrt-container-addon-2.10.9-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-abrt-doc-2.10.9-11.el8.noarch.rpm \
          "

SRC_URI[abrt.sha256sum] = "1b1a338726dd49184e28013a891190071446d9d283330c71ac802475b13137ec"
SRC_URI[abrt-addon-ccpp.sha256sum] = "24e1437ac9441da756c82ab5607c97c0f90bfa68fc60245ae3fc541fac4e3a2f"
SRC_URI[abrt-addon-coredump-helper.sha256sum] = "41c146f797d9cf14364b62f194cf25e392058b4c05fe99bdf7ec3087ea98bc9f"
SRC_URI[abrt-addon-kerneloops.sha256sum] = "8ab316b43699a492ceceb15a376d1beac48c80b0a3c8e212e61bddd2c428cc5f"
SRC_URI[abrt-addon-pstoreoops.sha256sum] = "5d104668d8318b0d3e239a13725f5ac91d1ae772008b8330cf4cf82e6191dd56"
SRC_URI[abrt-addon-vmcore.sha256sum] = "b9753b0dfa5564eeb5bad71b897ab866f95f86fe37105e15007f42a27eb2cd7e"
SRC_URI[abrt-addon-xorg.sha256sum] = "ecab1cb9b832684232ed5a5ec5569f15d040a1eb95ca0df2fd114632fc058e90"
SRC_URI[abrt-cli.sha256sum] = "463cfbf6fa7a22ef2732b6af9d38583191466e7cee197e0c5495dde563b1b9f8"
SRC_URI[abrt-cli-ng.sha256sum] = "444aa4bdced0afbb8b33daf70d5104bb25f0c7ae472527d0c62601007879a9ea"
SRC_URI[abrt-console-notification.sha256sum] = "5750eabb2a4cbceef3ea48d89de4707831559b8186739b820e901351fb2938f6"
SRC_URI[abrt-dbus.sha256sum] = "ec38c54387c200df392e1c061685d0d28e6d28d9b0847ef2a4e69bcdc36ce21e"
SRC_URI[abrt-desktop.sha256sum] = "1eaf997caa0dd8a0e4573eaf95ce4247b52606aa161e5c3f9004047ae0e3608d"
SRC_URI[abrt-gui.sha256sum] = "0860b4ccbda06016f4ed3307d4737c5decbb100aeb190ba7270f1b038409089f"
SRC_URI[abrt-gui-libs.sha256sum] = "56bf7ea012936d05046a9fcb7cc4265c424c19d7d5790e6f011c98bbd9065790"
SRC_URI[abrt-libs.sha256sum] = "664e4895972eb1eee7bcc27fb9a05e227310240688cdba33a7f09f93c9c93427"
SRC_URI[abrt-plugin-machine-id.sha256sum] = "39dbfe8d7314ac72d4836ee23157c8b61faeb0e1424eeeeee56e64f57d7332ba"
SRC_URI[abrt-plugin-sosreport.sha256sum] = "f45da3633f35c1d8ca510a84bb91a9c7d46fbfb9fc3e165f34968676a7e55b6e"
SRC_URI[abrt-tui.sha256sum] = "576bd7970d08b4ddb362118d4071b115d2647557f59045f555a871a7f1dcf3da"
SRC_URI[python3-abrt.sha256sum] = "e878c5ff128b09a215444c1ac31d3617563044cd8bf4bebe8b073ee0951ea77e"
SRC_URI[python3-abrt-addon.sha256sum] = "922c3ff7006459b166b22faf5c1d44d1b38167060ebdfcf36eaa12c93dcf2b45"
SRC_URI[python3-abrt-container-addon.sha256sum] = "34d750b8fc13976a121c03b795d13a03467ea69898af48426d56c89f12dbfad3"
SRC_URI[python3-abrt-doc.sha256sum] = "5cb0ad44b193b226ba5b63f648127f5070478db1d30cb9cbc415930bd2283376"
