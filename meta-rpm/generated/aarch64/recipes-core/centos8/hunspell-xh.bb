SUMMARY = "generated recipe based on hunspell-xh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-xh = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-xh-0.20091030-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-xh.sha256sum] = "68fabe49cb66cf6aea5a7cf2aa14b28802d4f6650d9b6cabe761a9c5c56418e4"
