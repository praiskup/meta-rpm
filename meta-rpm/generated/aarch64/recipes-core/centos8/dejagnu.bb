SUMMARY = "generated recipe based on dejagnu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_dejagnu = "bash expect"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dejagnu-1.6.1-2.el8.noarch.rpm \
          "

SRC_URI[dejagnu.sha256sum] = "de70e28feda6e550d2d80524cad4c95bd4ab1037fb890bfa26cd8f505f34cbca"
