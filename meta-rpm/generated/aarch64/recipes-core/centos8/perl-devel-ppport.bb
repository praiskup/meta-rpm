SUMMARY = "generated recipe based on perl-Devel-PPPort srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-PPPort = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-PPPort = "glibc perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Devel-PPPort-3.36-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Devel-PPPort.sha256sum] = "0863c07e247b9e540c5b89257a9125ad2b3f5c0606fcaeab1c40d5a84974a1f7"
