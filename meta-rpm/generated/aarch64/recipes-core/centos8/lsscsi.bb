SUMMARY = "generated recipe based on lsscsi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lsscsi = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lsscsi = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lsscsi-0.30-1.el8.aarch64.rpm \
          "

SRC_URI[lsscsi.sha256sum] = "54a0cc194a5c31f4f45d1d2ccf9790e51529d86f0aaf083092096e09011d7bab"
