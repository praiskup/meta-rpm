SUMMARY = "generated recipe based on rubygem-rspec-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec-core = "ruby rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rubygem-rspec-core-3.7.1-5.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec-core.sha256sum] = "082c72061b5ffc4ba434eaa6d73eae2b5496e3555507468f05badf329b51c366"
