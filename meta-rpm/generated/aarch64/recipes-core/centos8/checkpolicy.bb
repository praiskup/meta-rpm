SUMMARY = "generated recipe based on checkpolicy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_checkpolicy = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_checkpolicy = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/checkpolicy-2.9-1.el8.aarch64.rpm \
          "

SRC_URI[checkpolicy.sha256sum] = "01b89be34e48d345ba14a3856bba0d1ff94e79798b5f7529a6a0803b97adca15"
