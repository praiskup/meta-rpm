SUMMARY = "generated recipe based on mcstrans srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap libpcre libselinux pkgconfig-native"
RPM_SONAME_REQ_mcstrans = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libpcre.so.1 libselinux.so.1"
RDEPENDS_mcstrans = "bash glibc libcap libselinux pcre systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mcstrans-2.9-2.el8.aarch64.rpm \
          "

SRC_URI[mcstrans.sha256sum] = "c66d9d8b5dafd6ffdafcb3177614f82f73db0dc3b2c715de0499fa57cfc4e2c6"
