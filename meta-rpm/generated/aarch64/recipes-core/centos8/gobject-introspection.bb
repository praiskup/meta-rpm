SUMMARY = "generated recipe based on gobject-introspection srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libffi pkgconfig-native"
RPM_SONAME_PROV_gobject-introspection = "libgirepository-1.0.so.1"
RPM_SONAME_REQ_gobject-introspection = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0"
RDEPENDS_gobject-introspection = "glib2 glibc libffi"
RPM_SONAME_REQ_gobject-introspection-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0"
RPROVIDES_gobject-introspection-devel = "gobject-introspection-dev (= 1.56.1)"
RDEPENDS_gobject-introspection-devel = "glib2 glib2-devel glibc gobject-introspection libffi libffi-devel libtool pkgconf-pkg-config platform-python python3-mako"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gobject-introspection-1.56.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gobject-introspection-devel-1.56.1-1.el8.aarch64.rpm \
          "

SRC_URI[gobject-introspection.sha256sum] = "38b18b20b348adabd9df71ebf378a56c805f086a46b3fb89f2ed5e35f5505417"
SRC_URI[gobject-introspection-devel.sha256sum] = "65d603eddfa9e65c3746d8d7623cc37f441a52265e7f8a0508979cfb76226a43"
