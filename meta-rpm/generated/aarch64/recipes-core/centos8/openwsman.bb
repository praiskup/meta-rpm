SUMMARY = "generated recipe based on openwsman srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc libxcrypt libxml2 openssl pam pkgconfig-native ruby sblim-sfcc"
RPM_SONAME_REQ_libwsman-devel = "libwsman.so.1 libwsman_client.so.4 libwsman_clientpp.so.1 libwsman_curl_client_transport.so.1 libwsman_server.so.1"
RPROVIDES_libwsman-devel = "libwsman-dev (= 2.6.5)"
RDEPENDS_libwsman-devel = "libcurl-devel libwsman1 libxml2-devel openwsman-client openwsman-server pam-devel pkgconf-pkg-config sblim-sfcc-devel"
RPM_SONAME_PROV_libwsman1 = "libwsman.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1"
RPM_SONAME_REQ_libwsman1 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libpthread.so.0 libssl.so.1.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1 libxml2.so.2"
RDEPENDS_libwsman1 = "glibc libcurl libxml2 openssl-libs"
RPM_SONAME_PROV_openwsman-client = "libwsman_clientpp.so.1"
RPM_SONAME_REQ_openwsman-client = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_openwsman-client = "glibc libgcc libstdc++"
RPM_SONAME_REQ_openwsman-python3 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libpthread.so.0 libssl.so.1.1 libwsman.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1 libxml2.so.2"
RDEPENDS_openwsman-python3 = "glibc libcurl libwsman1 libxml2 openssl-libs platform-python"
RPM_SONAME_PROV_openwsman-server = "libredirect.so.1 libwsman_cim_plugin.so.1 libwsman_file_auth.so.1 libwsman_identify_plugin.so.1 libwsman_pam_auth.so.1 libwsman_ruby_plugin.so libwsman_server.so.1 libwsman_test.so.1"
RPM_SONAME_REQ_openwsman-server = "ld-linux-aarch64.so.1 libc.so.6 libcmpisfcc.so.1 libcrypt.so.1 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libpam.so.0 libpthread.so.0 libredirect.so.1 libruby.so.2.5 libssl.so.1.1 libwsman.so.1 libwsman_cim_plugin.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1 libwsman_file_auth.so.1 libwsman_identify_plugin.so.1 libwsman_pam_auth.so.1 libwsman_server.so.1 libwsman_test.so.1 libxml2.so.2"
RDEPENDS_openwsman-server = "bash glibc libcurl libwsman1 libxcrypt libxml2 openssl-libs pam ruby-libs sblim-sfcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwsman1-2.6.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openwsman-client-2.6.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openwsman-python3-2.6.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openwsman-server-2.6.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwsman-devel-2.6.5-5.el8.aarch64.rpm \
          "

SRC_URI[libwsman-devel.sha256sum] = "36619f3b1e7517488fdacd9f941655ec51568378280e40ea6e57ac6cb0509b05"
SRC_URI[libwsman1.sha256sum] = "d75d9d62934c0c2113ebe947550ee987b4417829618c85584691134f8d35d876"
SRC_URI[openwsman-client.sha256sum] = "c0d02fed7b7c6cdb829044fa81a75c581548c18c1ef375c4a0d0d64f9964d080"
SRC_URI[openwsman-python3.sha256sum] = "9d5d89a3e73603a351c60ad50b37c0d3300abcf4bd63f9f14039a8049039b4d6"
SRC_URI[openwsman-server.sha256sum] = "0fea63bfaf498d410bbcb4f56ddc9463ff227f8d7e6b2f353ccf200623ca2786"
