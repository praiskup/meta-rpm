SUMMARY = "generated recipe based on libtalloc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcrypt pkgconfig-native platform-python3"
RPM_SONAME_PROV_libtalloc = "libtalloc.so.2"
RPM_SONAME_REQ_libtalloc = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2"
RDEPENDS_libtalloc = "glibc libxcrypt"
RPM_SONAME_REQ_libtalloc-devel = "libtalloc.so.2"
RPROVIDES_libtalloc-devel = "libtalloc-dev (= 2.2.0)"
RDEPENDS_libtalloc-devel = "libtalloc pkgconf-pkg-config"
RPM_SONAME_PROV_python3-talloc = "libpytalloc-util.cpython-36m-aarch64-linux-gnu.so.2"
RPM_SONAME_REQ_python3-talloc = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libpytalloc-util.cpython-36m-aarch64-linux-gnu.so.2 libpython3.6m.so.1.0 libtalloc.so.2 libutil.so.1"
RDEPENDS_python3-talloc = "glibc libtalloc libxcrypt platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtalloc-2.2.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtalloc-devel-2.2.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-talloc-2.2.0-7.el8.aarch64.rpm \
          "

SRC_URI[libtalloc.sha256sum] = "999bd55a3f03309c7b323ff639ccdd1173f9a431539b19dfad82982370a3a6e1"
SRC_URI[libtalloc-devel.sha256sum] = "00be8256186605debbf71c44e37acb17c0e5ebb7f962a56fda5e6c11f9ed91fc"
SRC_URI[python3-talloc.sha256sum] = "a7ea5558a683c37b0fae9e0a19f94c5712b5bd094ddd677abbbed409b81eec40"
