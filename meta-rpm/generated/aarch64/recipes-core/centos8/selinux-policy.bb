SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
RDEPENDS_selinux-policy = "bash coreutils gawk policycoreutils rpm-plugin-selinux"
RPROVIDES_selinux-policy-devel = "selinux-policy-dev (= 3.14.3)"
RDEPENDS_selinux-policy-devel = "bash checkpolicy m4 make policycoreutils-devel selinux-policy"
RDEPENDS_selinux-policy-doc = "selinux-policy"
RDEPENDS_selinux-policy-minimum = "bash coreutils policycoreutils-python-utils selinux-policy"
RDEPENDS_selinux-policy-targeted = "bash coreutils policycoreutils selinux-policy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-3.14.3-41.el8_2.8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-devel-3.14.3-41.el8_2.8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-doc-3.14.3-41.el8_2.8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-minimum-3.14.3-41.el8_2.8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-targeted-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy.sha256sum] = "318bb66d340b4fc4e0e27ab96f5b652ad75064834dc5b6f81248a2b4c3379b1f"
SRC_URI[selinux-policy-devel.sha256sum] = "3f11916b71892d95fd6da94d0f9b031e16c0c2948061a745aafb5ec86f5e0b6b"
SRC_URI[selinux-policy-doc.sha256sum] = "16e51e509804a5c08729ab790563de6408654cf2e75292ceb0fa4c6d6fe7b0c4"
SRC_URI[selinux-policy-minimum.sha256sum] = "73b4927e0dc4cb5aca0cfc9b1510294cdec5cf875289a2852715f07eb6c38666"
SRC_URI[selinux-policy-targeted.sha256sum] = "9d71f83a538fbc93c26cdb088e7fe4aa3f7c50a041b50c368719b30bf50411c4"
