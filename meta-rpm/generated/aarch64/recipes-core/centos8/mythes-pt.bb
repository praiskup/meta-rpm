SUMMARY = "generated recipe based on mythes-pt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-pt = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-pt-0.20060817-19.el8.noarch.rpm \
          "

SRC_URI[mythes-pt.sha256sum] = "9e45e914159a7c2134304d654ff026844d3d84d60dd870c5410ca6ef21b443ce"
