SUMMARY = "generated recipe based on bsh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_bsh = "bash bsf java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools jline"
RDEPENDS_bsh-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bsh-2.0-13.b6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bsh-javadoc-2.0-13.b6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bsh-manual-2.0-13.b6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[bsh.sha256sum] = "b57a04ea445cac505cfd66bebca02082296cd199699b34e4bf57bc9f8981372f"
SRC_URI[bsh-javadoc.sha256sum] = "64df060af5756a149a9b65c596c85b3265a3c8dd3270722247e62bbd91a4269e"
SRC_URI[bsh-manual.sha256sum] = "4cf5138d20dd76ce970cc7909fc38cf271bc732813d84f36fddb3b9b59c23c7c"
