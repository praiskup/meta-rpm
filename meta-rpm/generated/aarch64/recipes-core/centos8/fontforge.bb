SUMMARY = "generated recipe based on fontforge srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo fontconfig freetype giflib glib-2.0 libice libjpeg-turbo libpng libsm libspiro libtool libuninameslist libx11 libxft libxml2 pango pkgconfig-native platform-python3 readline tiff zlib"
RPM_SONAME_PROV_fontforge = "libfontforge.so.2 libfontforgeexe.so.2 libgdraw.so.5 libgioftp.so.2 libgunicode.so.4 libgutils.so.2"
RPM_SONAME_REQ_fontforge = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXft.so.2 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfontforge.so.2 libfontforgeexe.so.2 libfreetype.so.6 libgdraw.so.5 libgif.so.7 libgio-2.0.so.0 libgioftp.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libgunicode.so.4 libgutils.so.2 libjpeg.so.62 libltdl.so.7 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0 libpng16.so.16 libpthread.so.0 libpython3.6m.so.1.0 libreadline.so.7 libspiro.so.0 libtiff.so.5 libuninameslist.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_fontforge = "autotrace cairo fontconfig freetype giflib glib2 glibc hicolor-icon-theme libICE libSM libX11 libXft libjpeg-turbo libpng libspiro libtiff libtool-ltdl libuninameslist libxml2 pango platform-python python3-libs readline xdg-utils zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/fontforge-20170731-14.el8.aarch64.rpm \
          "

SRC_URI[fontforge.sha256sum] = "0c1f0d72cc978dfaa4861d6c872061a0f3f776c336af11058e11b12f08000f8d"
