SUMMARY = "generated recipe based on perl-Switch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Switch = "perl-Carp perl-Filter perl-Text-Balanced perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Switch-2.17-10.el8.noarch.rpm \
          "

SRC_URI[perl-Switch.sha256sum] = "aecb07c01107acc685486a739047dd616cff4afc250cc7467b83df2042d8ad01"
