SUMMARY = "generated recipe based on sblim-sfcCommon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sblim-sfcCommon = "libsfcUtil.so.0"
RPM_SONAME_REQ_sblim-sfcCommon = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_sblim-sfcCommon = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sblim-sfcCommon-1.0.1-13.el8.aarch64.rpm \
          "

SRC_URI[sblim-sfcCommon.sha256sum] = "7bd7f199789fff264b8d934c2f14a2a9d79c565d0948544002741f0a6b492116"
