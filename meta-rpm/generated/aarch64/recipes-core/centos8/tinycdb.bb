SUMMARY = "generated recipe based on tinycdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_tinycdb = "libcdb.so.1"
RPM_SONAME_REQ_tinycdb = "ld-linux-aarch64.so.1 libc.so.6 libcdb.so.1"
RDEPENDS_tinycdb = "glibc"
RPM_SONAME_REQ_tinycdb-devel = "libcdb.so.1"
RPROVIDES_tinycdb-devel = "tinycdb-dev (= 0.78)"
RDEPENDS_tinycdb-devel = "pkgconf-pkg-config tinycdb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tinycdb-0.78-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tinycdb-devel-0.78-9.el8.aarch64.rpm \
          "

SRC_URI[tinycdb.sha256sum] = "83ab2b35d8fd4838b8c1ef0706edef0f7f9dc1841c9405908966e953efb717a0"
SRC_URI[tinycdb-devel.sha256sum] = "152d2d0a1e3c88bc06581834a6bf0f94c1c5bcbee0c0cb621ed9a61f1df8794a"
