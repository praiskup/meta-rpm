SUMMARY = "generated recipe based on libdwarf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native zlib"
RPM_SONAME_PROV_libdwarf = "libdwarf.so.1"
RPM_SONAME_REQ_libdwarf = "ld-linux-aarch64.so.1 libc.so.6 libelf.so.1 libz.so.1"
RDEPENDS_libdwarf = "elfutils-libelf glibc zlib"
RPM_SONAME_REQ_libdwarf-devel = "libdwarf.so.1"
RPROVIDES_libdwarf-devel = "libdwarf-dev (= 20180129)"
RDEPENDS_libdwarf-devel = "libdwarf"
RDEPENDS_libdwarf-static = "libdwarf-devel"
RPM_SONAME_REQ_libdwarf-tools = "ld-linux-aarch64.so.1 libc.so.6 libdwarf.so.1 libelf.so.1 libz.so.1"
RDEPENDS_libdwarf-tools = "elfutils-libelf glibc libdwarf zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdwarf-20180129-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdwarf-devel-20180129-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdwarf-static-20180129-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdwarf-tools-20180129-4.el8.aarch64.rpm \
          "

SRC_URI[libdwarf.sha256sum] = "363c2be8a11d8ed137d2ba85b5f2c64efb181040f2030c3c0d073f946512bd3f"
SRC_URI[libdwarf-devel.sha256sum] = "1cd08acf9604b81a7eb37f60e8a3efd95582b4d3030b4f5d62fe15bccc9edcab"
SRC_URI[libdwarf-static.sha256sum] = "d5f46881bb3ee48cb9303b33c9bcb1a69b3f227de4658a1b008c8be2562960f0"
SRC_URI[libdwarf-tools.sha256sum] = "0944340bad6473624e9d973d8641c7c03d853a62eafcae8b957523b3347f097f"
