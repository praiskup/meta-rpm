SUMMARY = "generated recipe based on web-assets srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_web-assets-devel = "web-assets-dev (= 5)"
RDEPENDS_web-assets-devel = "web-assets-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/web-assets-devel-5-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/web-assets-filesystem-5-7.el8.noarch.rpm \
          "

SRC_URI[web-assets-devel.sha256sum] = "364e801dbdee1d16fc62fce3249f5104a5d206932644dfcdc11c7a8d6acd1ed5"
SRC_URI[web-assets-filesystem.sha256sum] = "47638e2c51c3c097f88172aeb5b2500667533c0cad9da53b55afa5ed2a8f8297"
