SUMMARY = "generated recipe based on python36 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPROVIDES_python36 = "python3 (= 3.6.8)"
RDEPENDS_python36 = "bash chkconfig platform-python python3-pip python3-setuptools"
RPROVIDES_python36-devel = "python3-dev (= 3.6.8) python36-dev (= 3.6.8)"
RDEPENDS_python36-devel = "bash platform-python-devel python36"
RDEPENDS_python36-rpm-macros = "python3-rpm-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python36-3.6.8-2.module_el8.1.0+245+c39af44f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python36-devel-3.6.8-2.module_el8.1.0+245+c39af44f.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python36-rpm-macros-3.6.8-2.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python36.sha256sum] = "3dc8b67868f5821e581daf293b321070c4cd1efef46b582d02c6a07af69ed300"
SRC_URI[python36-devel.sha256sum] = "a2ef6ee7eeaf0fdeee9c1d8cf72b98999678e4e2f8f25ba191268ee630bf41b4"
SRC_URI[python36-rpm-macros.sha256sum] = "a0da789613911ef63eb47d159a589fe23d2f9945392dbd87ce822dedc9a03ecd"
