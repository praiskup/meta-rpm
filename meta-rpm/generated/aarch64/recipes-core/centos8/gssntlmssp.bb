SUMMARY = "generated recipe based on gssntlmssp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs libunistring openssl pkgconfig-native samba zlib"
RPM_SONAME_REQ_gssntlmssp = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0 libssl.so.1.1 libunistring.so.2 libwbclient.so.0 libz.so.1"
RDEPENDS_gssntlmssp = "glibc krb5-libs libcom_err libunistring libwbclient openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gssntlmssp-0.7.0-6.el8.aarch64.rpm \
          "

SRC_URI[gssntlmssp.sha256sum] = "3be902c36510130622f2d5203434ded6baf58340e66db500460adab60dc5a710"
