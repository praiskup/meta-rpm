SUMMARY = "generated recipe based on xorg-x11-server-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libice libx11 libxcursor libxext libxi libxinerama libxmu libxrandr libxrender libxt libxxf86misc libxxf86vm pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-server-utils = "ld-linux-aarch64.so.1 libICE.so.6 libX11.so.6 libXcursor.so.1 libXext.so.6 libXi.so.6 libXinerama.so.1 libXmu.so.6 libXmuu.so.1 libXrandr.so.2 libXrender.so.1 libXt.so.6 libXxf86misc.so.1 libXxf86vm.so.1 libc.so.6 libm.so.6"
RDEPENDS_xorg-x11-server-utils = "glibc libICE libX11 libXcursor libXext libXi libXinerama libXmu libXrandr libXrender libXt libXxf86misc libXxf86vm mcpp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-server-utils-7.7-27.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-server-utils.sha256sum] = "68da5c01fee906e7d36a9ddd60d47924a527006313d92d133b2c58cf4109fed1"
