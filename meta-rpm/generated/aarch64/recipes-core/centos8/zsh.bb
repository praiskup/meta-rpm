SUMMARY = "generated recipe based on zsh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm libpcre ncurses pkgconfig-native"
RPM_SONAME_REQ_zsh = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgdbm.so.6 libm.so.6 libncursesw.so.6 libpcre.so.1 librt.so.1 libtinfo.so.6"
RDEPENDS_zsh = "bash coreutils gdbm-libs glibc grep info ncurses-libs pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/zsh-html-5.5.1-6.el8_1.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/zsh-5.5.1-6.el8_1.2.aarch64.rpm \
          "

SRC_URI[zsh.sha256sum] = "5db186d75041ecbd11a92c7b88da09db7530365ee349097965922078d0c53c7d"
SRC_URI[zsh-html.sha256sum] = "4688983035295c77ca41a830ff5152bbc188d3650742461b6794894f8cc7c692"
