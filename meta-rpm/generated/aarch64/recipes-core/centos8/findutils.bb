SUMMARY = "generated recipe based on findutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux pkgconfig-native"
RPM_SONAME_REQ_findutils = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libselinux.so.1"
RDEPENDS_findutils = "bash glibc info libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/findutils-4.6.0-20.el8.aarch64.rpm \
          "

SRC_URI[findutils.sha256sum] = "985479064966d05aa82010ed5b8905942e47e2bebb919c9c1bd004a28addad1d"
