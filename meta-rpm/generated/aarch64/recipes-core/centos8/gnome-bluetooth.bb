SUMMARY = "generated recipe based on gnome-bluetooth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gtk+3 libcanberra libnotify pkgconfig-native systemd systemd-libs"
RPM_SONAME_REQ_gnome-bluetooth = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-bluetooth.so.13 libgobject-2.0.so.0 libgtk-3.so.0"
RDEPENDS_gnome-bluetooth = "bluez bluez-obexd glib2 glibc gnome-bluetooth-libs gtk3 pulseaudio-module-bluetooth"
RPM_SONAME_PROV_gnome-bluetooth-libs = "libgnome-bluetooth.so.13"
RPM_SONAME_REQ_gnome-bluetooth-libs = "ld-linux-aarch64.so.1 libc.so.6 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libnotify.so.4 libpthread.so.0 libudev.so.1"
RDEPENDS_gnome-bluetooth-libs = "glib2 glibc gtk3 libcanberra libcanberra-gtk3 libnotify systemd-libs"
RPM_SONAME_REQ_gnome-bluetooth-libs-devel = "libgnome-bluetooth.so.13"
RPROVIDES_gnome-bluetooth-libs-devel = "gnome-bluetooth-libs-dev (= 3.28.2)"
RDEPENDS_gnome-bluetooth-libs-devel = "glib2-devel gnome-bluetooth gnome-bluetooth-libs gtk3-devel pkgconf-pkg-config systemd-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-bluetooth-3.28.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-bluetooth-libs-3.28.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gnome-bluetooth-libs-devel-3.28.2-2.el8.aarch64.rpm \
          "

SRC_URI[gnome-bluetooth.sha256sum] = "c8d935695acd9e7aa976a3543c05ac0a5fb585e2561927c501a48e82f3fc3326"
SRC_URI[gnome-bluetooth-libs.sha256sum] = "820dfc059013f53a05f468824feffee48391dd444b7b823eda1cd015d8d84563"
SRC_URI[gnome-bluetooth-libs-devel.sha256sum] = "785402b398257630a46c2031f13944729eaf19351705061ef27ad5356ca961bc"
