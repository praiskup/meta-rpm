SUMMARY = "generated recipe based on qt5-qtxmlpatterns srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtxmlpatterns = "libQt5XmlPatterns.so.5 libqmlxmllistmodelplugin.so"
RPM_SONAME_REQ_qt5-qtxmlpatterns = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtxmlpatterns = "glibc libgcc libstdc++ qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtxmlpatterns-devel = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Network.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtxmlpatterns-devel = "qt5-qtxmlpatterns-dev (= 5.12.5)"
RDEPENDS_qt5-qtxmlpatterns-devel = "cmake-filesystem glibc libgcc libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtxmlpatterns"
RPM_SONAME_REQ_qt5-qtxmlpatterns-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Widgets.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtxmlpatterns-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtxmlpatterns"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtxmlpatterns-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtxmlpatterns-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtxmlpatterns-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtxmlpatterns.sha256sum] = "51b04fc1fe2b25b523695a065954219e9775b1bc8af067fee852a9747367127a"
SRC_URI[qt5-qtxmlpatterns-devel.sha256sum] = "5e05e486d5300e339ef9e675598ccfaf41974b353f871236bd681392a8326e38"
SRC_URI[qt5-qtxmlpatterns-examples.sha256sum] = "97beae00c99ab3f0aade84a7edb4b010fcea6c3c321ceaf0f7319fa13a3dc2fc"
