SUMMARY = "generated recipe based on PackageKit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus fontconfig freetype gdk-pixbuf glib-2.0 gstreamer1.0 gtk+3 gtk2 libappstream-glib libdnf libgcc librepo pango pkgconfig-native polkit rpm sqlite3 systemd-libs"
RPM_SONAME_PROV_PackageKit = "libpk_backend_dnf.so libpk_backend_dummy.so libpk_backend_test_fail.so libpk_backend_test_nop.so libpk_backend_test_spawn.so libpk_backend_test_succeed.so libpk_backend_test_thread.so"
RPM_SONAME_REQ_PackageKit = "ld-linux-aarch64.so.1 libappstream-glib.so.8 libc.so.6 libdnf.so.2 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpackagekit-glib2.so.18 libpolkit-gobject-1.so.0 libpthread.so.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libsystemd.so.0"
RDEPENDS_PackageKit = "PackageKit-glib bash gdk-pixbuf2 glib2 glibc libappstream-glib libdnf libgcc librepo polkit-libs rpm-libs shared-mime-info sqlite-libs systemd systemd-libs"
RPM_SONAME_REQ_PackageKit-command-not-found = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpackagekit-glib2.so.18 libsqlite3.so.0"
RDEPENDS_PackageKit-command-not-found = "PackageKit PackageKit-glib bash glib2 glibc libgcc sqlite-libs"
RDEPENDS_PackageKit-cron = "PackageKit bash crontabs"
RPM_SONAME_PROV_PackageKit-glib = "libpackagekit-glib2.so.18"
RPM_SONAME_REQ_PackageKit-glib = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libsqlite3.so.0"
RDEPENDS_PackageKit-glib = "dbus glib2 glibc gobject-introspection libgcc sqlite-libs"
RPM_SONAME_REQ_PackageKit-glib-devel = "libpackagekit-glib2.so.18"
RPROVIDES_PackageKit-glib-devel = "PackageKit-glib-dev (= 1.1.12)"
RDEPENDS_PackageKit-glib-devel = "PackageKit-glib dbus-devel glib2-devel pkgconf-pkg-config sqlite-devel"
RPM_SONAME_REQ_PackageKit-gstreamer-plugin = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstreamer-1.0.so.0 libpackagekit-glib2.so.18 libsqlite3.so.0"
RDEPENDS_PackageKit-gstreamer-plugin = "PackageKit-glib glib2 glibc gstreamer1 libgcc sqlite-libs"
RPM_SONAME_PROV_PackageKit-gtk3-module = "libpk-gtk-module.so"
RPM_SONAME_REQ_PackageKit-gtk3-module = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_PackageKit-gtk3-module = "PackageKit-glib atk cairo cairo-gobject fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 gtk3 libgcc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/PackageKit-1.1.12-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/PackageKit-command-not-found-1.1.12-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/PackageKit-cron-1.1.12-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/PackageKit-glib-1.1.12-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/PackageKit-gstreamer-plugin-1.1.12-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/PackageKit-gtk3-module-1.1.12-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/PackageKit-glib-devel-1.1.12-4.el8.aarch64.rpm \
          "

SRC_URI[PackageKit.sha256sum] = "9972db736ed0dc74ec2860750897695353a75567fab21afa44687e35afad1ef8"
SRC_URI[PackageKit-command-not-found.sha256sum] = "6ff412fbddea4bb81feda36dd7a5a0ebf610d64b2bc3a71cbe2762915dc0c7e0"
SRC_URI[PackageKit-cron.sha256sum] = "8304c4663ccd9373bfde5c85cbb56ba87766a14d7a3d37368d37be0ed09183a2"
SRC_URI[PackageKit-glib.sha256sum] = "68c5d2f874f31f20490ed9933d22be2f082bb3f739265360f34652d9f3480613"
SRC_URI[PackageKit-glib-devel.sha256sum] = "68850cc29c669c8fceff46bbd865d48ec522893135462733fcba3acd6f43bac0"
SRC_URI[PackageKit-gstreamer-plugin.sha256sum] = "d5336394b047ca02f348c4ea463f75d5ff3f7a8c7de0333f40844d46f43e568a"
SRC_URI[PackageKit-gtk3-module.sha256sum] = "7d79a7596aded8b66ec824faacdd1e10c147acea22765d0cd4b1343801f1d58d"
