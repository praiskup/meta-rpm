SUMMARY = "generated recipe based on libmpc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp mpfr pkgconfig-native"
RPM_SONAME_PROV_libmpc = "libmpc.so.3"
RPM_SONAME_REQ_libmpc = "ld-linux-aarch64.so.1 libc.so.6 libgmp.so.10 libm.so.6 libmpfr.so.4"
RDEPENDS_libmpc = "glibc gmp mpfr"
RPM_SONAME_REQ_libmpc-devel = "libmpc.so.3"
RPROVIDES_libmpc-devel = "libmpc-dev (= 1.0.2)"
RDEPENDS_libmpc-devel = "bash gmp-devel libmpc mpfr-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmpc-1.0.2-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmpc-devel-1.0.2-9.el8.aarch64.rpm \
          "

SRC_URI[libmpc.sha256sum] = "19e57922d2e5e10632571993d8880855e070e9446e636b36808e3c6c134f409c"
SRC_URI[libmpc-devel.sha256sum] = "31815db892cff135c0b889a0354cee7d1ead0a5de5f5a3fed00f869fd8cf5bc7"
