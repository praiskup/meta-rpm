SUMMARY = "generated recipe based on man-db srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm libpipeline pkgconfig-native zlib"
RPM_SONAME_PROV_man-db = "libman-2.7.6.1.so libmandb-2.7.6.1.so"
RPM_SONAME_REQ_man-db = "ld-linux-aarch64.so.1 libc.so.6 libgdbm.so.6 libman-2.7.6.1.so libmandb-2.7.6.1.so libpipeline.so.1 libz.so.1"
RDEPENDS_man-db = "bash coreutils gdbm-libs glibc grep groff-base gzip less libpipeline zlib"
RDEPENDS_man-db-cron = "bash crontabs man-db"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/man-db-2.7.6.1-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/man-db-cron-2.7.6.1-17.el8.noarch.rpm \
          "

SRC_URI[man-db.sha256sum] = "9d9492991dc9808ee56e00ab111f95714c62fee7fcbdb2141684c473ed2b2cc9"
SRC_URI[man-db-cron.sha256sum] = "59cf51d25f07d4a38c51884b3ddc3555c5bc8ddf9c7d1f2b62647c41365b4d95"
