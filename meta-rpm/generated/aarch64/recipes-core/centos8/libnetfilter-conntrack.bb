SUMMARY = "generated recipe based on libnetfilter_conntrack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl libnfnetlink pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_conntrack = "libnetfilter_conntrack.so.3"
RPM_SONAME_REQ_libnetfilter_conntrack = "ld-linux-aarch64.so.1 libc.so.6 libmnl.so.0 libnfnetlink.so.0"
RDEPENDS_libnetfilter_conntrack = "glibc libmnl libnfnetlink"
RPM_SONAME_REQ_libnetfilter_conntrack-devel = "libnetfilter_conntrack.so.3"
RPROVIDES_libnetfilter_conntrack-devel = "libnetfilter_conntrack-dev (= 1.0.6)"
RDEPENDS_libnetfilter_conntrack-devel = "kernel-headers libnetfilter_conntrack libnfnetlink-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnetfilter_conntrack-1.0.6-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnetfilter_conntrack-devel-1.0.6-5.el8.aarch64.rpm \
          "

SRC_URI[libnetfilter_conntrack.sha256sum] = "4e43b0f85746f74064b082fdf6914ba4e9fe386651b1c39aeaecc702b2a59fc0"
SRC_URI[libnetfilter_conntrack-devel.sha256sum] = "398da10c29e22cf9fc2eaf095bf15a4be8ceff59d379bdb621d74e343ed9d62d"
