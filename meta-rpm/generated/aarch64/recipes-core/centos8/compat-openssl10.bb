SUMMARY = "generated recipe based on compat-openssl10 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_compat-openssl10 = "libcrypto.so.10 libssl.so.10"
RPM_SONAME_REQ_compat-openssl10 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.10 libdl.so.2 libz.so.1"
RDEPENDS_compat-openssl10 = "coreutils crypto-policies glibc make zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/compat-openssl10-1.0.2o-3.el8.aarch64.rpm \
          "

SRC_URI[compat-openssl10.sha256sum] = "ba3e1799be3903aea556ee29b2e656dc3ae81a6db6b03db7d4f8abe2e3414ed8"
