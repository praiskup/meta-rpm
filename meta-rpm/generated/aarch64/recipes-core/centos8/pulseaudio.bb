SUMMARY = "generated recipe based on pulseaudio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib dbus-libs glib-2.0 libasyncns libcap libgcc libice libsm libsndfile2 libtdb libtool libx11 libxcb libxtst orc pkgconfig-native sbc speexdsp systemd-libs webrtc-audio-processing"
RPM_SONAME_PROV_pulseaudio = "libalsa-util.so libcli.so libprotocol-cli.so libprotocol-esound.so libprotocol-http.so libprotocol-native.so libprotocol-simple.so libpulsecore-11.1.so librtp.so libwebrtc-util.so"
RPM_SONAME_REQ_pulseaudio = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libalsa-util.so libasound.so.2 libasyncns.so.0 libc.so.6 libcap.so.2 libcli.so libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libltdl.so.7 libm.so.6 liborc-0.4.so.0 libprotocol-cli.so libprotocol-esound.so libprotocol-http.so libprotocol-native.so libprotocol-simple.so libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsecore-11.1.so librt.so.1 librtp.so libsndfile.so.1 libspeexdsp.so.1 libstdc++.so.6 libsystemd.so.0 libtdb.so.1 libudev.so.1 libwebrtc-util.so libwebrtc_audio_processing.so.1 libxcb.so.1"
RDEPENDS_pulseaudio = "alsa-lib bash dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libgcc libsndfile libstdc++ libtdb libtool-ltdl libxcb orc pulseaudio-libs rtkit shadow-utils speexdsp systemd systemd-libs webrtc-audio-processing"
RPM_SONAME_PROV_pulseaudio-libs = "libpulse-simple.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsedsp.so"
RPM_SONAME_REQ_pulseaudio-libs = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libm.so.6 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so librt.so.1 libsndfile.so.1 libsystemd.so.0 libxcb.so.1"
RDEPENDS_pulseaudio-libs = "dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libxcb systemd-libs"
RPM_SONAME_REQ_pulseaudio-libs-devel = "libpulse-mainloop-glib.so.0 libpulse-simple.so.0 libpulse.so.0"
RPROVIDES_pulseaudio-libs-devel = "pulseaudio-libs-dev (= 11.1)"
RDEPENDS_pulseaudio-libs-devel = "glib2-devel pkgconf-pkg-config pulseaudio-libs pulseaudio-libs-glib2"
RPM_SONAME_PROV_pulseaudio-libs-glib2 = "libpulse-mainloop-glib.so.0"
RPM_SONAME_REQ_pulseaudio-libs-glib2 = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libm.so.6 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so librt.so.1 libsndfile.so.1 libsystemd.so.0 libxcb.so.1"
RDEPENDS_pulseaudio-libs-glib2 = "dbus-libs glib2 glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libxcb pulseaudio-libs systemd-libs"
RPM_SONAME_PROV_pulseaudio-module-bluetooth = "libbluez5-util.so"
RPM_SONAME_REQ_pulseaudio-module-bluetooth = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libbluez5-util.so libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libltdl.so.7 libm.so.6 liborc-0.4.so.0 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsecore-11.1.so librt.so.1 libsbc.so.1 libsndfile.so.1 libspeexdsp.so.1 libsystemd.so.0 libtdb.so.1 libxcb.so.1"
RDEPENDS_pulseaudio-module-bluetooth = "bluez dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libtdb libtool-ltdl libxcb orc pulseaudio pulseaudio-libs sbc speexdsp systemd-libs"
RPM_SONAME_REQ_pulseaudio-module-x11 = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libltdl.so.7 libm.so.6 liborc-0.4.so.0 libprotocol-native.so libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsecore-11.1.so librt.so.1 libsndfile.so.1 libspeexdsp.so.1 libsystemd.so.0 libtdb.so.1 libxcb.so.1"
RDEPENDS_pulseaudio-module-x11 = "bash dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libtdb libtool-ltdl libxcb orc pulseaudio pulseaudio-libs pulseaudio-utils speexdsp systemd-libs"
RPM_SONAME_REQ_pulseaudio-utils = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libm.so.6 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so librt.so.1 libsndfile.so.1 libsystemd.so.0 libxcb.so.1"
RDEPENDS_pulseaudio-utils = "bash dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libxcb pulseaudio-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-11.1-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-libs-11.1-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-libs-devel-11.1-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-libs-glib2-11.1-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-module-bluetooth-11.1-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-module-x11-11.1-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pulseaudio-utils-11.1-23.el8.aarch64.rpm \
          "

SRC_URI[pulseaudio.sha256sum] = "159c56507a345ee2a8dc2ccf7206823e7e24b1ac133a762d9f2df90ecc2a2b27"
SRC_URI[pulseaudio-libs.sha256sum] = "c5d7619c922198b0caf747e8c774fa6960236f59443a6adca01f66d0e22f3d49"
SRC_URI[pulseaudio-libs-devel.sha256sum] = "d583acb4ed09f21032a36bc5300d91aa26bfe205269bd8c5a79e9553fc2f630e"
SRC_URI[pulseaudio-libs-glib2.sha256sum] = "71454705b42402aa16fd685495e644e9f2fa546454214be03ffa5135f8d71abf"
SRC_URI[pulseaudio-module-bluetooth.sha256sum] = "838b9b08cd44fef46f5b49746409879f002b06a8bac2151ff2f76bf9849389e3"
SRC_URI[pulseaudio-module-x11.sha256sum] = "c5f7932107ec1a10608cc51acb6451ac60beb072734d986aa787f0832dde9cba"
SRC_URI[pulseaudio-utils.sha256sum] = "26a4dc56e3d96edc77572c7dec336e1363dc59dac8d5187282b145de10cee430"
