SUMMARY = "generated recipe based on qt5-qtsensors srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtsensors = "libQt5Sensors.so.5"
RPM_SONAME_REQ_qt5-qtsensors = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Sensors.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_qt5-qtsensors = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtsensors-devel = "libQt5Sensors.so.5"
RPROVIDES_qt5-qtsensors-devel = "qt5-qtsensors-dev (= 5.12.5)"
RDEPENDS_qt5-qtsensors-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtsensors"
RPM_SONAME_PROV_qt5-qtsensors-examples = "libdeclarative_explorer.so libdeclarative_grue.so libgruesensor.so.1 libqtsensors_grue.so"
RPM_SONAME_REQ_qt5-qtsensors-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sensors.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libgruesensor.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtsensors-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtsensors"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtsensors-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtsensors-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtsensors-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtsensors.sha256sum] = "a5ca1ca36f6f3efa5b7490d08e422e686c8613af4ebb3dce19aad8fed92c2cf5"
SRC_URI[qt5-qtsensors-devel.sha256sum] = "dcd945eb480e393c37b9ee54b3f1a4be1f60dfaf873557cbaa112426a84bcbc0"
SRC_URI[qt5-qtsensors-examples.sha256sum] = "90306ebdfccd30a54afafc54f63833964aa016b4e06112f287f9e28fd00de366"
