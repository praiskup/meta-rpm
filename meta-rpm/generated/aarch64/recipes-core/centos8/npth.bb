SUMMARY = "generated recipe based on npth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_npth = "libnpth.so.0"
RPM_SONAME_REQ_npth = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_npth = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/npth-1.5-4.el8.aarch64.rpm \
          "

SRC_URI[npth.sha256sum] = "eaaeb7ee9274c38650feab7a7abae0b6b38637cded9cf6c828651326b791dc68"
