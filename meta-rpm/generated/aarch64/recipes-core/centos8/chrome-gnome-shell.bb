SUMMARY = "generated recipe based on chrome-gnome-shell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_chrome-gnome-shell = "dbus gnome-shell hicolor-icon-theme mozilla-filesystem platform-python python3-gobject-base python3-requests"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/chrome-gnome-shell-10.1-6.el8.aarch64.rpm \
          "

SRC_URI[chrome-gnome-shell.sha256sum] = "1c99c6b5289d1a29b1b06613859fd87ba5dfe717a8e27d92cc6726e745a0600d"
