SUMMARY = "generated recipe based on gnome-user-docs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-user-docs-3.28.2-1.el8.noarch.rpm \
          "

SRC_URI[gnome-user-docs.sha256sum] = "32de5a3562bae9bd962601e2cdb740042ac2584dcbeebe40db98d3cc1aab8a33"
