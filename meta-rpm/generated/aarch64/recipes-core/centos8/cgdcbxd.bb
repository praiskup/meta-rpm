SUMMARY = "generated recipe based on cgdcbxd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcgroup libmnl pkgconfig-native"
RPM_SONAME_REQ_cgdcbxd = "libc.so.6 libcgroup.so.1 libmnl.so.0 librt.so.1"
RDEPENDS_cgdcbxd = "bash glibc libcgroup libmnl systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cgdcbxd-1.0.2-9.el8.aarch64.rpm \
          "

SRC_URI[cgdcbxd.sha256sum] = "65a5d6b7673bee8d4ec303f64feece61e0f6e79cc2faa9a89e9153129173ec16"
