SUMMARY = "generated recipe based on libhbalinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpciaccess pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libhbalinux = "libhbalinux.so.2"
RPM_SONAME_REQ_libhbalinux = "ld-linux-aarch64.so.1 libc.so.6 libpciaccess.so.0 libudev.so.1"
RDEPENDS_libhbalinux = "bash glibc grep libhbaapi libpciaccess systemd-libs"
RPM_SONAME_REQ_libhbalinux-devel = "libhbalinux.so.2"
RPROVIDES_libhbalinux-devel = "libhbalinux-dev (= 1.0.17)"
RDEPENDS_libhbalinux-devel = "libhbalinux pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libhbalinux-1.0.17-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libhbalinux-devel-1.0.17-7.el8.aarch64.rpm \
          "

SRC_URI[libhbalinux.sha256sum] = "a1e7dfa626615a90038fc7ef5a0b7c4f8516a458c057dae6b8766fd467e216da"
SRC_URI[libhbalinux-devel.sha256sum] = "ffbf3240fde0c755a5ed238af6ca90abc92060b91f91ef3df5fd3c2ee6e4c5fb"
