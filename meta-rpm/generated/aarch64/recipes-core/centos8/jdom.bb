SUMMARY = "generated recipe based on jdom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jdom = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jdom-demo = "jdom"
RDEPENDS_jdom-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdom-1.1.3-17.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdom-demo-1.1.3-17.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdom-javadoc-1.1.3-17.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jdom.sha256sum] = "74756ffa5a5d3b88237b57e6af34a90272891b9381aa3f30881f35d7aa18dd5f"
SRC_URI[jdom-demo.sha256sum] = "61563804370da05341312eb46acc45c2255029581e40325ef8067a028d80f0d4"
SRC_URI[jdom-javadoc.sha256sum] = "e82bd584d4becc8bc508dab7f8fb7f5d6f49d19f7b90502cb625be95fdb4cf99"
