SUMMARY = "generated recipe based on libnl3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native platform-python3"
RPM_SONAME_PROV_libnl3 = "libnl-3.so.200 libnl-genl-3.so.200 libnl-idiag-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libnl-xfrm-3.so.200"
RPM_SONAME_REQ_libnl3 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0"
RDEPENDS_libnl3 = "glibc libgcc"
RPM_SONAME_PROV_libnl3-cli = "libnl-cli-3.so.200"
RPM_SONAME_REQ_libnl3-cli = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-idiag-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libpthread.so.0"
RDEPENDS_libnl3-cli = "glibc libnl3"
RPM_SONAME_REQ_libnl3-devel = "libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-idiag-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libnl-xfrm-3.so.200"
RPROVIDES_libnl3-devel = "libnl3-dev (= 3.5.0)"
RDEPENDS_libnl3-devel = "kernel-headers libnl3 libnl3-cli pkgconf-pkg-config"
RDEPENDS_libnl3-doc = "libnl3"
RPM_SONAME_REQ_python3-libnl3 = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libnl-genl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-libnl3 = "glibc libnl3 platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnl3-3.5.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnl3-cli-3.5.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnl3-devel-3.5.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnl3-doc-3.5.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libnl3-3.5.0-1.el8.aarch64.rpm \
          "

SRC_URI[libnl3.sha256sum] = "851a9cebfb68b8c301231b1121f573311fbb165ace0f4b1a599fa42f80113df9"
SRC_URI[libnl3-cli.sha256sum] = "47822e5b7a8886e09ac50e1143738976ec2ca431f675834d5cf1dd5031316dbc"
SRC_URI[libnl3-devel.sha256sum] = "b472fc0b25c76cb31782c30eb3ca8cf8e3395d99e82dec648535c85b1b6fcf86"
SRC_URI[libnl3-doc.sha256sum] = "6ff8912c48c8ae9b4ae37cf00745b8faff8b25a97f9aef86d8b22b2178ca74a9"
SRC_URI[python3-libnl3.sha256sum] = "7626bf97a1abd71a0043c75c8a59755e2a17c368d83c1e6c3f5218d57ce8fffd"
