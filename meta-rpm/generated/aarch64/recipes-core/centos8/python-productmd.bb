SUMMARY = "generated recipe based on python-productmd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-productmd = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-productmd-1.11-3.el8.noarch.rpm \
          "

SRC_URI[python3-productmd.sha256sum] = "cc561cfa6937be7edf404e77fc57345a5269901341bb15623bedf25967a6ad75"
