SUMMARY = "generated recipe based on perl-PerlIO-utf8_strict srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-PerlIO-utf8_strict = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-PerlIO-utf8_strict = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-PerlIO-utf8_strict-0.007-5.el8.aarch64.rpm \
          "

SRC_URI[perl-PerlIO-utf8_strict.sha256sum] = "41ff5d9b855d3acc635b9b269cadc7de40f3be749c90581336719889d5a3e099"
