SUMMARY = "generated recipe based on libcomps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libxml2 pkgconfig-native platform-python3 zlib"
RPM_SONAME_PROV_libcomps = "libcomps.so.0.1.11"
RPM_SONAME_REQ_libcomps = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libm.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libcomps = "expat glibc libxml2 zlib"
RPM_SONAME_REQ_libcomps-devel = "libcomps.so.0.1.11"
RPROVIDES_libcomps-devel = "libcomps-dev (= 0.1.11)"
RDEPENDS_libcomps-devel = "libcomps"
RPM_SONAME_REQ_python3-libcomps = "ld-linux-aarch64.so.1 libc.so.6 libcomps.so.0.1.11 libexpat.so.1 libm.so.6 libpython3.6m.so.1.0 libxml2.so.2 libz.so.1"
RDEPENDS_python3-libcomps = "expat glibc libcomps libxml2 platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcomps-0.1.11-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcomps-devel-0.1.11-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libcomps-0.1.11-4.el8.aarch64.rpm \
          "

SRC_URI[libcomps.sha256sum] = "e320e3b0499ba7304b5f9f86fbb3c54ef49e316a681a0b7768e8a6df12b26c45"
SRC_URI[libcomps-devel.sha256sum] = "cc815b272b84ef09f72f7812a18dbf31ba706d2d05d879a4d971ace0d3c3a4a1"
SRC_URI[python3-libcomps.sha256sum] = "03f05a38ebea801390aebb5da1da25d2c805300d02b4d0cc557562d2995527e0"
