SUMMARY = "generated recipe based on chkconfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/update-alternatives"
DEPENDS = "libnewt libselinux libsepol pkgconfig-native popt"
RPM_SONAME_REQ_chkconfig = "ld-linux-aarch64.so.1 libc.so.6 libpopt.so.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_chkconfig = "glibc libselinux libsepol popt"
RPM_SONAME_REQ_ntsysv = "ld-linux-aarch64.so.1 libc.so.6 libnewt.so.0.52 libpopt.so.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_ntsysv = "chkconfig glibc libselinux libsepol newt popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/chkconfig-1.11-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ntsysv-1.11-1.el8.aarch64.rpm \
          "

SRC_URI[chkconfig.sha256sum] = "172da3cd3ea13036c2a6cbe953b5d5b2b41c9a3aa7a2d6fdb5efd4c1d51174f3"
SRC_URI[ntsysv.sha256sum] = "61ab2b6aee5fd81929100003425dc820c7c5010b2e3af0d8f97d20b32477f9ea"
