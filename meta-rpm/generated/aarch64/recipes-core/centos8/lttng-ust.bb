SUMMARY = "generated recipe based on lttng-ust srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemtap-sdt-devel userspace-rcu"
RPM_SONAME_PROV_lttng-ust = "liblttng-ust-ctl.so.2 liblttng-ust-cyg-profile-fast.so.0 liblttng-ust-cyg-profile.so.0 liblttng-ust-dl.so.0 liblttng-ust-fork.so.0 liblttng-ust-libc-wrapper.so.0 liblttng-ust-pthread-wrapper.so.0 liblttng-ust-python-agent.so.0 liblttng-ust-tracepoint.so.0 liblttng-ust.so.0"
RPM_SONAME_REQ_lttng-ust = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblttng-ust-tracepoint.so.0 liblttng-ust.so.0 libpthread.so.0 librt.so.1 liburcu-bp.so.6 liburcu-cds.so.6"
RDEPENDS_lttng-ust = "glibc userspace-rcu"
RPM_SONAME_REQ_lttng-ust-devel = "liblttng-ust-ctl.so.2 liblttng-ust-cyg-profile-fast.so.0 liblttng-ust-cyg-profile.so.0 liblttng-ust-dl.so.0 liblttng-ust-fork.so.0 liblttng-ust-libc-wrapper.so.0 liblttng-ust-pthread-wrapper.so.0 liblttng-ust-python-agent.so.0 liblttng-ust-tracepoint.so.0 liblttng-ust.so.0"
RPROVIDES_lttng-ust-devel = "lttng-ust-dev (= 2.8.1)"
RDEPENDS_lttng-ust-devel = "lttng-ust pkgconf-pkg-config platform-python systemtap-sdt-devel userspace-rcu-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lttng-ust-2.8.1-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lttng-ust-devel-2.8.1-11.el8.aarch64.rpm \
          "

SRC_URI[lttng-ust.sha256sum] = "301e4b64851fd34bf48f3c38f3171933f024c892a979e8c65459f49f2f892217"
SRC_URI[lttng-ust-devel.sha256sum] = "b1d6e255184ff589a0588a005d445649142eae7bfb43827d9d83b5d55187848b"
