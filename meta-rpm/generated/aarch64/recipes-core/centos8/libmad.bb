SUMMARY = "generated recipe based on libmad srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmad = "libmad.so.0"
RPM_SONAME_REQ_libmad = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libmad = "glibc"
RPM_SONAME_REQ_libmad-devel = "libmad.so.0"
RPROVIDES_libmad-devel = "libmad-dev (= 0.15.1b)"
RDEPENDS_libmad-devel = "libmad pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmad-0.15.1b-25.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmad-devel-0.15.1b-25.el8.aarch64.rpm \
          "

SRC_URI[libmad.sha256sum] = "a83a888324653a03f19fe2419f3653faea33ec0a9a546e1763de2fde8bdc49ec"
SRC_URI[libmad-devel.sha256sum] = "9abf8a36d1fe8115b99df6be331f7eb1042c6d750622f622f49539573dc4e7e9"
