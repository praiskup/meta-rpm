SUMMARY = "generated recipe based on python2-pycairo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo pkgconfig-native"
RPM_SONAME_REQ_python2-cairo = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libpthread.so.0 libpython2.7.so.1.0"
RDEPENDS_python2-cairo = "cairo glibc platform-python"
RPROVIDES_python2-cairo-devel = "python2-cairo-dev (= 1.16.3)"
RDEPENDS_python2-cairo-devel = "cairo-devel pkgconf-pkg-config python2-cairo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python2-cairo-1.16.3-6.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python2-cairo-devel-1.16.3-6.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
          "

SRC_URI[python2-cairo.sha256sum] = "d19d511ee3667d5cd137cb2b60164aec3b703cce6b1c8568700f795974b62cb8"
SRC_URI[python2-cairo-devel.sha256sum] = "4524544dc0d1a4595465217938cbd62fa0e26411a2ddd41694b81f23a947706d"
