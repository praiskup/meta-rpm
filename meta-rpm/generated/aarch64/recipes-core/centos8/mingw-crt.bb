SUMMARY = "generated recipe based on mingw-crt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-crt = "mingw32-filesystem"
RDEPENDS_mingw64-crt = "mingw64-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw32-crt-5.0.2-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw64-crt-5.0.2-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-crt.sha256sum] = "b9383ee0fa91d49e23888defbc5ef30308ab96e06855582ce6d95d830b089a94"
SRC_URI[mingw64-crt.sha256sum] = "493b604c15622c3ebc8cf084327a48d3e8577978339936fb0ad050b99cbdb1c5"
