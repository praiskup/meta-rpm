SUMMARY = "generated recipe based on cim-schema srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cim-schema-2.43.0-8.el8.noarch.rpm \
          "

SRC_URI[cim-schema.sha256sum] = "5639c5d2fd4ad4120a13c7773ff5fb2367023d04384c281901f9a5d4e7a07b94"
