SUMMARY = "generated recipe based on aqute-bnd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_aqute-bnd = "ant-lib aqute-bndlib bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools osgi-annotation osgi-compendium osgi-core slf4j"
RDEPENDS_aqute-bnd-javadoc = "javapackages-filesystem"
RDEPENDS_aqute-bndlib = "java-1.8.0-openjdk-headless javapackages-filesystem osgi-annotation osgi-compendium osgi-core slf4j"
RDEPENDS_bnd-maven-plugin = "aqute-bndlib java-1.8.0-openjdk-headless javapackages-filesystem plexus-build-api slf4j"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/aqute-bnd-3.5.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/aqute-bnd-javadoc-3.5.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/aqute-bndlib-3.5.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bnd-maven-plugin-3.5.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[aqute-bnd.sha256sum] = "e6504a0b65766d67889c246f2a477e105d96b510c99035f641e7ad0433822d5c"
SRC_URI[aqute-bnd-javadoc.sha256sum] = "5a558b41698f58159e57c916a9bb44dc94c8ad7d05bb18011d27c02135add717"
SRC_URI[aqute-bndlib.sha256sum] = "9e683403d90cf7f900b48bebc9a5e4d9c6178875b8e9aaf752b9adc674c62e17"
SRC_URI[bnd-maven-plugin.sha256sum] = "f8888cfda80651b81588c9ac2ddc32adf41fe2180e4c57be7b7d8d64e64755d1"
