SUMMARY = "generated recipe based on wget srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls gpgme libidn2 libmetalink libpsl libuuid nettle pkgconfig-native zlib"
RPM_SONAME_REQ_wget = "ld-linux-aarch64.so.1 libc.so.6 libgnutls.so.30 libgpgme.so.11 libidn2.so.0 libmetalink.so.3 libnettle.so.6 libpsl.so.5 libuuid.so.1 libz.so.1"
RDEPENDS_wget = "bash glibc gnutls gpgme info libidn2 libmetalink libpsl libuuid nettle zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/wget-1.19.5-8.el8_1.1.aarch64.rpm \
          "

SRC_URI[wget.sha256sum] = "f54e70eeb2fb92ee230d0971353d2df6fe4186e255f9fdc6b465e38c15e19db1"
