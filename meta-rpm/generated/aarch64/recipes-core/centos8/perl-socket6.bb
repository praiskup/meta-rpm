SUMMARY = "generated recipe based on perl-Socket6 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Socket6 = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Socket6 = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Socket6-0.28-6.el8.aarch64.rpm \
          "

SRC_URI[perl-Socket6.sha256sum] = "1c211edbff355c4b5c2345cbdb2f02a9aa9add752a4a3dcb7e6f9a06f4af3743"
