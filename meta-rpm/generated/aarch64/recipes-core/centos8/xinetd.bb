SUMMARY = "generated recipe based on xinetd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux libtirpc libxcrypt pkgconfig-native"
RPM_SONAME_REQ_xinetd = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libm.so.6 libselinux.so.1 libtirpc.so.3"
RDEPENDS_xinetd = "bash filesystem glibc libselinux libtirpc libxcrypt setup systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xinetd-2.3.15-24.el8.aarch64.rpm \
          "

SRC_URI[xinetd.sha256sum] = "88d6f60dc590515ad62fcdc1a27aa712967ec07ef24a4a3712d6dc6e5b251c44"
