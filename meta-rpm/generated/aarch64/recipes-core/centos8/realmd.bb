SUMMARY = "generated recipe based on realmd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs glib-2.0 krb5-libs openldap pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_realmd = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libpolkit-gobject-1.so.0 libpthread.so.0 libresolv.so.2 libsystemd.so.0"
RDEPENDS_realmd = "authselect glib2 glibc krb5-libs libcom_err openldap polkit polkit-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/realmd-0.16.3-18.el8.aarch64.rpm \
          "

SRC_URI[realmd.sha256sum] = "9401f3db897d9a75fe06a7633eec6def9861e88933c9ec96617cf718c1af0cbe"
