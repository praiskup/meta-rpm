SUMMARY = "generated recipe based on mod_authnz_pam srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pam pkgconfig-native"
RPM_SONAME_REQ_mod_authnz_pam = "ld-linux-aarch64.so.1 libc.so.6 libpam.so.0"
RDEPENDS_mod_authnz_pam = "glibc httpd pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_authnz_pam-1.1.0-7.el8.aarch64.rpm \
          "

SRC_URI[mod_authnz_pam.sha256sum] = "83a2df3639aa07e19ed8b069da23f66f9226ceb19076f408aebda4fcd86ae0da"
