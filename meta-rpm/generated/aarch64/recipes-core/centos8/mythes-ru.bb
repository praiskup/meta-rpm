SUMMARY = "generated recipe based on mythes-ru srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-ru = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-ru-0.20070613-17.el8.noarch.rpm \
          "

SRC_URI[mythes-ru.sha256sum] = "db979e9d96c46d4e1f9b1a1ea8c40acc780255b84eab12d6b1be3a49882c1733"
