SUMMARY = "generated recipe based on perl-ExtUtils-ParseXS srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-ExtUtils-ParseXS = "perl-Exporter perl-Getopt-Long perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-ExtUtils-ParseXS-3.35-2.el8.noarch.rpm \
          "

SRC_URI[perl-ExtUtils-ParseXS.sha256sum] = "6cb9fe730cbc590f166353c5511c697b487dc4a6485f50ac984756e7d075fef6"
