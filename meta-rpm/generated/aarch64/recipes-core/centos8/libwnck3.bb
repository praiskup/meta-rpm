SUMMARY = "generated recipe based on libwnck3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libx11 libxext libxrender libxres pango pkgconfig-native startup-notification"
RPM_SONAME_PROV_libwnck3 = "libwnck-3.so.0"
RPM_SONAME_REQ_libwnck3 = "ld-linux-aarch64.so.1 libX11.so.6 libXRes.so.1 libXext.so.6 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libstartup-notification-1.so.0 libwnck-3.so.0"
RDEPENDS_libwnck3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libXext libXrender libXres pango startup-notification"
RPM_SONAME_REQ_libwnck3-devel = "ld-linux-aarch64.so.1 libX11.so.6 libXRes.so.1 libXext.so.6 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libstartup-notification-1.so.0 libwnck-3.so.0"
RPROVIDES_libwnck3-devel = "libwnck3-dev (= 3.24.1)"
RDEPENDS_libwnck3-devel = "atk cairo cairo-devel cairo-gobject gdk-pixbuf2 glib2 glib2-devel glibc gtk3 gtk3-devel libX11 libX11-devel libXext libXrender libXres libXres-devel libwnck3 pango pango-devel pkgconf-pkg-config startup-notification startup-notification-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libwnck3-3.24.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libwnck3-devel-3.24.1-2.el8.aarch64.rpm \
          "

SRC_URI[libwnck3.sha256sum] = "b4987bc8beaf8798d5084e7ed5fdc38349e7783233bbe33cd23f1c98e7a71364"
SRC_URI[libwnck3-devel.sha256sum] = "ce11152150fa60bc14cb6b9e69770e26b28a9f17685c9ad5720305b032f6c565"
