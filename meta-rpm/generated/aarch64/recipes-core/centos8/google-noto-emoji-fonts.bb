SUMMARY = "generated recipe based on google-noto-emoji-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_google-noto-emoji-color-fonts = "fontpackages-filesystem"
RDEPENDS_google-noto-emoji-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-noto-emoji-color-fonts-20180508-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/google-noto-emoji-fonts-20180508-4.el8.noarch.rpm \
          "

SRC_URI[google-noto-emoji-color-fonts.sha256sum] = "7782c0c0f4384fda352119917005b369ab377eedc98b31c89a143da7666d1e08"
SRC_URI[google-noto-emoji-fonts.sha256sum] = "c94aeb968835d8076d8e8205c6aebf281bd0eabcb8624707f556ca1abdb1f3bf"
