SUMMARY = "generated recipe based on perl-Mozilla-LDAP srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "nspr nss openldap perl pkgconfig-native"
RPM_SONAME_REQ_perl-Mozilla-LDAP = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblber-2.4.so.2 libldap-2.4.so.2 libnspr4.so libnss3.so libnssutil3.so libperl.so.5.26 libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so"
RDEPENDS_perl-Mozilla-LDAP = "glibc nspr nss nss-util openldap perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Mozilla-LDAP-1.5.3-25.el8.aarch64.rpm \
          "

SRC_URI[perl-Mozilla-LDAP.sha256sum] = "bced1f1672d26bf4cd30b35da135cba0387ab7c8c3c9e709f3990b418ad376be"
