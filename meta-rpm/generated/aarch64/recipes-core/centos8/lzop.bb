SUMMARY = "generated recipe based on lzop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lzo pkgconfig-native"
RPM_SONAME_REQ_lzop = "ld-linux-aarch64.so.1 libc.so.6 liblzo2.so.2"
RDEPENDS_lzop = "glibc lzo"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lzop-1.03-20.el8.aarch64.rpm \
          "

SRC_URI[lzop.sha256sum] = "003b309833a1ed94ad97ed62f04c2fcda4a20fb8b7b5933c36459974f4e4986c"
