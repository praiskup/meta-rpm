SUMMARY = "generated recipe based on libiscsi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cunit libgcrypt pkgconfig-native rdma-core"
RPM_SONAME_PROV_libiscsi = "libiscsi.so.8"
RPM_SONAME_REQ_libiscsi = "ld-linux-aarch64.so.1 libc.so.6 libgcrypt.so.20 libibverbs.so.1 librdmacm.so.1"
RDEPENDS_libiscsi = "glibc libgcrypt libibverbs librdmacm"
RPM_SONAME_REQ_libiscsi-devel = "libiscsi.so.8"
RPROVIDES_libiscsi-devel = "libiscsi-dev (= 1.18.0)"
RDEPENDS_libiscsi-devel = "libiscsi pkgconf-pkg-config"
RPM_SONAME_REQ_libiscsi-utils = "ld-linux-aarch64.so.1 libc.so.6 libcunit.so.1 libdl.so.2 libgcrypt.so.20 libibverbs.so.1 libiscsi.so.8 librdmacm.so.1"
RDEPENDS_libiscsi-utils = "CUnit glibc libgcrypt libibverbs libiscsi librdmacm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libiscsi-1.18.0-8.module_el8.2.0+524+f765f7e0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libiscsi-devel-1.18.0-8.module_el8.2.0+524+f765f7e0.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libiscsi-utils-1.18.0-8.module_el8.2.0+524+f765f7e0.aarch64.rpm \
          "

SRC_URI[libiscsi.sha256sum] = "36fb06ad4414bcf161dfd8f54b2c896d8316aebe4027ebb34e2ebd5a3714bc2e"
SRC_URI[libiscsi-devel.sha256sum] = "4f985c76e1c94cf129c5c86c4f63fd4c710cf8221f9957881ae315afb16d803a"
SRC_URI[libiscsi-utils.sha256sum] = "9d83b045ca1afdbe411737a8c4d7da6a5acff0e17f43971c760406dd1f9af610"
