SUMMARY = "generated recipe based on blktrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libaio pkgconfig-native"
RPM_SONAME_REQ_blktrace = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_blktrace = "bash glibc libaio platform-python"
RPM_SONAME_REQ_iowatcher = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 librt.so.1"
RDEPENDS_iowatcher = "blktrace glibc librsvg2-tools sysstat theora-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/iowatcher-1.2.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/blktrace-1.2.0-10.el8.aarch64.rpm \
          "

SRC_URI[blktrace.sha256sum] = "0fbd9ce3d696b2650fa401052eca309f6e33e2fb086d5b0817fa061c94af448c"
SRC_URI[iowatcher.sha256sum] = "6fe5f2bf2cb25c14792897e2b2072ef3b12e99ef54b7b31d4ca2946afab71194"
