SUMMARY = "generated recipe based on fcoe-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpciaccess pkgconfig-native"
RPM_SONAME_REQ_fcoe-utils = "ld-linux-aarch64.so.1 libc.so.6 libpciaccess.so.0 librt.so.1"
RDEPENDS_fcoe-utils = "bash device-mapper-multipath glibc iproute libpciaccess lldpad systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fcoe-utils-1.0.32-7.el8.aarch64.rpm \
          "

SRC_URI[fcoe-utils.sha256sum] = "bf30b2606c48e775f64bd48c034d34615f43ae51e3588a4e2d117b7cff06f4f8"
