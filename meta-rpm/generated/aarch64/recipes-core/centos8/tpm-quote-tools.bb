SUMMARY = "generated recipe based on tpm-quote-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native trousers"
RPM_SONAME_REQ_tpm-quote-tools = "ld-linux-aarch64.so.1 libc.so.6 libtspi.so.1"
RDEPENDS_tpm-quote-tools = "glibc trousers-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm-quote-tools-1.0.3-4.el8.aarch64.rpm \
          "

SRC_URI[tpm-quote-tools.sha256sum] = "acda8856c8f6e20467506fbf4097d882bde21161530f8826339d4f4a8584dce3"
