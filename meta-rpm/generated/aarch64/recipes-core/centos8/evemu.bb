SUMMARY = "generated recipe based on evemu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevdev pkgconfig-native"
RPM_SONAME_REQ_evemu = "ld-linux-aarch64.so.1 libc.so.6 libevdev.so.2 libevemu.so.3"
RDEPENDS_evemu = "evemu-libs glibc libevdev"
RPM_SONAME_PROV_evemu-libs = "libevemu.so.3"
RPM_SONAME_REQ_evemu-libs = "ld-linux-aarch64.so.1 libc.so.6 libevdev.so.2"
RDEPENDS_evemu-libs = "glibc libevdev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evemu-2.7.0-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evemu-libs-2.7.0-8.el8.aarch64.rpm \
          "

SRC_URI[evemu.sha256sum] = "31d6307d2398d653058633dd7e60878c4a650e14d13c96a652c4fb6601389814"
SRC_URI[evemu-libs.sha256sum] = "884581fdec78303b28904e2a30c829e7d376d29a4ce03d418281e368a54db4f7"
