SUMMARY = "generated recipe based on perl-Devel-Symdump srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-Symdump = "perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-Symdump-2.18-5.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-Symdump.sha256sum] = "360a374ece2ff8da362873beecaa4174d47b03c668d5b61ec90ac725a6d2d680"
