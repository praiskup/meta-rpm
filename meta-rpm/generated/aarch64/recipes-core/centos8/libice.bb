SUMMARY = "generated recipe based on libICE srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libICE = "libICE.so.6"
RPM_SONAME_REQ_libICE = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libICE = "glibc"
RPM_SONAME_REQ_libICE-devel = "libICE.so.6"
RPROVIDES_libICE-devel = "libICE-dev (= 1.0.9)"
RDEPENDS_libICE-devel = "libICE pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libICE-1.0.9-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libICE-devel-1.0.9-15.el8.aarch64.rpm \
          "

SRC_URI[libICE.sha256sum] = "07c70f832b3b259ffe4d89f896d2d07cb681d893f04b4c35c32c48a472179ed2"
SRC_URI[libICE-devel.sha256sum] = "716c5cd4733e12fa0402722ced2400c0a80fdfb8807578e99d6fe82fbd75f4cf"
