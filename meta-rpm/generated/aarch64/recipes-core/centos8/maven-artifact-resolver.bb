SUMMARY = "generated recipe based on maven-artifact-resolver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-artifact-resolver = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib"
RDEPENDS_maven-artifact-resolver-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-artifact-resolver-1.0-18.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-artifact-resolver-javadoc-1.0-18.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-artifact-resolver.sha256sum] = "6ae1e7fe6d92d10ec946fabd9771d3357c6540f05bd8fcb4baacb71c9090c269"
SRC_URI[maven-artifact-resolver-javadoc.sha256sum] = "09a4b1b9e39d57d57dd6ee5f3cc541f8d800c86509419f154503fe223ebf18ba"
