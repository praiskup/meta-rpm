SUMMARY = "generated recipe based on dnsmasq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs gmp libidn2 nettle pkgconfig-native"
RPM_SONAME_REQ_dnsmasq = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libgmp.so.10 libhogweed.so.4 libidn2.so.0 libnettle.so.6"
RDEPENDS_dnsmasq = "bash dbus-libs glibc gmp libidn2 nettle systemd"
RPM_SONAME_REQ_dnsmasq-utils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_dnsmasq-utils = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dnsmasq-2.79-11.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dnsmasq-utils-2.79-11.el8_2.1.aarch64.rpm \
          "

SRC_URI[dnsmasq.sha256sum] = "9d3e1b9d1012064bcc260e6dad586fe64a4fe7aa9bc8858e0953006eedcff885"
SRC_URI[dnsmasq-utils.sha256sum] = "6f6e15383c29c82296038db69a9ce1f73c87cb382fa76eb2fc8f8654ac775c2b"
