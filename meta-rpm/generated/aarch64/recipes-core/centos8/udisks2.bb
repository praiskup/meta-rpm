SUMMARY = "generated recipe based on udisks2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl glib-2.0 iscsi-initiator-utils libatasmart libblockdev libgcc libgudev libmount pkgconfig-native polkit systemd-libs"
RPM_SONAME_PROV_libudisks2 = "libudisks2.so.0"
RPM_SONAME_REQ_libudisks2 = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_libudisks2 = "glib2 glibc"
RPM_SONAME_REQ_udisks2 = "ld-linux-aarch64.so.1 libacl.so.1 libatasmart.so.4 libbd_utils.so.2 libblockdev.so.2 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmount.so.1 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsystemd.so.0 libudisks2.so.0"
RDEPENDS_udisks2 = "bash dbus dosfstools e2fsprogs gdisk glib2 glibc libacl libatasmart libblockdev libblockdev-crypto libblockdev-fs libblockdev-loop libblockdev-mdraid libblockdev-part libblockdev-swap libblockdev-utils libgcc libgudev libmount libudisks2 polkit-libs systemd systemd-libs systemd-udev util-linux xfsprogs"
RPM_SONAME_PROV_udisks2-iscsi = "libudisks2_iscsi.so"
RPM_SONAME_REQ_udisks2-iscsi = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libiscsi.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_udisks2-iscsi = "glib2 glibc iscsi-initiator-utils libgudev polkit-libs udisks2"
RPM_SONAME_PROV_udisks2-lvm2 = "libudisks2_lvm2.so"
RPM_SONAME_REQ_udisks2-lvm2 = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_udisks2-lvm2 = "glib2 glibc libblockdev-lvm libgudev lvm2 polkit-libs udisks2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libudisks2-2.8.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/udisks2-2.8.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/udisks2-iscsi-2.8.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/udisks2-lvm2-2.8.3-2.el8.aarch64.rpm \
          "

SRC_URI[libudisks2.sha256sum] = "e7abba6960fb5f506f7d07382a0567192f24fc076b7e7db069a3119be21f1010"
SRC_URI[udisks2.sha256sum] = "d1e6231d8b2297b3d1d9cb1d922dc2ec95936c4ba011b3af7b8967c05d6c59f0"
SRC_URI[udisks2-iscsi.sha256sum] = "8de54a18ef3c9598843114f04e849d7adc841d612d392d73d5cdae3610ab05b4"
SRC_URI[udisks2-lvm2.sha256sum] = "1654a9b483612cd6f62a329818196ba192ead7423fe96ab11091db9e429dc945"
