SUMMARY = "generated recipe based on libvirt-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libvirt libxml2 pkgconfig-native"
RPM_SONAME_PROV_libvirt-gconfig = "libvirt-gconfig-1.0.so.0"
RPM_SONAME_REQ_libvirt-gconfig = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libxml2.so.2"
RDEPENDS_libvirt-gconfig = "glib2 glibc libxml2"
RPM_SONAME_PROV_libvirt-glib = "libvirt-glib-1.0.so.0"
RPM_SONAME_REQ_libvirt-glib = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libvirt.so.0"
RDEPENDS_libvirt-glib = "glib2 glibc libvirt-libs"
RPM_SONAME_PROV_libvirt-gobject = "libvirt-gobject-1.0.so.0"
RPM_SONAME_REQ_libvirt-gobject = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libvirt-gconfig-1.0.so.0 libvirt-glib-1.0.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_libvirt-gobject = "glib2 glibc libvirt-gconfig libvirt-glib libvirt-libs libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-gconfig-2.0.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-glib-2.0.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvirt-gobject-2.0.0-1.el8.aarch64.rpm \
          "

SRC_URI[libvirt-gconfig.sha256sum] = "9490a6ab3b4c881ff10937fbafd9aa34671eecfd08574b0259ddad918dd61af7"
SRC_URI[libvirt-glib.sha256sum] = "e6749ebd1801ea12f0cb78010c8f18d6c40cc2d82b2e2c6530f3aa45128b35c9"
SRC_URI[libvirt-gobject.sha256sum] = "2b4788d6f4b043bdc65d630996974c645a262e9b298543ec16fd90fc28c59992"
