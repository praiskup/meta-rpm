SUMMARY = "generated recipe based on ftp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_ftp = "ld-linux-aarch64.so.1 libc.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_ftp = "glibc ncurses-libs readline"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ftp-0.17-78.el8.aarch64.rpm \
          "

SRC_URI[ftp.sha256sum] = "f4447871f3a7f5a7446a794f6ad1b31aa40567a9ac95a414889626ed0a97e50f"
