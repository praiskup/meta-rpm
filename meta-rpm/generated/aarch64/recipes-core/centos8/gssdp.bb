SUMMARY = "generated recipe based on gssdp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libsoup-2.4 pkgconfig-native"
RPM_SONAME_PROV_gssdp = "libgssdp-1.0.so.3"
RPM_SONAME_REQ_gssdp = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libsoup-2.4.so.1"
RDEPENDS_gssdp = "dbus glib2 glibc libsoup"
RPM_SONAME_REQ_gssdp-devel = "libgssdp-1.0.so.3"
RPROVIDES_gssdp-devel = "gssdp-dev (= 1.0.2)"
RDEPENDS_gssdp-devel = "glib2-devel gssdp pkgconf-pkg-config"
RDEPENDS_gssdp-docs = "gssdp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gssdp-1.0.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gssdp-devel-1.0.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gssdp-docs-1.0.2-6.el8.noarch.rpm \
          "

SRC_URI[gssdp.sha256sum] = "36c9c70c15d1f2ca08456d3bcfb5a1e68498834085a79ef7a8afb2fb06089118"
SRC_URI[gssdp-devel.sha256sum] = "cb8733fec3013eae289354d3f337bd548f741c3008d91799ad667e8f8726d2a0"
SRC_URI[gssdp-docs.sha256sum] = "1f9d77021f0d351ee83aaeeaa07ff256aa2bf3d6628fb9b1621496d482435bde"
