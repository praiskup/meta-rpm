SUMMARY = "generated recipe based on qt5-qtcanvas3d srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtcanvas3d = "libqtcanvas3d.so"
RPM_SONAME_REQ_qt5-qtcanvas3d = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtcanvas3d = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtcanvas3d-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtcanvas3d-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtcanvas3d qt5-qtdeclarative"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtcanvas3d-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtcanvas3d-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtcanvas3d.sha256sum] = "b8e07f6b97f4d3375f685c07a7dea38c9aa505261bc4387bf7b7761b013e7fa7"
SRC_URI[qt5-qtcanvas3d-examples.sha256sum] = "3939db4bccca1124ce5b73c88b00172518128c27c0d8174b169b8ddb9f8c7713"
