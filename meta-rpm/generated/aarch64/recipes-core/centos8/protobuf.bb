SUMMARY = "generated recipe based on protobuf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_protobuf = "libprotobuf.so.15"
RPM_SONAME_REQ_protobuf = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_protobuf = "glibc libgcc libstdc++ zlib"
RPM_SONAME_PROV_protobuf-compiler = "libprotoc.so.15"
RPM_SONAME_REQ_protobuf-compiler = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libprotobuf.so.15 libprotoc.so.15 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_protobuf-compiler = "glibc libgcc libstdc++ protobuf zlib"
RPM_SONAME_REQ_protobuf-devel = "libprotobuf.so.15 libprotoc.so.15"
RPROVIDES_protobuf-devel = "protobuf-dev (= 3.5.0)"
RDEPENDS_protobuf-devel = "pkgconf-pkg-config protobuf protobuf-compiler zlib-devel"
RPM_SONAME_PROV_protobuf-lite = "libprotobuf-lite.so.15"
RPM_SONAME_REQ_protobuf-lite = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_protobuf-lite = "glibc libgcc libstdc++ zlib"
RDEPENDS_python3-protobuf = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/protobuf-3.5.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/protobuf-lite-3.5.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-protobuf-3.5.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/protobuf-compiler-3.5.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/protobuf-devel-3.5.0-7.el8.aarch64.rpm \
          "

SRC_URI[protobuf.sha256sum] = "4f59da6fd38e5be951a01baa6b0f561d0d2471abdd992e2641a9d27a9ad90b1e"
SRC_URI[protobuf-compiler.sha256sum] = "e93d75856e05464d4db2f1baaf052a372c12d4cff4fe0bdb2209dc8b5d5d2589"
SRC_URI[protobuf-devel.sha256sum] = "2d7ec2d6bef21174d8a5bff66d8ac1683dea3db1facc4272958859619f7fc4ad"
SRC_URI[protobuf-lite.sha256sum] = "402e01aedfffb6fb9f1a2a22e58665e0dbaea2df7eed3b41f70aa29865b0cc07"
SRC_URI[python3-protobuf.sha256sum] = "cec600b0e7b474e1d60501a011603ff9dfb3ab8b98b4f369fb578e81698a7560"
