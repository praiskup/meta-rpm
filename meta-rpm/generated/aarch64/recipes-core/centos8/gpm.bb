SUMMARY = "generated recipe based on gpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_gpm = "ld-linux-aarch64.so.1 libc.so.6 libgpm.so.2 libm.so.6"
RDEPENDS_gpm = "bash glibc gpm-libs info linuxconsoletools systemd"
RPM_SONAME_REQ_gpm-devel = "libgpm.so.2"
RPROVIDES_gpm-devel = "gpm-dev (= 1.20.7)"
RDEPENDS_gpm-devel = "gpm gpm-libs"
RPM_SONAME_PROV_gpm-libs = "libgpm.so.2"
RPM_SONAME_REQ_gpm-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_gpm-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gpm-1.20.7-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gpm-devel-1.20.7-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gpm-libs-1.20.7-15.el8.aarch64.rpm \
          "

SRC_URI[gpm.sha256sum] = "ff01f194aa720746acb2d8e4aa99c70223b4adfd05566a7622d48f92d1d32b73"
SRC_URI[gpm-devel.sha256sum] = "acdf320085f386b51b46ea51fada9a92258e088887469049acfc4bf8e050200b"
SRC_URI[gpm-libs.sha256sum] = "99c8cbcf0c213cd51a68dfcfdaf940848258027eaab0f8dc6213d1ddf7f3e379"
