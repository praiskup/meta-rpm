SUMMARY = "generated recipe based on gegl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "babl cairo gdk-pixbuf glib-2.0 ilmbase jasper libgcc libjpeg-turbo libopenraw libpng librsvg libspiro openexr pango pkgconfig-native sdl suitesparse zlib"
RPM_SONAME_PROV_gegl = "libgegl-0.2.so.0"
RPM_SONAME_REQ_gegl = "ld-linux-aarch64.so.1 libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmImf-2_2.so.22 libIlmThread-2_2.so.12 libImath-2_2.so.12 libSDL-1.2.so.0 libbabl-0.1.so.0 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgegl-0.2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libjasper.so.4 libjpeg.so.62 libm.so.6 libopenraw.so.7 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librsvg-2.so.2 libspiro.so.0 libstdc++.so.6 libumfpack.so.5 libz.so.1"
RDEPENDS_gegl = "OpenEXR-libs SDL babl cairo dcraw gdk-pixbuf2 glib2 glibc ilmbase jasper-libs libgcc libjpeg-turbo libopenraw libpng librsvg2 libspiro libstdc++ pango suitesparse zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gegl-0.2.0-39.el8.aarch64.rpm \
          "

SRC_URI[gegl.sha256sum] = "ea48723f14758cec4d370137af6cad8a6f52b041b5e72a53fac33a48481769ff"
