SUMMARY = "generated recipe based on evolution srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo enchant evolution-data-server gcr gdk-pixbuf glib-2.0 gnome-autoar gnome-desktop3 gtk+3 gtkspell3 libarchive libcanberra libgdata libgweather libical libnotify libsecret libsoup-2.4 libx11 libxml2 nspr nss openldap p11-kit pango pkgconfig-native sqlite3 webkit2gtk3"
RPM_SONAME_PROV_evolution = "libeabutil.so libeabwidgets.so libecontacteditor.so libecontactlisteditor.so libecontactprint.so libedomutils.so libemail-engine.so libessmime.so libevolution-addressbook-importers.so libevolution-calendar-importers.so libevolution-calendar.so libevolution-mail-composer.so libevolution-mail-formatter.so libevolution-mail-importers.so libevolution-mail.so libevolution-shell.so libevolution-smime.so libevolution-util.so libewebextension.so libgnomecanvas.so liborg-gnome-dbx-import.so liborg-gnome-email-custom-header.so liborg-gnome-evolution-attachment-reminder.so liborg-gnome-evolution-bbdb.so liborg-gnome-external-editor.so liborg-gnome-face.so liborg-gnome-mail-notification.so liborg-gnome-mail-to-task.so liborg-gnome-mailing-list-actions.so liborg-gnome-prefer-plain.so liborg-gnome-publish-calendar.so liborg-gnome-save-calendar.so liborg-gnome-templates.so"
RPM_SONAME_REQ_evolution = "ld-linux-aarch64.so.1 libX11.so.6 libarchive.so.13 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcamel-1.2.so.61 libcanberra-gtk3.so.0 libcanberra.so.0 libdl.so.2 libeabutil.so libeabwidgets.so libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libecal-1.2.so.19 libecontacteditor.so libecontactlisteditor.so libecontactprint.so libedata-book-1.2.so.25 libedataserver-1.2.so.23 libedataserverui-1.2.so.2 libedomutils.so libemail-engine.so libenchant.so.1 libessmime.so libevolution-addressbook-importers.so libevolution-calendar-importers.so libevolution-calendar.so libevolution-mail-composer.so libevolution-mail-formatter.so libevolution-mail-importers.so libevolution-mail.so libevolution-shell.so libevolution-smime.so libevolution-util.so libgailutil-3.so.0 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-autoar-0.so.0 libgnome-autoar-gtk-0.so.0 libgnome-desktop-3.so.17 libgnomecanvas.so libgobject-2.0.so.0 libgtk-3.so.0 libgtkspell3-3.so.0 libgweather-3.so.15 libical.so.3 libicalss.so.3 libicalvcal.so.3 libjavascriptcoregtk-4.0.so.18 liblber-2.4.so.2 libldap-2.4.so.2 libm.so.6 libnotify.so.4 libnspr4.so libnss3.so libnssutil3.so libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libplc4.so libplds4.so libpthread.so.0 libresolv.so.2 libsecret-1.so.0 libsmime3.so libsoup-2.4.so.1 libsqlite3.so.0 libssl3.so libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_evolution = "atk cairo cairo-gobject enchant evolution-data-server evolution-langpacks gcr gdk-pixbuf2 glib2 glibc gnome-autoar gnome-desktop3 gtk3 gtkspell3 gvfs highlight libX11 libarchive libcanberra libcanberra-gtk3 libgweather libical libnotify libsecret libsoup libxml2 nspr nss nss-util openldap p11-kit pango psmisc sqlite-libs webkit2gtk3 webkit2gtk3-jsc"
RPROVIDES_evolution-devel = "evolution-dev (= 3.28.5)"
RDEPENDS_evolution-devel = "enchant-devel evolution evolution-data-server-devel glib2-devel gnome-desktop3-devel gtk3-devel gtkspell3-devel libgdata-devel libgweather-devel libsoup-devel libxml2-devel pkgconf-pkg-config webkit2gtk3-devel"
RDEPENDS_evolution-help = "evolution yelp"
RDEPENDS_evolution-langpacks = "evolution"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-3.28.5-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-help-3.28.5-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-langpacks-3.28.5-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/evolution-devel-3.28.5-12.el8.aarch64.rpm \
          "

SRC_URI[evolution.sha256sum] = "5623fe969ed68802eeb644d620cca378160039bddddbf964344eb7cfc0c7f317"
SRC_URI[evolution-devel.sha256sum] = "244ea42d0186beda7bbe95ee622495e53082e076253a42a237eef7704de9e363"
SRC_URI[evolution-help.sha256sum] = "fa7013e3bf316f24b1d8ad522829b128d86de1dcbd3840d05549298ef8bc7acf"
SRC_URI[evolution-langpacks.sha256sum] = "a9092a09b24225d87892fbef1e52e86f4a65b1ee653bc82283900066f8d77188"
