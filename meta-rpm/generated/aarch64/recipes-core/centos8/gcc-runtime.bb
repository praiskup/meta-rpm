SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libatomic = "libatomic.so.1"
RPM_SONAME_REQ_libatomic = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_libatomic = "glibc info"
RDEPENDS_libatomic-static = "libatomic"
RPM_SONAME_PROV_libgomp = "libgomp.so.1"
RPM_SONAME_REQ_libgomp = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_libgomp = "bash glibc info"
RPM_SONAME_PROV_libitm = "libitm.so.1"
RPM_SONAME_REQ_libitm = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_libitm = "bash glibc info"
RPM_SONAME_REQ_libitm-devel = "libitm.so.1"
RPROVIDES_libitm-devel = "libitm-dev (= 8.3.1)"
RDEPENDS_libitm-devel = "gcc libitm"
RPM_SONAME_PROV_libstdc++ = "libstdc++.so.6"
RPM_SONAME_REQ_libstdc++ = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6"
RDEPENDS_libstdc++ = "glibc libgcc"
RPM_SONAME_REQ_libstdc++-devel = "libstdc++.so.6"
RPROVIDES_libstdc++-devel = "libstdc++-dev (= 8.3.1)"
RDEPENDS_libstdc++-devel = "libstdc++"
RDEPENDS_libstdc++-static = "libstdc++-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libitm-devel-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libstdc++-devel-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libstdc++-docs-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libatomic-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libatomic-static-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgomp-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libitm-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libstdc++-8.3.1-5.el8.0.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libstdc++-static-8.3.1-5.el8.0.2.aarch64.rpm \
          "

SRC_URI[libatomic.sha256sum] = "b1df42673eea96305ea63da12398a64fa6afc6f3f65a8488f5e1b0a448966a39"
SRC_URI[libatomic-static.sha256sum] = "ea54f03bc30dc513860fe6cace8337c8f11ea2bbcaa8d158a5c27b4f8ed6c807"
SRC_URI[libgomp.sha256sum] = "dbe5fd8af1be211256da3bf4f9657fb07ac4f5b8c1639d3c09e81a02c8153715"
SRC_URI[libitm.sha256sum] = "bc3102a72cdf6817e8c6750169591782027ad2cb685eb2d81a800dbaf7e4a36d"
SRC_URI[libitm-devel.sha256sum] = "96c906e53ff747816e792998b1daff14685df9c60a5d7912a91e1a8584809ead"
SRC_URI[libstdc++.sha256sum] = "3eda94720ea5bf2c6f1e55dee0b817b1ca117d3e5c87c415ad30c430ae18a767"
SRC_URI[libstdc++-devel.sha256sum] = "973560ea524046e4847e3420f0be266154f1ef4a2fa84c5e9ce5d0df9987acf0"
SRC_URI[libstdc++-docs.sha256sum] = "e3ab40ecb46d58df0860489f29314223e05ebd7a08d1f928eb0b0b06cad091bd"
SRC_URI[libstdc++-static.sha256sum] = "852c6ef6df00c971bce2a74438a09d056c3c7851e3a5af83b0a82fa6cc9a0e0b"
