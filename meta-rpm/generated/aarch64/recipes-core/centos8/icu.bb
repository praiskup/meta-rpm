SUMMARY = "generated recipe based on icu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_icu = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuio.so.60 libicutu.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_icu = "glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_libicu = "libicudata.so.60 libicui18n.so.60 libicuio.so.60 libicutest.so.60 libicutu.so.60 libicuuc.so.60"
RPM_SONAME_REQ_libicu = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicutu.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libicu = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libicu-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuio.so.60 libicutest.so.60 libicutu.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_libicu-devel = "libicu-dev (= 60.3)"
RDEPENDS_libicu-devel = "bash glibc libgcc libicu libstdc++ pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/icu-60.3-2.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libicu-60.3-2.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libicu-devel-60.3-2.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libicu-doc-60.3-2.el8_1.noarch.rpm \
          "

SRC_URI[icu.sha256sum] = "aafe88fc6b2fb42117baa76be3670aaba068bbe541275f97b1ef424dd42a9df0"
SRC_URI[libicu.sha256sum] = "0c1f449b4ae0a3106160210d008aa3d64835384bbd316db4e455639e5f0167d5"
SRC_URI[libicu-devel.sha256sum] = "fd4cbc356e9b03ee060831a7f9fe28880809344697870a69365793760ad1e8c1"
SRC_URI[libicu-doc.sha256sum] = "779b80c0b92442477af96167d035690beed9891eb5061ab4e800d8b81df667d2"
