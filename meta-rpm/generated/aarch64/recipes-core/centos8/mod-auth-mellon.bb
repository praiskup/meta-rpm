SUMMARY = "generated recipe based on mod_auth_mellon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 lasso openssl pkgconfig-native"
RPM_SONAME_REQ_mod_auth_mellon = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libglib-2.0.so.0 liblasso.so.3 libssl.so.1.1"
RDEPENDS_mod_auth_mellon = "bash glib2 glibc httpd lasso libcurl openssl-libs"
RPM_SONAME_REQ_mod_auth_mellon-diagnostics = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libglib-2.0.so.0 liblasso.so.3 libssl.so.1.1"
RDEPENDS_mod_auth_mellon-diagnostics = "glib2 glibc lasso libcurl mod_auth_mellon openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_auth_mellon-0.14.0-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_auth_mellon-diagnostics-0.14.0-11.el8.aarch64.rpm \
          "

SRC_URI[mod_auth_mellon.sha256sum] = "51feaca5e32a88ffb0b8ccfa581328f218eaa467797912539e8248f328bcaf1d"
SRC_URI[mod_auth_mellon-diagnostics.sha256sum] = "6d7619bfe41e4e7e47741008e760e7d7718068ec38376aedf73875962491862b"
