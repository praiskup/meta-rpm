SUMMARY = "generated recipe based on vinagre srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk avahi avahi-libs cairo freerdp gdk-pixbuf glib-2.0 gnutls gtk+3 gtk-vnc libpcre2 libsecret libx11 libxml2 pango pkgconfig-native vte291 zlib"
RPM_SONAME_REQ_vinagre = "ld-linux-aarch64.so.1 libX11.so.6 libatk-1.0.so.0 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libavahi-gobject.so.0 libavahi-ui-gtk3.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libfreerdp2.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgtk-vnc-2.0.so.0 libgvnc-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpcre2-8.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1 libvte-2.91.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_vinagre = "atk avahi-glib avahi-gobject avahi-libs avahi-ui-gtk3 cairo cairo-gobject dbus desktop-file-utils freerdp-libs gdk-pixbuf2 glib2 glibc gnutls gtk-vnc2 gtk3 gvnc libX11 libsecret libxml2 pango pcre2 shared-mime-info vte291 zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vinagre-3.22.0-21.el8.aarch64.rpm \
          "

SRC_URI[vinagre.sha256sum] = "f03c11812ffec408a8e604f68fe7894740a43265f070781058b666b17c317d60"
