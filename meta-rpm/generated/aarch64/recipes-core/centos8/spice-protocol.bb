SUMMARY = "generated recipe based on spice-protocol srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_spice-protocol = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-protocol-0.14.0-1.el8.noarch.rpm \
          "

SRC_URI[spice-protocol.sha256sum] = "8e706e451498c7798648f07ba81858eeee9682f42b8ac708a84e4a864333b57e"
