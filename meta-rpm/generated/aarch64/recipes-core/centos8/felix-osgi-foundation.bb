SUMMARY = "generated recipe based on felix-osgi-foundation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_felix-osgi-foundation = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_felix-osgi-foundation-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-osgi-foundation-1.2.0-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/felix-osgi-foundation-javadoc-1.2.0-23.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[felix-osgi-foundation.sha256sum] = "4028b88118b3b894b388219cbd48b155683dc8710965271af5f575ddf1503a84"
SRC_URI[felix-osgi-foundation-javadoc.sha256sum] = "38b21a23e7760c62904f5fd602aa13518f3c137536e439b7cab56bb596dee9b3"
