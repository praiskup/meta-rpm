SUMMARY = "generated recipe based on xorg-x11-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdmx libx11 libxcb libxcomposite libxext libxi libxinerama libxrandr libxrender libxtst libxv libxxf86dga libxxf86misc libxxf86vm pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-utils = "ld-linux-aarch64.so.1 libX11-xcb.so.1 libX11.so.6 libXcomposite.so.1 libXext.so.6 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libXtst.so.6 libXv.so.1 libXxf86dga.so.1 libXxf86misc.so.1 libXxf86vm.so.1 libc.so.6 libdmx.so.1 libm.so.6 libxcb-shape.so.0 libxcb.so.1"
RDEPENDS_xorg-x11-utils = "glibc libX11 libX11-xcb libXcomposite libXext libXi libXinerama libXrandr libXrender libXtst libXv libXxf86dga libXxf86misc libXxf86vm libdmx libxcb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-utils-7.5-28.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-utils.sha256sum] = "badc17895ef6fa0d75293877b90a050c4d451bb624cc7df37ec6defd654a59ea"
