SUMMARY = "generated recipe based on perl-MRO-Compat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-MRO-Compat = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-MRO-Compat-0.13-4.el8.noarch.rpm \
          "

SRC_URI[perl-MRO-Compat.sha256sum] = "7d4bf25fe0e682be066c01634f1b517cd672516dda33ab4edbbc7f5ab53a002a"
