SUMMARY = "generated recipe based on libdmx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libdmx = "libdmx.so.1"
RPM_SONAME_REQ_libdmx = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libdmx = "glibc libX11 libXext"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdmx-1.1.4-3.el8.aarch64.rpm \
          "

SRC_URI[libdmx.sha256sum] = "805e52c2b665913ce003af639f8eb931cdea07c35c7fc46fe9fab2fdffb5e5a6"
