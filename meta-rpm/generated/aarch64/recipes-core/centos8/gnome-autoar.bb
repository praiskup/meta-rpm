SUMMARY = "generated recipe based on gnome-autoar srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libarchive libgcc pango pkgconfig-native"
RPM_SONAME_PROV_gnome-autoar = "libgnome-autoar-0.so.0 libgnome-autoar-gtk-0.so.0"
RPM_SONAME_REQ_gnome-autoar = "ld-linux-aarch64.so.1 libarchive.so.13 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-autoar-0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gnome-autoar = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libarchive libgcc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-autoar-0.2.3-1.el8.aarch64.rpm \
          "

SRC_URI[gnome-autoar.sha256sum] = "7bfbc5009c0146df76fe0e4b10dc844d518e7b943b2344988d213260444cb466"
