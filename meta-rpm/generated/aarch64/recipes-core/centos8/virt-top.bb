SUMMARY = "generated recipe based on virt-top srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt ncurses pkgconfig-native"
RPM_SONAME_REQ_virt-top = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libncursesw.so.6 libtinfo.so.6 libvirt.so.0"
RDEPENDS_virt-top = "glibc libvirt-libs ncurses-libs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/virt-top-1.0.8-32.el8.aarch64.rpm \
          "

SRC_URI[virt-top.sha256sum] = "27f2b45765313f29c358808a3f11e67dd72dcdb4c45aa7908fe03b37fa02aa22"
