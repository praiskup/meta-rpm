SUMMARY = "generated recipe based on ocaml-camlp4 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ocaml-camlp4 = "ocaml-compiler-libs ocaml-runtime"
RPM_SONAME_REQ_ocaml-camlp4-devel = "libc.so.6 libdl.so.2 libm.so.6"
RPROVIDES_ocaml-camlp4-devel = "ocaml-camlp4-dev (= 4.07.0)"
RDEPENDS_ocaml-camlp4-devel = "glibc ocaml-camlp4 ocaml-runtime"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-camlp4-4.07.0-0.gitd32d9973.1.el8.3.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-camlp4-devel-4.07.0-0.gitd32d9973.1.el8.3.aarch64.rpm \
          "

SRC_URI[ocaml-camlp4.sha256sum] = "a480a8c0f236de6ba340316e86f3a72489e6aa150308bbbc3cec4decd12c7fe9"
SRC_URI[ocaml-camlp4-devel.sha256sum] = "1ebbbfde3cccdf7f928ea88371b47958ff1ed63876906d364616e96f5128241d"
