SUMMARY = "generated recipe based on perl-Test-Simple srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Simple = "perl-Carp perl-Data-Dumper perl-Exporter perl-File-Temp perl-IO perl-PathTools perl-Scalar-List-Utils perl-Storable perl-Term-ANSIColor perl-interpreter perl-libs perl-threads-shared"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Test-Simple-1.302135-1.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Simple.sha256sum] = "642f1c33ce2f33616113d23b0c51e8b129ed54cd15360e31c8cda0196ebfc44d"
