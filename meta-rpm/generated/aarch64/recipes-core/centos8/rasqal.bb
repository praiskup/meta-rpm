SUMMARY = "generated recipe based on rasqal srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre mpfr pkgconfig-native raptor2"
RPM_SONAME_PROV_rasqal = "librasqal.so.3"
RPM_SONAME_REQ_rasqal = "ld-linux-aarch64.so.1 libc.so.6 libmpfr.so.4 libpcre.so.1 libraptor2.so.0 librasqal.so.3"
RDEPENDS_rasqal = "glibc mpfr pcre raptor2"
RPM_SONAME_REQ_rasqal-devel = "librasqal.so.3"
RPROVIDES_rasqal-devel = "rasqal-dev (= 0.9.33)"
RDEPENDS_rasqal-devel = "bash pkgconf-pkg-config raptor2-devel rasqal"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rasqal-0.9.33-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rasqal-devel-0.9.33-6.el8.aarch64.rpm \
          "

SRC_URI[rasqal.sha256sum] = "2fad02c11e62718087e1a6584b954a5b4ee5ecc4b90ece11937625cc2dab9c41"
SRC_URI[rasqal-devel.sha256sum] = "310e48d95673126d834dbdd9f4a3837868db400db50b32c9cb1f21e3d94e4db4"
