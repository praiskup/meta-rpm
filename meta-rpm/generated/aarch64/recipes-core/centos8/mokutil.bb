SUMMARY = "generated recipe based on mokutil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "efivar libxcrypt openssl pkgconfig-native"
RPM_SONAME_REQ_mokutil = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libefivar.so.1 libssl.so.1.1"
RDEPENDS_mokutil = "efivar-libs glibc libxcrypt openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mokutil-0.3.0-9.el8.aarch64.rpm \
          "

SRC_URI[mokutil.sha256sum] = "8c51a122cdeeda7d93b8390f20cc4127172b97d5e00aa3b163ac315b35a89f5a"
