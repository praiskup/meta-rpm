SUMMARY = "generated recipe based on munge srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 libgcc libgcrypt libgpg-error pkgconfig-native zlib"
RPM_SONAME_REQ_munge = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgpg-error.so.0 libmunge.so.2 libpthread.so.0 libz.so.1"
RDEPENDS_munge = "bash bzip2-libs glibc libgcc libgcrypt libgpg-error munge-libs shadow-utils systemd zlib"
RPM_SONAME_REQ_munge-devel = "libmunge.so.2"
RPROVIDES_munge-devel = "munge-dev (= 0.5.13)"
RDEPENDS_munge-devel = "munge-libs pkgconf-pkg-config"
RPM_SONAME_PROV_munge-libs = "libmunge.so.2"
RPM_SONAME_REQ_munge-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_munge-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/munge-0.5.13-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/munge-libs-0.5.13-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/munge-devel-0.5.13-1.el8.aarch64.rpm \
          "

SRC_URI[munge.sha256sum] = "23a2e69a7f77e99f0ff035e3d13d13088579bae38b59447a4007b742a046ad28"
SRC_URI[munge-devel.sha256sum] = "f98f09a2d78db9cf33bf56ba6c3d3a4136e48a4fba96ff268d10ad0c1bfd651e"
SRC_URI[munge-libs.sha256sum] = "d6f8a137f229c081efc2133e57eee2b22a0420be9705026c5923122762d916d4"
