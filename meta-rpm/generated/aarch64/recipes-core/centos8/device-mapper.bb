SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs libselinux pkgconfig-native systemd"
RPM_SONAME_REQ_device-mapper = "ld-linux-aarch64.so.1 libc.so.6 libdevmapper.so.1.02"
RDEPENDS_device-mapper = "bash device-mapper-libs glibc systemd util-linux"
RPM_SONAME_REQ_device-mapper-devel = "libdevmapper.so.1.02"
RPROVIDES_device-mapper-devel = "device-mapper-dev (= 1.02.169)"
RDEPENDS_device-mapper-devel = "device-mapper device-mapper-libs libselinux-devel pkgconf-pkg-config systemd-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-1.02.169-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/device-mapper-devel-1.02.169-3.el8.aarch64.rpm \
          "

SRC_URI[device-mapper.sha256sum] = "846b89c4ae4b4b34d207c5ddbd7d4ae66f6312cca7ebe08006f48323c91aa80f"
SRC_URI[device-mapper-devel.sha256sum] = "83cc6b6253234cb75b1a3f30753e2362e32e126d78014a06ad6ddd8b3d7b789b"
