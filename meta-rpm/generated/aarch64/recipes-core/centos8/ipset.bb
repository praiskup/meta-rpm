SUMMARY = "generated recipe based on ipset srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_REQ_ipset = "libc.so.6 libipset.so.13"
RDEPENDS_ipset = "bash glibc ipset-libs"
RPM_SONAME_REQ_ipset-devel = "libipset.so.13"
RPROVIDES_ipset-devel = "ipset-dev (= 7.1)"
RDEPENDS_ipset-devel = "ipset-libs libmnl-devel pkgconf-pkg-config"
RPM_SONAME_PROV_ipset-libs = "libipset.so.13"
RPM_SONAME_REQ_ipset-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libmnl.so.0"
RDEPENDS_ipset-libs = "glibc libmnl"
RDEPENDS_ipset-service = "bash ipset iptables-services systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ipset-7.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ipset-libs-7.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ipset-service-7.1-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ipset-devel-7.1-1.el8.aarch64.rpm \
          "

SRC_URI[ipset.sha256sum] = "979928665e57d4a33024ae2098579d051caeaf505e6e0bf20006647d74956581"
SRC_URI[ipset-devel.sha256sum] = "99f6f0654f8afc8032fcf8b34aea56d34c1c14ee0d40e0219e95776b3bcef2d6"
SRC_URI[ipset-libs.sha256sum] = "5a0a3ebbf94f1c49c9dab50c210e568ea3015db2fe4340652c9883ea249e898d"
SRC_URI[ipset-service.sha256sum] = "2fea3dcc8b91ebda627d9c4da9c93db70fd53a4f062b1923a880f1006ee29516"
