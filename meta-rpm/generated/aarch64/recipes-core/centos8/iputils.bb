SUMMARY = "generated recipe based on iputils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap libidn2 openssl pkgconfig-native"
RPM_SONAME_REQ_iputils = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libidn2.so.0 libm.so.6 libresolv.so.2 librt.so.1"
RDEPENDS_iputils = "bash glibc libcap libidn2 openssl-libs systemd"
RPM_SONAME_REQ_iputils-ninfod = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_iputils-ninfod = "bash glibc iputils libcap openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iputils-20180629-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iputils-ninfod-20180629-2.el8.aarch64.rpm \
          "

SRC_URI[iputils.sha256sum] = "4948d75c8b9a2abd4cb29fd516308e319ffdadabbd4aab623123ab35b9bc1b77"
SRC_URI[iputils-ninfod.sha256sum] = "536368377a2373b0e2da1d8e376c66c9f41951dcae3ee8bfb26218594d9d1e21"
