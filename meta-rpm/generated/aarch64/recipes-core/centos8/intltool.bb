SUMMARY = "generated recipe based on intltool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_intltool = "automake bash gettext-devel patch perl-Encode perl-Getopt-Long perl-PathTools perl-Text-Tabs+Wrap perl-XML-Parser perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/intltool-0.51.0-11.el8.noarch.rpm \
          "

SRC_URI[intltool.sha256sum] = "89180457c6652a7c2d6bbf3c8637408c786a7854c6bc055134c49dc74f84aa66"
