SUMMARY = "generated recipe based on perl-Sub-Uplevel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Uplevel = "perl-Carp perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Sub-Uplevel-0.2800-4.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Uplevel.sha256sum] = "c10d1211ce3fc8a31fc124f962cef6631cc9f86ca6e1461381f4164d98a1061f"
