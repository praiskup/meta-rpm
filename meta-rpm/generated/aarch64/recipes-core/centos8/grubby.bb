SUMMARY = "generated recipe based on grubby srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native rpm"
RPM_SONAME_REQ_grubby = "ld-linux-aarch64.so.1 libc.so.6 librpm.so.8"
RDEPENDS_grubby = "bash findutils glibc grub2-tools grub2-tools-minimal rpm-libs util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grubby-8.40-38.el8.aarch64.rpm \
          "

SRC_URI[grubby.sha256sum] = "378dd162d1d62232663434e861ad7421b9e7ce091ed7b73d016992d33d00f0c4"
