SUMMARY = "generated recipe based on yajl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_yajl = "libyajl.so.2"
RPM_SONAME_REQ_yajl = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libyajl.so.2"
RDEPENDS_yajl = "glibc"
RPM_SONAME_REQ_yajl-devel = "libyajl.so.2"
RPROVIDES_yajl-devel = "yajl-dev (= 2.1.0)"
RDEPENDS_yajl-devel = "pkgconf-pkg-config yajl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/yajl-2.1.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/yajl-devel-2.1.0-10.el8.aarch64.rpm \
          "

SRC_URI[yajl.sha256sum] = "255e74b387f5e9b517d82cd00f3b62af88b32054095be91a63b3e5eb5db34939"
SRC_URI[yajl-devel.sha256sum] = "ffae4456529e8ce347aa14290327559490a696ce6d35fff21138ca7d7d0e1ee1"
