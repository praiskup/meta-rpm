SUMMARY = "generated recipe based on libavc1394 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libraw1394 pkgconfig-native"
RPM_SONAME_PROV_libavc1394 = "libavc1394.so.0 librom1394.so.0"
RPM_SONAME_REQ_libavc1394 = "ld-linux-aarch64.so.1 libavc1394.so.0 libc.so.6 libm.so.6 libraw1394.so.11 librom1394.so.0"
RDEPENDS_libavc1394 = "glibc libraw1394"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libavc1394-0.5.4-7.el8.aarch64.rpm \
          "

SRC_URI[libavc1394.sha256sum] = "08214cb23554191ea623645cb503ce2d5d82c413506adaca8f45f09f97a0069c"
