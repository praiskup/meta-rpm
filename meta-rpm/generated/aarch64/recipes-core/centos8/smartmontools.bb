SUMMARY = "generated recipe based on smartmontools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap-ng libgcc libselinux pkgconfig-native"
RPM_SONAME_REQ_smartmontools = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libgcc_s.so.1 libm.so.6 libselinux.so.1 libstdc++.so.6"
RDEPENDS_smartmontools = "bash glibc libcap-ng libgcc libselinux libstdc++ systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/smartmontools-6.6-3.el8.aarch64.rpm \
          "

SRC_URI[smartmontools.sha256sum] = "04e2a1ccbe991d0fca70e9b2215e61ede179ceea3ae5cf30599aa70df11747df"
