SUMMARY = "generated recipe based on pangomm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo cairomm glib-2.0 glibmm24 libgcc libsigc++20 pango pkgconfig-native"
RPM_SONAME_PROV_pangomm = "libpangomm-1.4.so.1"
RPM_SONAME_REQ_pangomm = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libcairomm-1.0.so.1 libgcc_s.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_pangomm = "cairo cairomm glib2 glibc glibmm24 libgcc libsigc++20 libstdc++ pango"
RPM_SONAME_REQ_pangomm-devel = "libpangomm-1.4.so.1"
RPROVIDES_pangomm-devel = "pangomm-dev (= 2.40.1)"
RDEPENDS_pangomm-devel = "cairomm-devel glibmm24-devel pango-devel pangomm pkgconf-pkg-config"
RDEPENDS_pangomm-doc = "glibmm24-doc libsigc++20-doc pangomm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pangomm-2.40.1-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pangomm-devel-2.40.1-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pangomm-doc-2.40.1-5.el8.noarch.rpm \
          "

SRC_URI[pangomm.sha256sum] = "450d75b48ebbb6f9322b9bd75f11f74b1943cfa5ac1c783c731ae211e4a7daa9"
SRC_URI[pangomm-devel.sha256sum] = "2e933690db3e041cc6b867fe0cf943ae76615da39a7dd61e9873a896ee89dc9c"
SRC_URI[pangomm-doc.sha256sum] = "a551143cee5db5a095f7be46f0fff24b594a0d05c09e5ab9560994a1a39dfab7"
