SUMMARY = "generated recipe based on spirv-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_spirv-tools = "ld-linux-aarch64.so.1 libSPIRV-Tools-link.so libSPIRV-Tools-opt.so libSPIRV-Tools-reduce.so libSPIRV-Tools.so libc.so.6 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6"
RDEPENDS_spirv-tools = "bash glibc libgcc libstdc++ spirv-tools-libs"
RPROVIDES_spirv-tools-devel = "spirv-tools-dev (= 2019.5)"
RDEPENDS_spirv-tools-devel = "cmake-filesystem pkgconf-pkg-config spirv-tools-libs"
RPM_SONAME_PROV_spirv-tools-libs = "libSPIRV-Tools-link.so libSPIRV-Tools-opt.so libSPIRV-Tools-reduce.so libSPIRV-Tools-shared.so libSPIRV-Tools.so"
RPM_SONAME_REQ_spirv-tools-libs = "ld-linux-aarch64.so.1 libSPIRV-Tools-opt.so libSPIRV-Tools.so libc.so.6 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6"
RDEPENDS_spirv-tools-libs = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spirv-tools-2019.5-2.20200421.git67f4838.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spirv-tools-libs-2019.5-2.20200421.git67f4838.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/spirv-tools-devel-2019.5-2.20200421.git67f4838.el8.aarch64.rpm \
          "

SRC_URI[spirv-tools.sha256sum] = "9116ebcf70ff767ecc8d88b3ffb8ebf944126de6ec331cc63000bacc825d5835"
SRC_URI[spirv-tools-devel.sha256sum] = "90ebc6c112c4f264b72afc65d356b97fc2da3c391a1960e7a0c361413c3dd72e"
SRC_URI[spirv-tools-libs.sha256sum] = "aa5596b36ca620564e1a13bdd1d04a71b8a675da966d2f69acaeea2bc99e823b"
