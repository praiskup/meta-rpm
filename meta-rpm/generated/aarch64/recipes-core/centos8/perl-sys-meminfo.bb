SUMMARY = "generated recipe based on perl-Sys-MemInfo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-MemInfo = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-MemInfo = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Sys-MemInfo-0.99-6.el8.aarch64.rpm \
          "

SRC_URI[perl-Sys-MemInfo.sha256sum] = "60fe4e87faf8986fe232c4246ec8f0a86d5b5e401507260ccaf718a96f295e40"
