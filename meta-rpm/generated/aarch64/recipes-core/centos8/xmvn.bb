SUMMARY = "generated recipe based on xmvn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xmvn = "maven xmvn-minimal"
RDEPENDS_xmvn-api = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_xmvn-bisect = "bash beust-jcommander java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools maven-invoker"
RDEPENDS_xmvn-connector-aether = "java-1.8.0-openjdk-headless javapackages-filesystem xmvn-api xmvn-core"
RDEPENDS_xmvn-connector-ivy = "java-1.8.0-openjdk-headless javapackages-filesystem slf4j xmvn-api"
RDEPENDS_xmvn-core = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_xmvn-install = "apache-commons-compress bash beust-jcommander java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools objectweb-asm slf4j xmvn-api xmvn-core"
RDEPENDS_xmvn-minimal = "apache-commons-cli apache-commons-lang3 atinject bash google-guice guava20 maven-lib maven-resolver-api maven-resolver-impl maven-resolver-spi maven-resolver-util maven-wagon-provider-api plexus-cipher plexus-classworlds plexus-containers-component-annotations plexus-interpolation plexus-sec-dispatcher plexus-utils sisu-inject sisu-plexus slf4j xmvn-api xmvn-connector-aether xmvn-core"
RDEPENDS_xmvn-mojo = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-util xmvn-api xmvn-core"
RDEPENDS_xmvn-parent-pom = "java-1.8.0-openjdk-headless javapackages-filesystem maven-compiler-plugin maven-jar-plugin"
RDEPENDS_xmvn-resolve = "bash beust-jcommander java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools xmvn-api xmvn-core"
RDEPENDS_xmvn-subst = "bash beust-jcommander java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools xmvn-api xmvn-core"
RDEPENDS_xmvn-tools-pom = "java-1.8.0-openjdk-headless javapackages-filesystem xmvn-parent-pom"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-api-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-bisect-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-connector-aether-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-connector-ivy-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-core-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-install-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-javadoc-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-minimal-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-mojo-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-parent-pom-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-resolve-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-subst-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmvn-tools-pom-3.0.0-21.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xmvn.sha256sum] = "e8db37805bca1725a8c1404fe8dc55d080362bb9fbdb12a96a477753f7a5b71a"
SRC_URI[xmvn-api.sha256sum] = "f70313cfbe8ec51e4c93a791c705e81515a5a98932781b002e89138b34ba9585"
SRC_URI[xmvn-bisect.sha256sum] = "9872e04a52a7198172098d255ca50a053eafa9c243fbf06dc3def2042e0835ef"
SRC_URI[xmvn-connector-aether.sha256sum] = "b8f50529ebf41dff0bcd133c05bd4de14891c67c8b2a363e48d0c3cce6067c70"
SRC_URI[xmvn-connector-ivy.sha256sum] = "70f0ecd541e99d6df12b0f6c7c59b717335946764daade35c51e6db746b5b81d"
SRC_URI[xmvn-core.sha256sum] = "f3aab973cf66b23c97fc22485dcc10666abf953cb93fa757dd16fbc97da26b5e"
SRC_URI[xmvn-install.sha256sum] = "c906425bd7163dbea7af5e12f05649167394365e618ea3ea2e1e58013b03a7bf"
SRC_URI[xmvn-javadoc.sha256sum] = "21ec313eabf9094d7ba939ab6b5734354a7a8b021546c7140512f3c6aacb2027"
SRC_URI[xmvn-minimal.sha256sum] = "d009d93b2c5e52f6afe8263b301a9033a2aa9b241a9d6b3ee9d525df9ab800cb"
SRC_URI[xmvn-mojo.sha256sum] = "ae47409826ac2af69500ea7d2b94123ca8418b8db4c0d44bb256e177c03c0c20"
SRC_URI[xmvn-parent-pom.sha256sum] = "5b3f21d061bcc066ab2d65050d377655d9e4e641df9a5f463921a581acc4de2f"
SRC_URI[xmvn-resolve.sha256sum] = "d661ae14fbf89801c1b207424cdd77c0d31fc5d2190d1b73517a49df9ddb4d84"
SRC_URI[xmvn-subst.sha256sum] = "c5ad01117a7b7bbee48dab5ea908220a00c9b82820eba4dacb0ae89fb6dcd78a"
SRC_URI[xmvn-tools-pom.sha256sum] = "bf03322c20751a49ebf97c69d0a786c63d8e1e469098c0e37530b57a82d69db5"
