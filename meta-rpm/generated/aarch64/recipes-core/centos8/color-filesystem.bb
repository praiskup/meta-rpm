SUMMARY = "generated recipe based on color-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_color-filesystem = "filesystem rpm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/color-filesystem-1-20.el8.noarch.rpm \
          "

SRC_URI[color-filesystem.sha256sum] = "8bff7bd153ee903849143db9b4cb011268250e287c0aa0603a4c1e0f195592b8"
