SUMMARY = "generated recipe based on bolt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_bolt = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libudev.so.1"
RDEPENDS_bolt = "bash glib2 glibc libgcc polkit-libs systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bolt-0.8-2.el8.aarch64.rpm \
          "

SRC_URI[bolt.sha256sum] = "574d3b227937323ac7676f89e9ee9b7172095d6af673b3652b64ef1be5ced896"
