SUMMARY = "generated recipe based on isl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp pkgconfig-native"
RPM_SONAME_PROV_isl = "libisl.so.13 libisl.so.15"
RPM_SONAME_REQ_isl = "ld-linux-aarch64.so.1 libc.so.6 libgmp.so.10"
RDEPENDS_isl = "glibc gmp"
RPM_SONAME_REQ_isl-devel = "libisl.so.15"
RPROVIDES_isl-devel = "isl-dev (= 0.16.1)"
RDEPENDS_isl-devel = "gmp-devel isl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/isl-0.16.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/isl-devel-0.16.1-6.el8.aarch64.rpm \
          "

SRC_URI[isl.sha256sum] = "b9bd73b0edcd9573548853bd44f5a58919d9de77d8b1304a4176c7fad726b472"
SRC_URI[isl-devel.sha256sum] = "8b9a931c2feced5ed81debdb5f219ad2d9b9dcc8a2a3a102d0b0159253986710"
