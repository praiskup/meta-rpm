SUMMARY = "generated recipe based on iptraf-ng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_iptraf-ng = "ld-linux-aarch64.so.1 libc.so.6 libncursesw.so.6 libpanel.so.6 libtinfo.so.6"
RDEPENDS_iptraf-ng = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptraf-ng-1.1.4-18.el8.aarch64.rpm \
          "

SRC_URI[iptraf-ng.sha256sum] = "8e0cbfa79e9b52cdb7049b52be382aff1cfb540c51cffcc14785c322e0eaccd6"
