SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgcc = "libgcc_s.so.1"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgcc-8.3.1-5.el8.0.2.aarch64.rpm \
          "

SRC_URI[libgcc.sha256sum] = "9bb31907f761b2fb5eb908374c13aee5ae0b31e3abfba1e0e729e7c2acaf3a74"
