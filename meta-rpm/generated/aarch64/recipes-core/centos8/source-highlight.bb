SUMMARY = "generated recipe based on source-highlight srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost libgcc pkgconfig-native"
RPM_SONAME_PROV_source-highlight = "libsource-highlight.so.4"
RPM_SONAME_REQ_source-highlight = "ld-linux-aarch64.so.1 libboost_regex.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libsource-highlight.so.4 libstdc++.so.6"
RDEPENDS_source-highlight = "bash boost-regex ctags glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/source-highlight-3.1.8-16.el8.aarch64.rpm \
          "

SRC_URI[source-highlight.sha256sum] = "23e9b89df31a87b108dd395f6b4a4c063e83ce840800b59b62023b3244da0b95"
