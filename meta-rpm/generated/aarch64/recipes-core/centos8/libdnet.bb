SUMMARY = "generated recipe based on libdnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdnet = "libdnet.so.1"
RPM_SONAME_REQ_libdnet = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libdnet = "glibc"
RPM_SONAME_REQ_libdnet-devel = "libdnet.so.1"
RPROVIDES_libdnet-devel = "libdnet-dev (= 1.12)"
RDEPENDS_libdnet-devel = "bash libdnet"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdnet-1.12-26.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdnet-devel-1.12-26.el8.aarch64.rpm \
          "

SRC_URI[libdnet.sha256sum] = "51a098d982fe713bcdaf21868574023c9e85fe29cbe3db2f0371fd8484883de0"
SRC_URI[libdnet-devel.sha256sum] = "cddd59de089625cbb6e70f86c017ba7c42b27e4e09494e4e6e885e9c1ddec476"
