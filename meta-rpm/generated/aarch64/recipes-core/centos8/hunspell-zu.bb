SUMMARY = "generated recipe based on hunspell-zu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-zu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-zu-0.20100126-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-zu.sha256sum] = "9e6a99f880e3b0c806ddf3e36517c5646111d8f627a1e2903651f86abf318efd"
