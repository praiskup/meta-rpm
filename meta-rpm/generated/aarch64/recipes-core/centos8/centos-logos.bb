SUMMARY = "generated recipe based on centos-logos srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_centos-backgrounds = "centos-logos"
RDEPENDS_centos-logos = "bash coreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/centos-backgrounds-80.5-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/centos-logos-ipa-80.5-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/centos-logos-80.5-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/centos-logos-httpd-80.5-2.el8.noarch.rpm \
          "

SRC_URI[centos-backgrounds.sha256sum] = "f380577daa9c8e9d0930a0ae649c48b015056b238296d3be0763d02c4cf99ead"
SRC_URI[centos-logos.sha256sum] = "6cc505a9206b3c10c1ea3138d1ba41d6e5f74da2191a3a033f661f63cd3951b3"
SRC_URI[centos-logos-httpd.sha256sum] = "5717f6fb0a3d0a4cb327a19f5a9ac7114eb7fa251f969dfa386f729c4a37a3d1"
SRC_URI[centos-logos-ipa.sha256sum] = "22eaeb0c9401b53779a766b5044ea7f174fcf3fce41c706dc11aaa04f9fca24c"
