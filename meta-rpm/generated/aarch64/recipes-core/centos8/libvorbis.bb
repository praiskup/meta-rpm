SUMMARY = "generated recipe based on libvorbis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libogg pkgconfig-native"
RPM_SONAME_PROV_libvorbis = "libvorbis.so.0 libvorbisenc.so.2 libvorbisfile.so.3"
RPM_SONAME_REQ_libvorbis = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libogg.so.0 libvorbis.so.0"
RDEPENDS_libvorbis = "glibc libogg"
RPM_SONAME_REQ_libvorbis-devel = "libvorbis.so.0 libvorbisenc.so.2 libvorbisfile.so.3"
RPROVIDES_libvorbis-devel = "libvorbis-dev (= 1.3.6)"
RDEPENDS_libvorbis-devel = "libogg-devel libvorbis pkgconf-pkg-config"
RDEPENDS_libvorbis-devel-docs = "libvorbis-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvorbis-1.3.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvorbis-devel-1.3.6-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvorbis-devel-docs-1.3.6-2.el8.noarch.rpm \
          "

SRC_URI[libvorbis.sha256sum] = "0047d6ff3cba57ee7344b398c8e925e5f0b2090c384f1c662a8e441419accd25"
SRC_URI[libvorbis-devel.sha256sum] = "a4e1ae8eee83293b8500e7cde3ebe5cf5971d04e5289c9c87392d5cc3f86b6c8"
SRC_URI[libvorbis-devel-docs.sha256sum] = "06394fa449c5fa9d21e6e11b5231c40df7f9edb3cfe35d92873c7ecab9fad220"
