SUMMARY = "generated recipe based on vulkan-loader srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_vulkan-loader = "libvulkan.so.1"
RPM_SONAME_REQ_vulkan-loader = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_vulkan-loader = "glibc"
RPM_SONAME_REQ_vulkan-loader-devel = "libvulkan.so.1"
RPROVIDES_vulkan-loader-devel = "vulkan-loader-dev (= 1.2.135.0)"
RDEPENDS_vulkan-loader-devel = "pkgconf-pkg-config vulkan-headers vulkan-loader"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vulkan-loader-1.2.135.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vulkan-loader-devel-1.2.135.0-1.el8.aarch64.rpm \
          "

SRC_URI[vulkan-loader.sha256sum] = "e703b63bfc1f212dc18a1dfd83c6b924dac22793b4ee6e5381ec3505b9430022"
SRC_URI[vulkan-loader-devel.sha256sum] = "c1f422dfed8bb55d91cd3dac0f1c9e806260515bdc91579e3333ac42ec969656"
