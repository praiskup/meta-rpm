SUMMARY = "generated recipe based on libpinyin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 kyotocabinet libgcc pkgconfig-native"
RPM_SONAME_PROV_libpinyin = "libpinyin.so.13"
RPM_SONAME_REQ_libpinyin = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libkyotocabinet.so.16 libm.so.6 libstdc++.so.6"
RDEPENDS_libpinyin = "glib2 glibc kyotocabinet-libs libgcc libpinyin-data libstdc++"
RDEPENDS_libpinyin-data = "libpinyin"
RPM_SONAME_PROV_libzhuyin = "libzhuyin.so.13"
RPM_SONAME_REQ_libzhuyin = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libkyotocabinet.so.16 libm.so.6 libstdc++.so.6"
RDEPENDS_libzhuyin = "glib2 glibc kyotocabinet-libs libgcc libpinyin libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpinyin-2.2.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpinyin-data-2.2.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libzhuyin-2.2.0-1.el8.aarch64.rpm \
          "

SRC_URI[libpinyin.sha256sum] = "0700c6f2a054504b6008820d28e3299b642859c36d822704724f1f8edcb9639a"
SRC_URI[libpinyin-data.sha256sum] = "1c79f9e6972318e9dc025e1ca76cbc9c92c7b6f6c210a6f0f96d4e69e3219b40"
SRC_URI[libzhuyin.sha256sum] = "26ac463d81750fb42da5ca1819e974e7d0b616e2871fa313beebb9dae45541cd"
