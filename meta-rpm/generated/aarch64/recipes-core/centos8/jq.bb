SUMMARY = "generated recipe based on jq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "oniguruma pkgconfig-native"
RPM_SONAME_PROV_jq = "libjq.so.1"
RPM_SONAME_REQ_jq = "ld-linux-aarch64.so.1 libc.so.6 libjq.so.1 libm.so.6 libonig.so.5"
RDEPENDS_jq = "glibc oniguruma"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jq-1.5-12.el8.aarch64.rpm \
          "

SRC_URI[jq.sha256sum] = "a0d9785deff96688b1890dab35b7b5505142a38a2d0c88b362e4d3ebb71e2f14"
