SUMMARY = "generated recipe based on liblognorm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libestr libfastjson libpcre pkgconfig-native"
RPM_SONAME_PROV_liblognorm = "liblognorm.so.5"
RPM_SONAME_REQ_liblognorm = "ld-linux-aarch64.so.1 libc.so.6 libestr.so.0 libfastjson.so.4 libpcre.so.1"
RDEPENDS_liblognorm = "glibc libestr libfastjson pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liblognorm-2.0.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/liblognorm-doc-2.0.5-1.el8.aarch64.rpm \
          "

SRC_URI[liblognorm.sha256sum] = "0bbc78424e19a61cfdc955b5cf8a52302875bf4f9fb3f95496e814f6c9409871"
SRC_URI[liblognorm-doc.sha256sum] = "04a363fff9fea12e1c362f1076999685725180d7a1527dc0e1f498fe2783d2dd"
