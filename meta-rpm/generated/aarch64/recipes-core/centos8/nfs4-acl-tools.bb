SUMMARY = "generated recipe based on nfs4-acl-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr pkgconfig-native"
RPM_SONAME_REQ_nfs4-acl-tools = "ld-linux-aarch64.so.1 libattr.so.1 libc.so.6"
RDEPENDS_nfs4-acl-tools = "glibc libattr"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nfs4-acl-tools-0.3.5-3.el8.aarch64.rpm \
          "

SRC_URI[nfs4-acl-tools.sha256sum] = "f2a8658b23085c47ba4ff091f3e36f7ba46fc22b308217c46668f98e1d09b62c"
