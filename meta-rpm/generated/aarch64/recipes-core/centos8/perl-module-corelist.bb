SUMMARY = "generated recipe based on perl-Module-CoreList srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-CoreList = "perl-Scalar-List-Utils perl-libs perl-version"
RDEPENDS_perl-Module-CoreList-tools = "perl-Getopt-Long perl-Module-CoreList perl-Pod-Usage perl-Scalar-List-Utils perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Module-CoreList-5.20181130-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Module-CoreList-tools-5.20181130-1.el8.noarch.rpm \
          "

SRC_URI[perl-Module-CoreList.sha256sum] = "a427a7c589f03776b035b71714f80750465a3e48f23de3fec786fc503f904e65"
SRC_URI[perl-Module-CoreList-tools.sha256sum] = "b4d1c36d4dae3dccabfcdb58e25ffacf5668abb138aa1efac4e48c3fda31b100"
