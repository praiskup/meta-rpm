SUMMARY = "generated recipe based on rarian srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_rarian = "librarian.so.0"
RPM_SONAME_REQ_rarian = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librarian.so.0 libstdc++.so.6"
RDEPENDS_rarian = "coreutils gawk glibc libgcc libstdc++ libxml2 libxslt util-linux"
RPM_SONAME_REQ_rarian-compat = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librarian.so.0 libstdc++.so.6"
RDEPENDS_rarian-compat = "bash glibc libgcc libstdc++ rarian"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rarian-0.8.1-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rarian-compat-0.8.1-19.el8.aarch64.rpm \
          "

SRC_URI[rarian.sha256sum] = "998022c4d375c37d9b8247c3a465fa5d7e31e60b90c4a318d30ec8665cfa2736"
SRC_URI[rarian-compat.sha256sum] = "c9f83ae1589c29e1749e7c50fee02cf923f96470019ccc14d4d9b6eb78906d6a"
