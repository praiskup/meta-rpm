SUMMARY = "generated recipe based on gflags srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_gflags = "libgflags.so.2.1 libgflags_nothreads.so.2.1"
RPM_SONAME_REQ_gflags = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gflags = "bash glibc libgcc libstdc++"
RPM_SONAME_REQ_gflags-devel = "libgflags.so.2.1 libgflags_nothreads.so.2.1"
RPROVIDES_gflags-devel = "gflags-dev (= 2.1.2)"
RDEPENDS_gflags-devel = "cmake-filesystem gflags"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gflags-2.1.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gflags-devel-2.1.2-6.el8.aarch64.rpm \
          "

SRC_URI[gflags.sha256sum] = "839b38f90a564e3a5c4b30af9d3ae8720fad04583c8515f0e81912b24d6feb76"
SRC_URI[gflags-devel.sha256sum] = "b1cf81424201bac2bcd0776291874db451ef0ec7e1b0e94191d504177d8e98a3"
