SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-Bignum srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-Bignum = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Crypt-OpenSSL-Bignum = "glibc openssl-libs perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Crypt-OpenSSL-Bignum-0.09-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-Bignum.sha256sum] = "0efbfbc414cd225b6b1f33a69d79d779d89218a1cac0c1bd9ba1ca70984fd6e4"
