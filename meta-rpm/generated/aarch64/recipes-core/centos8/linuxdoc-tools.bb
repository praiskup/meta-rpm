SUMMARY = "generated recipe based on linuxdoc-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_linuxdoc-tools = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_linuxdoc-tools = "bash gawk glibc groff openjade perl-Exporter perl-File-Temp perl-PathTools perl-interpreter perl-libs texlive-collection-latexrecommended texlive-tetex"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/linuxdoc-tools-0.9.72-5.el8.aarch64.rpm \
          "

SRC_URI[linuxdoc-tools.sha256sum] = "8c6dffa4f03cd3c6cf73d05c438208593132fec3d9f847694fb0b7ab3a83453b"
