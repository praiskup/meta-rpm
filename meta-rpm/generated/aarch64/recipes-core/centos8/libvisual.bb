SUMMARY = "generated recipe based on libvisual srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libvisual = "libvisual-0.4.so.0"
RPM_SONAME_REQ_libvisual = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_libvisual = "glibc"
RPM_SONAME_REQ_libvisual-devel = "libvisual-0.4.so.0"
RPROVIDES_libvisual-devel = "libvisual-dev (= 0.4.0)"
RDEPENDS_libvisual-devel = "libvisual pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvisual-0.4.0-24.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvisual-devel-0.4.0-24.el8.aarch64.rpm \
          "

SRC_URI[libvisual.sha256sum] = "4a5d81e944ea763efa60cb633c888c8d88689520703c52b8569622e8bb4dcda4"
SRC_URI[libvisual-devel.sha256sum] = "0871aef0a2f6d454aea590931cc3cd0b24236b2f4af1d5c04bf76f1ae4dfc485"
