SUMMARY = "generated recipe based on libpng15 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libpng15 = "libpng15.so.15"
RPM_SONAME_REQ_libpng15 = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libz.so.1"
RDEPENDS_libpng15 = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpng15-1.5.30-7.el8.aarch64.rpm \
          "

SRC_URI[libpng15.sha256sum] = "1038004df503eb775620132b796facab67fd781205f5d06894991ec20ac2c04f"
