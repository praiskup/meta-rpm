SUMMARY = "generated recipe based on wsmancli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openwsman pkgconfig-native"
RPM_SONAME_REQ_wsmancli = "ld-linux-aarch64.so.1 libc.so.6 libwsman.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1"
RDEPENDS_wsmancli = "curl glibc libwsman1"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/wsmancli-2.6.0-8.el8.aarch64.rpm \
          "

SRC_URI[wsmancli.sha256sum] = "0260ec629c353a8a3981778111fcdb2a1e59659a9292dab99704c5af84795c37"
