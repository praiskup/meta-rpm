SUMMARY = "generated recipe based on potrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_potrace = "libpotrace.so.0"
RPM_SONAME_REQ_potrace = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpotrace.so.0 libz.so.1"
RDEPENDS_potrace = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/potrace-1.15-2.el8.aarch64.rpm \
          "

SRC_URI[potrace.sha256sum] = "09f6a338ff4a5347e5a7028d8f7a4b350c3a1245e6e4ce6de9595b6326855957"
