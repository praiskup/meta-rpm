SUMMARY = "generated recipe based on libiec61883 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libraw1394 pkgconfig-native"
RPM_SONAME_PROV_libiec61883 = "libiec61883.so.0"
RPM_SONAME_REQ_libiec61883 = "ld-linux-aarch64.so.1 libc.so.6 libraw1394.so.11"
RDEPENDS_libiec61883 = "glibc libraw1394"
RPM_SONAME_REQ_libiec61883-devel = "libiec61883.so.0"
RPROVIDES_libiec61883-devel = "libiec61883-dev (= 1.2.0)"
RDEPENDS_libiec61883-devel = "libiec61883 libraw1394-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libiec61883-1.2.0-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libiec61883-devel-1.2.0-18.el8.aarch64.rpm \
          "

SRC_URI[libiec61883.sha256sum] = "315851f5a3dc7e966c2aef1d69a1f885f27fd00206690e0c649f701430902111"
SRC_URI[libiec61883-devel.sha256sum] = "d4141adf7154ec5853bb0144201e835d7e3ef7f66f5440d9bd456880d407f2fb"
