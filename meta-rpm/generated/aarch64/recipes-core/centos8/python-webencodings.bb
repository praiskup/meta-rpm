SUMMARY = "generated recipe based on python-webencodings srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-webencodings = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-webencodings-0.5.1-6.el8.noarch.rpm \
          "

SRC_URI[python3-webencodings.sha256sum] = "6ee1c354e7a78accc7a1f7495f9ff401181d403e7dc30877cd0a95c5ebb5ef69"
