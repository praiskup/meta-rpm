SUMMARY = "generated recipe based on objectweb-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_objectweb-pom = "java-1.8.0-openjdk-headless javapackages-filesystem maven-enforcer-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/objectweb-pom-1.5-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[objectweb-pom.sha256sum] = "02f38dd1c609021ab5e774afbb4ba5cc2b1f308f333c2ffcbe2ce8eb19db1822"
