SUMMARY = "generated recipe based on ucx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc numactl pkgconfig-native"
RPM_SONAME_PROV_ucx = "libucm.so.0 libucp.so.0 libucs.so.0 libuct.so.0"
RPM_SONAME_REQ_ucx = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgomp.so.1 libm.so.6 libnuma.so.1 libpthread.so.0 librt.so.1 libucm.so.0 libucp.so.0 libucs.so.0 libuct.so.0"
RDEPENDS_ucx = "glibc libgcc libgomp numactl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ucx-1.6.1-1.el8.aarch64.rpm \
          "

SRC_URI[ucx.sha256sum] = "34ae130810b14bc6d847146f7bd5e664f055e906dd454e28ba91dc6660f2df6a"
