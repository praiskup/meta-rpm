SUMMARY = "generated recipe based on python-ldap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openldap pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-ldap = "ld-linux-aarch64.so.1 libc.so.6 liblber-2.4.so.2 libldap_r-2.4.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-ldap = "bash glibc openldap platform-python platform-python-setuptools python3-libs python3-pyasn1 python3-pyasn1-modules"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-ldap-3.1.0-5.el8.aarch64.rpm \
          "

SRC_URI[python3-ldap.sha256sum] = "ee50c581cd0207c8c9cf58873ba2a72d599b74d6020d31eb7d7b8823f59fc666"
