SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/libc virtual/libiconv virtual/libintl"
DEPENDS = "kernel-headers"
RPM_SONAME_PROV_glibc = "ld-linux-aarch64.so.1 libBrokenLocale.so.1 libCNS.so libGB.so libISOIR165.so libJIS.so libJISX0213.so libKSC.so libSegFault.so libanl.so.1 libc.so.6 libdl.so.2 libm.so.6 libmemusage.so libnss_compat.so.2 libnss_dns.so.2 libnss_files.so.2 libpcprofile.so libpthread.so.0 libresolv.so.2 librt.so.1 libthread_db.so.1 libutil.so.1"
RPM_SONAME_REQ_glibc = "ld-linux-aarch64.so.1 libCNS.so libGB.so libISOIR165.so libJIS.so libJISX0213.so libKSC.so libc.so.6 libdl.so.2 libpthread.so.0 libresolv.so.2"
RPROVIDES_glibc = "glibc-charmap-big5 (= 2.28) glibc-charmap-cp1251 (= 2.28) glibc-charmap-euc-jp (= 2.28) glibc-charmap-euc-kr (= 2.28) glibc-charmap-iso-8859-1 (= 2.28) glibc-charmap-iso-8859-2 (= 2.28) glibc-charmap-iso-8859-9 (= 2.28) glibc-charmap-koi8-r (= 2.28) glibc-gconv-ansi_x3.110 (= 2.28) glibc-gconv-armscii-8 (= 2.28) glibc-gconv-asmo_449 (= 2.28) glibc-gconv-big5 (= 2.28) glibc-gconv-big5hkscs (= 2.28) glibc-gconv-brf (= 2.28) glibc-gconv-cp10007 (= 2.28) glibc-gconv-cp1125 (= 2.28) glibc-gconv-cp1250 (= 2.28) glibc-gconv-cp1251 (= 2.28) glibc-gconv-cp1252 (= 2.28) glibc-gconv-cp1253 (= 2.28) glibc-gconv-cp1254 (= 2.28) glibc-gconv-cp1255 (= 2.28) glibc-gconv-cp1256 (= 2.28) glibc-gconv-cp1257 (= 2.28) glibc-gconv-cp1258 (= 2.28) glibc-gconv-cp737 (= 2.28) glibc-gconv-cp770 (= 2.28) glibc-gconv-cp771 (= 2.28) glibc-gconv-cp772 (= 2.28) glibc-gconv-cp773 (= 2.28) glibc-gconv-cp774 (= 2.28) glibc-gconv-cp775 (= 2.28) glibc-gconv-cp932 (= 2.28) glibc-gconv-csn_369103 (= 2.28) glibc-gconv-cwi (= 2.28) glibc-gconv-dec-mcs (= 2.28) glibc-gconv-ebcdic-at-de (= 2.28) glibc-gconv-ebcdic-at-de-a (= 2.28) glibc-gconv-ebcdic-ca-fr (= 2.28) glibc-gconv-ebcdic-dk-no (= 2.28) glibc-gconv-ebcdic-dk-no-a (= 2.28) glibc-gconv-ebcdic-es (= 2.28) glibc-gconv-ebcdic-es-a (= 2.28) glibc-gconv-ebcdic-es-s (= 2.28) glibc-gconv-ebcdic-fi-se (= 2.28) glibc-gconv-ebcdic-fi-se-a (= 2.28) glibc-gconv-ebcdic-fr (= 2.28) glibc-gconv-ebcdic-is-friss (= 2.28) glibc-gconv-ebcdic-it (= 2.28) glibc-gconv-ebcdic-pt (= 2.28) glibc-gconv-ebcdic-uk (= 2.28) glibc-gconv-ebcdic-us (= 2.28) glibc-gconv-ecma-cyrillic (= 2.28) glibc-gconv-euc-cn (= 2.28) glibc-gconv-euc-jisx0213 (= 2.28) glibc-gconv-euc-jp (= 2.28) glibc-gconv-euc-jp-ms (= 2.28) glibc-gconv-euc-kr (= 2.28) glibc-gconv-euc-tw (= 2.28) glibc-gconv-gb18030 (= 2.28) glibc-gconv-gbbig5 (= 2.28) glibc-gconv-gbgbk (= 2.28) glibc-gconv-gbk (= 2.28) glibc-gconv-georgian-academy (= 2.28) glibc-gconv-georgian-ps (= 2.28) glibc-gconv-gost_19768-74 (= 2.28) glibc-gconv-greek-ccitt (= 2.28) glibc-gconv-greek7 (= 2.28) glibc-gconv-greek7-old (= 2.28) glibc-gconv-hp-greek8 (= 2.28) glibc-gconv-hp-roman8 (= 2.28) glibc-gconv-hp-roman9 (= 2.28) glibc-gconv-hp-thai8 (= 2.28) glibc-gconv-hp-turkish8 (= 2.28) glibc-gconv-ibm037 (= 2.28) glibc-gconv-ibm038 (= 2.28) glibc-gconv-ibm1004 (= 2.28) glibc-gconv-ibm1008 (= 2.28) glibc-gconv-ibm1008_420 (= 2.28) glibc-gconv-ibm1025 (= 2.28) glibc-gconv-ibm1026 (= 2.28) glibc-gconv-ibm1046 (= 2.28) glibc-gconv-ibm1047 (= 2.28) glibc-gconv-ibm1097 (= 2.28) glibc-gconv-ibm1112 (= 2.28) glibc-gconv-ibm1122 (= 2.28) glibc-gconv-ibm1123 (= 2.28) glibc-gconv-ibm1124 (= 2.28) glibc-gconv-ibm1129 (= 2.28) glibc-gconv-ibm1130 (= 2.28) glibc-gconv-ibm1132 (= 2.28) glibc-gconv-ibm1133 (= 2.28) glibc-gconv-ibm1137 (= 2.28) glibc-gconv-ibm1140 (= 2.28) glibc-gconv-ibm1141 (= 2.28) glibc-gconv-ibm1142 (= 2.28) glibc-gconv-ibm1143 (= 2.28) glibc-gconv-ibm1144 (= 2.28) glibc-gconv-ibm1145 (= 2.28) glibc-gconv-ibm1146 (= 2.28) glibc-gconv-ibm1147 (= 2.28) glibc-gconv-ibm1148 (= 2.28) glibc-gconv-ibm1149 (= 2.28) glibc-gconv-ibm1153 (= 2.28) glibc-gconv-ibm1154 (= 2.28) glibc-gconv-ibm1155 (= 2.28) glibc-gconv-ibm1156 (= 2.28) glibc-gconv-ibm1157 (= 2.28) glibc-gconv-ibm1158 (= 2.28) glibc-gconv-ibm1160 (= 2.28) glibc-gconv-ibm1161 (= 2.28) glibc-gconv-ibm1162 (= 2.28) glibc-gconv-ibm1163 (= 2.28) glibc-gconv-ibm1164 (= 2.28) glibc-gconv-ibm1166 (= 2.28) glibc-gconv-ibm1167 (= 2.28) glibc-gconv-ibm12712 (= 2.28) glibc-gconv-ibm1364 (= 2.28) glibc-gconv-ibm1371 (= 2.28) glibc-gconv-ibm1388 (= 2.28) glibc-gconv-ibm1390 (= 2.28) glibc-gconv-ibm1399 (= 2.28) glibc-gconv-ibm16804 (= 2.28) glibc-gconv-ibm256 (= 2.28) glibc-gconv-ibm273 (= 2.28) glibc-gconv-ibm274 (= 2.28) glibc-gconv-ibm275 (= 2.28) glibc-gconv-ibm277 (= 2.28) glibc-gconv-ibm278 (= 2.28) glibc-gconv-ibm280 (= 2.28) glibc-gconv-ibm281 (= 2.28) glibc-gconv-ibm284 (= 2.28) glibc-gconv-ibm285 (= 2.28) glibc-gconv-ibm290 (= 2.28) glibc-gconv-ibm297 (= 2.28) glibc-gconv-ibm420 (= 2.28) glibc-gconv-ibm423 (= 2.28) glibc-gconv-ibm424 (= 2.28) glibc-gconv-ibm437 (= 2.28) glibc-gconv-ibm4517 (= 2.28) glibc-gconv-ibm4899 (= 2.28) glibc-gconv-ibm4909 (= 2.28) glibc-gconv-ibm4971 (= 2.28) glibc-gconv-ibm500 (= 2.28) glibc-gconv-ibm5347 (= 2.28) glibc-gconv-ibm803 (= 2.28) glibc-gconv-ibm850 (= 2.28) glibc-gconv-ibm851 (= 2.28) glibc-gconv-ibm852 (= 2.28) glibc-gconv-ibm855 (= 2.28) glibc-gconv-ibm856 (= 2.28) glibc-gconv-ibm857 (= 2.28) glibc-gconv-ibm858 (= 2.28) glibc-gconv-ibm860 (= 2.28) glibc-gconv-ibm861 (= 2.28) glibc-gconv-ibm862 (= 2.28) glibc-gconv-ibm863 (= 2.28) glibc-gconv-ibm864 (= 2.28) glibc-gconv-ibm865 (= 2.28) glibc-gconv-ibm866 (= 2.28) glibc-gconv-ibm866nav (= 2.28) glibc-gconv-ibm868 (= 2.28) glibc-gconv-ibm869 (= 2.28) glibc-gconv-ibm870 (= 2.28) glibc-gconv-ibm871 (= 2.28) glibc-gconv-ibm874 (= 2.28) glibc-gconv-ibm875 (= 2.28) glibc-gconv-ibm880 (= 2.28) glibc-gconv-ibm891 (= 2.28) glibc-gconv-ibm901 (= 2.28) glibc-gconv-ibm902 (= 2.28) glibc-gconv-ibm903 (= 2.28) glibc-gconv-ibm9030 (= 2.28) glibc-gconv-ibm904 (= 2.28) glibc-gconv-ibm905 (= 2.28) glibc-gconv-ibm9066 (= 2.28) glibc-gconv-ibm918 (= 2.28) glibc-gconv-ibm921 (= 2.28) glibc-gconv-ibm922 (= 2.28) glibc-gconv-ibm930 (= 2.28) glibc-gconv-ibm932 (= 2.28) glibc-gconv-ibm933 (= 2.28) glibc-gconv-ibm935 (= 2.28) glibc-gconv-ibm937 (= 2.28) glibc-gconv-ibm939 (= 2.28) glibc-gconv-ibm943 (= 2.28) glibc-gconv-ibm9448 (= 2.28) glibc-gconv-iec_p27-1 (= 2.28) glibc-gconv-inis (= 2.28) glibc-gconv-inis-8 (= 2.28) glibc-gconv-inis-cyrillic (= 2.28) glibc-gconv-isiri-3342 (= 2.28) glibc-gconv-iso-2022-cn (= 2.28) glibc-gconv-iso-2022-cn-ext (= 2.28) glibc-gconv-iso-2022-jp (= 2.28) glibc-gconv-iso-2022-jp-3 (= 2.28) glibc-gconv-iso-2022-kr (= 2.28) glibc-gconv-iso-ir-197 (= 2.28) glibc-gconv-iso-ir-209 (= 2.28) glibc-gconv-iso646 (= 2.28) glibc-gconv-iso8859-1 (= 2.28) glibc-gconv-iso8859-10 (= 2.28) glibc-gconv-iso8859-11 (= 2.28) glibc-gconv-iso8859-13 (= 2.28) glibc-gconv-iso8859-14 (= 2.28) glibc-gconv-iso8859-15 (= 2.28) glibc-gconv-iso8859-16 (= 2.28) glibc-gconv-iso8859-2 (= 2.28) glibc-gconv-iso8859-3 (= 2.28) glibc-gconv-iso8859-4 (= 2.28) glibc-gconv-iso8859-5 (= 2.28) glibc-gconv-iso8859-6 (= 2.28) glibc-gconv-iso8859-7 (= 2.28) glibc-gconv-iso8859-8 (= 2.28) glibc-gconv-iso8859-9 (= 2.28) glibc-gconv-iso8859-9e (= 2.28) glibc-gconv-iso_10367-box (= 2.28) glibc-gconv-iso_11548-1 (= 2.28) glibc-gconv-iso_2033 (= 2.28) glibc-gconv-iso_5427 (= 2.28) glibc-gconv-iso_5427-ext (= 2.28) glibc-gconv-iso_5428 (= 2.28) glibc-gconv-iso_6937 (= 2.28) glibc-gconv-iso_6937-2 (= 2.28) glibc-gconv-johab (= 2.28) glibc-gconv-koi-8 (= 2.28) glibc-gconv-koi8-r (= 2.28) glibc-gconv-koi8-ru (= 2.28) glibc-gconv-koi8-t (= 2.28) glibc-gconv-koi8-u (= 2.28) glibc-gconv-latin-greek (= 2.28) glibc-gconv-latin-greek-1 (= 2.28) glibc-gconv-mac-centraleurope (= 2.28) glibc-gconv-mac-is (= 2.28) glibc-gconv-mac-sami (= 2.28) glibc-gconv-mac-uk (= 2.28) glibc-gconv-macintosh (= 2.28) glibc-gconv-mik (= 2.28) glibc-gconv-nats-dano (= 2.28) glibc-gconv-nats-sefi (= 2.28) glibc-gconv-pt154 (= 2.28) glibc-gconv-rk1048 (= 2.28) glibc-gconv-sami-ws2 (= 2.28) glibc-gconv-shift_jisx0213 (= 2.28) glibc-gconv-sjis (= 2.28) glibc-gconv-t.61 (= 2.28) glibc-gconv-tcvn5712-1 (= 2.28) glibc-gconv-tis-620 (= 2.28) glibc-gconv-tscii (= 2.28) glibc-gconv-uhc (= 2.28) glibc-gconv-unicode (= 2.28) glibc-gconv-utf-16 (= 2.28) glibc-gconv-utf-32 (= 2.28) glibc-gconv-utf-7 (= 2.28) glibc-gconv-viscii (= 2.28) ldconfig (= 2.28)"
RDEPENDS_glibc = "basesystem glibc-common glibc-minimal-langpack"
RDEPENDS_glibc-all-langpacks = "glibc glibc-common"
RPM_SONAME_REQ_glibc-common = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libselinux.so.1"
RDEPENDS_glibc-common = "bash glibc libselinux tzdata"
RPM_SONAME_REQ_glibc-devel = "libBrokenLocale.so.1 libanl.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libresolv.so.2 librt.so.1 libthread_db.so.1 libutil.so.1"
RPROVIDES_glibc-devel = "glibc-dev (= 2.28)"
RDEPENDS_glibc-devel = "bash glibc glibc-headers info libgcc libxcrypt-devel"
RDEPENDS_glibc-headers = "bash glibc kernel-headers"
RPROVIDES_glibc-langpack-aa = "locale-base-aa-dj (= 2.28) locale-base-aa-dj.utf8 (= 2.28) locale-base-aa-er (= 2.28) locale-base-aa-er@saaho (= 2.28) locale-base-aa-et (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa-dj (= 2.28) virtual-locale-aa-dj.utf8 (= 2.28) virtual-locale-aa-er (= 2.28) virtual-locale-aa-er@saaho (= 2.28) virtual-locale-aa-et (= 2.28)"
RDEPENDS_glibc-langpack-aa = "glibc glibc-common"
RPROVIDES_glibc-langpack-af = "locale-base-af-za (= 2.28) locale-base-af-za.utf8 (= 2.28) virtual-locale-af (= 2.28) virtual-locale-af (= 2.28) virtual-locale-af-za (= 2.28) virtual-locale-af-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-af = "glibc glibc-common"
RPROVIDES_glibc-langpack-agr = "locale-base-agr-pe (= 2.28) virtual-locale-agr (= 2.28) virtual-locale-agr-pe (= 2.28)"
RDEPENDS_glibc-langpack-agr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ak = "locale-base-ak-gh (= 2.28) virtual-locale-ak (= 2.28) virtual-locale-ak-gh (= 2.28)"
RDEPENDS_glibc-langpack-ak = "glibc glibc-common"
RPROVIDES_glibc-langpack-am = "locale-base-am-et (= 2.28) virtual-locale-am (= 2.28) virtual-locale-am-et (= 2.28)"
RDEPENDS_glibc-langpack-am = "glibc glibc-common"
RPROVIDES_glibc-langpack-an = "locale-base-an-es (= 2.28) locale-base-an-es.utf8 (= 2.28) virtual-locale-an (= 2.28) virtual-locale-an (= 2.28) virtual-locale-an-es (= 2.28) virtual-locale-an-es.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-an = "glibc glibc-common"
RPROVIDES_glibc-langpack-anp = "locale-base-anp-in (= 2.28) virtual-locale-anp (= 2.28) virtual-locale-anp-in (= 2.28)"
RDEPENDS_glibc-langpack-anp = "glibc glibc-common"
RPROVIDES_glibc-langpack-ar = "locale-base-ar-ae (= 2.28) locale-base-ar-ae.utf8 (= 2.28) locale-base-ar-bh (= 2.28) locale-base-ar-bh.utf8 (= 2.28) locale-base-ar-dz (= 2.28) locale-base-ar-dz.utf8 (= 2.28) locale-base-ar-eg (= 2.28) locale-base-ar-eg.utf8 (= 2.28) locale-base-ar-in (= 2.28) locale-base-ar-iq (= 2.28) locale-base-ar-iq.utf8 (= 2.28) locale-base-ar-jo (= 2.28) locale-base-ar-jo.utf8 (= 2.28) locale-base-ar-kw (= 2.28) locale-base-ar-kw.utf8 (= 2.28) locale-base-ar-lb (= 2.28) locale-base-ar-lb.utf8 (= 2.28) locale-base-ar-ly (= 2.28) locale-base-ar-ly.utf8 (= 2.28) locale-base-ar-ma (= 2.28) locale-base-ar-ma.utf8 (= 2.28) locale-base-ar-om (= 2.28) locale-base-ar-om.utf8 (= 2.28) locale-base-ar-qa (= 2.28) locale-base-ar-qa.utf8 (= 2.28) locale-base-ar-sa (= 2.28) locale-base-ar-sa.utf8 (= 2.28) locale-base-ar-sd (= 2.28) locale-base-ar-sd.utf8 (= 2.28) locale-base-ar-ss (= 2.28) locale-base-ar-sy (= 2.28) locale-base-ar-sy.utf8 (= 2.28) locale-base-ar-tn (= 2.28) locale-base-ar-tn.utf8 (= 2.28) locale-base-ar-ye (= 2.28) locale-base-ar-ye.utf8 (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar-ae (= 2.28) virtual-locale-ar-ae.utf8 (= 2.28) virtual-locale-ar-bh (= 2.28) virtual-locale-ar-bh.utf8 (= 2.28) virtual-locale-ar-dz (= 2.28) virtual-locale-ar-dz.utf8 (= 2.28) virtual-locale-ar-eg (= 2.28) virtual-locale-ar-eg.utf8 (= 2.28) virtual-locale-ar-in (= 2.28) virtual-locale-ar-iq (= 2.28) virtual-locale-ar-iq.utf8 (= 2.28) virtual-locale-ar-jo (= 2.28) virtual-locale-ar-jo.utf8 (= 2.28) virtual-locale-ar-kw (= 2.28) virtual-locale-ar-kw.utf8 (= 2.28) virtual-locale-ar-lb (= 2.28) virtual-locale-ar-lb.utf8 (= 2.28) virtual-locale-ar-ly (= 2.28) virtual-locale-ar-ly.utf8 (= 2.28) virtual-locale-ar-ma (= 2.28) virtual-locale-ar-ma.utf8 (= 2.28) virtual-locale-ar-om (= 2.28) virtual-locale-ar-om.utf8 (= 2.28) virtual-locale-ar-qa (= 2.28) virtual-locale-ar-qa.utf8 (= 2.28) virtual-locale-ar-sa (= 2.28) virtual-locale-ar-sa.utf8 (= 2.28) virtual-locale-ar-sd (= 2.28) virtual-locale-ar-sd.utf8 (= 2.28) virtual-locale-ar-ss (= 2.28) virtual-locale-ar-sy (= 2.28) virtual-locale-ar-sy.utf8 (= 2.28) virtual-locale-ar-tn (= 2.28) virtual-locale-ar-tn.utf8 (= 2.28) virtual-locale-ar-ye (= 2.28) virtual-locale-ar-ye.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ar = "glibc glibc-common"
RPROVIDES_glibc-langpack-as = "locale-base-as-in (= 2.28) virtual-locale-as (= 2.28) virtual-locale-as-in (= 2.28)"
RDEPENDS_glibc-langpack-as = "glibc glibc-common"
RPROVIDES_glibc-langpack-ast = "locale-base-ast-es (= 2.28) locale-base-ast-es.utf8 (= 2.28) virtual-locale-ast (= 2.28) virtual-locale-ast (= 2.28) virtual-locale-ast-es (= 2.28) virtual-locale-ast-es.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ast = "glibc glibc-common"
RPROVIDES_glibc-langpack-ayc = "locale-base-ayc-pe (= 2.28) virtual-locale-ayc (= 2.28) virtual-locale-ayc-pe (= 2.28)"
RDEPENDS_glibc-langpack-ayc = "glibc glibc-common"
RPROVIDES_glibc-langpack-az = "locale-base-az-az (= 2.28) locale-base-az-ir (= 2.28) virtual-locale-az (= 2.28) virtual-locale-az (= 2.28) virtual-locale-az-az (= 2.28) virtual-locale-az-ir (= 2.28)"
RDEPENDS_glibc-langpack-az = "glibc glibc-common"
RPROVIDES_glibc-langpack-be = "locale-base-be-by (= 2.28) locale-base-be-by.utf8 (= 2.28) locale-base-be-by@latin (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be-by (= 2.28) virtual-locale-be-by.utf8 (= 2.28) virtual-locale-be-by@latin (= 2.28)"
RDEPENDS_glibc-langpack-be = "glibc glibc-common"
RPROVIDES_glibc-langpack-bem = "locale-base-bem-zm (= 2.28) virtual-locale-bem (= 2.28) virtual-locale-bem-zm (= 2.28)"
RDEPENDS_glibc-langpack-bem = "glibc glibc-common"
RPROVIDES_glibc-langpack-ber = "locale-base-ber-dz (= 2.28) locale-base-ber-ma (= 2.28) virtual-locale-ber (= 2.28) virtual-locale-ber (= 2.28) virtual-locale-ber-dz (= 2.28) virtual-locale-ber-ma (= 2.28)"
RDEPENDS_glibc-langpack-ber = "glibc glibc-common"
RPROVIDES_glibc-langpack-bg = "locale-base-bg-bg (= 2.28) locale-base-bg-bg.utf8 (= 2.28) virtual-locale-bg (= 2.28) virtual-locale-bg (= 2.28) virtual-locale-bg-bg (= 2.28) virtual-locale-bg-bg.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bg = "glibc glibc-common"
RPROVIDES_glibc-langpack-bhb = "locale-base-bhb-in.utf8 (= 2.28) virtual-locale-bhb (= 2.28) virtual-locale-bhb-in.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bhb = "glibc glibc-common"
RPROVIDES_glibc-langpack-bho = "locale-base-bho-in (= 2.28) locale-base-bho-np (= 2.28) virtual-locale-bho (= 2.28) virtual-locale-bho (= 2.28) virtual-locale-bho-in (= 2.28) virtual-locale-bho-np (= 2.28)"
RDEPENDS_glibc-langpack-bho = "glibc glibc-common"
RPROVIDES_glibc-langpack-bi = "locale-base-bi-vu (= 2.28) virtual-locale-bi (= 2.28) virtual-locale-bi-vu (= 2.28)"
RDEPENDS_glibc-langpack-bi = "glibc glibc-common"
RPROVIDES_glibc-langpack-bn = "locale-base-bn-bd (= 2.28) locale-base-bn-in (= 2.28) virtual-locale-bn (= 2.28) virtual-locale-bn (= 2.28) virtual-locale-bn-bd (= 2.28) virtual-locale-bn-in (= 2.28)"
RDEPENDS_glibc-langpack-bn = "glibc glibc-common"
RPROVIDES_glibc-langpack-bo = "locale-base-bo-cn (= 2.28) locale-base-bo-in (= 2.28) virtual-locale-bo (= 2.28) virtual-locale-bo (= 2.28) virtual-locale-bo-cn (= 2.28) virtual-locale-bo-in (= 2.28)"
RDEPENDS_glibc-langpack-bo = "glibc glibc-common"
RPROVIDES_glibc-langpack-br = "locale-base-br-fr (= 2.28) locale-base-br-fr.utf8 (= 2.28) locale-base-br-fr@euro (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br-fr (= 2.28) virtual-locale-br-fr.utf8 (= 2.28) virtual-locale-br-fr@euro (= 2.28)"
RDEPENDS_glibc-langpack-br = "glibc glibc-common"
RPROVIDES_glibc-langpack-brx = "locale-base-brx-in (= 2.28) virtual-locale-brx (= 2.28) virtual-locale-brx-in (= 2.28)"
RDEPENDS_glibc-langpack-brx = "glibc glibc-common"
RPROVIDES_glibc-langpack-bs = "locale-base-bs-ba (= 2.28) locale-base-bs-ba.utf8 (= 2.28) virtual-locale-bs (= 2.28) virtual-locale-bs (= 2.28) virtual-locale-bs-ba (= 2.28) virtual-locale-bs-ba.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bs = "glibc glibc-common"
RPROVIDES_glibc-langpack-byn = "locale-base-byn-er (= 2.28) virtual-locale-byn (= 2.28) virtual-locale-byn-er (= 2.28)"
RDEPENDS_glibc-langpack-byn = "glibc glibc-common"
RPROVIDES_glibc-langpack-ca = "locale-base-ca-ad (= 2.28) locale-base-ca-ad.utf8 (= 2.28) locale-base-ca-es (= 2.28) locale-base-ca-es.utf8 (= 2.28) locale-base-ca-es@euro (= 2.28) locale-base-ca-es@valencia (= 2.28) locale-base-ca-fr (= 2.28) locale-base-ca-fr.utf8 (= 2.28) locale-base-ca-it (= 2.28) locale-base-ca-it.utf8 (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca-ad (= 2.28) virtual-locale-ca-ad.utf8 (= 2.28) virtual-locale-ca-es (= 2.28) virtual-locale-ca-es.utf8 (= 2.28) virtual-locale-ca-es@euro (= 2.28) virtual-locale-ca-es@valencia (= 2.28) virtual-locale-ca-fr (= 2.28) virtual-locale-ca-fr.utf8 (= 2.28) virtual-locale-ca-it (= 2.28) virtual-locale-ca-it.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ca = "glibc glibc-common"
RPROVIDES_glibc-langpack-ce = "locale-base-ce-ru (= 2.28) virtual-locale-ce (= 2.28) virtual-locale-ce-ru (= 2.28)"
RDEPENDS_glibc-langpack-ce = "glibc glibc-common"
RPROVIDES_glibc-langpack-chr = "locale-base-chr-us (= 2.28) virtual-locale-chr (= 2.28) virtual-locale-chr-us (= 2.28)"
RDEPENDS_glibc-langpack-chr = "glibc glibc-common"
RPROVIDES_glibc-langpack-cmn = "locale-base-cmn-tw (= 2.28) virtual-locale-cmn (= 2.28) virtual-locale-cmn-tw (= 2.28)"
RDEPENDS_glibc-langpack-cmn = "glibc glibc-common"
RPROVIDES_glibc-langpack-crh = "locale-base-crh-ua (= 2.28) virtual-locale-crh (= 2.28) virtual-locale-crh-ua (= 2.28)"
RDEPENDS_glibc-langpack-crh = "glibc glibc-common"
RPROVIDES_glibc-langpack-cs = "locale-base-cs-cz (= 2.28) locale-base-cs-cz.utf8 (= 2.28) virtual-locale-cs (= 2.28) virtual-locale-cs (= 2.28) virtual-locale-cs-cz (= 2.28) virtual-locale-cs-cz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-cs = "glibc glibc-common"
RPROVIDES_glibc-langpack-csb = "locale-base-csb-pl (= 2.28) virtual-locale-csb (= 2.28) virtual-locale-csb-pl (= 2.28)"
RDEPENDS_glibc-langpack-csb = "glibc glibc-common"
RPROVIDES_glibc-langpack-cv = "locale-base-cv-ru (= 2.28) virtual-locale-cv (= 2.28) virtual-locale-cv-ru (= 2.28)"
RDEPENDS_glibc-langpack-cv = "glibc glibc-common"
RPROVIDES_glibc-langpack-cy = "locale-base-cy-gb (= 2.28) locale-base-cy-gb.utf8 (= 2.28) virtual-locale-cy (= 2.28) virtual-locale-cy (= 2.28) virtual-locale-cy-gb (= 2.28) virtual-locale-cy-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-cy = "glibc glibc-common"
RPROVIDES_glibc-langpack-da = "locale-base-da-dk (= 2.28) locale-base-da-dk.iso885915 (= 2.28) locale-base-da-dk.utf8 (= 2.28) virtual-locale-da (= 2.28) virtual-locale-da (= 2.28) virtual-locale-da (= 2.28) virtual-locale-da-dk (= 2.28) virtual-locale-da-dk.iso885915 (= 2.28) virtual-locale-da-dk.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-da = "glibc glibc-common"
RPROVIDES_glibc-langpack-de = "locale-base-de-at (= 2.28) locale-base-de-at.utf8 (= 2.28) locale-base-de-at@euro (= 2.28) locale-base-de-be (= 2.28) locale-base-de-be.utf8 (= 2.28) locale-base-de-be@euro (= 2.28) locale-base-de-ch (= 2.28) locale-base-de-ch.utf8 (= 2.28) locale-base-de-de (= 2.28) locale-base-de-de.utf8 (= 2.28) locale-base-de-de@euro (= 2.28) locale-base-de-it (= 2.28) locale-base-de-it.utf8 (= 2.28) locale-base-de-li.utf8 (= 2.28) locale-base-de-lu (= 2.28) locale-base-de-lu.utf8 (= 2.28) locale-base-de-lu@euro (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de-at (= 2.28) virtual-locale-de-at.utf8 (= 2.28) virtual-locale-de-at@euro (= 2.28) virtual-locale-de-be (= 2.28) virtual-locale-de-be.utf8 (= 2.28) virtual-locale-de-be@euro (= 2.28) virtual-locale-de-ch (= 2.28) virtual-locale-de-ch.utf8 (= 2.28) virtual-locale-de-de (= 2.28) virtual-locale-de-de.utf8 (= 2.28) virtual-locale-de-de@euro (= 2.28) virtual-locale-de-it (= 2.28) virtual-locale-de-it.utf8 (= 2.28) virtual-locale-de-li.utf8 (= 2.28) virtual-locale-de-lu (= 2.28) virtual-locale-de-lu.utf8 (= 2.28) virtual-locale-de-lu@euro (= 2.28)"
RDEPENDS_glibc-langpack-de = "glibc glibc-common"
RPROVIDES_glibc-langpack-doi = "locale-base-doi-in (= 2.28) virtual-locale-doi (= 2.28) virtual-locale-doi-in (= 2.28)"
RDEPENDS_glibc-langpack-doi = "glibc glibc-common"
RPROVIDES_glibc-langpack-dsb = "locale-base-dsb-de (= 2.28) virtual-locale-dsb (= 2.28) virtual-locale-dsb-de (= 2.28)"
RDEPENDS_glibc-langpack-dsb = "glibc glibc-common"
RPROVIDES_glibc-langpack-dv = "locale-base-dv-mv (= 2.28) virtual-locale-dv (= 2.28) virtual-locale-dv-mv (= 2.28)"
RDEPENDS_glibc-langpack-dv = "glibc glibc-common"
RPROVIDES_glibc-langpack-dz = "locale-base-dz-bt (= 2.28) virtual-locale-dz (= 2.28) virtual-locale-dz-bt (= 2.28)"
RDEPENDS_glibc-langpack-dz = "glibc glibc-common"
RPROVIDES_glibc-langpack-el = "locale-base-el-cy (= 2.28) locale-base-el-cy.utf8 (= 2.28) locale-base-el-gr (= 2.28) locale-base-el-gr.utf8 (= 2.28) locale-base-el-gr@euro (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el-cy (= 2.28) virtual-locale-el-cy.utf8 (= 2.28) virtual-locale-el-gr (= 2.28) virtual-locale-el-gr.utf8 (= 2.28) virtual-locale-el-gr@euro (= 2.28)"
RDEPENDS_glibc-langpack-el = "glibc glibc-common"
RPROVIDES_glibc-langpack-en = "locale-base-en-ag (= 2.28) locale-base-en-au (= 2.28) locale-base-en-au.utf8 (= 2.28) locale-base-en-bw (= 2.28) locale-base-en-bw.utf8 (= 2.28) locale-base-en-ca (= 2.28) locale-base-en-ca.utf8 (= 2.28) locale-base-en-dk (= 2.28) locale-base-en-dk.utf8 (= 2.28) locale-base-en-gb (= 2.28) locale-base-en-gb.iso885915 (= 2.28) locale-base-en-gb.utf8 (= 2.28) locale-base-en-hk (= 2.28) locale-base-en-hk.utf8 (= 2.28) locale-base-en-ie (= 2.28) locale-base-en-ie.utf8 (= 2.28) locale-base-en-ie@euro (= 2.28) locale-base-en-il (= 2.28) locale-base-en-in (= 2.28) locale-base-en-ng (= 2.28) locale-base-en-nz (= 2.28) locale-base-en-nz.utf8 (= 2.28) locale-base-en-ph (= 2.28) locale-base-en-ph.utf8 (= 2.28) locale-base-en-sc.utf8 (= 2.28) locale-base-en-sg (= 2.28) locale-base-en-sg.utf8 (= 2.28) locale-base-en-us (= 2.28) locale-base-en-us.iso885915 (= 2.28) locale-base-en-us.utf8 (= 2.28) locale-base-en-za (= 2.28) locale-base-en-za.utf8 (= 2.28) locale-base-en-zm (= 2.28) locale-base-en-zw (= 2.28) locale-base-en-zw.utf8 (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en-ag (= 2.28) virtual-locale-en-au (= 2.28) virtual-locale-en-au.utf8 (= 2.28) virtual-locale-en-bw (= 2.28) virtual-locale-en-bw.utf8 (= 2.28) virtual-locale-en-ca (= 2.28) virtual-locale-en-ca.utf8 (= 2.28) virtual-locale-en-dk (= 2.28) virtual-locale-en-dk.utf8 (= 2.28) virtual-locale-en-gb (= 2.28) virtual-locale-en-gb.iso885915 (= 2.28) virtual-locale-en-gb.utf8 (= 2.28) virtual-locale-en-hk (= 2.28) virtual-locale-en-hk.utf8 (= 2.28) virtual-locale-en-ie (= 2.28) virtual-locale-en-ie.utf8 (= 2.28) virtual-locale-en-ie@euro (= 2.28) virtual-locale-en-il (= 2.28) virtual-locale-en-in (= 2.28) virtual-locale-en-ng (= 2.28) virtual-locale-en-nz (= 2.28) virtual-locale-en-nz.utf8 (= 2.28) virtual-locale-en-ph (= 2.28) virtual-locale-en-ph.utf8 (= 2.28) virtual-locale-en-sc.utf8 (= 2.28) virtual-locale-en-sg (= 2.28) virtual-locale-en-sg.utf8 (= 2.28) virtual-locale-en-us (= 2.28) virtual-locale-en-us.iso885915 (= 2.28) virtual-locale-en-us.utf8 (= 2.28) virtual-locale-en-za (= 2.28) virtual-locale-en-za.utf8 (= 2.28) virtual-locale-en-zm (= 2.28) virtual-locale-en-zw (= 2.28) virtual-locale-en-zw.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-en = "glibc glibc-common"
RDEPENDS_glibc-langpack-eo = "glibc glibc-common"
RPROVIDES_glibc-langpack-es = "locale-base-es-ar (= 2.28) locale-base-es-ar.utf8 (= 2.28) locale-base-es-bo (= 2.28) locale-base-es-bo.utf8 (= 2.28) locale-base-es-cl (= 2.28) locale-base-es-cl.utf8 (= 2.28) locale-base-es-co (= 2.28) locale-base-es-co.utf8 (= 2.28) locale-base-es-cr (= 2.28) locale-base-es-cr.utf8 (= 2.28) locale-base-es-cu (= 2.28) locale-base-es-do (= 2.28) locale-base-es-do.utf8 (= 2.28) locale-base-es-ec (= 2.28) locale-base-es-ec.utf8 (= 2.28) locale-base-es-es (= 2.28) locale-base-es-es.utf8 (= 2.28) locale-base-es-es@euro (= 2.28) locale-base-es-gt (= 2.28) locale-base-es-gt.utf8 (= 2.28) locale-base-es-hn (= 2.28) locale-base-es-hn.utf8 (= 2.28) locale-base-es-mx (= 2.28) locale-base-es-mx.utf8 (= 2.28) locale-base-es-ni (= 2.28) locale-base-es-ni.utf8 (= 2.28) locale-base-es-pa (= 2.28) locale-base-es-pa.utf8 (= 2.28) locale-base-es-pe (= 2.28) locale-base-es-pe.utf8 (= 2.28) locale-base-es-pr (= 2.28) locale-base-es-pr.utf8 (= 2.28) locale-base-es-py (= 2.28) locale-base-es-py.utf8 (= 2.28) locale-base-es-sv (= 2.28) locale-base-es-sv.utf8 (= 2.28) locale-base-es-us (= 2.28) locale-base-es-us.utf8 (= 2.28) locale-base-es-uy (= 2.28) locale-base-es-uy.utf8 (= 2.28) locale-base-es-ve (= 2.28) locale-base-es-ve.utf8 (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es-ar (= 2.28) virtual-locale-es-ar.utf8 (= 2.28) virtual-locale-es-bo (= 2.28) virtual-locale-es-bo.utf8 (= 2.28) virtual-locale-es-cl (= 2.28) virtual-locale-es-cl.utf8 (= 2.28) virtual-locale-es-co (= 2.28) virtual-locale-es-co.utf8 (= 2.28) virtual-locale-es-cr (= 2.28) virtual-locale-es-cr.utf8 (= 2.28) virtual-locale-es-cu (= 2.28) virtual-locale-es-do (= 2.28) virtual-locale-es-do.utf8 (= 2.28) virtual-locale-es-ec (= 2.28) virtual-locale-es-ec.utf8 (= 2.28) virtual-locale-es-es (= 2.28) virtual-locale-es-es.utf8 (= 2.28) virtual-locale-es-es@euro (= 2.28) virtual-locale-es-gt (= 2.28) virtual-locale-es-gt.utf8 (= 2.28) virtual-locale-es-hn (= 2.28) virtual-locale-es-hn.utf8 (= 2.28) virtual-locale-es-mx (= 2.28) virtual-locale-es-mx.utf8 (= 2.28) virtual-locale-es-ni (= 2.28) virtual-locale-es-ni.utf8 (= 2.28) virtual-locale-es-pa (= 2.28) virtual-locale-es-pa.utf8 (= 2.28) virtual-locale-es-pe (= 2.28) virtual-locale-es-pe.utf8 (= 2.28) virtual-locale-es-pr (= 2.28) virtual-locale-es-pr.utf8 (= 2.28) virtual-locale-es-py (= 2.28) virtual-locale-es-py.utf8 (= 2.28) virtual-locale-es-sv (= 2.28) virtual-locale-es-sv.utf8 (= 2.28) virtual-locale-es-us (= 2.28) virtual-locale-es-us.utf8 (= 2.28) virtual-locale-es-uy (= 2.28) virtual-locale-es-uy.utf8 (= 2.28) virtual-locale-es-ve (= 2.28) virtual-locale-es-ve.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-es = "glibc glibc-common"
RPROVIDES_glibc-langpack-et = "locale-base-et-ee (= 2.28) locale-base-et-ee.iso885915 (= 2.28) locale-base-et-ee.utf8 (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et-ee (= 2.28) virtual-locale-et-ee.iso885915 (= 2.28) virtual-locale-et-ee.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-et = "glibc glibc-common"
RPROVIDES_glibc-langpack-eu = "locale-base-eu-es (= 2.28) locale-base-eu-es.utf8 (= 2.28) locale-base-eu-es@euro (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu-es (= 2.28) virtual-locale-eu-es.utf8 (= 2.28) virtual-locale-eu-es@euro (= 2.28)"
RDEPENDS_glibc-langpack-eu = "glibc glibc-common"
RPROVIDES_glibc-langpack-fa = "locale-base-fa-ir (= 2.28) virtual-locale-fa (= 2.28) virtual-locale-fa-ir (= 2.28)"
RDEPENDS_glibc-langpack-fa = "glibc glibc-common"
RPROVIDES_glibc-langpack-ff = "locale-base-ff-sn (= 2.28) virtual-locale-ff (= 2.28) virtual-locale-ff-sn (= 2.28)"
RDEPENDS_glibc-langpack-ff = "glibc glibc-common"
RPROVIDES_glibc-langpack-fi = "locale-base-fi-fi (= 2.28) locale-base-fi-fi.utf8 (= 2.28) locale-base-fi-fi@euro (= 2.28) virtual-locale-fi (= 2.28) virtual-locale-fi (= 2.28) virtual-locale-fi (= 2.28) virtual-locale-fi-fi (= 2.28) virtual-locale-fi-fi.utf8 (= 2.28) virtual-locale-fi-fi@euro (= 2.28)"
RDEPENDS_glibc-langpack-fi = "glibc glibc-common"
RPROVIDES_glibc-langpack-fil = "locale-base-fil-ph (= 2.28) virtual-locale-fil (= 2.28) virtual-locale-fil-ph (= 2.28)"
RDEPENDS_glibc-langpack-fil = "glibc glibc-common"
RPROVIDES_glibc-langpack-fo = "locale-base-fo-fo (= 2.28) locale-base-fo-fo.utf8 (= 2.28) virtual-locale-fo (= 2.28) virtual-locale-fo (= 2.28) virtual-locale-fo-fo (= 2.28) virtual-locale-fo-fo.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-fo = "glibc glibc-common"
RPROVIDES_glibc-langpack-fr = "locale-base-fr-be (= 2.28) locale-base-fr-be.utf8 (= 2.28) locale-base-fr-be@euro (= 2.28) locale-base-fr-ca (= 2.28) locale-base-fr-ca.utf8 (= 2.28) locale-base-fr-ch (= 2.28) locale-base-fr-ch.utf8 (= 2.28) locale-base-fr-fr (= 2.28) locale-base-fr-fr.utf8 (= 2.28) locale-base-fr-fr@euro (= 2.28) locale-base-fr-lu (= 2.28) locale-base-fr-lu.utf8 (= 2.28) locale-base-fr-lu@euro (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr-be (= 2.28) virtual-locale-fr-be.utf8 (= 2.28) virtual-locale-fr-be@euro (= 2.28) virtual-locale-fr-ca (= 2.28) virtual-locale-fr-ca.utf8 (= 2.28) virtual-locale-fr-ch (= 2.28) virtual-locale-fr-ch.utf8 (= 2.28) virtual-locale-fr-fr (= 2.28) virtual-locale-fr-fr.utf8 (= 2.28) virtual-locale-fr-fr@euro (= 2.28) virtual-locale-fr-lu (= 2.28) virtual-locale-fr-lu.utf8 (= 2.28) virtual-locale-fr-lu@euro (= 2.28)"
RDEPENDS_glibc-langpack-fr = "glibc glibc-common"
RPROVIDES_glibc-langpack-fur = "locale-base-fur-it (= 2.28) virtual-locale-fur (= 2.28) virtual-locale-fur-it (= 2.28)"
RDEPENDS_glibc-langpack-fur = "glibc glibc-common"
RPROVIDES_glibc-langpack-fy = "locale-base-fy-de (= 2.28) locale-base-fy-nl (= 2.28) virtual-locale-fy (= 2.28) virtual-locale-fy (= 2.28) virtual-locale-fy-de (= 2.28) virtual-locale-fy-nl (= 2.28)"
RDEPENDS_glibc-langpack-fy = "glibc glibc-common"
RPROVIDES_glibc-langpack-ga = "locale-base-ga-ie (= 2.28) locale-base-ga-ie.utf8 (= 2.28) locale-base-ga-ie@euro (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga-ie (= 2.28) virtual-locale-ga-ie.utf8 (= 2.28) virtual-locale-ga-ie@euro (= 2.28)"
RDEPENDS_glibc-langpack-ga = "glibc glibc-common"
RPROVIDES_glibc-langpack-gd = "locale-base-gd-gb (= 2.28) locale-base-gd-gb.utf8 (= 2.28) virtual-locale-gd (= 2.28) virtual-locale-gd (= 2.28) virtual-locale-gd-gb (= 2.28) virtual-locale-gd-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-gd = "glibc glibc-common"
RPROVIDES_glibc-langpack-gez = "locale-base-gez-er (= 2.28) locale-base-gez-er@abegede (= 2.28) locale-base-gez-et (= 2.28) locale-base-gez-et@abegede (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez-er (= 2.28) virtual-locale-gez-er@abegede (= 2.28) virtual-locale-gez-et (= 2.28) virtual-locale-gez-et@abegede (= 2.28)"
RDEPENDS_glibc-langpack-gez = "glibc glibc-common"
RPROVIDES_glibc-langpack-gl = "locale-base-gl-es (= 2.28) locale-base-gl-es.utf8 (= 2.28) locale-base-gl-es@euro (= 2.28) virtual-locale-gl (= 2.28) virtual-locale-gl (= 2.28) virtual-locale-gl (= 2.28) virtual-locale-gl-es (= 2.28) virtual-locale-gl-es.utf8 (= 2.28) virtual-locale-gl-es@euro (= 2.28)"
RDEPENDS_glibc-langpack-gl = "glibc glibc-common"
RPROVIDES_glibc-langpack-gu = "locale-base-gu-in (= 2.28) virtual-locale-gu (= 2.28) virtual-locale-gu-in (= 2.28)"
RDEPENDS_glibc-langpack-gu = "glibc glibc-common"
RPROVIDES_glibc-langpack-gv = "locale-base-gv-gb (= 2.28) locale-base-gv-gb.utf8 (= 2.28) virtual-locale-gv (= 2.28) virtual-locale-gv (= 2.28) virtual-locale-gv-gb (= 2.28) virtual-locale-gv-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-gv = "glibc glibc-common"
RPROVIDES_glibc-langpack-ha = "locale-base-ha-ng (= 2.28) virtual-locale-ha (= 2.28) virtual-locale-ha-ng (= 2.28)"
RDEPENDS_glibc-langpack-ha = "glibc glibc-common"
RPROVIDES_glibc-langpack-hak = "locale-base-hak-tw (= 2.28) virtual-locale-hak (= 2.28) virtual-locale-hak-tw (= 2.28)"
RDEPENDS_glibc-langpack-hak = "glibc glibc-common"
RPROVIDES_glibc-langpack-he = "locale-base-he-il (= 2.28) locale-base-he-il.utf8 (= 2.28) virtual-locale-he (= 2.28) virtual-locale-he (= 2.28) virtual-locale-he-il (= 2.28) virtual-locale-he-il.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-he = "glibc glibc-common"
RPROVIDES_glibc-langpack-hi = "locale-base-hi-in (= 2.28) virtual-locale-hi (= 2.28) virtual-locale-hi-in (= 2.28)"
RDEPENDS_glibc-langpack-hi = "glibc glibc-common"
RPROVIDES_glibc-langpack-hif = "locale-base-hif-fj (= 2.28) virtual-locale-hif (= 2.28) virtual-locale-hif-fj (= 2.28)"
RDEPENDS_glibc-langpack-hif = "glibc glibc-common"
RPROVIDES_glibc-langpack-hne = "locale-base-hne-in (= 2.28) virtual-locale-hne (= 2.28) virtual-locale-hne-in (= 2.28)"
RDEPENDS_glibc-langpack-hne = "glibc glibc-common"
RPROVIDES_glibc-langpack-hr = "locale-base-hr-hr (= 2.28) locale-base-hr-hr.utf8 (= 2.28) virtual-locale-hr (= 2.28) virtual-locale-hr (= 2.28) virtual-locale-hr-hr (= 2.28) virtual-locale-hr-hr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-hr = "glibc glibc-common"
RPROVIDES_glibc-langpack-hsb = "locale-base-hsb-de (= 2.28) locale-base-hsb-de.utf8 (= 2.28) virtual-locale-hsb (= 2.28) virtual-locale-hsb (= 2.28) virtual-locale-hsb-de (= 2.28) virtual-locale-hsb-de.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-hsb = "glibc glibc-common"
RPROVIDES_glibc-langpack-ht = "locale-base-ht-ht (= 2.28) virtual-locale-ht (= 2.28) virtual-locale-ht-ht (= 2.28)"
RDEPENDS_glibc-langpack-ht = "glibc glibc-common"
RPROVIDES_glibc-langpack-hu = "locale-base-hu-hu (= 2.28) locale-base-hu-hu.utf8 (= 2.28) virtual-locale-hu (= 2.28) virtual-locale-hu (= 2.28) virtual-locale-hu-hu (= 2.28) virtual-locale-hu-hu.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-hu = "glibc glibc-common"
RPROVIDES_glibc-langpack-hy = "locale-base-hy-am (= 2.28) locale-base-hy-am.armscii8 (= 2.28) virtual-locale-hy (= 2.28) virtual-locale-hy (= 2.28) virtual-locale-hy-am (= 2.28) virtual-locale-hy-am.armscii8 (= 2.28)"
RDEPENDS_glibc-langpack-hy = "glibc glibc-common"
RPROVIDES_glibc-langpack-ia = "locale-base-ia-fr (= 2.28) virtual-locale-ia (= 2.28) virtual-locale-ia-fr (= 2.28)"
RDEPENDS_glibc-langpack-ia = "glibc glibc-common"
RPROVIDES_glibc-langpack-id = "locale-base-id-id (= 2.28) locale-base-id-id.utf8 (= 2.28) virtual-locale-id (= 2.28) virtual-locale-id (= 2.28) virtual-locale-id-id (= 2.28) virtual-locale-id-id.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-id = "glibc glibc-common"
RPROVIDES_glibc-langpack-ig = "locale-base-ig-ng (= 2.28) virtual-locale-ig (= 2.28) virtual-locale-ig-ng (= 2.28)"
RDEPENDS_glibc-langpack-ig = "glibc glibc-common"
RPROVIDES_glibc-langpack-ik = "locale-base-ik-ca (= 2.28) virtual-locale-ik (= 2.28) virtual-locale-ik-ca (= 2.28)"
RDEPENDS_glibc-langpack-ik = "glibc glibc-common"
RPROVIDES_glibc-langpack-is = "locale-base-is-is (= 2.28) locale-base-is-is.utf8 (= 2.28) virtual-locale-is (= 2.28) virtual-locale-is (= 2.28) virtual-locale-is-is (= 2.28) virtual-locale-is-is.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-is = "glibc glibc-common"
RPROVIDES_glibc-langpack-it = "locale-base-it-ch (= 2.28) locale-base-it-ch.utf8 (= 2.28) locale-base-it-it (= 2.28) locale-base-it-it.utf8 (= 2.28) locale-base-it-it@euro (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it-ch (= 2.28) virtual-locale-it-ch.utf8 (= 2.28) virtual-locale-it-it (= 2.28) virtual-locale-it-it.utf8 (= 2.28) virtual-locale-it-it@euro (= 2.28)"
RDEPENDS_glibc-langpack-it = "glibc glibc-common"
RPROVIDES_glibc-langpack-iu = "locale-base-iu-ca (= 2.28) virtual-locale-iu (= 2.28) virtual-locale-iu-ca (= 2.28)"
RDEPENDS_glibc-langpack-iu = "glibc glibc-common"
RPROVIDES_glibc-langpack-ja = "locale-base-ja-jp.eucjp (= 2.28) locale-base-ja-jp.utf8 (= 2.28) virtual-locale-ja (= 2.28) virtual-locale-ja (= 2.28) virtual-locale-ja-jp.eucjp (= 2.28) virtual-locale-ja-jp.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ja = "glibc glibc-common"
RPROVIDES_glibc-langpack-ka = "locale-base-ka-ge (= 2.28) locale-base-ka-ge.utf8 (= 2.28) virtual-locale-ka (= 2.28) virtual-locale-ka (= 2.28) virtual-locale-ka-ge (= 2.28) virtual-locale-ka-ge.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ka = "glibc glibc-common"
RPROVIDES_glibc-langpack-kab = "locale-base-kab-dz (= 2.28) virtual-locale-kab (= 2.28) virtual-locale-kab-dz (= 2.28)"
RDEPENDS_glibc-langpack-kab = "glibc glibc-common"
RPROVIDES_glibc-langpack-kk = "locale-base-kk-kz (= 2.28) locale-base-kk-kz.utf8 (= 2.28) virtual-locale-kk (= 2.28) virtual-locale-kk (= 2.28) virtual-locale-kk-kz (= 2.28) virtual-locale-kk-kz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kk = "glibc glibc-common"
RPROVIDES_glibc-langpack-kl = "locale-base-kl-gl (= 2.28) locale-base-kl-gl.utf8 (= 2.28) virtual-locale-kl (= 2.28) virtual-locale-kl (= 2.28) virtual-locale-kl-gl (= 2.28) virtual-locale-kl-gl.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kl = "glibc glibc-common"
RPROVIDES_glibc-langpack-km = "locale-base-km-kh (= 2.28) virtual-locale-km (= 2.28) virtual-locale-km-kh (= 2.28)"
RDEPENDS_glibc-langpack-km = "glibc glibc-common"
RPROVIDES_glibc-langpack-kn = "locale-base-kn-in (= 2.28) virtual-locale-kn (= 2.28) virtual-locale-kn-in (= 2.28)"
RDEPENDS_glibc-langpack-kn = "glibc glibc-common"
RPROVIDES_glibc-langpack-ko = "locale-base-ko-kr.euckr (= 2.28) locale-base-ko-kr.utf8 (= 2.28) virtual-locale-ko (= 2.28) virtual-locale-ko (= 2.28) virtual-locale-ko-kr.euckr (= 2.28) virtual-locale-ko-kr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ko = "glibc glibc-common"
RPROVIDES_glibc-langpack-kok = "locale-base-kok-in (= 2.28) virtual-locale-kok (= 2.28) virtual-locale-kok-in (= 2.28)"
RDEPENDS_glibc-langpack-kok = "glibc glibc-common"
RPROVIDES_glibc-langpack-ks = "locale-base-ks-in (= 2.28) locale-base-ks-in@devanagari (= 2.28) virtual-locale-ks (= 2.28) virtual-locale-ks (= 2.28) virtual-locale-ks-in (= 2.28) virtual-locale-ks-in@devanagari (= 2.28)"
RDEPENDS_glibc-langpack-ks = "glibc glibc-common"
RPROVIDES_glibc-langpack-ku = "locale-base-ku-tr (= 2.28) locale-base-ku-tr.utf8 (= 2.28) virtual-locale-ku (= 2.28) virtual-locale-ku (= 2.28) virtual-locale-ku-tr (= 2.28) virtual-locale-ku-tr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ku = "glibc glibc-common"
RPROVIDES_glibc-langpack-kw = "locale-base-kw-gb (= 2.28) locale-base-kw-gb.utf8 (= 2.28) virtual-locale-kw (= 2.28) virtual-locale-kw (= 2.28) virtual-locale-kw-gb (= 2.28) virtual-locale-kw-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kw = "glibc glibc-common"
RPROVIDES_glibc-langpack-ky = "locale-base-ky-kg (= 2.28) virtual-locale-ky (= 2.28) virtual-locale-ky-kg (= 2.28)"
RDEPENDS_glibc-langpack-ky = "glibc glibc-common"
RPROVIDES_glibc-langpack-lb = "locale-base-lb-lu (= 2.28) virtual-locale-lb (= 2.28) virtual-locale-lb-lu (= 2.28)"
RDEPENDS_glibc-langpack-lb = "glibc glibc-common"
RPROVIDES_glibc-langpack-lg = "locale-base-lg-ug (= 2.28) locale-base-lg-ug.utf8 (= 2.28) virtual-locale-lg (= 2.28) virtual-locale-lg (= 2.28) virtual-locale-lg-ug (= 2.28) virtual-locale-lg-ug.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lg = "glibc glibc-common"
RPROVIDES_glibc-langpack-li = "locale-base-li-be (= 2.28) locale-base-li-nl (= 2.28) virtual-locale-li (= 2.28) virtual-locale-li (= 2.28) virtual-locale-li-be (= 2.28) virtual-locale-li-nl (= 2.28)"
RDEPENDS_glibc-langpack-li = "glibc glibc-common"
RPROVIDES_glibc-langpack-lij = "locale-base-lij-it (= 2.28) virtual-locale-lij (= 2.28) virtual-locale-lij-it (= 2.28)"
RDEPENDS_glibc-langpack-lij = "glibc glibc-common"
RPROVIDES_glibc-langpack-ln = "locale-base-ln-cd (= 2.28) virtual-locale-ln (= 2.28) virtual-locale-ln-cd (= 2.28)"
RDEPENDS_glibc-langpack-ln = "glibc glibc-common"
RPROVIDES_glibc-langpack-lo = "locale-base-lo-la (= 2.28) virtual-locale-lo (= 2.28) virtual-locale-lo-la (= 2.28)"
RDEPENDS_glibc-langpack-lo = "glibc glibc-common"
RPROVIDES_glibc-langpack-lt = "locale-base-lt-lt (= 2.28) locale-base-lt-lt.utf8 (= 2.28) virtual-locale-lt (= 2.28) virtual-locale-lt (= 2.28) virtual-locale-lt-lt (= 2.28) virtual-locale-lt-lt.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lt = "glibc glibc-common"
RPROVIDES_glibc-langpack-lv = "locale-base-lv-lv (= 2.28) locale-base-lv-lv.utf8 (= 2.28) virtual-locale-lv (= 2.28) virtual-locale-lv (= 2.28) virtual-locale-lv-lv (= 2.28) virtual-locale-lv-lv.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lv = "glibc glibc-common"
RPROVIDES_glibc-langpack-lzh = "locale-base-lzh-tw (= 2.28) virtual-locale-lzh (= 2.28) virtual-locale-lzh-tw (= 2.28)"
RDEPENDS_glibc-langpack-lzh = "glibc glibc-common"
RPROVIDES_glibc-langpack-mag = "locale-base-mag-in (= 2.28) virtual-locale-mag (= 2.28) virtual-locale-mag-in (= 2.28)"
RDEPENDS_glibc-langpack-mag = "glibc glibc-common"
RPROVIDES_glibc-langpack-mai = "locale-base-mai-in (= 2.28) locale-base-mai-np (= 2.28) virtual-locale-mai (= 2.28) virtual-locale-mai (= 2.28) virtual-locale-mai-in (= 2.28) virtual-locale-mai-np (= 2.28)"
RDEPENDS_glibc-langpack-mai = "glibc glibc-common"
RPROVIDES_glibc-langpack-mfe = "locale-base-mfe-mu (= 2.28) virtual-locale-mfe (= 2.28) virtual-locale-mfe-mu (= 2.28)"
RDEPENDS_glibc-langpack-mfe = "glibc glibc-common"
RPROVIDES_glibc-langpack-mg = "locale-base-mg-mg (= 2.28) locale-base-mg-mg.utf8 (= 2.28) virtual-locale-mg (= 2.28) virtual-locale-mg (= 2.28) virtual-locale-mg-mg (= 2.28) virtual-locale-mg-mg.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mg = "glibc glibc-common"
RPROVIDES_glibc-langpack-mhr = "locale-base-mhr-ru (= 2.28) virtual-locale-mhr (= 2.28) virtual-locale-mhr-ru (= 2.28)"
RDEPENDS_glibc-langpack-mhr = "glibc glibc-common"
RPROVIDES_glibc-langpack-mi = "locale-base-mi-nz (= 2.28) locale-base-mi-nz.utf8 (= 2.28) virtual-locale-mi (= 2.28) virtual-locale-mi (= 2.28) virtual-locale-mi-nz (= 2.28) virtual-locale-mi-nz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mi = "glibc glibc-common"
RPROVIDES_glibc-langpack-miq = "locale-base-miq-ni (= 2.28) virtual-locale-miq (= 2.28) virtual-locale-miq-ni (= 2.28)"
RDEPENDS_glibc-langpack-miq = "glibc glibc-common"
RPROVIDES_glibc-langpack-mjw = "locale-base-mjw-in (= 2.28) virtual-locale-mjw (= 2.28) virtual-locale-mjw-in (= 2.28)"
RDEPENDS_glibc-langpack-mjw = "glibc glibc-common"
RPROVIDES_glibc-langpack-mk = "locale-base-mk-mk (= 2.28) locale-base-mk-mk.utf8 (= 2.28) virtual-locale-mk (= 2.28) virtual-locale-mk (= 2.28) virtual-locale-mk-mk (= 2.28) virtual-locale-mk-mk.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mk = "glibc glibc-common"
RPROVIDES_glibc-langpack-ml = "locale-base-ml-in (= 2.28) virtual-locale-ml (= 2.28) virtual-locale-ml-in (= 2.28)"
RDEPENDS_glibc-langpack-ml = "glibc glibc-common"
RPROVIDES_glibc-langpack-mn = "locale-base-mn-mn (= 2.28) virtual-locale-mn (= 2.28) virtual-locale-mn-mn (= 2.28)"
RDEPENDS_glibc-langpack-mn = "glibc glibc-common"
RPROVIDES_glibc-langpack-mni = "locale-base-mni-in (= 2.28) virtual-locale-mni (= 2.28) virtual-locale-mni-in (= 2.28)"
RDEPENDS_glibc-langpack-mni = "glibc glibc-common"
RPROVIDES_glibc-langpack-mr = "locale-base-mr-in (= 2.28) virtual-locale-mr (= 2.28) virtual-locale-mr-in (= 2.28)"
RDEPENDS_glibc-langpack-mr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ms = "locale-base-ms-my (= 2.28) locale-base-ms-my.utf8 (= 2.28) virtual-locale-ms (= 2.28) virtual-locale-ms (= 2.28) virtual-locale-ms-my (= 2.28) virtual-locale-ms-my.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ms = "glibc glibc-common"
RPROVIDES_glibc-langpack-mt = "locale-base-mt-mt (= 2.28) locale-base-mt-mt.utf8 (= 2.28) virtual-locale-mt (= 2.28) virtual-locale-mt (= 2.28) virtual-locale-mt-mt (= 2.28) virtual-locale-mt-mt.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mt = "glibc glibc-common"
RPROVIDES_glibc-langpack-my = "locale-base-my-mm (= 2.28) virtual-locale-my (= 2.28) virtual-locale-my-mm (= 2.28)"
RDEPENDS_glibc-langpack-my = "glibc glibc-common"
RPROVIDES_glibc-langpack-nan = "locale-base-nan-tw (= 2.28) locale-base-nan-tw@latin (= 2.28) virtual-locale-nan (= 2.28) virtual-locale-nan (= 2.28) virtual-locale-nan-tw (= 2.28) virtual-locale-nan-tw@latin (= 2.28)"
RDEPENDS_glibc-langpack-nan = "glibc glibc-common"
RPROVIDES_glibc-langpack-nb = "locale-base-nb-no (= 2.28) locale-base-nb-no.utf8 (= 2.28) virtual-locale-nb (= 2.28) virtual-locale-nb (= 2.28) virtual-locale-nb-no (= 2.28) virtual-locale-nb-no.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-nb = "glibc glibc-common"
RPROVIDES_glibc-langpack-nds = "locale-base-nds-de (= 2.28) locale-base-nds-nl (= 2.28) virtual-locale-nds (= 2.28) virtual-locale-nds (= 2.28) virtual-locale-nds-de (= 2.28) virtual-locale-nds-nl (= 2.28)"
RDEPENDS_glibc-langpack-nds = "glibc glibc-common"
RPROVIDES_glibc-langpack-ne = "locale-base-ne-np (= 2.28) virtual-locale-ne (= 2.28) virtual-locale-ne-np (= 2.28)"
RDEPENDS_glibc-langpack-ne = "glibc glibc-common"
RPROVIDES_glibc-langpack-nhn = "locale-base-nhn-mx (= 2.28) virtual-locale-nhn (= 2.28) virtual-locale-nhn-mx (= 2.28)"
RDEPENDS_glibc-langpack-nhn = "glibc glibc-common"
RPROVIDES_glibc-langpack-niu = "locale-base-niu-nu (= 2.28) locale-base-niu-nz (= 2.28) virtual-locale-niu (= 2.28) virtual-locale-niu (= 2.28) virtual-locale-niu-nu (= 2.28) virtual-locale-niu-nz (= 2.28)"
RDEPENDS_glibc-langpack-niu = "glibc glibc-common"
RPROVIDES_glibc-langpack-nl = "locale-base-nl-aw (= 2.28) locale-base-nl-be (= 2.28) locale-base-nl-be.utf8 (= 2.28) locale-base-nl-be@euro (= 2.28) locale-base-nl-nl (= 2.28) locale-base-nl-nl.utf8 (= 2.28) locale-base-nl-nl@euro (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl-aw (= 2.28) virtual-locale-nl-be (= 2.28) virtual-locale-nl-be.utf8 (= 2.28) virtual-locale-nl-be@euro (= 2.28) virtual-locale-nl-nl (= 2.28) virtual-locale-nl-nl.utf8 (= 2.28) virtual-locale-nl-nl@euro (= 2.28)"
RDEPENDS_glibc-langpack-nl = "glibc glibc-common"
RPROVIDES_glibc-langpack-nn = "locale-base-nn-no (= 2.28) locale-base-nn-no.utf8 (= 2.28) virtual-locale-nn (= 2.28) virtual-locale-nn (= 2.28) virtual-locale-nn-no (= 2.28) virtual-locale-nn-no.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-nn = "glibc glibc-common"
RPROVIDES_glibc-langpack-nr = "locale-base-nr-za (= 2.28) virtual-locale-nr (= 2.28) virtual-locale-nr-za (= 2.28)"
RDEPENDS_glibc-langpack-nr = "glibc glibc-common"
RPROVIDES_glibc-langpack-nso = "locale-base-nso-za (= 2.28) virtual-locale-nso (= 2.28) virtual-locale-nso-za (= 2.28)"
RDEPENDS_glibc-langpack-nso = "glibc glibc-common"
RPROVIDES_glibc-langpack-oc = "locale-base-oc-fr (= 2.28) locale-base-oc-fr.utf8 (= 2.28) virtual-locale-oc (= 2.28) virtual-locale-oc (= 2.28) virtual-locale-oc-fr (= 2.28) virtual-locale-oc-fr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-oc = "glibc glibc-common"
RPROVIDES_glibc-langpack-om = "locale-base-om-et (= 2.28) locale-base-om-ke (= 2.28) locale-base-om-ke.utf8 (= 2.28) virtual-locale-om (= 2.28) virtual-locale-om (= 2.28) virtual-locale-om (= 2.28) virtual-locale-om-et (= 2.28) virtual-locale-om-ke (= 2.28) virtual-locale-om-ke.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-om = "glibc glibc-common"
RPROVIDES_glibc-langpack-or = "locale-base-or-in (= 2.28) virtual-locale-or (= 2.28) virtual-locale-or-in (= 2.28)"
RDEPENDS_glibc-langpack-or = "glibc glibc-common"
RPROVIDES_glibc-langpack-os = "locale-base-os-ru (= 2.28) virtual-locale-os (= 2.28) virtual-locale-os-ru (= 2.28)"
RDEPENDS_glibc-langpack-os = "glibc glibc-common"
RPROVIDES_glibc-langpack-pa = "locale-base-pa-in (= 2.28) locale-base-pa-pk (= 2.28) virtual-locale-pa (= 2.28) virtual-locale-pa (= 2.28) virtual-locale-pa-in (= 2.28) virtual-locale-pa-pk (= 2.28)"
RDEPENDS_glibc-langpack-pa = "glibc glibc-common"
RPROVIDES_glibc-langpack-pap = "locale-base-pap-aw (= 2.28) locale-base-pap-cw (= 2.28) virtual-locale-pap (= 2.28) virtual-locale-pap (= 2.28) virtual-locale-pap-aw (= 2.28) virtual-locale-pap-cw (= 2.28)"
RDEPENDS_glibc-langpack-pap = "glibc glibc-common"
RPROVIDES_glibc-langpack-pl = "locale-base-pl-pl (= 2.28) locale-base-pl-pl.utf8 (= 2.28) virtual-locale-pl (= 2.28) virtual-locale-pl (= 2.28) virtual-locale-pl-pl (= 2.28) virtual-locale-pl-pl.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-pl = "glibc glibc-common"
RPROVIDES_glibc-langpack-ps = "locale-base-ps-af (= 2.28) virtual-locale-ps (= 2.28) virtual-locale-ps-af (= 2.28)"
RDEPENDS_glibc-langpack-ps = "glibc glibc-common"
RPROVIDES_glibc-langpack-pt = "locale-base-pt-br (= 2.28) locale-base-pt-br.utf8 (= 2.28) locale-base-pt-pt (= 2.28) locale-base-pt-pt.utf8 (= 2.28) locale-base-pt-pt@euro (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt-br (= 2.28) virtual-locale-pt-br.utf8 (= 2.28) virtual-locale-pt-pt (= 2.28) virtual-locale-pt-pt.utf8 (= 2.28) virtual-locale-pt-pt@euro (= 2.28)"
RDEPENDS_glibc-langpack-pt = "glibc glibc-common"
RPROVIDES_glibc-langpack-quz = "locale-base-quz-pe (= 2.28) virtual-locale-quz (= 2.28) virtual-locale-quz-pe (= 2.28)"
RDEPENDS_glibc-langpack-quz = "glibc glibc-common"
RPROVIDES_glibc-langpack-raj = "locale-base-raj-in (= 2.28) virtual-locale-raj (= 2.28) virtual-locale-raj-in (= 2.28)"
RDEPENDS_glibc-langpack-raj = "glibc glibc-common"
RPROVIDES_glibc-langpack-ro = "locale-base-ro-ro (= 2.28) locale-base-ro-ro.utf8 (= 2.28) virtual-locale-ro (= 2.28) virtual-locale-ro (= 2.28) virtual-locale-ro-ro (= 2.28) virtual-locale-ro-ro.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ro = "glibc glibc-common"
RPROVIDES_glibc-langpack-ru = "locale-base-ru-ru (= 2.28) locale-base-ru-ru.koi8r (= 2.28) locale-base-ru-ru.utf8 (= 2.28) locale-base-ru-ua (= 2.28) locale-base-ru-ua.utf8 (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru-ru (= 2.28) virtual-locale-ru-ru.koi8r (= 2.28) virtual-locale-ru-ru.utf8 (= 2.28) virtual-locale-ru-ua (= 2.28) virtual-locale-ru-ua.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ru = "glibc glibc-common"
RPROVIDES_glibc-langpack-rw = "locale-base-rw-rw (= 2.28) virtual-locale-rw (= 2.28) virtual-locale-rw-rw (= 2.28)"
RDEPENDS_glibc-langpack-rw = "glibc glibc-common"
RPROVIDES_glibc-langpack-sa = "locale-base-sa-in (= 2.28) virtual-locale-sa (= 2.28) virtual-locale-sa-in (= 2.28)"
RDEPENDS_glibc-langpack-sa = "glibc glibc-common"
RPROVIDES_glibc-langpack-sah = "locale-base-sah-ru (= 2.28) virtual-locale-sah (= 2.28) virtual-locale-sah-ru (= 2.28)"
RDEPENDS_glibc-langpack-sah = "glibc glibc-common"
RPROVIDES_glibc-langpack-sat = "locale-base-sat-in (= 2.28) virtual-locale-sat (= 2.28) virtual-locale-sat-in (= 2.28)"
RDEPENDS_glibc-langpack-sat = "glibc glibc-common"
RPROVIDES_glibc-langpack-sc = "locale-base-sc-it (= 2.28) virtual-locale-sc (= 2.28) virtual-locale-sc-it (= 2.28)"
RDEPENDS_glibc-langpack-sc = "glibc glibc-common"
RPROVIDES_glibc-langpack-sd = "locale-base-sd-in (= 2.28) locale-base-sd-in@devanagari (= 2.28) virtual-locale-sd (= 2.28) virtual-locale-sd (= 2.28) virtual-locale-sd-in (= 2.28) virtual-locale-sd-in@devanagari (= 2.28)"
RDEPENDS_glibc-langpack-sd = "glibc glibc-common"
RPROVIDES_glibc-langpack-se = "locale-base-se-no (= 2.28) virtual-locale-se (= 2.28) virtual-locale-se-no (= 2.28)"
RDEPENDS_glibc-langpack-se = "glibc glibc-common"
RPROVIDES_glibc-langpack-sgs = "locale-base-sgs-lt (= 2.28) virtual-locale-sgs (= 2.28) virtual-locale-sgs-lt (= 2.28)"
RDEPENDS_glibc-langpack-sgs = "glibc glibc-common"
RPROVIDES_glibc-langpack-shn = "locale-base-shn-mm (= 2.28) virtual-locale-shn (= 2.28) virtual-locale-shn-mm (= 2.28)"
RDEPENDS_glibc-langpack-shn = "glibc glibc-common"
RPROVIDES_glibc-langpack-shs = "locale-base-shs-ca (= 2.28) virtual-locale-shs (= 2.28) virtual-locale-shs-ca (= 2.28)"
RDEPENDS_glibc-langpack-shs = "glibc glibc-common"
RPROVIDES_glibc-langpack-si = "locale-base-si-lk (= 2.28) virtual-locale-si (= 2.28) virtual-locale-si-lk (= 2.28)"
RDEPENDS_glibc-langpack-si = "glibc glibc-common"
RPROVIDES_glibc-langpack-sid = "locale-base-sid-et (= 2.28) virtual-locale-sid (= 2.28) virtual-locale-sid-et (= 2.28)"
RDEPENDS_glibc-langpack-sid = "glibc glibc-common"
RPROVIDES_glibc-langpack-sk = "locale-base-sk-sk (= 2.28) locale-base-sk-sk.utf8 (= 2.28) virtual-locale-sk (= 2.28) virtual-locale-sk (= 2.28) virtual-locale-sk-sk (= 2.28) virtual-locale-sk-sk.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sk = "glibc glibc-common"
RPROVIDES_glibc-langpack-sl = "locale-base-sl-si (= 2.28) locale-base-sl-si.utf8 (= 2.28) virtual-locale-sl (= 2.28) virtual-locale-sl (= 2.28) virtual-locale-sl-si (= 2.28) virtual-locale-sl-si.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sl = "glibc glibc-common"
RPROVIDES_glibc-langpack-sm = "locale-base-sm-ws (= 2.28) virtual-locale-sm (= 2.28) virtual-locale-sm-ws (= 2.28)"
RDEPENDS_glibc-langpack-sm = "glibc glibc-common"
RPROVIDES_glibc-langpack-so = "locale-base-so-dj (= 2.28) locale-base-so-dj.utf8 (= 2.28) locale-base-so-et (= 2.28) locale-base-so-ke (= 2.28) locale-base-so-ke.utf8 (= 2.28) locale-base-so-so (= 2.28) locale-base-so-so.utf8 (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so-dj (= 2.28) virtual-locale-so-dj.utf8 (= 2.28) virtual-locale-so-et (= 2.28) virtual-locale-so-ke (= 2.28) virtual-locale-so-ke.utf8 (= 2.28) virtual-locale-so-so (= 2.28) virtual-locale-so-so.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-so = "glibc glibc-common"
RPROVIDES_glibc-langpack-sq = "locale-base-sq-al (= 2.28) locale-base-sq-al.utf8 (= 2.28) locale-base-sq-mk (= 2.28) virtual-locale-sq (= 2.28) virtual-locale-sq (= 2.28) virtual-locale-sq (= 2.28) virtual-locale-sq-al (= 2.28) virtual-locale-sq-al.utf8 (= 2.28) virtual-locale-sq-mk (= 2.28)"
RDEPENDS_glibc-langpack-sq = "glibc glibc-common"
RPROVIDES_glibc-langpack-sr = "locale-base-sr-me (= 2.28) locale-base-sr-rs (= 2.28) locale-base-sr-rs@latin (= 2.28) virtual-locale-sr (= 2.28) virtual-locale-sr (= 2.28) virtual-locale-sr (= 2.28) virtual-locale-sr-me (= 2.28) virtual-locale-sr-rs (= 2.28) virtual-locale-sr-rs@latin (= 2.28)"
RDEPENDS_glibc-langpack-sr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ss = "locale-base-ss-za (= 2.28) virtual-locale-ss (= 2.28) virtual-locale-ss-za (= 2.28)"
RDEPENDS_glibc-langpack-ss = "glibc glibc-common"
RPROVIDES_glibc-langpack-st = "locale-base-st-za (= 2.28) locale-base-st-za.utf8 (= 2.28) virtual-locale-st (= 2.28) virtual-locale-st (= 2.28) virtual-locale-st-za (= 2.28) virtual-locale-st-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-st = "glibc glibc-common"
RPROVIDES_glibc-langpack-sv = "locale-base-sv-fi (= 2.28) locale-base-sv-fi.utf8 (= 2.28) locale-base-sv-fi@euro (= 2.28) locale-base-sv-se (= 2.28) locale-base-sv-se.iso885915 (= 2.28) locale-base-sv-se.utf8 (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv-fi (= 2.28) virtual-locale-sv-fi.utf8 (= 2.28) virtual-locale-sv-fi@euro (= 2.28) virtual-locale-sv-se (= 2.28) virtual-locale-sv-se.iso885915 (= 2.28) virtual-locale-sv-se.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sv = "glibc glibc-common"
RPROVIDES_glibc-langpack-sw = "locale-base-sw-ke (= 2.28) locale-base-sw-tz (= 2.28) virtual-locale-sw (= 2.28) virtual-locale-sw (= 2.28) virtual-locale-sw-ke (= 2.28) virtual-locale-sw-tz (= 2.28)"
RDEPENDS_glibc-langpack-sw = "glibc glibc-common"
RPROVIDES_glibc-langpack-szl = "locale-base-szl-pl (= 2.28) virtual-locale-szl (= 2.28) virtual-locale-szl-pl (= 2.28)"
RDEPENDS_glibc-langpack-szl = "glibc glibc-common"
RPROVIDES_glibc-langpack-ta = "locale-base-ta-in (= 2.28) locale-base-ta-lk (= 2.28) virtual-locale-ta (= 2.28) virtual-locale-ta (= 2.28) virtual-locale-ta-in (= 2.28) virtual-locale-ta-lk (= 2.28)"
RDEPENDS_glibc-langpack-ta = "glibc glibc-common"
RPROVIDES_glibc-langpack-tcy = "locale-base-tcy-in.utf8 (= 2.28) virtual-locale-tcy (= 2.28) virtual-locale-tcy-in.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tcy = "glibc glibc-common"
RPROVIDES_glibc-langpack-te = "locale-base-te-in (= 2.28) virtual-locale-te (= 2.28) virtual-locale-te-in (= 2.28)"
RDEPENDS_glibc-langpack-te = "glibc glibc-common"
RPROVIDES_glibc-langpack-tg = "locale-base-tg-tj (= 2.28) locale-base-tg-tj.utf8 (= 2.28) virtual-locale-tg (= 2.28) virtual-locale-tg (= 2.28) virtual-locale-tg-tj (= 2.28) virtual-locale-tg-tj.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tg = "glibc glibc-common"
RPROVIDES_glibc-langpack-th = "locale-base-th-th (= 2.28) locale-base-th-th.utf8 (= 2.28) virtual-locale-th (= 2.28) virtual-locale-th (= 2.28) virtual-locale-th-th (= 2.28) virtual-locale-th-th.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-th = "glibc glibc-common"
RPROVIDES_glibc-langpack-the = "locale-base-the-np (= 2.28) virtual-locale-the (= 2.28) virtual-locale-the-np (= 2.28)"
RDEPENDS_glibc-langpack-the = "glibc glibc-common"
RPROVIDES_glibc-langpack-ti = "locale-base-ti-er (= 2.28) locale-base-ti-et (= 2.28) virtual-locale-ti (= 2.28) virtual-locale-ti (= 2.28) virtual-locale-ti-er (= 2.28) virtual-locale-ti-et (= 2.28)"
RDEPENDS_glibc-langpack-ti = "glibc glibc-common"
RPROVIDES_glibc-langpack-tig = "locale-base-tig-er (= 2.28) virtual-locale-tig (= 2.28) virtual-locale-tig-er (= 2.28)"
RDEPENDS_glibc-langpack-tig = "glibc glibc-common"
RPROVIDES_glibc-langpack-tk = "locale-base-tk-tm (= 2.28) virtual-locale-tk (= 2.28) virtual-locale-tk-tm (= 2.28)"
RDEPENDS_glibc-langpack-tk = "glibc glibc-common"
RPROVIDES_glibc-langpack-tl = "locale-base-tl-ph (= 2.28) locale-base-tl-ph.utf8 (= 2.28) virtual-locale-tl (= 2.28) virtual-locale-tl (= 2.28) virtual-locale-tl-ph (= 2.28) virtual-locale-tl-ph.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tl = "glibc glibc-common"
RPROVIDES_glibc-langpack-tn = "locale-base-tn-za (= 2.28) virtual-locale-tn (= 2.28) virtual-locale-tn-za (= 2.28)"
RDEPENDS_glibc-langpack-tn = "glibc glibc-common"
RPROVIDES_glibc-langpack-to = "locale-base-to-to (= 2.28) virtual-locale-to (= 2.28) virtual-locale-to-to (= 2.28)"
RDEPENDS_glibc-langpack-to = "glibc glibc-common"
RPROVIDES_glibc-langpack-tpi = "locale-base-tpi-pg (= 2.28) virtual-locale-tpi (= 2.28) virtual-locale-tpi-pg (= 2.28)"
RDEPENDS_glibc-langpack-tpi = "glibc glibc-common"
RPROVIDES_glibc-langpack-tr = "locale-base-tr-cy (= 2.28) locale-base-tr-cy.utf8 (= 2.28) locale-base-tr-tr (= 2.28) locale-base-tr-tr.utf8 (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr-cy (= 2.28) virtual-locale-tr-cy.utf8 (= 2.28) virtual-locale-tr-tr (= 2.28) virtual-locale-tr-tr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ts = "locale-base-ts-za (= 2.28) virtual-locale-ts (= 2.28) virtual-locale-ts-za (= 2.28)"
RDEPENDS_glibc-langpack-ts = "glibc glibc-common"
RPROVIDES_glibc-langpack-tt = "locale-base-tt-ru (= 2.28) locale-base-tt-ru@iqtelif (= 2.28) virtual-locale-tt (= 2.28) virtual-locale-tt (= 2.28) virtual-locale-tt-ru (= 2.28) virtual-locale-tt-ru@iqtelif (= 2.28)"
RDEPENDS_glibc-langpack-tt = "glibc glibc-common"
RPROVIDES_glibc-langpack-ug = "locale-base-ug-cn (= 2.28) virtual-locale-ug (= 2.28) virtual-locale-ug-cn (= 2.28)"
RDEPENDS_glibc-langpack-ug = "glibc glibc-common"
RPROVIDES_glibc-langpack-uk = "locale-base-uk-ua (= 2.28) locale-base-uk-ua.utf8 (= 2.28) virtual-locale-uk (= 2.28) virtual-locale-uk (= 2.28) virtual-locale-uk-ua (= 2.28) virtual-locale-uk-ua.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-uk = "glibc glibc-common"
RPROVIDES_glibc-langpack-unm = "locale-base-unm-us (= 2.28) virtual-locale-unm (= 2.28) virtual-locale-unm-us (= 2.28)"
RDEPENDS_glibc-langpack-unm = "glibc glibc-common"
RPROVIDES_glibc-langpack-ur = "locale-base-ur-in (= 2.28) locale-base-ur-pk (= 2.28) virtual-locale-ur (= 2.28) virtual-locale-ur (= 2.28) virtual-locale-ur-in (= 2.28) virtual-locale-ur-pk (= 2.28)"
RDEPENDS_glibc-langpack-ur = "glibc glibc-common"
RPROVIDES_glibc-langpack-uz = "locale-base-uz-uz (= 2.28) locale-base-uz-uz.utf8 (= 2.28) locale-base-uz-uz@cyrillic (= 2.28) virtual-locale-uz (= 2.28) virtual-locale-uz (= 2.28) virtual-locale-uz (= 2.28) virtual-locale-uz-uz (= 2.28) virtual-locale-uz-uz.utf8 (= 2.28) virtual-locale-uz-uz@cyrillic (= 2.28)"
RDEPENDS_glibc-langpack-uz = "glibc glibc-common"
RPROVIDES_glibc-langpack-ve = "locale-base-ve-za (= 2.28) virtual-locale-ve (= 2.28) virtual-locale-ve-za (= 2.28)"
RDEPENDS_glibc-langpack-ve = "glibc glibc-common"
RPROVIDES_glibc-langpack-vi = "locale-base-vi-vn (= 2.28) virtual-locale-vi (= 2.28) virtual-locale-vi-vn (= 2.28)"
RDEPENDS_glibc-langpack-vi = "glibc glibc-common"
RPROVIDES_glibc-langpack-wa = "locale-base-wa-be (= 2.28) locale-base-wa-be.utf8 (= 2.28) locale-base-wa-be@euro (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa-be (= 2.28) virtual-locale-wa-be.utf8 (= 2.28) virtual-locale-wa-be@euro (= 2.28)"
RDEPENDS_glibc-langpack-wa = "glibc glibc-common"
RPROVIDES_glibc-langpack-wae = "locale-base-wae-ch (= 2.28) virtual-locale-wae (= 2.28) virtual-locale-wae-ch (= 2.28)"
RDEPENDS_glibc-langpack-wae = "glibc glibc-common"
RPROVIDES_glibc-langpack-wal = "locale-base-wal-et (= 2.28) virtual-locale-wal (= 2.28) virtual-locale-wal-et (= 2.28)"
RDEPENDS_glibc-langpack-wal = "glibc glibc-common"
RPROVIDES_glibc-langpack-wo = "locale-base-wo-sn (= 2.28) virtual-locale-wo (= 2.28) virtual-locale-wo-sn (= 2.28)"
RDEPENDS_glibc-langpack-wo = "glibc glibc-common"
RPROVIDES_glibc-langpack-xh = "locale-base-xh-za (= 2.28) locale-base-xh-za.utf8 (= 2.28) virtual-locale-xh (= 2.28) virtual-locale-xh (= 2.28) virtual-locale-xh-za (= 2.28) virtual-locale-xh-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-xh = "glibc glibc-common"
RPROVIDES_glibc-langpack-yi = "locale-base-yi-us (= 2.28) locale-base-yi-us.utf8 (= 2.28) virtual-locale-yi (= 2.28) virtual-locale-yi (= 2.28) virtual-locale-yi-us (= 2.28) virtual-locale-yi-us.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-yi = "glibc glibc-common"
RPROVIDES_glibc-langpack-yo = "locale-base-yo-ng (= 2.28) virtual-locale-yo (= 2.28) virtual-locale-yo-ng (= 2.28)"
RDEPENDS_glibc-langpack-yo = "glibc glibc-common"
RPROVIDES_glibc-langpack-yue = "locale-base-yue-hk (= 2.28) virtual-locale-yue (= 2.28) virtual-locale-yue-hk (= 2.28)"
RDEPENDS_glibc-langpack-yue = "glibc glibc-common"
RPROVIDES_glibc-langpack-yuw = "locale-base-yuw-pg (= 2.28) virtual-locale-yuw (= 2.28) virtual-locale-yuw-pg (= 2.28)"
RDEPENDS_glibc-langpack-yuw = "glibc glibc-common"
RPROVIDES_glibc-langpack-zh = "locale-base-zh-cn (= 2.28) locale-base-zh-cn.gb18030 (= 2.28) locale-base-zh-cn.gbk (= 2.28) locale-base-zh-cn.utf8 (= 2.28) locale-base-zh-hk (= 2.28) locale-base-zh-hk.utf8 (= 2.28) locale-base-zh-sg (= 2.28) locale-base-zh-sg.gbk (= 2.28) locale-base-zh-sg.utf8 (= 2.28) locale-base-zh-tw (= 2.28) locale-base-zh-tw.euctw (= 2.28) locale-base-zh-tw.utf8 (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh-cn (= 2.28) virtual-locale-zh-cn.gb18030 (= 2.28) virtual-locale-zh-cn.gbk (= 2.28) virtual-locale-zh-cn.utf8 (= 2.28) virtual-locale-zh-hk (= 2.28) virtual-locale-zh-hk.utf8 (= 2.28) virtual-locale-zh-sg (= 2.28) virtual-locale-zh-sg.gbk (= 2.28) virtual-locale-zh-sg.utf8 (= 2.28) virtual-locale-zh-tw (= 2.28) virtual-locale-zh-tw.euctw (= 2.28) virtual-locale-zh-tw.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-zh = "glibc glibc-common"
RPROVIDES_glibc-langpack-zu = "locale-base-zu-za (= 2.28) locale-base-zu-za.utf8 (= 2.28) virtual-locale-zu (= 2.28) virtual-locale-zu (= 2.28) virtual-locale-zu-za (= 2.28) virtual-locale-zu-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-zu = "glibc glibc-common"
RDEPENDS_glibc-locale-source = "glibc glibc-common"
RDEPENDS_glibc-minimal-langpack = "glibc glibc-common"
RPM_SONAME_REQ_glibc-nss-devel = "libnss_compat.so.2 libnss_db.so.2 libnss_dns.so.2 libnss_files.so.2 libnss_hesiod.so.2"
RPROVIDES_glibc-nss-devel = "glibc-nss-dev (= 2.28)"
RDEPENDS_glibc-nss-devel = "glibc nss_db nss_hesiod"
RDEPENDS_glibc-static = "glibc-devel libxcrypt-static"
RPM_SONAME_PROV_libnsl = "libnsl.so.1"
RPM_SONAME_REQ_libnsl = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libnsl = "glibc"
RPM_SONAME_REQ_nscd = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcap.so.2 libpthread.so.0 libselinux.so.1"
RDEPENDS_nscd = "audit-libs bash coreutils glibc libcap libselinux shadow-utils systemd"
RPM_SONAME_PROV_nss_db = "libnss_db.so.2"
RPM_SONAME_REQ_nss_db = "ld-linux-aarch64.so.1 libc.so.6 libnss_files.so.2"
RDEPENDS_nss_db = "glibc"
RPM_SONAME_PROV_nss_hesiod = "libnss_hesiod.so.2"
RPM_SONAME_REQ_nss_hesiod = "ld-linux-aarch64.so.1 libc.so.6 libnss_files.so.2 libresolv.so.2"
RDEPENDS_nss_hesiod = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/compat-libpthread-nonshared-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-all-langpacks-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-common-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-devel-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-headers-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-aa-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-af-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-agr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ak-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-am-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-an-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-anp-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ar-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-as-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ast-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ayc-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-az-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-be-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bem-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ber-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bg-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bhb-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bho-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bo-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-br-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-brx-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-bs-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-byn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ca-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ce-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-chr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-cmn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-crh-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-cs-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-csb-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-cv-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-cy-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-da-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-de-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-doi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-dsb-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-dv-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-dz-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-el-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-en-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-eo-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-es-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-et-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-eu-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fa-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ff-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fil-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fo-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fur-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-fy-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ga-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-gd-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-gez-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-gl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-gu-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-gv-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ha-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hak-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-he-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hif-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hne-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hsb-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ht-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hu-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-hy-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ia-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-id-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ig-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ik-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-is-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-it-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-iu-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ja-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ka-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-kab-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-kk-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-kl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-km-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-kn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ko-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-kok-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ks-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ku-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-kw-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ky-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lb-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lg-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-li-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lij-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ln-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lo-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lt-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lv-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lzh-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mag-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mai-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mfe-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mg-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mhr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-miq-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mjw-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mk-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ml-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mni-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ms-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mt-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-my-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nan-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nb-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nds-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ne-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nhn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-niu-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nso-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-oc-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-om-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-or-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-os-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-pa-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-pap-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-pl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ps-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-pt-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-quz-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-raj-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ro-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ru-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-rw-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sa-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sah-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sat-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sc-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sd-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-se-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sgs-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-shn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-shs-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-si-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sid-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sk-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sm-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-so-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sq-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ss-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-st-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sv-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-sw-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-szl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ta-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tcy-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-te-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tg-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-th-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-the-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ti-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tig-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tk-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tn-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-to-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tpi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tr-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ts-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tt-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ug-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-uk-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-unm-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ur-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-uz-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ve-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-vi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-wa-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-wae-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-wal-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-wo-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-xh-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-yi-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-yo-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-yue-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-yuw-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-zh-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-zu-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-locale-source-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-minimal-langpack-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnsl-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nscd-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nss_db-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glibc-nss-devel-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glibc-static-2.28-101.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/nss_hesiod-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[compat-libpthread-nonshared.sha256sum] = "7bb7074460b238901d28e7ea6236260bb9695f535cc616663e51cf829fa12818"
SRC_URI[glibc.sha256sum] = "85e90447a64a0c04ea9177966f38bd350f4a80ae416d8e63e479d46e7cc910ff"
SRC_URI[glibc-all-langpacks.sha256sum] = "200b987c8647d9cc7e90c0ec562518b6e78ecbdabaf75c8856faad861c3e8842"
SRC_URI[glibc-common.sha256sum] = "c607c327fdd624416fd313a61d2b878b4b3436894f54a129772269e699cf6622"
SRC_URI[glibc-devel.sha256sum] = "9e78fb5e19de0dd91e3112f0044e004a455662b8f8d03ca9b9f1e9b9b02d3cfe"
SRC_URI[glibc-headers.sha256sum] = "5fedeeb8d4e1bff7c903260d2322f188f5d2f41ef3f273cf8b0f16b4703bc7eb"
SRC_URI[glibc-langpack-aa.sha256sum] = "f43a065c8849c731fbad46bb46b12d33857143a0e7034b5b1ecac13c8340c7a8"
SRC_URI[glibc-langpack-af.sha256sum] = "3d3cdec46b07f6fd515b4a41efaf9bdaba8a270bd5f9c624c8ad1d7a21afc3c7"
SRC_URI[glibc-langpack-agr.sha256sum] = "1a1f718e9df216b030e53761328d0b43bb3775bc7d60e10bbd0cb6562446af28"
SRC_URI[glibc-langpack-ak.sha256sum] = "bfde58b371619b47de269c2a13fd71c7cef6655084202f81217dc1d6adc75112"
SRC_URI[glibc-langpack-am.sha256sum] = "8060ce8e61765f97a2acaa5495a4dcf339974120443a3f1374a0dceb9863ab47"
SRC_URI[glibc-langpack-an.sha256sum] = "bedc158124d100cc46387c8189452489c7c7079cdcfdbc9f220134ca655a77c3"
SRC_URI[glibc-langpack-anp.sha256sum] = "22967e2202e7b679f9e1c357b82553e8b435d616cd6550dfa8f5c52c9e60edb6"
SRC_URI[glibc-langpack-ar.sha256sum] = "fad322285286be77b0404b6f1b55a434412dc8605fd22cb56c3a43a1488b6a45"
SRC_URI[glibc-langpack-as.sha256sum] = "c4488b7b0f5c68965f0645d879835297b9cf156c5107e697843d07e839acb7be"
SRC_URI[glibc-langpack-ast.sha256sum] = "6040df50644a5afcd960b9b9bf47b288668abc9ba203303ba7791258b736eca2"
SRC_URI[glibc-langpack-ayc.sha256sum] = "868895bf6faf219cde0f302807970064a7f79f747cc1255e356b04a3e6e14790"
SRC_URI[glibc-langpack-az.sha256sum] = "2979a2ff395527825a05e2546edd3d9d658b050c8d5af2df1a2a9c54c6bf63d7"
SRC_URI[glibc-langpack-be.sha256sum] = "1f1378f90f75df27cd301986e84aec2be6cb41366e43b8ed84e42fa8fb93d578"
SRC_URI[glibc-langpack-bem.sha256sum] = "05e3308537e4562b446921020e51f7e350eda077e5347d04d6e66f1ef3bf5b8b"
SRC_URI[glibc-langpack-ber.sha256sum] = "780a416009058cbf42c943ffe8b4ca7cffcd31d20e762a4c34a3c3591e6817be"
SRC_URI[glibc-langpack-bg.sha256sum] = "78e7a8efb78e18c7a7635013552fb219b4f215a3a4e18d0f22a0f33712f4dc35"
SRC_URI[glibc-langpack-bhb.sha256sum] = "db842b9360bbd87190835a28eddef293b8e26b6a40143f36fa29bffdab785b6a"
SRC_URI[glibc-langpack-bho.sha256sum] = "0cef310a1007bb7cdb3073d66610b8202d325d35218e9e92bfbdc15a9f803b8c"
SRC_URI[glibc-langpack-bi.sha256sum] = "d7ef1a02e739393e27273ed692c9036c4e7ce73f218795a350d2df36210d425c"
SRC_URI[glibc-langpack-bn.sha256sum] = "69e5938750da82b3267ce8bfa07d631c74c1e0c20c6e136a9264fe8b7f6f5b6d"
SRC_URI[glibc-langpack-bo.sha256sum] = "c6c0a80585c1fcfc71ec6dd4d5b3dc285a18d43bb3b0f93c535507d60723e2ed"
SRC_URI[glibc-langpack-br.sha256sum] = "7e420bb6a30ac9428edd0f0bb158af0b3f6f24943bff229f0cc66629350c326d"
SRC_URI[glibc-langpack-brx.sha256sum] = "94d33e3d7cd92dd81b74f7cb7d450eee3e62743be8afd69a8bec63fffc76f329"
SRC_URI[glibc-langpack-bs.sha256sum] = "d0a6dfff60c99298012b039de212590ea906cb6c113436376ebcc25c68c8969e"
SRC_URI[glibc-langpack-byn.sha256sum] = "4846f1cd1b246a26239d4e01287e01734724fa15abd8b4c7155a2b9221fa2726"
SRC_URI[glibc-langpack-ca.sha256sum] = "78121105d23ef728d694a513d3060b9901321c5983bc304c5f001397975054d1"
SRC_URI[glibc-langpack-ce.sha256sum] = "be5f8618fc77ce89c5a4f0c7bca42372e8ed63f42a6a49fcb853eb3dff85ed15"
SRC_URI[glibc-langpack-chr.sha256sum] = "75cc0d7e08561a3171c6aa6a055c46567807ad2942b977e0c0312e2e5f6620d2"
SRC_URI[glibc-langpack-cmn.sha256sum] = "5213d7e21c433fb9c54d5f4d8bd32e5da8cc2e4b87b4f766e19fcf572fcbe9e2"
SRC_URI[glibc-langpack-crh.sha256sum] = "c4a9be288db623ff9a5cedb8717bf59045814aaa2f0068fcfdec613ff1fe2134"
SRC_URI[glibc-langpack-cs.sha256sum] = "ac100153b77c0a553aac94f03bc961ce6ec072e8a977ef6cbbab661fb718c352"
SRC_URI[glibc-langpack-csb.sha256sum] = "1ac9174402896699902cc7aa01b8283ae9cefc33efe64f400c61ab55b1e2792d"
SRC_URI[glibc-langpack-cv.sha256sum] = "cd41e6147fb8b468afb26930e07ef9b242b2f5cc6294adb65d1d89d2a35150ed"
SRC_URI[glibc-langpack-cy.sha256sum] = "3ff14b8da7b350903e2eaaf62f9f69d1716da5fbb73d8f930f20e1ea38220098"
SRC_URI[glibc-langpack-da.sha256sum] = "8a59e59943c4c93bf8105d21f776130d0fe70479c70371fcd0b5722161d8a133"
SRC_URI[glibc-langpack-de.sha256sum] = "76737c0c9d6f0ad2445a87077cd4b209286eec8ec571105cd65372cb5c1dc5af"
SRC_URI[glibc-langpack-doi.sha256sum] = "675b8a85cbf560873dfcc883bbc6c1d743b9b9e88dc2ce7d39d059acfff98d77"
SRC_URI[glibc-langpack-dsb.sha256sum] = "ac2fd28e4d82020f42ead9f524627ec368f580773abc3243f9cb0cbafb5d098f"
SRC_URI[glibc-langpack-dv.sha256sum] = "536d8bf1051f33909f11a35c3b2bc044eb1d6914e252bb5977dcf386855fb444"
SRC_URI[glibc-langpack-dz.sha256sum] = "a094b7d0a079651118537b94cda968f0f93413da32712941aaf6864a930e34fc"
SRC_URI[glibc-langpack-el.sha256sum] = "f34adb5232d6117b788846e4910931ddf00c6444960298fb31d01038aeeddf54"
SRC_URI[glibc-langpack-en.sha256sum] = "0ad4cc01893d5eee35bba8715b90b261af9f8101454e6cc5d474394d322c534b"
SRC_URI[glibc-langpack-eo.sha256sum] = "30be6645f62876e6a2f96cfe11c775e8f0eefe8d98b778ac9d40b6ac064c2e78"
SRC_URI[glibc-langpack-es.sha256sum] = "e90b1fdf14a53b695a1ddff72639c622f1e1e0dce536f1e4ee0084ff791ed720"
SRC_URI[glibc-langpack-et.sha256sum] = "df176e7c749db512610783560017f5671865c196a457eaa55a294e28f9939ebb"
SRC_URI[glibc-langpack-eu.sha256sum] = "edfa61b32e00efe9bc187503ec4670001143bd202be18c947ee0ce08ad6c06e9"
SRC_URI[glibc-langpack-fa.sha256sum] = "7ef21dfabe303d41986c6596bcc7cbb649ba28c11e53073470f03aaaa357651e"
SRC_URI[glibc-langpack-ff.sha256sum] = "f40d9487f06c0f7bc5bf9fe0bcd06fc83b792d3edc74044d80f4180305c4d5cd"
SRC_URI[glibc-langpack-fi.sha256sum] = "8ec2c6638f14f51fa99a66d8ff8952e6ef35acbc41831651d360cdf75cf9e8d1"
SRC_URI[glibc-langpack-fil.sha256sum] = "093d9a162dcd78eaceb9e4ad2585660b194cdd2b03bb32022d3b043e6e21a1ea"
SRC_URI[glibc-langpack-fo.sha256sum] = "f022bbab66adb2d8562bcb1fabf6b230e25110b028098828fb6407c100c71fe1"
SRC_URI[glibc-langpack-fr.sha256sum] = "957698a86ff81529b9add4c1378f508b1db1b449735cc67d8bf0a78bd5dc1341"
SRC_URI[glibc-langpack-fur.sha256sum] = "e5fbcd615127ee5fb9431d042608bb3e3721bcd204deda62cb3d63f2679f1f38"
SRC_URI[glibc-langpack-fy.sha256sum] = "30d5cc10f2f6b84a0ac4d32e037564a2e2cb00c9899b75a0039f8121412ba11e"
SRC_URI[glibc-langpack-ga.sha256sum] = "c2d9ed39ba98a89bf6f32709ad01ab0a2373788343215cae532ec24910253986"
SRC_URI[glibc-langpack-gd.sha256sum] = "0a048fca55a57eb8bb9141320f1fa09e935a0c55f31f5e2a87f43a8e68abdc8e"
SRC_URI[glibc-langpack-gez.sha256sum] = "e0fb2eb8ebb948f03899273004a53b851edbdcbe39bf72449eeb895c15257957"
SRC_URI[glibc-langpack-gl.sha256sum] = "da547ffb1e42952ee51e8d0b47c748b6f6018c3295ef564e143b7eab6f39c0a3"
SRC_URI[glibc-langpack-gu.sha256sum] = "b1482e37b35a3870ed8722c9aabecc6d48369ad9448e975eb08f3f6a94f7df7b"
SRC_URI[glibc-langpack-gv.sha256sum] = "312528d25dde582403bf69ca40a605e7953e8a830ff188372b2eb84e088daa19"
SRC_URI[glibc-langpack-ha.sha256sum] = "21ceb03489c1d7087436f0e356715b3c86cfe3f12c442d132b41996d10b332d0"
SRC_URI[glibc-langpack-hak.sha256sum] = "760fe55ea0974b059760876ce8b0d070d77d6d036592e62ee2255effc27959fe"
SRC_URI[glibc-langpack-he.sha256sum] = "b741b82d2e23afa162f943c5d8fc4762f0a053800ed730fd085550d6a2f44bb5"
SRC_URI[glibc-langpack-hi.sha256sum] = "3730646e3807de4f17ab22b4efea184245982195d50742c52dfb8eb7be701b0a"
SRC_URI[glibc-langpack-hif.sha256sum] = "c121911dc8f16d9cde55bc1fde105f1129273663cc9f462238c5af50072c5293"
SRC_URI[glibc-langpack-hne.sha256sum] = "73e3a71c21a0dffb31f9c5fcc9c94a767ff4fe37e3dcee1358b6c357f0035936"
SRC_URI[glibc-langpack-hr.sha256sum] = "e04e87e7789375c2474b4a006220b2a1406bc62d91118ada72ba36a681af8749"
SRC_URI[glibc-langpack-hsb.sha256sum] = "bb141b5e1678302783e76af524bf24ae2270281bdc2d194055f5ec168a17de6c"
SRC_URI[glibc-langpack-ht.sha256sum] = "4f173552c2fa56b7a4a19e8083b2d39909db08e6a64809591c74cc51ac96061d"
SRC_URI[glibc-langpack-hu.sha256sum] = "ce2bf03f84558011c93ad3e2705d66162e8026e868c1bc0c536c2e008999424c"
SRC_URI[glibc-langpack-hy.sha256sum] = "fa2ecadbf156667d719286ffc6e96fe24e44e728c86bb20834007654a936d2f5"
SRC_URI[glibc-langpack-ia.sha256sum] = "c6a0bc21c646a0eab74f207026ab2d49d4ce96cb3ee0ffbf3691af5b947d264e"
SRC_URI[glibc-langpack-id.sha256sum] = "988262ca88b85792a3a35b7591ec618493bfba44d47ab5c8417915e6eb2b98f7"
SRC_URI[glibc-langpack-ig.sha256sum] = "fdcc20b04b1736a077099dc62d5db60d247e97fdd325a7b13d3baaf6ea26869a"
SRC_URI[glibc-langpack-ik.sha256sum] = "268235653c08a2781f823525334142476803bf689b37f8e608a2a8147ea3d377"
SRC_URI[glibc-langpack-is.sha256sum] = "b7990c43d5b4c380473a52ba0db0bc012222549d3bbb5116a7016092f6eda666"
SRC_URI[glibc-langpack-it.sha256sum] = "ccc1c8afd1576ef82a60330695b166821f5a7396ede74c72e5eb4beeb0960c97"
SRC_URI[glibc-langpack-iu.sha256sum] = "1628503faed06bed707d35a70b230f546531035971b870dc433fe4a873616f3f"
SRC_URI[glibc-langpack-ja.sha256sum] = "3c5b31d5a756d31c3a55fdcd01f23ca897126cda52fadd714cc9a64fecd29bbd"
SRC_URI[glibc-langpack-ka.sha256sum] = "3ea3932cda4dc61220277ca315de4bb255ddf62bccf6adab7db672b6b9c589a7"
SRC_URI[glibc-langpack-kab.sha256sum] = "9f5a08588eb70f7dc8638d9c1b03ea31d93714ec6a75a12eba109b4ed2f092f3"
SRC_URI[glibc-langpack-kk.sha256sum] = "fb89f86bbbb61c1abaaa71ff9f93a3fd44777cb9252b750df9337707878c26fe"
SRC_URI[glibc-langpack-kl.sha256sum] = "40c766d8cf71ebb06ea789aac928464d85f92e10adac62aac66a9850d151b3f2"
SRC_URI[glibc-langpack-km.sha256sum] = "0b6f495b535c92fb897e6f63da2fd1ef65aaf9525611360cfa6a17a137d28d77"
SRC_URI[glibc-langpack-kn.sha256sum] = "a33ce584855291080703223007ad492934ba6cd21ea04dd9bbce47f482ee3f08"
SRC_URI[glibc-langpack-ko.sha256sum] = "05807fd222f19622861ccca1b5f7913b31ddb02876722a983714d988220cd5cd"
SRC_URI[glibc-langpack-kok.sha256sum] = "916c3a02f39c231dfb598db1f1d4cb13e700ced5c2965ffb5b92a89fc9e472bf"
SRC_URI[glibc-langpack-ks.sha256sum] = "1bc3265a78ece108f64b1bcac6d18875664dc757c0d150aee9f9393f0502b50a"
SRC_URI[glibc-langpack-ku.sha256sum] = "c67656b6cd068bfa5d98142267590f90e0835060408b5ed3e37fbb179fdaeacb"
SRC_URI[glibc-langpack-kw.sha256sum] = "f058e0c19cdc072d876652793eb9f8577a8969e5fda9ec4cc3a498f1a87990b1"
SRC_URI[glibc-langpack-ky.sha256sum] = "ddf220c850412ca74059607ba87ba40860ea0ff135e5c0d43fa222e8bb6def14"
SRC_URI[glibc-langpack-lb.sha256sum] = "5487e22d664268f71dc302c33294b1c38e1838e5e9ee6d26515ed28f706b8aaf"
SRC_URI[glibc-langpack-lg.sha256sum] = "14fee9b5d674b4686828d31393deee83d60fbb2e6765513b69a9ba44ea7c7b17"
SRC_URI[glibc-langpack-li.sha256sum] = "f754106142545f269ee66d3308d6ad1159b3e468243725b251ce21d2f4f2291f"
SRC_URI[glibc-langpack-lij.sha256sum] = "06d2254535e582234c3cb18b9cd89d9febefa1e9a163abb78a94b210536c30c9"
SRC_URI[glibc-langpack-ln.sha256sum] = "dafc6e812f4af3c904745fa734451bd36abf7e03dea279bdbe5b66406622d40e"
SRC_URI[glibc-langpack-lo.sha256sum] = "4dd908d4ebac1ec75ecf97622466c30a494d78abd70f767a76f144fd2bbf022e"
SRC_URI[glibc-langpack-lt.sha256sum] = "51f81cb6616990f22da7acf0a6fa4485d4e581fccd9123302eb81be7c661c563"
SRC_URI[glibc-langpack-lv.sha256sum] = "b1c1e59aa936a2287f0c15f4713e60f9a7a53fe11f27043ce31a1e2dd2b6bcb0"
SRC_URI[glibc-langpack-lzh.sha256sum] = "81e6c5a0d387d9bf2cab83583141b98f4abb174e1592b1f6983c67936c3d7748"
SRC_URI[glibc-langpack-mag.sha256sum] = "26f8031ed0811e9501bac3cc1ee7f122b8ab4bfef7a83a3ae5b9c9e53bb2cb65"
SRC_URI[glibc-langpack-mai.sha256sum] = "241187db4a1c159a0664b284a03e2e70170af0175eb5c0fad8149d01de1ad43d"
SRC_URI[glibc-langpack-mfe.sha256sum] = "d285f9ce0d5b51c34a21ce08165ffc3e87682cd0e79d2a0943bec44e25de725e"
SRC_URI[glibc-langpack-mg.sha256sum] = "46b5148dee198daa256057c739e096be02b2db0cdcb9fd394580526f618400bc"
SRC_URI[glibc-langpack-mhr.sha256sum] = "a771f33a294533d7af83060be33a7b2feda64ade3eed9a6e6d400ee64314f848"
SRC_URI[glibc-langpack-mi.sha256sum] = "82aa6f54f369263fa46258c0cdf03b1f42b9e5819926d826b255c31d034cdf88"
SRC_URI[glibc-langpack-miq.sha256sum] = "70c3f7ee511fdff439040aacac2a998f82b6410028d1497004f3dae9b77c5055"
SRC_URI[glibc-langpack-mjw.sha256sum] = "f49f55143fcce73adce3ba4443951eab8d7e95f31a1d0a69ca72de281f86310c"
SRC_URI[glibc-langpack-mk.sha256sum] = "f544d65af4523342ed597ae57df24879b95e56dbae77387af9b8a6e97c8c13c9"
SRC_URI[glibc-langpack-ml.sha256sum] = "30262caf64224d1d2df4a7921293e516ce6b94b3e87bae169124fdcc24542b45"
SRC_URI[glibc-langpack-mn.sha256sum] = "f7d78d31788477d6586aa54ec63d65c60e103eff97d5fe2636cbf3ff780717f6"
SRC_URI[glibc-langpack-mni.sha256sum] = "77457f91d0bfe2a8aed68d01c5b55e867ff78f555bff9c1a4fc4dad1d6527127"
SRC_URI[glibc-langpack-mr.sha256sum] = "a89ed969ecd4a2a97efe4cb8c288e91cb334acb7cfd3d1092b27c465739f8863"
SRC_URI[glibc-langpack-ms.sha256sum] = "539a5a600c840a677bb1be6c68b9d5a2b98a495782c1b812d8cba7b71a541d0b"
SRC_URI[glibc-langpack-mt.sha256sum] = "1d24b913e5a44121c4a18497526c5672df001692833499e0758ba588a8403210"
SRC_URI[glibc-langpack-my.sha256sum] = "9111793682b9f04e8ecfcc16e98b4d0f44939214845f555f055abc57b21ab100"
SRC_URI[glibc-langpack-nan.sha256sum] = "1024840132cf425404e949b8c9a5db26e4f8b4537c0c441d0f615e2505060bb9"
SRC_URI[glibc-langpack-nb.sha256sum] = "e6076e6f550ea7c30b43e4e7518c2767eddf158e5e4e7bcf51f70153c3589594"
SRC_URI[glibc-langpack-nds.sha256sum] = "e7940b357d1aa83c9ceb197b73dc6693c0832984a2e4bd30e0dcddbd335d608c"
SRC_URI[glibc-langpack-ne.sha256sum] = "0eb85b352465a52a67e1c282ac308821c4a406b1041878e9408196c6fc1434e8"
SRC_URI[glibc-langpack-nhn.sha256sum] = "710efffc84afe256b87c814ae66c4504e9d8492bf128b255d726c457c1c542cf"
SRC_URI[glibc-langpack-niu.sha256sum] = "b9d5cf6f006a7e611d09098346e0b13b82b9d25bd47cdb8d96ca1ef6f0e481e8"
SRC_URI[glibc-langpack-nl.sha256sum] = "f48d85a462686212285df65c7124302aede1e931269d62306b5925fcdded39a2"
SRC_URI[glibc-langpack-nn.sha256sum] = "f7a14b9abd9e682c67548e8e1137ee5962d81f9bb20dce6d8fe4deafce5714ea"
SRC_URI[glibc-langpack-nr.sha256sum] = "cd1c7fde985f9789fc0a1ac80395ea9513ac76cad156d756636e4815084170b3"
SRC_URI[glibc-langpack-nso.sha256sum] = "6515789f4fc1f247b701de9209d3a3b43296cc9af84918b9801e1ed83303cf39"
SRC_URI[glibc-langpack-oc.sha256sum] = "a52e9df17f1b8c5c3288976d70b4a3687b465a002d5fa61d904e57adafbf0a33"
SRC_URI[glibc-langpack-om.sha256sum] = "0ae6696036e280fc39eafea9e5072abdcfd687ea479268b8e876fb98eba46579"
SRC_URI[glibc-langpack-or.sha256sum] = "f79c499eb43e4369c57aa2b107c4f5e548b04abdc49ff96e89046bb8e555e73f"
SRC_URI[glibc-langpack-os.sha256sum] = "bd336fb8207aae8f23db9c028703e3ea9e7b4fbe4e278239f4834274825f2dfb"
SRC_URI[glibc-langpack-pa.sha256sum] = "2ee776c5b3ddb796cf2ad732b83f5bc173c64b484bcb49719d2d86e47a61c48d"
SRC_URI[glibc-langpack-pap.sha256sum] = "f0ca11af8ff0e6904580dcf1e57626cda7f3bb04f8520cba6dd2a5cc1336eaa3"
SRC_URI[glibc-langpack-pl.sha256sum] = "2e734b036ebd0c2c45ff619e139679c9376396c24eeb7da2ef25afca3152ec02"
SRC_URI[glibc-langpack-ps.sha256sum] = "476654c8ea567af1ca4aa80f56365c50fc3e0c73bc2603077fdc6432c77dc04f"
SRC_URI[glibc-langpack-pt.sha256sum] = "7344e284deb957e6b0f7d66a38679a9d2b85751255a69d3af77cdbcad545adc8"
SRC_URI[glibc-langpack-quz.sha256sum] = "f5417f6bf750f1f88a0319243741e0bfebefe461a8d962eaee6d77cfa6548611"
SRC_URI[glibc-langpack-raj.sha256sum] = "7b0a5012ed9a41cba20edd8610e3324460cb5d88a78f5eb30765adc11286f5b4"
SRC_URI[glibc-langpack-ro.sha256sum] = "03cd136342db24e22f970872e4b7ea9579ca87bd12a792c1aa61ebffc085f2eb"
SRC_URI[glibc-langpack-ru.sha256sum] = "5a6c822e7720a1837a48a5378ee6c37d18e03ae67c371c5fc3ed11b4bd0ae5cf"
SRC_URI[glibc-langpack-rw.sha256sum] = "b40f7f7cf4aae63b4b65c0cb7f65d71a7d3db5282a187f09b03607b8be1fb4ef"
SRC_URI[glibc-langpack-sa.sha256sum] = "228ed267f09d542245049b611e29f0bfac8f1eb026450da378b3fae4d0f2d708"
SRC_URI[glibc-langpack-sah.sha256sum] = "f46d3ed35c4619e30ccf1cb837a8e4a1ae1ea610fc7dd3db3db1a1df16332659"
SRC_URI[glibc-langpack-sat.sha256sum] = "1b30d67b8f26817617d88013251502efb785ef299f8ffadcf8fe6d82b4f34e1e"
SRC_URI[glibc-langpack-sc.sha256sum] = "56c3b7ff047c1356fdef1feb782612eeb78c07e92bc42cf1eb6f0efd315b13fe"
SRC_URI[glibc-langpack-sd.sha256sum] = "1357561c5580aebf437880f1f10b0cc55665b0c8291ac14beab93a5331b16622"
SRC_URI[glibc-langpack-se.sha256sum] = "e7cf81ca87edca0d4ec511e95a8e1a1a6c3e9ed9ddb000dc919b1ed8838b033a"
SRC_URI[glibc-langpack-sgs.sha256sum] = "c0f3a98365de4f95739ca4f6f39ad0e845be760fea1913d0ed71b2929b2e07a3"
SRC_URI[glibc-langpack-shn.sha256sum] = "f67c7a550cf1e020e91b4a4e7ed0b2aba26c98b14fbd704172b19727a97bfc7f"
SRC_URI[glibc-langpack-shs.sha256sum] = "b6bbcf3ba5beca8e3cbe192572b08bcd8415a077f8ba88da939694ac59c6396e"
SRC_URI[glibc-langpack-si.sha256sum] = "ad4c3793a0520b9634d565578e2bd2459825c2095177572bd760b79673d38091"
SRC_URI[glibc-langpack-sid.sha256sum] = "f0659ea7bd287b11330fb3786b8035b29dd399717bd9d88211c31e713befd2b0"
SRC_URI[glibc-langpack-sk.sha256sum] = "81aa8fc997f7745bdab6b49429ec77d3cbc4d73ff2765dc4abae31264551c896"
SRC_URI[glibc-langpack-sl.sha256sum] = "18298a520916acc437148bff4bdcb06c505c91598268b87f3c1495d84611cc9e"
SRC_URI[glibc-langpack-sm.sha256sum] = "4f255329fb89d18d4fb05f12dd2d7c96dad8f16c57b0cac1ada0ee1413ecd7d0"
SRC_URI[glibc-langpack-so.sha256sum] = "96968f7939a17036899b893c4cbfc9b72148331d7667a54db0b8094dda873ef5"
SRC_URI[glibc-langpack-sq.sha256sum] = "74e15d08248764c92d0a13415f3bb1d5aa42bb57d0e8457f77a687690feeb046"
SRC_URI[glibc-langpack-sr.sha256sum] = "524c7cce9f73e74276c61e3ac1fa2a8b02f60867f3ea31060c6d21875a2c032d"
SRC_URI[glibc-langpack-ss.sha256sum] = "1a7d6f7ef9290d2570866ff19d0cc233144b378182219e817bcbd3e25180a62a"
SRC_URI[glibc-langpack-st.sha256sum] = "d160b54171421e6031c3554d6ed138730762a0f8cad9f6843228d152448bd7ff"
SRC_URI[glibc-langpack-sv.sha256sum] = "b74e5e08b24209dbdef3e304e4fea96c7585b6630e513e703a4ae14e2c02b8c2"
SRC_URI[glibc-langpack-sw.sha256sum] = "00619e0bd631244316e83fb9df8bb8b7b3bf2205a59c9ef083c24c3cef61e63a"
SRC_URI[glibc-langpack-szl.sha256sum] = "e33153c7d737d240fb56e1a16058e515a0eacdd7f4cdfd86d6997506be4b5989"
SRC_URI[glibc-langpack-ta.sha256sum] = "f291f3dd3b53e71bc18dec1644a2e240b9733f0b78f8998374a88ffb1d921855"
SRC_URI[glibc-langpack-tcy.sha256sum] = "3a0d7ef3849ea42dc77ff8bdc60ddcd96387479f680f7a59fdb7b62ed462fb2e"
SRC_URI[glibc-langpack-te.sha256sum] = "f102869cf71e80b31f6c2adcd45758881e834c01c67f71cd29b61427e1bf9dab"
SRC_URI[glibc-langpack-tg.sha256sum] = "917039155f09ea9b434f2e7a5ce44674bd60d351d3283a2b9b850704b3f7fc0e"
SRC_URI[glibc-langpack-th.sha256sum] = "5ebf5a099346a585aebb46021d2d68cb4c3c188748a8f0aecb3e9cb02255297f"
SRC_URI[glibc-langpack-the.sha256sum] = "16928adad9a0adced89f0cb310aaa8a2fc1812c8267d11d4f7551ab8905c79fd"
SRC_URI[glibc-langpack-ti.sha256sum] = "ee80d13572ab306885e86033f2a123c4847bf89a101c9df5b169d2f74465e24e"
SRC_URI[glibc-langpack-tig.sha256sum] = "a2edb3192b2ded103564fa3a57a4c8f5e40117397dc91cc67b1b18cb14c46520"
SRC_URI[glibc-langpack-tk.sha256sum] = "7a9b892533df35e3519b7d33b4638f210f4e1ad83ef03ea6fddbdf0eda80940e"
SRC_URI[glibc-langpack-tl.sha256sum] = "28444bc81621e015ad08af3d38d5d9c2f329ee2f1a1f25ad6a91640331c012e0"
SRC_URI[glibc-langpack-tn.sha256sum] = "08ce700f4851463ace2d59e2689f264c11a3b53fe814aae508af423e10ace494"
SRC_URI[glibc-langpack-to.sha256sum] = "17cafae0c7154173b6c08603a4030db51981bb91d5aeb78ac964da1923b2751f"
SRC_URI[glibc-langpack-tpi.sha256sum] = "4fcedb5bbc2a1ceddc7e0937442477f8e6999b4c0b6c70299ac1c0e0854104f3"
SRC_URI[glibc-langpack-tr.sha256sum] = "e4b22ab383979c521ee078828a337db8bb6c8ba04c18dea78d417d1c371e0939"
SRC_URI[glibc-langpack-ts.sha256sum] = "0a717cfbe84c631d7e56588a6b174a74d37e92e5652c60af0b3319bdf71fbd56"
SRC_URI[glibc-langpack-tt.sha256sum] = "130da571de4b9b0c518a61ecd6082871d514531e6720bbca2070ce2a43f8eb69"
SRC_URI[glibc-langpack-ug.sha256sum] = "ed1c3cff011efeaf3d3df1cbf28a2c427666684b6addc3cb53adcf4b93891430"
SRC_URI[glibc-langpack-uk.sha256sum] = "ae5e8c7360f34dfb1bd30993105c43afde4f729cf5bafdacb87bbdf6e5e7881d"
SRC_URI[glibc-langpack-unm.sha256sum] = "a47a807189eb0f04e790f75781d8625c36e4f08a6eed11ec662ac870a882814e"
SRC_URI[glibc-langpack-ur.sha256sum] = "3346094e47e6fc4c465c257bdeb72db0f1273937c0e5687db6335e6cbd6682b5"
SRC_URI[glibc-langpack-uz.sha256sum] = "9d962eb1a2502845673a7d0979a18c09daf4d0e8c9d76c548735c783ee4f247e"
SRC_URI[glibc-langpack-ve.sha256sum] = "0703401339f5e835e81fc9fbadb2ffba460166d92e21f5695c4e56f2d72c15ce"
SRC_URI[glibc-langpack-vi.sha256sum] = "41ea24f14ce7ccb2a029bd54d001acc8dd1c17ce6279a04447033ca4443b8543"
SRC_URI[glibc-langpack-wa.sha256sum] = "6d6357c2a9af97eea0047560bf9dcfbd1f9d197d39596c186f051bc997aecde6"
SRC_URI[glibc-langpack-wae.sha256sum] = "3a77849510610300731b0f68ce08b97f5eaefd25bed530a4cd2b7180435aa046"
SRC_URI[glibc-langpack-wal.sha256sum] = "79c37d03ccfd6679d16e8c8d5e58d84a1d594b0c3385381fe1569b9ab973bb7d"
SRC_URI[glibc-langpack-wo.sha256sum] = "a4c0217046a6850e4d6b6c403d9bd3c0b9d1f39f52989a2d2cc85c840ca3c10c"
SRC_URI[glibc-langpack-xh.sha256sum] = "083667dc5d6de4c59c248f2cb3dbcadf62e51b05b122bd4c0b97745db0a3d01e"
SRC_URI[glibc-langpack-yi.sha256sum] = "39dc383be13b471bbe7fcf82260aa7e04445d19df50b5eebc42eb7d1f50a7e2d"
SRC_URI[glibc-langpack-yo.sha256sum] = "4e82befd4a70b89592bb1349b08bac7869a68c6a5240136d1a285b93ff42567f"
SRC_URI[glibc-langpack-yue.sha256sum] = "dca6bd38dc9b15ff830fb67df2725a5c7feadb6dfd77648109278d2ce8e89a18"
SRC_URI[glibc-langpack-yuw.sha256sum] = "5e1b69917ba46becdda27af17848fa44d11a6519f2ec20fc7fcfe41151e3d0be"
SRC_URI[glibc-langpack-zh.sha256sum] = "bbd1ba30ee525a6f0567437eb537a68cdf64af57531fb35f4cd2de54f45ddb15"
SRC_URI[glibc-langpack-zu.sha256sum] = "b963e627e12d0c176c378e3346468f913877059f96626719d4e4cf1ede429714"
SRC_URI[glibc-locale-source.sha256sum] = "9be7ee88f7a587edc4c122a9b1c7ac4298628169504ed23d086854781fc40b7d"
SRC_URI[glibc-minimal-langpack.sha256sum] = "1a2f2263c4c38c1b3e66e3396d097d681bc20ba4c8f5504243c1737f59f676a8"
SRC_URI[glibc-nss-devel.sha256sum] = "ac5bfb93e2a81dbdb76373bf179c4dd58763fe0f74aac111cc5944d119c0d4fc"
SRC_URI[glibc-static.sha256sum] = "7998903401a99ef3529eac8ed3ace4f7b78ccd46064ce466d98f82c4521ff54c"
SRC_URI[libnsl.sha256sum] = "cb81ff89ffcea100fe75542687e04aadd31b39dd9ecdeadf270674f792a8800d"
SRC_URI[nscd.sha256sum] = "db9f1c9e0c221b0194946df3599c1cc2d41f4bdb9166f29c179ab3273a282030"
SRC_URI[nss_db.sha256sum] = "4872454d48e811742936da40caac237d876e463576064af50e22f3acfb16a118"
SRC_URI[nss_hesiod.sha256sum] = "01ecc40f91a0e1a4a05c9dd79ed2c6d851fa6df199847283c6339ec19ee1f873"
