SUMMARY = "generated recipe based on pygtk2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 pango pkgconfig-native pygobject2 python2-pycairo"
RPM_SONAME_REQ_pygtk2 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_pygtk2 = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 pango platform-python pygobject2 python2-cairo"
RDEPENDS_pygtk2-codegen = "bash"
RPROVIDES_pygtk2-devel = "pygtk2-dev (= 2.24.0)"
RDEPENDS_pygtk2-devel = "gtk2-devel pkgconf-pkg-config pygobject2-devel pygtk2 pygtk2-codegen pygtk2-doc python2-cairo-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygtk2-2.24.0-24.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygtk2-codegen-2.24.0-24.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygtk2-devel-2.24.0-24.module_el8.0.0+36+bb6a76a2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pygtk2-doc-2.24.0-24.module_el8.0.0+36+bb6a76a2.noarch.rpm \
          "

SRC_URI[pygtk2.sha256sum] = "dd16422ede91fb8cc10ad32a6c46e3ad67e92fd5de003f60ee3c743159148594"
SRC_URI[pygtk2-codegen.sha256sum] = "fe957679631dd8593cf47ffc1ad95dca7fbe75254a18e3649f04b5ad1bcf740a"
SRC_URI[pygtk2-devel.sha256sum] = "3cc2521a54a18242768556775424ac148ab5392c58495f3a86109630c3e5e8b3"
SRC_URI[pygtk2-doc.sha256sum] = "edb5dec5a8608aa0712a5f89618b6a1658dac5be3180cee120d24b4fe2e20d62"
