SUMMARY = "generated recipe based on perl-MIME-Base64 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-MIME-Base64 = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-MIME-Base64 = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-MIME-Base64-3.15-396.el8.aarch64.rpm \
          "

SRC_URI[perl-MIME-Base64.sha256sum] = "51fb220966a4691eb8bd02160c6822bcffc0ce99ce20a25fe64e4f8d510cd848"
