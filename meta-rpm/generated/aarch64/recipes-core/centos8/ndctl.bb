SUMMARY = "generated recipe based on ndctl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "json-c keyutils kmod libuuid pkgconfig-native systemd-libs"
RPM_SONAME_REQ_daxctl = "ld-linux-aarch64.so.1 libc.so.6 libdaxctl.so.1 libjson-c.so.4 libuuid.so.1"
RDEPENDS_daxctl = "daxctl-libs glibc json-c libuuid"
RPM_SONAME_REQ_daxctl-devel = "libdaxctl.so.1"
RPROVIDES_daxctl-devel = "daxctl-dev (= 67)"
RDEPENDS_daxctl-devel = "daxctl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_daxctl-libs = "libdaxctl.so.1"
RPM_SONAME_REQ_daxctl-libs = "ld-linux-aarch64.so.1 libc.so.6 libkmod.so.2 libuuid.so.1"
RDEPENDS_daxctl-libs = "glibc kmod-libs libuuid"
RPM_SONAME_REQ_ndctl = "ld-linux-aarch64.so.1 libc.so.6 libdaxctl.so.1 libjson-c.so.4 libkeyutils.so.1 libndctl.so.6 libuuid.so.1"
RDEPENDS_ndctl = "daxctl-libs glibc json-c keyutils-libs libuuid ndctl-libs"
RPM_SONAME_REQ_ndctl-devel = "libndctl.so.6"
RPROVIDES_ndctl-devel = "ndctl-dev (= 67)"
RDEPENDS_ndctl-devel = "ndctl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_ndctl-libs = "libndctl.so.6"
RPM_SONAME_REQ_ndctl-libs = "ld-linux-aarch64.so.1 libc.so.6 libdaxctl.so.1 libkmod.so.2 libudev.so.1 libuuid.so.1"
RDEPENDS_ndctl-libs = "daxctl-libs glibc kmod-libs libuuid systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/daxctl-67-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/daxctl-libs-67-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ndctl-67-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ndctl-libs-67-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/daxctl-devel-67-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ndctl-devel-67-2.el8.aarch64.rpm \
          "

SRC_URI[daxctl.sha256sum] = "6f1530f8b89ed5dd437aef4a48dcb9f8d60b00a23a0440dd3efc06b25657af4c"
SRC_URI[daxctl-devel.sha256sum] = "8522c484c0ef618d8c9422962686c343639eee99b7b35674e9eec04847c69bc8"
SRC_URI[daxctl-libs.sha256sum] = "77f43fee32721d0beccf746808e5452d83a048b224b473f2c690aba381938933"
SRC_URI[ndctl.sha256sum] = "ff1832a7389f773c62fa17c6bc47afb48c8c2252eda709b9b55bab0909b40a48"
SRC_URI[ndctl-devel.sha256sum] = "75a10a7575fb18111f084756bd7bc32240fa1f6ad4ac375f8bd54887eaa82d3f"
SRC_URI[ndctl-libs.sha256sum] = "f94511cfcb7ef8fc1067c8632f995eaad8b12f2a86b97609051906c5247ac10a"
