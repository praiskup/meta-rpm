SUMMARY = "generated recipe based on Xaw3d srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libsm libx11 libxext libxmu libxpm libxt pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_Xaw3d = "libXaw3d.so.8"
RPM_SONAME_REQ_Xaw3d = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6"
RDEPENDS_Xaw3d = "glibc libX11 libXext libXmu libXpm libXt"
RPM_SONAME_REQ_Xaw3d-devel = "libXaw3d.so.8"
RPROVIDES_Xaw3d-devel = "Xaw3d-dev (= 1.6.2)"
RDEPENDS_Xaw3d-devel = "Xaw3d libSM-devel libX11-devel libXext-devel libXmu-devel libXpm-devel libXt-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/Xaw3d-1.6.2-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/Xaw3d-devel-1.6.2-14.el8.aarch64.rpm \
          "

SRC_URI[Xaw3d.sha256sum] = "67142c7cc216f915d848b7d709adfad9d627def210e0fe06512806b87f9b910e"
SRC_URI[Xaw3d-devel.sha256sum] = "11cd3b9edfd6fc61cf50e478f75593742769f05eeef822bf575dd2d18cff191f"
