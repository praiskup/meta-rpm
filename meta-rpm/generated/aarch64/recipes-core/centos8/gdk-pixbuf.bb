SUMMARY = "generated recipe based on gdk-pixbuf2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 jasper libjpeg-turbo libpng libx11 pkgconfig-native tiff zlib"
RPM_SONAME_PROV_gdk-pixbuf2 = "libgdk_pixbuf-2.0.so.0"
RPM_SONAME_REQ_gdk-pixbuf2 = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_gdk-pixbuf2 = "bash glib2 glibc libpng shared-mime-info zlib"
RPM_SONAME_REQ_gdk-pixbuf2-devel = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RPROVIDES_gdk-pixbuf2-devel = "gdk-pixbuf2-dev (= 2.36.12)"
RDEPENDS_gdk-pixbuf2-devel = "gdk-pixbuf2 glib2 glib2-devel glibc libpng libpng-devel pkgconf-pkg-config zlib"
RPM_SONAME_PROV_gdk-pixbuf2-modules = "libpixbufloader-ani.so libpixbufloader-bmp.so libpixbufloader-gif.so libpixbufloader-icns.so libpixbufloader-ico.so libpixbufloader-jasper.so libpixbufloader-jpeg.so libpixbufloader-pnm.so libpixbufloader-qtif.so libpixbufloader-tga.so libpixbufloader-tiff.so libpixbufloader-xbm.so libpixbufloader-xpm.so"
RPM_SONAME_REQ_gdk-pixbuf2-modules = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjasper.so.4 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libtiff.so.5 libz.so.1"
RDEPENDS_gdk-pixbuf2-modules = "gdk-pixbuf2 glib2 glibc jasper-libs libjpeg-turbo libpng libtiff zlib"
RPM_SONAME_PROV_gdk-pixbuf2-xlib = "libgdk_pixbuf_xlib-2.0.so.0"
RPM_SONAME_REQ_gdk-pixbuf2-xlib = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_gdk-pixbuf2-xlib = "gdk-pixbuf2 glib2 glibc libX11 libpng zlib"
RPM_SONAME_REQ_gdk-pixbuf2-xlib-devel = "libgdk_pixbuf_xlib-2.0.so.0"
RPROVIDES_gdk-pixbuf2-xlib-devel = "gdk-pixbuf2-xlib-dev (= 2.36.12)"
RDEPENDS_gdk-pixbuf2-xlib-devel = "gdk-pixbuf2-devel gdk-pixbuf2-xlib glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdk-pixbuf2-devel-2.36.12-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gdk-pixbuf2-modules-2.36.12-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gdk-pixbuf2-2.36.12-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gdk-pixbuf2-xlib-2.36.12-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gdk-pixbuf2-xlib-devel-2.36.12-5.el8.aarch64.rpm \
          "

SRC_URI[gdk-pixbuf2.sha256sum] = "b2c52d4191957d35b81428fc1e991915839a99fd4d0756c846b5c4dd386b38fe"
SRC_URI[gdk-pixbuf2-devel.sha256sum] = "9f602d1638750c040b224213cc644e7d0d324264bc47b4dd1ed8f183816ac812"
SRC_URI[gdk-pixbuf2-modules.sha256sum] = "fd19941680ae4928b175aa05abcccb802291dfb207d67816bf2a303dd6cb7f44"
SRC_URI[gdk-pixbuf2-xlib.sha256sum] = "320f4134102f7d10163f7138af6728e0a80886c3ace01a549e7ef7e6a9d170ff"
SRC_URI[gdk-pixbuf2-xlib-devel.sha256sum] = "43b60483522f9e7f3046293195723b03c578bee0ed722d1221f2f15120afabfe"
