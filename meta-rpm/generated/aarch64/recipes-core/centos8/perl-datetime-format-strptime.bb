SUMMARY = "generated recipe based on perl-DateTime-Format-Strptime srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Format-Strptime = "perl-Carp perl-DateTime perl-DateTime-Locale perl-DateTime-TimeZone perl-Exporter perl-Package-DeprecationManager perl-Params-ValidationCompiler perl-Specio perl-Try-Tiny perl-constant perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-DateTime-Format-Strptime-1.75-2.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Format-Strptime.sha256sum] = "5c228bb27eeef3f89913034637f5129ca26c5de9781283bfa62600118622260d"
