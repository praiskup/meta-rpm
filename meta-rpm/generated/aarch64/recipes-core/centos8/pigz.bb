SUMMARY = "generated recipe based on pigz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_pigz = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_pigz = "glibc libgcc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pigz-2.4-4.el8.aarch64.rpm \
          "

SRC_URI[pigz.sha256sum] = "bf8bbf6b7fab0e19535a3d7e7bad6a62971b41e7a231683cb42e534355a831b7"
