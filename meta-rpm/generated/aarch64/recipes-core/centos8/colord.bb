SUMMARY = "generated recipe based on colord srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 lcms2 libgcc libgudev libgusb pkgconfig-native polkit sqlite3 systemd-libs"
RPM_SONAME_PROV_colord = "libcolord_sensor_argyll.so libcolord_sensor_camera.so libcolord_sensor_colorhug.so libcolord_sensor_dtp94.so libcolord_sensor_dummy.so libcolord_sensor_huey.so libcolord_sensor_scanner.so"
RPM_SONAME_REQ_colord = "ld-linux-aarch64.so.1 libc.so.6 libcolord.so.2 libcolordprivate.so.2 libcolorhug.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libgusb.so.2 liblcms2.so.2 libm.so.6 libpolkit-gobject-1.so.0 libpthread.so.0 libsqlite3.so.0 libsystemd.so.0"
RDEPENDS_colord = "bash color-filesystem colord-libs glib2 glibc lcms2 libgcc libgudev libgusb polkit-libs shadow-utils sqlite-libs systemd systemd-libs"
RPM_SONAME_REQ_colord-devel = "libcolord.so.2 libcolordprivate.so.2 libcolorhug.so.2"
RPROVIDES_colord-devel = "colord-dev (= 1.4.2)"
RDEPENDS_colord-devel = "colord colord-libs glib2-devel libgusb-devel pkgconf-pkg-config"
RDEPENDS_colord-devel-docs = "colord"
RPM_SONAME_PROV_colord-libs = "libcolord.so.2 libcolordprivate.so.2 libcolorhug.so.2"
RPM_SONAME_REQ_colord-libs = "ld-linux-aarch64.so.1 libc.so.6 libcolordprivate.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgusb.so.2 liblcms2.so.2 libm.so.6 libudev.so.1"
RDEPENDS_colord-libs = "glib2 glibc lcms2 libgcc libgusb systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/colord-1.4.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/colord-libs-1.4.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/colord-devel-1.4.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/colord-devel-docs-1.4.2-1.el8.noarch.rpm \
          "

SRC_URI[colord.sha256sum] = "ec1f859b1e70c78d235892cf158f179c396428d0e23f9ffa2b51db74aaa3cb23"
SRC_URI[colord-devel.sha256sum] = "6d28e9881ad9f0c4bb71a85ceb8960c1b4dbc64ae2a81afff53e17c112e2ae7e"
SRC_URI[colord-devel-docs.sha256sum] = "b1fa7f4ace2f9b36988d1be8e950e32eb33bbc9aa08100f71198baf68a200ecf"
SRC_URI[colord-libs.sha256sum] = "c9f8b1c907484b65202f70a7de163e21d9de9ec5f73673371bc5bcf09a51b73a"
