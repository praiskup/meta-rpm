SUMMARY = "generated recipe based on perl-Text-Glob srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Glob = "perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Text-Glob-0.11-4.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Glob.sha256sum] = "6bc31ceb18afe8d4e855c391029f8b2118b42d05d45de508b8c472dedbcce7ee"
