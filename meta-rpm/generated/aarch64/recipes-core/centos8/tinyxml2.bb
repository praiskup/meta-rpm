SUMMARY = "generated recipe based on tinyxml2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_tinyxml2 = "libtinyxml2.so.6"
RPM_SONAME_REQ_tinyxml2 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_tinyxml2 = "glibc libgcc libstdc++"
RPM_SONAME_REQ_tinyxml2-devel = "libtinyxml2.so.6"
RPROVIDES_tinyxml2-devel = "tinyxml2-dev (= 6.0.0)"
RDEPENDS_tinyxml2-devel = "cmake-filesystem pkgconf-pkg-config tinyxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tinyxml2-6.0.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tinyxml2-devel-6.0.0-3.el8.aarch64.rpm \
          "

SRC_URI[tinyxml2.sha256sum] = "7a9ba5a2ac4972eb7b4bde63495dcf018fa385639d5af644cdef8256002761aa"
SRC_URI[tinyxml2-devel.sha256sum] = "d5f1c7afac752e287c49734950cb484f0e8c48c92a63e045c6d3e4ef14891348"
