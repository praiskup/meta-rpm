SUMMARY = "generated recipe based on mtx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mtx = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mtx = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mtx-1.3.12-17.el8.aarch64.rpm \
          "

SRC_URI[mtx.sha256sum] = "4d4b2045bd11b3d018922dcbcc65b9ba16e6150ed516fedeb0c07a75e5cf9d73"
