SUMMARY = "generated recipe based on plexus-ant-factory srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-ant-factory = "ant-lib java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default plexus-utils"
RDEPENDS_plexus-ant-factory-javadoc = "bash javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-ant-factory-1.0-0.20.a2.2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-ant-factory-javadoc-1.0-0.20.a2.2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-ant-factory.sha256sum] = "f2a2283b83d035a5bd253175f81344f34515d9b9f218dafae4bd9756f5313f34"
SRC_URI[plexus-ant-factory-javadoc.sha256sum] = "6c000cb30e6acd4c16c7c17d125fab4f396a3cb92975c5a2ae00933d48dd8a42"
