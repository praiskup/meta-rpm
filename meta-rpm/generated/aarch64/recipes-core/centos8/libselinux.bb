SUMMARY = "generated recipe based on libselinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre2 libsepol pkgconfig-native"
RPM_SONAME_PROV_libselinux = "libselinux.so.1"
RPM_SONAME_REQ_libselinux = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcre2-8.so.0"
RDEPENDS_libselinux = "glibc libsepol pcre2"
RPM_SONAME_REQ_libselinux-devel = "libselinux.so.1"
RPROVIDES_libselinux-devel = "libselinux-dev (= 2.9)"
RDEPENDS_libselinux-devel = "libselinux libsepol-devel pcre2-devel pkgconf-pkg-config"
RPM_SONAME_REQ_libselinux-utils = "ld-linux-aarch64.so.1 libc.so.6 libpcre2-8.so.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_libselinux-utils = "glibc libselinux libsepol pcre2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libselinux-2.9-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libselinux-devel-2.9-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libselinux-utils-2.9-3.el8.aarch64.rpm \
          "

SRC_URI[libselinux.sha256sum] = "761b39bec7d8a603a617a241f56dac786206bac98b997bbf428376e9ebd6d86d"
SRC_URI[libselinux-devel.sha256sum] = "a75012871c41cfa533fe4e4d694bba7886503c17f961f5284f87903de5bae25e"
SRC_URI[libselinux-utils.sha256sum] = "3bc77b12143ed0bd96fd44f733515bcef1bc334200047eb544df52b7ffc3e2c0"
