SUMMARY = "generated recipe based on glib2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libffi libgcc libmount libpcre libselinux pkgconfig-native zlib"
RPM_SONAME_PROV_glib2 = "libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0"
RPM_SONAME_REQ_glib2 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libz.so.1"
RDEPENDS_glib2 = "bash glibc gnutls libffi libgcc libmount libselinux pcre zlib"
RPM_SONAME_REQ_glib2-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libz.so.1"
RPROVIDES_glib2-devel = "glib2-dev (= 2.56.4)"
RDEPENDS_glib2-devel = "bash glib2 glibc gnutls libffi libmount libselinux pcre pcre-devel pkgconf-pkg-config platform-python zlib"
RDEPENDS_glib2-doc = "glib2"
RPM_SONAME_PROV_glib2-fam = "libgiofam.so"
RPM_SONAME_REQ_glib2-fam = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libfam.so.0 libffi.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libz.so.1"
RDEPENDS_glib2-fam = "gamin glib2 glibc gnutls libffi libmount libselinux pcre zlib"
RDEPENDS_glib2-static = "glib2-devel"
RPM_SONAME_PROV_glib2-tests = "libgdbus-example-objectmanager.so.0 libmoduletestplugin_a.so libmoduletestplugin_b.so libresourceplugin.so libtestmodulea.so libtestmoduleb.so"
RPM_SONAME_REQ_glib2-tests = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libgcc_s.so.1 libgdbus-example-objectmanager.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_glib2-tests = "bash glib2 glibc gnutls libffi libgcc libmount libselinux libstdc++ pcre zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glib2-2.56.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glib2-devel-2.56.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glib2-fam-2.56.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glib2-tests-2.56.4-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glib2-doc-2.56.4-8.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glib2-static-2.56.4-8.el8.aarch64.rpm \
          "
SRC_URI = "file://glib2-2.56.4-8.el8.patch"

SRC_URI[glib2.sha256sum] = "b10bf2d2a3a37a590685ed11cebf2e1b31cccaab5087234dcb790382e29e61dc"
SRC_URI[glib2-devel.sha256sum] = "8f38d2cfd44c2b38595da8a9250482b63dba4554c9180eab39e1d3bc204d461d"
SRC_URI[glib2-doc.sha256sum] = "69523bdbdeff9fc92b75186fc107b022fa0001317c60d931c1eae44ee727d31c"
SRC_URI[glib2-fam.sha256sum] = "3460cd744f1f7ae7c8bb07f947a428cb6dac910efec049dd06462d583bbc5c0a"
SRC_URI[glib2-static.sha256sum] = "26db920b3e67d64b1e0e6e9661e5a9bb806050fc9ffcda9de5450f5fa1da1cd8"
SRC_URI[glib2-tests.sha256sum] = "2d1e6e66f64fe288a34e6df84853d12ba143a2fc44090cb1cfb46542f7178363"
