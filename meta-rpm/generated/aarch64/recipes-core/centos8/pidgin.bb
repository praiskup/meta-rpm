SUMMARY = "generated recipe based on pidgin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs cyrus-sasl-lib dbus dbus-glib dbus-glib-devel dbus-libs farstream02 glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libidn libxml2 meanwhile nspr nss pkgconfig-native"
RPM_SONAME_PROV_libpurple = "libpurple-client.so.0 libpurple.so.0"
RPM_SONAME_REQ_libpurple = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libfarstream-0.2.so.5 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libidn.so.11 libm.so.6 libmeanwhile.so.1 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libpurple-client.so.0 libresolv.so.2 libsasl2.so.3 libsmime3.so libssl3.so libxml2.so.2"
RDEPENDS_libpurple = "avahi-glib avahi-libs bash ca-certificates cyrus-sasl-lib cyrus-sasl-md5 cyrus-sasl-plain cyrus-sasl-scram dbus-glib dbus-libs farstream02 glib2 glibc gstreamer1 gstreamer1-plugins-base libidn libxml2 meanwhile nspr nss nss-util platform-python python3-dbus"
RPM_SONAME_REQ_libpurple-devel = "libpurple-client.so.0 libpurple.so.0"
RPROVIDES_libpurple-devel = "libpurple-dev (= 2.13.0)"
RDEPENDS_libpurple-devel = "dbus-devel dbus-glib-devel glib2-devel libpurple pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpurple-2.13.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpurple-devel-2.13.0-5.el8.aarch64.rpm \
          "

SRC_URI[libpurple.sha256sum] = "441e8ec87e2439234e831f86d8d7503579dd24044e0b91128a944f450991ebe5"
SRC_URI[libpurple-devel.sha256sum] = "30a36976f8609124376783cae8d3c75fb51eff3253a83a16afd1541dd1f0b4cf"
