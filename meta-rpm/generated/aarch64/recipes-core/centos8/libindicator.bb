SUMMARY = "generated recipe based on libindicator srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 gtk+3 pkgconfig-native"
RPM_SONAME_PROV_libindicator-gtk3 = "libindicator3.so.7"
RPM_SONAME_REQ_libindicator-gtk3 = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpthread.so.0"
RDEPENDS_libindicator-gtk3 = "gdk-pixbuf2 glib2 glibc gtk3"
RPM_SONAME_REQ_libindicator-gtk3-devel = "libindicator3.so.7"
RPROVIDES_libindicator-gtk3-devel = "libindicator-gtk3-dev (= 12.10.1)"
RDEPENDS_libindicator-gtk3-devel = "gtk3-devel libindicator-gtk3 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libindicator-gtk3-12.10.1-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libindicator-gtk3-devel-12.10.1-14.el8.aarch64.rpm \
          "

SRC_URI[libindicator-gtk3.sha256sum] = "108ef3a34bf42ab8c243599ede0e91bede5bcd7910cd34b118c4143e4dcf4221"
SRC_URI[libindicator-gtk3-devel.sha256sum] = "1ad6cc4ea64d7a5d833e718fc52485edea74059afa12c74e53a96d2d658290a3"
