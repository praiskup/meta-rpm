SUMMARY = "generated recipe based on libsoup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 krb5-libs libxml2 pkgconfig-native sqlite3"
RPM_SONAME_PROV_libsoup = "libsoup-2.4.so.1 libsoup-gnome-2.4.so.1"
RPM_SONAME_REQ_libsoup = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libsoup-2.4.so.1 libsqlite3.so.0 libxml2.so.2"
RDEPENDS_libsoup = "glib-networking glib2 glibc krb5-libs libxml2 sqlite-libs"
RPM_SONAME_REQ_libsoup-devel = "libsoup-2.4.so.1 libsoup-gnome-2.4.so.1"
RPROVIDES_libsoup-devel = "libsoup-dev (= 2.62.3)"
RDEPENDS_libsoup-devel = "glib2-devel libsoup libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsoup-devel-2.62.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsoup-2.62.3-1.el8.aarch64.rpm \
          "

SRC_URI[libsoup.sha256sum] = "d9452ee3f72d83d5cc8ea20ef8443c006298b76181f6dde262b2a00d7e13de74"
SRC_URI[libsoup-devel.sha256sum] = "98128b1288b2e1764c39874ef5da1c31f0a6d7253f67278c93d4629613d5b9c2"
