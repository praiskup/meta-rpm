SUMMARY = "generated recipe based on redland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db libtool pkgconfig-native raptor2 rasqal sqlite3"
RPM_SONAME_PROV_redland = "librdf.so.0 librdf_storage_sqlite.so"
RPM_SONAME_REQ_redland = "ld-linux-aarch64.so.1 libc.so.6 libdb-5.3.so libdl.so.2 libltdl.so.7 libraptor2.so.0 librasqal.so.3 librdf.so.0 libsqlite3.so.0"
RDEPENDS_redland = "glibc libdb libtool-ltdl raptor2 rasqal sqlite-libs"
RPM_SONAME_REQ_redland-devel = "librdf.so.0"
RPROVIDES_redland-devel = "redland-dev (= 1.0.17)"
RDEPENDS_redland-devel = "bash pkgconf-pkg-config raptor2-devel rasqal-devel redland"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/redland-1.0.17-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/redland-devel-1.0.17-14.el8.aarch64.rpm \
          "

SRC_URI[redland.sha256sum] = "7fc14f81cdb40e1c8de847ce3217729368cca65b6b7b51b271badef82e823beb"
SRC_URI[redland-devel.sha256sum] = "f015106989664bfbff99b1eda4f7e9044c310d6810bbb661aa11ab880a5fcd02"
