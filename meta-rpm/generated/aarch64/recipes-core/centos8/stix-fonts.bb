SUMMARY = "generated recipe based on stix-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_stix-fonts = "fontpackages-filesystem"
RDEPENDS_stix-math-fonts = "stix-fonts"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/stix-fonts-1.1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/stix-math-fonts-1.1.0-12.el8.noarch.rpm \
          "

SRC_URI[stix-fonts.sha256sum] = "2921be7893dcdba5b41923d61cc38531a40486d4cb145ea3ffd664bc2c7554aa"
SRC_URI[stix-math-fonts.sha256sum] = "0dc8cd2c8677a52a8dd302f2f46a0ce5e4b633bc79901f72d3b51d73bea1e560"
