SUMMARY = "generated recipe based on drpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 openssl pkgconfig-native rpm xz zlib"
RPM_SONAME_PROV_drpm = "libdrpm.so.0"
RPM_SONAME_REQ_drpm = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblzma.so.5 librpm.so.8 librpmio.so.8 libz.so.1"
RDEPENDS_drpm = "bzip2-libs glibc openssl-libs rpm-libs xz-libs zlib"
RPM_SONAME_REQ_drpm-devel = "libdrpm.so.0"
RPROVIDES_drpm-devel = "drpm-dev (= 0.4.1)"
RDEPENDS_drpm-devel = "drpm pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/drpm-0.4.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/drpm-devel-0.4.1-1.el8.aarch64.rpm \
          "

SRC_URI[drpm.sha256sum] = "05463f4b8445789a0cd136348c32143e4ab156a30097cef4c397c921acd388f0"
SRC_URI[drpm-devel.sha256sum] = "cd1b3ee6d1cd8428cdd71a58dec345a4ce358c585caf73b271abd508ff6af7cb"
