SUMMARY = "generated recipe based on libdvdnav srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdvdread pkgconfig-native"
RPM_SONAME_PROV_libdvdnav = "libdvdnav.so.4"
RPM_SONAME_REQ_libdvdnav = "ld-linux-aarch64.so.1 libc.so.6 libdvdread.so.4 libpthread.so.0"
RDEPENDS_libdvdnav = "glibc libdvdread"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdvdnav-5.0.3-8.el8.aarch64.rpm \
          "

SRC_URI[libdvdnav.sha256sum] = "d6b2b8c5baf98fa054358405e30ee9af3623915071092a93854694e3642b918a"
