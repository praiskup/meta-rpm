SUMMARY = "generated recipe based on pango srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo fontconfig freetype fribidi glib-2.0 harfbuzz libthai libx11 libxft libxrender pkgconfig-native"
RPM_SONAME_PROV_pango = "libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0"
RPM_SONAME_REQ_pango = "ld-linux-aarch64.so.1 libX11.so.6 libXft.so.2 libXrender.so.1 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libfribidi.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libharfbuzz.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0 libthai.so.0"
RDEPENDS_pango = "bash cairo fontconfig freetype fribidi glib2 glibc harfbuzz libX11 libXft libXrender libthai"
RPM_SONAME_REQ_pango-devel = "libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0"
RPROVIDES_pango-devel = "pango-dev (= 1.42.4)"
RDEPENDS_pango-devel = "cairo-devel fontconfig-devel freetype-devel fribidi-devel glib2-devel harfbuzz-devel libXft-devel pango pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pango-1.42.4-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pango-devel-1.42.4-6.el8.aarch64.rpm \
          "

SRC_URI[pango.sha256sum] = "822a314a2d2080c1e3d52637229114c0a9b6b1d7a229b1d30caa048556a9c4ec"
SRC_URI[pango-devel.sha256sum] = "cb61b5f5b875fab728b7f7f219efa1198c13ccb74554ba656134b8b5644d9a56"
