SUMMARY = "generated recipe based on alsa-lib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_alsa-lib = "libasound.so.2 libatopology.so.2"
RPM_SONAME_REQ_alsa-lib = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_alsa-lib = "coreutils glibc"
RPM_SONAME_REQ_alsa-lib-devel = "libasound.so.2 libatopology.so.2"
RPROVIDES_alsa-lib-devel = "alsa-lib-dev (= 1.2.1.2)"
RDEPENDS_alsa-lib-devel = "alsa-lib pkgconf-pkg-config"
RDEPENDS_alsa-ucm = "alsa-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-lib-1.2.1.2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-lib-devel-1.2.1.2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-ucm-1.2.1.2-3.el8.noarch.rpm \
          "

SRC_URI[alsa-lib.sha256sum] = "1b9f0aa823d9f73c432609811d5312755b0615b18a7de0ac5af654bfc99ec37c"
SRC_URI[alsa-lib-devel.sha256sum] = "f06ddd314f2166b5dbc870875f4378c4b6af835dc84c454a17f757b86f8ef39c"
SRC_URI[alsa-ucm.sha256sum] = "daba0567491b6fe0ab3714fbb9fa0024fdfe40206e4b228e1869274650867298"
