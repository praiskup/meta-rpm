SUMMARY = "generated recipe based on sbd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "corosync glib-2.0 libaio libqb libuuid libxml2 pacemaker pkgconfig-native"
RPM_SONAME_REQ_sbd = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libcib.so.27 libcmap.so.4 libcrmcluster.so.29 libcrmcommon.so.34 libdl.so.2 libglib-2.0.so.0 libpe_rules.so.26 libpe_status.so.28 libqb.so.0 libuuid.so.1 libvotequorum.so.8 libxml2.so.2"
RDEPENDS_sbd = "bash corosynclib glib2 glibc libaio libqb libuuid libxml2 pacemaker-cluster-libs pacemaker-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sbd-1.4.1-3.el8.aarch64.rpm \
          "

SRC_URI[sbd.sha256sum] = "05612b720ca8e4a23f1be96dbebcf2090edcb166fdeeac09219e3a30de88ffd5"
