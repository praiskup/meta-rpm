SUMMARY = "generated recipe based on perl-Test-Differences srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Differences = "perl-Carp perl-Data-Dumper perl-Exporter perl-Text-Diff perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-Differences-0.6400-8.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Differences.sha256sum] = "3670247bf83dc5475c2f7c85b26e6b9959d96acaadf09a7f4819a24fc455f7bc"
