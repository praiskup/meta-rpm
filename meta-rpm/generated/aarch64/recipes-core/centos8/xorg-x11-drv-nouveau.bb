SUMMARY = "generated recipe based on xorg-x11-drv-nouveau srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdrm pkgconfig-native systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-nouveau = "ld-linux-aarch64.so.1 libc.so.6 libdrm.so.2 libdrm_nouveau.so.2 libudev.so.1"
RDEPENDS_xorg-x11-drv-nouveau = "glibc libdrm systemd-libs xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-nouveau-1.0.15-4.el8.1.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-nouveau.sha256sum] = "57bc85e711a6ddb4f4d22099089591f4aa0e70d8a16af45600f71f63a9104276"
