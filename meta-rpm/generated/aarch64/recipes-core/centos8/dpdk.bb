SUMMARY = "generated recipe based on dpdk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc numactl pkgconfig-native"
RPM_SONAME_PROV_dpdk = "librte_bitratestats.so.20.0 librte_bus_pci.so.20.0 librte_bus_vdev.so.20.0 librte_cmdline.so.20.0 librte_eal.so.20.0 librte_ethdev.so.20.0 librte_gro.so.20.0 librte_gso.so.20.0 librte_hash.so.20.0 librte_ip_frag.so.20.0 librte_kvargs.so.20.0 librte_latencystats.so.20.0 librte_mbuf.so.20.0 librte_member.so.20.0 librte_mempool.so.20.0 librte_mempool_bucket.so.20.0 librte_mempool_ring.so.20.0 librte_mempool_stack.so.20.0 librte_meter.so.20.0 librte_metrics.so.20.0 librte_net.so.20.0 librte_pci.so.20.0 librte_pdump.so.20.0 librte_pmd_e1000.so.20.0 librte_pmd_failsafe.so.20.0 librte_pmd_i40e.so.20.0 librte_pmd_ixgbe.so.20.0 librte_pmd_ring.so.20.0 librte_pmd_tap.so.20.0 librte_pmd_vhost.so.20.0 librte_pmd_virtio.so.20.0 librte_ring.so.20.0 librte_stack.so.0.200 librte_vhost.so.20.0"
RPM_SONAME_REQ_dpdk = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libnuma.so.1 libpthread.so.0 librt.so.1 librte_bitratestats.so.20.0 librte_bus_pci.so.20.0 librte_bus_vdev.so.20.0 librte_cmdline.so.20.0 librte_eal.so.20.0 librte_ethdev.so.20.0 librte_gro.so.20.0 librte_gso.so.20.0 librte_hash.so.20.0 librte_ip_frag.so.20.0 librte_kvargs.so.20.0 librte_latencystats.so.20.0 librte_mbuf.so.20.0 librte_member.so.20.0 librte_mempool.so.20.0 librte_mempool_ring.so.20.0 librte_meter.so.20.0 librte_metrics.so.20.0 librte_net.so.20.0 librte_pci.so.20.0 librte_pdump.so.20.0 librte_pmd_i40e.so.20.0 librte_pmd_ixgbe.so.20.0 librte_ring.so.20.0 librte_stack.so.0.200 librte_vhost.so.20.0"
RDEPENDS_dpdk = "glibc libgcc numactl-libs"
RPM_SONAME_REQ_dpdk-devel = "ld-linux-aarch64.so.1 libc.so.6 librte_bitratestats.so.20.0 librte_bus_pci.so.20.0 librte_bus_vdev.so.20.0 librte_cmdline.so.20.0 librte_eal.so.20.0 librte_ethdev.so.20.0 librte_gro.so.20.0 librte_gso.so.20.0 librte_hash.so.20.0 librte_ip_frag.so.20.0 librte_kvargs.so.20.0 librte_latencystats.so.20.0 librte_mbuf.so.20.0 librte_member.so.20.0 librte_mempool.so.20.0 librte_mempool_bucket.so.20.0 librte_mempool_ring.so.20.0 librte_mempool_stack.so.20.0 librte_meter.so.20.0 librte_metrics.so.20.0 librte_net.so.20.0 librte_pci.so.20.0 librte_pdump.so.20.0 librte_pmd_e1000.so.20.0 librte_pmd_failsafe.so.20.0 librte_pmd_i40e.so.20.0 librte_pmd_ixgbe.so.20.0 librte_pmd_ring.so.20.0 librte_pmd_tap.so.20.0 librte_pmd_vhost.so.20.0 librte_pmd_virtio.so.20.0 librte_ring.so.20.0 librte_stack.so.0.200 librte_vhost.so.20.0"
RPROVIDES_dpdk-devel = "dpdk-dev (= 19.11)"
RDEPENDS_dpdk-devel = "bash dpdk glibc"
RDEPENDS_dpdk-tools = "dpdk findutils iproute kmod pciutils platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dpdk-19.11-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dpdk-devel-19.11-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dpdk-doc-19.11-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dpdk-tools-19.11-4.el8.aarch64.rpm \
          "

SRC_URI[dpdk.sha256sum] = "0818fc1465cf854e603b4778470896302dfe18e41e513214e6f6e39e44e51b56"
SRC_URI[dpdk-devel.sha256sum] = "1e5a4e4c308c949ad459cb341d718ccc2e2d7dc0d40aabafcfe1b8f68679674c"
SRC_URI[dpdk-doc.sha256sum] = "ccd4e705a22b393b4a4eb44a36ff99f4b186cc4a882de184d2c0caa3b2b65686"
SRC_URI[dpdk-tools.sha256sum] = "b6df63e88abbd3cc1517084f270b9d2d1578e7f74b21bd1d6c258e6f8555a90c"
