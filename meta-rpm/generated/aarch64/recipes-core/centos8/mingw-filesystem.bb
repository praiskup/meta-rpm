SUMMARY = "generated recipe based on mingw-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw-filesystem-base = "bash"
RDEPENDS_mingw32-filesystem = "mingw-filesystem-base"
RDEPENDS_mingw64-filesystem = "mingw-filesystem-base"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw-filesystem-base-104-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw32-filesystem-104-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw64-filesystem-104-1.el8.noarch.rpm \
          "

SRC_URI[mingw-filesystem-base.sha256sum] = "506ed146fc95087c11ff9ae3a726f1dc9469babcd772d936621fcebbf203f1bf"
SRC_URI[mingw32-filesystem.sha256sum] = "cf2792e2ac226d9678cc154173ef5faca34b166409e6be51c95c3c9c7c869ba0"
SRC_URI[mingw64-filesystem.sha256sum] = "c77bb33585fac59f08642454043df5bbedd9da0d5d760092db99b4baa2a2c5b7"
