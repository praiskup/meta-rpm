SUMMARY = "generated recipe based on os-prober srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_os-prober = "libc.so.6"
RDEPENDS_os-prober = "bash coreutils device-mapper glibc grep kmod sed systemd-udev util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/os-prober-1.74-6.el8.aarch64.rpm \
          "

SRC_URI[os-prober.sha256sum] = "5515efce88237588ed2f13b8008846f139cafd1ba5063eccdfb156af03fc8f75"
