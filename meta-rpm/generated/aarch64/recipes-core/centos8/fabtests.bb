SUMMARY = "generated recipe based on fabtests srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libfabric pkgconfig-native"
RPM_SONAME_REQ_fabtests = "ld-linux-aarch64.so.1 libc.so.6 libfabric.so.1"
RDEPENDS_fabtests = "bash glibc libfabric ruby"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fabtests-1.9.0rc1-1.el8.aarch64.rpm \
          "

SRC_URI[fabtests.sha256sum] = "a53d957f7442e9ec73e274ec386757eb6c5d961e1796cfcaf5d52513873580f3"
