SUMMARY = "generated recipe based on apache-commons-logging srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-logging = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-logging-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-logging-1.2-13.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-logging-javadoc-1.2-13.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-logging.sha256sum] = "c9693b13e1075be118b44aab3db41946f7348f26749a43b4e3ca458466e6b11f"
SRC_URI[apache-commons-logging-javadoc.sha256sum] = "96e18f8d56c5e720f413241820e93dc2b666a8de7b69aabb6afbde8c5560ea33"
