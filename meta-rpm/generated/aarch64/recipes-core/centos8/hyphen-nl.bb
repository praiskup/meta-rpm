SUMMARY = "generated recipe based on hyphen-nl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-nl = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-nl-0.20050617-18.el8.noarch.rpm \
          "

SRC_URI[hyphen-nl.sha256sum] = "fdcfcbd9a086a67a46a153bc6559068d1970a5589edcaac7ad0825a8df4d7837"
