SUMMARY = "generated recipe based on libcap-ng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libcap-ng = "libcap-ng.so.0"
RPM_SONAME_REQ_libcap-ng = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libcap-ng = "glibc"
RPM_SONAME_REQ_libcap-ng-devel = "libcap-ng.so.0"
RPROVIDES_libcap-ng-devel = "libcap-ng-dev (= 0.7.9)"
RDEPENDS_libcap-ng-devel = "kernel-headers libcap-ng pkgconf-pkg-config"
RPM_SONAME_REQ_libcap-ng-utils = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0"
RDEPENDS_libcap-ng-utils = "glibc libcap-ng"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcap-ng-0.7.9-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcap-ng-devel-0.7.9-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcap-ng-utils-0.7.9-5.el8.aarch64.rpm \
          "

SRC_URI[libcap-ng.sha256sum] = "0a3c264fa00b82fda25a2c4bee6ce9012d7ae76f6c170d857946bb60570e389a"
SRC_URI[libcap-ng-devel.sha256sum] = "ceb49361e13ca83bbb0a753a574e1c07ee2795afeeabfa29a2db00e4ead87711"
SRC_URI[libcap-ng-utils.sha256sum] = "8748676cb4cf35b40bd8e50c8aaf7cb210a1f8947189341af9f445336a79feb2"
