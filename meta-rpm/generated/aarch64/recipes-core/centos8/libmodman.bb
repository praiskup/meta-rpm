SUMMARY = "generated recipe based on libmodman srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libmodman = "libmodman.so.1"
RPM_SONAME_REQ_libmodman = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libmodman = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmodman-2.0.1-17.el8.aarch64.rpm \
          "

SRC_URI[libmodman.sha256sum] = "f2628a19999f78a3ec1a796d66c8cd45f58cfb670850ecb7790528279f410677"
