SUMMARY = "generated recipe based on openssl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "krb5-libs pkgconfig-native zlib"
RPM_SONAME_REQ_openssl = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_openssl = "bash coreutils glibc openssl-libs zlib"
RPM_SONAME_REQ_openssl-devel = "libcrypto.so.1.1 libssl.so.1.1"
RPROVIDES_openssl-devel = "openssl-dev (= 1.1.1c)"
RDEPENDS_openssl-devel = "krb5-devel openssl-libs pkgconf-pkg-config zlib-devel"
RPM_SONAME_PROV_openssl-libs = "libcrypto.so.1.1 libssl.so.1.1"
RPM_SONAME_REQ_openssl-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libz.so.1"
RPROVIDES_openssl-libs = "libssl (= 1.1.1c)"
RDEPENDS_openssl-libs = "ca-certificates crypto-policies glibc zlib"
RDEPENDS_openssl-perl = "openssl perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-1.1.1c-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-devel-1.1.1c-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-libs-1.1.1c-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openssl-perl-1.1.1c-15.el8.aarch64.rpm \
          "

SRC_URI[openssl.sha256sum] = "cf10392c322c02c017b9f127102d45c91825af4c4edf21d2ae727fa1d8fc84c2"
SRC_URI[openssl-devel.sha256sum] = "d44d32917756f320b9a2a441723901c61a941fc690e542499c371a8bec6f73f5"
SRC_URI[openssl-libs.sha256sum] = "e25209f9c272a5407ed0d8397e6f8ce8d6de1bb3a6186b06d23ec1533d19d18a"
SRC_URI[openssl-perl.sha256sum] = "24af4bbc8e4af1e5e7d0a3ae1a3ddc9f0e08e8a90a02936645f308cab2563b71"
