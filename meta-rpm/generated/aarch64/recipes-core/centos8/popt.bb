SUMMARY = "generated recipe based on popt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_popt = "libpopt.so.0"
RPM_SONAME_REQ_popt = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_popt = "glibc"
RPM_SONAME_REQ_popt-devel = "libpopt.so.0"
RPROVIDES_popt-devel = "popt-dev (= 1.16)"
RDEPENDS_popt-devel = "pkgconf-pkg-config popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/popt-1.16-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/popt-devel-1.16-14.el8.aarch64.rpm \
          "

SRC_URI[popt.sha256sum] = "f58ce80c4f54569359e6f051b84d11cdce1bbe0f79ad51228ca89ecc614f937d"
SRC_URI[popt-devel.sha256sum] = "3968ca6a2050abc4119549c596a86510a452015f80f4516608bf65379b61aa43"
