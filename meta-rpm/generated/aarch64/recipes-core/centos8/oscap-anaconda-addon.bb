SUMMARY = "generated recipe based on oscap-anaconda-addon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_oscap-anaconda-addon = "anaconda-core openscap openscap-python3 openscap-utils python3-cpio python3-kickstart python3-pycurl scap-security-guide"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/oscap-anaconda-addon-1.0-10.el8.noarch.rpm \
          "

SRC_URI[oscap-anaconda-addon.sha256sum] = "04c3f035e6e5894980e826fb19695c35ea72f2d7b6ec7b4487a6fd504c9ad27b"
