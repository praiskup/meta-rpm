SUMMARY = "generated recipe based on kyotocabinet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc lzo pkgconfig-native xz zlib"
RPM_SONAME_PROV_kyotocabinet-libs = "libkyotocabinet.so.16"
RPM_SONAME_REQ_kyotocabinet-libs = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 liblzma.so.5 liblzo2.so.2 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_kyotocabinet-libs = "glibc libgcc libstdc++ lzo xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/kyotocabinet-libs-1.2.76-17.el8.aarch64.rpm \
          "

SRC_URI[kyotocabinet-libs.sha256sum] = "82136562dc0f848347eee7cf71987e519b00b478fcfc6bc7e2cb29f3315179a4"
