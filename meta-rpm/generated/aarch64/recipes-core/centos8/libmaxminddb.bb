SUMMARY = "generated recipe based on libmaxminddb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmaxminddb = "libmaxminddb.so.0"
RPM_SONAME_REQ_libmaxminddb = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libmaxminddb = "glibc"
RPM_SONAME_REQ_libmaxminddb-devel = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libmaxminddb.so.0"
RPROVIDES_libmaxminddb-devel = "libmaxminddb-dev (= 1.2.0)"
RDEPENDS_libmaxminddb-devel = "glibc libmaxminddb pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmaxminddb-1.2.0-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmaxminddb-devel-1.2.0-7.el8.aarch64.rpm \
          "

SRC_URI[libmaxminddb.sha256sum] = "f10f6ddc1ceca02329b7dbdfe59b08c8738b65a3323cd280dba6fed528330d1a"
SRC_URI[libmaxminddb-devel.sha256sum] = "7e577a115d7a88cecda02cccdbedfd094a2ba8aa20c0b311cd1b1e5d363acb4e"
