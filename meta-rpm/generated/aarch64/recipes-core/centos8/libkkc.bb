SUMMARY = "generated recipe based on libkkc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib libgcc libgee marisa pkgconfig-native"
RPM_SONAME_PROV_libkkc = "libkkc.so.2"
RPM_SONAME_REQ_libkkc = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgee-0.8.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libmarisa.so.0 libpthread.so.0"
RDEPENDS_libkkc = "glib2 glibc json-glib libgcc libgee libkkc-common libkkc-data marisa skkdic"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libkkc-0.3.5-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libkkc-common-0.3.5-12.el8.noarch.rpm \
          "

SRC_URI[libkkc.sha256sum] = "b1de6a494c8543fa9239d04125523642ede7f5a288a5da5f33bc4148180c9d36"
SRC_URI[libkkc-common.sha256sum] = "42346ddcae62ba19f9221aa074db26aab47e1dad088e0805e68149cd5ea71f71"
