SUMMARY = "generated recipe based on libxml2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3 xz zlib"
RPM_SONAME_PROV_libxml2 = "libxml2.so.2"
RPM_SONAME_REQ_libxml2 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libxml2 = "glibc xz-libs zlib"
RPM_SONAME_REQ_libxml2-devel = "libxml2.so.2"
RPROVIDES_libxml2-devel = "libxml2-dev (= 2.9.7)"
RDEPENDS_libxml2-devel = "bash cmake-filesystem libxml2 pkgconf-pkg-config xz-devel zlib-devel"
RPM_SONAME_PROV_python3-libxml2 = "libxml2mod.so"
RPM_SONAME_REQ_python3-libxml2 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_python3-libxml2 = "glibc libxml2 platform-python python3-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libxml2-devel-2.9.7-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libxml2-2.9.7-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libxml2-2.9.7-7.el8.aarch64.rpm \
          "

SRC_URI[libxml2.sha256sum] = "62a4d1c18b48aeb9c83d0d2e64af7cd3b50b84fb76c46bdbda35f6ff788a73e5"
SRC_URI[libxml2-devel.sha256sum] = "8cf319447eda83cbefa7fd1735df31ad0e34fe2ae0cfccabcdb9eb00529e8fee"
SRC_URI[python3-libxml2.sha256sum] = "eeecb6cbef56825dc2034ca0412bab2688a250e580f4062ffa7dbd52a175e014"
