SUMMARY = "generated recipe based on libuv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuv = "libuv.so.1"
RPM_SONAME_REQ_libuv = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_libuv = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libuv-1.23.1-1.el8.aarch64.rpm \
          "

SRC_URI[libuv.sha256sum] = "2129151ece518b506cdc5d41eeda0a2cdadf66885da7f67478b9f0cd09711412"
