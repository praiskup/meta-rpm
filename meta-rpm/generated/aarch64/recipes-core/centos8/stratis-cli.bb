SUMMARY = "generated recipe based on stratis-cli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_stratis-cli = "platform-python python3-dateutil python3-dbus-client-gen python3-dbus-python-client-gen python3-justbytes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/stratis-cli-2.0.0-1.el8.noarch.rpm \
          "

SRC_URI[stratis-cli.sha256sum] = "7219828c80914954398221e21ef51b5fa7429ed3626d499cfb7393415ec59302"
