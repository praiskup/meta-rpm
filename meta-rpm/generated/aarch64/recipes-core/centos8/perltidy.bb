SUMMARY = "generated recipe based on perltidy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perltidy = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-File-Temp perl-Getopt-Long perl-IO perl-PathTools perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perltidy-20180220-1.el8.noarch.rpm \
          "

SRC_URI[perltidy.sha256sum] = "479d2b9e610a8be8b590018af0c4ffe5ac71b720c4600e112b419021085253ae"
