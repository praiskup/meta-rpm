SUMMARY = "generated recipe based on atk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_atk = "libatk-1.0.so.0"
RPM_SONAME_REQ_atk = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_atk = "glib2 glibc"
RPM_SONAME_REQ_atk-devel = "libatk-1.0.so.0"
RPROVIDES_atk-devel = "atk-dev (= 2.28.1)"
RDEPENDS_atk-devel = "atk glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/atk-2.28.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/atk-devel-2.28.1-1.el8.aarch64.rpm \
          "

SRC_URI[atk.sha256sum] = "1cf5160cbe18bfa8af863216567c9ca693292534e8495074651eadaf705a8255"
SRC_URI[atk-devel.sha256sum] = "446e3670f9077dcfe6f17536b54029ce0ae7b850451a33ecd4245f350e4ebbe4"
