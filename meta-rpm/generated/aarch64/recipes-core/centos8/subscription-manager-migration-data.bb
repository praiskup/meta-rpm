SUMMARY = "generated recipe based on subscription-manager-migration-data srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/subscription-manager-migration-data-2.0.51-1.noarch.rpm \
          "

SRC_URI[subscription-manager-migration-data.sha256sum] = "29fa79ada0b8b660ab2933d7030fc5a9b25976e6609de1d583f0f13e0cdba0d1"
