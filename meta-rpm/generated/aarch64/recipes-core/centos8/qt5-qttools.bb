SUMMARY = "generated recipe based on qt5-qttools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "clang libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-assistant = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-assistant = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common qt5-qttools-libs-help"
RPM_SONAME_REQ_qt5-designer = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5DesignerComponents.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-designer = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-libs-designer qt5-qttools-libs-designercomponents"
RPM_SONAME_REQ_qt5-doctools = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libclang.so.9 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-doctools = "clang-libs glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qttools-libs-help"
RPM_SONAME_REQ_qt5-linguist = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-linguist = "cmake-filesystem glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RPM_SONAME_REQ_qt5-qdbusviewer = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qdbusviewer = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RPM_SONAME_REQ_qt5-qttools = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools = "glibc libgcc libstdc++ qt5-qtbase qt5-qttools-common"
RPM_SONAME_REQ_qt5-qttools-devel = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5DesignerComponents.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qttools-devel = "qt5-qttools-dev (= 5.12.5)"
RDEPENDS_qt5-qttools-devel = "cmake-filesystem glibc libgcc libglvnd-glx libstdc++ pkgconf-pkg-config qt5-designer qt5-doctools qt5-linguist qt5-qtbase qt5-qtbase-devel qt5-qtbase-gui qt5-qttools qt5-qttools-libs-designer qt5-qttools-libs-designercomponents qt5-qttools-libs-help"
RPM_SONAME_PROV_qt5-qttools-examples = "libcontainerextension.so libcustomwidgetplugin.so libqquickwidget.so libtaskmenuextension.so libworldtimeclockplugin.so"
RPM_SONAME_REQ_qt5-qttools-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickWidgets.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qttools-common qt5-qttools-libs-designer qt5-qttools-libs-help"
RPM_SONAME_PROV_qt5-qttools-libs-designer = "libQt5Designer.so.5"
RPM_SONAME_REQ_qt5-qttools-libs-designer = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-libs-designer = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RPM_SONAME_PROV_qt5-qttools-libs-designercomponents = "libQt5DesignerComponents.so.5"
RPM_SONAME_REQ_qt5-qttools-libs-designercomponents = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-libs-designercomponents = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common qt5-qttools-libs-designer"
RPM_SONAME_PROV_qt5-qttools-libs-help = "libQt5Help.so.5"
RPM_SONAME_REQ_qt5-qttools-libs-help = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-libs-help = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RDEPENDS_qt5-qttools-static = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qttools-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-assistant-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-designer-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-doctools-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-linguist-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qdbusviewer-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-common-5.12.5-1.el8.0.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-devel-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-examples-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-libs-designer-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-libs-designercomponents-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qttools-libs-help-5.12.5-1.el8.0.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qt5-qttools-static-5.12.5-1.el8.0.1.aarch64.rpm \
          "

SRC_URI[qt5-assistant.sha256sum] = "e5f5fc3494f74fa3509d42dc2c4efe90182829b55ec6a96fa6f78c17a99929ba"
SRC_URI[qt5-designer.sha256sum] = "d89763131631f7e82989892c1c896702244e096f0bd2f742d114a2e2176d4da9"
SRC_URI[qt5-doctools.sha256sum] = "b0fcf4998e8a3ed970b86b87e3917b3412b06185ecbe1b17bfdce4cbaba7fb0a"
SRC_URI[qt5-linguist.sha256sum] = "960ca07daf16bacd28b7fe4c4a71aa6d39237a96d6e319178eb58b0289148ce8"
SRC_URI[qt5-qdbusviewer.sha256sum] = "42de112018c7b2752ed3afff69c9dcc1c63b52b43301b9ac46db235fcb7a5e0e"
SRC_URI[qt5-qttools.sha256sum] = "c8d3066fae837d4ca9c8dbd7024260171ef0fb0a0d8686c662af7f23b390cc19"
SRC_URI[qt5-qttools-common.sha256sum] = "edaa0000c5ed9f5e19d25951eb7025cb84fd154bd2a4e4a93f347b1b3bbe89ba"
SRC_URI[qt5-qttools-devel.sha256sum] = "a76eb77d073b8ca983893553143b9aa8a22815761b82da7b36cd519762531ad8"
SRC_URI[qt5-qttools-examples.sha256sum] = "f876eda20c97425e2ad688ed03cbf62ea3596bb119eb45120315b421be7b209c"
SRC_URI[qt5-qttools-libs-designer.sha256sum] = "d1836a4b95597fcd0d39bef084aa2dc723ad1ba962c3677060db870feb7dbd52"
SRC_URI[qt5-qttools-libs-designercomponents.sha256sum] = "ad09376d68c36d15671ceb4b3ab217d6d600ebd5d4328dd519ad370492d40ec6"
SRC_URI[qt5-qttools-libs-help.sha256sum] = "a4f206b525251da8b3a82abfb62fa30e6337f12fd1c7165fbb2af92e369d2540"
SRC_URI[qt5-qttools-static.sha256sum] = "1299dc94939b18c8d4dfa66e3059e76288efab5863cd55a888bba04ec600b3f7"
