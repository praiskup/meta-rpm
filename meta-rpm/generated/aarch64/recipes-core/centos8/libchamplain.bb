SUMMARY = "generated recipe based on libchamplain srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo clutter clutter-gtk cogl gdk-pixbuf glib-2.0 gtk+3 libsoup-2.4 pango pkgconfig-native sqlite3"
RPM_SONAME_PROV_libchamplain = "libchamplain-0.12.so.0"
RPM_SONAME_REQ_libchamplain = "ld-linux-aarch64.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libcogl-path.so.20 libcogl.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0"
RDEPENDS_libchamplain = "cairo cairo-gobject clutter cogl gdk-pixbuf2 glib2 glibc gtk3 libsoup pango sqlite-libs"
RPM_SONAME_REQ_libchamplain-devel = "libchamplain-0.12.so.0 libchamplain-gtk-0.12.so.0"
RPROVIDES_libchamplain-devel = "libchamplain-dev (= 0.12.16)"
RDEPENDS_libchamplain-devel = "cairo-devel clutter-devel clutter-gtk-devel glib2-devel gtk3-devel libchamplain libchamplain-gtk libsoup-devel pkgconf-pkg-config sqlite-devel"
RPM_SONAME_PROV_libchamplain-gtk = "libchamplain-gtk-0.12.so.0"
RPM_SONAME_REQ_libchamplain-gtk = "ld-linux-aarch64.so.1 libc.so.6 libchamplain-0.12.so.0 libclutter-1.0.so.0 libclutter-gtk-1.0.so.0 libgdk-3.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpthread.so.0"
RDEPENDS_libchamplain-gtk = "clutter clutter-gtk glib2 glibc gtk3 libchamplain"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libchamplain-0.12.16-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libchamplain-devel-0.12.16-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libchamplain-gtk-0.12.16-2.el8.aarch64.rpm \
          "

SRC_URI[libchamplain.sha256sum] = "1c63e5b40306f505e3207f9d50a40b8c47d8688b12a70336eff99be2a461b1ef"
SRC_URI[libchamplain-devel.sha256sum] = "c26e980b8322a5ee1ede5d87f77f5eb0533522e680de19676b7006bb72a6a3e0"
SRC_URI[libchamplain-gtk.sha256sum] = "1af0849c0a7894d9e8e65d7d04d98c884c31991ed2ffc874a5a6005eb4799082"
