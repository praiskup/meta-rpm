SUMMARY = "generated recipe based on grep srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre pkgconfig-native"
RPM_SONAME_REQ_grep = "ld-linux-aarch64.so.1 libc.so.6 libpcre.so.1"
RDEPENDS_grep = "bash glibc info pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grep-3.1-6.el8.aarch64.rpm \
          "

SRC_URI[grep.sha256sum] = "7ffd6e95b0554466e97346b2f41fb5279aedcb29ae07828f63d06a8dedd7cd51"
