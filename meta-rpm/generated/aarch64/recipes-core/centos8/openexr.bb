SUMMARY = "generated recipe based on OpenEXR srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ilmbase libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_OpenEXR-devel = "libIlmImf-2_2.so.22 libIlmImfUtil-2_2.so.22"
RPROVIDES_OpenEXR-devel = "OpenEXR-dev (= 2.2.0)"
RDEPENDS_OpenEXR-devel = "OpenEXR-libs ilmbase-devel pkgconf-pkg-config"
RPM_SONAME_PROV_OpenEXR-libs = "libIlmImf-2_2.so.22 libIlmImfUtil-2_2.so.22"
RPM_SONAME_REQ_OpenEXR-libs = "ld-linux-aarch64.so.1 libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmImf-2_2.so.22 libIlmThread-2_2.so.12 libImath-2_2.so.12 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_OpenEXR-libs = "glibc ilmbase libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/OpenEXR-libs-2.2.0-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/OpenEXR-devel-2.2.0-11.el8.aarch64.rpm \
          "

SRC_URI[OpenEXR-devel.sha256sum] = "d999e2d2e8a346d435d900dc505588a8f030bd70ce656f7ddd30c339259a6da5"
SRC_URI[OpenEXR-libs.sha256sum] = "d0fbbd8b9be18c32d353d144e0c4a2820e5fe71177fb1eae3e90206c9c89ecca"
