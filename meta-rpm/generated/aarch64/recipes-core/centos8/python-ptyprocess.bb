SUMMARY = "generated recipe based on python-ptyprocess srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-ptyprocess = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-ptyprocess-0.5.2-4.el8.noarch.rpm \
          "

SRC_URI[python3-ptyprocess.sha256sum] = "499e48b35f3b5f5da45031fa78fba559fee6a480ecb106e6c300eb8344510958"
