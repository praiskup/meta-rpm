SUMMARY = "generated recipe based on pixman srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_pixman = "libpixman-1.so.0"
RPM_SONAME_REQ_pixman = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_pixman = "glibc"
RPM_SONAME_REQ_pixman-devel = "libpixman-1.so.0"
RPROVIDES_pixman-devel = "pixman-dev (= 0.38.4)"
RDEPENDS_pixman-devel = "pixman pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pixman-0.38.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pixman-devel-0.38.4-1.el8.aarch64.rpm \
          "

SRC_URI[pixman.sha256sum] = "9886953d4bc5b03f26b5c3164ce5b5fd86e9f80cf6358b91dd00f870f86052fe"
SRC_URI[pixman-devel.sha256sum] = "44c2f89cb4043467093f9b3218af33ab3430717578671baaec3372ac634492d4"
