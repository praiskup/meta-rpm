SUMMARY = "generated recipe based on openldap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib openssl pkgconfig-native"
RPM_SONAME_PROV_openldap = "liblber-2.4.so.2 libldap-2.4.so.2 libldap_r-2.4.so.2 libslapi-2.4.so.2"
RPM_SONAME_REQ_openldap = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 liblber-2.4.so.2 libpthread.so.0 libresolv.so.2 libsasl2.so.3 libssl.so.1.1"
RDEPENDS_openldap = "cyrus-sasl-lib glibc openssl-libs"
RPM_SONAME_REQ_openldap-clients = "ld-linux-aarch64.so.1 libc.so.6 liblber-2.4.so.2 libldap-2.4.so.2 libsasl2.so.3"
RDEPENDS_openldap-clients = "cyrus-sasl-lib glibc openldap"
RPM_SONAME_REQ_openldap-devel = "liblber-2.4.so.2 libldap-2.4.so.2 libldap_r-2.4.so.2 libslapi-2.4.so.2"
RPROVIDES_openldap-devel = "openldap-dev (= 2.4.46)"
RDEPENDS_openldap-devel = "cyrus-sasl-devel openldap"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openldap-2.4.46-11.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openldap-clients-2.4.46-11.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/openldap-devel-2.4.46-11.el8_1.aarch64.rpm \
          "

SRC_URI[openldap.sha256sum] = "11948242c0ac0a45fdffe142d15f658b0a2ebffafb12fb200175423733eaa765"
SRC_URI[openldap-clients.sha256sum] = "18c3cb2e9d4106560dba4fc55cdaf5932b5cfffa1290013563b54d29b8231870"
SRC_URI[openldap-devel.sha256sum] = "82e685c45fcee648bdd2d14199fe713b85c374e3b9aee6176ff4e599dd25b704"
