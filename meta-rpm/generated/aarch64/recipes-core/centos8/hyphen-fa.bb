SUMMARY = "generated recipe based on hyphen-fa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-fa = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-fa-0.20130404-9.el8.noarch.rpm \
          "

SRC_URI[hyphen-fa.sha256sum] = "659c723c310a35ff5f889fb46e2294bf911f8855221774bee76654dc8f60192f"
