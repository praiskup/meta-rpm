SUMMARY = "generated recipe based on readline srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_PROV_readline = "libhistory.so.7 libreadline.so.7"
RPM_SONAME_REQ_readline = "ld-linux-aarch64.so.1 libc.so.6 libtinfo.so.6"
RDEPENDS_readline = "bash glibc info ncurses-libs"
RPM_SONAME_REQ_readline-devel = "libhistory.so.7 libreadline.so.7"
RPROVIDES_readline-devel = "readline-dev (= 7.0)"
RDEPENDS_readline-devel = "bash info ncurses-devel readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/readline-7.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/readline-devel-7.0-10.el8.aarch64.rpm \
          "

SRC_URI[readline.sha256sum] = "ef74f2c65ed0e38dd021177d6e59fcdf7fb8de8929b7544b7a6f0709eff6562c"
SRC_URI[readline-devel.sha256sum] = "51c51644118f24afac0dd4e662bea7c97593468102018e2eda689a8d6e92f6f1"
