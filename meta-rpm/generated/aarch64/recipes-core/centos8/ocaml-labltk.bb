SUMMARY = "generated recipe based on ocaml-labltk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native tcl tk"
RPM_SONAME_REQ_ocaml-labltk = "libX11.so.6 libc.so.6 libtcl8.6.so libtk8.6.so"
RDEPENDS_ocaml-labltk = "glibc libX11 ocaml-runtime tcl tk"
RPROVIDES_ocaml-labltk-devel = "ocaml-labltk-dev (= 8.06.4)"
RDEPENDS_ocaml-labltk-devel = "bash ocaml-labltk ocaml-runtime"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-labltk-8.06.4-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-labltk-devel-8.06.4-7.el8.aarch64.rpm \
          "

SRC_URI[ocaml-labltk.sha256sum] = "c4a4e7f4f05839caa0669a5c383796a609a7e828e0ea5449dd44ff92f2b0790a"
SRC_URI[ocaml-labltk-devel.sha256sum] = "b269585876c4cae25ba8762e051af6593368fab46663a29651b8475d1bbc6bf1"
