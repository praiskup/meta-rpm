SUMMARY = "generated recipe based on sgpio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_sgpio = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_sgpio = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sgpio-1.2.0.10-21.el8.aarch64.rpm \
          "

SRC_URI[sgpio.sha256sum] = "55243cda51910f1f5a35e81e9e28a37ba8ddb0f58c986067ea03370c70d2e8b5"
