SUMMARY = "generated recipe based on fetchmail srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs libxcrypt openssl pkgconfig-native"
RPM_SONAME_REQ_fetchmail = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libssl.so.1.1"
RDEPENDS_fetchmail = "glibc krb5-libs libcom_err libxcrypt openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fetchmail-6.3.26-19.el8.aarch64.rpm \
          "

SRC_URI[fetchmail.sha256sum] = "2bd650e6ddcf2f7417821d7bc9871f5ee0f66ab06a09676b9b3f0f32afc73799"
