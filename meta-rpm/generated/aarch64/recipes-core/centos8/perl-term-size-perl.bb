SUMMARY = "generated recipe based on perl-Term-Size-Perl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Size-Perl = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Term-Size-Perl-0.031-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Term-Size-Perl.sha256sum] = "fa5592036264bb584da72868d1dcf71b0a6ddfa98341563d77734ec79ee4ad76"
