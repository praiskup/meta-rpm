SUMMARY = "generated recipe based on userspace-rcu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_userspace-rcu = "liburcu-bp.so.6 liburcu-cds.so.6 liburcu-common.so.6 liburcu-mb.so.6 liburcu-qsbr.so.6 liburcu-signal.so.6 liburcu.so.6"
RPM_SONAME_REQ_userspace-rcu = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 liburcu-common.so.6"
RDEPENDS_userspace-rcu = "glibc"
RPM_SONAME_REQ_userspace-rcu-devel = "liburcu-bp.so.6 liburcu-cds.so.6 liburcu-common.so.6 liburcu-mb.so.6 liburcu-qsbr.so.6 liburcu-signal.so.6 liburcu.so.6"
RPROVIDES_userspace-rcu-devel = "userspace-rcu-dev (= 0.10.1)"
RDEPENDS_userspace-rcu-devel = "pkgconf-pkg-config userspace-rcu"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/userspace-rcu-0.10.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/userspace-rcu-devel-0.10.1-2.el8.aarch64.rpm \
          "

SRC_URI[userspace-rcu.sha256sum] = "f15c53be4a191a5073c76e49a9c479b003545c9e9cc5b31871ff6e07751ff326"
SRC_URI[userspace-rcu-devel.sha256sum] = "db27cead74e6a42096114520ae8882ca00cb224be39083feac7f2f7da60f8033"
