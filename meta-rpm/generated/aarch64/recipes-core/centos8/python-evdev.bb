SUMMARY = "generated recipe based on python-evdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-evdev = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-evdev = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-evdev-1.1.2-3.el8.aarch64.rpm \
          "

SRC_URI[python3-evdev.sha256sum] = "80d3dd6d7020749e086b50f30114dfbc8cc6fab401e0fe741487992d1f3f1356"
