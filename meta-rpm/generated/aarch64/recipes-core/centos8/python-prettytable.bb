SUMMARY = "generated recipe based on python-prettytable srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-prettytable = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-prettytable-0.7.2-14.el8.noarch.rpm \
          "

SRC_URI[python3-prettytable.sha256sum] = "1b53c868277dec628058b31b1a8a8a2ccd8efe832ce9694805b5f82564136eb3"
