SUMMARY = "generated recipe based on v4l-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libjpeg-turbo pkgconfig-native"
RPM_SONAME_PROV_libv4l = "libv4l-mplane.so libv4l1.so.0 libv4l2.so.0 libv4l2rds.so.0 libv4lconvert.so.0"
RPM_SONAME_REQ_libv4l = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libjpeg.so.62 libm.so.6 libpthread.so.0 librt.so.1 libv4l1.so.0 libv4l2.so.0 libv4lconvert.so.0"
RDEPENDS_libv4l = "glibc libjpeg-turbo"
RPM_SONAME_REQ_libv4l-devel = "libv4l1.so.0 libv4l2.so.0 libv4l2rds.so.0 libv4lconvert.so.0"
RPROVIDES_libv4l-devel = "libv4l-dev (= 1.14.2)"
RDEPENDS_libv4l-devel = "libv4l pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libv4l-1.14.2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libv4l-devel-1.14.2-3.el8.aarch64.rpm \
          "

SRC_URI[libv4l.sha256sum] = "eee7794def44e7bcebc33fa9fcad7d3cacc348d6bdbbf87a164ed63290766489"
SRC_URI[libv4l-devel.sha256sum] = "2a06c0ac1b1309d27e901df0646f19cd8bf45442a7d1aae26895cf1286ee7f32"
