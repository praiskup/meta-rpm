SUMMARY = "generated recipe based on lua-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lua-filesystem = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lua-filesystem = "glibc lua"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lua-filesystem-1.6.3-7.el8.aarch64.rpm \
          "

SRC_URI[lua-filesystem.sha256sum] = "91ead55ecaf8eeed24637c9ba2ad230836dcf18a586f1a810c854d3313b0b753"
