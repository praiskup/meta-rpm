SUMMARY = "generated recipe based on avahi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs pkgconfig-native"
RPM_SONAME_REQ_avahi-devel = "libavahi-client.so.3 libavahi-common.so.3 libavahi-core.so.7"
RPROVIDES_avahi-devel = "avahi-dev (= 0.7)"
RDEPENDS_avahi-devel = "avahi avahi-libs pkgconf-pkg-config"
RPM_SONAME_PROV_avahi-libs = "libavahi-client.so.3 libavahi-common.so.3"
RPM_SONAME_REQ_avahi-libs = "ld-linux-aarch64.so.1 libavahi-common.so.3 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_avahi-libs = "dbus-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/avahi-libs-0.7-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/avahi-devel-0.7-19.el8.aarch64.rpm \
          "

SRC_URI[avahi-devel.sha256sum] = "18fb3e736d22216dc4ddaf4515522da6b59fb68070105ad9056643ab6b9667ce"
SRC_URI[avahi-libs.sha256sum] = "77f640f1e5880c900790c7c45f3b4ec0355fd3e02077bf6b874e0631ef98346e"
