SUMMARY = "generated recipe based on libmetalink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat pkgconfig-native"
RPM_SONAME_PROV_libmetalink = "libmetalink.so.3"
RPM_SONAME_REQ_libmetalink = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1"
RDEPENDS_libmetalink = "expat glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmetalink-0.1.3-7.el8.aarch64.rpm \
          "

SRC_URI[libmetalink.sha256sum] = "b86423694dd6d12a0b608760046ef18f6ee97f96cb8ad661ace419a45525e200"
