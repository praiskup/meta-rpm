SUMMARY = "generated recipe based on valgrind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_valgrind = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0"
RDEPENDS_valgrind = "glibc libgcc perl-interpreter"
RPROVIDES_valgrind-devel = "valgrind-dev (= 3.15.0)"
RDEPENDS_valgrind-devel = "pkgconf-pkg-config valgrind"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/valgrind-3.15.0-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/valgrind-devel-3.15.0-11.el8.aarch64.rpm \
          "

SRC_URI[valgrind.sha256sum] = "f243392113a738e5781c96714bea34dc369af5df9bbc9232941955ce97d5b064"
SRC_URI[valgrind-devel.sha256sum] = "d567304a7f56274488917409d5198105230335ebb5dc9426290f40a3f6729e26"
