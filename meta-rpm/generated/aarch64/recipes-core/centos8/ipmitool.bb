SUMMARY = "generated recipe based on ipmitool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses openssl pkgconfig-native readline"
RDEPENDS_exchange-bmc-os-info = "bash hostname ipmitool systemd"
RPM_SONAME_REQ_ipmievd = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_ipmievd = "bash glibc ipmitool ncurses-libs openssl-libs readline systemd"
RPM_SONAME_REQ_ipmitool = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_ipmitool = "bash glibc ncurses-libs openssl-libs readline systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/exchange-bmc-os-info-1.8.18-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ipmievd-1.8.18-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ipmitool-1.8.18-14.el8.aarch64.rpm \
          "

SRC_URI[exchange-bmc-os-info.sha256sum] = "6ce74a39e46570048b59b70392325e3811caa460ba28979ae666b1b8a4c283de"
SRC_URI[ipmievd.sha256sum] = "14258448b2baf9c24e7ba5f4ae2fb47bf0414c075b8805fea46bea6a8334a488"
SRC_URI[ipmitool.sha256sum] = "2bfd750f9c3a210ca409e6a870b0ebc4595552bf5855718914049636b69b16d7"
