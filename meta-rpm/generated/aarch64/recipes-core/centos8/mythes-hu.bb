SUMMARY = "generated recipe based on mythes-hu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-hu = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-hu-0.20101019-15.el8.noarch.rpm \
          "

SRC_URI[mythes-hu.sha256sum] = "e69808c677f3ef4150abbcbdb8bf6917d3d42cccfeefc34645a6bf2d68e3bb2c"
