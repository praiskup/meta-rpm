SUMMARY = "generated recipe based on perl-Sub-Info srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Info = "perl-Carp perl-Importer perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Sub-Info-0.002-5.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Info.sha256sum] = "e42bf20e0cf982de102bdfb8582ca2a6405fd32aeaae877b38a03ade6a57af8e"
