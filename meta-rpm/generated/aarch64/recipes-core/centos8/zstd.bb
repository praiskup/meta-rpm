SUMMARY = "generated recipe based on zstd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libzstd = "libzstd.so.1"
RPM_SONAME_REQ_libzstd = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libzstd = "glibc"
RPM_SONAME_REQ_libzstd-devel = "libzstd.so.1"
RPROVIDES_libzstd-devel = "libzstd-dev (= 1.4.2)"
RDEPENDS_libzstd-devel = "libzstd pkgconf-pkg-config"
RPM_SONAME_REQ_zstd = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_zstd = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/zstd-1.4.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libzstd-1.4.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libzstd-devel-1.4.2-2.el8.aarch64.rpm \
          "

SRC_URI[libzstd.sha256sum] = "c5db97dc6a99dd32e1bc02eaae7d3cf497aa28a7cdd48eea93ae15cb848732a8"
SRC_URI[libzstd-devel.sha256sum] = "6e5ac79b1b5a1b2c9b44f843e561637727117df1d33eb363c76153db68575511"
SRC_URI[zstd.sha256sum] = "4dce0bfd58c00eabae504c4e6c55a42458172f356b96c6c901119a2efa6aff25"
