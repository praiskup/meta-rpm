SUMMARY = "generated recipe based on libidn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libidn = "libidn.so.11"
RPM_SONAME_REQ_libidn = "ld-linux-aarch64.so.1 libc.so.6 libidn.so.11"
RDEPENDS_libidn = "bash emacs-filesystem glibc info"
RPM_SONAME_REQ_libidn-devel = "libidn.so.11"
RPROVIDES_libidn-devel = "libidn-dev (= 1.34)"
RDEPENDS_libidn-devel = "libidn pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libidn-1.34-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libidn-devel-1.34-5.el8.aarch64.rpm \
          "

SRC_URI[libidn.sha256sum] = "aa969cc7680e8cd6f5c5eb2e905ab4e72a164ed770ad225c365af08f9d3247fc"
SRC_URI[libidn-devel.sha256sum] = "45b0965b1a3ea55d494ba3447f9933fcc17af472ed398b3ccfc52d21615d8a20"
