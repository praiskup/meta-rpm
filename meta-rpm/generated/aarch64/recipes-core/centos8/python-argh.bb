SUMMARY = "generated recipe based on python-argh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-argh = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-argh-0.26.1-8.el8.noarch.rpm \
          "

SRC_URI[python3-argh.sha256sum] = "3aeeafbdb7771f12d5823dc7e0bbb7400fd0c6527a2945afb31c25cbd6d1cd39"
