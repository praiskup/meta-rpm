SUMMARY = "generated recipe based on whois srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libidn2 pkgconfig-native"
RPM_SONAME_REQ_whois = "ld-linux-aarch64.so.1 libc.so.6 libidn2.so.0"
RDEPENDS_whois = "bash chkconfig glibc libidn2 whois-nls"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/whois-5.5.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/whois-nls-5.5.1-2.el8.noarch.rpm \
          "

SRC_URI[whois.sha256sum] = "77b7cbc013dc8ebf63508569849168e21d33b1c029a563309e442a47811963e2"
SRC_URI[whois-nls.sha256sum] = "93f5ad20f63fd1cf4681c22afe470166dbd40b75f442cb368b0155262c7afbf0"
