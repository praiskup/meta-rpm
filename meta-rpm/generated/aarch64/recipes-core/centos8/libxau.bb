SUMMARY = "generated recipe based on libXau srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xau"
DEPENDS = "pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXau = "libXau.so.6"
RPM_SONAME_REQ_libXau = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libXau = "glibc"
RPM_SONAME_REQ_libXau-devel = "libXau.so.6"
RPROVIDES_libXau-devel = "libXau-dev (= 1.0.8)"
RDEPENDS_libXau-devel = "libXau pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXau-1.0.8-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXau-devel-1.0.8-13.el8.aarch64.rpm \
          "

SRC_URI[libXau.sha256sum] = "44f86b410396099845f2aeef4bf43a5dd5059a3f1edd372aad19e3ae44ac9adc"
SRC_URI[libXau-devel.sha256sum] = "37260c425bd07f454495a5fe23b86211afccbdaa2f0a19d541bcede334e28c90"
