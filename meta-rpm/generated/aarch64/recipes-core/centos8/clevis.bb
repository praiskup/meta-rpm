SUMMARY = "generated recipe based on clevis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit cryptsetup-libs glib-2.0 jansson jose libgcc luksmeta openssl pkgconfig-native udisks2"
RPM_SONAME_REQ_clevis = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libjansson.so.4 libjose.so.0"
RDEPENDS_clevis = "bash coreutils curl glibc jansson jose libgcc libjose openssl-libs shadow-utils tpm2-tools"
RDEPENDS_clevis-dracut = "bash clevis-systemd dracut-network"
RDEPENDS_clevis-luks = "bash clevis cryptsetup luksmeta"
RDEPENDS_clevis-systemd = "bash clevis-luks nmap-ncat systemd"
RPM_SONAME_REQ_clevis-udisks2 = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcryptsetup.so.12 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjansson.so.4 libluksmeta.so.0 libudisks2.so.0"
RDEPENDS_clevis-udisks2 = "audit-libs clevis-luks cryptsetup-libs glib2 glibc jansson libgcc libluksmeta libudisks2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clevis-11-9.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clevis-dracut-11-9.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clevis-luks-11-9.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clevis-systemd-11-9.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/clevis-udisks2-11-9.el8_2.1.aarch64.rpm \
          "

SRC_URI[clevis.sha256sum] = "ebf4e135b726b6c0d5dfb45e5bf82c659f3d452887d8c97c0cd98e470e5f54dd"
SRC_URI[clevis-dracut.sha256sum] = "2f51c577039b68fb80dffd7cac705aea469b5dcb38ee7de51b1ccd2a3639b226"
SRC_URI[clevis-luks.sha256sum] = "30e6a02c07d30b8b4b07189ea421c1169cbd33677a5f062e40184b223a87f561"
SRC_URI[clevis-systemd.sha256sum] = "c3e1388d86faecb10132d9b2363a98e2a8cfb022c8688e2cf131f230e355ce13"
SRC_URI[clevis-udisks2.sha256sum] = "6ac34ed6874a90359d88ed2922864cadc4dac25f2499f8f63c5914b6feb86cc1"
