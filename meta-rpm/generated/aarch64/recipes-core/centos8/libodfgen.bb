SUMMARY = "generated recipe based on libodfgen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libodfgen = "libodfgen-0.1.so.1"
RPM_SONAME_REQ_libodfgen = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 librevenge-stream-0.0.so.0 libstdc++.so.6"
RDEPENDS_libodfgen = "glibc libgcc librevenge libstdc++"
RPM_SONAME_REQ_libodfgen-devel = "libodfgen-0.1.so.1"
RPROVIDES_libodfgen-devel = "libodfgen-dev (= 0.1.6)"
RDEPENDS_libodfgen-devel = "libodfgen librevenge-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libodfgen-0.1.6-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libodfgen-devel-0.1.6-11.el8.aarch64.rpm \
          "

SRC_URI[libodfgen.sha256sum] = "c8c818d27a7cce1a058b12e8f1e7c56d9cfa1251da150f6fc0fd17041549a8a5"
SRC_URI[libodfgen-devel.sha256sum] = "5b496b23eb1180355ec1f64e96f2ed5d44f5b11799899c8f04f5a7857252c4ba"
