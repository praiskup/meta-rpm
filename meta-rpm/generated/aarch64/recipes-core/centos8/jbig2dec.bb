SUMMARY = "generated recipe based on jbig2dec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_jbig2dec-libs = "libjbig2dec.so.0"
RPM_SONAME_REQ_jbig2dec-libs = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_jbig2dec-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jbig2dec-libs-0.14-4.el8_2.aarch64.rpm \
          "

SRC_URI[jbig2dec-libs.sha256sum] = "c030cce399e0adfc9f408e71432f3357f1ef72f174de9b67bcca1a0db51ca3cd"
