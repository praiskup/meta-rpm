SUMMARY = "generated recipe based on objenesis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_objenesis = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_objenesis-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/objenesis-2.6-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/objenesis-javadoc-2.6-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[objenesis.sha256sum] = "a543ef887323635333a5a5f4b5a531522a356f91abbd538e249d65e5c8f6d17c"
SRC_URI[objenesis-javadoc.sha256sum] = "cbf4aa4a5f83d05212d21f5ee8a3b663408f0aa9ccb360008b3541a5a1e610e8"
