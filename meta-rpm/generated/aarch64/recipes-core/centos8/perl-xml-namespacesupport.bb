SUMMARY = "generated recipe based on perl-XML-NamespaceSupport srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-NamespaceSupport = "perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-NamespaceSupport-1.12-4.el8.noarch.rpm \
          "

SRC_URI[perl-XML-NamespaceSupport.sha256sum] = "e681139a5ec9640931ab532edfa71a58e6d18300398bbed5c5d5e07bc00576da"
