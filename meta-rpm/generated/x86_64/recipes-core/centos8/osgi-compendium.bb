SUMMARY = "generated recipe based on osgi-compendium srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_osgi-compendium = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_osgi-compendium-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/osgi-compendium-6.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/osgi-compendium-javadoc-6.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[osgi-compendium.sha256sum] = "94dedd6e4fa6804e2665d022597cdbe1d346eba38bde91d0e6c763123a1d973a"
SRC_URI[osgi-compendium-javadoc.sha256sum] = "f771befd9e7c0a1bb667eafde6ae47472efa12ad9aa1d514248bfe22cdce276d"
