SUMMARY = "generated recipe based on hyphen-da srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-da = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-da-0.20070903-17.el8.noarch.rpm \
          "

SRC_URI[hyphen-da.sha256sum] = "6cadc9ab8774c8607d28c95f8fca77c867c27cb5c7bf02706159fca5382da0a4"
