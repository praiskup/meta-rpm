SUMMARY = "generated recipe based on grep srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre pkgconfig-native"
RPM_SONAME_REQ_grep = "libc.so.6 libpcre.so.1"
RDEPENDS_grep = "bash glibc info pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grep-3.1-6.el8.x86_64.rpm \
          "

SRC_URI[grep.sha256sum] = "3f8ffe48bb481a5db7cbe42bf73b839d872351811e5df41b2f6697c61a030487"
