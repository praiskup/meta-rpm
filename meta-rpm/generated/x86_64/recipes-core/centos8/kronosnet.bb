SUMMARY = "generated recipe based on kronosnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libknet1 = "libknet.so.1"
RPM_SONAME_REQ_libknet1 = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_libknet1 = "glibc"
RPM_SONAME_REQ_libknet1-devel = "libknet.so.1"
RPROVIDES_libknet1-devel = "libknet1-dev (= 1.10)"
RDEPENDS_libknet1-devel = "libknet1 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libknet1-1.10-6.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libknet1-devel-1.10-6.el8_2.x86_64.rpm \
          "

SRC_URI[libknet1.sha256sum] = "2f1744d9132f7d110d7da2acab00c539faf4177231fd6c850a99e9f387217c60"
SRC_URI[libknet1-devel.sha256sum] = "0a3e8e75709298fa23dd8f930a2f47f3b4ce4f5747e75a4dade814be13a7b626"
