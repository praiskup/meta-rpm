SUMMARY = "generated recipe based on python-psutil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-psutil = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-psutil = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-psutil-5.4.3-10.el8.x86_64.rpm \
          "

SRC_URI[python3-psutil.sha256sum] = "d511660482af2fe44cd497a734406f79ceb4fa59618bab7bdeda67ac39aa3730"
