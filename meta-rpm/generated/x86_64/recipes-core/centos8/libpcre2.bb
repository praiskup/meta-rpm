SUMMARY = "generated recipe based on pcre2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "pcre2"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_pcre2 = "libpcre2-8.so.0 libpcre2-posix.so.2"
RPM_SONAME_REQ_pcre2 = "libc.so.6 libpcre2-8.so.0 libpthread.so.0"
RDEPENDS_pcre2 = "glibc"
RPM_SONAME_REQ_pcre2-devel = "libpcre2-16.so.0 libpcre2-32.so.0 libpcre2-8.so.0 libpcre2-posix.so.2"
RPROVIDES_pcre2-devel = "pcre2-dev (= 10.32)"
RDEPENDS_pcre2-devel = "bash pcre2 pcre2-utf16 pcre2-utf32 pkgconf-pkg-config"
RPM_SONAME_PROV_pcre2-utf16 = "libpcre2-16.so.0"
RPM_SONAME_REQ_pcre2-utf16 = "libc.so.6 libpthread.so.0"
RDEPENDS_pcre2-utf16 = "glibc"
RPM_SONAME_PROV_pcre2-utf32 = "libpcre2-32.so.0"
RPM_SONAME_REQ_pcre2-utf32 = "libc.so.6 libpthread.so.0"
RDEPENDS_pcre2-utf32 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre2-10.32-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre2-devel-10.32-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre2-utf16-10.32-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre2-utf32-10.32-1.el8.x86_64.rpm \
          "
SRC_URI = "file://pcre2-10.32-1.el8.patch"

SRC_URI[pcre2.sha256sum] = "bb081744c54f31862e62485e08608ab021c15ab9fe0c5e16fe0980830e01cb4c"
SRC_URI[pcre2-devel.sha256sum] = "6debfcffe28ad434de3cdda76bad55867659e84bb1f8bb6aafd74814af5cbd48"
SRC_URI[pcre2-utf16.sha256sum] = "7aceadd20b9024170c64384978f7596111addb57bd3123465fcd492ad0a6c76f"
SRC_URI[pcre2-utf32.sha256sum] = "36f79f9a81870b5d7f5c7277090bae94843737ee24f977822a213dcc010fc25a"
