SUMMARY = "generated recipe based on libwpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libwpd = "libwpd-0.10.so.10"
RPM_SONAME_REQ_libwpd = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libwpd = "glibc libgcc librevenge libstdc++"
RPM_SONAME_REQ_libwpd-devel = "libwpd-0.10.so.10"
RPROVIDES_libwpd-devel = "libwpd-dev (= 0.10.2)"
RDEPENDS_libwpd-devel = "librevenge-devel libwpd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwpd-0.10.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwpd-devel-0.10.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwpd-doc-0.10.2-2.el8.noarch.rpm \
          "

SRC_URI[libwpd.sha256sum] = "f7cc8364b6f08241e9f889b9ef8652f3db75be46ac416632cee94c72e6e2016b"
SRC_URI[libwpd-devel.sha256sum] = "aa094f01f3180641ba758ef2dc27f31b219886080b55c8a8cd69e4db8103be39"
SRC_URI[libwpd-doc.sha256sum] = "a14c7cad6c6c0b2ae75c644674e538b42b1ec833af6586452af3fe54146e04f7"
