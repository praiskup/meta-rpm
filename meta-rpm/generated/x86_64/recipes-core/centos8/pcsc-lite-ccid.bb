SUMMARY = "generated recipe based on pcsc-lite-ccid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_PROV_pcsc-lite-ccid = "libccid.so libccidtwin.so"
RPM_SONAME_REQ_pcsc-lite-ccid = "libc.so.6 libpthread.so.0 libusb-1.0.so.0"
RDEPENDS_pcsc-lite-ccid = "bash glibc libusbx pcsc-lite systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcsc-lite-ccid-1.4.29-3.el8.x86_64.rpm \
          "

SRC_URI[pcsc-lite-ccid.sha256sum] = "fe3d9485f3441927bc0733ce6b340d5cadb6b26ed05e1a8de10751472c851d48"
