SUMMARY = "generated recipe based on ansible-freeipa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ansible-freeipa = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ansible-freeipa-0.1.8-3.el8.noarch.rpm \
          "

SRC_URI[ansible-freeipa.sha256sum] = "79d4db895c561faee3cdd69ee8a583a74e95a7144ce3dbf452399dd025a00f01"
