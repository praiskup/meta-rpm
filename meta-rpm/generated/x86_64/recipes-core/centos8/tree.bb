SUMMARY = "generated recipe based on tree srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_tree = "libc.so.6"
RDEPENDS_tree = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tree-1.7.0-15.el8.x86_64.rpm \
          "

SRC_URI[tree.sha256sum] = "b5969c16b012fc777922ca87069d03f3f9e12486498e8beb969f4aeb9f51e12b"
