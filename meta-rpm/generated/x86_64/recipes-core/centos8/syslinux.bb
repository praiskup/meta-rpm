SUMMARY = "generated recipe based on syslinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libuuid pkgconfig-native"
RPM_SONAME_REQ_syslinux = "libc.so.6 libuuid.so.1"
RDEPENDS_syslinux = "glibc libuuid mtools syslinux-nonlinux"
RPM_SONAME_REQ_syslinux-extlinux = "libc.so.6"
RDEPENDS_syslinux-extlinux = "bash glibc syslinux syslinux-extlinux-nonlinux"
RDEPENDS_syslinux-extlinux-nonlinux = "syslinux"
RDEPENDS_syslinux-nonlinux = "syslinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/syslinux-6.04-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/syslinux-extlinux-6.04-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/syslinux-extlinux-nonlinux-6.04-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/syslinux-nonlinux-6.04-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/syslinux-tftpboot-6.04-4.el8.noarch.rpm \
          "

SRC_URI[syslinux.sha256sum] = "92f6e3cfab18a977ec9861a7ca10fbea552f5ad7ef3d633d219103fd5833d2e9"
SRC_URI[syslinux-extlinux.sha256sum] = "d09aa43a4c3cd3a93356246bf15bc4672c8698dca61158d1032b46ba3d8dddc4"
SRC_URI[syslinux-extlinux-nonlinux.sha256sum] = "45ebbd7a54750c8cfdafb2be90b432010c2cf1832b6f2bf77cbfe20869de36cf"
SRC_URI[syslinux-nonlinux.sha256sum] = "44de896aeb4419b7eb3d9448c4c9a0f5c58baaf28b5c591a08daae7f260ba82a"
SRC_URI[syslinux-tftpboot.sha256sum] = "df7ea712950f6a7b4c20150e1a982469d26ef7faa3df0dde4e377096e54cc740"
