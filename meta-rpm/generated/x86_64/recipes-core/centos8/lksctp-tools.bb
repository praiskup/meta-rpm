SUMMARY = "generated recipe based on lksctp-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lksctp-tools = "libsctp.so.1 libwithsctp.so.1"
RPM_SONAME_REQ_lksctp-tools = "libc.so.6 libdl.so.2 libsctp.so.1"
RDEPENDS_lksctp-tools = "bash glibc"
RPM_SONAME_REQ_lksctp-tools-devel = "libsctp.so.1 libwithsctp.so.1"
RPROVIDES_lksctp-tools-devel = "lksctp-tools-dev (= 1.0.18)"
RDEPENDS_lksctp-tools-devel = "lksctp-tools pkgconf-pkg-config"
RDEPENDS_lksctp-tools-doc = "lksctp-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lksctp-tools-1.0.18-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lksctp-tools-devel-1.0.18-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lksctp-tools-doc-1.0.18-3.el8.x86_64.rpm \
          "

SRC_URI[lksctp-tools.sha256sum] = "c4c003a0fd67ce59c9f0604c21a0c3966023aa702009298afbb13bc30b5b8bbc"
SRC_URI[lksctp-tools-devel.sha256sum] = "c13baa6f8f747b01b85c701137a5b0a0d8cf0db33eec8c8acda2cc6ea72a6182"
SRC_URI[lksctp-tools-doc.sha256sum] = "a7e6b53745033261c33797b94368d4861bebd8db66c78fabd68375b78be0b2a1"
