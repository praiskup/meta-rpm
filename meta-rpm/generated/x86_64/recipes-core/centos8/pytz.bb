SUMMARY = "generated recipe based on pytz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pytz = "platform-python tzdata"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pytz-2017.2-9.el8.noarch.rpm \
          "

SRC_URI[python3-pytz.sha256sum] = "0edde19e0c027c8cff31b82e1f4b0b198c00bf924047fcf4dd610a7d4965ab52"
