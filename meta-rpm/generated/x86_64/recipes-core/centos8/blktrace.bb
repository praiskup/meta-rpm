SUMMARY = "generated recipe based on blktrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libaio pkgconfig-native"
RPM_SONAME_REQ_blktrace = "libaio.so.1 libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_blktrace = "bash glibc libaio platform-python"
RPM_SONAME_REQ_iowatcher = "libc.so.6 libm.so.6 librt.so.1"
RDEPENDS_iowatcher = "blktrace glibc librsvg2-tools sysstat theora-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/iowatcher-1.2.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/blktrace-1.2.0-10.el8.x86_64.rpm \
          "

SRC_URI[blktrace.sha256sum] = "9cd1f0b39dc77039a56b2e58ab21a57b6669cbd7be0cb16acfae5d11ecec114b"
SRC_URI[iowatcher.sha256sum] = "c3ab819d09c7a888038c9a52df4131c79e291f7d8f85051d8d349811e3337c92"
