SUMMARY = "generated recipe based on totem-pl-parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libarchive libgcrypt libquvi libxml2 pkgconfig-native"
RPM_SONAME_PROV_totem-pl-parser = "libtotem-plparser-mini.so.18 libtotem-plparser.so.18"
RPM_SONAME_REQ_totem-pl-parser = "libarchive.so.13 libc.so.6 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libquvi-0.9-0.9.4.so libxml2.so.2"
RDEPENDS_totem-pl-parser = "glib2 glibc libarchive libgcrypt libquvi libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/totem-pl-parser-3.26.1-2.el8.x86_64.rpm \
          "

SRC_URI[totem-pl-parser.sha256sum] = "d5e6c07323adf3c53d7ce73300d7d31f44e98a8f61d3a6b8c98b6c26b69d834e"
