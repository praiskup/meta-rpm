SUMMARY = "generated recipe based on hicolor-icon-theme srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hicolor-icon-theme = "bash coreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hicolor-icon-theme-0.17-2.el8.noarch.rpm \
          "

SRC_URI[hicolor-icon-theme.sha256sum] = "22cfaa4da6a1d6c8026dc96078c6cb681c9d1b17dc09116a87d5bd4641ab799f"
