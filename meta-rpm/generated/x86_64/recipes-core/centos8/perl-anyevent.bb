SUMMARY = "generated recipe based on perl-AnyEvent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-AnyEvent = "perl-Carp perl-Errno perl-Exporter perl-File-Temp perl-Net-SSLeay perl-Scalar-List-Utils perl-Socket perl-Time-HiRes perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-AnyEvent-7.14-6.el8.x86_64.rpm \
          "

SRC_URI[perl-AnyEvent.sha256sum] = "0d1930bbdcd090965c846aeaeac6bcb8eca240c9115b72086085608eefd712ec"
