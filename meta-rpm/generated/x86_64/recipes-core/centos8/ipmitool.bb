SUMMARY = "generated recipe based on ipmitool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses openssl pkgconfig-native readline"
RDEPENDS_exchange-bmc-os-info = "bash hostname ipmitool systemd"
RPM_SONAME_REQ_ipmievd = "libc.so.6 libcrypto.so.1.1 libm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_ipmievd = "bash glibc ipmitool ncurses-libs openssl-libs readline systemd"
RPM_SONAME_REQ_ipmitool = "libc.so.6 libcrypto.so.1.1 libm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_ipmitool = "bash glibc ncurses-libs openssl-libs readline systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/exchange-bmc-os-info-1.8.18-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ipmievd-1.8.18-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ipmitool-1.8.18-14.el8.x86_64.rpm \
          "

SRC_URI[exchange-bmc-os-info.sha256sum] = "6ce74a39e46570048b59b70392325e3811caa460ba28979ae666b1b8a4c283de"
SRC_URI[ipmievd.sha256sum] = "59570cf7e93a5bebc886bbd97c587e200a5efb4f24e3bb646c48290b523870d1"
SRC_URI[ipmitool.sha256sum] = "6c1ce9ab7ac367a84ee1bb40636225b895e0f7639cfd2293e526cec41f01ebfe"
