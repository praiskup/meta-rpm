SUMMARY = "generated recipe based on python-snowballstemmer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-snowballstemmer = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-snowballstemmer-1.2.1-6.el8.noarch.rpm \
          "

SRC_URI[python3-snowballstemmer.sha256sum] = "d5dbe0d4104974537ced345acdaac2a01eb03f469af560278b495f7c08139e4d"
