SUMMARY = "generated recipe based on xml-commons-apis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xml-commons-apis = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_xml-commons-apis-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xml-commons-apis-1.4.01-25.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xml-commons-apis-javadoc-1.4.01-25.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xml-commons-apis-manual-1.4.01-25.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xml-commons-apis.sha256sum] = "f2dfb90249923b4782e7356ebc5817c87d90d311c7f709d1fac40cab5dc4e648"
SRC_URI[xml-commons-apis-javadoc.sha256sum] = "87131afaf97d26026823529e76a2eab4d9d4e91f0b0887b308382c4b8b3b3cc9"
SRC_URI[xml-commons-apis-manual.sha256sum] = "516e491aca972a31769dfdd043c5e5db41ec1ab973a4c59e3403aaeb3d076de9"
