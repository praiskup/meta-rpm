SUMMARY = "generated recipe based on abrt-java-connector srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "abrt glib-2.0 libreport pkgconfig-native rpm satyr systemd-libs"
RPM_SONAME_PROV_abrt-java-connector = "libabrt-java-connector.so"
RPM_SONAME_REQ_abrt-java-connector = "libabrt.so.0 libc.so.6 libglib-2.0.so.0 libreport.so.0 librpm.so.8 librpmio.so.8 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-java-connector = "abrt abrt-libs glib2 glibc libreport rpm-libs satyr systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-java-connector-1.1.0-16.el8.x86_64.rpm \
          "

SRC_URI[abrt-java-connector.sha256sum] = "2eb464a9cd43420731e7528aa866f2a118acf89b5db673c449141787d301e802"
