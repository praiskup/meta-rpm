SUMMARY = "generated recipe based on lsof srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux libtirpc pkgconfig-native"
RPM_SONAME_REQ_lsof = "libc.so.6 libselinux.so.1 libtirpc.so.3"
RDEPENDS_lsof = "glibc libselinux libtirpc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lsof-4.91-2.el8.x86_64.rpm \
          "

SRC_URI[lsof.sha256sum] = "cace28927a2c7cb542e06149c33e2b8c3eb8672d83dc893de0045de996a98484"
