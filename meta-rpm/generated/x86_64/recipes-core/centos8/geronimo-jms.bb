SUMMARY = "generated recipe based on geronimo-jms srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_geronimo-jms = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_geronimo-jms-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-jms-1.1.1-25.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-jms-javadoc-1.1.1-25.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[geronimo-jms.sha256sum] = "fefaf3bd4d022fa7c2813c85c193ebfe34a7094d47b68c6d0d26dec18e733b1f"
SRC_URI[geronimo-jms-javadoc.sha256sum] = "92231bb0fe5caafde7b7bebe0bce5de4e5fa8e5ef8ee09d8bed74c9523d5d63b"
