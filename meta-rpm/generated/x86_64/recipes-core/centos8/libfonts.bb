SUMMARY = "generated recipe based on libfonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_libfonts = "java-1.8.0-openjdk-headless javapackages-tools libloader"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libfonts-1.1.3-21.el8.noarch.rpm \
          "

SRC_URI[libfonts.sha256sum] = "0db6b2c2baf4de7e4978b8e79dc0d73692999412ee00c441cec61c165db6e0db"
