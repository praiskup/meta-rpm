SUMMARY = "generated recipe based on WALinuxAgent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_WALinuxAgent = "bash openssh openssh-server openssl parted platform-python python3-pyasn1 python36 systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/WALinuxAgent-2.2.38-1.el8.noarch.rpm \
          "

SRC_URI[WALinuxAgent.sha256sum] = "7e1fd11ca788238cd643ee37239463eb99b6df48dd5fa25b74adc4cb97ef99cb"
