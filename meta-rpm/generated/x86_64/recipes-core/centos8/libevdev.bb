SUMMARY = "generated recipe based on libevdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libevdev = "libevdev.so.2"
RPM_SONAME_REQ_libevdev = "libc.so.6 libm.so.6"
RDEPENDS_libevdev = "glibc"
RPM_SONAME_REQ_libevdev-devel = "libevdev.so.2"
RPROVIDES_libevdev-devel = "libevdev-dev (= 1.8.0)"
RDEPENDS_libevdev-devel = "libevdev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libevdev-1.8.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libevdev-devel-1.8.0-1.el8.x86_64.rpm \
          "

SRC_URI[libevdev.sha256sum] = "47f08bb0de61c294f3ad1093666c782d0812f5bc651fd3bf1bdd50d0777dee74"
SRC_URI[libevdev-devel.sha256sum] = "ba61c20c521ccaaf0c9f8f0cac5bc79c282ba1f31e82dd6bb557c501a395e755"
