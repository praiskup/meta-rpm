SUMMARY = "generated recipe based on adobe-mappings-cmap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_adobe-mappings-cmap-deprecated = "adobe-mappings-cmap"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/adobe-mappings-cmap-20171205-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/adobe-mappings-cmap-deprecated-20171205-3.el8.noarch.rpm \
          "

SRC_URI[adobe-mappings-cmap.sha256sum] = "16ccd8d293c1bdb3cb179531b8dfbd8fe9fe9b702263b721f87907a677225d57"
SRC_URI[adobe-mappings-cmap-deprecated.sha256sum] = "4274d2657366cddb9437fc80bbd2bf035e5ad869feeb265eee6c3842d590ac70"
