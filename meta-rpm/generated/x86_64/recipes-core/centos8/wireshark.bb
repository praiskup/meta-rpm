SUMMARY = "generated recipe based on wireshark srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "c-ares glib-2.0 gnutls krb5-libs libcap libgcc libgcrypt libmaxminddb libnl libpcap libsmi libssh pkgconfig-native qt5-qtbase qt5-qtmultimedia zlib"
RPM_SONAME_REQ_wireshark = "libQt5Core.so.5 libQt5Gui.so.5 libQt5Multimedia.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libm.so.6 libnl-3.so.200 libnl-genl-3.so.200 libnl-route-3.so.200 libpcap.so.1 libstdc++.so.6 libwireshark.so.11 libwiretap.so.8 libwscodecs.so.2 libwsutil.so.9 libz.so.1"
RDEPENDS_wireshark = "glib2 glibc hicolor-icon-theme libgcc libmaxminddb libnl3 libpcap libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtmultimedia wireshark-cli xdg-utils zlib"
RPM_SONAME_PROV_wireshark-cli = "libwireshark.so.11 libwiretap.so.8 libwscodecs.so.2 libwsutil.so.9"
RPM_SONAME_REQ_wireshark-cli = "libc.so.6 libcap.so.2 libcares.so.2 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libmaxminddb.so.0 libnl-3.so.200 libnl-genl-3.so.200 libpcap.so.1 libsmi.so.2 libssh.so.4 libwireshark.so.11 libwiretap.so.8 libwscodecs.so.2 libwsutil.so.9 libz.so.1"
RDEPENDS_wireshark-cli = "bash c-ares glib2 glibc gnutls krb5-libs libcap libgcrypt libmaxminddb libnl3 libpcap libsmi libssh shadow-utils systemd-udev zlib"
RPM_SONAME_REQ_wireshark-devel = "libwireshark.so.11 libwiretap.so.8 libwscodecs.so.2 libwsutil.so.9"
RPROVIDES_wireshark-devel = "wireshark-dev (= 2.6.2)"
RDEPENDS_wireshark-devel = "glib2-devel glibc-devel pkgconf-pkg-config wireshark wireshark-cli"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wireshark-2.6.2-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wireshark-cli-2.6.2-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/wireshark-devel-2.6.2-12.el8.x86_64.rpm \
          "

SRC_URI[wireshark.sha256sum] = "f214f415d3647f11de5f94f1996f011c46a0c9d510c14542bf070aeec3f605af"
SRC_URI[wireshark-cli.sha256sum] = "df68f7936c53556a75afcbae1eda3e156ad36b63b9507f0e00218cf8166f6527"
SRC_URI[wireshark-devel.sha256sum] = "c154cd93f7491ba227cc1cc0c5b6fdf6e1be4e7476e7cbbd31cb79c012f468fb"
