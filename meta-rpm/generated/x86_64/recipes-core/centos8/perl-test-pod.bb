SUMMARY = "generated recipe based on perl-Test-Pod srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Pod = "perl-PathTools perl-Pod-Simple perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Pod-1.51-8.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Pod.sha256sum] = "28530415690bc403b710aee74fda336d4d422ffe01b37af30f25820e6f0f7031"
