SUMMARY = "generated recipe based on gc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libatomic-ops libgcc pkgconfig-native"
RPM_SONAME_PROV_gc = "libcord.so.1 libgc.so.1 libgccpp.so.1"
RPM_SONAME_REQ_gc = "ld-linux-x86-64.so.2 libatomic_ops.so.1 libc.so.6 libdl.so.2 libgc.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gc = "glibc libatomic_ops libgcc libstdc++"
RPM_SONAME_REQ_gc-devel = "libcord.so.1 libgc.so.1 libgccpp.so.1"
RPROVIDES_gc-devel = "gc-dev (= 7.6.4)"
RDEPENDS_gc-devel = "gc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gc-7.6.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gc-devel-7.6.4-3.el8.x86_64.rpm \
          "

SRC_URI[gc.sha256sum] = "d84d87071ea78f860e2e029ca9e5fa07fe5cc50586dfef3a2ded793b87759964"
SRC_URI[gc-devel.sha256sum] = "31695f512a8f7a849c0e28a1482cbdcc6b4546dfdb17c10121265ea2b7da1f6c"
