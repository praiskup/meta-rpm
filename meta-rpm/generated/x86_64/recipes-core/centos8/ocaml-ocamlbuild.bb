SUMMARY = "generated recipe based on ocaml-ocamlbuild srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-ocamlbuild = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-ocamlbuild = "glibc ocaml-runtime"
RPROVIDES_ocaml-ocamlbuild-devel = "ocaml-ocamlbuild-dev (= 0.12.0)"
RDEPENDS_ocaml-ocamlbuild-devel = "ocaml-ocamlbuild"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-ocamlbuild-0.12.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-ocamlbuild-devel-0.12.0-6.el8.x86_64.rpm \
          "

SRC_URI[ocaml-ocamlbuild.sha256sum] = "db95dbb6b0001e39c0a01e04515472eb9c63b2a3a9f58d533b7117b5713fb608"
SRC_URI[ocaml-ocamlbuild-devel.sha256sum] = "e8ef7e748e666d4c82f990c6b07581377b7da16dacf52a85a074b32db8f0c46e"
