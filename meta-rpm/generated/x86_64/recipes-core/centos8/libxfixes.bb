SUMMARY = "generated recipe based on libXfixes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXfixes = "libXfixes.so.3"
RPM_SONAME_REQ_libXfixes = "libX11.so.6 libc.so.6"
RDEPENDS_libXfixes = "glibc libX11"
RPM_SONAME_REQ_libXfixes-devel = "libXfixes.so.3"
RPROVIDES_libXfixes-devel = "libXfixes-dev (= 5.0.3)"
RDEPENDS_libXfixes-devel = "libX11-devel libXfixes pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXfixes-5.0.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXfixes-devel-5.0.3-7.el8.x86_64.rpm \
          "

SRC_URI[libXfixes.sha256sum] = "81f7df4c736963636c9ebab7441ca4f4e41a7483ef6e7b2ac0d1bf37afe52a14"
SRC_URI[libXfixes-devel.sha256sum] = "17d9290df9886f24ff771d497e8afc8e5987bb1576af781fa6f4291d46fd1e88"
