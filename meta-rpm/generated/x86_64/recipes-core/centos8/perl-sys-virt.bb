SUMMARY = "generated recipe based on perl-Sys-Virt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-Virt = "libc.so.6 libperl.so.5.26 libpthread.so.0 libvirt.so.0"
RDEPENDS_perl-Sys-Virt = "glibc libvirt-libs perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Sys-Virt-4.5.0-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[perl-Sys-Virt.sha256sum] = "44cd9f5084f04aed14279645cffb83e80d042a7e744dc39e6b0941498ec30a1c"
