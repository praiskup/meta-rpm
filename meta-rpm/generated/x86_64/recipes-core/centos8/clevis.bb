SUMMARY = "generated recipe based on clevis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit cryptsetup-libs glib-2.0 jansson jose libgcc luksmeta openssl pkgconfig-native udisks2"
RPM_SONAME_REQ_clevis = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libjansson.so.4 libjose.so.0"
RDEPENDS_clevis = "bash coreutils curl glibc jansson jose libgcc libjose openssl-libs shadow-utils tpm2-tools"
RDEPENDS_clevis-dracut = "bash clevis-systemd dracut-network"
RDEPENDS_clevis-luks = "bash clevis cryptsetup luksmeta"
RDEPENDS_clevis-systemd = "bash clevis-luks nmap-ncat systemd"
RPM_SONAME_REQ_clevis-udisks2 = "libaudit.so.1 libc.so.6 libcryptsetup.so.12 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjansson.so.4 libluksmeta.so.0 libudisks2.so.0"
RDEPENDS_clevis-udisks2 = "audit-libs clevis-luks cryptsetup-libs glib2 glibc jansson libgcc libluksmeta libudisks2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clevis-11-9.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clevis-dracut-11-9.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clevis-luks-11-9.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clevis-systemd-11-9.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clevis-udisks2-11-9.el8_2.1.x86_64.rpm \
          "

SRC_URI[clevis.sha256sum] = "59cc2be3f34a9671a306fa2177050c2539fd7e23cde02e6745df50c6180a94a7"
SRC_URI[clevis-dracut.sha256sum] = "0f4e742db9ce392e8012f85ec77d891121a0ba1c1c0da4b1c3ab4f2e77b91a08"
SRC_URI[clevis-luks.sha256sum] = "5b1bb2a2e92161e9aa828a1f63038004b2fb5fde6333ace72ee3025f02cd908a"
SRC_URI[clevis-systemd.sha256sum] = "193afe821b342d5768c3fb5d4c0c57d94f3879bf6d27cf63564a344a90617709"
SRC_URI[clevis-udisks2.sha256sum] = "f4bc1cb8d0a725ca06d2a35bc5208559e409fc7ce9643e85a81fcee33e8066ea"
