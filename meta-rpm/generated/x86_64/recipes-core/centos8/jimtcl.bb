SUMMARY = "generated recipe based on jimtcl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_jimtcl = "libjim.so.0.77"
RPM_SONAME_REQ_jimtcl = "libc.so.6 libdl.so.2 libjim.so.0.77 libm.so.6"
RDEPENDS_jimtcl = "glibc"
RPM_SONAME_REQ_jimtcl-devel = "libjim.so.0.77"
RPROVIDES_jimtcl-devel = "jimtcl-dev (= 0.77)"
RDEPENDS_jimtcl-devel = "jimtcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/jimtcl-0.77-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jimtcl-devel-0.77-5.el8.x86_64.rpm \
          "

SRC_URI[jimtcl.sha256sum] = "34b184ac6c33425236334659ff4f89cd45130d465feae6149cbf6b95ce329067"
SRC_URI[jimtcl-devel.sha256sum] = "4ddf027a0f8f03ffbe9c6288f7cffa933f3325472b45978ff67fd91e4e37bf8e"
