SUMMARY = "generated recipe based on yajl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_yajl = "libyajl.so.2"
RPM_SONAME_REQ_yajl = "libc.so.6 libm.so.6 libyajl.so.2"
RDEPENDS_yajl = "glibc"
RPM_SONAME_REQ_yajl-devel = "libyajl.so.2"
RPROVIDES_yajl-devel = "yajl-dev (= 2.1.0)"
RDEPENDS_yajl-devel = "pkgconf-pkg-config yajl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/yajl-2.1.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/yajl-devel-2.1.0-10.el8.x86_64.rpm \
          "

SRC_URI[yajl.sha256sum] = "a7797aa70d6a35116ec3253523dc91d1b08df44bad7442b94af07bb6c0a661f0"
SRC_URI[yajl-devel.sha256sum] = "915a97b572cb7d3235d854013a757f48f67434858895839bce1b2a7096cf17e6"
