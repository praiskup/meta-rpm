SUMMARY = "generated recipe based on geronimo-annotation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_geronimo-annotation = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_geronimo-annotation-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-annotation-1.0-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-annotation-javadoc-1.0-23.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[geronimo-annotation.sha256sum] = "1c67d9564eb406b1050045e16700f8db45b86173cf160910e3713e47892f43e6"
SRC_URI[geronimo-annotation-javadoc.sha256sum] = "ef6e9f67420a5c401ee50c84649ad23a16e4dfd76c26c62a0f907d5b4ecfd189"
