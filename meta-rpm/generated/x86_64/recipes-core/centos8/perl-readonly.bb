SUMMARY = "generated recipe based on perl-Readonly srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Readonly = "perl-Carp perl-Exporter perl-Storable perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Readonly-2.05-5.el8.noarch.rpm \
          "

SRC_URI[perl-Readonly.sha256sum] = "638ba83911b1f4e1df6920c9044c0615d7f38e4e2fa51b3006b224df2618fd78"
