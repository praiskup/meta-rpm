SUMMARY = "generated recipe based on perl-Devel-PPPort srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-PPPort = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-PPPort = "glibc perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Devel-PPPort-3.36-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Devel-PPPort.sha256sum] = "13f7cd7a809ca43bf678d8df3c944830417ccf531191f7a0d65ba696019083d1"
