SUMMARY = "generated recipe based on findutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux pkgconfig-native"
RPM_SONAME_REQ_findutils = "libc.so.6 libm.so.6 libselinux.so.1"
RDEPENDS_findutils = "bash glibc info libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/findutils-4.6.0-20.el8.x86_64.rpm \
          "

SRC_URI[findutils.sha256sum] = "811eb112646b7d87773c65af47efdca975468f3e5df44aa9944e30de24d83890"
