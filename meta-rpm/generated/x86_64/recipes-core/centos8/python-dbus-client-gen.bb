SUMMARY = "generated recipe based on python-dbus-client-gen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-dbus-client-gen = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-dbus-client-gen-0.4-1.el8.noarch.rpm \
          "

SRC_URI[python3-dbus-client-gen.sha256sum] = "0bf27613c68640e60dea7097c51644a9d36296d15eff736f2d8cddc80ebad696"
