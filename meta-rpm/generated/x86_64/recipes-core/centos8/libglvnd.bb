SUMMARY = "generated recipe based on libglvnd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libglvnd = "libGLdispatch.so.0"
RPM_SONAME_REQ_libglvnd = "libc.so.6 libdl.so.2"
RDEPENDS_libglvnd = "glibc"
RPROVIDES_libglvnd-core-devel = "libglvnd-core-dev (= 1.2.0)"
RDEPENDS_libglvnd-core-devel = "pkgconf-pkg-config"
RPM_SONAME_REQ_libglvnd-devel = "libEGL.so.1 libGL.so.1 libGLESv1_CM.so.1 libGLESv2.so.2 libGLX.so.0 libGLdispatch.so.0 libOpenGL.so.0"
RPROVIDES_libglvnd-devel = "libglvnd-dev (= 1.2.0)"
RDEPENDS_libglvnd-devel = "libX11-devel libglvnd libglvnd-core-devel libglvnd-egl libglvnd-gles libglvnd-glx libglvnd-opengl pkgconf-pkg-config"
RPM_SONAME_PROV_libglvnd-egl = "libEGL.so.1"
RPM_SONAME_REQ_libglvnd-egl = "libGLdispatch.so.0 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_libglvnd-egl = "glibc libglvnd mesa-libEGL"
RPM_SONAME_PROV_libglvnd-gles = "libGLESv1_CM.so.1 libGLESv2.so.2"
RPM_SONAME_REQ_libglvnd-gles = "libGLdispatch.so.0 libc.so.6 libdl.so.2"
RDEPENDS_libglvnd-gles = "glibc libglvnd mesa-libEGL"
RPM_SONAME_PROV_libglvnd-glx = "libGL.so.1 libGLX.so.0"
RPM_SONAME_REQ_libglvnd-glx = "libGLX.so.0 libGLdispatch.so.0 libX11.so.6 libXext.so.6 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_libglvnd-glx = "glibc libX11 libXext libglvnd mesa-libGL"
RPM_SONAME_PROV_libglvnd-opengl = "libOpenGL.so.0"
RPM_SONAME_REQ_libglvnd-opengl = "libGLdispatch.so.0 libc.so.6 libdl.so.2"
RDEPENDS_libglvnd-opengl = "glibc libglvnd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-1.2.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-core-devel-1.2.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-devel-1.2.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-egl-1.2.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-gles-1.2.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-glx-1.2.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libglvnd-opengl-1.2.0-6.el8.x86_64.rpm \
          "

SRC_URI[libglvnd.sha256sum] = "a44919485747e2989f6fc269ac4111023e4ff732931eaff57d3bb60be785e839"
SRC_URI[libglvnd-core-devel.sha256sum] = "700ea7411923572cc05d874a0736de6ca790135ddf4f779c7d455b6b39a2178d"
SRC_URI[libglvnd-devel.sha256sum] = "cf9de46efce3003247a50f809b51eea27126e3202071085b502c15f66108f129"
SRC_URI[libglvnd-egl.sha256sum] = "a79c9240e4e3e01a7ecb556341bcfb475096391e1cbe6c3c88615a2b3e84c16b"
SRC_URI[libglvnd-gles.sha256sum] = "7e6d0a374632c01e610a4abde8670e43f1e7da9de3536df8809cc20edf710fd6"
SRC_URI[libglvnd-glx.sha256sum] = "0a36224bca7ae9c5bfcda75033c52ff5095ca63bc4e1f1a3330672c6a3206f57"
SRC_URI[libglvnd-opengl.sha256sum] = "1f1e78ddb838631ec88356cd49b81d82078911160d18404e7dd6f5e24d37aff9"
