SUMMARY = "generated recipe based on libcomps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libxml2 pkgconfig-native platform-python3 zlib"
RPM_SONAME_PROV_libcomps = "libcomps.so.0.1.11"
RPM_SONAME_REQ_libcomps = "libc.so.6 libexpat.so.1 libm.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libcomps = "expat glibc libxml2 zlib"
RPM_SONAME_REQ_libcomps-devel = "libcomps.so.0.1.11"
RPROVIDES_libcomps-devel = "libcomps-dev (= 0.1.11)"
RDEPENDS_libcomps-devel = "libcomps"
RPM_SONAME_REQ_python3-libcomps = "libc.so.6 libcomps.so.0.1.11 libexpat.so.1 libm.so.6 libpython3.6m.so.1.0 libxml2.so.2 libz.so.1"
RDEPENDS_python3-libcomps = "expat glibc libcomps libxml2 platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcomps-0.1.11-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcomps-devel-0.1.11-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libcomps-0.1.11-4.el8.x86_64.rpm \
          "

SRC_URI[libcomps.sha256sum] = "52706222d2a27219e1aad074e1be00c2758be58ff7e9d6f5624bf792f2828acb"
SRC_URI[libcomps-devel.sha256sum] = "e267e9534513dd0bd9952f819a65d039b1863d62038bee3b091b1a433139006f"
SRC_URI[python3-libcomps.sha256sum] = "ee9da8af03986b45d272e95cb62ebbf608d3a1f7759d9ace05f2f1f8682ea5a1"
