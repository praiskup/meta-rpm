SUMMARY = "generated recipe based on Xaw3d srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libsm libx11 libxext libxmu libxpm libxt pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_Xaw3d = "libXaw3d.so.8"
RPM_SONAME_REQ_Xaw3d = "libX11.so.6 libXext.so.6 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6"
RDEPENDS_Xaw3d = "glibc libX11 libXext libXmu libXpm libXt"
RPM_SONAME_REQ_Xaw3d-devel = "libXaw3d.so.8"
RPROVIDES_Xaw3d-devel = "Xaw3d-dev (= 1.6.2)"
RDEPENDS_Xaw3d-devel = "Xaw3d libSM-devel libX11-devel libXext-devel libXmu-devel libXpm-devel libXt-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/Xaw3d-1.6.2-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/Xaw3d-devel-1.6.2-14.el8.x86_64.rpm \
          "

SRC_URI[Xaw3d.sha256sum] = "973e898f0986d6f22914649e1c266e2a49dc6f62af0dc665372546965518b2ca"
SRC_URI[Xaw3d-devel.sha256sum] = "a8b0a12742ad1ea047cc4158e53199dc394cc834260e2e61f7b796da388350ab"
