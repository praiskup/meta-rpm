SUMMARY = "generated recipe based on mpich srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "hwloc libgcc pkgconfig-native"
RPM_SONAME_PROV_mpich = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPM_SONAME_REQ_mpich = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libhwloc.so.5 libmpi.so.12 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_mpich = "environment-modules glibc hwloc-libs libgcc libstdc++ perl-interpreter"
RPM_SONAME_REQ_mpich-devel = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPROVIDES_mpich-devel = "mpich-dev (= 3.2.1)"
RDEPENDS_mpich-devel = "bash gcc-gfortran mpich pkgconf-pkg-config rpm-mpi-hooks"
RDEPENDS_python3-mpich = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpich-3.2.1-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpich-devel-3.2.1-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-mpich-3.2.1-9.el8.x86_64.rpm \
          "

SRC_URI[mpich.sha256sum] = "95bdba4a5c604bf364cba359fd9b78078c3fe7597a64715aa195fa49380eb43a"
SRC_URI[mpich-devel.sha256sum] = "0babe5fc014a5f93de423874d893bd20b2010af5f894b3484eef6217c6074cde"
SRC_URI[python3-mpich.sha256sum] = "4a39506cc875c6c7c78d553b635294213c0d1f765203508f6012a63c6ea3737b"
