SUMMARY = "generated recipe based on cups-pk-helper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs glib-2.0 pkgconfig-native polkit"
RPM_SONAME_REQ_cups-pk-helper = "libc.so.6 libcups.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_cups-pk-helper = "cups-libs dbus dbus-glib glib2 glibc polkit-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-pk-helper-0.2.6-5.el8.x86_64.rpm \
          "

SRC_URI[cups-pk-helper.sha256sum] = "2d216c9150d093010f1854608639f3cdc2284fc9b139781aa978a194146ce044"
