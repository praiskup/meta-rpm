SUMMARY = "generated recipe based on ongres-scram srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ongres-scram = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_ongres-scram-client = "java-1.8.0-openjdk-headless javapackages-filesystem ongres-scram"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ongres-scram-1.0.0~beta.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ongres-scram-client-1.0.0~beta.2-5.el8.noarch.rpm \
          "

SRC_URI[ongres-scram.sha256sum] = "5b743909c8f5794c5d61e119c5e7ff47e7c9a29e4add515ee0737ce6502c8624"
SRC_URI[ongres-scram-client.sha256sum] = "45fee493a929139d55cf6b4b4f405b9e4f51d178e58365c3fc9a9d52cd1b7e75"
