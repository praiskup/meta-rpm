SUMMARY = "generated recipe based on perl-Error srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Error = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Error-0.17025-2.el8.noarch.rpm \
          "

SRC_URI[perl-Error.sha256sum] = "de4051d5d3f1f14a1fea06e6e5fbe9f47ff39ed051f345e500dcd0b2c684616c"
