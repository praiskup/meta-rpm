SUMMARY = "generated recipe based on libinput srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevdev libwacom mtdev pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libinput = "libinput.so.10"
RPM_SONAME_REQ_libinput = "libc.so.6 libevdev.so.2 libinput.so.10 libm.so.6 libmtdev.so.1 librt.so.1 libudev.so.1 libwacom.so.2"
RDEPENDS_libinput = "bash glibc libevdev libwacom mtdev systemd-libs"
RPM_SONAME_REQ_libinput-devel = "libinput.so.10"
RPROVIDES_libinput-devel = "libinput-dev (= 1.14.3)"
RDEPENDS_libinput-devel = "libinput pkgconf-pkg-config"
RPM_SONAME_REQ_libinput-utils = "libc.so.6 libevdev.so.2 libinput.so.10 librt.so.1 libudev.so.1"
RDEPENDS_libinput-utils = "glibc libevdev libinput platform-python python3-evdev python3-pyudev systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libinput-1.14.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libinput-utils-1.14.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libinput-devel-1.14.3-1.el8.x86_64.rpm \
          "

SRC_URI[libinput.sha256sum] = "fe5a1ad1e1ae7b964efcfbd7c7f26a962eca8578a924e9729e47a10339d92032"
SRC_URI[libinput-devel.sha256sum] = "0be3d469a42653c0d11c60a972272b975a172a41c3fcd925ace24a62fa390a64"
SRC_URI[libinput-utils.sha256sum] = "5b96d6b1441dd217380f80d4ac0e0e384a7f0ad635d4f2b6fe1c004def28926d"
