SUMMARY = "generated recipe based on portreserve srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_portreserve = "libc.so.6"
RDEPENDS_portreserve = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/portreserve-0.0.5-19.el8.x86_64.rpm \
          "

SRC_URI[portreserve.sha256sum] = "a38d61d8c71d3c704d1eeada4a846e593dd7d6e95394f934c62f5342c5de0441"
