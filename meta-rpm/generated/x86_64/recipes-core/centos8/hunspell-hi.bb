SUMMARY = "generated recipe based on hunspell-hi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-hi-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-hi.sha256sum] = "0ffab70bfd7a5e1cb0a75590ed6e64a0e56fac962c71649ddf49715664045282"
