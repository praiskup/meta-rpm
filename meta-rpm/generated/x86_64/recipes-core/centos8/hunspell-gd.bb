SUMMARY = "generated recipe based on hunspell-gd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-gd = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-gd-2.6-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-gd.sha256sum] = "06724f4346dcedc4bcf6d5e5abbd7b7a201b6b5caf3458c753b79bad64def8a7"
