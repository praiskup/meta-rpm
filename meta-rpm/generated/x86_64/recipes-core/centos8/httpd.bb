SUMMARY = "generated recipe based on httpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "apr apr-util brotli expat libpcre libselinux libxcrypt libxml2 lua openldap openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_httpd = "libapr-1.so.0 libaprutil-1.so.0 libbrotlienc.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libexpat.so.1 liblua-5.3.so libm.so.6 libpcre.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libz.so.1"
RDEPENDS_httpd = "apr apr-util bash brotli centos-logos-httpd expat glibc httpd-filesystem httpd-tools libselinux libxcrypt lua-libs mailcap mod_http2 pcre systemd systemd-libs zlib"
RPROVIDES_httpd-devel = "httpd-dev (= 2.4.37)"
RDEPENDS_httpd-devel = "apr-devel apr-util-devel bash httpd perl-interpreter perl-libs pkgconf-pkg-config"
RDEPENDS_httpd-filesystem = "bash shadow-utils"
RDEPENDS_httpd-manual = "httpd"
RPM_SONAME_REQ_httpd-tools = "libapr-1.so.0 libaprutil-1.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libexpat.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1"
RDEPENDS_httpd-tools = "apr apr-util expat glibc libxcrypt openssl-libs"
RPM_SONAME_REQ_mod_ldap = "libc.so.6 liblber-2.4.so.2 libldap_r-2.4.so.2 libpthread.so.0"
RDEPENDS_mod_ldap = "apr-util-ldap glibc httpd openldap"
RPM_SONAME_REQ_mod_proxy_html = "libc.so.6 libpthread.so.0 libxml2.so.2"
RDEPENDS_mod_proxy_html = "glibc httpd libxml2"
RPM_SONAME_REQ_mod_session = "libc.so.6 libpthread.so.0"
RDEPENDS_mod_session = "glibc httpd"
RPM_SONAME_REQ_mod_ssl = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libssl.so.1.1"
RDEPENDS_mod_ssl = "bash glibc httpd httpd-filesystem openssl-libs sscg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/httpd-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/httpd-devel-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/httpd-filesystem-2.4.37-21.module_el8.2.0+494+1df74eae.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/httpd-manual-2.4.37-21.module_el8.2.0+494+1df74eae.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/httpd-tools-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_ldap-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_proxy_html-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_session-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_ssl-2.4.37-21.module_el8.2.0+494+1df74eae.x86_64.rpm \
          "

SRC_URI[httpd.sha256sum] = "0f150c08a6702f03327e62e9eaa09c9a2926aece19ca3b47178368eb9ee3830c"
SRC_URI[httpd-devel.sha256sum] = "fe7a27e2f468b0b9c435e472ac17dd0d7272b20d3e696c5e5b1ac9e81ab9e189"
SRC_URI[httpd-filesystem.sha256sum] = "4d2289ce76df7bda61fb994b2e51bc3a5ac4efefcf1f7027c001b8d555b1263f"
SRC_URI[httpd-manual.sha256sum] = "269b77fe906a5e120f948ae9036350b8f98b7739c500d56b3fb6aa5dfb617886"
SRC_URI[httpd-tools.sha256sum] = "8af4f724d01339edbc232fb280599b1dc1a379c95b7d9c800a0048d8764f1b0b"
SRC_URI[mod_ldap.sha256sum] = "95434f4d133ebc7e6660c5cd6a7fe481bdf3ff1067cfe51c764575807f816c6c"
SRC_URI[mod_proxy_html.sha256sum] = "4d7de76ea94284d61ab52ec030c2ec35d4ea1051694ad8df1599ccf461d101b7"
SRC_URI[mod_session.sha256sum] = "d33930e2e36f26dabac476437bc1b2ef66487d5e2358693482316e8f097e1481"
SRC_URI[mod_ssl.sha256sum] = "aae7ee24864ca693c629889d3e98f9df25da35c8ea288780e20fb34feb9317a4"
