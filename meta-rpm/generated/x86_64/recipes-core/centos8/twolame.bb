SUMMARY = "generated recipe based on twolame srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_twolame-libs = "libtwolame.so.0"
RPM_SONAME_REQ_twolame-libs = "libc.so.6 libm.so.6"
RDEPENDS_twolame-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/twolame-libs-0.3.13-11.el8.x86_64.rpm \
          "

SRC_URI[twolame-libs.sha256sum] = "9b0d93c5e345d0626e19d00aa26af45f36e19af675011d7678a00634f32deda3"
