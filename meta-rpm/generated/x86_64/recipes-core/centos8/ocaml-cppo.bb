SUMMARY = "generated recipe based on ocaml-cppo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-cppo = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-cppo = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-cppo-1.6.4-4.el8.x86_64.rpm \
          "

SRC_URI[ocaml-cppo.sha256sum] = "f9cb39da3abcb660e70e83aa394215db164fed8902f433705babb234eaeecf61"
