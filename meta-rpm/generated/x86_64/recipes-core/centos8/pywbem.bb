SUMMARY = "generated recipe based on pywbem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pywbem = "platform-python python3-ply python3-pyyaml python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pywbem-0.11.0-8.el8.noarch.rpm \
          "

SRC_URI[python3-pywbem.sha256sum] = "389ecf861559eab3d3c6e87e3da614c927a219d23ad6e79aade7220998733267"
