SUMMARY = "generated recipe based on mtools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mtools = "libc.so.6"
RDEPENDS_mtools = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mtools-4.0.18-14.el8.x86_64.rpm \
          "

SRC_URI[mtools.sha256sum] = "f726efa5063fdb4b0bff847b20087a3286f9c069ce62f75561a6d1adee0dad5a"
