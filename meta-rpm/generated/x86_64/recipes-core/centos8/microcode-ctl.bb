SUMMARY = "generated recipe based on microcode_ctl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_microcode_ctl = "bash coreutils dracut systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/microcode_ctl-20191115-4.20200609.1.el8_2.x86_64.rpm \
          "

SRC_URI[microcode_ctl.sha256sum] = "6f282d0b0317953d80d495f6275d49cd60e93c22db151816bca7852495fae494"
