SUMMARY = "generated recipe based on perl-Perl-Destruct-Level srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Perl-Destruct-Level = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Perl-Destruct-Level = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Perl-Destruct-Level-0.02-20.el8.x86_64.rpm \
          "

SRC_URI[perl-Perl-Destruct-Level.sha256sum] = "ee09b50982b29ad506f49fd3de46e1b40045a5a7e352fd2b37ed056b96ba9994"
