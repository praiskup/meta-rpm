SUMMARY = "generated recipe based on pkgconf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "pkgconfig"
RPM_SONAME_PROV_libpkgconf = "libpkgconf.so.3"
RPM_SONAME_REQ_libpkgconf = "libc.so.6"
RDEPENDS_libpkgconf = "glibc"
RPM_SONAME_REQ_pkgconf = "libc.so.6 libpkgconf.so.3"
RDEPENDS_pkgconf = "glibc libpkgconf"
RDEPENDS_pkgconf-pkg-config = "bash pkgconf pkgconf-m4"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpkgconf-1.4.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pkgconf-1.4.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pkgconf-m4-1.4.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pkgconf-pkg-config-1.4.2-1.el8.x86_64.rpm \
          "

SRC_URI[libpkgconf.sha256sum] = "a76ff4cf270d2e38106a4bba1880c3a0899d186cd4e1986d7e97c01b934e13b7"
SRC_URI[pkgconf.sha256sum] = "dd08de48d25573f0a8492cf858ce8c37abb10eb560975d9df0e45a7f91b3b41d"
SRC_URI[pkgconf-m4.sha256sum] = "56187f25e8ae7c2a5ce228d13c6e93b9c6a701960d61dff8ad720a8879b6059e"
SRC_URI[pkgconf-pkg-config.sha256sum] = "bf5319e42dbe96c24cd64c974b17f422847cc658c4461d9d61cfe76ad76e9c67"
