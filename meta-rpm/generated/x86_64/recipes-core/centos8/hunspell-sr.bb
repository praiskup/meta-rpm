SUMMARY = "generated recipe based on hunspell-sr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sr = "hunspell"
RDEPENDS_hyphen-sr = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-sr-0.20130330-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-sr-0.20130330-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-sr.sha256sum] = "18311032fd24a569de0e99af6cf39f81510ead9c65d0a613ebcf5961210afa12"
SRC_URI[hyphen-sr.sha256sum] = "a14616debbcbc24714e1f1ed4fc73e48c41315d2e06c5bee233a30dccb1aa5b5"
