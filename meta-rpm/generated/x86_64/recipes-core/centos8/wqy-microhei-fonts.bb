SUMMARY = "generated recipe based on wqy-microhei-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_wqy-microhei-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wqy-microhei-fonts-0.2.0-0.22.beta.el8.noarch.rpm \
          "

SRC_URI[wqy-microhei-fonts.sha256sum] = "2104b702e6abdf9a59a363acf6f00816679d41d539251ec9a47894b147f38a52"
