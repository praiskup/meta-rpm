SUMMARY = "generated recipe based on sblim-sfcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl pkgconfig-native"
RPM_SONAME_PROV_sblim-sfcc = "libcimcClientXML.so.0 libcimcclient.so.0 libcmpisfcc.so.1"
RPM_SONAME_REQ_sblim-sfcc = "libc.so.6 libcimcClientXML.so.0 libcimcclient.so.0 libcurl.so.4 libdl.so.2 libpthread.so.0"
RDEPENDS_sblim-sfcc = "glibc libcurl"
RPM_SONAME_REQ_sblim-sfcc-devel = "libcimcclient.so.0 libcmpisfcc.so.1"
RPROVIDES_sblim-sfcc-devel = "sblim-sfcc-dev (= 2.2.8)"
RDEPENDS_sblim-sfcc-devel = "sblim-sfcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sblim-sfcc-2.2.8-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sblim-sfcc-devel-2.2.8-9.el8.x86_64.rpm \
          "

SRC_URI[sblim-sfcc.sha256sum] = "6455bdbfb14b830c7ef39a74cfd5253a25307859ae0e7b16e8895a3a69416ddb"
SRC_URI[sblim-sfcc-devel.sha256sum] = "d6a91155ff61176e8a90d61e99ff4fd335b478721a04b5bde0efe97496d65003"
