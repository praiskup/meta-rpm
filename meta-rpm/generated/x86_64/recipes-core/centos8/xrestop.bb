SUMMARY = "generated recipe based on xrestop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxres ncurses pkgconfig-native"
RPM_SONAME_REQ_xrestop = "libX11.so.6 libXRes.so.1 libXext.so.6 libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_xrestop = "glibc libX11 libXext libXres ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xrestop-0.4-21.el8.x86_64.rpm \
          "

SRC_URI[xrestop.sha256sum] = "494afc684e2f52629c993d42a5fcde0db903d6ebdbb9fc62959fea252921c019"
