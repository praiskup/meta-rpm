SUMMARY = "generated recipe based on http-parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_http-parser = "libhttp_parser.so.2 libhttp_parser_strict.so.2"
RPM_SONAME_REQ_http-parser = "libc.so.6"
RDEPENDS_http-parser = "glibc"
RPM_SONAME_REQ_http-parser-devel = "libhttp_parser.so.2 libhttp_parser_strict.so.2"
RPROVIDES_http-parser-devel = "http-parser-dev (= 2.8.0)"
RDEPENDS_http-parser-devel = "http-parser"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/http-parser-2.8.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/http-parser-devel-2.8.0-9.el8.x86_64.rpm \
          "

SRC_URI[http-parser.sha256sum] = "d011236af30907934625704bf44c76f702a28b1337a4e5afe5e2c7f883799d37"
SRC_URI[http-parser-devel.sha256sum] = "d2a5cb379793dd61ae3f556e98fa6829946e5278a83a8b9fe0d9a8c745a5a745"
