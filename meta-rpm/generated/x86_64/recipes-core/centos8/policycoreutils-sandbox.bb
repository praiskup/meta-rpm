SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap-ng libselinux pkgconfig-native"
RPM_SONAME_REQ_policycoreutils-sandbox = "libc.so.6 libcap-ng.so.0 libselinux.so.1"
RDEPENDS_policycoreutils-sandbox = "bash glibc libcap-ng libselinux matchbox-window-manager platform-python python3-policycoreutils rsync xorg-x11-server-Xephyr xorg-x11-server-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/policycoreutils-sandbox-2.9-9.el8.x86_64.rpm \
          "

SRC_URI[policycoreutils-sandbox.sha256sum] = "ff0d3ccb89ae37c35ac44e0a9d4cd599bfc936c5c44af23a6f62c0022c6377b7"
