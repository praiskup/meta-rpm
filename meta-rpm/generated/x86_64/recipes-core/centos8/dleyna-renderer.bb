SUMMARY = "generated recipe based on dleyna-renderer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dleyna-core glib-2.0 gssdp gupnp gupnp-av gupnp-dlna libsoup-2.4 pkgconfig-native"
RPM_SONAME_PROV_dleyna-renderer = "libdleyna-renderer-1.0.so.1"
RPM_SONAME_REQ_dleyna-renderer = "libc.so.6 libdleyna-core-1.0.so.5 libdleyna-renderer-1.0.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssdp-1.0.so.3 libgupnp-1.0.so.4 libgupnp-av-1.0.so.2 libgupnp-dlna-2.0.so.3 libm.so.6 libpthread.so.0 libsoup-2.4.so.1"
RDEPENDS_dleyna-renderer = "dbus dleyna-connector-dbus dleyna-core glib2 glibc gssdp gupnp gupnp-av gupnp-dlna libsoup"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dleyna-renderer-0.6.0-2.el8.x86_64.rpm \
          "

SRC_URI[dleyna-renderer.sha256sum] = "05d507fac0a2989bb1bdfd0cd6bbcda3d90048cbf4c9de5f1d2713c6d2c1821e"
