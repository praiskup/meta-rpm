SUMMARY = "generated recipe based on qt5-qttranslations srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttranslations-5.12.5-1.el8.noarch.rpm \
          "

SRC_URI[qt5-qttranslations.sha256sum] = "cbea9b6e383ad7f2165a24313170601bfdb4612835b8476eda743ad9078330a0"
