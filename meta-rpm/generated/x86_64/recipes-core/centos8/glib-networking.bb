SUMMARY = "generated recipe based on glib-networking srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gnutls libproxy p11-kit pkgconfig-native"
RPM_SONAME_PROV_glib-networking = "libgiognomeproxy.so libgiognutls.so libgiolibproxy.so"
RPM_SONAME_REQ_glib-networking = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libp11-kit.so.0 libproxy.so.1"
RDEPENDS_glib-networking = "ca-certificates glib2 glibc gnutls gsettings-desktop-schemas libproxy p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glib-networking-2.56.1-1.1.el8.x86_64.rpm \
          "

SRC_URI[glib-networking.sha256sum] = "a7f9ae54f45ca4fcecf78d9885d12a789f7325119794178bfa2814c6185a953d"
