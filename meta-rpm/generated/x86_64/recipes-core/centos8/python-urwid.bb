SUMMARY = "generated recipe based on python-urwid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-urwid = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-urwid = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-urwid-1.3.1-4.el8.x86_64.rpm \
          "

SRC_URI[python3-urwid.sha256sum] = "c19b20300c9b40ba29a0332b0a5d2c7bf609e15285ace8dc3adcec960c8b4521"
