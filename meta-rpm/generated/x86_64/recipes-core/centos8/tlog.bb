SUMMARY = "generated recipe based on tlog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl json-c pkgconfig-native systemd-libs"
RPM_SONAME_PROV_tlog = "libtlog.so.0"
RPM_SONAME_REQ_tlog = "libc.so.6 libcurl.so.4 libjson-c.so.4 libm.so.6 libpthread.so.0 librt.so.1 libsystemd.so.0 libtlog.so.0 libutil.so.1"
RDEPENDS_tlog = "bash glibc json-c libcurl sed systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tlog-7-2.el8.x86_64.rpm \
          "

SRC_URI[tlog.sha256sum] = "6d3759c93705e181b24c2dc7a2389b3671243fbc7ca76e54915980ec4a36e8ca"
