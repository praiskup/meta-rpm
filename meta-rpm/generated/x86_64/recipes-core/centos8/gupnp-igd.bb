SUMMARY = "generated recipe based on gupnp-igd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gssdp gupnp pkgconfig-native"
RPM_SONAME_PROV_gupnp-igd = "libgupnp-igd-1.0.so.4"
RPM_SONAME_REQ_gupnp-igd = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgssdp-1.0.so.3 libgupnp-1.0.so.4 libpthread.so.0"
RDEPENDS_gupnp-igd = "glib2 glibc gssdp gupnp"
RPM_SONAME_REQ_gupnp-igd-devel = "libgupnp-igd-1.0.so.4"
RPROVIDES_gupnp-igd-devel = "gupnp-igd-dev (= 0.2.5)"
RDEPENDS_gupnp-igd-devel = "gupnp-devel gupnp-igd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gupnp-igd-0.2.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gupnp-igd-devel-0.2.5-4.el8.x86_64.rpm \
          "

SRC_URI[gupnp-igd.sha256sum] = "58cb59e43dd9f742fa9844f4828f573a760619ecf1440efcc4cd0f8c14c24813"
SRC_URI[gupnp-igd-devel.sha256sum] = "8707224e1157788de45f38cf75e856a4f9ab2e5b6c4e33b4bcb5f547813875dc"
