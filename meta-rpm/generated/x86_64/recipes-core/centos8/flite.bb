SUMMARY = "generated recipe based on flite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native"
RPM_SONAME_PROV_flite = "libflite.so.1 libflite_cmu_time_awb.so.1 libflite_cmu_us_kal.so.1 libflite_cmu_us_kal16.so.1 libflite_cmulex.so.1 libflite_usenglish.so.1"
RPM_SONAME_REQ_flite = "libasound.so.2 libc.so.6 libflite.so.1 libflite_cmu_time_awb.so.1 libflite_cmu_us_kal.so.1 libflite_cmulex.so.1 libflite_usenglish.so.1 libm.so.6"
RDEPENDS_flite = "alsa-lib glibc"
RPM_SONAME_REQ_flite-devel = "libflite.so.1 libflite_cmu_time_awb.so.1 libflite_cmu_us_kal.so.1 libflite_cmu_us_kal16.so.1 libflite_cmulex.so.1 libflite_usenglish.so.1"
RPROVIDES_flite-devel = "flite-dev (= 1.3)"
RDEPENDS_flite-devel = "flite"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/flite-1.3-31.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/flite-devel-1.3-31.el8.x86_64.rpm \
          "

SRC_URI[flite.sha256sum] = "461a612ed13c4fecd3ba0dd5f546da73357a407687dae8b925c0d58fdb6b8d69"
SRC_URI[flite-devel.sha256sum] = "74bfdc4057e980f64c7cbe6a39be8ac2e9d36f7e0871c99663c1eec422b5eb25"
