SUMMARY = "generated recipe based on nss-pam-ldapd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "krb5-libs libgcc openldap pam pkgconfig-native"
RPM_SONAME_PROV_nss-pam-ldapd = "libnss_ldap.so.2"
RPM_SONAME_REQ_nss-pam-ldapd = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 liblber-2.4.so.2 libldap_r-2.4.so.2 libnss_ldap.so.2 libpam.so.0 libpthread.so.0"
RDEPENDS_nss-pam-ldapd = "bash glibc krb5-libs libgcc openldap pam systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-pam-ldapd-0.9.9-3.el8.x86_64.rpm \
          "

SRC_URI[nss-pam-ldapd.sha256sum] = "d3b5fe60e36fae2bab80031865a13c19e0d15fcb6680f1c2ce0f3cdfad8fc1f5"
