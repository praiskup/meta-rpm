SUMMARY = "generated recipe based on fabtests srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libfabric pkgconfig-native"
RPM_SONAME_REQ_fabtests = "libc.so.6 libfabric.so.1"
RDEPENDS_fabtests = "bash glibc libfabric ruby"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fabtests-1.9.0rc1-1.el8.x86_64.rpm \
          "

SRC_URI[fabtests.sha256sum] = "40f4c7c1dc151fdc1c6968a81e149ffe005cd99aa692ecc235f132e567c51448"
