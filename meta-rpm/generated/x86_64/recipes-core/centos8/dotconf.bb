SUMMARY = "generated recipe based on dotconf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_dotconf = "libdotconf.so.0"
RPM_SONAME_REQ_dotconf = "libc.so.6"
RDEPENDS_dotconf = "glibc"
RPM_SONAME_REQ_dotconf-devel = "libdotconf.so.0"
RPROVIDES_dotconf-devel = "dotconf-dev (= 1.3)"
RDEPENDS_dotconf-devel = "dotconf pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotconf-1.3-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dotconf-devel-1.3-18.el8.x86_64.rpm \
          "

SRC_URI[dotconf.sha256sum] = "f60b0810c51d358b4ae67974eeea43a274866e365a5648dbd63a146b36c8bb48"
SRC_URI[dotconf-devel.sha256sum] = "764264440c75f156b7d3eab57f60f9951c2a2a2658c47b690aea73ed89b2d8a1"
