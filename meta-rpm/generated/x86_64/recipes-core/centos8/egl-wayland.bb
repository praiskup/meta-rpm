SUMMARY = "generated recipe based on egl-wayland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native wayland"
RPM_SONAME_PROV_egl-wayland = "libnvidia-egl-wayland.so.1"
RPM_SONAME_REQ_egl-wayland = "libc.so.6 libdl.so.2 libpthread.so.0 libwayland-client.so.0 libwayland-server.so.0"
RDEPENDS_egl-wayland = "glibc libglvnd-egl libwayland-client libwayland-server"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/egl-wayland-1.1.4-1.el8.x86_64.rpm \
          "

SRC_URI[egl-wayland.sha256sum] = "7345ed5e61324bac3f59a4590b9f9d5d85777874e06d6f80ecfeb48e1c12c7db"
