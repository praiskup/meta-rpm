SUMMARY = "generated recipe based on ndctl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "json-c keyutils kmod libuuid pkgconfig-native systemd-libs"
RPM_SONAME_REQ_daxctl = "libc.so.6 libdaxctl.so.1 libjson-c.so.4 libuuid.so.1"
RDEPENDS_daxctl = "daxctl-libs glibc json-c libuuid"
RPM_SONAME_REQ_daxctl-devel = "libdaxctl.so.1"
RPROVIDES_daxctl-devel = "daxctl-dev (= 67)"
RDEPENDS_daxctl-devel = "daxctl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_daxctl-libs = "libdaxctl.so.1"
RPM_SONAME_REQ_daxctl-libs = "libc.so.6 libkmod.so.2 libuuid.so.1"
RDEPENDS_daxctl-libs = "glibc kmod-libs libuuid"
RPM_SONAME_REQ_ndctl = "libc.so.6 libdaxctl.so.1 libjson-c.so.4 libkeyutils.so.1 libndctl.so.6 libuuid.so.1"
RDEPENDS_ndctl = "daxctl-libs glibc json-c keyutils-libs libuuid ndctl-libs"
RPM_SONAME_REQ_ndctl-devel = "libndctl.so.6"
RPROVIDES_ndctl-devel = "ndctl-dev (= 67)"
RDEPENDS_ndctl-devel = "ndctl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_ndctl-libs = "libndctl.so.6"
RPM_SONAME_REQ_ndctl-libs = "libc.so.6 libdaxctl.so.1 libkmod.so.2 libudev.so.1 libuuid.so.1"
RDEPENDS_ndctl-libs = "daxctl-libs glibc kmod-libs libuuid systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/daxctl-devel-67-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ndctl-devel-67-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/daxctl-67-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/daxctl-libs-67-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ndctl-67-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ndctl-libs-67-2.el8.x86_64.rpm \
          "

SRC_URI[daxctl.sha256sum] = "8220298ec86661f38a75abf735bc9dc08f364eda4f18c4d75acfcc6a49363f9c"
SRC_URI[daxctl-devel.sha256sum] = "9d5a6d84c62fb25d0ef04a034b69993b94ec27ba04cfc33678de20e7c6c9499d"
SRC_URI[daxctl-libs.sha256sum] = "a734244063387a6c64ed65e581039a4ca93cb3ac0f15d014f5cb1ef5b1a04788"
SRC_URI[ndctl.sha256sum] = "8bcf7eed5df3ea692c6004a17790c05a858238ffc1907b2b6d8eae34a91b38bd"
SRC_URI[ndctl-devel.sha256sum] = "813a714dde233acfcd5e15e361ada6a99fe7edb3ec592855df2ff851050016f3"
SRC_URI[ndctl-libs.sha256sum] = "103ce61f4b8c0bec0206b0ca99f97286c612f855d63c7b822c44aef924502b33"
