SUMMARY = "generated recipe based on gnome-online-accounts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo e2fsprogs gcr gdk-pixbuf glib-2.0 gtk+3 json-glib krb5-libs libsecret libsoup-2.4 libxml2 p11-kit pango pkgconfig-native rest webkit2gtk3"
RPM_SONAME_PROV_gnome-online-accounts = "libgoa-1.0.so.0 libgoa-backend-1.0.so.1 libgoawebextension.so"
RPM_SONAME_REQ_gnome-online-accounts = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcom_err.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgoa-1.0.so.0 libgoa-backend-1.0.so.1 libgobject-2.0.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 libjson-glib-1.0.so.0 libk5crypto.so.3 libkrb5.so.3 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 librest-0.7.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_gnome-online-accounts = "atk cairo cairo-gobject gcr gdk-pixbuf2 glib2 glibc gtk3 json-glib krb5-libs libcom_err libsecret libsoup libxml2 p11-kit pango rest webkit2gtk3 webkit2gtk3-jsc"
RPM_SONAME_REQ_gnome-online-accounts-devel = "libgoa-1.0.so.0 libgoa-backend-1.0.so.1"
RPROVIDES_gnome-online-accounts-devel = "gnome-online-accounts-dev (= 3.28.2)"
RDEPENDS_gnome-online-accounts-devel = "glib2-devel gnome-online-accounts gtk3-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-online-accounts-3.28.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-online-accounts-devel-3.28.2-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-online-accounts.sha256sum] = "39ab05d25bdee9240ece105afc381ec675be26ad193d04ac8297a23c58d0dfa0"
SRC_URI[gnome-online-accounts-devel.sha256sum] = "78ebf0b88126bb8c8ce807d0e9e851c8865d34165297470f230704f78c5b39aa"
