SUMMARY = "generated recipe based on perl-Math-BigRat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Math-BigRat = "perl-Carp perl-Math-BigInt perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Math-BigRat-0.2614-1.el8.noarch.rpm \
          "

SRC_URI[perl-Math-BigRat.sha256sum] = "b60a3b44947143288adc5592e6d08646c1f4e64180f7e189d732cfadc947919d"
