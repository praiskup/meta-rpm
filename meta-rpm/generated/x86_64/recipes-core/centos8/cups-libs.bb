SUMMARY = "generated recipe based on cups srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi-libs e2fsprogs gnutls krb5-libs libgcc libxcrypt pkgconfig-native zlib"
RPM_SONAME_REQ_cups-devel = "libcups.so.2 libcupscgi.so.1 libcupsimage.so.2 libcupsmime.so.1 libcupsppdc.so.1"
RPROVIDES_cups-devel = "cups-dev (= 2.2.6)"
RDEPENDS_cups-devel = "bash cups-libs gnutls-devel krb5-devel zlib-devel"
RPM_SONAME_PROV_cups-libs = "libcups.so.2 libcupscgi.so.1 libcupsimage.so.2 libcupsmime.so.1 libcupsppdc.so.1"
RPM_SONAME_REQ_cups-libs = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgcc_s.so.1 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_cups-libs = "avahi-libs glibc gnutls krb5-libs libcom_err libgcc libstdc++ libxcrypt zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-devel-2.2.6-33.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cups-libs-2.2.6-33.el8.x86_64.rpm \
          "

SRC_URI[cups-devel.sha256sum] = "499174487566046f84c19f953e9985da52032ff240a4a4172d632acc074c919e"
SRC_URI[cups-libs.sha256sum] = "fc87ea5ea52385ee67af8241b20be79d159a49b126c7e608d9a09c681db7e9e3"
