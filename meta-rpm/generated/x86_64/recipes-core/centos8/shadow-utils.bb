SUMMARY = "generated recipe based on shadow-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr audit libselinux libsemanage libxcrypt pkgconfig-native"
RPM_SONAME_REQ_shadow-utils = "libacl.so.1 libattr.so.1 libaudit.so.1 libc.so.6 libcrypt.so.1 libselinux.so.1 libsemanage.so.1"
RDEPENDS_shadow-utils = "audit-libs coreutils glibc libacl libattr libselinux libsemanage libxcrypt setup"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/shadow-utils-4.6-8.el8.x86_64.rpm \
          "

SRC_URI[shadow-utils.sha256sum] = "e7790ddb34f9db041d48eb29d6926eeb170939b5689168b0828fd37750e18574"
