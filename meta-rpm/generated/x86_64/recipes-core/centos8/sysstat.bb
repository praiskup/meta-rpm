SUMMARY = "generated recipe based on sysstat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lm-sensors pkgconfig-native"
RPM_SONAME_REQ_sysstat = "libc.so.6 libsensors.so.4"
RDEPENDS_sysstat = "bash findutils glibc lm_sensors-libs systemd xz"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sysstat-11.7.3-2.el8.x86_64.rpm \
          "

SRC_URI[sysstat.sha256sum] = "aaf50007fd90d506bd142d6c1eeb2bd78c74c19c3aec6088b5ef85d5766a360e"
