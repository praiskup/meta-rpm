SUMMARY = "generated recipe based on gnome-calculator srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk glib-2.0 gtk+3 gtksourceview3 libmpc libsoup-2.4 libxml2 mpfr pkgconfig-native"
RPM_SONAME_REQ_gnome-calculator = "libatk-1.0.so.0 libc.so.6 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libmpc.so.3 libmpfr.so.4 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_gnome-calculator = "atk glib2 glibc gtk3 gtksourceview3 libmpc libsoup libxml2 mpfr"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-calculator-3.28.2-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-calculator.sha256sum] = "8d3ee9dc35da90e8f2b6255aed029df7c1d87a4b14e8b9292016d218759b2800"
