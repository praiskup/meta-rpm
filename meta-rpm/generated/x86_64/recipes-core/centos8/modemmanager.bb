SUMMARY = "generated recipe based on ModemManager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libgudev libmbim libqmi pkgconfig-native systemd-libs"
RPM_SONAME_REQ_ModemManager = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmbim-glib.so.4 libmm-glib.so.0 libpthread.so.0 libqmi-glib.so.5 libsystemd.so.0"
RDEPENDS_ModemManager = "ModemManager-glib bash glib2 glibc libgcc libgudev libmbim libmbim-utils libqmi libqmi-utils systemd systemd-libs"
RPROVIDES_ModemManager-devel = "ModemManager-dev (= 1.10.8)"
RDEPENDS_ModemManager-devel = "ModemManager pkgconf-pkg-config"
RPM_SONAME_PROV_ModemManager-glib = "libmm-glib.so.0"
RPM_SONAME_REQ_ModemManager-glib = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_ModemManager-glib = "glib2 glibc"
RPM_SONAME_REQ_ModemManager-glib-devel = "libmm-glib.so.0"
RPROVIDES_ModemManager-glib-devel = "ModemManager-glib-dev (= 1.10.8)"
RDEPENDS_ModemManager-glib-devel = "ModemManager ModemManager-devel ModemManager-glib glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ModemManager-1.10.8-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ModemManager-glib-1.10.8-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ModemManager-devel-1.10.8-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ModemManager-glib-devel-1.10.8-2.el8.x86_64.rpm \
          "

SRC_URI[ModemManager.sha256sum] = "8427609a673158cfdb215697442d42ce69a981e89157106016737ff9438840af"
SRC_URI[ModemManager-devel.sha256sum] = "c55ac53fa8e33e8521c087be6fc220748db7a49e09f299cb064a52b78abac59c"
SRC_URI[ModemManager-glib.sha256sum] = "508e9470ed648c28c1ef5a4a74072b188daa795f2369d10929f1ddbb5d3b5a3f"
SRC_URI[ModemManager-glib-devel.sha256sum] = "8eb9aedea55157605e4e093179f76b841a8c078c9cc83faa0c0befa72bae27da"
