SUMMARY = "generated recipe based on ghostscript srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs e2fsprogs fontconfig freetype jbig2dec krb5-libs lcms2 libice libidn libijs libjpeg-turbo libpaper libpng libsm libx11 libxcrypt libxext libxt openjpeg2 pkgconfig-native tiff zlib"
RPM_SONAME_REQ_ghostscript = "libc.so.6 libgs.so.9"
RDEPENDS_ghostscript = "bash glibc libgs"
RDEPENDS_ghostscript-doc = "ghostscript"
RDEPENDS_ghostscript-tools-dvipdf = "bash ghostscript texlive-dvips"
RDEPENDS_ghostscript-tools-fonts = "bash ghostscript"
RDEPENDS_ghostscript-tools-printing = "bash ghostscript"
RPM_SONAME_REQ_ghostscript-x11 = "libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXt.so.6 libc.so.6"
RDEPENDS_ghostscript-x11 = "ghostscript glibc libICE libSM libX11 libXext libXt"
RPM_SONAME_PROV_libgs = "libgs.so.9"
RPM_SONAME_REQ_libgs = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgssapi_krb5.so.2 libidn.so.11 libijs-0.35.so libjbig2dec.so.0 libjpeg.so.62 libk5crypto.so.3 libkrb5.so.3 liblcms2.so.2 libm.so.6 libopenjp2.so.7 libpaper.so.1 libpng16.so.16 libpthread.so.0 libtiff.so.5 libz.so.1"
RDEPENDS_libgs = "adobe-mappings-cmap adobe-mappings-cmap-deprecated adobe-mappings-pdf cups-libs fontconfig freetype glibc google-droid-sans-fonts jbig2dec-libs krb5-libs lcms2 libcom_err libidn libijs libjpeg-turbo libpaper libpng libtiff libxcrypt openjpeg2 urw-base35-fonts zlib"
RPM_SONAME_REQ_libgs-devel = "libgs.so.9"
RPROVIDES_libgs-devel = "libgs-dev (= 9.25)"
RDEPENDS_libgs-devel = "libgs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ghostscript-9.25-5.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgs-9.25-5.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ghostscript-doc-9.25-5.el8_1.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ghostscript-tools-dvipdf-9.25-5.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ghostscript-tools-fonts-9.25-5.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ghostscript-tools-printing-9.25-5.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ghostscript-x11-9.25-5.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgs-devel-9.25-5.el8_1.1.x86_64.rpm \
          "

SRC_URI[ghostscript.sha256sum] = "a1ff3a78808b076d5b57d779ce8e8b2ef3eb9c30a0d12925487e04e82afb0350"
SRC_URI[ghostscript-doc.sha256sum] = "9e17e9613ebc2032aca81729d0a900d112972fbee37cd99980221683a921728d"
SRC_URI[ghostscript-tools-dvipdf.sha256sum] = "c88e023d48d0e82de312e5d8510673874ce5b1ec8da483e9d55930c730479412"
SRC_URI[ghostscript-tools-fonts.sha256sum] = "8a4382562c3b08e49d239269e0b1ad8c0bbecc989fcbd344ba7e2401bfa89e87"
SRC_URI[ghostscript-tools-printing.sha256sum] = "0dcc227a4ca63b68609cd31d4a3a3f039e3edff64d9cf55e5a10fcfebc2e8629"
SRC_URI[ghostscript-x11.sha256sum] = "b3b027087c610f1661a8241926caac3fe03d67b34c3c0a83da2a8e3cdf6c221f"
SRC_URI[libgs.sha256sum] = "e63b9aca99cb23d148e65c0b5e63ad59cf4b26a4f235be986d94cc2e57005c20"
SRC_URI[libgs-devel.sha256sum] = "8e2cf89f1d8fdab9cab493c840044f2d9f60dd96ecd3ce536829ab96e368ef70"
