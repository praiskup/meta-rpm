SUMMARY = "generated recipe based on bcel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_bcel = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_bcel-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/bcel-6.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/bcel-javadoc-6.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[bcel.sha256sum] = "a92888c6d9844218ff4e6b3b33fc450c98f4dcaad9c1e3e599b2618aec2e9e08"
SRC_URI[bcel-javadoc.sha256sum] = "9670b3d55b60e64e078be0fd438077991a28ee6ed188b44c1364857c1cd35489"
