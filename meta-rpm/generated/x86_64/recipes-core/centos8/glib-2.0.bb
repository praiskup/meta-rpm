SUMMARY = "generated recipe based on glib2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libffi libgcc libmount libpcre libselinux pkgconfig-native zlib"
RPM_SONAME_PROV_glib2 = "libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0"
RPM_SONAME_REQ_glib2 = "libc.so.6 libdl.so.2 libffi.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libz.so.1"
RDEPENDS_glib2 = "bash glibc gnutls libffi libgcc libmount libselinux pcre zlib"
RPM_SONAME_REQ_glib2-devel = "libc.so.6 libdl.so.2 libffi.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libz.so.1"
RPROVIDES_glib2-devel = "glib2-dev (= 2.56.4)"
RDEPENDS_glib2-devel = "bash glib2 glibc gnutls libffi libmount libselinux pcre pcre-devel pkgconf-pkg-config platform-python zlib"
RDEPENDS_glib2-doc = "glib2"
RPM_SONAME_PROV_glib2-fam = "libgiofam.so"
RPM_SONAME_REQ_glib2-fam = "libc.so.6 libdl.so.2 libfam.so.0 libffi.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libz.so.1"
RDEPENDS_glib2-fam = "gamin glib2 glibc gnutls libffi libmount libselinux pcre zlib"
RDEPENDS_glib2-static = "glib2-devel"
RPM_SONAME_PROV_glib2-tests = "libgdbus-example-objectmanager.so.0 libmoduletestplugin_a.so libmoduletestplugin_b.so libresourceplugin.so libtestmodulea.so libtestmoduleb.so"
RPM_SONAME_REQ_glib2-tests = "libc.so.6 libdl.so.2 libffi.so.6 libgcc_s.so.1 libgdbus-example-objectmanager.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libmount.so.1 libpcre.so.1 libpthread.so.0 libresolv.so.2 libselinux.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_glib2-tests = "bash glib2 glibc gnutls libffi libgcc libmount libselinux libstdc++ pcre zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glib2-2.56.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glib2-devel-2.56.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glib2-fam-2.56.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glib2-tests-2.56.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glib2-doc-2.56.4-8.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glib2-static-2.56.4-8.el8.x86_64.rpm \
          "
SRC_URI = "file://glib2-2.56.4-8.el8.patch"

SRC_URI[glib2.sha256sum] = "a699d54d1e8d376831af32cc45d95cda66820e42849416e86d12e238ac7c780e"
SRC_URI[glib2-devel.sha256sum] = "1a5a831b445e557c8f0a47fb38c438debf19080787040e5ff519344f7c81a262"
SRC_URI[glib2-doc.sha256sum] = "69523bdbdeff9fc92b75186fc107b022fa0001317c60d931c1eae44ee727d31c"
SRC_URI[glib2-fam.sha256sum] = "457caddc25c8b6b0eb3f0a411aded4e5c8f1b1a8221b2bc53fbf41148d357ebe"
SRC_URI[glib2-static.sha256sum] = "abe13f27d1988aa752f98ad15800cddc8cf0f4a7ce18189f0eca6ced858f8037"
SRC_URI[glib2-tests.sha256sum] = "a7b84cd73ebf9cb573275b3a77663424a690e7b46dff13da14df10feb551c957"
