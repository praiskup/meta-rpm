SUMMARY = "generated recipe based on xfsdump srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr libgcc libuuid ncurses pkgconfig-native xfsprogs"
RPM_SONAME_REQ_xfsdump = "libattr.so.1 libc.so.6 libgcc_s.so.1 libhandle.so.1 libncurses.so.6 libpthread.so.0 libtinfo.so.6 libuuid.so.1"
RDEPENDS_xfsdump = "attr glibc libattr libgcc libuuid ncurses-libs xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xfsdump-3.1.8-2.el8.x86_64.rpm \
          "

SRC_URI[xfsdump.sha256sum] = "b353557a4dc1c184c81961063ead2fc6e4846e56a91dc78d69c38eecb8c604f1"
