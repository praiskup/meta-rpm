SUMMARY = "generated recipe based on symlinks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_symlinks = "libc.so.6"
RDEPENDS_symlinks = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/symlinks-1.4-19.el8.x86_64.rpm \
          "

SRC_URI[symlinks.sha256sum] = "0883d90cad753c96cda88da476d6ec8ebf925c9c255cee8527f9fa5443354292"
