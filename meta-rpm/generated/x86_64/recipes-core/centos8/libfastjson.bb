SUMMARY = "generated recipe based on libfastjson srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libfastjson = "libfastjson.so.4"
RPM_SONAME_REQ_libfastjson = "libc.so.6"
RDEPENDS_libfastjson = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libfastjson-0.99.8-2.el8.x86_64.rpm \
          "

SRC_URI[libfastjson.sha256sum] = "105beb1a237e66802e64e620f79b357dfc8f3bb8952ac2f293548f1d68ca40b1"
