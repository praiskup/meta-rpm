SUMMARY = "generated recipe based on c2esp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-filters cups-libs jbigkit pkgconfig-native zlib"
RPM_SONAME_REQ_c2esp = "libc.so.6 libcups.so.2 libcupsfilters.so.1 libcupsimage.so.2 libjbig85.so.2.1 libz.so.1"
RDEPENDS_c2esp = "cups-filesystem cups-filters-libs cups-libs glibc jbigkit-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/c2esp-2.7-14.el8.x86_64.rpm \
          "

SRC_URI[c2esp.sha256sum] = "cf91b691bc4d3c2633157c58a3d50509f831af6323a710c4db8a72bab0c81a37"
