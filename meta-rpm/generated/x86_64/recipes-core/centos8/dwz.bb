SUMMARY = "generated recipe based on dwz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_dwz = "libc.so.6 libelf.so.1"
RDEPENDS_dwz = "elfutils-libelf glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dwz-0.12-9.el8.x86_64.rpm \
          "

SRC_URI[dwz.sha256sum] = "37ddd4817ea8ec29c9969a7d8402bfd75d51ca8350f43d6ac80778d730e84a54"
