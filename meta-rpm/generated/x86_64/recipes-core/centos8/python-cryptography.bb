SUMMARY = "generated recipe based on python-cryptography srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cryptography = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_python3-cryptography = "glibc openssl-libs platform-python python3-asn1crypto python3-cffi python3-idna python3-libs python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-cryptography-2.3-3.el8.x86_64.rpm \
          "

SRC_URI[python3-cryptography.sha256sum] = "c7f0ca0bf3cfef0d4a82bfa860e22cbd3ee69f5980f49f3deda757c1ce8fcab5"
