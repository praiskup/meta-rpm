SUMMARY = "generated recipe based on lldpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevent libxml2 lm-sensors net-snmp openssl pkgconfig-native readline rpm"
RPM_SONAME_PROV_lldpd = "liblldpctl.so.4"
RPM_SONAME_REQ_lldpd = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libevent-2.1.so.6 liblldpctl.so.4 libm.so.6 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libreadline.so.7 librpm.so.8 librpmio.so.8 libsensors.so.4 libssl.so.1.1 libxml2.so.2"
RDEPENDS_lldpd = "bash glibc libevent libxml2 lm_sensors-libs net-snmp-agent-libs net-snmp-libs openssl-libs readline rpm-libs shadow-utils systemd"
RPM_SONAME_REQ_lldpd-devel = "liblldpctl.so.4"
RPROVIDES_lldpd-devel = "lldpd-dev (= 1.0.1)"
RDEPENDS_lldpd-devel = "lldpd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lldpd-1.0.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lldpd-devel-1.0.1-2.el8.x86_64.rpm \
          "

SRC_URI[lldpd.sha256sum] = "0adca6d4505ea336258149ad90a59a2d740531d923108e3fd2dea249b28c5a13"
SRC_URI[lldpd-devel.sha256sum] = "d18e9d54e096d0e57dc05264d3fca760a7a48f92e3a8dd9ab7a13e6778befdcd"
