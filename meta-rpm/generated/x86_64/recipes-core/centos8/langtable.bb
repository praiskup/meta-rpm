SUMMARY = "generated recipe based on langtable srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_langtable-data = "langtable"
RDEPENDS_python3-langtable = "langtable langtable-data platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langtable-0.0.38-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langtable-data-0.0.38-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-langtable-0.0.38-5.el8.noarch.rpm \
          "

SRC_URI[langtable.sha256sum] = "645fcb53bcc3833266cba5aac5b4a8ed1c4b31a7ec43adca40c3fdc5f43282c5"
SRC_URI[langtable-data.sha256sum] = "7a26e9f060e6eca98155c6330a87a9fdbfc19ddaf63cacfbe8a69526d792e707"
SRC_URI[python3-langtable.sha256sum] = "467d01dbe2329ec1aacbe669fe195c5b0a14c1bea2118a54afb35456c2917b0a"
