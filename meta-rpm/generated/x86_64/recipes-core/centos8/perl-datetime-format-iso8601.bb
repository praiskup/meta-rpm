SUMMARY = "generated recipe based on perl-DateTime-Format-ISO8601 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Format-ISO8601 = "perl-Carp perl-DateTime perl-DateTime-Format-Builder perl-Params-Validate perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-Format-ISO8601-0.08-17.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Format-ISO8601.sha256sum] = "55dadb0e9d1d2a43577ecdfc823ce43cac9513f8ba60a4c21d25f25f0de3322d"
