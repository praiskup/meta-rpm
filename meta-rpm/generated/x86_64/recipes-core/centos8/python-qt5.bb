SUMMARY = "generated recipe based on python-qt5 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs libgcc libglvnd pkgconfig-native platform-python3 qt5-qtbase qt5-qtconnectivity qt5-qtdeclarative qt5-qtlocation qt5-qtmultimedia qt5-qtsensors qt5-qtserialport qt5-qtsvg qt5-qttools qt5-qtwebchannel qt5-qtwebsockets qt5-qtx11extras qt5-qtxmlpatterns sip"
RPM_SONAME_REQ_python3-qt5 = "libGL.so.1 libQt5Bluetooth.so.5 libQt5Core.so.5 libQt5Designer.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Location.so.5 libQt5Multimedia.so.5 libQt5MultimediaWidgets.so.5 libQt5Network.so.5 libQt5Nfc.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickWidgets.so.5 libQt5Sensors.so.5 libQt5SerialPort.so.5 libQt5Sql.so.5 libQt5Svg.so.5 libQt5WebChannel.so.5 libQt5WebSockets.so.5 libQt5Widgets.so.5 libQt5X11Extras.so.5 libQt5Xml.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_python3-qt5 = "glibc libgcc libglvnd-glx libstdc++ platform-python python3-qt5-base qt5-qtbase qt5-qtbase-gui qt5-qtconnectivity qt5-qtdeclarative qt5-qtlocation qt5-qtmultimedia qt5-qtsensors qt5-qtserialport qt5-qtsvg qt5-qttools-libs-designer qt5-qttools-libs-help qt5-qtwebchannel qt5-qtwebsockets qt5-qtx11extras qt5-qtxmlpatterns"
RPM_SONAME_REQ_python3-qt5-base = "libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Designer.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Qml.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libdbus-1.so.3 libgcc_s.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6"
RDEPENDS_python3-qt5-base = "bash dbus-libs glibc libgcc libglvnd-glx libstdc++ platform-python python-qt5-rpm-macros python3-dbus python3-libs python3-pyqt5-sip qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qttools-libs-designer"
RPROVIDES_python3-qt5-devel = "python3-qt5-dev (= 5.13.1)"
RDEPENDS_python3-qt5-devel = "python3-qt5 python3-sip-devel qt5-qtbase-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python-qt5-rpm-macros-5.13.1-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-qt5-5.13.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-qt5-base-5.13.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-qt5-devel-5.13.1-1.el8.x86_64.rpm \
          "

SRC_URI[python-qt5-rpm-macros.sha256sum] = "049da79bd2f1b719c725f054114c20aedce96eddcd887631c1b6d6c3ddddc789"
SRC_URI[python3-qt5.sha256sum] = "658f1147ba25539cc9a23b7fe77fd9d4fe5e7e696cc188a4c9c9f1f5cca061b8"
SRC_URI[python3-qt5-base.sha256sum] = "a4badb9da7dcf0ceec4a44c9200fb2223e61745cf49961d9f485d0ffa5c05adf"
SRC_URI[python3-qt5-devel.sha256sum] = "7597fe4132d851fb2f72f124b87722d29b6f86e0406a380c63cd284ba9b46a8e"
