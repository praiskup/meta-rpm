SUMMARY = "generated recipe based on libmad srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmad = "libmad.so.0"
RPM_SONAME_REQ_libmad = "libc.so.6"
RDEPENDS_libmad = "glibc"
RPM_SONAME_REQ_libmad-devel = "libmad.so.0"
RPROVIDES_libmad-devel = "libmad-dev (= 0.15.1b)"
RDEPENDS_libmad-devel = "libmad pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmad-0.15.1b-25.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmad-devel-0.15.1b-25.el8.x86_64.rpm \
          "

SRC_URI[libmad.sha256sum] = "15ab29f0afc44fcf44e74aac7f0e0b54d24c0c0e80c8991ce2221e8a812a3b4e"
SRC_URI[libmad-devel.sha256sum] = "aaf7caafdf1a2642051e096d7ad163dce9015682fa7029994912777bb8177ce4"
