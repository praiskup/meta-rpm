SUMMARY = "generated recipe based on rear srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rear = "attr bash bc binutils crontabs ethtool gawk gzip iproute iputils openssl parted syslinux tar util-linux xorriso"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rear-2.4-12.el8.x86_64.rpm \
          "

SRC_URI[rear.sha256sum] = "a4842c72329f502c7aa19b033ffe4709a26d744c2d223ee8c680facef915ffdc"
