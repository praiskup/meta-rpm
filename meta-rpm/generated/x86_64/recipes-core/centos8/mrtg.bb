SUMMARY = "generated recipe based on mrtg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gd pkgconfig-native"
RPM_SONAME_REQ_mrtg = "libc.so.6 libgd.so.3 libm.so.6"
RDEPENDS_mrtg = "bash gd glibc perl-CGI perl-Carp perl-Exporter perl-Getopt-Long perl-IO perl-IO-Socket-INET6 perl-Math-BigInt perl-Pod-Usage perl-SNMP_Session perl-Socket perl-Socket6 perl-interpreter perl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mrtg-2.17.7-1.el8.x86_64.rpm \
          "

SRC_URI[mrtg.sha256sum] = "d77ac7148fea81e348d4f7902d6ec7e00a44d660eac7fe442517e226817e8acc"
