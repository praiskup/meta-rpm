SUMMARY = "generated recipe based on enscript srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_enscript = "libc.so.6 libm.so.6"
RDEPENDS_enscript = "bash glibc info perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/enscript-1.6.6-17.el8.x86_64.rpm \
          "

SRC_URI[enscript.sha256sum] = "39d66e84d3035dbf24bfb0cb54cba7990c2eb5b867e006cdb47bf38b723e1395"
