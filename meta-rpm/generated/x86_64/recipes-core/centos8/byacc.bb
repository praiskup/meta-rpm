SUMMARY = "generated recipe based on byacc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_byacc = "libc.so.6"
RDEPENDS_byacc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/byacc-1.9.20170709-4.el8.x86_64.rpm \
          "

SRC_URI[byacc.sha256sum] = "0833f47bbf92b09b4a4a6a1c0034e11b433cce6a97f09b3f5473c6e4c7599b80"
