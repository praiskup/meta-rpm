SUMMARY = "generated recipe based on dpdk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc numactl pkgconfig-native rdma-core"
RPM_SONAME_PROV_dpdk = "librte_bitratestats.so.20.0 librte_bus_pci.so.20.0 librte_bus_vdev.so.20.0 librte_bus_vmbus.so.20.0 librte_cmdline.so.20.0 librte_eal.so.20.0 librte_ethdev.so.20.0 librte_gro.so.20.0 librte_gso.so.20.0 librte_hash.so.20.0 librte_ip_frag.so.20.0 librte_kvargs.so.20.0 librte_latencystats.so.20.0 librte_mbuf.so.20.0 librte_member.so.20.0 librte_mempool.so.20.0 librte_mempool_bucket.so.20.0 librte_mempool_ring.so.20.0 librte_mempool_stack.so.20.0 librte_meter.so.20.0 librte_metrics.so.20.0 librte_net.so.20.0 librte_pci.so.20.0 librte_pdump.so.20.0 librte_pmd_bnxt.so.20.0 librte_pmd_e1000.so.20.0 librte_pmd_enic.so.20.0 librte_pmd_failsafe.so.20.0 librte_pmd_i40e.so.20.0 librte_pmd_ixgbe.so.20.0 librte_pmd_mlx4.so.20.0 librte_pmd_mlx5.so.20.0 librte_pmd_netvsc.so.20.0 librte_pmd_nfp.so.20.0 librte_pmd_qede.so.20.0 librte_pmd_ring.so.20.0 librte_pmd_tap.so.20.0 librte_pmd_vdev_netvsc.so.20.0 librte_pmd_vhost.so.20.0 librte_pmd_virtio.so.20.0 librte_ring.so.20.0 librte_stack.so.0.200 librte_vhost.so.20.0"
RPM_SONAME_REQ_dpdk = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libibverbs.so.1 libm.so.6 libmlx4.so.1 libmlx5.so.1 libnuma.so.1 libpthread.so.0 librt.so.1 librte_bitratestats.so.20.0 librte_bus_pci.so.20.0 librte_bus_vdev.so.20.0 librte_bus_vmbus.so.20.0 librte_cmdline.so.20.0 librte_eal.so.20.0 librte_ethdev.so.20.0 librte_gro.so.20.0 librte_gso.so.20.0 librte_hash.so.20.0 librte_ip_frag.so.20.0 librte_kvargs.so.20.0 librte_latencystats.so.20.0 librte_mbuf.so.20.0 librte_member.so.20.0 librte_mempool.so.20.0 librte_mempool_ring.so.20.0 librte_meter.so.20.0 librte_metrics.so.20.0 librte_net.so.20.0 librte_pci.so.20.0 librte_pdump.so.20.0 librte_pmd_bnxt.so.20.0 librte_pmd_i40e.so.20.0 librte_pmd_ixgbe.so.20.0 librte_ring.so.20.0 librte_stack.so.0.200 librte_vhost.so.20.0"
RDEPENDS_dpdk = "glibc libgcc libibverbs numactl-libs"
RPM_SONAME_REQ_dpdk-devel = "libc.so.6 librte_bitratestats.so.20.0 librte_bus_pci.so.20.0 librte_bus_vdev.so.20.0 librte_bus_vmbus.so.20.0 librte_cmdline.so.20.0 librte_eal.so.20.0 librte_ethdev.so.20.0 librte_gro.so.20.0 librte_gso.so.20.0 librte_hash.so.20.0 librte_ip_frag.so.20.0 librte_kvargs.so.20.0 librte_latencystats.so.20.0 librte_mbuf.so.20.0 librte_member.so.20.0 librte_mempool.so.20.0 librte_mempool_bucket.so.20.0 librte_mempool_ring.so.20.0 librte_mempool_stack.so.20.0 librte_meter.so.20.0 librte_metrics.so.20.0 librte_net.so.20.0 librte_pci.so.20.0 librte_pdump.so.20.0 librte_pmd_bnxt.so.20.0 librte_pmd_e1000.so.20.0 librte_pmd_enic.so.20.0 librte_pmd_failsafe.so.20.0 librte_pmd_i40e.so.20.0 librte_pmd_ixgbe.so.20.0 librte_pmd_mlx4.so.20.0 librte_pmd_mlx5.so.20.0 librte_pmd_netvsc.so.20.0 librte_pmd_nfp.so.20.0 librte_pmd_qede.so.20.0 librte_pmd_ring.so.20.0 librte_pmd_tap.so.20.0 librte_pmd_vdev_netvsc.so.20.0 librte_pmd_vhost.so.20.0 librte_pmd_virtio.so.20.0 librte_ring.so.20.0 librte_stack.so.0.200 librte_vhost.so.20.0"
RPROVIDES_dpdk-devel = "dpdk-dev (= 19.11)"
RDEPENDS_dpdk-devel = "bash dpdk glibc"
RDEPENDS_dpdk-tools = "dpdk findutils iproute kmod pciutils platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dpdk-19.11-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dpdk-devel-19.11-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dpdk-doc-19.11-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dpdk-tools-19.11-4.el8.x86_64.rpm \
          "

SRC_URI[dpdk.sha256sum] = "a919eb2f994ab9618d6f90d4d653ba6c5249fa0d005899aa513032912129b659"
SRC_URI[dpdk-devel.sha256sum] = "df50acd30bb3be518442d9ee49ffa83231e52e4945b73d5b0c92be2c60a06c50"
SRC_URI[dpdk-doc.sha256sum] = "ccd4e705a22b393b4a4eb44a36ff99f4b186cc4a882de184d2c0caa3b2b65686"
SRC_URI[dpdk-tools.sha256sum] = "c4f417242992128f9ba4470cc99f027408a4e48f4376113e2c9bb62f0bf2d20d"
