SUMMARY = "generated recipe based on hyphen-ta srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ta = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-ta-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-ta.sha256sum] = "f0c71278e5e9bf76dfe71d880f1646a4a11e0ffeaae073cda6dba2e76a10df89"
