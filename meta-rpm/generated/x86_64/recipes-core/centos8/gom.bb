SUMMARY = "generated recipe based on gom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native sqlite3"
RPM_SONAME_PROV_gom = "libgom-1.0.so.0"
RPM_SONAME_REQ_gom = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libsqlite3.so.0"
RDEPENDS_gom = "glib2 glibc platform-python sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gom-0.3.3-5.el8.x86_64.rpm \
          "

SRC_URI[gom.sha256sum] = "61235b077e1557a341d0ac3aec55b26993caded3982b85c4ce4745ec45194ff1"
