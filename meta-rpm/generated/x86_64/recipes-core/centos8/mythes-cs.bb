SUMMARY = "generated recipe based on mythes-cs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-cs = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-cs-0.20070926-19.el8.noarch.rpm \
          "

SRC_URI[mythes-cs.sha256sum] = "ff50c73380022e54a60878bc3813fe0579761e32e95e9c8fdedecc232db6e5e0"
