SUMMARY = "generated recipe based on gcc-toolset-9-dyninst srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost dyninst elfutils libgcc pkgconfig-native tbb"
RPM_SONAME_PROV_gcc-toolset-9-dyninst = "libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libinstructionAPI.so.10.1 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPM_SONAME_REQ_gcc-toolset-9-dyninst = "ld-linux-x86-64.so.2 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libcommon.so.10.1 libdl.so.2 libdw.so.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libelf.so.1 libgcc_s.so.1 libgomp.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libpthread.so.0 libstackwalk.so.10.1 libstdc++.so.6 libsymtabAPI.so.10.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_gcc-toolset-9-dyninst = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer elfutils-libelf elfutils-libs gcc-toolset-9-runtime glibc libgcc libgomp libstdc++ tbb"
RPM_SONAME_PROV_gcc-toolset-9-dyninst-devel = "libInst.so"
RPM_SONAME_REQ_gcc-toolset-9-dyninst-devel = "libc.so.6 libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libgcc_s.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libstdc++.so.6 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPROVIDES_gcc-toolset-9-dyninst-devel = "gcc-toolset-9-dyninst-dev (= 10.1.0)"
RDEPENDS_gcc-toolset-9-dyninst-devel = "boost-devel dyninst gcc-toolset-9-dyninst glibc libgcc libstdc++ tbb-devel"
RDEPENDS_gcc-toolset-9-dyninst-static = "gcc-toolset-9-dyninst-devel"
RPM_SONAME_REQ_gcc-toolset-9-dyninst-testsuite = "libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libcommon.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libgcc_s.so.1 libgomp.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libpthread.so.0 libstackwalk.so.10.1 libstdc++.so.6 libsymtabAPI.so.10.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_gcc-toolset-9-dyninst-testsuite = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer dyninst gcc-toolset-9-dyninst gcc-toolset-9-dyninst-devel gcc-toolset-9-dyninst-static glibc glibc-static libgcc libgomp libstdc++ tbb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-dyninst-10.1.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gcc-toolset-9-dyninst-devel-10.1.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gcc-toolset-9-dyninst-doc-10.1.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gcc-toolset-9-dyninst-static-10.1.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gcc-toolset-9-dyninst-testsuite-10.1.0-1.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-dyninst.sha256sum] = "5fcd61b9843e6b67d126dcaed536fd4e8a36af28ded1261936785e66485c1f4f"
SRC_URI[gcc-toolset-9-dyninst-devel.sha256sum] = "113d9692f451b29a72d564639638e7fc92e2dd0bacf4aedfe4f5dcc055d620cc"
SRC_URI[gcc-toolset-9-dyninst-doc.sha256sum] = "15dfa3c232bccf2c22ab1b68c4d9d3c417a58257075875bcf0193df180eb4ca0"
SRC_URI[gcc-toolset-9-dyninst-static.sha256sum] = "a0f3f5a69a8c0dc9f7725e5b5962682f8395f64de13826db40992a6e661ec672"
SRC_URI[gcc-toolset-9-dyninst-testsuite.sha256sum] = "9967f84526850031a48ab81ad3f2d68441b27fc3082237a0175e59d48bb14ea6"
