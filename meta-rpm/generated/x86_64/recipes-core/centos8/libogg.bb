SUMMARY = "generated recipe based on libogg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libogg = "libogg.so.0"
RPM_SONAME_REQ_libogg = "libc.so.6"
RDEPENDS_libogg = "glibc"
RPM_SONAME_REQ_libogg-devel = "libogg.so.0"
RPROVIDES_libogg-devel = "libogg-dev (= 1.3.2)"
RDEPENDS_libogg-devel = "automake libogg pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libogg-1.3.2-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libogg-devel-1.3.2-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libogg-devel-docs-1.3.2-10.el8.noarch.rpm \
          "

SRC_URI[libogg.sha256sum] = "35f80ecc7540818e702e49c13cce081bda78ac28087247acf71e6d774e6f0c3e"
SRC_URI[libogg-devel.sha256sum] = "0300dd8a2f92c0fcc7f6f1a54d1087d820d5f905910f7093ffbc2a6d78e286cf"
SRC_URI[libogg-devel-docs.sha256sum] = "0cd3850a60057258e06c4c10a6c4a05d22b31741dc472e043d2ecdd9b2d8b35a"
