SUMMARY = "generated recipe based on marisa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_marisa = "libmarisa.so.0"
RPM_SONAME_REQ_marisa = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_marisa = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/marisa-0.2.4-36.el8.x86_64.rpm \
          "

SRC_URI[marisa.sha256sum] = "bf3dc5f4b5155ec52275851668d2565a83169efcc2403b8342fe860de4bfa609"
