SUMMARY = "generated recipe based on mesa-libGLw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libx11 libxt mesa motif pkgconfig-native"
RPM_SONAME_PROV_mesa-libGLw = "libGLw.so.1"
RPM_SONAME_REQ_mesa-libGLw = "libGL.so.1 libX11.so.6 libXm.so.4 libXt.so.6 libc.so.6"
RDEPENDS_mesa-libGLw = "glibc libX11 libXt libglvnd-glx motif"
RPM_SONAME_REQ_mesa-libGLw-devel = "libGLw.so.1"
RPROVIDES_mesa-libGLw-devel = "mesa-libGLw-dev (= 8.0.0)"
RDEPENDS_mesa-libGLw-devel = "libX11-devel libXt-devel libglvnd-devel mesa-libGL-devel mesa-libGLw motif-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libGLw-8.0.0-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libGLw-devel-8.0.0-18.el8.x86_64.rpm \
          "

SRC_URI[mesa-libGLw.sha256sum] = "13f9c9f7b6fc4bf9a66fece2d07e0c389b786f1ee95de531396afff77f3eab06"
SRC_URI[mesa-libGLw-devel.sha256sum] = "d00e0436829a1d10d58e87df22ef4ec9609de8bca5c6787cdfe7fe14d58d55c9"
