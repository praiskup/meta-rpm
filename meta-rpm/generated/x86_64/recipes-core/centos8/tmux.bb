SUMMARY = "generated recipe based on tmux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevent ncurses pkgconfig-native"
RPM_SONAME_REQ_tmux = "libc.so.6 libevent-2.1.so.6 libresolv.so.2 libtinfo.so.6 libutil.so.1"
RDEPENDS_tmux = "bash glibc libevent ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tmux-2.7-1.el8.x86_64.rpm \
          "

SRC_URI[tmux.sha256sum] = "107243139d802c14df1bbab6eb779886411feb22d5638d1b90ffd09314c7d739"
