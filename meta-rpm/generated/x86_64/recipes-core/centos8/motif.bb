SUMMARY = "generated recipe based on motif srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libice libjpeg-turbo libpng libsm libx11 libxext libxft libxmu libxp libxt pkgconfig-native"
RPM_SONAME_PROV_motif = "libMrm.so.4 libUil.so.4 libXm.so.4"
RPM_SONAME_REQ_motif = "libICE.so.6 libMrm.so.4 libSM.so.6 libX11.so.6 libXext.so.6 libXft.so.2 libXm.so.4 libXmu.so.6 libXp.so.6 libXt.so.6 libc.so.6 libjpeg.so.62 libpng16.so.16"
RDEPENDS_motif = "bash glibc libICE libSM libX11 libXext libXft libXmu libXp libXt libjpeg-turbo libpng xorg-x11-xbitmaps xorg-x11-xinit"
RPM_SONAME_REQ_motif-devel = "libICE.so.6 libMrm.so.4 libSM.so.6 libUil.so.4 libX11.so.6 libXext.so.6 libXft.so.2 libXm.so.4 libXmu.so.6 libXp.so.6 libXt.so.6 libc.so.6 libjpeg.so.62 libpng16.so.16"
RPROVIDES_motif-devel = "motif-dev (= 2.3.4)"
RDEPENDS_motif-devel = "glibc libICE libSM libX11 libXext libXext-devel libXft libXft-devel libXmu libXmu-devel libXp libXp-devel libXt libXt-devel libjpeg-turbo libjpeg-turbo-devel libpng libpng-devel motif"
RDEPENDS_motif-static = "motif-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/motif-2.3.4-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/motif-devel-2.3.4-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/motif-static-2.3.4-16.el8.x86_64.rpm \
          "

SRC_URI[motif.sha256sum] = "d512400d8b32c4453dd6adf517674fc79b44785034fbebb8c0ece5433371a7dc"
SRC_URI[motif-devel.sha256sum] = "d95e781d148a0b7cbdf2dc7d94c0c60bb85404eb447e51fdaf00fc18ae014008"
SRC_URI[motif-static.sha256sum] = "cd7751356adb3cffba0fe16b2c5eea138e558f834a25faad5838326ea84d3b1f"
