SUMMARY = "generated recipe based on perl-TimeDate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-TimeDate = "perl-Carp perl-Exporter perl-Time-Local perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-TimeDate-2.30-13.el8.noarch.rpm \
          "

SRC_URI[perl-TimeDate.sha256sum] = "f06e5f6a9197c7204e520cb813bb41bceb19afcd7c040a4ddafba9625b255681"
