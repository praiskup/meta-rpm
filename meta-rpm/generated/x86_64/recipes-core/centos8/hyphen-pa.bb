SUMMARY = "generated recipe based on hyphen-pa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-pa = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-pa-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-pa.sha256sum] = "eabfe20dcc0c0e9e52c128e6c75d01ea182cf69ac13bf3bd23079cf5a86625ca"
