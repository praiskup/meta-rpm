SUMMARY = "generated recipe based on libva srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdrm libglvnd libx11 libxext libxfixes pkgconfig-native wayland"
RPM_SONAME_PROV_libva = "libva-drm.so.2 libva-glx.so.2 libva-wayland.so.2 libva-x11.so.2 libva.so.2"
RPM_SONAME_REQ_libva = "libGL.so.1 libX11.so.6 libXext.so.6 libXfixes.so.3 libc.so.6 libdl.so.2 libdrm.so.2 libva-x11.so.2 libva.so.2 libwayland-client.so.0"
RDEPENDS_libva = "glibc libX11 libXext libXfixes libdrm libglvnd-glx libwayland-client mesa-filesystem"
RPM_SONAME_REQ_libva-devel = "libva-drm.so.2 libva-glx.so.2 libva-wayland.so.2 libva-x11.so.2 libva.so.2"
RPROVIDES_libva-devel = "libva-dev (= 2.5.0)"
RDEPENDS_libva-devel = "libva pkgconf-pkg-config wayland-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libva-2.5.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libva-devel-2.5.0-2.el8.x86_64.rpm \
          "

SRC_URI[libva.sha256sum] = "0451cf81d339e0ba4ddc0709214c837a60f5c94b34449d908e644d7962e5fe61"
SRC_URI[libva-devel.sha256sum] = "aee7190d83d974e0badaa4bba025caa66e9319201907a00abcea27cf0b17cef4"
