SUMMARY = "generated recipe based on rubygem-rspec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec = "rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rubygem-rspec-3.7.0-2.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec.sha256sum] = "65f89fc11c0406b15d79093b92d2cb9ba697f6d0c5ffe3cf2d8ad8c9883b66ac"
