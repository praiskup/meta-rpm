SUMMARY = "generated recipe based on hunspell-cop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-cop = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-cop-0.3-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-cop.sha256sum] = "7bc1fd0dd67f6ed3b246b537997e741904b72b18240613c67b2eb182d324037a"
