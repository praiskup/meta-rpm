SUMMARY = "generated recipe based on qt5-qtquickcontrols2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtquickcontrols2 = "libQt5QuickControls2.so.5 libQt5QuickTemplates2.so.5"
RPM_SONAME_REQ_qt5-qtquickcontrols2 = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickControls2.so.5 libQt5QuickTemplates2.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtquickcontrols2 = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtgraphicaleffects"
RPM_SONAME_REQ_qt5-qtquickcontrols2-devel = "libQt5QuickControls2.so.5 libQt5QuickTemplates2.so.5"
RPROVIDES_qt5-qtquickcontrols2-devel = "qt5-qtquickcontrols2-dev (= 5.12.5)"
RDEPENDS_qt5-qtquickcontrols2-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtquickcontrols2"
RPM_SONAME_REQ_qt5-qtquickcontrols2-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickControls2.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtquickcontrols2-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtquickcontrols2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtquickcontrols2-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtquickcontrols2-examples-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qt5-qtquickcontrols2-devel-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtquickcontrols2.sha256sum] = "5394a52a8b533eb555164cc21fb1db7f3f5df4daa7eb9a0edd815a1bb11de9ce"
SRC_URI[qt5-qtquickcontrols2-devel.sha256sum] = "b47d286ac4b9bd0e49c47cad615bcd6c881405479352d47dd5a6b674ae8f5dcf"
SRC_URI[qt5-qtquickcontrols2-examples.sha256sum] = "2835071ebf044af5b088d7a64b4e301228c7a7fb3d21c635aa6b3e06fafccac6"
