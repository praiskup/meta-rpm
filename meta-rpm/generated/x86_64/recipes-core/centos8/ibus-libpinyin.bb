SUMMARY = "generated recipe based on ibus-libpinyin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 ibus libgcc libpinyin lua pkgconfig-native sqlite3"
RPM_SONAME_REQ_ibus-libpinyin = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 liblua-5.3.so libm.so.6 libpinyin.so.13 libpthread.so.0 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_ibus-libpinyin = "bash glib2 glibc ibus ibus-libs libgcc libpinyin libpinyin-data libstdc++ lua-libs python3-gobject sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-libpinyin-1.10.0-1.el8.x86_64.rpm \
          "

SRC_URI[ibus-libpinyin.sha256sum] = "dd8cd2afef807463dc1cea70ab75e3b73d5a17a93ed3d4301a73e25e7923f91d"
