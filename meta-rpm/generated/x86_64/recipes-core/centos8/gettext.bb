SUMMARY = "generated recipe based on gettext srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libcroco libgcc libunistring libxml2 ncurses pkgconfig-native"
RPM_SONAME_REQ_gettext = "libc.so.6 libcroco-0.6.so.3 libgcc_s.so.1 libgettextlib-0.19.8.1.so libgettextsrc-0.19.8.1.so libglib-2.0.so.0 libgomp.so.1 libm.so.6 libncurses.so.6 libpthread.so.0 libtinfo.so.6 libunistring.so.2 libxml2.so.2"
RDEPENDS_gettext = "bash gettext-libs glib2 glibc info libcroco libgcc libgomp libunistring libxml2 ncurses-libs"
RPROVIDES_gettext-common-devel = "gettext-common-dev (= 0.19.8.1)"
RPM_SONAME_PROV_gettext-devel = "libgnuintl.so.8"
RPM_SONAME_REQ_gettext-devel = "libasprintf.so.0 libc.so.6 libgettextpo.so.0"
RPROVIDES_gettext-devel = "gettext-dev (= 0.19.8.1)"
RDEPENDS_gettext-devel = "bash gettext gettext-common-devel gettext-libs glibc info xz"
RPM_SONAME_PROV_gettext-libs = "libasprintf.so.0 libgettextlib-0.19.8.1.so libgettextpo.so.0 libgettextsrc-0.19.8.1.so"
RPM_SONAME_REQ_gettext-libs = "libc.so.6 libcroco-0.6.so.3 libgcc_s.so.1 libgettextlib-0.19.8.1.so libglib-2.0.so.0 libgomp.so.1 libm.so.6 libncurses.so.6 libpthread.so.0 libstdc++.so.6 libtinfo.so.6 libunistring.so.2 libxml2.so.2"
RDEPENDS_gettext-libs = "glib2 glibc libcroco libgcc libgomp libstdc++ libunistring libxml2 ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gettext-0.19.8.1-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gettext-common-devel-0.19.8.1-17.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gettext-devel-0.19.8.1-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gettext-libs-0.19.8.1-17.el8.x86_64.rpm \
          "

SRC_URI[gettext.sha256sum] = "829c842bbd79dca18d37198414626894c44e5b8faf0cce0054ca0ba6623ae136"
SRC_URI[gettext-common-devel.sha256sum] = "089ec84f7dae0bc5ea4fcc1f697797f433911eeaa50909a8d3d7c15863252359"
SRC_URI[gettext-devel.sha256sum] = "59cce0565016413b2bdb26096be56cd8042ae7defc942748e16bfabc47341efa"
SRC_URI[gettext-libs.sha256sum] = "ade52756aaf236e77dadd6cf97716821141c2759129ca7808524ab79607bb4c4"
