SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs libselinux pkgconfig-native systemd"
RPM_SONAME_REQ_device-mapper = "libc.so.6 libdevmapper.so.1.02"
RDEPENDS_device-mapper = "bash device-mapper-libs glibc systemd util-linux"
RPM_SONAME_REQ_device-mapper-devel = "libdevmapper.so.1.02"
RPROVIDES_device-mapper-devel = "device-mapper-dev (= 1.02.169)"
RDEPENDS_device-mapper-devel = "device-mapper device-mapper-libs libselinux-devel pkgconf-pkg-config systemd-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-1.02.169-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/device-mapper-devel-1.02.169-3.el8.x86_64.rpm \
          "

SRC_URI[device-mapper.sha256sum] = "81f31518d59fca673699c3e64fc0ca060c89e64e299d21b7d41252a3be167e60"
SRC_URI[device-mapper-devel.sha256sum] = "ee42749ee26e941e7e84e6a89fcba13dfc79cd1703fd25e3b25e305d265f8333"
