SUMMARY = "generated recipe based on wget srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls gpgme libidn2 libmetalink libpsl libuuid nettle pkgconfig-native zlib"
RPM_SONAME_REQ_wget = "libc.so.6 libgnutls.so.30 libgpgme.so.11 libidn2.so.0 libmetalink.so.3 libnettle.so.6 libpsl.so.5 libuuid.so.1 libz.so.1"
RDEPENDS_wget = "bash glibc gnutls gpgme info libidn2 libmetalink libpsl libuuid nettle zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wget-1.19.5-8.el8_1.1.x86_64.rpm \
          "

SRC_URI[wget.sha256sum] = "06df8a8486713424a788aa4553876faaea4a55bcf031b585752c3afed97f48e0"
