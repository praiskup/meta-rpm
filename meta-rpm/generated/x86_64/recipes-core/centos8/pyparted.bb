SUMMARY = "generated recipe based on pyparted srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "parted pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pyparted = "libc.so.6 libparted.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pyparted = "glibc parted platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pyparted-3.11.0-13.el8.x86_64.rpm \
          "

SRC_URI[python3-pyparted.sha256sum] = "afa7f94d2463693a0824e8c8efb88969cdca6961ee40ecd8e994b71d38a10fc8"
