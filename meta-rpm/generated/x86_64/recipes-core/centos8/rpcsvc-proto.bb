SUMMARY = "generated recipe based on rpcsvc-proto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_rpcgen = "libc.so.6"
RDEPENDS_rpcgen = "glibc"
RPROVIDES_rpcsvc-proto-devel = "rpcsvc-proto-dev (= 1.3.1)"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rpcgen-1.3.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rpcsvc-proto-devel-1.3.1-4.el8.x86_64.rpm \
          "

SRC_URI[rpcgen.sha256sum] = "442ab8818755fa8dfd0ded4ea2ccdaf6b7dc55b4c56578bfb828d0399b2674e4"
SRC_URI[rpcsvc-proto-devel.sha256sum] = "60489487791a51a1cd30604902ce340e9df7f949a90db3a61f10ae6e469241f1"
