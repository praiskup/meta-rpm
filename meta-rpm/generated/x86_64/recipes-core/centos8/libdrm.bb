SUMMARY = "generated recipe based on libdrm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "drm"
DEPENDS = "libpciaccess pkgconfig-native"
RPM_SONAME_PROV_libdrm = "libdrm.so.2 libdrm_amdgpu.so.1 libdrm_intel.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libkms.so.1"
RPM_SONAME_REQ_libdrm = "libc.so.6 libdrm.so.2 libm.so.6 libpciaccess.so.0 libpthread.so.0"
RDEPENDS_libdrm = "glibc libpciaccess"
RPM_SONAME_REQ_libdrm-devel = "libdrm.so.2 libdrm_amdgpu.so.1 libdrm_intel.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libkms.so.1"
RPROVIDES_libdrm-devel = "libdrm-dev (= 2.4.100)"
RDEPENDS_libdrm-devel = "kernel-headers libdrm pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdrm-2.4.100-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdrm-devel-2.4.100-1.el8.x86_64.rpm \
          "

SRC_URI[libdrm.sha256sum] = "cfd58922daece61ba7c480eaf32dd4c1b983356733795387f8ff5dce8f0aa6b2"
SRC_URI[libdrm-devel.sha256sum] = "ddadd965199a0487f51478a631cc7eac720d5fac1d749cde06bd7140fb26e91e"
