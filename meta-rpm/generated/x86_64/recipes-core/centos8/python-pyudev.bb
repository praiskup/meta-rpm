SUMMARY = "generated recipe based on python-pyudev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyudev = "platform-python python3-six systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pyudev-0.21.0-7.el8.noarch.rpm \
          "

SRC_URI[python3-pyudev.sha256sum] = "aa0007192287faeaecc72d9fa48efdb3108c764d450dc8fea65474476da32c45"
