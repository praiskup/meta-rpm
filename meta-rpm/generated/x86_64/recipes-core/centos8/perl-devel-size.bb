SUMMARY = "generated recipe based on perl-Devel-Size srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-Size = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Size = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Devel-Size-0.81-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Devel-Size.sha256sum] = "afaccd4f0c937c4ec0bfe6a9b4799214c3358464c924c7faac1ef48df8cd4367"
