SUMMARY = "generated recipe based on re2c srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_re2c = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_re2c = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/re2c-0.14.3-2.el8.x86_64.rpm \
          "

SRC_URI[re2c.sha256sum] = "b516fb9ed657aab0d7700ae336cc3e749a1a5952023b4786fe9e5af55906cb39"
