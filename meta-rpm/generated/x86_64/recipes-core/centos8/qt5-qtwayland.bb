SUMMARY = "generated recipe based on qt5-qtwayland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype glib-2.0 libgcc libglvnd libx11 libxcomposite libxext libxkbcommon pkgconfig-native qt5-qtbase qt5-qtdeclarative wayland zlib"
RPM_SONAME_PROV_qt5-qtwayland = "libQt5WaylandClient.so.5 libQt5WaylandCompositor.so.5 libbradient.so libdmabuf-server.so libdrm-egl-server.so libivi-shell.so libqt-plugin-wayland-egl.so libqwayland-egl.so libqwayland-generic.so libqwayland-xcomposite-egl.so libqwayland-xcomposite-glx.so libshm-emulation-server.so libwayland-eglstream-controller.so libwl-shell.so libxcomposite-egl.so libxcomposite-glx.so libxdg-shell-v5.so libxdg-shell-v6.so libxdg-shell.so"
RPM_SONAME_REQ_qt5-qtwayland = "libEGL.so.1 libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5WaylandClient.so.5 libQt5WaylandCompositor.so.5 libX11.so.6 libXcomposite.so.1 libXext.so.6 libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxkbcommon.so.0 libz.so.1"
RDEPENDS_qt5-qtwayland = "fontconfig freetype glib2 glibc libX11 libXcomposite libXext libgcc libglvnd-egl libglvnd-glx libstdc++ libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative zlib"
RPM_SONAME_REQ_qt5-qtwayland-devel = "libQt5Core.so.5 libQt5WaylandClient.so.5 libQt5WaylandCompositor.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtwayland-devel = "qt5-qtwayland-dev (= 5.12.5)"
RDEPENDS_qt5-qtwayland-devel = "cmake-filesystem glibc libgcc libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtwayland"
RPM_SONAME_REQ_qt5-qtwayland-examples = "libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5WaylandClient.so.5 libQt5WaylandCompositor.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-server.so.0 libxkbcommon.so.0"
RDEPENDS_qt5-qtwayland-examples = "glibc libgcc libglvnd-glx libstdc++ libwayland-client libwayland-cursor libwayland-server libxkbcommon qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtwayland"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwayland-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwayland-examples-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qt5-qtwayland-devel-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtwayland.sha256sum] = "eb9f840ba6a99d185824ad54be52570b7a7d54d20ae9591631ab6c81ddb5c368"
SRC_URI[qt5-qtwayland-devel.sha256sum] = "46034351417975da2fd3a4bfd776336527e735a8d40ec50591074a4692648024"
SRC_URI[qt5-qtwayland-examples.sha256sum] = "044fe276f96109e1f240113bfa3a855c79279e4852f23c4d770e1384f145c9c8"
