SUMMARY = "generated recipe based on potrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_potrace = "libpotrace.so.0"
RPM_SONAME_REQ_potrace = "libc.so.6 libm.so.6 libpotrace.so.0 libz.so.1"
RDEPENDS_potrace = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/potrace-1.15-2.el8.x86_64.rpm \
          "

SRC_URI[potrace.sha256sum] = "26219faaa37f02ceca3461a988c152304bbaacfcc6387c8d44e53c248cced7b8"
