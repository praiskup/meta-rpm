SUMMARY = "generated recipe based on opencv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base harfbuzz jasper libdc1394 libgcc libglvnd libjpeg-turbo libpng libucil libunicap libva libwebp openblas openexr pkgconfig-native qt5-qtbase tbb tiff v4l-utils zlib"
RPM_SONAME_REQ_opencv = "libc.so.6 libgcc_s.so.1 libm.so.6 libopencv_aruco.so.3.4 libopencv_calib3d.so.3.4 libopencv_core.so.3.4 libopencv_features2d.so.3.4 libopencv_highgui.so.3.4 libopencv_imgcodecs.so.3.4 libopencv_imgproc.so.3.4 libopencv_videoio.so.3.4 libopencv_xobjdetect.so.3.4 libstdc++.so.6"
RDEPENDS_opencv = "glibc libgcc libstdc++ opencv-contrib opencv-core"
RPM_SONAME_PROV_opencv-contrib = "libopencv_aruco.so.3.4 libopencv_bgsegm.so.3.4 libopencv_bioinspired.so.3.4 libopencv_ccalib.so.3.4 libopencv_cvv.so.3.4 libopencv_datasets.so.3.4 libopencv_dpm.so.3.4 libopencv_face.so.3.4 libopencv_freetype.so.3.4 libopencv_fuzzy.so.3.4 libopencv_hfs.so.3.4 libopencv_img_hash.so.3.4 libopencv_line_descriptor.so.3.4 libopencv_optflow.so.3.4 libopencv_phase_unwrapping.so.3.4 libopencv_plot.so.3.4 libopencv_reg.so.3.4 libopencv_rgbd.so.3.4 libopencv_saliency.so.3.4 libopencv_stereo.so.3.4 libopencv_structured_light.so.3.4 libopencv_surface_matching.so.3.4 libopencv_tracking.so.3.4 libopencv_ximgproc.so.3.4 libopencv_xobjdetect.so.3.4 libopencv_xphoto.so.3.4"
RPM_SONAME_REQ_opencv-contrib = "libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libc.so.6 libfreetype.so.6 libgcc_s.so.1 libgomp.so.1 libharfbuzz.so.0 libm.so.6 libopencv_calib3d.so.3.4 libopencv_core.so.3.4 libopencv_features2d.so.3.4 libopencv_flann.so.3.4 libopencv_highgui.so.3.4 libopencv_imgcodecs.so.3.4 libopencv_imgproc.so.3.4 libopencv_objdetect.so.3.4 libopencv_phase_unwrapping.so.3.4 libopencv_video.so.3.4 libopencv_ximgproc.so.3.4 libpthread.so.0 libstdc++.so.6"
RDEPENDS_opencv-contrib = "freetype glibc harfbuzz libgcc libgomp libstdc++ opencv-core qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_PROV_opencv-core = "libopencv_calib3d.so.3.4 libopencv_core.so.3.4 libopencv_features2d.so.3.4 libopencv_flann.so.3.4 libopencv_highgui.so.3.4 libopencv_imgcodecs.so.3.4 libopencv_imgproc.so.3.4 libopencv_ml.so.3.4 libopencv_objdetect.so.3.4 libopencv_photo.so.3.4 libopencv_shape.so.3.4 libopencv_stitching.so.3.4 libopencv_superres.so.3.4 libopencv_video.so.3.4 libopencv_videoio.so.3.4 libopencv_videostab.so.3.4"
RPM_SONAME_REQ_opencv-core = "libGL.so.1 libIlmImf-2_2.so.22 libQt5Core.so.5 libQt5Gui.so.5 libQt5OpenGL.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libdc1394.so.22 libdl.so.2 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libgomp.so.1 libgstapp-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgstriff-1.0.so.0 libjasper.so.4 libjpeg.so.62 libm.so.6 libopenblas.so.0 libopencv_calib3d.so.3.4 libopencv_core.so.3.4 libopencv_features2d.so.3.4 libopencv_flann.so.3.4 libopencv_imgcodecs.so.3.4 libopencv_imgproc.so.3.4 libopencv_photo.so.3.4 libopencv_video.so.3.4 libopencv_videoio.so.3.4 libpng16.so.16 libpthread.so.0 librt.so.1 libstdc++.so.6 libtbb.so.2 libtiff.so.5 libucil.so.2 libunicap.so.2 libv4l1.so.0 libv4l2.so.0 libva.so.2 libwebp.so.7 libz.so.1"
RDEPENDS_opencv-core = "OpenEXR-libs glib2 glibc gstreamer1 gstreamer1-plugins-base jasper-libs libdc1394 libgcc libglvnd-glx libgomp libjpeg-turbo libpng libstdc++ libtiff libucil libunicap libv4l libva libwebp openblas qt5-qtbase qt5-qtbase-gui tbb zlib"
RPM_SONAME_REQ_opencv-devel = "libopencv_aruco.so.3.4 libopencv_bgsegm.so.3.4 libopencv_bioinspired.so.3.4 libopencv_calib3d.so.3.4 libopencv_ccalib.so.3.4 libopencv_core.so.3.4 libopencv_cvv.so.3.4 libopencv_datasets.so.3.4 libopencv_dpm.so.3.4 libopencv_face.so.3.4 libopencv_features2d.so.3.4 libopencv_flann.so.3.4 libopencv_freetype.so.3.4 libopencv_fuzzy.so.3.4 libopencv_hfs.so.3.4 libopencv_highgui.so.3.4 libopencv_img_hash.so.3.4 libopencv_imgcodecs.so.3.4 libopencv_imgproc.so.3.4 libopencv_line_descriptor.so.3.4 libopencv_ml.so.3.4 libopencv_objdetect.so.3.4 libopencv_optflow.so.3.4 libopencv_phase_unwrapping.so.3.4 libopencv_photo.so.3.4 libopencv_plot.so.3.4 libopencv_reg.so.3.4 libopencv_rgbd.so.3.4 libopencv_saliency.so.3.4 libopencv_shape.so.3.4 libopencv_stereo.so.3.4 libopencv_stitching.so.3.4 libopencv_structured_light.so.3.4 libopencv_superres.so.3.4 libopencv_surface_matching.so.3.4 libopencv_tracking.so.3.4 libopencv_video.so.3.4 libopencv_videoio.so.3.4 libopencv_videostab.so.3.4 libopencv_ximgproc.so.3.4 libopencv_xobjdetect.so.3.4 libopencv_xphoto.so.3.4"
RPROVIDES_opencv-devel = "opencv-dev (= 3.4.6)"
RDEPENDS_opencv-devel = "cmake-filesystem opencv-contrib opencv-core pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/opencv-contrib-3.4.6-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/opencv-core-3.4.6-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opencv-3.4.6-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opencv-devel-3.4.6-5.el8.x86_64.rpm \
          "

SRC_URI[opencv.sha256sum] = "0a3fe8430fe19747fb041c9192e191223906b34d045876c0aa9a3a3fe7583b5b"
SRC_URI[opencv-contrib.sha256sum] = "3cdc1c5e243d23bf56433439aba6af6376ef3536ce226461b680b32eb122e893"
SRC_URI[opencv-core.sha256sum] = "67c043d99ca59995baec97883401422bdd39b4caa364a87830d8c69043f55251"
SRC_URI[opencv-devel.sha256sum] = "e6aae18b7ba6330c4d0446de930c6f3b7ba134d489d1b5ba7b5af7cbb53aeb7c"
