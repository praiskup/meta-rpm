SUMMARY = "generated recipe based on geoclue2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs glib-2.0 json-glib libgcc libnotify libsoup-2.4 modemmanager pkgconfig-native"
RPM_SONAME_REQ_geoclue2 = "libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libmm-glib.so.0 libnotify.so.4 libsoup-2.4.so.1"
RDEPENDS_geoclue2 = "ModemManager-glib avahi-glib avahi-libs bash dbus glib2 glibc json-glib libgcc libnotify libsoup shadow-utils systemd"
RPM_SONAME_REQ_geoclue2-demos = "libc.so.6 libgcc_s.so.1 libgeoclue-2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_geoclue2-demos = "geoclue2 geoclue2-libs glib2 glibc libgcc"
RPM_SONAME_REQ_geoclue2-devel = "libgeoclue-2.so.0"
RPROVIDES_geoclue2-devel = "geoclue2-dev (= 2.5.5)"
RDEPENDS_geoclue2-devel = "geoclue2 geoclue2-libs glib2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_geoclue2-libs = "libgeoclue-2.so.0"
RPM_SONAME_REQ_geoclue2-libs = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_geoclue2-libs = "geoclue2 glib2 glibc libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geoclue2-2.5.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geoclue2-demos-2.5.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geoclue2-libs-2.5.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geoclue2-devel-2.5.5-1.el8.x86_64.rpm \
          "

SRC_URI[geoclue2.sha256sum] = "1dbf5732aa24e30a88f0c746b2c0350a110e269fe8fe2c8a59eadcf565612275"
SRC_URI[geoclue2-demos.sha256sum] = "dd1707dc916e02f7628e8e49407a66f952a23f925cc02017e35e9fcfab2b7baa"
SRC_URI[geoclue2-devel.sha256sum] = "f198c0b34bfb4d443529fd2fd68cb246c0edf6d4c08789861e733dfb43bc431c"
SRC_URI[geoclue2-libs.sha256sum] = "856f616ca79ad6d60113d395abf011d111035a4e92bb54661ee7eddb2f7a18ad"
