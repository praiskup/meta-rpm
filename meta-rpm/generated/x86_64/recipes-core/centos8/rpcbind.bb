SUMMARY = "generated recipe based on rpcbind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtirpc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_rpcbind = "libc.so.6 libpthread.so.0 libsystemd.so.0 libtirpc.so.3"
RDEPENDS_rpcbind = "bash chkconfig coreutils glibc glibc-common libtirpc policycoreutils setup shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpcbind-1.2.5-7.el8.x86_64.rpm \
          "

SRC_URI[rpcbind.sha256sum] = "5bbecb4a58306bce667e0976efbaef0c432c7099850a7f9d882592c2474961ab"
