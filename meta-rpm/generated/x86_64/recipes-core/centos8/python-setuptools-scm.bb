SUMMARY = "generated recipe based on python-setuptools_scm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-setuptools_scm = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-setuptools_scm-1.15.7-4.el8.noarch.rpm \
          "

SRC_URI[python3-setuptools_scm.sha256sum] = "947612f10e39d323d6a656272383020439f7c95b70101da6f472c495762fa2ac"
