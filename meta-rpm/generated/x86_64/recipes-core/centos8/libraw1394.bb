SUMMARY = "generated recipe based on libraw1394 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libraw1394 = "libraw1394.so.11"
RPM_SONAME_REQ_libraw1394 = "libc.so.6 libraw1394.so.11"
RDEPENDS_libraw1394 = "glibc"
RPM_SONAME_REQ_libraw1394-devel = "libraw1394.so.11"
RPROVIDES_libraw1394-devel = "libraw1394-dev (= 2.1.2)"
RDEPENDS_libraw1394-devel = "libraw1394 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libraw1394-2.1.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libraw1394-devel-2.1.2-5.el8.x86_64.rpm \
          "

SRC_URI[libraw1394.sha256sum] = "c9228b57f4abe6f6553a265c32adf080173d3c223dccb9e3156436052a245a89"
SRC_URI[libraw1394-devel.sha256sum] = "1485415db3fdbe189e4b7d34b65ba1208ace243adace1a2df299b348c5e436bc"
