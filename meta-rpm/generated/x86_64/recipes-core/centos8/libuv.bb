SUMMARY = "generated recipe based on libuv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuv = "libuv.so.1"
RPM_SONAME_REQ_libuv = "libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_libuv = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libuv-1.23.1-1.el8.x86_64.rpm \
          "

SRC_URI[libuv.sha256sum] = "c766757b8aff356fb0256cfc2739295fcdcda0debec74edbd1095fa46d87d72a"
