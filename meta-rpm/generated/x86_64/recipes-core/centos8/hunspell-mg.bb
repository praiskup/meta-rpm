SUMMARY = "generated recipe based on hunspell-mg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mg = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mg-0.20050109-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-mg.sha256sum] = "c129e3a33767401de6faf78eacd4cc0f877ca37ae9afa37a1743926d26b96bde"
