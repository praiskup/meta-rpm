SUMMARY = "generated recipe based on nss srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "nspr pkgconfig-native sqlite3 zlib"
RPM_SONAME_PROV_nss = "libnss3.so libsmime3.so libssl3.so"
RPM_SONAME_REQ_nss = "libc.so.6 libdl.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0"
RDEPENDS_nss = "bash crypto-policies glibc nspr nss-softokn nss-sysinit nss-util p11-kit-trust"
RPROVIDES_nss-devel = "nss-dev (= 3.53.1)"
RDEPENDS_nss-devel = "bash nspr-devel nss nss-softokn-devel nss-util-devel pkgconf-pkg-config"
RPM_SONAME_PROV_nss-softokn = "libnssdbm3.so libsoftokn3.so"
RPM_SONAME_REQ_nss-softokn = "libc.so.6 libdl.so.2 libnspr4.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsqlite3.so.0"
RDEPENDS_nss-softokn = "glibc nspr nss-softokn-freebl nss-util sqlite-libs"
RPROVIDES_nss-softokn-devel = "nss-softokn-dev (= 3.53.1)"
RDEPENDS_nss-softokn-devel = "bash nspr-devel nss-softokn nss-softokn-freebl-devel nss-util-devel pkgconf-pkg-config"
RPM_SONAME_PROV_nss-softokn-freebl = "libfreebl3.so libfreeblpriv3.so"
RPM_SONAME_REQ_nss-softokn-freebl = "libc.so.6 libdl.so.2"
RDEPENDS_nss-softokn-freebl = "bash glibc nspr nss-util"
RPROVIDES_nss-softokn-freebl-devel = "nss-softokn-freebl-dev (= 3.53.1)"
RDEPENDS_nss-softokn-freebl-devel = "nss-softokn-freebl"
RPM_SONAME_PROV_nss-sysinit = "libnsssysinit.so"
RPM_SONAME_REQ_nss-sysinit = "libc.so.6 libdl.so.2 libnspr4.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0"
RDEPENDS_nss-sysinit = "bash coreutils glibc nspr nss nss-util sed"
RPM_SONAME_REQ_nss-tools = "libc.so.6 libdl.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libz.so.1"
RDEPENDS_nss-tools = "glibc nspr nss nss-util zlib"
RPM_SONAME_PROV_nss-util = "libnssutil3.so"
RPM_SONAME_REQ_nss-util = "libc.so.6 libdl.so.2 libnspr4.so libplc4.so libplds4.so libpthread.so.0"
RDEPENDS_nss-util = "glibc nspr"
RPROVIDES_nss-util-devel = "nss-util-dev (= 3.53.1)"
RDEPENDS_nss-util-devel = "bash nspr-devel nss-util pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-devel-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-softokn-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-softokn-devel-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-softokn-freebl-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-softokn-freebl-devel-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-sysinit-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-tools-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-util-3.53.1-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss-util-devel-3.53.1-11.el8_2.x86_64.rpm \
          "

SRC_URI[nss.sha256sum] = "531c06e65c1285f7073088dd900ffcd8cb8016b653bc23f8896c63878d61440d"
SRC_URI[nss-devel.sha256sum] = "78f7a510de884c5b955451409aacf3b71156d2341ea5d5df05957bf376a96dbe"
SRC_URI[nss-softokn.sha256sum] = "2060fd4c1e6e12b8ac1ad79691b1bfaa5abf575f292d3d9e6d9fe6c2a7d4b0f6"
SRC_URI[nss-softokn-devel.sha256sum] = "decf8f67ce21897b740be10fd4144230440d7d5f8e7ddf7b7d06fe1b95be11ac"
SRC_URI[nss-softokn-freebl.sha256sum] = "b15c9481607d0e31f169cb70c521207d7716596cc43d19f9b09e7f4d98929f31"
SRC_URI[nss-softokn-freebl-devel.sha256sum] = "d53d4a58a6ac1797585dace260c36691802b43199b12271bb3e582f23d6f6de5"
SRC_URI[nss-sysinit.sha256sum] = "8da1e5ee94ca15833c476fa712205ea86562bac679e649e867f05be8ac881a96"
SRC_URI[nss-tools.sha256sum] = "940d02b8ee2774a0ab5bfac50ef7932ebab002cb546d5b17c930cfa012f02082"
SRC_URI[nss-util.sha256sum] = "c88e3e6346f9dd72498baaf0cb100e51e42de7be6a6f8de5ff75587e27ebd735"
SRC_URI[nss-util-devel.sha256sum] = "9e2eab8ab19515a5a72656e0b416cba4bf149055c04acca69d061bf61698ac35"
