SUMMARY = "generated recipe based on openchange srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libldb libtalloc libtevent pkgconfig-native samba"
RPM_SONAME_PROV_openchange = "libmapi-openchange.so.0 libmapiadmin.so.0 libmapipp.so.0 libocpf.so.0"
RPM_SONAME_REQ_openchange = "libc.so.6 libdcerpc-binding.so.0 libdcerpc-samr.so.0 libdcerpc.so.0 libgcc_s.so.1 libldb.so.2 libm.so.6 libmapi-openchange.so.0 libndr-standard.so.0 libndr.so.0 libpthread.so.0 libsamba-credentials.so.0 libsamba-hostconfig.so.0 libsamba-util.so.0 libstdc++.so.6 libtalloc.so.2 libtevent.so.0"
RDEPENDS_openchange = "glibc libgcc libldb libstdc++ libtalloc libtevent samba-client-libs samba-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openchange-2.3-24.el8.0.2.x86_64.rpm \
          "

SRC_URI[openchange.sha256sum] = "4d749a97fa5839951249e3cff318867e169951fe5afd4deaf1831bbea4a39dc2"
