SUMMARY = "generated recipe based on certmonger srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl dbus-libs e2fsprogs krb5-libs libidn2 libtalloc libtevent libuuid libxml2 nspr nss openldap openssl pkgconfig-native popt xmlrpc-c"
RPM_SONAME_REQ_certmonger = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcurl.so.4 libdbus-1.so.3 libdl.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 libldap-2.4.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpopt.so.0 libpthread.so.0 libresolv.so.2 libsmime3.so libssl3.so libtalloc.so.2 libtevent.so.0 libuuid.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_certmonger = "bash dbus dbus-libs dbus-tools glibc krb5-libs libcom_err libcurl libidn2 libtalloc libtevent libuuid libxml2 nspr nss nss-util openldap openssl-libs popt sed systemd xmlrpc-c xmlrpc-c-client"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/certmonger-0.79.7-6.el8.x86_64.rpm \
          "

SRC_URI[certmonger.sha256sum] = "0a94a69100f35420c6ec247d075347e4679ec708804b1ae0dac03cf7e6e72f49"
