SUMMARY = "generated recipe based on httpcomponents-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_httpcomponents-core = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_httpcomponents-core-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/httpcomponents-core-4.4.10-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/httpcomponents-core-javadoc-4.4.10-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[httpcomponents-core.sha256sum] = "c30a712aa63f64267ed65ad202d1bd2194a8d430d7d64a538784eac8d88beda0"
SRC_URI[httpcomponents-core-javadoc.sha256sum] = "708ee2850ff48261add6d9a805e56824037da1756bcbe82384d38f967644e089"
