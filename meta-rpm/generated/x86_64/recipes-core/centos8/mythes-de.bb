SUMMARY = "generated recipe based on mythes-de srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-de = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-de-0.20180226-3.el8.noarch.rpm \
          "

SRC_URI[mythes-de.sha256sum] = "73ea7f0715d63899d3964a9cad14c470bec8143b89a5ab414d85db50cf6eb780"
