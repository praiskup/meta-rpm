SUMMARY = "generated recipe based on libdaemon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdaemon = "libdaemon.so.0"
RPM_SONAME_REQ_libdaemon = "libc.so.6"
RDEPENDS_libdaemon = "glibc"
RPM_SONAME_REQ_libdaemon-devel = "libdaemon.so.0"
RPROVIDES_libdaemon-devel = "libdaemon-dev (= 0.14)"
RDEPENDS_libdaemon-devel = "libdaemon pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libdaemon-0.14-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdaemon-devel-0.14-15.el8.x86_64.rpm \
          "

SRC_URI[libdaemon.sha256sum] = "dae52ddd215b506d1805b66873194df5043fd758d42960dee68f029eab329b42"
SRC_URI[libdaemon-devel.sha256sum] = "55e34c733ee1f2890e1bdaef07a9574f37d8c2985c3cf812a4127eee5be889e8"
