SUMMARY = "generated recipe based on iscsi-initiator-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "isns-utils kmod libgcc libmount openssl pkgconfig-native platform-python3 systemd-libs"
RPM_SONAME_PROV_iscsi-initiator-utils = "libiscsi.so.0 libopeniscsiusr.so.0.2.0"
RPM_SONAME_REQ_iscsi-initiator-utils = "libc.so.6 libcrypto.so.1.1 libisns.so.0 libkmod.so.2 libmount.so.1 libopeniscsiusr.so.0.2.0 librt.so.1 libsystemd.so.0"
RDEPENDS_iscsi-initiator-utils = "bash glibc iscsi-initiator-utils-iscsiuio isns-utils-libs kmod-libs libmount openssl-libs systemd systemd-libs"
RPM_SONAME_REQ_iscsi-initiator-utils-iscsiuio = "libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 libsystemd.so.0"
RDEPENDS_iscsi-initiator-utils-iscsiuio = "bash glibc iscsi-initiator-utils libgcc systemd-libs"
RPM_SONAME_PROV_python3-iscsi-initiator-utils = "libiscsi.cpython-36m-x86_64-linux-gnu.so"
RPM_SONAME_REQ_python3-iscsi-initiator-utils = "libc.so.6 libiscsi.so.0 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-iscsi-initiator-utils = "glibc iscsi-initiator-utils platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iscsi-initiator-utils-6.2.0.878-4.gitd791ce0.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iscsi-initiator-utils-iscsiuio-6.2.0.878-4.gitd791ce0.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-iscsi-initiator-utils-6.2.0.878-4.gitd791ce0.el8.x86_64.rpm \
          "

SRC_URI[iscsi-initiator-utils.sha256sum] = "5934b477a6fd426c27fa9bd8a8f6d3764b11a46f8a3d905a95ec62ffd3a0afb3"
SRC_URI[iscsi-initiator-utils-iscsiuio.sha256sum] = "bc31351b3fd40900df381ff9b2699ce6632999c4663b2a8b6496445d6328789d"
SRC_URI[python3-iscsi-initiator-utils.sha256sum] = "267e04502a16d9d7c1c568cb40aaf36e3c1e407a7da1b2f5cb441b3af126ce67"
