SUMMARY = "generated recipe based on dlm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_dlm-lib = "libdlm.so.3 libdlm_lt.so.3 libdlmcontrol.so.3"
RPM_SONAME_REQ_dlm-lib = "libc.so.6 libpthread.so.0"
RDEPENDS_dlm-lib = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dlm-lib-4.0.9-3.el8.x86_64.rpm \
          "

SRC_URI[dlm-lib.sha256sum] = "b6d518176729077e4154d5665ac5da8c83164f4409391a972979d43ab8bca054"
