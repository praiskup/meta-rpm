SUMMARY = "generated recipe based on libmetalink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat pkgconfig-native"
RPM_SONAME_PROV_libmetalink = "libmetalink.so.3"
RPM_SONAME_REQ_libmetalink = "libc.so.6 libexpat.so.1"
RDEPENDS_libmetalink = "expat glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmetalink-0.1.3-7.el8.x86_64.rpm \
          "

SRC_URI[libmetalink.sha256sum] = "c4087dec9ffc6e6a164563c46ef09bc0c0bbb5cb992f5fbc8cd3bf20417750e1"
