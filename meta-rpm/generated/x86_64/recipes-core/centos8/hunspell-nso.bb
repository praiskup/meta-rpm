SUMMARY = "generated recipe based on hunspell-nso srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-nso = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-nso-0.20091201-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-nso.sha256sum] = "32665f6ed87b5f2ddcd6e0b67d5a5337ed2c9d3846b94856b07ffc5ce3e31d89"
