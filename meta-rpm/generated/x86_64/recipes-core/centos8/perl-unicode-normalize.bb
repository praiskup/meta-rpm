SUMMARY = "generated recipe based on perl-Unicode-Normalize srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-Normalize = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-Normalize = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Unicode-Normalize-1.25-396.el8.x86_64.rpm \
          "

SRC_URI[perl-Unicode-Normalize.sha256sum] = "99678a57c35343d8b2e2a502efcccc17bde3e40d97d7d2c5f988af8d3aa166d0"
