SUMMARY = "generated recipe based on hunspell-yi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-yi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-yi-1.1-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-yi.sha256sum] = "05eaf50e1e28bb6b4c518588421b473cbe9bc39851fc11f324e74eab140490dc"
