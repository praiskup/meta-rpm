SUMMARY = "generated recipe based on exempi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_exempi = "libexempi.so.3"
RPM_SONAME_REQ_exempi = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libexempi.so.3 libexpat.so.1 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_exempi = "expat glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_exempi-devel = "libexempi.so.3"
RPROVIDES_exempi-devel = "exempi-dev (= 2.4.5)"
RDEPENDS_exempi-devel = "exempi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/exempi-2.4.5-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/exempi-devel-2.4.5-2.el8.x86_64.rpm \
          "

SRC_URI[exempi.sha256sum] = "05cefb6555a263aa58068b9014d3451c0c69833e1ca455c26da5b5f0e7b48d0a"
SRC_URI[exempi-devel.sha256sum] = "db7f7255ba7b760e7af711afe6e1fbaaf756f2073ca689cc1d9726dff7192b48"
