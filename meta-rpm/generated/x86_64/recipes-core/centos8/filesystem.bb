SUMMARY = "generated recipe based on filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_filesystem = "bash setup"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/filesystem-3.8-2.el8.x86_64.rpm \
          "

SRC_URI[filesystem.sha256sum] = "351293e23886ba1e3de8790141f616b3152692c48924ccb61d96bf7ff3ca9aee"
