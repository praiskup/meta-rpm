SUMMARY = "generated recipe based on mod_fcgid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mod_fcgid = "libc.so.6 libpthread.so.0"
RDEPENDS_mod_fcgid = "glibc httpd systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_fcgid-2.3.9-16.el8.x86_64.rpm \
          "

SRC_URI[mod_fcgid.sha256sum] = "3e456d6fe9c70e0ae740ec5f1a546135907063300ebe6cc7c66e674685fab5cb"
