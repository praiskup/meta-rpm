SUMMARY = "generated recipe based on bcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "clang elfutils libgcc ncurses pkgconfig-native zlib"
RPM_SONAME_PROV_bcc = "libbcc.so.0 libbcc_bpf.so.0"
RPM_SONAME_REQ_bcc = "ld-linux-x86-64.so.2 libc.so.6 libclangAST.so.9 libclangASTMatchers.so.9 libclangAnalysis.so.9 libclangBasic.so.9 libclangCodeGen.so.9 libclangDriver.so.9 libclangEdit.so.9 libclangFrontend.so.9 libclangLex.so.9 libclangParse.so.9 libclangRewrite.so.9 libclangSema.so.9 libclangSerialization.so.9 libdl.so.2 libelf.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_bcc = "bcc-tools clang-libs elfutils-libelf glibc libgcc libstdc++ ncurses-libs zlib"
RPM_SONAME_REQ_bcc-devel = "libbcc.so.0 libbcc_bpf.so.0"
RPROVIDES_bcc-devel = "bcc-dev (= 0.11.0)"
RDEPENDS_bcc-devel = "bcc pkgconf-pkg-config"
RDEPENDS_bcc-doc = "bash platform-python"
RPM_SONAME_REQ_bcc-tools = "libc.so.6"
RDEPENDS_bcc-tools = "bash glibc kernel-devel platform-python python3-bcc python3-netaddr"
RDEPENDS_python3-bcc = "bcc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bcc-0.11.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bcc-tools-0.11.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-bcc-0.11.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/bcc-devel-0.11.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/bcc-doc-0.11.0-2.el8.noarch.rpm \
          "

SRC_URI[bcc.sha256sum] = "3069e97634f8d8d08f15e3e85eeb834f6cc8089da417b5ee4107e151cd4a1225"
SRC_URI[bcc-devel.sha256sum] = "86ab37b216a3b82801293b6da00fe7587fbddc92459e7ea396b3873ca0b493ac"
SRC_URI[bcc-doc.sha256sum] = "e350590b3619c65ff6197d6db83f7e760101756150b080e4ec44673db136a314"
SRC_URI[bcc-tools.sha256sum] = "8e934e2b8406fac95b4111546ff598ab3f229e3c96397bc849ed94a179acef2b"
SRC_URI[python3-bcc.sha256sum] = "cc821e907a654e6f8df4beb919eda6ae728c5849ddb696f89d7a8097b6efb197"
