SUMMARY = "generated recipe based on nghttp2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libnghttp2 = "libnghttp2.so.14"
RPM_SONAME_REQ_libnghttp2 = "libc.so.6"
RDEPENDS_libnghttp2 = "glibc"
RPM_SONAME_REQ_libnghttp2-devel = "libnghttp2.so.14"
RPROVIDES_libnghttp2-devel = "libnghttp2-dev (= 1.33.0)"
RDEPENDS_libnghttp2-devel = "libnghttp2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnghttp2-1.33.0-3.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnghttp2-devel-1.33.0-3.el8_2.1.x86_64.rpm \
          "

SRC_URI[libnghttp2.sha256sum] = "0126a384853d46484dec98601a4cb4ce58b2e0411f8f7ef09937174dd5975bac"
SRC_URI[libnghttp2-devel.sha256sum] = "f830291a7fb1a6cd42aed5ef64a958f0e34c9431baf103c2f2031bae1d280aea"
