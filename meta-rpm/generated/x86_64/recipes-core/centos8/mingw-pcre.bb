SUMMARY = "generated recipe based on mingw-pcre srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-pcre = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-gcc-c++ mingw32-pkg-config pkgconf-pkg-config"
RDEPENDS_mingw32-pcre-static = "mingw32-pcre"
RDEPENDS_mingw64-pcre = "mingw64-crt mingw64-filesystem mingw64-gcc mingw64-gcc-c++ mingw64-pkg-config pkgconf-pkg-config"
RDEPENDS_mingw64-pcre-static = "mingw64-pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-pcre-8.38-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-pcre-static-8.38-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-pcre-8.38-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-pcre-static-8.38-4.el8.noarch.rpm \
          "

SRC_URI[mingw32-pcre.sha256sum] = "b5a0547dec6c6720131bd7eca1634e22d01c957af01b3d06ea18c19431e37a2d"
SRC_URI[mingw32-pcre-static.sha256sum] = "b4f8d8af44136aa860a56321c9ccde89af7cef2c58f9c42e8ef9547d60290334"
SRC_URI[mingw64-pcre.sha256sum] = "bb545eb9ad872cfdd06cd9f035424c17c9d007cbfc4d89a56a297eedc6c53b58"
SRC_URI[mingw64-pcre-static.sha256sum] = "6f374ebadd144dd306c371e12e7f2bcac126c61564ea92fffc08b675d76638fe"
