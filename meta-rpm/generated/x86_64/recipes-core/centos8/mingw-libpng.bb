SUMMARY = "generated recipe based on mingw-libpng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-libpng = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-pkg-config mingw32-zlib pkgconf-pkg-config"
RDEPENDS_mingw32-libpng-static = "mingw32-libpng"
RDEPENDS_mingw64-libpng = "mingw64-crt mingw64-filesystem mingw64-pkg-config mingw64-zlib pkgconf-pkg-config"
RDEPENDS_mingw64-libpng-static = "mingw64-libpng"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libpng-1.6.29-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libpng-static-1.6.29-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libpng-1.6.29-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libpng-static-1.6.29-4.el8.noarch.rpm \
          "

SRC_URI[mingw32-libpng.sha256sum] = "25b1356f77d1b401e4fcb89a96fe69c9b2c8466aa16a5a529afdf3876e77b311"
SRC_URI[mingw32-libpng-static.sha256sum] = "52a22c8ca5247a16e36f7e1eeeaa08342b2a93d826b8bdafcf09392c51957785"
SRC_URI[mingw64-libpng.sha256sum] = "6093dbdcea7f076b1615b27057c069c0defba65433e5245b6883110267d00a3c"
SRC_URI[mingw64-libpng-static.sha256sum] = "5740bcaec9fe5412c1381e82a1ec0532d45d58d4b9fdbca8181d39a382f52aa3"
