SUMMARY = "generated recipe based on perl-Params-Check srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Params-Check = "perl-Carp perl-Exporter perl-Locale-Maketext-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Params-Check-0.38-395.el8.noarch.rpm \
          "

SRC_URI[perl-Params-Check.sha256sum] = "27bf689af1b9fe72f704eadf09ea54fe1ba0c5ab55d766afacce1d4b650c696a"
