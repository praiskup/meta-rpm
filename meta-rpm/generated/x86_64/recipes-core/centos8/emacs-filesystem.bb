SUMMARY = "generated recipe based on emacs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/emacs-filesystem-26.1-5.el8.noarch.rpm \
          "

SRC_URI[emacs-filesystem.sha256sum] = "b7b3f2a6ea64d0c7c3afb64c3740d68b9fd6e28a976f5867e0be14682fc494a9"
