SUMMARY = "generated recipe based on mingw-readline srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-readline = "mingw32-crt mingw32-filesystem mingw32-termcap"
RDEPENDS_mingw64-readline = "mingw64-crt mingw64-filesystem mingw64-termcap"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-readline-6.2-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-readline-6.2-11.el8.noarch.rpm \
          "

SRC_URI[mingw32-readline.sha256sum] = "9cd5afcf8c75f175d3b2bf11a17092beff10631bc3a4ea16b8b92b3992adced3"
SRC_URI[mingw64-readline.sha256sum] = "3b7074f95ac05dbab4b69174ef370f6cf9a4f0c2552c19b1008235cc8090a9de"
