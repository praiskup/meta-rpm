SUMMARY = "generated recipe based on libmatchbox srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype glib-2.0 libjpeg-turbo libpng libx11 libxext libxft pango pkgconfig-native zlib"
RPM_SONAME_PROV_libmatchbox = "libmb.so.1"
RPM_SONAME_REQ_libmatchbox = "libX11.so.6 libXext.so.6 libXft.so.2 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 libpango-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0 libpng16.so.16 libz.so.1"
RDEPENDS_libmatchbox = "fontconfig freetype glib2 glibc libX11 libXext libXft libjpeg-turbo libpng pango zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmatchbox-1.9-23.el8.x86_64.rpm \
          "

SRC_URI[libmatchbox.sha256sum] = "38bf42b780376e9e2c4b0203b5679ade14338fd575fee39fa84f095583fa8e00"
