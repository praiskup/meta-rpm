SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libasan = "libasan.so.5"
RPM_SONAME_REQ_libasan = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_libasan = "glibc info libgcc libstdc++"
RPM_SONAME_PROV_liblsan = "liblsan.so.0"
RPM_SONAME_REQ_liblsan = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_liblsan = "glibc info libgcc libstdc++"
RPM_SONAME_PROV_libtsan = "libtsan.so.0"
RPM_SONAME_REQ_libtsan = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_libtsan = "glibc info libgcc libstdc++"
RPM_SONAME_PROV_libubsan = "libubsan.so.1"
RPM_SONAME_REQ_libubsan = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_libubsan = "glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblsan-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libasan-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtsan-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libubsan-8.3.1-5.el8.0.2.x86_64.rpm \
          "

SRC_URI[libasan.sha256sum] = "540d34fb1aaab931faabd6702fb7bffec99a89c842288f849e0928775be7b7d9"
SRC_URI[liblsan.sha256sum] = "9172c07abb36e10ffab1850789dd934e4aec85e5c9483b7c6211a38d25e8a395"
SRC_URI[libtsan.sha256sum] = "02c89b238c1f8b9b4ccdaea1d52e143b09a8f12e183429e5b7a51e4de9f05623"
SRC_URI[libubsan.sha256sum] = "f62223efd7129f2e69fcd6ad38fbcd87c68b767a425c21d69ad33d14793c1c76"
