SUMMARY = "generated recipe based on xorg-x11-drivers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xorg-x11-drivers = "xorg-x11-drv-ati xorg-x11-drv-dummy xorg-x11-drv-evdev xorg-x11-drv-fbdev xorg-x11-drv-intel xorg-x11-drv-libinput xorg-x11-drv-nouveau xorg-x11-drv-qxl xorg-x11-drv-v4l xorg-x11-drv-vesa xorg-x11-drv-vmware xorg-x11-drv-wacom xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drivers-7.7-22.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drivers.sha256sum] = "f973f22604966095ca9cc8c24fc1a2385852fc2108e1ca55492514eddbb0b31c"
