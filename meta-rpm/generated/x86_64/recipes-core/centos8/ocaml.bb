SUMMARY = "generated recipe based on ocaml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native zlib"
RPM_SONAME_PROV_ocaml = "libasmrun_shared.so libcamlrun_shared.so"
RPM_SONAME_REQ_ocaml = "libX11.so.6 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_ocaml = "bash gawk gcc glibc libX11 ocaml-runtime zlib"
RDEPENDS_ocaml-compiler-libs = "ocaml ocaml-runtime"
RPM_SONAME_REQ_ocaml-ocamldoc = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-ocamldoc = "glibc ocaml ocaml-compiler-libs ocaml-runtime"
RPM_SONAME_REQ_ocaml-runtime = "libX11.so.6 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_ocaml-runtime = "glibc libX11 ocaml-compiler-libs util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-4.07.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-compiler-libs-4.07.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-ocamldoc-4.07.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-runtime-4.07.0-3.el8.x86_64.rpm \
          "

SRC_URI[ocaml.sha256sum] = "a6acbcac55c38d20bad96f50a4eb1720f6444dd92a41b0eed7658b92a5af20c7"
SRC_URI[ocaml-compiler-libs.sha256sum] = "a43c7b51c5b0bcfbeb72e8d8b28e8190d6f67eedfbea20469707bae291c6ccfd"
SRC_URI[ocaml-ocamldoc.sha256sum] = "0f655d13efbec016fcf0be7cf8d17ab128d7ab20c2106c09753832793ac4d77a"
SRC_URI[ocaml-runtime.sha256sum] = "d8d2c6a9d1f1e8a13b916fb2d2cd3ab6584bb35bd0d69abb94d49c67779f7525"
