SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_glibc-benchtests = "libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_glibc-benchtests = "bash glibc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glibc-benchtests-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-benchtests.sha256sum] = "9e6adcb18c275d1e7e922b7638be7ad04fb04e96c2eb453944e9d73bcc018841"
