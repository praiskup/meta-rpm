SUMMARY = "generated recipe based on aspell-en srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_aspell-en = "aspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aspell-en-2017.08.24-2.el8.x86_64.rpm \
          "

SRC_URI[aspell-en.sha256sum] = "266dd454e74474b36b74c1494f43b3a37e365646255bb79a5401ed2236dfc947"
