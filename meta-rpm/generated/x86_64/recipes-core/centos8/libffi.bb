SUMMARY = "generated recipe based on libffi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libffi = "libffi.so.6"
RPM_SONAME_REQ_libffi = "libc.so.6"
RDEPENDS_libffi = "glibc"
RPM_SONAME_REQ_libffi-devel = "libffi.so.6"
RPROVIDES_libffi-devel = "libffi-dev (= 3.1)"
RDEPENDS_libffi-devel = "bash info libffi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libffi-3.1-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libffi-devel-3.1-21.el8.x86_64.rpm \
          "

SRC_URI[libffi.sha256sum] = "ed4977e3a7430ab283921516fae09ef747f6c3758635984330f1d9876d398a14"
SRC_URI[libffi-devel.sha256sum] = "b1734a9677b53dd16c1aa9dc28b71a6def6ade2048a34123c9d9b036adebd41a"
