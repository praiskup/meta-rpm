SUMMARY = "generated recipe based on libpagemaker srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libpagemaker = "libpagemaker-0.0.so.0"
RPM_SONAME_REQ_libpagemaker = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libpagemaker = "glibc libgcc librevenge libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpagemaker-0.0.4-3.el8.x86_64.rpm \
          "

SRC_URI[libpagemaker.sha256sum] = "4a36db6334a0cf6be0b40b21e39faa59eaa5b056d2e50122b29aca4b55d21dde"
