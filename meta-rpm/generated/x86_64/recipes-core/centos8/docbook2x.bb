SUMMARY = "generated recipe based on docbook2X srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_docbook2X = "libc.so.6"
RDEPENDS_docbook2X = "bash glibc info libxslt openjade opensp perl-Exporter perl-Getopt-Long perl-IO perl-Text-Tabs+Wrap perl-XML-SAX perl-interpreter perl-libs texinfo"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/docbook2X-0.8.8-29.el8.x86_64.rpm \
          "

SRC_URI[docbook2X.sha256sum] = "7b80900cbf5329b7baee95e66e94e96df815fe9186e8097bc667b40fa26645c5"
