SUMMARY = "generated recipe based on subscription-manager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-subscription-manager-rhsm = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_python3-subscription-manager-rhsm = "glibc openssl-libs platform-python python3-dateutil python3-iniparse python3-libs python3-rpm python3-six subscription-manager-rhsm-certificates"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-subscription-manager-rhsm-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/subscription-manager-rhsm-certificates-1.26.20-1.el8_2.x86_64.rpm \
          "

SRC_URI[python3-subscription-manager-rhsm.sha256sum] = "96de52e1674100a3100bedbaa4b76b3e1fade99be15de21c85facf656c47034a"
SRC_URI[subscription-manager-rhsm-certificates.sha256sum] = "4b9a11e56e1f5d47c6de4cfc6951a3e94d1d63230b0164f1fd7450e060efc865"
