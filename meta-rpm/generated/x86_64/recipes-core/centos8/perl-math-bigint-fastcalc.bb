SUMMARY = "generated recipe based on perl-Math-BigInt-FastCalc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Math-BigInt-FastCalc = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Math-BigInt-FastCalc = "glibc perl-Math-BigInt perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Math-BigInt-FastCalc-0.500.600-6.el8.x86_64.rpm \
          "

SRC_URI[perl-Math-BigInt-FastCalc.sha256sum] = "0325791bc689e4f282b6eca35ea2df6bc64077d375881fa4ab3d9e8a483160bb"
