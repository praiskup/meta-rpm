SUMMARY = "generated recipe based on satyr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native rpm zlib"
RPM_SONAME_PROV_satyr = "libsatyr.so.3"
RPM_SONAME_REQ_satyr = "libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 librpm.so.8 librpmio.so.8 libsatyr.so.3 libstdc++.so.6 libz.so.1"
RDEPENDS_satyr = "elfutils-libelf elfutils-libs glibc libstdc++ rpm-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/satyr-0.26-2.el8.x86_64.rpm \
          "

SRC_URI[satyr.sha256sum] = "b893b1a0e50deb3d518699cb383455767ff681ecb9803dab293b2bcb4cadc30b"
