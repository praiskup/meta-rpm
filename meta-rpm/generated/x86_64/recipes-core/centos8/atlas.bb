SUMMARY = "generated recipe based on atlas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc pkgconfig-native"
RPM_SONAME_PROV_atlas = "libsatlas.so.3 libtatlas.so.3"
RPM_SONAME_REQ_atlas = "libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_atlas = "glibc libgfortran"
RPM_SONAME_PROV_atlas-corei2 = "libsatlas.so.3 libtatlas.so.3"
RPM_SONAME_REQ_atlas-corei2 = "libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_atlas-corei2 = "glibc libgfortran"
RPM_SONAME_REQ_atlas-corei2-devel = "libsatlas.so.3 libtatlas.so.3"
RPROVIDES_atlas-corei2-devel = "atlas-corei2-dev (= 3.10.3)"
RDEPENDS_atlas-corei2-devel = "atlas atlas-corei2 chkconfig"
RPM_SONAME_REQ_atlas-devel = "libsatlas.so.3 libtatlas.so.3"
RPROVIDES_atlas-devel = "atlas-dev (= 3.10.3)"
RDEPENDS_atlas-devel = "atlas bash chkconfig pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/atlas-3.10.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/atlas-corei2-3.10.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/atlas-corei2-devel-3.10.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/atlas-devel-3.10.3-7.el8.x86_64.rpm \
          "

SRC_URI[atlas.sha256sum] = "5079c688ce3b5037e1a15a7a51f50a7ee496cd5a21de51ea49e73a2fbe3daecc"
SRC_URI[atlas-corei2.sha256sum] = "85035f591dad5f346942b1b3366c77e871fe596acb55a002c23bbc1ebe3cefc9"
SRC_URI[atlas-corei2-devel.sha256sum] = "5f8aa437fc9e404568d3258d040702b103af43d94af62e5ace7d1a7d01397e35"
SRC_URI[atlas-devel.sha256sum] = "739e9653ebb04d78471aa25b912e857b2aa5c701ddf7345d052ff41341a3fbd6"
