SUMMARY = "generated recipe based on perl-common-sense srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-common-sense = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-common-sense-3.7.4-8.el8.x86_64.rpm \
          "

SRC_URI[perl-common-sense.sha256sum] = "9af9f6c6aecc1d423824378083ecf8b7ae17d36bcd27f4cfb7fb7eeb9b7c3a1c"
