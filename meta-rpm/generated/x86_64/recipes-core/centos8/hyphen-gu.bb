SUMMARY = "generated recipe based on hyphen-gu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-gu = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-gu-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-gu.sha256sum] = "ba59ae519caeaa8e33a6727e5fe4e16a8e98527c8f1851c3364af904a3fb984b"
