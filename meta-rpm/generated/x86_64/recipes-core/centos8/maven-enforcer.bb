SUMMARY = "generated recipe based on maven-enforcer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-enforcer = "java-1.8.0-openjdk-headless javapackages-filesystem maven-parent"
RDEPENDS_maven-enforcer-api = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib plexus-containers-container-default"
RDEPENDS_maven-enforcer-javadoc = "javapackages-filesystem"
RDEPENDS_maven-enforcer-plugin = "apache-commons-lang java-1.8.0-openjdk-headless javapackages-filesystem maven-enforcer-api maven-enforcer-rules maven-lib maven-plugin-testing-harness plexus-utils"
RDEPENDS_maven-enforcer-rules = "apache-commons-lang bsh java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact-transfer maven-common-artifact-filters maven-dependency-tree maven-enforcer-api maven-lib plexus-i18n plexus-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-enforcer-1.4.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-enforcer-api-1.4.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-enforcer-javadoc-1.4.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-enforcer-plugin-1.4.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-enforcer-rules-1.4.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-enforcer.sha256sum] = "e958c95d2bef4283c0f70dd13f999bdebe32b897fb8569410c6fd78226f7cd41"
SRC_URI[maven-enforcer-api.sha256sum] = "98e571fd5a80fe4ee2778a3d474e68f38b9adc7f0121d8288d5bff576ac2dcb6"
SRC_URI[maven-enforcer-javadoc.sha256sum] = "cea2c0c4e034017715a200a470ead6c3a8cc2cfa9b9f6b8be0377614bef391c8"
SRC_URI[maven-enforcer-plugin.sha256sum] = "c0db0a8f60e44d1982d2ca14d274fc4afb379c850b18e5af56c8410cd6df3638"
SRC_URI[maven-enforcer-rules.sha256sum] = "e824afcc346e0a56d823e801e7dc1cba27d508cfb8a0101a96bb56bfc723a939"
