SUMMARY = "generated recipe based on texinfo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_info = "libc.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_info = "bash glibc ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/info-6.5-6.el8.x86_64.rpm \
          "

SRC_URI[info.sha256sum] = "611da4957e11f4621f53b5d7d491bcba09854de4fad8a5be34e762f4f36b1102"
