SUMMARY = "generated recipe based on iptables srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl libnetfilter-conntrack libnfnetlink libnftnl libpcap pkgconfig-native"
RPM_SONAME_PROV_iptables = "libarpt_mangle.so libebt_802_3.so libebt_among.so libebt_arp.so libebt_arpreply.so libebt_dnat.so libebt_ip.so libebt_ip6.so libebt_log.so libebt_mark.so libebt_mark_m.so libebt_nflog.so libebt_pkttype.so libebt_redirect.so libebt_snat.so libebt_stp.so libebt_vlan.so libip6t_DNAT.so libip6t_DNPT.so libip6t_HL.so libip6t_LOG.so libip6t_MASQUERADE.so libip6t_NETMAP.so libip6t_REDIRECT.so libip6t_REJECT.so libip6t_SNAT.so libip6t_SNPT.so libip6t_ah.so libip6t_dst.so libip6t_eui64.so libip6t_frag.so libip6t_hbh.so libip6t_hl.so libip6t_icmp6.so libip6t_ipv6header.so libip6t_mh.so libip6t_rt.so libip6t_srh.so libipt_CLUSTERIP.so libipt_DNAT.so libipt_ECN.so libipt_LOG.so libipt_MASQUERADE.so libipt_NETMAP.so libipt_REDIRECT.so libipt_REJECT.so libipt_SNAT.so libipt_TTL.so libipt_ULOG.so libipt_ah.so libipt_icmp.so libipt_realm.so libipt_ttl.so libxt_AUDIT.so libxt_CHECKSUM.so libxt_CLASSIFY.so libxt_CONNMARK.so libxt_CONNSECMARK.so libxt_CT.so libxt_DSCP.so libxt_HMARK.so libxt_IDLETIMER.so libxt_LED.so libxt_MARK.so libxt_NFLOG.so libxt_NFQUEUE.so libxt_RATEEST.so libxt_SECMARK.so libxt_SET.so libxt_SYNPROXY.so libxt_TCPMSS.so libxt_TCPOPTSTRIP.so libxt_TEE.so libxt_TOS.so libxt_TPROXY.so libxt_TRACE.so libxt_addrtype.so libxt_bpf.so libxt_cgroup.so libxt_cluster.so libxt_comment.so libxt_connbytes.so libxt_connlabel.so libxt_connlimit.so libxt_connmark.so libxt_conntrack.so libxt_cpu.so libxt_dccp.so libxt_devgroup.so libxt_dscp.so libxt_ecn.so libxt_esp.so libxt_hashlimit.so libxt_helper.so libxt_ipcomp.so libxt_iprange.so libxt_ipvs.so libxt_length.so libxt_limit.so libxt_mac.so libxt_mark.so libxt_multiport.so libxt_nfacct.so libxt_osf.so libxt_owner.so libxt_physdev.so libxt_pkttype.so libxt_policy.so libxt_quota.so libxt_rateest.so libxt_recent.so libxt_rpfilter.so libxt_sctp.so libxt_set.so libxt_socket.so libxt_standard.so libxt_statistic.so libxt_string.so libxt_tcp.so libxt_tcpmss.so libxt_time.so libxt_tos.so libxt_u32.so libxt_udp.so"
RPM_SONAME_REQ_iptables = "libc.so.6 libdl.so.2 libm.so.6 libmnl.so.0 libnetfilter_conntrack.so.3 libnfnetlink.so.0 libnftnl.so.11 libpcap.so.1 libxtables.so.12"
RDEPENDS_iptables = "bash glibc iptables-libs libmnl libnetfilter_conntrack libnfnetlink libnftnl libpcap"
RDEPENDS_iptables-arptables = "bash iptables"
RPM_SONAME_REQ_iptables-devel = "libip4tc.so.2 libip6tc.so.2 libxtables.so.12"
RPROVIDES_iptables-devel = "iptables-dev (= 1.8.4)"
RDEPENDS_iptables-devel = "iptables iptables-libs pkgconf-pkg-config"
RDEPENDS_iptables-ebtables = "bash iptables"
RPM_SONAME_PROV_iptables-libs = "libip4tc.so.0 libip4tc.so.2 libip6tc.so.0 libip6tc.so.2 libiptc.so.0 libxtables.so.12"
RPM_SONAME_REQ_iptables-libs = "libc.so.6 libdl.so.2 libip4tc.so.0 libip6tc.so.0 libpcap.so.1"
RDEPENDS_iptables-libs = "glibc libpcap"
RDEPENDS_iptables-services = "bash iptables systemd"
RPM_SONAME_REQ_iptables-utils = "libc.so.6 libnfnetlink.so.0 libpcap.so.1"
RDEPENDS_iptables-utils = "glibc iptables libnfnetlink libpcap"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-1.8.4-10.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-arptables-1.8.4-10.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-devel-1.8.4-10.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-ebtables-1.8.4-10.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-libs-1.8.4-10.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-services-1.8.4-10.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptables-utils-1.8.4-10.el8_2.1.x86_64.rpm \
          "

SRC_URI[iptables.sha256sum] = "6ff5963dba7ce22d516cd9041e1d7d375403f286e3f5dfd06fad7dd82fece564"
SRC_URI[iptables-arptables.sha256sum] = "10de6ac968f781521946bed90ca77fdde4b22ce81c9c0ec3ee314beb9cf60d19"
SRC_URI[iptables-devel.sha256sum] = "3d50c9d4c1499d69a29b46b9b57f5b2c2af7d389e5f62bf9506b5fd04dde275c"
SRC_URI[iptables-ebtables.sha256sum] = "8df776879d6909b6ef0c760c2345d1327395017f36132417501c7cfc7230bdf8"
SRC_URI[iptables-libs.sha256sum] = "9cc3aab46540977a2568b2022688b01c19e293ccfbde718c605a6e62bde6077f"
SRC_URI[iptables-services.sha256sum] = "377300f0812f4dcc5ac4fc0af4ae008c66e53c643e852fad3bbe161812e4974c"
SRC_URI[iptables-utils.sha256sum] = "fc5cc98d4a0476d241e983ecbb2355e81349d6d5b2d264829605d2117c264104"
