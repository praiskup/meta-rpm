SUMMARY = "generated recipe based on assertj-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_assertj-core = "java-1.8.0-openjdk-headless javapackages-filesystem mockito"
RDEPENDS_assertj-core-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/assertj-core-3.8.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/assertj-core-javadoc-3.8.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[assertj-core.sha256sum] = "1ac93120866d70fcdf9af24fd8f2efc82614d17321eb38aea83fd9f2f6c82b12"
SRC_URI[assertj-core-javadoc.sha256sum] = "c9eef82d4c479ee5c1f314733e25916a79761d211a188a1655525189162f88f8"
