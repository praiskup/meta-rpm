SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libblkid libselinux libsepol pkgconfig-native systemd-libs"
RPM_SONAME_PROV_device-mapper-libs = "libdevmapper.so.1.02"
RPM_SONAME_REQ_device-mapper-libs = "libblkid.so.1 libc.so.6 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libudev.so.1"
RDEPENDS_device-mapper-libs = "device-mapper glibc libblkid libselinux libsepol systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-libs-1.02.169-3.el8.x86_64.rpm \
          "

SRC_URI[device-mapper-libs.sha256sum] = "7456d89afb7d8afc54616bada9f12880768a51ba64e2204cd11c8708d7aecf70"
