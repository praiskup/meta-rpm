SUMMARY = "generated recipe based on gnome-getting-started-docs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-getting-started-docs = "gnome-user-docs"
RDEPENDS_gnome-getting-started-docs-cs = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-de = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-es = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-fr = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-gl = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-hu = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-it = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-pl = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-pt_BR = "gnome-getting-started-docs"
RDEPENDS_gnome-getting-started-docs-ru = "gnome-getting-started-docs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-cs-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-de-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-es-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-fr-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-gl-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-hu-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-it-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-pl-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-pt_BR-3.28.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-getting-started-docs-ru-3.28.2-1.el8.noarch.rpm \
          "

SRC_URI[gnome-getting-started-docs.sha256sum] = "cbcf8dd9e3c8a142a5443f2201c37506dbf19b20f308585cb1205f6c66743afd"
SRC_URI[gnome-getting-started-docs-cs.sha256sum] = "31a18e4cd4c51e71b44e966fd652579d8516aaec4d673b53dcb127502addf025"
SRC_URI[gnome-getting-started-docs-de.sha256sum] = "eb2c38cbaa3662e291e3b961aee982a06081663be083521892635c91f2c4e507"
SRC_URI[gnome-getting-started-docs-es.sha256sum] = "34a1ce8dab1877f55852a728a00f8c81f351c50f16ab88051a501ae20a85c3cc"
SRC_URI[gnome-getting-started-docs-fr.sha256sum] = "ce6bbb08ba7f6ed81e65e4d7921ac11f9d27b483302bb33f381140bf8391fea4"
SRC_URI[gnome-getting-started-docs-gl.sha256sum] = "cf3c0f346203be0a21d076b9ba60460a47938957217da72f81e01f40afbc6a88"
SRC_URI[gnome-getting-started-docs-hu.sha256sum] = "7c2a7061da8bc48a2850970666fdf0793eb27ecfd35f86e76f467b990b72748a"
SRC_URI[gnome-getting-started-docs-it.sha256sum] = "ff99ebf5852f5acada48f2c2b70cff0532924f6c7de2914b6a524394ccc02fb7"
SRC_URI[gnome-getting-started-docs-pl.sha256sum] = "54a199eebb8fc206e537a3bd483e6e4ebc4cbb014ed25db751ad76a857f9cfb7"
SRC_URI[gnome-getting-started-docs-pt_BR.sha256sum] = "d810771b4b1d589f8db950b98184eb91214b2b90dfac6245b2c7fcf136fe2b29"
SRC_URI[gnome-getting-started-docs-ru.sha256sum] = "d734850e83dec69f54906ff521983aff31f0d1855dd8ae82112bed5f242bc02b"
