SUMMARY = "generated recipe based on python-pluggy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pluggy = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pluggy-0.6.0-3.el8.noarch.rpm \
          "

SRC_URI[python3-pluggy.sha256sum] = "c02e078545455f0bb56a2a05fb7e4ae161db0dc4e7ee5a5431f725b190c5ce6c"
