SUMMARY = "generated recipe based on yp-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnsl2 libtirpc libxcrypt pkgconfig-native"
RPM_SONAME_REQ_yp-tools = "libc.so.6 libcrypt.so.1 libnsl.so.2 libtirpc.so.3"
RDEPENDS_yp-tools = "glibc libnsl2 libtirpc libxcrypt ypbind"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/yp-tools-4.2.3-1.el8.x86_64.rpm \
          "

SRC_URI[yp-tools.sha256sum] = "b0d75c4430953971a7e1de0565deb9322f8a1bc81c92839a9388f421a12bba9f"
