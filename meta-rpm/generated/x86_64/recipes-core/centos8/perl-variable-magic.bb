SUMMARY = "generated recipe based on perl-Variable-Magic srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Variable-Magic = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Variable-Magic = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Variable-Magic-0.62-3.el8.x86_64.rpm \
          "

SRC_URI[perl-Variable-Magic.sha256sum] = "8ad4d9baf7a460b200011d43b0718facea34b5fac4f4a31c9a2faee32e75f15c"
