SUMMARY = "generated recipe based on perl-threads-shared srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-threads-shared = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-threads-shared = "glibc perl-Carp perl-Scalar-List-Utils perl-libs perl-threads"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-threads-shared-1.58-2.el8.x86_64.rpm \
          "

SRC_URI[perl-threads-shared.sha256sum] = "b4a14dc0e3550da946d7ca65e54d19fc805e30c6c3dbf5ef3fc077d1d94e6d71"
