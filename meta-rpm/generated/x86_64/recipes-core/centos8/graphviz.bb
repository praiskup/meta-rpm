SUMMARY = "generated recipe based on graphviz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo expat fontconfig freetype gd gdk-pixbuf ghostscript glib-2.0 gtk2 libgcc libice librsvg libsm libtool libx11 libxaw libxext libxmu libxpm libxrender libxt pango pkgconfig-native platform-python3 zlib"
RPM_SONAME_PROV_graphviz = "libcdt.so.5 libcgraph.so.6 libgvc.so.6 libgvplugin_core.so.6 libgvplugin_dot_layout.so.6 libgvplugin_gdk.so.6 libgvplugin_gs.so.6 libgvplugin_gtk.so.6 libgvplugin_neato_layout.so.6 libgvplugin_pango.so.6 libgvplugin_rsvg.so.6 libgvplugin_visio.so.6 libgvplugin_xlib.so.6 libgvpr.so.2 liblab_gamut.so.1 libpathplan.so.4 libxdot.so.4"
RPM_SONAME_REQ_graphviz = "libICE.so.6 libSM.so.6 libX11.so.6 libXaw.so.7 libXext.so.6 libXmu.so.6 libXpm.so.4 libXrender.so.1 libXt.so.6 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcdt.so.5 libcgraph.so.6 libexpat.so.1 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgd.so.3 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgs.so.9 libgtk-x11-2.0.so.0 libgvc.so.6 libgvplugin_neato_layout.so.6 libgvpr.so.2 liblab_gamut.so.1 libltdl.so.7 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpathplan.so.4 librsvg-2.so.2 libstdc++.so.6 libxdot.so.4 libz.so.1"
RDEPENDS_graphviz = "atk bash cairo expat fontconfig freetype gd gdk-pixbuf2 glib2 glibc gtk2 libICE libSM libX11 libXaw libXext libXmu libXpm libXrender libXt libgcc libgs librsvg2 libstdc++ libtool-ltdl pango urw-base35-fonts xorg-x11-fonts-ISO8859-1-100dpi zlib"
RPM_SONAME_REQ_graphviz-devel = "libcdt.so.5 libcgraph.so.6 libgvc.so.6 libgvplugin_core.so.6 libgvplugin_dot_layout.so.6 libgvplugin_gd.so.6 libgvplugin_gdk.so.6 libgvplugin_gs.so.6 libgvplugin_gtk.so.6 libgvplugin_neato_layout.so.6 libgvplugin_pango.so.6 libgvplugin_rsvg.so.6 libgvplugin_visio.so.6 libgvplugin_xlib.so.6 libgvpr.so.2 liblab_gamut.so.1 libpathplan.so.4 libxdot.so.4"
RPROVIDES_graphviz-devel = "graphviz-dev (= 2.40.1)"
RDEPENDS_graphviz-devel = "graphviz graphviz-gd pkgconf-pkg-config"
RPM_SONAME_PROV_graphviz-gd = "libgvplugin_gd.so.6"
RPM_SONAME_REQ_graphviz-gd = "libc.so.6 libcairo.so.2 libcdt.so.5 libcgraph.so.6 libexpat.so.1 libgd.so.3 libglib-2.0.so.0 libgobject-2.0.so.0 libgvc.so.6 libltdl.so.7 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpathplan.so.4 libxdot.so.4 libz.so.1"
RDEPENDS_graphviz-gd = "bash cairo expat gd glib2 glibc graphviz libtool-ltdl pango zlib"
RPM_SONAME_PROV_graphviz-python3 = "libgv_python.so"
RPM_SONAME_REQ_graphviz-python3 = "libc.so.6 libcdt.so.5 libcgraph.so.6 libexpat.so.1 libgcc_s.so.1 libgvc.so.6 libltdl.so.7 libm.so.6 libpathplan.so.4 libpython3.6m.so.1.0 libstdc++.so.6 libxdot.so.4 libz.so.1"
RDEPENDS_graphviz-python3 = "expat glibc graphviz libgcc libstdc++ libtool-ltdl platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/graphviz-2.40.1-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/graphviz-devel-2.40.1-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/graphviz-doc-2.40.1-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/graphviz-gd-2.40.1-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/graphviz-python3-2.40.1-40.el8.x86_64.rpm \
          "

SRC_URI[graphviz.sha256sum] = "b70b6fdaf8fe66a2823d5d415b016acfb0d6f6b6a2a7154820db9ef76e908a90"
SRC_URI[graphviz-devel.sha256sum] = "9f6ab7b0b282c9f2b8a6779546cf59e1812b5fbe879b08aabd823cadd341dec5"
SRC_URI[graphviz-doc.sha256sum] = "7dc06e6a64ceb0bf84f189f12344fae235f43ca8444ed8e9483d75374a8a7e11"
SRC_URI[graphviz-gd.sha256sum] = "bde17f97b1a12fd7bbd3218fa5ba588b78e447cc71d8f9398e37fb92fd0cc130"
SRC_URI[graphviz-python3.sha256sum] = "84a4d8fb5cae43d9f8691c050f63636d70fa9c48c7ad9ca5b6b49a5e36c53115"
