SUMMARY = "generated recipe based on python-configobj srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-configobj = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-configobj-5.0.6-11.el8.noarch.rpm \
          "

SRC_URI[python3-configobj.sha256sum] = "1bd969e0521820374122f5132e5998d9bd2ab185be66565a7266096297419c8e"
