SUMMARY = "generated recipe based on sil-nuosu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-nuosu-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-nuosu-fonts-2.1.1-14.el8.noarch.rpm \
          "

SRC_URI[sil-nuosu-fonts.sha256sum] = "8fa61af2c45134b23a39d1e32738604ed55ba67a6a4633c64edfb39b8e19f036"
