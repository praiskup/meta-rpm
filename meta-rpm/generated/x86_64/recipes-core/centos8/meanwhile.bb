SUMMARY = "generated recipe based on meanwhile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_meanwhile = "libmeanwhile.so.1"
RPM_SONAME_REQ_meanwhile = "libc.so.6 libglib-2.0.so.0 libm.so.6"
RDEPENDS_meanwhile = "glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/meanwhile-1.1.0-23.el8.x86_64.rpm \
          "

SRC_URI[meanwhile.sha256sum] = "fed162fb75c818789a884aaaf3b6322496023221c11d61293c8a34f8aac7da2f"
