SUMMARY = "generated recipe based on qhull srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libqhull = "libqhull.so.7"
RPM_SONAME_REQ_libqhull = "libc.so.6 libm.so.6"
RDEPENDS_libqhull = "glibc"
RPM_SONAME_PROV_libqhull_p = "libqhull_p.so.7"
RPM_SONAME_REQ_libqhull_p = "libc.so.6 libm.so.6"
RDEPENDS_libqhull_p = "glibc"
RPM_SONAME_PROV_libqhull_r = "libqhull_r.so.7"
RPM_SONAME_REQ_libqhull_r = "libc.so.6 libm.so.6"
RDEPENDS_libqhull_r = "glibc"
RPM_SONAME_REQ_qhull-devel = "libqhull.so.7 libqhull_p.so.7 libqhull_r.so.7"
RPROVIDES_qhull-devel = "qhull-dev (= 2015.2)"
RDEPENDS_qhull-devel = "libqhull libqhull_p libqhull_r"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libqhull-2015.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libqhull_p-2015.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libqhull_r-2015.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qhull-devel-2015.2-5.el8.x86_64.rpm \
          "

SRC_URI[libqhull.sha256sum] = "91f5144c4d0c2e56271132f88f40494f35ec1bb7b38392f4e79f630c91cf7cf6"
SRC_URI[libqhull_p.sha256sum] = "75c296f7458e32af0f130879d2a16005f4d240101e264bb907caf58c75e414fb"
SRC_URI[libqhull_r.sha256sum] = "2a3c5bd3780765c1062a73e677aa29bbf49760dff23f78420cc1065cfd9e262f"
SRC_URI[qhull-devel.sha256sum] = "b501a54d603a217c800c3ed1654fc79c7554baba17db87f62fd2df3a82a4b0f2"
