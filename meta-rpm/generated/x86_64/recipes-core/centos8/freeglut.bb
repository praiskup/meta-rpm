SUMMARY = "generated recipe based on freeglut srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libice libx11 libxext libxi libxxf86vm mesa mesa-libglu pkgconfig-native"
RPM_SONAME_PROV_freeglut = "libglut.so.3"
RPM_SONAME_REQ_freeglut = "libGL.so.1 libICE.so.6 libX11.so.6 libXext.so.6 libXi.so.6 libXxf86vm.so.1 libc.so.6 libm.so.6"
RDEPENDS_freeglut = "glibc libICE libX11 libXext libXi libXxf86vm libglvnd-glx"
RPM_SONAME_REQ_freeglut-devel = "libglut.so.3"
RPROVIDES_freeglut-devel = "freeglut-dev (= 3.0.0)"
RDEPENDS_freeglut-devel = "freeglut mesa-libGL-devel mesa-libGLU-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/freeglut-3.0.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/freeglut-devel-3.0.0-8.el8.x86_64.rpm \
          "

SRC_URI[freeglut.sha256sum] = "a63d428d1f02d42d5e815a3dca900e5bf1249a3342bb7bb3f215495aacb9dd2c"
SRC_URI[freeglut-devel.sha256sum] = "68b5984c49a82b268194f4b46360c90fcdb3d768d9238fa6f8a9ef7402395428"
