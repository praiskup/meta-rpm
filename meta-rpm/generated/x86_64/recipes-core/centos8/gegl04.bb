SUMMARY = "generated recipe based on gegl04 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "babl cairo gdk-pixbuf glib-2.0 ilmbase jasper json-glib lcms2 libgcc libjpeg-turbo libpng libraw librsvg libwebp openexr pango pkgconfig-native sdl suitesparse tiff zlib"
RPM_SONAME_PROV_gegl04 = "libgegl-0.4.so.0 libgegl-npd-0.4.so libgegl-sc-0.4.so"
RPM_SONAME_REQ_gegl04 = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmImf-2_2.so.22 libIlmThread-2_2.so.12 libImath-2_2.so.12 libSDL-1.2.so.0 libbabl-0.1.so.0 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgegl-0.4.so.0 libgegl-npd-0.4.so libgegl-sc-0.4.so libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgomp.so.1 libgthread-2.0.so.0 libjasper.so.4 libjpeg.so.62 libjson-glib-1.0.so.0 liblcms2.so.2 libm.so.6 libmvec.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 libraw.so.19 librsvg-2.so.2 libstdc++.so.6 libtiff.so.5 libumfpack.so.5 libwebp.so.7 libz.so.1"
RDEPENDS_gegl04 = "LibRaw OpenEXR-libs SDL babl cairo gdk-pixbuf2 glib2 glibc ilmbase jasper-libs json-glib lcms2 libgcc libgomp libjpeg-turbo libpng librsvg2 libstdc++ libtiff libwebp pango suitesparse zlib"
RPM_SONAME_REQ_gegl04-devel = "libgegl-0.4.so.0"
RPROVIDES_gegl04-devel = "gegl04-dev (= 0.4.4)"
RDEPENDS_gegl04-devel = "babl-devel gegl04 glib2-devel json-glib-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gegl04-0.4.4-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gegl04-devel-0.4.4-6.el8.x86_64.rpm \
          "

SRC_URI[gegl04.sha256sum] = "2c6a5113fffd420adf9de62aa233c5bca2c49dea2bf3b82943c10a1c3219a535"
SRC_URI[gegl04-devel.sha256sum] = "ee1b6034b383c86a64c7b41648c6b673090329a34f80f656a9b715dcfd7c8f4b"
