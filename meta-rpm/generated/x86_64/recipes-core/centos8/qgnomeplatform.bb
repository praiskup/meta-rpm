SUMMARY = "generated recipe based on qgnomeplatform srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcc libglvnd pango pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qgnomeplatform = "libqgnomeplatform.so"
RPM_SONAME_REQ_qgnomeplatform = "libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qgnomeplatform = "adwaita-qt atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcc libglvnd-glx libstdc++ pango qt5-qtbase qt5-qtbase-gui"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qgnomeplatform-0.4-3.el8.x86_64.rpm \
          "

SRC_URI[qgnomeplatform.sha256sum] = "ad7cf1472770a9de36f292e28d1091315244b45b017f3502e862a16de32deaee"
