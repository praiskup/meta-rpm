SUMMARY = "generated recipe based on linuxdoc-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_linuxdoc-tools = "libc.so.6"
RDEPENDS_linuxdoc-tools = "bash gawk glibc groff openjade perl-Exporter perl-File-Temp perl-PathTools perl-interpreter perl-libs texlive-collection-latexrecommended texlive-tetex"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/linuxdoc-tools-0.9.72-5.el8.x86_64.rpm \
          "

SRC_URI[linuxdoc-tools.sha256sum] = "de25e27a19ad36de514489b41338a7ef6aa6c4b15c71bbf718c72b31a02e15c9"
