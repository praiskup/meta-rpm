SUMMARY = "generated recipe based on qt5-qtserialport srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase systemd-libs"
RPM_SONAME_PROV_qt5-qtserialport = "libQt5SerialPort.so.5"
RPM_SONAME_REQ_qt5-qtserialport = "libQt5Core.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libudev.so.1"
RDEPENDS_qt5-qtserialport = "glibc libgcc libstdc++ qt5-qtbase systemd-libs"
RPM_SONAME_REQ_qt5-qtserialport-devel = "libQt5SerialPort.so.5"
RPROVIDES_qt5-qtserialport-devel = "qt5-qtserialport-dev (= 5.12.5)"
RDEPENDS_qt5-qtserialport-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtserialport"
RPM_SONAME_REQ_qt5-qtserialport-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5SerialPort.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtserialport-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtserialport"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtserialport-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtserialport-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtserialport-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtserialport.sha256sum] = "7ba3472b6e1781116217d807884fedf5e62632e82ae2c42ad64ec49436136e1b"
SRC_URI[qt5-qtserialport-devel.sha256sum] = "4c2ad63abd876b33a1d39ca3cadb021fbe0c55f126f36bb3f0efc9e4efad4902"
SRC_URI[qt5-qtserialport-examples.sha256sum] = "1e2eb16067cff22fddccdf39d47d71585b1dfa9108636673b9f0cce112be695d"
