SUMMARY = "generated recipe based on gcc-toolset-9-systemtap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi-libs boost bzip2 dyninst json-c libgcc libselinux ncurses nspr nss pkgconfig-native readline rpm sqlite3 xz zlib"
RDEPENDS_gcc-toolset-9-systemtap = "bash gcc-toolset-9-runtime gcc-toolset-9-systemtap-client gcc-toolset-9-systemtap-devel"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-client = "ld-linux-x86-64.so.2 libavahi-client.so.3 libavahi-common.so.3 libbz2.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6 libz.so.1"
RDEPENDS_gcc-toolset-9-systemtap-client = "avahi-libs bash bzip2-libs coreutils gcc-toolset-9-runtime gcc-toolset-9-systemtap-runtime glibc grep libgcc libstdc++ mokutil nspr nss nss-util openssh-clients perl-interpreter readline rpm-libs sed sqlite-libs unzip xz-libs zip zlib"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-devel = "ld-linux-x86-64.so.2 libavahi-client.so.3 libavahi-common.so.3 libbz2.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6 libz.so.1"
RPROVIDES_gcc-toolset-9-systemtap-devel = "gcc-toolset-9-systemtap-dev (= 4.1)"
RDEPENDS_gcc-toolset-9-systemtap-devel = "avahi-libs bash bzip2-libs gcc gcc-toolset-9-runtime glibc libgcc libstdc++ make nspr nss nss-util readline rpm-libs sqlite-libs xz-libs zlib"
RDEPENDS_gcc-toolset-9-systemtap-initscript = "bash chkconfig gcc-toolset-9-runtime gcc-toolset-9-systemtap initscripts"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-runtime = "ld-linux-x86-64.so.2 libboost_system.so.1.66.0 libc.so.6 libdl.so.2 libdyninstAPI.so.10.1 libgcc_s.so.1 libjson-c.so.4 libm.so.6 libncurses.so.6 libnspr4.so libnss3.so libnssutil3.so libpanel.so.6 libplc4.so libplds4.so libpthread.so.0 libselinux.so.1 libsmime3.so libssl3.so libstdc++.so.6 libsymtabAPI.so.10.1 libtinfo.so.6 libz.so.1"
RDEPENDS_gcc-toolset-9-systemtap-runtime = "bash boost-system dyninst gcc-toolset-9-runtime glibc json-c libgcc libselinux libstdc++ ncurses-libs nspr nss nss-util shadow-utils zlib"
RPROVIDES_gcc-toolset-9-systemtap-sdt-devel = "gcc-toolset-9-systemtap-sdt-dev (= 4.1)"
RDEPENDS_gcc-toolset-9-systemtap-sdt-devel = "gcc-toolset-9-runtime python3-pyparsing python36"
RPM_SONAME_REQ_gcc-toolset-9-systemtap-server = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_gcc-toolset-9-systemtap-server = "avahi-libs bash chkconfig coreutils gcc-toolset-9-runtime gcc-toolset-9-systemtap-devel glibc initscripts libgcc libstdc++ nspr nss nss-util openssl shadow-utils unzip zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-4.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-client-4.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-devel-4.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-initscript-4.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-runtime-4.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-sdt-devel-4.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-systemtap-server-4.1-4.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-systemtap.sha256sum] = "3e77895031252d355895e2a2eaf170b1c783ae33541e7c4d805aa1e8a0ec5c89"
SRC_URI[gcc-toolset-9-systemtap-client.sha256sum] = "504787906b3804e2057bbfa10616bb6eb36d953a4ce2e7d22d66ae877e2c544f"
SRC_URI[gcc-toolset-9-systemtap-devel.sha256sum] = "4e89291105d416bbbdd4bfc7345aece79184ee3c8fbbdb740494403360e3e38b"
SRC_URI[gcc-toolset-9-systemtap-initscript.sha256sum] = "79b0719a90cdaa8938ade0d454ad841d9da96e074dcfbdca4f62ffeda1c68467"
SRC_URI[gcc-toolset-9-systemtap-runtime.sha256sum] = "43bf57ccb7c6ca0adc4d2117ced1eed0b53322859f756703584322567dfc8b99"
SRC_URI[gcc-toolset-9-systemtap-sdt-devel.sha256sum] = "8630d234e4dd714f9abd406bb262827d554e62188e7e974b55c3bd15757552f5"
SRC_URI[gcc-toolset-9-systemtap-server.sha256sum] = "7500849f095aac3c323405ce1cfc5768d48782c2acc8a7cacf55dd4ad79e0b71"
