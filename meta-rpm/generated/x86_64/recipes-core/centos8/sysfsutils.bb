SUMMARY = "generated recipe based on sysfsutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsysfs = "libsysfs.so.2"
RPM_SONAME_REQ_libsysfs = "libc.so.6"
RDEPENDS_libsysfs = "glibc"
RPM_SONAME_REQ_libsysfs-devel = "libsysfs.so.2"
RPROVIDES_libsysfs-devel = "libsysfs-dev (= 2.1.0)"
RDEPENDS_libsysfs-devel = "libsysfs"
RPM_SONAME_REQ_sysfsutils = "libc.so.6 libsysfs.so.2"
RDEPENDS_sysfsutils = "glibc libsysfs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sysfsutils-2.1.0-24.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsysfs-2.1.0-24.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsysfs-devel-2.1.0-24.el8.x86_64.rpm \
          "

SRC_URI[libsysfs.sha256sum] = "53ae3ecd59ec61852cabbd039c748ed63ceb6a88da98587d4c33323ba4095154"
SRC_URI[libsysfs-devel.sha256sum] = "f45ba509ba6ecea07ec4d12ad2452cf545666f89137f580f08994c66c00fb560"
SRC_URI[sysfsutils.sha256sum] = "03d2ef84b1ef0c320caa52a372785013b5e5b065efdde137300387a190d5f2fb"
