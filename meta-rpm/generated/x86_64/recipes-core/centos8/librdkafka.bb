SUMMARY = "generated recipe based on librdkafka srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib libgcc lz4 openssl pkgconfig-native zlib"
RPM_SONAME_PROV_librdkafka = "librdkafka++.so.1 librdkafka.so.1"
RPM_SONAME_REQ_librdkafka = "ld-linux-x86-64.so.2 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblz4.so.1 libpthread.so.0 librdkafka.so.1 librt.so.1 libsasl2.so.3 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_librdkafka = "cyrus-sasl-lib glibc libgcc libstdc++ lz4-libs openssl-libs zlib"
RPM_SONAME_REQ_librdkafka-devel = "librdkafka++.so.1 librdkafka.so.1"
RPROVIDES_librdkafka-devel = "librdkafka-dev (= 0.11.4)"
RDEPENDS_librdkafka-devel = "librdkafka pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librdkafka-0.11.4-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librdkafka-devel-0.11.4-1.el8.x86_64.rpm \
          "

SRC_URI[librdkafka.sha256sum] = "66039e06e56e422be5d082b9ed1120e6b1342db3ff8d80e933009a065f96741b"
SRC_URI[librdkafka-devel.sha256sum] = "9ae216c8341853c767375fd15e77fb4d739186f007078edf37fbf0d0959aa283"
