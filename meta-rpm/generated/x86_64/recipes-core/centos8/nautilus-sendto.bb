SUMMARY = "generated recipe based on nautilus-sendto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_nautilus-sendto = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_nautilus-sendto = "glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nautilus-sendto-3.8.6-2.el8.x86_64.rpm \
          "

SRC_URI[nautilus-sendto.sha256sum] = "d1cb11f88b7729f1c51b134f42c213485b8131f68b8ba16bd90627cc1ec02170"
