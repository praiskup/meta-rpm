SUMMARY = "generated recipe based on perl-XML-LibXML srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxml2 perl pkgconfig-native xz zlib"
RPM_SONAME_REQ_perl-XML-LibXML = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libperl.so.5.26 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_perl-XML-LibXML = "bash glibc libxml2 perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-IO perl-Scalar-List-Utils perl-XML-NamespaceSupport perl-XML-SAX perl-XML-SAX-Base perl-constant perl-interpreter perl-libs perl-parent xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-XML-LibXML-2.0132-2.el8.x86_64.rpm \
          "

SRC_URI[perl-XML-LibXML.sha256sum] = "3c7ab39a2f62e520baf6a2ad34a87bcad1cdcae3362a05729cdc72bb5bfa0343"
