SUMMARY = "generated recipe based on hplip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs dbus-libs libgcc libjpeg-turbo libusb1 net-snmp openssl pkgconfig-native sane-backends zlib"
RPM_SONAME_REQ_hplip = "libc.so.6 libcrypto.so.1.1 libcups.so.2 libcupsimage.so.2 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libhpdiscovery.so.0 libhpip.so.0 libhpmud.so.0 libjpeg.so.62 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libstdc++.so.6 libusb-1.0.so.0 libz.so.1"
RDEPENDS_hplip = "bash cups cups-libs dbus-libs glibc gnupg2 hplip-libs libgcc libjpeg-turbo libstdc++ libusbx net-snmp-libs openssl-libs platform-python python3-dbus python3-pillow systemd wget zlib"
RDEPENDS_hplip-gui = "hplip libsane-hpaio platform-python python3-gobject python3-qt5 python3-reportlab"
RPM_SONAME_PROV_hplip-libs = "libhpdiscovery.so.0 libhpip.so.0 libhpipp.so.0 libhpmud.so.0"
RPM_SONAME_REQ_hplip-libs = "libc.so.6 libcrypto.so.1.1 libcups.so.2 libdl.so.2 libhpdiscovery.so.0 libhpipp.so.0 libhpmud.so.0 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libsane.so.1 libusb-1.0.so.0"
RDEPENDS_hplip-libs = "cups-libs glibc hplip-common libusbx net-snmp-libs openssl-libs platform-python sane-backends-libs"
RPM_SONAME_PROV_libsane-hpaio = "libsane-hpaio.so.1"
RPM_SONAME_REQ_libsane-hpaio = "libc.so.6 libcrypto.so.1.1 libcups.so.2 libdbus-1.so.3 libdl.so.2 libhpdiscovery.so.0 libhpip.so.0 libhpipp.so.0 libhpmud.so.0 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libsane-hpaio.so.1 libusb-1.0.so.0"
RDEPENDS_libsane-hpaio = "cups-libs dbus-libs glibc hplip-libs libusbx net-snmp-libs openssl-libs sane-backends"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hplip-3.18.4-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hplip-common-3.18.4-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hplip-gui-3.18.4-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hplip-libs-3.18.4-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsane-hpaio-3.18.4-9.el8.x86_64.rpm \
          "

SRC_URI[hplip.sha256sum] = "9fe466a08acc435d68268c982bda36c439c10d4e0bfbbc962c31ea5d2b34a4f4"
SRC_URI[hplip-common.sha256sum] = "eb18f1323e58b14075899515ecb0e104a14fbc63c3f062b81f7a3ca6766f6c42"
SRC_URI[hplip-gui.sha256sum] = "59ca765f9a3b9105f027b16ea5ea832bd85be2da8b7abea8e7c0afa422baf428"
SRC_URI[hplip-libs.sha256sum] = "4cccc3536e642e529b5c9ae09b149de054c3bc57d2e57c1b6c3633d42740160d"
SRC_URI[libsane-hpaio.sha256sum] = "b44f1556b64abb19e10ca2a8bd461ac50693280dac35949ef64297ddf7745ac6"
