SUMMARY = "generated recipe based on passwd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit glib-2.0 libselinux libuser pam pkgconfig-native popt"
RPM_SONAME_REQ_passwd = "libaudit.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libpam.so.0 libpam_misc.so.0 libpopt.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_passwd = "audit-libs glib2 glibc libselinux libuser pam popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/passwd-0.80-3.el8.x86_64.rpm \
          "

SRC_URI[passwd.sha256sum] = "9c849b1d4fd605ae749fd9d090715b6034520af871c23729cd4003f22e0990de"
