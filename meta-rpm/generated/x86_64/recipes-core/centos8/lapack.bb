SUMMARY = "generated recipe based on lapack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc pkgconfig-native"
RPM_SONAME_PROV_blas = "libblas.so.3 libcblas.so.3"
RPM_SONAME_REQ_blas = "libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_blas = "glibc libgfortran"
RPM_SONAME_REQ_blas-devel = "libblas.so.3 libcblas.so.3"
RPROVIDES_blas-devel = "blas-dev (= 3.8.0)"
RDEPENDS_blas-devel = "blas gcc-gfortran pkgconf-pkg-config"
RPM_SONAME_PROV_blas64 = "libblas64_.so.3 libcblas64_.so.3"
RPM_SONAME_REQ_blas64 = "libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_blas64 = "blas glibc libgfortran"
RPM_SONAME_PROV_lapack = "liblapack.so.3 liblapacke.so.3"
RPM_SONAME_REQ_lapack = "libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_lapack = "blas glibc libgfortran"
RPM_SONAME_REQ_lapack-devel = "liblapack.so.3 liblapacke.so.3"
RPROVIDES_lapack-devel = "lapack-dev (= 3.8.0)"
RDEPENDS_lapack-devel = "blas-devel lapack pkgconf-pkg-config"
RDEPENDS_lapack-static = "lapack-devel"
RPM_SONAME_PROV_lapack64 = "liblapack64_.so.3"
RPM_SONAME_REQ_lapack64 = "libblas.so.3 libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_lapack64 = "blas blas64 glibc libgfortran"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/blas-3.8.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/blas64-3.8.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lapack-3.8.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lapack64-3.8.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/blas-devel-3.8.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lapack-devel-3.8.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lapack-static-3.8.0-8.el8.x86_64.rpm \
          "

SRC_URI[blas.sha256sum] = "78659f3d904cad45380c3344f4a7ef58b62485bda950b043c2ac6e48f2aab3b4"
SRC_URI[blas-devel.sha256sum] = "7879f5043122ee22887230e82a32c6df42953514efc6c26965cc311557c7bddd"
SRC_URI[blas64.sha256sum] = "67cc9a63609a5a131ba053dd948772fa82bc6d37ddc58f1f3e0a37ff38e0c8a8"
SRC_URI[lapack.sha256sum] = "644673ec7cd975b1ef2f8d4ead72f8c349635a6c0875a4bc243f4b5621a64f42"
SRC_URI[lapack-devel.sha256sum] = "1007ccedf62d63bb0d1a36df4e3d400c1d19769947f872301ec5ac166262cecf"
SRC_URI[lapack-static.sha256sum] = "febd44e64caab8471cd3919bed7a920668484119a6ffa04b5f2d31e962d2ea89"
SRC_URI[lapack64.sha256sum] = "f18bd04d62710c352e029ef1c0ebe83c571f71e10a111ed4f1b6b0b6ca5219ae"
