SUMMARY = "generated recipe based on openblas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc libgcc pkgconfig-native"
RPM_SONAME_PROV_openblas = "libopenblas.so.0"
RPM_SONAME_REQ_openblas = "libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas = "glibc libgfortran"
RPM_SONAME_PROV_openblas-Rblas = "libRblas.so"
RPM_SONAME_REQ_openblas-Rblas = "libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas-Rblas = "glibc libgfortran"
RPM_SONAME_REQ_openblas-devel = "libopenblaso.so.0 libopenblaso64.so.0 libopenblaso64_.so.0"
RPROVIDES_openblas-devel = "openblas-dev (= 0.3.3)"
RDEPENDS_openblas-devel = "openblas openblas-openmp openblas-openmp64 openblas-openmp64_ openblas-serial64 openblas-serial64_ openblas-srpm-macros openblas-threads openblas-threads64 openblas-threads64_"
RPM_SONAME_PROV_openblas-openmp = "libopenblaso.so.0"
RPM_SONAME_REQ_openblas-openmp = "libc.so.6 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-openmp = "glibc libgcc libgfortran libgomp"
RPM_SONAME_PROV_openblas-openmp64 = "libopenblaso64.so.0"
RPM_SONAME_REQ_openblas-openmp64 = "libc.so.6 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-openmp64 = "glibc libgcc libgfortran libgomp"
RPM_SONAME_PROV_openblas-openmp64_ = "libopenblaso64_.so.0"
RPM_SONAME_REQ_openblas-openmp64_ = "libc.so.6 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-openmp64_ = "glibc libgcc libgfortran libgomp"
RPM_SONAME_PROV_openblas-serial64 = "libopenblas64.so.0"
RPM_SONAME_REQ_openblas-serial64 = "libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas-serial64 = "glibc libgfortran"
RPM_SONAME_PROV_openblas-serial64_ = "libopenblas64_.so.0"
RPM_SONAME_REQ_openblas-serial64_ = "libc.so.6 libgfortran.so.5 libm.so.6"
RDEPENDS_openblas-serial64_ = "glibc libgfortran"
RDEPENDS_openblas-static = "openblas-devel"
RPM_SONAME_PROV_openblas-threads = "libopenblasp.so.0"
RPM_SONAME_REQ_openblas-threads = "libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-threads = "glibc libgfortran"
RPM_SONAME_PROV_openblas-threads64 = "libopenblasp64.so.0"
RPM_SONAME_REQ_openblas-threads64 = "libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-threads64 = "glibc libgfortran"
RPM_SONAME_PROV_openblas-threads64_ = "libopenblasp64_.so.0"
RPM_SONAME_REQ_openblas-threads64_ = "libc.so.6 libgfortran.so.5 libm.so.6 libpthread.so.0"
RDEPENDS_openblas-threads64_ = "glibc libgfortran"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openblas-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openblas-threads-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-Rblas-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-devel-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-openmp-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-openmp64-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-openmp64_-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-serial64-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-serial64_-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-static-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-threads64-0.3.3-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openblas-threads64_-0.3.3-5.el8.x86_64.rpm \
          "

SRC_URI[openblas.sha256sum] = "f4b72ae569f0871218d01a473e55266f1bd976a05d800a4d3c0aaf6a65f27f93"
SRC_URI[openblas-Rblas.sha256sum] = "cc7800cfe9286c80ee8e6c43b4b23384b11b38a9e49d1b54eea8f646bcf36767"
SRC_URI[openblas-devel.sha256sum] = "12bd63ffc668bd5277c4fdc8d65a00d22a6c604327067658ea6e9347bab7580a"
SRC_URI[openblas-openmp.sha256sum] = "f81dd70043b078789da7e1e372ff647aacaf6b47ef4161f4673bad4014f06868"
SRC_URI[openblas-openmp64.sha256sum] = "046d37189027ddbaa58820d24c862ef7cedf06271c03911d73ac498781cde950"
SRC_URI[openblas-openmp64_.sha256sum] = "94c0f6415ac060a489633b92618d8a736e42f9fbf540bd668cd8474205ae7242"
SRC_URI[openblas-serial64.sha256sum] = "499749f45fbb79d04eb2faa04290e7560b3a8cf0bfee0f42551d954fb434a4cf"
SRC_URI[openblas-serial64_.sha256sum] = "f2d3361a4b535f1e7d57bb2fc144937aa3e22608e4da89d86a0cb7edf66d0e90"
SRC_URI[openblas-static.sha256sum] = "92656c095e7eb07297c5f9a80246ec6e005e3e48a54da863ed122cc7bbcb46cd"
SRC_URI[openblas-threads.sha256sum] = "d7b28d79f64532e6d23f12cd84c31d1f79a97f35f2efe5db729c05e925ec86e3"
SRC_URI[openblas-threads64.sha256sum] = "42233facdf0b2f2545540ac8e16a800b37235f32ea244a07a4f38fc62601a693"
SRC_URI[openblas-threads64_.sha256sum] = "50cd6476424ed8adbf04176e39cb31c9131a3383c49230c6d4fde5c45b3eb1ae"
