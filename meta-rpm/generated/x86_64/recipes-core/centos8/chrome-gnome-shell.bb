SUMMARY = "generated recipe based on chrome-gnome-shell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_chrome-gnome-shell = "dbus gnome-shell hicolor-icon-theme mozilla-filesystem platform-python python3-gobject-base python3-requests"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/chrome-gnome-shell-10.1-6.el8.x86_64.rpm \
          "

SRC_URI[chrome-gnome-shell.sha256sum] = "f56896ed3db7e250185f36acd83ce408a649a2a3f921d5a177a7ebd5963b5154"
