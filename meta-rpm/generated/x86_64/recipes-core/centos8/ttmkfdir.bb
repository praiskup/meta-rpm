SUMMARY = "generated recipe based on ttmkfdir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_ttmkfdir = "libc.so.6 libfreetype.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_ttmkfdir = "freetype glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ttmkfdir-3.0.9-54.el8.x86_64.rpm \
          "

SRC_URI[ttmkfdir.sha256sum] = "00ab80523e3b617a20f791387a37f4a7394d4e4a0063b4c90b95662fe258f1d5"
