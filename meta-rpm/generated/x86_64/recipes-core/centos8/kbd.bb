SUMMARY = "generated recipe based on kbd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pam pkgconfig-native"
RPM_SONAME_REQ_kbd = "libc.so.6 libpam.so.0 libpam_misc.so.0"
RDEPENDS_kbd = "bash glibc kbd-legacy kbd-misc pam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kbd-2.0.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kbd-legacy-2.0.4-8.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kbd-misc-2.0.4-8.el8.noarch.rpm \
          "

SRC_URI[kbd.sha256sum] = "077638ee2e6c494b6d6a20af51c4cf668a41e226317c69fc9aad77224f9dcda9"
SRC_URI[kbd-legacy.sha256sum] = "331cfded11fa060603aa6fe768ac78e0233481dad2ae2b082462d3902f1ae7c9"
SRC_URI[kbd-misc.sha256sum] = "e5f83b3943b3fa81681a8cfabbf818afff97818b24c891e40027f8ff11048b6a"
