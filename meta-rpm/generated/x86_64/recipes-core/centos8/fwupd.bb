SUMMARY = "generated recipe based on fwupd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "efivar elfutils gcab glib-2.0 gnutls gpgme json-glib libappstream-glib libarchive libgcc libgpg-error libgudev libgusb libsmbios libsoup-2.4 libuuid pkgconfig-native polkit sqlite3"
RPM_SONAME_PROV_fwupd = "libfu_plugin_altos.so libfu_plugin_amt.so libfu_plugin_colorhug.so libfu_plugin_csr.so libfu_plugin_dell.so libfu_plugin_dell_dock.so libfu_plugin_dell_esrt.so libfu_plugin_dfu.so libfu_plugin_ebitdo.so libfu_plugin_flashrom.so libfu_plugin_nitrokey.so libfu_plugin_nvme.so libfu_plugin_redfish.so libfu_plugin_rts54hid.so libfu_plugin_rts54hub.so libfu_plugin_steelseries.so libfu_plugin_superio.so libfu_plugin_synapticsmst.so libfu_plugin_test.so libfu_plugin_thunderbolt.so libfu_plugin_thunderbolt_power.so libfu_plugin_udev.so libfu_plugin_uefi.so libfu_plugin_unifying.so libfu_plugin_upower.so libfu_plugin_wacomhid.so libfwupd.so.2"
RPM_SONAME_REQ_fwupd = "libappstream-glib.so.8 libarchive.so.13 libc.so.6 libefiboot.so.1 libefivar.so.1 libelf.so.1 libfwupd.so.2 libgcab-1.0.so.0 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libgudev-1.0.so.0 libgusb.so.2 libjson-glib-1.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsmbios_c.so.2 libsoup-2.4.so.1 libsqlite3.so.0 libuuid.so.1"
RDEPENDS_fwupd = "bash bubblewrap efivar-libs elfutils-libelf glib2 glibc gnutls gpgme json-glib libappstream-glib libarchive libgcab1 libgcc libgpg-error libgudev libgusb libsmbios libsoup libuuid platform-python polkit-libs sqlite-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fwupd-1.1.4-7.el8.x86_64.rpm \
          "

SRC_URI[fwupd.sha256sum] = "9b7457951d5291cc9197cdaf2f2d6c66919731fd4234fa4a2edb0d00e8ec36c2"
