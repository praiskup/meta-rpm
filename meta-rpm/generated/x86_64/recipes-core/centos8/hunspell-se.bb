SUMMARY = "generated recipe based on hunspell-se srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-se = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-se-1.0-0.14.beta7.el8.noarch.rpm \
          "

SRC_URI[hunspell-se.sha256sum] = "a8677f454bacea4e80fa14cba8b6d76514715ef3b1becd405e043de2458aa563"
