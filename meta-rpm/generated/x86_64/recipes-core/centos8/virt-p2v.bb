SUMMARY = "generated recipe based on virt-p2v srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_virt-p2v-maker = "bash binutils gawk gzip libguestfs-tools-c xz"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/virt-p2v-maker-1.42.0-5.el8.x86_64.rpm \
          "

SRC_URI[virt-p2v-maker.sha256sum] = "8f37775f37572f85e0c571f09e21240db4905b43a38a8e0c933191f3ff572a40"
