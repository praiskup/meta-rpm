SUMMARY = "generated recipe based on pcre srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_pcre = "libpcre.so.1 libpcreposix.so.0"
RPM_SONAME_REQ_pcre = "libc.so.6 libpcre.so.1 libpthread.so.0"
RDEPENDS_pcre = "glibc"
RPM_SONAME_PROV_pcre-cpp = "libpcrecpp.so.0"
RPM_SONAME_REQ_pcre-cpp = "libc.so.6 libgcc_s.so.1 libm.so.6 libpcre.so.1 libstdc++.so.6"
RDEPENDS_pcre-cpp = "glibc libgcc libstdc++ pcre"
RPM_SONAME_REQ_pcre-devel = "libpcre.so.1 libpcre16.so.0 libpcre32.so.0 libpcrecpp.so.0 libpcreposix.so.0"
RPROVIDES_pcre-devel = "pcre-dev (= 8.42)"
RDEPENDS_pcre-devel = "bash pcre pcre-cpp pcre-utf16 pcre-utf32 pkgconf-pkg-config"
RDEPENDS_pcre-static = "pcre-devel"
RPM_SONAME_PROV_pcre-utf16 = "libpcre16.so.0"
RPM_SONAME_REQ_pcre-utf16 = "libc.so.6 libpthread.so.0"
RDEPENDS_pcre-utf16 = "glibc"
RPM_SONAME_PROV_pcre-utf32 = "libpcre32.so.0"
RPM_SONAME_REQ_pcre-utf32 = "libc.so.6 libpthread.so.0"
RDEPENDS_pcre-utf32 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre-8.42-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre-cpp-8.42-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre-devel-8.42-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre-utf16-8.42-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcre-utf32-8.42-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pcre-static-8.42-4.el8.x86_64.rpm \
          "
SRC_URI = "file://pcre-8.42-4.el8.patch"

SRC_URI[pcre.sha256sum] = "96a45c43313c3b063529bca7fce7220ac7653c4809c3c32154863693e553f935"
SRC_URI[pcre-cpp.sha256sum] = "6ffd0dbbe7f529f9dc9373125ec2f58d8203674a44d95f0a9c5bb9c0d3c7cda9"
SRC_URI[pcre-devel.sha256sum] = "fdf4c33521872202a1e17f3744ad76fb7317ed9cef449d2810507c93dc2e5bf0"
SRC_URI[pcre-static.sha256sum] = "79c85e8ab7d24bfe6a403c75b6f56ff857bb90462b92e09d8cef2e546d0f9070"
SRC_URI[pcre-utf16.sha256sum] = "ea0bbd83ed45cfff452f1cc4364644c6f61a4382851ab1385d77a1dbaa864730"
SRC_URI[pcre-utf32.sha256sum] = "fd132f662ee253b1dc3f7798d183cb51379f8f1464242187cfcf80ef5ccf5f95"
