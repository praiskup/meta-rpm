SUMMARY = "generated recipe based on libvorbis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libogg pkgconfig-native"
RPM_SONAME_PROV_libvorbis = "libvorbis.so.0 libvorbisenc.so.2 libvorbisfile.so.3"
RPM_SONAME_REQ_libvorbis = "libc.so.6 libm.so.6 libogg.so.0 libvorbis.so.0"
RDEPENDS_libvorbis = "glibc libogg"
RPM_SONAME_REQ_libvorbis-devel = "libvorbis.so.0 libvorbisenc.so.2 libvorbisfile.so.3"
RPROVIDES_libvorbis-devel = "libvorbis-dev (= 1.3.6)"
RDEPENDS_libvorbis-devel = "libogg-devel libvorbis pkgconf-pkg-config"
RDEPENDS_libvorbis-devel-docs = "libvorbis-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvorbis-1.3.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvorbis-devel-1.3.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvorbis-devel-docs-1.3.6-2.el8.noarch.rpm \
          "

SRC_URI[libvorbis.sha256sum] = "5349766076fcd168287f116b023caa93d451243663b00a5ca5991f74067bf7af"
SRC_URI[libvorbis-devel.sha256sum] = "eff02653076ae6ad23de5dd6e78b9337ac9384a32aca81b219411f1aa21f3e9f"
SRC_URI[libvorbis-devel-docs.sha256sum] = "06394fa449c5fa9d21e6e11b5231c40df7f9edb3cfe35d92873c7ecab9fad220"
