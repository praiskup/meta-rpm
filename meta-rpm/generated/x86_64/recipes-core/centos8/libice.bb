SUMMARY = "generated recipe based on libICE srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libICE = "libICE.so.6"
RPM_SONAME_REQ_libICE = "libc.so.6"
RDEPENDS_libICE = "glibc"
RPM_SONAME_REQ_libICE-devel = "libICE.so.6"
RPROVIDES_libICE-devel = "libICE-dev (= 1.0.9)"
RDEPENDS_libICE-devel = "libICE pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libICE-1.0.9-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libICE-devel-1.0.9-15.el8.x86_64.rpm \
          "

SRC_URI[libICE.sha256sum] = "4ffd6fc4d6fc75c6666aecdda49ec9a559635480588562c9a85e36cc962569b5"
SRC_URI[libICE-devel.sha256sum] = "286c6dbe53f0e862919f01ffdf2d5b4d6f4319716a394e832466acf23f80d9a5"
