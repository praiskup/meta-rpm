SUMMARY = "generated recipe based on mallard-rng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mallard-rng = "bash libxml2 pkgconf-pkg-config xml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mallard-rng-1.0.3-1.el8.noarch.rpm \
          "

SRC_URI[mallard-rng.sha256sum] = "19634a9be97d1cbf76b59e9b69567334ea11eef984b03ee96efdbe0cebb1bc3f"
