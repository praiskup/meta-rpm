SUMMARY = "generated recipe based on avahi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk avahi-libs cairo dbus-libs expat gdbm gdk-pixbuf glib-2.0 gtk+3 libcap libdaemon pango pkgconfig-native"
RPM_SONAME_PROV_avahi = "libavahi-core.so.7"
RPM_SONAME_REQ_avahi = "libavahi-common.so.3 libavahi-core.so.7 libc.so.6 libcap.so.2 libdaemon.so.0 libdbus-1.so.3 libdl.so.2 libexpat.so.1 libpthread.so.0"
RDEPENDS_avahi = "avahi-libs bash coreutils dbus dbus-libs expat glibc libcap libdaemon shadow-utils systemd"
RPM_SONAME_REQ_avahi-autoipd = "libc.so.6 libdaemon.so.0"
RDEPENDS_avahi-autoipd = "avahi-libs bash glibc libdaemon shadow-utils"
RPM_SONAME_PROV_avahi-compat-howl = "libhowl.so.0"
RPM_SONAME_REQ_avahi-compat-howl = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_avahi-compat-howl = "avahi-libs dbus-libs glibc"
RPM_SONAME_REQ_avahi-compat-howl-devel = "libhowl.so.0"
RPROVIDES_avahi-compat-howl-devel = "avahi-compat-howl-dev (= 0.7)"
RDEPENDS_avahi-compat-howl-devel = "avahi-compat-howl avahi-devel pkgconf-pkg-config"
RPM_SONAME_PROV_avahi-compat-libdns_sd = "libdns_sd.so.1"
RPM_SONAME_REQ_avahi-compat-libdns_sd = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_avahi-compat-libdns_sd = "avahi-libs dbus-libs glibc"
RPM_SONAME_REQ_avahi-compat-libdns_sd-devel = "libdns_sd.so.1"
RPROVIDES_avahi-compat-libdns_sd-devel = "avahi-compat-libdns_sd-dev (= 0.7)"
RDEPENDS_avahi-compat-libdns_sd-devel = "avahi-compat-libdns_sd avahi-devel pkgconf-pkg-config"
RPM_SONAME_PROV_avahi-glib = "libavahi-glib.so.1"
RPM_SONAME_REQ_avahi-glib = "libavahi-common.so.3 libc.so.6 libglib-2.0.so.0 libpthread.so.0"
RDEPENDS_avahi-glib = "avahi-libs glib2 glibc"
RPM_SONAME_PROV_avahi-gobject = "libavahi-gobject.so.0"
RPM_SONAME_REQ_avahi-gobject = "libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_avahi-gobject = "avahi-glib avahi-libs dbus-libs glib2 glibc"
RPM_SONAME_PROV_avahi-ui-gtk3 = "libavahi-ui-gtk3.so.0"
RPM_SONAME_REQ_avahi-ui-gtk3 = "libatk-1.0.so.0 libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libdl.so.2 libgdbm.so.6 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_avahi-ui-gtk3 = "atk avahi-glib avahi-libs cairo cairo-gobject dbus-libs gdbm-libs gdk-pixbuf2 glib2 glibc gtk3 pango"
RDEPENDS_python3-avahi = "avahi avahi-libs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/avahi-ui-gtk3-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/avahi-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/avahi-autoipd-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/avahi-glib-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/avahi-gobject-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-avahi-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/avahi-compat-howl-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/avahi-compat-howl-devel-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/avahi-compat-libdns_sd-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/avahi-compat-libdns_sd-devel-0.7-19.el8.x86_64.rpm \
          "

SRC_URI[avahi.sha256sum] = "443871b78b561d6cf1cd59a2a37dcfdc6a1d122461d12e5171a3687511ce9466"
SRC_URI[avahi-autoipd.sha256sum] = "928819dfbcb34a849bd9b15eaea69ffbcd09872cc83880694149a3ac69c93376"
SRC_URI[avahi-compat-howl.sha256sum] = "4a6271a6ba8bcfa3720534f924ec1e25e294042a589072883e80fe5296153207"
SRC_URI[avahi-compat-howl-devel.sha256sum] = "1140a9fe6b0d7898961de1a7fb32d016c026dde663fbb01298395a62e85ab68e"
SRC_URI[avahi-compat-libdns_sd.sha256sum] = "0cb070c9bb8a3fd573cb2a0998de23b992b2ddb78b7d43a6678680ddafe471bd"
SRC_URI[avahi-compat-libdns_sd-devel.sha256sum] = "f8fbc03e28274c5478d3611be0bdd333451598feb9522b4a26a42e83e841256b"
SRC_URI[avahi-glib.sha256sum] = "9c906043721d3bd832c322fb52ca6ab318123b17a1652ea2800daa05253c0663"
SRC_URI[avahi-gobject.sha256sum] = "751d35e55a490a4838f40fa6f0b84452270c0b772575f8e58ff4e9737b63906e"
SRC_URI[avahi-ui-gtk3.sha256sum] = "5058ea2369d5ac304078b453899859e4c900841482db16230a7c9929ebdf2967"
SRC_URI[python3-avahi.sha256sum] = "2bec23ad7ce105d372526aa52bd9abb1f869d1f74d8a7c7c10606f7a0fa055dd"
