SUMMARY = "generated recipe based on hunspell-so srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-so = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-so-1.0.2-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-so.sha256sum] = "e584c64adbefdfe760e115303e510f313c80cf7996e41950d81ee14178df7b81"
