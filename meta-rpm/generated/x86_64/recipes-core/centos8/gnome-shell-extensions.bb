SUMMARY = "generated recipe based on gnome-shell-extensions srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-classic-session = "gnome-shell-extension-apps-menu gnome-shell-extension-desktop-icons gnome-shell-extension-horizontal-workspaces gnome-shell-extension-launch-new-instance gnome-shell-extension-places-menu gnome-shell-extension-window-list nautilus"
RDEPENDS_gnome-shell-extension-apps-menu = "gnome-menus gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-auto-move-windows = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-common = "gnome-shell"
RDEPENDS_gnome-shell-extension-dash-to-dock = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-desktop-icons = "gjs gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-disable-screenshield = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-drive-menu = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-horizontal-workspaces = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-launch-new-instance = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-native-window-placement = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-no-hot-corner = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-panel-favorites = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-places-menu = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-screenshot-window-sizer = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-systemMonitor = "gnome-shell-extension-common libgtop2"
RDEPENDS_gnome-shell-extension-top-icons = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-updates-dialog = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-user-theme = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-window-grouper = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-window-list = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-windowsNavigator = "gnome-shell-extension-common"
RDEPENDS_gnome-shell-extension-workspace-indicator = "gnome-shell-extension-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-classic-session-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-apps-menu-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-auto-move-windows-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-common-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-dash-to-dock-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-desktop-icons-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-disable-screenshield-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-drive-menu-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-horizontal-workspaces-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-launch-new-instance-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-native-window-placement-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-no-hot-corner-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-panel-favorites-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-places-menu-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-screenshot-window-sizer-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-systemMonitor-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-top-icons-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-updates-dialog-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-user-theme-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-window-grouper-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-window-list-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-windowsNavigator-3.32.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-extension-workspace-indicator-3.32.1-10.el8.noarch.rpm \
          "

SRC_URI[gnome-classic-session.sha256sum] = "1330cb41c0f8cbac4c5ae8a63fac8d98a1d90859e3b4024409aad55879a43f9f"
SRC_URI[gnome-shell-extension-apps-menu.sha256sum] = "cb0eede68d3b9a6a6831afe1e6414237b01cb4c84571fdabdcb829b9d8ec1f42"
SRC_URI[gnome-shell-extension-auto-move-windows.sha256sum] = "6eefe9a3ac3cc1ca45240d36217670241a58d5853c686d58774ae3fa6606bba2"
SRC_URI[gnome-shell-extension-common.sha256sum] = "a187c07e86d9a3cfc4665ed1651107eb50e0014d249abb8b344448e2bfd439fb"
SRC_URI[gnome-shell-extension-dash-to-dock.sha256sum] = "b4e04d4ac4fcc9a51778f51338fdf363a345a7e2cbd7f825d78cea095a736044"
SRC_URI[gnome-shell-extension-desktop-icons.sha256sum] = "ea9cdff1b57ed49fd8d928b979f9cf51a201bf9253eea3abf13de7180a09bd4a"
SRC_URI[gnome-shell-extension-disable-screenshield.sha256sum] = "26230e9ac39dacaf544f73c34f5599a150d4ac26ca37bc90e6da3b701c49eec8"
SRC_URI[gnome-shell-extension-drive-menu.sha256sum] = "fc6cb67935de442e3d0216d696320ba6738a504f163fff40e32da4491985dc0f"
SRC_URI[gnome-shell-extension-horizontal-workspaces.sha256sum] = "de0884ec797b5072896eea2902566afaecf5d6d5bf68c38559688139758b6446"
SRC_URI[gnome-shell-extension-launch-new-instance.sha256sum] = "c2046394a4a0b5fcfe918b572c5c03b3912a6f716b3da3d6033372e3bb71fc0f"
SRC_URI[gnome-shell-extension-native-window-placement.sha256sum] = "00d8c9af4a9d8161de4e8e1f06eb198bcfc14083cc146c6a39f139414c36e605"
SRC_URI[gnome-shell-extension-no-hot-corner.sha256sum] = "afcd35a83bccfe641dbc366f74fc5bd18bb092ad8e629ce100e0ce6ab8ae1337"
SRC_URI[gnome-shell-extension-panel-favorites.sha256sum] = "ce205340cb162f8dded45e4ad2a30fe3de0199fd588c9fee68d4bb27ff8db2ab"
SRC_URI[gnome-shell-extension-places-menu.sha256sum] = "f3a255a0dcaf805589ce288d738b90392cf867761c55b3f9d81cbaba1edb5b2c"
SRC_URI[gnome-shell-extension-screenshot-window-sizer.sha256sum] = "f131193949b206df6cadd8dc8b0267e82bfd25456a63bd924fc251fcbfb10d04"
SRC_URI[gnome-shell-extension-systemMonitor.sha256sum] = "1d8b31dffd9845ed2712db1732a8d49e2111f04e631cd4cf51420a4764b8747e"
SRC_URI[gnome-shell-extension-top-icons.sha256sum] = "a0962a0b1359d451d2be339c049db37cf075e6fdaf48ba45ab10151bdd93948d"
SRC_URI[gnome-shell-extension-updates-dialog.sha256sum] = "dd07af2ba1bd5b3fe47ee354e2a4282af8214602ade93d63bb5f9aadcf9bc97b"
SRC_URI[gnome-shell-extension-user-theme.sha256sum] = "8428d4083eeb82eebbfe752d152e36d423bf8bb0939e85d7b1c5bc62f4b84ed6"
SRC_URI[gnome-shell-extension-window-grouper.sha256sum] = "6472be905b3a4a956fb73c236aec635f76c28dcc108470f112b634a12f3cdadf"
SRC_URI[gnome-shell-extension-window-list.sha256sum] = "cbe02e9c9a3f7bd5442f1bc3712857b0dcc1e924f9fc7445ca650e324c72fac4"
SRC_URI[gnome-shell-extension-windowsNavigator.sha256sum] = "6b40988ecbf95c973fa264938baff5fc6cdb63094535f4969174528bd4f64c15"
SRC_URI[gnome-shell-extension-workspace-indicator.sha256sum] = "46fa743f99b65d3b72168f385e8b01cebd1729d5d701c355113c73d247a127e0"
