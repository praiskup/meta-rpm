SUMMARY = "generated recipe based on sgpio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_sgpio = "libc.so.6"
RDEPENDS_sgpio = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sgpio-1.2.0.10-21.el8.x86_64.rpm \
          "

SRC_URI[sgpio.sha256sum] = "3bb480b7c30f3f98e45d6be4fe0751c4527dc78ab48ab15c397f2d35afc48f35"
