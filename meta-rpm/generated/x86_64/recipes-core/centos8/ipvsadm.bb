SUMMARY = "generated recipe based on ipvsadm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native popt"
RPM_SONAME_REQ_ipvsadm = "libc.so.6 libnl-3.so.200 libnl-genl-3.so.200 libpopt.so.0"
RDEPENDS_ipvsadm = "bash glibc libnl3 popt systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ipvsadm-1.31-1.el8.x86_64.rpm \
          "

SRC_URI[ipvsadm.sha256sum] = "c31be378519efe79f81f2400dd93867460a9fe4711b05dfad355101004727711"
