SUMMARY = "generated recipe based on perl-String-CRC32 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-String-CRC32 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-String-CRC32 = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-String-CRC32-1.6-4.el8.x86_64.rpm \
          "

SRC_URI[perl-String-CRC32.sha256sum] = "88ad0aa5bb55144491992189b74c4a6ed38daa80e12aba45c9ebc037c88649bd"
