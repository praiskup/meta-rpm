SUMMARY = "generated recipe based on hyphen-bg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-bg = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-bg-4.3-13.el8.noarch.rpm \
          "

SRC_URI[hyphen-bg.sha256sum] = "36a4dffe51d427f26e43dfda6e3e92b8c41d23ae0e74458bdec9e8edfe595338"
