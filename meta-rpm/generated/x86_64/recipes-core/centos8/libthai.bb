SUMMARY = "generated recipe based on libthai srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdatrie pkgconfig-native"
RPM_SONAME_PROV_libthai = "libthai.so.0"
RPM_SONAME_REQ_libthai = "libc.so.6 libdatrie.so.1"
RDEPENDS_libthai = "glibc libdatrie"
RPM_SONAME_REQ_libthai-devel = "libthai.so.0"
RPROVIDES_libthai-devel = "libthai-dev (= 0.1.27)"
RDEPENDS_libthai-devel = "libdatrie-devel libthai pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libthai-0.1.27-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libthai-devel-0.1.27-2.el8.x86_64.rpm \
          "

SRC_URI[libthai.sha256sum] = "91bbf9cd7d7ae62682a3d24a889512daef57c3c4b41866336c42af6361702fef"
SRC_URI[libthai-devel.sha256sum] = "81ee9d9bb80daaa9510d47a98dc87341fac3d5e162b71c540b67c1d637547986"
