SUMMARY = "generated recipe based on plexus-interactivity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-interactivity = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-components-pom"
RDEPENDS_plexus-interactivity-api = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-component-api plexus-utils"
RDEPENDS_plexus-interactivity-javadoc = "javapackages-filesystem"
RDEPENDS_plexus-interactivity-jline = "java-1.8.0-openjdk-headless javapackages-filesystem jline plexus-interactivity-api"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-interactivity-1.0-0.27.alpha6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-interactivity-api-1.0-0.27.alpha6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-interactivity-javadoc-1.0-0.27.alpha6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-interactivity-jline-1.0-0.27.alpha6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-interactivity.sha256sum] = "c91dfdd35b509f75567bd7512510e5e104a40b60ea6f31c72c2bd4c6812bfbf8"
SRC_URI[plexus-interactivity-api.sha256sum] = "1010f27af3a4cef257eac9338b2750f14169e66f0c7ca3d1d9e02e9ee5fc3e2d"
SRC_URI[plexus-interactivity-javadoc.sha256sum] = "9aae09253bc3ff6e52d0577af286451acfcb425f78ea7e4e77a7f0bdbcbad3fe"
SRC_URI[plexus-interactivity-jline.sha256sum] = "c972c3541454a270d4c4f055ac12c1373441b49960aab30239f1128aba788bf9"
