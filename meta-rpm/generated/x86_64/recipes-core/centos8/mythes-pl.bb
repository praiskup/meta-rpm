SUMMARY = "generated recipe based on mythes-pl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-pl = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-pl-1.5-20.el8.noarch.rpm \
          "

SRC_URI[mythes-pl.sha256sum] = "2cf60e7b23292f2d4d2033ea5f8ce6e6f91823aaa0ba1f191f9bdd872593438c"
