SUMMARY = "generated recipe based on kernel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libbpf libxcrypt ncurses numactl openssl pciutils perl pkgconfig-native platform-python3 slang xz zlib"
RPM_SONAME_REQ_bpftool = "libc.so.6 libdl.so.2 libelf.so.1 libz.so.1"
RDEPENDS_bpftool = "elfutils-libelf glibc zlib"
RDEPENDS_kernel = "kernel-core kernel-modules"
RDEPENDS_kernel-core = "bash coreutils dracut linux-firmware systemd systemd-udev"
RDEPENDS_kernel-debug = "kernel-debug-core kernel-debug-modules"
RDEPENDS_kernel-debug-core = "bash coreutils dracut linux-firmware systemd systemd-udev"
RPROVIDES_kernel-debug-devel = "kernel-debug-dev (= 4.18.0)"
RDEPENDS_kernel-debug-devel = "bash findutils perl-interpreter"
RDEPENDS_kernel-debug-modules = "bash kernel-core"
RDEPENDS_kernel-debug-modules-extra = "bash kernel-core kernel-debug-modules"
RPROVIDES_kernel-devel = "kernel-dev (= 4.18.0)"
RDEPENDS_kernel-devel = "bash findutils perl-interpreter"
RDEPENDS_kernel-modules = "bash kernel-core"
RDEPENDS_kernel-modules-extra = "bash kernel-core kernel-modules"
RPM_SONAME_REQ_kernel-tools = "libc.so.6 libcpupower.so.0 libm.so.6 libncursesw.so.6 libpanelw.so.6 libpci.so.3 libpthread.so.0 librt.so.1 libtinfo.so.6"
RDEPENDS_kernel-tools = "glibc kernel-tools-libs ncurses-libs pciutils-libs platform-python"
RPM_SONAME_PROV_kernel-tools-libs = "libcpupower.so.0"
RPM_SONAME_REQ_kernel-tools-libs = "libc.so.6"
RDEPENDS_kernel-tools-libs = "bash glibc"
RPM_SONAME_REQ_kernel-tools-libs-devel = "libcpupower.so.0"
RPROVIDES_kernel-tools-libs-devel = "kernel-tools-libs-dev (= 4.18.0)"
RDEPENDS_kernel-tools-libs-devel = "kernel-tools kernel-tools-libs"
RPM_SONAME_PROV_perf = "libperf-jvmti.so"
RPM_SONAME_REQ_perf = "libbpf.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libdw.so.1 libelf.so.1 liblzma.so.5 libm.so.6 libnuma.so.1 libperl.so.5.26 libpthread.so.0 libpython3.6m.so.1.0 libresolv.so.2 librt.so.1 libslang.so.2 libutil.so.1 libz.so.1"
RDEPENDS_perf = "bash elfutils-libelf elfutils-libs glibc libbpf libxcrypt numactl-libs openssl-libs perl-Exporter perl-interpreter perl-libs python3-libs slang xz-libs zlib"
RPM_SONAME_REQ_python3-perf = "libc.so.6 libnuma.so.1 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-perf = "glibc numactl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bpftool-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-abi-whitelists-4.18.0-193.28.1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-core-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-cross-headers-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-debug-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-debug-core-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-debug-devel-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-debug-modules-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-debug-modules-extra-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-devel-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-doc-4.18.0-193.28.1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-modules-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-modules-extra-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-tools-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kernel-tools-libs-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perf-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-perf-4.18.0-193.28.1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/kernel-tools-libs-devel-4.18.0-193.28.1.el8_2.x86_64.rpm \
          "

SRC_URI[bpftool.sha256sum] = "bc94ea7ba7d1ab69e64e211cf06dec1a13af1db0e18135f969853d9c36c9ba86"
SRC_URI[kernel.sha256sum] = "8abfa9bc9d3414dc7cc3014bf5682adffdc13ee63648ac688ab6a33c1a3e6132"
SRC_URI[kernel-abi-whitelists.sha256sum] = "f3926b631a76d0fb965e48edde0580d191eca6ac26aa1849978e1ee268b7b2d9"
SRC_URI[kernel-core.sha256sum] = "63edbd3bececa9380c4f55c49b06907efd4ed38e891f5cfe31bc2be246b95f10"
SRC_URI[kernel-cross-headers.sha256sum] = "d45e458816a68117ad9984dd57d774171502c4ee56ea18c5bce2acfe5562f606"
SRC_URI[kernel-debug.sha256sum] = "2c0589a62ef1173dbb7f549fcd1786eae3f54a5a7acb2a9a9801806911fa6be3"
SRC_URI[kernel-debug-core.sha256sum] = "be135c613c4597980cd697901684be085a6dafb72f9a3f1e1f157986b58b25cd"
SRC_URI[kernel-debug-devel.sha256sum] = "488718b3c1d42e3cac3d810452a3927782de8b46db9066e9ed14d1336b36e293"
SRC_URI[kernel-debug-modules.sha256sum] = "2361e4284a178125a86cdd4c45dd3ac1fd56f45d66a4744e6280801dcc39a95f"
SRC_URI[kernel-debug-modules-extra.sha256sum] = "897e89c2a6ef85217262f1a929b7e2ee36d84be13f59711348259dac6332c38c"
SRC_URI[kernel-devel.sha256sum] = "9dd9dcaecdc1e008ec52e3d64f09c3b9a41dc4bdd8d7194e137d668da07a82a0"
SRC_URI[kernel-doc.sha256sum] = "1164814dc2fcd9e3e3c7798e02a2bc3ad176a259f1dc55dcac6a7d571f1cf625"
SRC_URI[kernel-modules.sha256sum] = "c3bae6b20cfaff1f97ebc2e3936cea21b51af4ab5fd38dcb612b00609fc1ead4"
SRC_URI[kernel-modules-extra.sha256sum] = "e9ab3ab2e816c58c25228d2fe0bbb37489be026dcdead1da685bfef941cfeebd"
SRC_URI[kernel-tools.sha256sum] = "426eeeb329f4a17e0c0750e2dff71e8c676060bbe48bd1840cc3a21538ac6d99"
SRC_URI[kernel-tools-libs.sha256sum] = "c9e31bbae846362db4194b1244e300ca0d06fdb0c41a40c64bf6270cccc2b3ee"
SRC_URI[kernel-tools-libs-devel.sha256sum] = "04b48c31560d938dc411eeacd969c1b811f418952308d3e3cff68785ed6b8582"
SRC_URI[perf.sha256sum] = "25524e46d1e37cd09dcf80d8c86f3ae7e700bf657f955a712f5c56b6a7206c6d"
SRC_URI[python3-perf.sha256sum] = "ce85361bb82f229ccde832ca3ccf566bd89130112559cc5b4a076308ca23a183"
