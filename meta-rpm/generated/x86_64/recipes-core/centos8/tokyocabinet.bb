SUMMARY = "generated recipe based on tokyocabinet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 pkgconfig-native zlib"
RPM_SONAME_PROV_tokyocabinet = "libtokyocabinet.so.9"
RPM_SONAME_REQ_tokyocabinet = "libbz2.so.1 libc.so.6 libm.so.6 libpthread.so.0 librt.so.1 libtokyocabinet.so.9 libz.so.1"
RDEPENDS_tokyocabinet = "bzip2-libs glibc zlib"
RPM_SONAME_REQ_tokyocabinet-devel = "libtokyocabinet.so.9"
RPROVIDES_tokyocabinet-devel = "tokyocabinet-dev (= 1.4.48)"
RDEPENDS_tokyocabinet-devel = "pkgconf-pkg-config tokyocabinet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tokyocabinet-1.4.48-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tokyocabinet-devel-1.4.48-10.el8.x86_64.rpm \
          "

SRC_URI[tokyocabinet.sha256sum] = "b3c0f2e8874bb8e4871b34fe277ff25676bf089b3a10c95edeef1ea0e15f2e83"
SRC_URI[tokyocabinet-devel.sha256sum] = "ff07843bbdefcc708f98e28ee9a2d60622e25e4b270b080d86b652c7cbf70058"
