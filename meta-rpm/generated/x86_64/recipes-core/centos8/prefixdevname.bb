SUMMARY = "generated recipe based on prefixdevname srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_prefixdevname = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 librt.so.1 libudev.so.1"
RDEPENDS_prefixdevname = "bash glibc libgcc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/prefixdevname-0.1.0-6.el8.x86_64.rpm \
          "

SRC_URI[prefixdevname.sha256sum] = "776a182ecaab9f86c7e869db2eb2deea1f291caee701cddbd752da8cd3c57458"
