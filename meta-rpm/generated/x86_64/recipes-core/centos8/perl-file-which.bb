SUMMARY = "generated recipe based on perl-File-Which srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Which = "perl-Exporter perl-PathTools perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-File-Which-1.22-2.el8.noarch.rpm \
          "

SRC_URI[perl-File-Which.sha256sum] = "78410be4e1c359a5648d72ebfdd4880346df0520d0f6b6705feb90d3002668d4"
