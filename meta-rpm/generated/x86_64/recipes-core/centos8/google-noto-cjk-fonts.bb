SUMMARY = "generated recipe based on google-noto-cjk-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_google-noto-sans-cjk-jp-fonts = "google-noto-cjk-fonts-common"
RDEPENDS_google-noto-sans-cjk-ttc-fonts = "google-noto-cjk-fonts-common"
RDEPENDS_google-noto-serif-cjk-ttc-fonts = "google-noto-cjk-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-cjk-fonts-common-20190416-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-cjk-ttc-fonts-20190416-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-cjk-ttc-fonts-20190416-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/google-noto-sans-cjk-jp-fonts-20190416-1.el8.noarch.rpm \
          "

SRC_URI[google-noto-cjk-fonts-common.sha256sum] = "49addb49c391cb8a39a73c6ff538c42bddc6c41dbca03f823b6381aacb1ead08"
SRC_URI[google-noto-sans-cjk-jp-fonts.sha256sum] = "312720ecd29a5100fd5325ce78662580b2d8009e99b92833eebdcb9c3fde0b41"
SRC_URI[google-noto-sans-cjk-ttc-fonts.sha256sum] = "e08439c08be62b8cdee428150d9710365f734b2ef3192ccba4a738b49f7629b8"
SRC_URI[google-noto-serif-cjk-ttc-fonts.sha256sum] = "bf6b3ef23b2b79f2c144e1a03394fee7dde9aeb2dbee785aaa462465a212802f"
