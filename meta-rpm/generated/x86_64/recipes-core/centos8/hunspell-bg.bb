SUMMARY = "generated recipe based on hunspell-bg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-bg = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-bg-4.3-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-bg.sha256sum] = "9977e75ab551ffbd83aadf1bd5a012f5ff22eee4bdd8660691f10114947e7911"
