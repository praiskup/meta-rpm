SUMMARY = "generated recipe based on perl-TermReadKey srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-TermReadKey = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-TermReadKey = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-TermReadKey-2.37-7.el8.x86_64.rpm \
          "

SRC_URI[perl-TermReadKey.sha256sum] = "62df66999d0074fb92b4cc78554ab7134782803d070d5629e6bfa293bcfb55a6"
