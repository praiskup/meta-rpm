SUMMARY = "generated recipe based on libvarlink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libvarlink = "libvarlink.so.0"
RPM_SONAME_REQ_libvarlink = "libc.so.6 libgcc_s.so.1"
RDEPENDS_libvarlink = "glibc libgcc"
RPM_SONAME_REQ_libvarlink-devel = "libvarlink.so.0"
RPROVIDES_libvarlink-devel = "libvarlink-dev (= 18)"
RDEPENDS_libvarlink-devel = "libvarlink pkgconf-pkg-config"
RPM_SONAME_REQ_libvarlink-util = "libc.so.6 libgcc_s.so.1"
RDEPENDS_libvarlink-util = "glibc libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libvarlink-18-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libvarlink-util-18-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvarlink-devel-18-3.el8.x86_64.rpm \
          "

SRC_URI[libvarlink.sha256sum] = "71d283f577077f56a95b6efbab8eea2c09b072836d9b2e70719ca1f9769db668"
SRC_URI[libvarlink-devel.sha256sum] = "d9a2f9c25f949c41b93e78cf55874523ed0ee1dfd2908e80e6d1e196bfbfee2e"
SRC_URI[libvarlink-util.sha256sum] = "ab315685e1835d0381c27f66b9494e464d09e929b1891f9266e7c46482d64aeb"
