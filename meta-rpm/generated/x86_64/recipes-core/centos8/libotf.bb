SUMMARY = "generated recipe based on libotf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libice libsm libx11 libxaw libxmu libxt pkgconfig-native"
RPM_SONAME_PROV_libotf = "libotf.so.0"
RPM_SONAME_REQ_libotf = "libICE.so.6 libSM.so.6 libX11.so.6 libXaw.so.7 libXmu.so.6 libXt.so.6 libc.so.6 libfreetype.so.6 libotf.so.0"
RDEPENDS_libotf = "freetype glibc libICE libSM libX11 libXaw libXmu libXt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libotf-0.9.13-11.el8.x86_64.rpm \
          "

SRC_URI[libotf.sha256sum] = "7f66c9eaab5e94c9f017e1c4918998c9453706668469bf9ed81b1b4b328c8bba"
