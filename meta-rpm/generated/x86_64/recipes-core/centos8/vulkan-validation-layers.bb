SUMMARY = "generated recipe based on vulkan-validation-layers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native spirv-tools"
RPM_SONAME_PROV_vulkan-validation-layers = "libVkLayer_khronos_validation.so libVkLayer_utils.so"
RPM_SONAME_REQ_vulkan-validation-layers = "libSPIRV-Tools-opt.so libSPIRV-Tools.so libVkLayer_utils.so libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_vulkan-validation-layers = "glibc libgcc libstdc++ spirv-tools-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vulkan-validation-layers-1.2.135.0-1.el8.x86_64.rpm \
          "

SRC_URI[vulkan-validation-layers.sha256sum] = "c3be6ecf48965080b4fad3b984e070a1592c4f08129c0cf4ea781a547a36973f"
