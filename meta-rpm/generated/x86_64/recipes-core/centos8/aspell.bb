SUMMARY = "generated recipe based on aspell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc ncurses pkgconfig-native"
RPM_SONAME_PROV_aspell = "libaspell.so.15 libpspell.so.15"
RPM_SONAME_REQ_aspell = "libaspell.so.15 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libncursesw.so.6 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_aspell = "bash glibc info libgcc libstdc++ ncurses-libs"
RPM_SONAME_REQ_aspell-devel = "libaspell.so.15 libpspell.so.15"
RPROVIDES_aspell-devel = "aspell-dev (= 0.60.6.1)"
RDEPENDS_aspell-devel = "aspell bash info pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aspell-0.60.6.1-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/aspell-devel-0.60.6.1-21.el8.x86_64.rpm \
          "

SRC_URI[aspell.sha256sum] = "a641f5ef80d4b77b37e240d1898a6ec1d1b3567445db2bd67aa854c6b1bd18f0"
SRC_URI[aspell-devel.sha256sum] = "4cd24a8121bea8961a34b0d9ef967c4543b27578eb5939002ceb54f43febe1fd"
