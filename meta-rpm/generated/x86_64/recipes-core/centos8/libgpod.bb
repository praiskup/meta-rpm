SUMMARY = "generated recipe based on libgpod srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 libimobiledevice libplist libusb1 libxml2 pkgconfig-native sg3-utils sqlite3 zlib"
RPM_SONAME_PROV_libgpod = "libgpod.so.4"
RPM_SONAME_REQ_libgpod = "libc.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpod.so.4 libimobiledevice.so.6 libm.so.6 libplist.so.3 libpthread.so.0 libsgutils2.so.2 libsqlite3.so.0 libusb-1.0.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_libgpod = "gdk-pixbuf2 glib2 glibc libimobiledevice libplist libusbx libxml2 sg3_utils-libs sqlite-libs systemd zlib"
RPM_SONAME_REQ_libgpod-devel = "libgpod.so.4"
RPROVIDES_libgpod-devel = "libgpod-dev (= 0.8.3)"
RDEPENDS_libgpod-devel = "gdk-pixbuf2-devel glib2-devel libgpod libimobiledevice-devel pkgconf-pkg-config"
RDEPENDS_libgpod-doc = "libgpod"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgpod-0.8.3-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgpod-devel-0.8.3-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgpod-doc-0.8.3-23.el8.x86_64.rpm \
          "

SRC_URI[libgpod.sha256sum] = "c9eeae4669274e00161097d4b09c77127ca1ae39cd60d26255c73685c455032b"
SRC_URI[libgpod-devel.sha256sum] = "5b6af503e3e8d52e3b917e72bc965f6af025a0a4d469764159741cd71743f309"
SRC_URI[libgpod-doc.sha256sum] = "8f3b6a635261ae3828efd90e0ad36e9ae597896b458a3b7cd72dc6e4174f7099"
