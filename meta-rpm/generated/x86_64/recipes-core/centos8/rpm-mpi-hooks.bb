SUMMARY = "generated recipe based on rpm-mpi-hooks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rpm-mpi-hooks = "bash environment-modules filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpm-mpi-hooks-5-4.el8.noarch.rpm \
          "

SRC_URI[rpm-mpi-hooks.sha256sum] = "4717be8d9be931dc98ced8b0e86e286edc95fdd3826908439c9d6d76e2ff53c7"
