SUMMARY = "generated recipe based on jboss-jaxrs-2.0-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-jaxrs-2.0-api = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jboss-jaxrs-2.0-api-1.0.0-6.el8.noarch.rpm \
          "

SRC_URI[jboss-jaxrs-2.0-api.sha256sum] = "81dbece199f8ef5bac1e3baaf1cbf588aa349cdd9cda0a67ce86c0d720580a72"
