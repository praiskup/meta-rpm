SUMMARY = "generated recipe based on gspell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo enchant2 gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_PROV_gspell = "libgspell-1.so.2"
RPM_SONAME_REQ_gspell = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant-2.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_gspell = "atk cairo cairo-gobject enchant2 gdk-pixbuf2 glib2 glibc gtk3 iso-codes pango"
RPM_SONAME_REQ_gspell-devel = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant-2.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgspell-1.so.2 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RPROVIDES_gspell-devel = "gspell-dev (= 1.8.1)"
RDEPENDS_gspell-devel = "atk cairo cairo-gobject enchant2 enchant2-devel gdk-pixbuf2 glib2 glib2-devel glibc gspell gtk3 gtk3-devel pango pkgconf-pkg-config"
RDEPENDS_gspell-doc = "gspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gspell-1.8.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gspell-devel-1.8.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gspell-doc-1.8.1-1.el8.noarch.rpm \
          "

SRC_URI[gspell.sha256sum] = "b1546e3fc92459e3e3ba44bcbece932165c2f133d912f48a15e09177fd5457a6"
SRC_URI[gspell-devel.sha256sum] = "f73e8bd6e598f9f20d69ac93e20f33be45d5bafa4699ec0add669adff6f236f0"
SRC_URI[gspell-doc.sha256sum] = "50ead194f720f93e068284445b07ea448a36a107953171af4d2df83eb13fa185"
