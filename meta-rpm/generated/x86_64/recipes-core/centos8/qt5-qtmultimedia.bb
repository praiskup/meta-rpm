SUMMARY = "generated recipe based on qt5-qtmultimedia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib glib-2.0 gstreamer1.0 gstreamer1.0-plugins-bad gstreamer1.0-plugins-base libgcc libglvnd openal-soft pkgconfig-native pulseaudio qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtmultimedia = "libQt5Multimedia.so.5 libQt5MultimediaGstTools.so.5 libQt5MultimediaQuick.so.5 libQt5MultimediaWidgets.so.5"
RPM_SONAME_REQ_qt5-qtmultimedia = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Multimedia.so.5 libQt5MultimediaGstTools.so.5 libQt5MultimediaQuick.so.5 libQt5MultimediaWidgets.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Widgets.so.5 libasound.so.2 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstphotography-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libm.so.6 libopenal.so.1 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtmultimedia = "alsa-lib glib2 glibc gstreamer1 gstreamer1-plugins-bad-free gstreamer1-plugins-base libgcc libglvnd-glx libstdc++ openal-soft pulseaudio-libs pulseaudio-libs-glib2 qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtmultimedia-devel = "libQt5Multimedia.so.5 libQt5MultimediaGstTools.so.5 libQt5MultimediaQuick.so.5 libQt5MultimediaWidgets.so.5"
RPROVIDES_qt5-qtmultimedia-devel = "qt5-qtmultimedia-dev (= 5.12.5)"
RDEPENDS_qt5-qtmultimedia-devel = "cmake-filesystem pkgconf-pkg-config pulseaudio-libs-devel qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtmultimedia"
RPM_SONAME_PROV_qt5-qtmultimedia-examples = "libfftreal.so.1"
RPM_SONAME_REQ_qt5-qtmultimedia-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Multimedia.so.5 libQt5MultimediaWidgets.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libfftreal.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtmultimedia-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtmultimedia"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtmultimedia-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtmultimedia-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtmultimedia-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtmultimedia.sha256sum] = "a6a8d6ff3514de8cf0f6ec5724ad105d7ad97b6c84679288528c7f53627af533"
SRC_URI[qt5-qtmultimedia-devel.sha256sum] = "fca1a9bae7bcd438a05da1aa6c8b92b06b0e43239413e7484da301dfff7d078d"
SRC_URI[qt5-qtmultimedia-examples.sha256sum] = "3fe1d25182ba27ae6186a18f5df8d774e8d5f9f4a3ef3a165ac153a52538d3e8"
