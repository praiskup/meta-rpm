SUMMARY = "generated recipe based on perl-File-ShareDir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-ShareDir = "perl-Carp perl-Class-Inspector perl-Exporter perl-PathTools perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-File-ShareDir-1.104-3.el8.noarch.rpm \
          "

SRC_URI[perl-File-ShareDir.sha256sum] = "3cfbaf649e0e29098610d07a12148a7f699ea64ef83cd524902c3eb8f74733ad"
