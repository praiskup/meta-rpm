SUMMARY = "generated recipe based on libexttextcat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libexttextcat = "libexttextcat-2.0.so.0"
RPM_SONAME_REQ_libexttextcat = "libc.so.6"
RDEPENDS_libexttextcat = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libexttextcat-3.4.5-2.el8.x86_64.rpm \
          "

SRC_URI[libexttextcat.sha256sum] = "81a63fb91914acd71da88badec218ad72c14af505ab3622f34945a1f7cb48104"
