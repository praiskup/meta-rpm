SUMMARY = "generated recipe based on accountsservice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libxcrypt pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_accountsservice = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0"
RDEPENDS_accountsservice = "bash glib2 glibc libgcc polkit polkit-libs shadow-utils systemd"
RPM_SONAME_REQ_accountsservice-devel = "libaccountsservice.so.0"
RPROVIDES_accountsservice-devel = "accountsservice-dev (= 0.6.50)"
RDEPENDS_accountsservice-devel = "accountsservice-libs pkgconf-pkg-config"
RPM_SONAME_PROV_accountsservice-libs = "libaccountsservice.so.0"
RPM_SONAME_REQ_accountsservice-libs = "libc.so.6 libcrypt.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_accountsservice-libs = "accountsservice glib2 glibc libgcc libxcrypt systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/accountsservice-0.6.50-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/accountsservice-libs-0.6.50-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/accountsservice-devel-0.6.50-8.el8.x86_64.rpm \
          "

SRC_URI[accountsservice.sha256sum] = "cf75da116e2235aaa5645745467cb1f1d977797538d33f6dd2d73946ccff672c"
SRC_URI[accountsservice-devel.sha256sum] = "6639e542d82731444d242a9749192d15cdc10da2218a0c57589226db725b4eeb"
SRC_URI[accountsservice-libs.sha256sum] = "804fd921cb32d4c20d6360325ac294b334c26f83e5b151942ac09400a06ef618"
