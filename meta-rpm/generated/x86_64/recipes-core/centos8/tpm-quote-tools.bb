SUMMARY = "generated recipe based on tpm-quote-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native trousers"
RPM_SONAME_REQ_tpm-quote-tools = "libc.so.6 libtspi.so.1"
RDEPENDS_tpm-quote-tools = "glibc trousers-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tpm-quote-tools-1.0.3-4.el8.x86_64.rpm \
          "

SRC_URI[tpm-quote-tools.sha256sum] = "3a2b6a1ed95ccc30fed79806093c19ab725e83a356498448482254b843237dbe"
