SUMMARY = "generated recipe based on poppler-data srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/poppler-data-0.4.9-1.el8.noarch.rpm \
          "

SRC_URI[poppler-data.sha256sum] = "453ae84d0afbce380631ab4400ffa8fdcc56a5d0ba4001675fdb90de5fb65901"
