SUMMARY = "generated recipe based on libblockdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cryptsetup-libs device-mapper-libs glib-2.0 kmod libblkid libbytesize libgcc libmount libuuid libyaml ndctl nspr nss parted pkgconfig-native systemd-libs volume-key"
RPM_SONAME_PROV_libblockdev = "libblockdev.so.2"
RPM_SONAME_REQ_libblockdev = "libbd_utils.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev = "glib2 glibc kmod-libs libblockdev-utils libgcc systemd-libs"
RPM_SONAME_PROV_libblockdev-crypto = "libbd_crypto.so.2"
RPM_SONAME_REQ_libblockdev-crypto = "libbd_utils.so.2 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libudev.so.1 libvolume_key.so.1"
RDEPENDS_libblockdev-crypto = "cryptsetup-libs glib2 glibc kmod-libs libblkid libblockdev-utils nspr nss nss-util systemd-libs volume_key-libs"
RPM_SONAME_PROV_libblockdev-dm = "libbd_dm.so.2"
RPM_SONAME_REQ_libblockdev-dm = "libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-dm = "device-mapper device-mapper-libs glib2 glibc kmod-libs libblockdev-utils systemd-libs"
RPM_SONAME_PROV_libblockdev-fs = "libbd_fs.so.2"
RPM_SONAME_REQ_libblockdev-fs = "libbd_part_err.so.2 libbd_utils.so.2 libblkid.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libmount.so.1 libparted-fs-resize.so.0 libparted.so.2 libudev.so.1"
RDEPENDS_libblockdev-fs = "glib2 glibc kmod-libs libblkid libblockdev-utils libgcc libmount parted systemd-libs"
RPM_SONAME_PROV_libblockdev-kbd = "libbd_kbd.so.2"
RPM_SONAME_REQ_libblockdev-kbd = "libbd_utils.so.2 libbytesize.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-kbd = "glib2 glibc kmod-libs libblockdev-utils libbytesize systemd-libs"
RPM_SONAME_PROV_libblockdev-loop = "libbd_loop.so.2"
RPM_SONAME_REQ_libblockdev-loop = "libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-loop = "glib2 glibc kmod-libs libblockdev-utils systemd-libs"
RPM_SONAME_PROV_libblockdev-lvm = "libbd_lvm.so.2"
RPM_SONAME_REQ_libblockdev-lvm = "libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-lvm = "device-mapper-libs device-mapper-persistent-data glib2 glibc kmod-libs libblockdev-utils lvm2 systemd-libs"
RPM_SONAME_PROV_libblockdev-lvm-dbus = "libbd_lvm-dbus.so.2"
RPM_SONAME_REQ_libblockdev-lvm-dbus = "libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libpthread.so.0 libudev.so.1"
RDEPENDS_libblockdev-lvm-dbus = "device-mapper-libs device-mapper-persistent-data glib2 glibc kmod-libs libblockdev-utils lvm2-dbusd systemd-libs"
RPM_SONAME_PROV_libblockdev-mdraid = "libbd_mdraid.so.2"
RPM_SONAME_REQ_libblockdev-mdraid = "libbd_utils.so.2 libbytesize.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-mdraid = "glib2 glibc kmod-libs libblockdev-utils libbytesize mdadm systemd-libs"
RPM_SONAME_PROV_libblockdev-mpath = "libbd_mpath.so.2"
RPM_SONAME_REQ_libblockdev-mpath = "libbd_utils.so.2 libc.so.6 libdevmapper.so.1.02 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-mpath = "device-mapper-libs device-mapper-multipath glib2 glibc kmod-libs libblockdev-utils systemd-libs"
RPM_SONAME_PROV_libblockdev-nvdimm = "libbd_nvdimm.so.2"
RPM_SONAME_REQ_libblockdev-nvdimm = "libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libndctl.so.6 libudev.so.1 libuuid.so.1"
RDEPENDS_libblockdev-nvdimm = "glib2 glibc kmod-libs libblockdev-utils libuuid ndctl ndctl-libs systemd-libs"
RPM_SONAME_PROV_libblockdev-part = "libbd_part.so.2"
RPM_SONAME_REQ_libblockdev-part = "libbd_part_err.so.2 libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libparted-fs-resize.so.0 libparted.so.2 libudev.so.1"
RDEPENDS_libblockdev-part = "gdisk glib2 glibc kmod-libs libblockdev-utils parted systemd-libs util-linux"
RDEPENDS_libblockdev-plugins-all = "libblockdev libblockdev-crypto libblockdev-dm libblockdev-fs libblockdev-kbd libblockdev-loop libblockdev-lvm libblockdev-mdraid libblockdev-mpath libblockdev-nvdimm libblockdev-part libblockdev-swap libblockdev-vdo"
RPM_SONAME_PROV_libblockdev-swap = "libbd_swap.so.2"
RPM_SONAME_REQ_libblockdev-swap = "libbd_utils.so.2 libblkid.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1"
RDEPENDS_libblockdev-swap = "glib2 glibc kmod-libs libblkid libblockdev-utils systemd-libs util-linux"
RPM_SONAME_PROV_libblockdev-utils = "libbd_part_err.so.2 libbd_utils.so.2"
RPM_SONAME_REQ_libblockdev-utils = "ld-linux-x86-64.so.2 libbd_utils.so.2 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libparted-fs-resize.so.0 libparted.so.2 libudev.so.1"
RDEPENDS_libblockdev-utils = "glib2 glibc kmod-libs parted systemd-libs"
RPM_SONAME_PROV_libblockdev-vdo = "libbd_vdo.so.2"
RPM_SONAME_REQ_libblockdev-vdo = "libbd_utils.so.2 libbytesize.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libkmod.so.2 libm.so.6 libudev.so.1 libyaml-0.so.2"
RDEPENDS_libblockdev-vdo = "glib2 glibc kmod-kvdo kmod-libs libblockdev-utils libbytesize libyaml systemd-libs vdo"
RDEPENDS_python3-blockdev = "libblockdev platform-python python3-gobject-base"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-crypto-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-dm-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-fs-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-kbd-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-loop-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-lvm-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-lvm-dbus-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-mdraid-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-mpath-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-nvdimm-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-part-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-plugins-all-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-swap-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-utils-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libblockdev-vdo-2.19-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-blockdev-2.19-12.el8.x86_64.rpm \
          "

SRC_URI[libblockdev.sha256sum] = "3ae324d404d73f05999abfc355c01df596cfe05f5ffa7e8051f123f8298207d8"
SRC_URI[libblockdev-crypto.sha256sum] = "2090d24e672e8be305dee6915a28b5680ced4595e3c490e7829274b63c68ffb0"
SRC_URI[libblockdev-dm.sha256sum] = "5716244e113e7992fbc461faf03c24ae5ce6e727bf2b45dc6be1c080b12347dc"
SRC_URI[libblockdev-fs.sha256sum] = "16141c071e7d1afb8e8ee74a51b1067d6dc77f3b1b3a0916973cc08f24cb95c7"
SRC_URI[libblockdev-kbd.sha256sum] = "430602292c261aa0d32c448891d8b75f08db51e56924acab83165d4cbe1a310c"
SRC_URI[libblockdev-loop.sha256sum] = "1e6226968ad9843b3057e7cd124b592e1d2e95ccc4bd3949317d41f292c90043"
SRC_URI[libblockdev-lvm.sha256sum] = "4c51c5fa0ff6ac2d069248c1a53dccb9705dd2e0c519a89528a244f9dbe415e5"
SRC_URI[libblockdev-lvm-dbus.sha256sum] = "47090437dc86b4ef9df1d7bdf589e29fb480ac53a448fef699a123f7ac10aa5d"
SRC_URI[libblockdev-mdraid.sha256sum] = "f1699f748bf5c2135cab09d5063a9298c3a3e78d3477e21a8ad550fc2e739cd6"
SRC_URI[libblockdev-mpath.sha256sum] = "3c176fbd4ae6a967d5d69e908968eaa33e0c84512e3ac6d35bc0402a31a144a3"
SRC_URI[libblockdev-nvdimm.sha256sum] = "7b2edb6c6538429455ff3e37fc853907d7141781921b2e2ccbbb80031d3a2378"
SRC_URI[libblockdev-part.sha256sum] = "107d020a80134e8495445b1983fe430e0eafa9953eb799fd5df20b8bf24ea3e1"
SRC_URI[libblockdev-plugins-all.sha256sum] = "f914b17f9d3ae98ef1ed8ba3d09fe97e4a41618ca3fab6533318b0dc29548f95"
SRC_URI[libblockdev-swap.sha256sum] = "28803490180e490ad901c32bfaa2d4a6ab4dc8d26dc414b47c024c31babc8ea4"
SRC_URI[libblockdev-utils.sha256sum] = "26d4d87de2a7c17d0efe367df5029b80be8d376dbdc94c264f0acc5c6fc1ce5b"
SRC_URI[libblockdev-vdo.sha256sum] = "e51fdb0fd7885b3e1488aae1a1f6a42de5abf26d5874888d7315ba44f815f8aa"
SRC_URI[python3-blockdev.sha256sum] = "17825b1a2b31bffe604eea2cdca484f736a18d0002c8af14639445361c273f15"
