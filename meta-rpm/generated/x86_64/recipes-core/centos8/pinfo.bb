SUMMARY = "generated recipe based on pinfo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_pinfo = "libc.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_pinfo = "bash glibc info ncurses-libs xdg-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pinfo-0.6.10-18.el8.x86_64.rpm \
          "

SRC_URI[pinfo.sha256sum] = "40a48f6c6a63d3d4721da65b889c72f0841a7c2f2330ba8ecd63abf80a1ce0da"
