SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-Random srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-Random = "libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0 libssl.so.1.1"
RDEPENDS_perl-Crypt-OpenSSL-Random = "glibc openssl-libs perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Crypt-OpenSSL-Random-0.15-3.el8.x86_64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-Random.sha256sum] = "7d7947ad41729871e6b123d4b39cc2e01991036d92853ffd85d7ed1b7c62016b"
