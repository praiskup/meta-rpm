SUMMARY = "generated recipe based on gcc-toolset-9-make srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-make = "libc.so.6 libdl.so.2"
RDEPENDS_gcc-toolset-9-make = "gcc-toolset-9-runtime glibc"
RPROVIDES_gcc-toolset-9-make-devel = "gcc-toolset-9-make-dev (= 4.2.1)"
RDEPENDS_gcc-toolset-9-make-devel = "gcc-toolset-9-runtime"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-make-4.2.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-make-devel-4.2.1-2.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-make.sha256sum] = "cf63db9ff1610970c997b30ac1266879a913a9eb7d81d39f5402b627b012e1f3"
SRC_URI[gcc-toolset-9-make-devel.sha256sum] = "63fa22c51cb8a144e965c7e5f7cc04499a4d352ed72b577ddb905a7ed9a0c44d"
