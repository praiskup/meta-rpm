SUMMARY = "generated recipe based on linuxconsoletools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sdl"
RPM_SONAME_REQ_linuxconsoletools = "libSDL-1.2.so.0 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_linuxconsoletools = "SDL bash gawk glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/linuxconsoletools-1.6.0-4.el8.x86_64.rpm \
          "

SRC_URI[linuxconsoletools.sha256sum] = "d5c303b207490ab0edb0d4465397759d4b9c7501f9829d26bd295077e7d5d161"
