SUMMARY = "generated recipe based on hunspell-sq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sq = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-sq-1.6.4-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-sq.sha256sum] = "327f119eeda94611e848497630c53a7f98638dff33779c8f767d6220958c943d"
