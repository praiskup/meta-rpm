SUMMARY = "generated recipe based on grubby srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native rpm"
RPM_SONAME_REQ_grubby = "libc.so.6 librpm.so.8"
RDEPENDS_grubby = "bash findutils glibc grub2-tools grub2-tools-minimal rpm-libs util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grubby-8.40-38.el8.x86_64.rpm \
          "

SRC_URI[grubby.sha256sum] = "19d0023dbb23236aa91a5e6ea2af9556074c92311bfd0800d9525ca679b0682f"
