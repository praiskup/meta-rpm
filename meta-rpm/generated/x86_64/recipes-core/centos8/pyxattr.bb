SUMMARY = "generated recipe based on pyxattr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pyxattr = "libattr.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pyxattr = "glibc libattr platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-pyxattr-0.5.3-18.el8.x86_64.rpm \
          "

SRC_URI[python3-pyxattr.sha256sum] = "f0121f8121a2dc66022729b03afe2af8b4c4f882a4cb628c404a25acdea619ba"
