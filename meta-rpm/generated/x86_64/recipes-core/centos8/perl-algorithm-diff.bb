SUMMARY = "generated recipe based on perl-Algorithm-Diff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Algorithm-Diff = "perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Algorithm-Diff-1.1903-9.el8.noarch.rpm \
          "

SRC_URI[perl-Algorithm-Diff.sha256sum] = "c417a8ddc01dd1ca4b58cca6b1224c7c9234a9f879985384496277ae0c711d65"
