SUMMARY = "generated recipe based on lzop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lzo pkgconfig-native"
RPM_SONAME_REQ_lzop = "libc.so.6 liblzo2.so.2"
RDEPENDS_lzop = "glibc lzo"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lzop-1.03-20.el8.x86_64.rpm \
          "

SRC_URI[lzop.sha256sum] = "04eae61018a5be7656be832797016f97cd7b6e19d56f58cb658cd3969dedf2b0"
