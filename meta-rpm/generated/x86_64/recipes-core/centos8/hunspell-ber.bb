SUMMARY = "generated recipe based on hunspell-ber srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ber = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ber-0.20080210-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-ber.sha256sum] = "dc91257ea22bac6373b958494caa96deb4a50ad68aee01c999bc7c96e86660b4"
