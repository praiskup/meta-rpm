SUMMARY = "generated recipe based on lohit-kannada-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-kannada-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-kannada-fonts-2.5.4-3.el8.noarch.rpm \
          "

SRC_URI[lohit-kannada-fonts.sha256sum] = "d544fe3b116d35b18834d697357b08e22ce9bd37db1596dee21f338926ace58a"
