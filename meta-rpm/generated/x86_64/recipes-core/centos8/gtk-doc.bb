SUMMARY = "generated recipe based on gtk-doc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gtk-doc = "bash docbook-style-xsl docbook-utils libxslt pkgconf-pkg-config platform-python python3-six source-highlight"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtk-doc-1.28-1.el8.x86_64.rpm \
          "

SRC_URI[gtk-doc.sha256sum] = "e56c8261e32d85f5a9d7d372e3464f8276e19f7c971ed7a8689fb212d6fe878b"
