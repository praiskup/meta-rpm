SUMMARY = "generated recipe based on lohit-devanagari-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-devanagari-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-devanagari-fonts-2.95.4-3.el8.noarch.rpm \
          "

SRC_URI[lohit-devanagari-fonts.sha256sum] = "308762b9a67166a418ee6a818cf6c76197731dea02d99cfd653c4130517a309f"
