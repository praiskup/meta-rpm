SUMMARY = "generated recipe based on osinfo-db srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_osinfo-db = "hwdata"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/osinfo-db-20200203-1.el8.noarch.rpm \
          "

SRC_URI[osinfo-db.sha256sum] = "088650365660579d7358e9a35d052a1832653cc092176a37a8f4448f11a9436b"
