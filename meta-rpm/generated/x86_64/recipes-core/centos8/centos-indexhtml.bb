SUMMARY = "generated recipe based on centos-indexhtml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-indexhtml-8.0-0.el8.noarch.rpm \
          "

SRC_URI[centos-indexhtml.sha256sum] = "abddeaa0efdbd4f3754719b42feb4d509818e318c1b1e117ef128545b47df6cf"
