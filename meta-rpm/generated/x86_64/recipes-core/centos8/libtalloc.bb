SUMMARY = "generated recipe based on libtalloc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcrypt pkgconfig-native platform-python3"
RPM_SONAME_PROV_libtalloc = "libtalloc.so.2"
RPM_SONAME_REQ_libtalloc = "libc.so.6 libcrypt.so.1 libdl.so.2"
RDEPENDS_libtalloc = "glibc libxcrypt"
RPM_SONAME_REQ_libtalloc-devel = "libtalloc.so.2"
RPROVIDES_libtalloc-devel = "libtalloc-dev (= 2.2.0)"
RDEPENDS_libtalloc-devel = "libtalloc pkgconf-pkg-config"
RPM_SONAME_PROV_python3-talloc = "libpytalloc-util.cpython-36m-x86-64-linux-gnu.so.2"
RPM_SONAME_REQ_python3-talloc = "libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libpytalloc-util.cpython-36m-x86-64-linux-gnu.so.2 libpython3.6m.so.1.0 libtalloc.so.2 libutil.so.1"
RDEPENDS_python3-talloc = "glibc libtalloc libxcrypt platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtalloc-2.2.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtalloc-devel-2.2.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-talloc-2.2.0-7.el8.x86_64.rpm \
          "

SRC_URI[libtalloc.sha256sum] = "972a56411185b5b9e35d8f512f4578f1df611fe99ee0fcc85c32bfa3fca3e440"
SRC_URI[libtalloc-devel.sha256sum] = "9cd5af76270fc1ade3f532462cd5207604811e8318003525bccf30f070195f80"
SRC_URI[python3-talloc.sha256sum] = "af20246c1d10a98fc1227951ac026f1074710e3df94f8d5ac0a2be2b1b0fb347"
