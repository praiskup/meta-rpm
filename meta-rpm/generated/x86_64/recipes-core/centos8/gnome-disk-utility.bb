SUMMARY = "generated recipe based on gnome-disk-utility srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libcanberra libdvdread libnotify libpwquality libsecret pango pkgconfig-native systemd-libs udisks2 xz"
RPM_SONAME_REQ_gnome-disk-utility = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libdvdread.so.4 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 liblzma.so.5 libm.so.6 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libpwquality.so.1 libsecret-1.so.0 libsystemd.so.0 libudisks2.so.0"
RDEPENDS_gnome-disk-utility = "atk cairo gdk-pixbuf2 glib2 glibc gtk3 libcanberra-gtk3 libdvdread libnotify libpwquality libsecret libudisks2 pango systemd-libs udisks2 xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-disk-utility-3.28.3-2.el8.x86_64.rpm \
          "

SRC_URI[gnome-disk-utility.sha256sum] = "c83fb64da6e990921bbe0860e02c8495c8ef2ac5d12189f1fa643b25a6759052"
