SUMMARY = "generated recipe based on hunspell-gl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-gl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-gl-0.20080515-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-gl.sha256sum] = "739fb3b4e57817f71f311a5bdd341c120c33ba09dbd0bb9158eaf4d74594e80f"
