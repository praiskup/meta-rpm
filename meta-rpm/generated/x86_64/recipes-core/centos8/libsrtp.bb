SUMMARY = "generated recipe based on libsrtp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native zlib"
RPM_SONAME_PROV_libsrtp = "libsrtp.so.1"
RPM_SONAME_REQ_libsrtp = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libssl.so.1.1 libz.so.1"
RDEPENDS_libsrtp = "glibc openssl-libs zlib"
RPM_SONAME_REQ_libsrtp-devel = "libsrtp.so.1"
RPROVIDES_libsrtp-devel = "libsrtp-dev (= 1.5.4)"
RDEPENDS_libsrtp-devel = "libsrtp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsrtp-1.5.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsrtp-devel-1.5.4-8.el8.x86_64.rpm \
          "

SRC_URI[libsrtp.sha256sum] = "ed71036d30822e8210578ae81c3ed3e9fbeab24eda60dc4655a7cf4a23c3743b"
SRC_URI[libsrtp-devel.sha256sum] = "633f599f292fa4c60225968bfc425c40498dc674b9b4d35ee2ea3adb372707df"
