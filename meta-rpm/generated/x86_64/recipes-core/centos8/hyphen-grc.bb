SUMMARY = "generated recipe based on hyphen-grc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-grc = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-grc-0.20110913-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-grc.sha256sum] = "bb630d9d246162dda50d53330965ac16c607de71dc743797fbb5106e3ab3783b"
