SUMMARY = "generated recipe based on hyphen-de srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-de = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-de-0.20060120-19.el8.noarch.rpm \
          "

SRC_URI[hyphen-de.sha256sum] = "1a0e0bd6ea9a2619718dfcaf97778940d6a26972c61d7b0593192e8efd97cf1f"
