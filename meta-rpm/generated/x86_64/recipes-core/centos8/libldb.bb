SUMMARY = "generated recipe based on libldb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtalloc libtdb libtevent libxcrypt openldap pkgconfig-native platform-python3 popt"
RPM_SONAME_PROV_ldb-tools = "libldb-cmdline.so"
RPM_SONAME_REQ_ldb-tools = "libc.so.6 libcrypt.so.1 libdl.so.2 libldb-cmdline.so libldb.so.2 libpopt.so.0 libpthread.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_ldb-tools = "glibc libldb libtalloc libtdb libtevent libxcrypt popt"
RPM_SONAME_PROV_libldb = "libldb-key-value.so libldb-tdb-err-map.so libldb-tdb-int.so libldb.so.2"
RPM_SONAME_REQ_libldb = "libc.so.6 libcrypt.so.1 libdl.so.2 liblber-2.4.so.2 libldap-2.4.so.2 libldb-key-value.so libldb-tdb-err-map.so libldb-tdb-int.so libldb.so.2 libpthread.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_libldb = "glibc libtalloc libtdb libtevent libxcrypt openldap"
RPM_SONAME_REQ_libldb-devel = "libldb.so.2"
RPROVIDES_libldb-devel = "libldb-dev (= 2.0.7)"
RDEPENDS_libldb-devel = "libldb libtalloc-devel libtdb-devel libtevent-devel pkgconf-pkg-config"
RPM_SONAME_PROV_python3-ldb = "libpyldb-util.cpython-36m-x86-64-linux-gnu.so.2"
RPM_SONAME_REQ_python3-ldb = "libc.so.6 libcrypt.so.1 libdl.so.2 libldb.so.2 libm.so.6 libpthread.so.0 libpyldb-util.cpython-36m-x86-64-linux-gnu.so.2 libpython3.6m.so.1.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0 libutil.so.1"
RDEPENDS_python3-ldb = "glibc libldb libtalloc libtdb libtevent libxcrypt platform-python python3-libs python3-tdb"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ldb-tools-2.0.7-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libldb-2.0.7-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libldb-devel-2.0.7-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-ldb-2.0.7-3.el8.x86_64.rpm \
          "

SRC_URI[ldb-tools.sha256sum] = "b2e9521e8fa53e3a712c040ef88df32ff2a52d5baf67f7f93adef7b8077164fe"
SRC_URI[libldb.sha256sum] = "4796defcf7c4585f8100019f75862db8fb3ff15af021cd007f5faaffa19264c9"
SRC_URI[libldb-devel.sha256sum] = "8030dfa2c8652f9ed344bb66e463999512ac5c5031a605de7999fd41b3545eeb"
SRC_URI[python3-ldb.sha256sum] = "4a4c237422ebf298c11b45a6e079dfcdff750d49675517d7b77ad4970fab9d9a"
