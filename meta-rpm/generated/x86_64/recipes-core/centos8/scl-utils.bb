SUMMARY = "generated recipe based on scl-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native rpm"
RPM_SONAME_REQ_scl-utils = "libc.so.6 librpm.so.8 librpmio.so.8"
RDEPENDS_scl-utils = "bash environment-modules glibc rpm-libs"
RDEPENDS_scl-utils-build = "bash iso-codes redhat-rpm-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/scl-utils-2.0.2-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/scl-utils-build-2.0.2-12.el8.x86_64.rpm \
          "

SRC_URI[scl-utils.sha256sum] = "57dc5cfe29f4a9cf96f802a2947403ade75c5dd6dd18e64ac32710505a72192f"
SRC_URI[scl-utils-build.sha256sum] = "37c4c180a1c0a314566d1e29068dfc3775c33c127b2968326e2ca755aa4b8b96"
