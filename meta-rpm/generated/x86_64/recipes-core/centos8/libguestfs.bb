SUMMARY = "generated recipe based on libguestfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fuse glib-2.0 jansson libconfig libgcc libpcre libselinux libtirpc libvirt libxcrypt libxml2 ncurses perl pkgconfig-native readline ruby xz"
RPM_SONAME_PROV_libguestfs = "libguestfs.so.0"
RPM_SONAME_REQ_libguestfs = "libc.so.6 libfuse.so.2 libgcc_s.so.1 libguestfs.so.0 libjansson.so.4 libpcre.so.1 libpthread.so.0 libselinux.so.1 libtirpc.so.3 libvirt.so.0 libxml2.so.2"
RDEPENDS_libguestfs = "acl attr augeas-libs bash binutils bzip2 coreutils cpio cryptsetup dhcp-client diffutils dosfstools e2fsprogs file findutils fuse fuse-libs gawk gdisk genisoimage glibc grep gzip hivex iproute jansson kernel kmod less libacl libcap libdb-utils libgcc libselinux libtirpc libvirt-daemon-kvm libvirt-libs libxml2 lsscsi lvm2 lzop mdadm parted pcre policycoreutils procps-ng psmisc qemu-img scrub sed squashfs-tools supermin syslinux syslinux-extlinux systemd systemd-libs systemd-udev tar util-linux xz yajl"
RDEPENDS_libguestfs-bash-completion = "bash-completion libguestfs-tools-c"
RPM_SONAME_REQ_libguestfs-benchmarking = "libc.so.6 libgcc_s.so.1 libguestfs.so.0 libm.so.6 libpcre.so.1 libpthread.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_libguestfs-benchmarking = "glibc libgcc libguestfs libvirt-libs libxml2 pcre perl-Getopt-Long perl-Pod-Usage perl-interpreter perl-libs"
RPM_SONAME_REQ_libguestfs-devel = "libguestfs.so.0"
RPROVIDES_libguestfs-devel = "libguestfs-dev (= 1.38.4)"
RDEPENDS_libguestfs-devel = "bash libguestfs libguestfs-tools-c pkgconf-pkg-config xz"
RDEPENDS_libguestfs-gfs2 = "gfs2-utils libguestfs"
RPM_SONAME_PROV_libguestfs-gobject = "libguestfs-gobject-1.0.so.0"
RPM_SONAME_REQ_libguestfs-gobject = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libguestfs.so.0 libpthread.so.0"
RDEPENDS_libguestfs-gobject = "glib2 glibc libguestfs"
RPM_SONAME_REQ_libguestfs-gobject-devel = "libguestfs-gobject-1.0.so.0"
RPROVIDES_libguestfs-gobject-devel = "libguestfs-gobject-dev (= 1.38.4)"
RDEPENDS_libguestfs-gobject-devel = "glib2-devel libguestfs-devel libguestfs-gobject pkgconf-pkg-config"
RDEPENDS_libguestfs-inspect-icons = "icoutils libguestfs netpbm-progs"
RPM_SONAME_PROV_libguestfs-java = "libguestfs_jni.so.1"
RPM_SONAME_REQ_libguestfs-java = "libc.so.6 libgcc_s.so.1 libguestfs.so.0 libpthread.so.0"
RDEPENDS_libguestfs-java = "glibc java-1.8.0-openjdk-headless javapackages-tools libgcc libguestfs"
RPM_SONAME_REQ_libguestfs-java-devel = "libguestfs_jni.so.1"
RPROVIDES_libguestfs-java-devel = "libguestfs-java-dev (= 1.38.4)"
RDEPENDS_libguestfs-java-devel = "libguestfs libguestfs-java"
RDEPENDS_libguestfs-javadoc = "javapackages-tools libguestfs libguestfs-java"
RDEPENDS_libguestfs-man-pages-ja = "libguestfs"
RDEPENDS_libguestfs-man-pages-uk = "libguestfs"
RDEPENDS_libguestfs-rescue = "iputils libguestfs-tools-c lsof openssh-clients pciutils strace vim-minimal"
RDEPENDS_libguestfs-rsync = "libguestfs rsync"
RDEPENDS_libguestfs-tools = "libguestfs libguestfs-tools-c perl-File-Temp perl-Getopt-Long perl-Pod-Usage perl-Sys-Guestfs perl-Sys-Virt perl-hivex perl-interpreter perl-libintl-perl perl-libs"
RPM_SONAME_REQ_libguestfs-tools-c = "libc.so.6 libconfig.so.9 libcrypt.so.1 libdl.so.2 libfuse.so.2 libgcc_s.so.1 libguestfs.so.0 libjansson.so.4 liblzma.so.5 libm.so.6 libncurses.so.6 libpcre.so.1 libpthread.so.0 libreadline.so.7 libtinfo.so.6 libtirpc.so.3 libvirt.so.0 libxml2.so.2"
RDEPENDS_libguestfs-tools-c = "bash curl fuse-libs glibc gnupg2 hexedit jansson less libconfig libgcc libguestfs libtirpc libvirt-libs libxcrypt libxml2 man-db ncurses-libs pcre readline vim-minimal xz xz-libs"
RDEPENDS_libguestfs-xfs = "libguestfs xfsprogs"
RPM_SONAME_PROV_lua-guestfs = "libluaguestfs.so"
RPM_SONAME_REQ_lua-guestfs = "libc.so.6 libguestfs.so.0 libpthread.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_lua-guestfs = "glibc libguestfs libvirt-libs libxml2 lua"
RPM_SONAME_REQ_ocaml-libguestfs = "libc.so.6 libguestfs.so.0"
RDEPENDS_ocaml-libguestfs = "glibc libguestfs ocaml-runtime"
RPROVIDES_ocaml-libguestfs-devel = "ocaml-libguestfs-dev (= 1.38.4)"
RDEPENDS_ocaml-libguestfs-devel = "ocaml-libguestfs"
RPM_SONAME_REQ_perl-Sys-Guestfs = "libc.so.6 libguestfs.so.0 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-Guestfs = "glibc libguestfs perl-interpreter perl-libs"
RPM_SONAME_PROV_python3-libguestfs = "libguestfsmod.cpython-36m-x86_64-linux-gnu.so"
RPM_SONAME_REQ_python3-libguestfs = "libc.so.6 libguestfs.so.0 libpthread.so.0"
RDEPENDS_python3-libguestfs = "glibc libguestfs platform-python"
RPM_SONAME_REQ_ruby-libguestfs = "libc.so.6 libguestfs.so.0 libruby.so.2.5"
RDEPENDS_ruby-libguestfs = "glibc libguestfs ruby ruby-libs"
RPM_SONAME_REQ_virt-dib = "libc.so.6 libdl.so.2 libgcc_s.so.1 libguestfs.so.0 libm.so.6 libpcre.so.1 libpthread.so.0 libxml2.so.2"
RDEPENDS_virt-dib = "curl glibc kpartx libgcc libguestfs libxml2 pcre qemu-img which"
RPM_SONAME_REQ_virt-v2v = "libc.so.6 libdl.so.2 libgcc_s.so.1 libguestfs.so.0 libm.so.6 libpcre.so.1 libvirt.so.0 libxml2.so.2"
RDEPENDS_virt-v2v = "curl edk2-ovmf gawk glibc gzip libgcc libguestfs libguestfs-tools-c libguestfs-winsupport libvirt-client libvirt-libs libxml2 nbdkit nbdkit-plugin-python3 nbdkit-plugin-vddk pcre platform-python qemu-kvm unzip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-bash-completion-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-benchmarking-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-devel-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-gfs2-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-gobject-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-gobject-devel-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-inspect-icons-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-java-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-java-devel-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-javadoc-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-man-pages-ja-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-man-pages-uk-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-rescue-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-rsync-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-tools-1.38.4-15.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-tools-c-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-xfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lua-guestfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Sys-Guestfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/virt-dib-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/virt-v2v-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-libguestfs-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-libguestfs-devel-1.38.4-15.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[libguestfs.sha256sum] = "178c6b757665fcdb47b2d1e2bcc49cbcd5f502eabd314bf150cf46d3c441dcbe"
SRC_URI[libguestfs-bash-completion.sha256sum] = "5582a8d5f2daf51aae560a6192542b3dace22921baa89a3f7ebb0847d04f23a5"
SRC_URI[libguestfs-benchmarking.sha256sum] = "a99e6d32c3cbbabff110de01a14c711e6a2f638b49965da4c1adc152335cf555"
SRC_URI[libguestfs-devel.sha256sum] = "f201894abc32d9fd177783fbfa9a0491572f4ee34b37cb68e9ecf106309af19f"
SRC_URI[libguestfs-gfs2.sha256sum] = "4cb68fc953a9bf9cfbd4a7c97b0b1ef3761761195b099d741b925e967975242a"
SRC_URI[libguestfs-gobject.sha256sum] = "a093bf099778bfe56658942b83f9d9a6cb8181d8867e6af7408664b0ed06105a"
SRC_URI[libguestfs-gobject-devel.sha256sum] = "e29bd1b7cc21c781a9b1910bf593e51765ee3a6516b63dc9fce25a1ea2ab00b2"
SRC_URI[libguestfs-inspect-icons.sha256sum] = "a0ba3f814b6d8c684ca3437393ec11e313e99251995fd2fddd48e7bd48bc5541"
SRC_URI[libguestfs-java.sha256sum] = "9bccee8cca0731cb029e24149d17b5668e37905a60e9d73314d9f3ecf03e4dfd"
SRC_URI[libguestfs-java-devel.sha256sum] = "df402d859a49908337269124a8b33d60e38af189812a14021f9c14368a463716"
SRC_URI[libguestfs-javadoc.sha256sum] = "199803c27fa9aad06df934d2589a4436ce8ca88fc2aed99b6573401ce9f020a0"
SRC_URI[libguestfs-man-pages-ja.sha256sum] = "2cbef59a05cae1fc6aa338fba86ec1a689218ada63ccdacd4111d1c800be336b"
SRC_URI[libguestfs-man-pages-uk.sha256sum] = "21eb020d66726ebbaa9811e1eb273d945873bfadc11799cfbcaa571c61835c1c"
SRC_URI[libguestfs-rescue.sha256sum] = "a5b2bc6261527cd39653ab34461b6ed84406e3382fbb15dea69e6dbb611b50b8"
SRC_URI[libguestfs-rsync.sha256sum] = "a0b2837fd2ec802a509b1f08cae177cf10555d186bd4cf4373b0b6868ebe54a3"
SRC_URI[libguestfs-tools.sha256sum] = "5b89187a857e342ea6974a35951a018b2e04ed64775d128094bb6b2e7e6f7b1c"
SRC_URI[libguestfs-tools-c.sha256sum] = "ad06c33a954ca103222546f66d0ec39989c2dc4786b4a77aadc4f396b2ee722b"
SRC_URI[libguestfs-xfs.sha256sum] = "687e581d42c4ae55b6256a2482151a64de731cf9bea880222c6cd6ee7a2c72b2"
SRC_URI[lua-guestfs.sha256sum] = "cc2ac04305bbef6b77a4459fd51bca907b79cc7ab3e0397718170254b7b9b56c"
SRC_URI[ocaml-libguestfs.sha256sum] = "92552ce235b7b1a06e5de23c53da36d80a76f3897e52311ff8421ebdb6c84658"
SRC_URI[ocaml-libguestfs-devel.sha256sum] = "a5149c26d30dd9b780f20afc3c0aa002f1b2bc88ddef44a5a36cfbf0b6b017cb"
SRC_URI[perl-Sys-Guestfs.sha256sum] = "cf3cc6e2ae4ef32e9bc64f8d7bbc25fd26fe1b37bbbab6b405ac4c7cf899f066"
SRC_URI[python3-libguestfs.sha256sum] = "9ab300d39591e7ae04dddec861ab13495eb262765de4716f80cd913f0706f231"
SRC_URI[ruby-libguestfs.sha256sum] = "19c3e73710d884b539caeafeb3e29906e3d18093768e70e86eb8c4d4e4cd2edb"
SRC_URI[virt-dib.sha256sum] = "6ef247a51855c6b5c4abd223960fa3f44cd871166ef7e2d6232d75cd785871b9"
SRC_URI[virt-v2v.sha256sum] = "be503a34ae0be5e5ada5f16e8cd695b000d95bd8e210de823cfc9de8f46abba9"
