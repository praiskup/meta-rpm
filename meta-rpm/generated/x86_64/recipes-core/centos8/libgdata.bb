SUMMARY = "generated recipe based on libgdata srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcr glib-2.0 gnome-online-accounts json-glib libgcc liboauth libsoup-2.4 libxml2 p11-kit pkgconfig-native"
RPM_SONAME_PROV_libgdata = "libgdata.so.22"
RPM_SONAME_REQ_libgdata = "libc.so.6 libgcc_s.so.1 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libjson-glib-1.0.so.0 liboauth.so.0 libp11-kit.so.0 libpthread.so.0 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_libgdata = "gcr glib2 glibc gnome-online-accounts json-glib libgcc liboauth libsoup libxml2 p11-kit"
RPM_SONAME_REQ_libgdata-devel = "libgdata.so.22"
RPROVIDES_libgdata-devel = "libgdata-dev (= 0.17.9)"
RDEPENDS_libgdata-devel = "gcr-devel glib2-devel gnome-online-accounts-devel json-glib-devel libgdata liboauth-devel libsoup-devel libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgdata-0.17.9-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgdata-devel-0.17.9-2.el8.x86_64.rpm \
          "

SRC_URI[libgdata.sha256sum] = "a27b74d13b392e5de533da774f2fdc519507f9725a74be85673ef08c201e12c4"
SRC_URI[libgdata-devel.sha256sum] = "875529a296f84f31ec3223fcf36800497028dabb98ca7fd17dfc4a5742fa5720"
