SUMMARY = "generated recipe based on mod_authnz_pam srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pam pkgconfig-native"
RPM_SONAME_REQ_mod_authnz_pam = "libc.so.6 libpam.so.0"
RDEPENDS_mod_authnz_pam = "glibc httpd pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_authnz_pam-1.1.0-7.el8.x86_64.rpm \
          "

SRC_URI[mod_authnz_pam.sha256sum] = "8ca60879e0626bbda1b4c47af16c10da1436405b73e4357469bd2fd8f4564779"
