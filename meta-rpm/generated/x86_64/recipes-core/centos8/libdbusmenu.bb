SUMMARY = "generated recipe based on libdbusmenu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-glib-devel gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_PROV_libdbusmenu = "libdbusmenu-glib.so.4"
RPM_SONAME_REQ_libdbusmenu = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_libdbusmenu = "glib2 glibc"
RPM_SONAME_REQ_libdbusmenu-devel = "libdbusmenu-glib.so.4"
RPROVIDES_libdbusmenu-devel = "libdbusmenu-dev (= 16.04.0)"
RDEPENDS_libdbusmenu-devel = "dbus-glib-devel libdbusmenu pkgconf-pkg-config"
RPM_SONAME_PROV_libdbusmenu-gtk3 = "libdbusmenu-gtk3.so.4"
RPM_SONAME_REQ_libdbusmenu-gtk3 = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbusmenu-glib.so.4 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_libdbusmenu-gtk3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libdbusmenu pango"
RPM_SONAME_REQ_libdbusmenu-gtk3-devel = "libdbusmenu-gtk3.so.4"
RPROVIDES_libdbusmenu-gtk3-devel = "libdbusmenu-gtk3-dev (= 16.04.0)"
RDEPENDS_libdbusmenu-gtk3-devel = "dbus-glib-devel gdk-pixbuf2-devel gtk3-devel libdbusmenu libdbusmenu-devel libdbusmenu-gtk3 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdbusmenu-16.04.0-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdbusmenu-gtk3-16.04.0-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdbusmenu-devel-16.04.0-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdbusmenu-doc-16.04.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdbusmenu-gtk3-devel-16.04.0-12.el8.x86_64.rpm \
          "

SRC_URI[libdbusmenu.sha256sum] = "cf2187b2bfc9d9aeddc4d79e2141877827efe408ab4fe637138b0a95635d7791"
SRC_URI[libdbusmenu-devel.sha256sum] = "58e8677efb7fdf987852675a340a629c37b6ad0d6e3b92997a8149ed0b6e01f5"
SRC_URI[libdbusmenu-doc.sha256sum] = "22ff2d0bdcef0ac950d6bdb843a3316d509d3e242e623e8faae30d0a8327a02c"
SRC_URI[libdbusmenu-gtk3.sha256sum] = "6736ff7ddc5316d9b058351d443452ae4f23ef4140fd1b82c580046f8cfd0b4b"
SRC_URI[libdbusmenu-gtk3-devel.sha256sum] = "4cde3b4aeb3e98f9f81578988ec26f99cacee8a65c7d7a7cd0778eec70c4afe3"
