SUMMARY = "generated recipe based on libselinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre2 libsepol pkgconfig-native"
RPM_SONAME_PROV_libselinux = "libselinux.so.1"
RPM_SONAME_REQ_libselinux = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libpcre2-8.so.0"
RDEPENDS_libselinux = "glibc libsepol pcre2"
RPM_SONAME_REQ_libselinux-devel = "libselinux.so.1"
RPROVIDES_libselinux-devel = "libselinux-dev (= 2.9)"
RDEPENDS_libselinux-devel = "libselinux libsepol-devel pcre2-devel pkgconf-pkg-config"
RPM_SONAME_REQ_libselinux-utils = "libc.so.6 libpcre2-8.so.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_libselinux-utils = "glibc libselinux libsepol pcre2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libselinux-2.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libselinux-devel-2.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libselinux-utils-2.9-3.el8.x86_64.rpm \
          "

SRC_URI[libselinux.sha256sum] = "2f9bd6c5e4ab0935f59c000dfa75ecba8aebec243478bdca32432f38a1454eb8"
SRC_URI[libselinux-devel.sha256sum] = "3132b3f515e93d652f8208a72fc197d890d69e42b50f7034c3d4be247f7b6296"
SRC_URI[libselinux-utils.sha256sum] = "67d4a726f8e163dfaf03b219391a462feae351b40f07e8a74d9b08b3860e0d9c"
