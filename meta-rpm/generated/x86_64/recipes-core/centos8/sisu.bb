SUMMARY = "generated recipe based on sisu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sisu-inject = "cdi-api java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_sisu-javadoc = "javapackages-filesystem"
RDEPENDS_sisu-plexus = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-classworlds plexus-containers-component-annotations plexus-utils sisu-inject"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sisu-inject-0.3.3-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sisu-javadoc-0.3.3-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sisu-plexus-0.3.3-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[sisu-inject.sha256sum] = "760a6669a557c34576c1e369d467bd8eff07a8aa30c9debd60b35d79255047ee"
SRC_URI[sisu-javadoc.sha256sum] = "cabb6094ea13feb9d54e023e4ca45d2ae2e1f4ec1ca235f21b8371583c3ea2d1"
SRC_URI[sisu-plexus.sha256sum] = "ffa6fff129224a2ad8a118fe41f20581ceb6977fea7ca4c1e8854607d93148d0"
