SUMMARY = "generated recipe based on cpio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cpio = "libc.so.6"
RDEPENDS_cpio = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cpio-2.12-8.el8.x86_64.rpm \
          "

SRC_URI[cpio.sha256sum] = "c5bf206709fc7895b1ba51073d5984d3df562b94b80e0144a809a930284f0288"
