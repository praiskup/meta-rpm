SUMMARY = "generated recipe based on postfix srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib db icu libnsl2 libpcre libpq mariadb-connector-c openldap openssl pkgconfig-native sqlite3 tinycdb"
RPM_SONAME_REQ_postfix = "libc.so.6 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libnsl.so.2 libresolv.so.2 libsasl2.so.3 libssl.so.1.1"
RDEPENDS_postfix = "bash chkconfig cyrus-sasl-lib diffutils glibc hostname libdb libicu libnsl2 openssl openssl-libs shadow-utils systemd"
RPM_SONAME_REQ_postfix-cdb = "libc.so.6 libcdb.so.1"
RDEPENDS_postfix-cdb = "glibc postfix tinycdb"
RPM_SONAME_REQ_postfix-ldap = "libc.so.6 liblber-2.4.so.2 libldap-2.4.so.2"
RDEPENDS_postfix-ldap = "glibc openldap postfix"
RPM_SONAME_REQ_postfix-mysql = "libc.so.6 libm.so.6 libmariadb.so.3"
RDEPENDS_postfix-mysql = "glibc mariadb-connector-c postfix"
RPM_SONAME_REQ_postfix-pcre = "libc.so.6 libpcre.so.1"
RDEPENDS_postfix-pcre = "glibc pcre postfix"
RDEPENDS_postfix-perl-scripts = "perl-Date-Calc perl-Getopt-Long perl-IO perl-interpreter perl-libs postfix"
RPM_SONAME_REQ_postfix-pgsql = "libc.so.6 libpq.so.5"
RDEPENDS_postfix-pgsql = "glibc libpq postfix"
RPM_SONAME_REQ_postfix-sqlite = "libc.so.6 libsqlite3.so.0"
RDEPENDS_postfix-sqlite = "glibc postfix sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-cdb-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-ldap-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-mysql-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-pcre-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-perl-scripts-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-pgsql-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postfix-sqlite-3.3.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/postfix-3.3.1-12.el8.x86_64.rpm \
          "

SRC_URI[postfix.sha256sum] = "ad58bf49f9a69a93de7e09a5c49f85fff8557c8816735e895a6b3b093138ae4d"
SRC_URI[postfix-cdb.sha256sum] = "bf17d9d46c82bcb4e0ca6cdc8a2b7c6eff278a8573bd339265a12dc6fa877237"
SRC_URI[postfix-ldap.sha256sum] = "34c5b2750abd9d61b01e08db8d3513c509b6364b067ace946eb19085720a66ce"
SRC_URI[postfix-mysql.sha256sum] = "419466f00e81ce6179f69436e355c24ee9036e4c9dae543d6940fca17efaca7b"
SRC_URI[postfix-pcre.sha256sum] = "21ac278624e780b4ab788df3e80986ceaad991dc86981e0ffbe83385501b9d01"
SRC_URI[postfix-perl-scripts.sha256sum] = "318cc5f5aea5d0c86542191f22aa2c7b7864189214988dd9db98c89264e94102"
SRC_URI[postfix-pgsql.sha256sum] = "e0604fc7152bf8227a5a01f2393d964dfb6cb03cec92fd2ef56befa0d81f2aa1"
SRC_URI[postfix-sqlite.sha256sum] = "3ff288a25d24d859198a57f79e795ae254780fbb236f43f69f46224c069452a1"
