SUMMARY = "generated recipe based on lldpad srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libconfig libnl pkgconfig-native"
RPM_SONAME_PROV_lldpad = "liblldp_clif.so.1"
RPM_SONAME_REQ_lldpad = "libc.so.6 libconfig.so.9 libdl.so.2 liblldp_clif.so.1 libnl-3.so.200 librt.so.1"
RDEPENDS_lldpad = "bash glibc libconfig libnl3 readline systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lldpad-1.0.1-13.git036e314.el8.x86_64.rpm \
          "

SRC_URI[lldpad.sha256sum] = "a6bf29d940cb51e8bd989edda291ae48024ea183070637f2b6c43de2effb4153"
