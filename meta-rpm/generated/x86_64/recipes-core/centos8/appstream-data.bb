SUMMARY = "generated recipe based on appstream-data srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/appstream-data-8-20191129.el8.noarch.rpm \
          "

SRC_URI[appstream-data.sha256sum] = "ef39e4d234034d4416a8fc9bc9fd18e85ea859efe340e5429e2076f4402ceb1f"
