SUMMARY = "generated recipe based on dtc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dtc = "libc.so.6"
RDEPENDS_dtc = "bash glibc"
RPM_SONAME_PROV_libfdt = "libfdt.so.1"
RPM_SONAME_REQ_libfdt = "libc.so.6"
RDEPENDS_libfdt = "glibc"
RPM_SONAME_REQ_libfdt-devel = "libfdt.so.1"
RPROVIDES_libfdt-devel = "libfdt-dev (= 1.4.6)"
RDEPENDS_libfdt-devel = "libfdt"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dtc-1.4.6-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libfdt-1.4.6-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libfdt-devel-1.4.6-1.el8.x86_64.rpm \
          "

SRC_URI[dtc.sha256sum] = "a5dfa719c520b225cab39d18c39da2f20df1d4880d4c28dd81a5553b11aa030a"
SRC_URI[libfdt.sha256sum] = "d1180de1cac8cab957737462c85fd0be620f7b62c66771907e0bcc0574cbe84f"
SRC_URI[libfdt-devel.sha256sum] = "6cdad3ffd1c83917f3c7fb99ab87fd00eb178cbacfaa8f484391c29ffe7ca4ac"
