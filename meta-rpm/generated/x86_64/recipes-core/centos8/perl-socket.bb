SUMMARY = "generated recipe based on perl-Socket srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Socket = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Socket = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Socket-2.027-3.el8.x86_64.rpm \
          "

SRC_URI[perl-Socket.sha256sum] = "de138a9614191af63b9603cf0912d4ffd9bd9e5b122c2d0a78ae0eac009a602f"
