SUMMARY = "generated recipe based on xmlto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xmlto = "libc.so.6"
RDEPENDS_xmlto = "bash docbook-dtds docbook-style-xsl flex glibc libxslt util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xmlto-0.0.28-7.el8.x86_64.rpm \
          "

SRC_URI[xmlto.sha256sum] = "12b351e096aabbcf78b5658afbc36b48a40126ac314f2d0284139ed4f7e955f7"
