SUMMARY = "generated recipe based on qt5-qtbase srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cups-libs dbus-libs fontconfig freetype glib-2.0 harfbuzz icu libdrm libgcc libglvnd libice libinput libjpeg-turbo libpcre2 libpng libpq libsm libx11 libxcb libxext libxkbcommon libxrender mariadb-connector-c mesa openssl pkgconfig-native sqlite3 systemd-libs unixodbc xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm zlib"
RPM_SONAME_PROV_qt5-qtbase = "libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Network.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Xml.so.5"
RPM_SONAME_REQ_qt5-qtbase = "ld-linux-x86-64.so.2 libQt5Core.so.5 libQt5DBus.so.5 libQt5Network.so.5 libQt5Sql.so.5 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpcre2-16.so.0 libpthread.so.0 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libsystemd.so.0 libz.so.1"
RDEPENDS_qt5-qtbase = "bash chkconfig dbus-libs glib2 glibc libgcc libicu libstdc++ openssl-libs pcre2-utf16 qt5-qtbase-common sqlite-libs systemd-libs zlib"
RDEPENDS_qt5-qtbase-common = "qt5-qtbase"
RPM_SONAME_REQ_qt5-qtbase-devel = "ld-linux-x86-64.so.2 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5EglFSDeviceIntegration.so.5 libQt5EglFsKmsSupport.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libQt5XcbQpa.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RPROVIDES_qt5-qtbase-devel = "qt5-qtbase-dev (= 5.12.5)"
RDEPENDS_qt5-qtbase-devel = "bash cmake-filesystem glibc libgcc libglvnd-devel libstdc++ perl-File-Path perl-Getopt-Long perl-IO perl-PathTools perl-interpreter perl-libs pkgconf-pkg-config platform-python qt5-qtbase qt5-qtbase-gui qt5-rpm-macros zlib"
RPM_SONAME_PROV_qt5-qtbase-examples = "libechoplugin.so libpnp_extrafilters.so libsimplestyleplugin.so"
RPM_SONAME_REQ_qt5-qtbase-examples = "libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtbase-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_PROV_qt5-qtbase-gui = "libQt5EglFSDeviceIntegration.so.5 libQt5EglFsKmsSupport.so.5 libQt5Gui.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5XcbQpa.so.5"
RPM_SONAME_REQ_qt5-qtbase-gui = "libEGL.so.1 libGL.so.1 libICE.so.6 libQt5Core.so.5 libQt5DBus.so.5 libQt5EglFSDeviceIntegration.so.5 libQt5EglFsKmsSupport.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5XcbQpa.so.5 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcups.so.2 libdl.so.2 libdrm.so.2 libfontconfig.so.1 libfreetype.so.6 libgbm.so.1 libgcc_s.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libharfbuzz.so.0 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libstdc++.so.6 libudev.so.1 libxcb-glx.so.0 libxcb-icccm.so.4 libxcb-image.so.0 libxcb-keysyms.so.1 libxcb-randr.so.0 libxcb-render-util.so.0 libxcb-render.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-sync.so.1 libxcb-xfixes.so.0 libxcb-xinerama.so.0 libxcb-xinput.so.0 libxcb.so.1 libxkbcommon.so.0 libz.so.1"
RDEPENDS_qt5-qtbase-gui = "bash cups-libs fontconfig freetype glib2 glibc glx-utils harfbuzz libICE libSM libX11 libX11-xcb libXext libXrender libdrm libgcc libglvnd-egl libglvnd-glx libjpeg-turbo libpng libstdc++ libxcb libxkbcommon mesa-libgbm qt5-qtbase systemd-libs xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm zlib"
RPM_SONAME_REQ_qt5-qtbase-mysql = "libQt5Core.so.5 libQt5Sql.so.5 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qtbase-mysql = "glibc libgcc libstdc++ mariadb-connector-c openssl-libs qt5-qtbase zlib"
RPM_SONAME_REQ_qt5-qtbase-odbc = "libQt5Core.so.5 libQt5Sql.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libodbc.so.2 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtbase-odbc = "glibc libgcc libstdc++ qt5-qtbase unixODBC"
RPM_SONAME_REQ_qt5-qtbase-postgresql = "libQt5Core.so.5 libQt5Sql.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpq.so.5 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtbase-postgresql = "glibc libgcc libpq libstdc++ qt5-qtbase"
RPROVIDES_qt5-qtbase-private-devel = "qt5-qtbase-private-dev (= 5.12.5)"
RDEPENDS_qt5-qtbase-private-devel = "cups-devel qt5-qtbase-devel"
RDEPENDS_qt5-qtbase-static = "cmake-filesystem fontconfig-devel glib2-devel libinput-devel libxkbcommon-devel pkgconf-pkg-config qt5-qtbase-devel zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-common-5.12.5-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-devel-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-examples-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-gui-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-mysql-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-odbc-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-postgresql-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtbase-private-devel-5.12.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qt5-qtbase-static-5.12.5-4.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtbase.sha256sum] = "a9925392196eaf59dbbf7d6c9caa1805763031b786087ee9cdb70154bdddf13e"
SRC_URI[qt5-qtbase-common.sha256sum] = "50cfe2978887454e23fdf0fdb296c13eb9792d5bc48ebe664a5b74e7c9f32c3e"
SRC_URI[qt5-qtbase-devel.sha256sum] = "3a5ff3b719e38d0b37d37c1bf9ea96aa8e8e50be8ac6fa5573b3a176fa767589"
SRC_URI[qt5-qtbase-examples.sha256sum] = "f12e2f4a6fff89b8cda5d8f4cd42aab2830c88acded7df8105674878f280c52f"
SRC_URI[qt5-qtbase-gui.sha256sum] = "768aee057e04240dc60f1fdabcfefb82d810755c6eab4e229121d6929f3c78c0"
SRC_URI[qt5-qtbase-mysql.sha256sum] = "42e22228c04d33ef7e25100f384a8ffa4b8c8ea184c06fc5a66049376ee964d1"
SRC_URI[qt5-qtbase-odbc.sha256sum] = "e06c8dbeff7f2c1706c0b94fd5100670dcbedf10374637ea8175ac20d64702d0"
SRC_URI[qt5-qtbase-postgresql.sha256sum] = "3ba6f2c4311691cc495bee978350c8e5a9989f300ce0b7ec1709bacbe36eda4a"
SRC_URI[qt5-qtbase-private-devel.sha256sum] = "12aa536e589dacb5afde49bbcddfd922702346bf9821dab8cbf2611c86e42ffa"
SRC_URI[qt5-qtbase-static.sha256sum] = "156ff4634df06ed710c170592e47b3c1902798568561ed8047f6de4f35723071"
