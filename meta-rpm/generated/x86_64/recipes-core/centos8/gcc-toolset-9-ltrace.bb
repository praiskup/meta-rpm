SUMMARY = "generated recipe based on gcc-toolset-9-ltrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libselinux pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-ltrace = "libc.so.6 libdw.so.1 libelf.so.1 libselinux.so.1 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-ltrace = "elfutils-libelf elfutils-libs gcc-toolset-9-runtime glibc libselinux libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-ltrace-0.7.91-1.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-ltrace.sha256sum] = "3e465521a381a28b791d6274e186a2ec8acca821fab920d7645bf34fe58754dc"
