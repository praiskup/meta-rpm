SUMMARY = "generated recipe based on lohit-gurmukhi-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-gurmukhi-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-gurmukhi-fonts-2.91.2-3.el8.noarch.rpm \
          "

SRC_URI[lohit-gurmukhi-fonts.sha256sum] = "821a37f8f4b7a5a778736f96a941aef3a34db3fde4dc0fc8b7c9b21d4d0b0245"
