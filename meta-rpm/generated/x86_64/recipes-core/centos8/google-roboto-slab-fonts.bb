SUMMARY = "generated recipe based on google-roboto-slab-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/google-roboto-slab-fonts-1.100263-0.7.20150923git.el8.noarch.rpm \
          "

SRC_URI[google-roboto-slab-fonts.sha256sum] = "fb83524a608bbbd6d36bc7656980f8cddbb12888add12db473bf74ec0a68bea9"
