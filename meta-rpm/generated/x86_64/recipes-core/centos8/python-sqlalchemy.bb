SUMMARY = "generated recipe based on python-sqlalchemy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-sqlalchemy = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-sqlalchemy = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python-sqlalchemy-doc-1.3.2-1.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-sqlalchemy-1.3.2-1.module_el8.1.0+245+c39af44f.x86_64.rpm \
          "

SRC_URI[python-sqlalchemy-doc.sha256sum] = "74574cbe3ed07b4d456c681715744e796b0703304ed4b9e8f59ea5843a10bc65"
SRC_URI[python3-sqlalchemy.sha256sum] = "0c19b1e1611cbd9a49fc34dc10064eb4bf25b6fc44c0c6d29947fe6c2f7139a2"
