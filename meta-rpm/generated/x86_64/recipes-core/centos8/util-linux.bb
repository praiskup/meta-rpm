SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit libblkid libcap-ng libmount libselinux libuser libutempter libuuid libxcrypt ncurses pam pkgconfig-native systemd-libs zlib"
RPM_SONAME_PROV_libfdisk = "libfdisk.so.1"
RPM_SONAME_REQ_libfdisk = "ld-linux-x86-64.so.2 libblkid.so.1 libc.so.6 libuuid.so.1"
RDEPENDS_libfdisk = "glibc libblkid libuuid"
RPM_SONAME_REQ_libfdisk-devel = "libfdisk.so.1"
RPROVIDES_libfdisk-devel = "libfdisk-dev (= 2.32.1)"
RDEPENDS_libfdisk-devel = "libblkid-devel libfdisk libuuid-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libsmartcols = "libsmartcols.so.1"
RPM_SONAME_REQ_libsmartcols = "ld-linux-x86-64.so.2 libc.so.6"
RDEPENDS_libsmartcols = "glibc"
RPM_SONAME_REQ_libsmartcols-devel = "libsmartcols.so.1"
RPROVIDES_libsmartcols-devel = "libsmartcols-dev (= 2.32.1)"
RDEPENDS_libsmartcols-devel = "libsmartcols pkgconf-pkg-config"
RPM_SONAME_REQ_util-linux = "ld-linux-x86-64.so.2 libaudit.so.1 libblkid.so.1 libc.so.6 libcap-ng.so.0 libcrypt.so.1 libfdisk.so.1 libm.so.6 libmount.so.1 libncursesw.so.6 libpam.so.0 libpam_misc.so.0 librt.so.1 libselinux.so.1 libsmartcols.so.1 libsystemd.so.0 libtinfo.so.6 libudev.so.1 libutempter.so.0 libutil.so.1 libuuid.so.1 libz.so.1"
RDEPENDS_util-linux = "audit-libs bash coreutils glibc libblkid libcap-ng libfdisk libmount libselinux libsmartcols libutempter libuuid libxcrypt ncurses-libs pam systemd-libs zlib"
RPM_SONAME_REQ_util-linux-user = "libc.so.6 libpam.so.0 libpam_misc.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_util-linux-user = "glibc libselinux libuser pam util-linux"
RPM_SONAME_REQ_uuidd = "libc.so.6 librt.so.1 libsystemd.so.0 libuuid.so.1"
RDEPENDS_uuidd = "bash glibc libuuid shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libfdisk-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libfdisk-devel-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsmartcols-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsmartcols-devel-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/util-linux-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/util-linux-user-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/uuidd-2.32.1-22.el8.x86_64.rpm \
          "

SRC_URI[libfdisk.sha256sum] = "14663f470558bff6f3e1482a0e29228f75f31e91c89b82a45fbaf3b190160de9"
SRC_URI[libfdisk-devel.sha256sum] = "8cfb2d441f0235457937b2cc30f4fdd1bb5306713005335c4e12d17221164dcc"
SRC_URI[libsmartcols.sha256sum] = "1073d27b41369598d708523ec2d8df99a29c5661f11448159734eaef7d189c6f"
SRC_URI[libsmartcols-devel.sha256sum] = "28a894826f8b3fd925f8edef9cb6b4af35ded8ed1eef047df1d193fde1052ada"
SRC_URI[util-linux.sha256sum] = "36c1aab315afe3bc95c28c130e9f978df0146f8af0221f272eadb08b5d952a85"
SRC_URI[util-linux-user.sha256sum] = "77da278d86120dbec4522028c4754ae1fcad69f57598a94303ae254a8c292f67"
SRC_URI[uuidd.sha256sum] = "24f87c11e5b2b1fcf514f7e75d2bef95889e2e0d16af8b6d0b1c90c98267274a"
