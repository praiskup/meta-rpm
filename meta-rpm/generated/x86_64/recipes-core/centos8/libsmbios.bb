SUMMARY = "generated recipe based on libsmbios srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsmbios = "libsmbios_c.so.2"
RPM_SONAME_REQ_libsmbios = "libc.so.6"
RDEPENDS_libsmbios = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsmbios-2.4.1-2.el8.x86_64.rpm \
          "

SRC_URI[libsmbios.sha256sum] = "5092704c041bb246eaafd478bedface3f99ce8fa233ada5cf96807f67e980e42"
