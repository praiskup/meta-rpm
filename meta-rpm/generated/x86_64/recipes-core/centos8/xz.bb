SUMMARY = "generated recipe based on xz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xz = "libc.so.6 liblzma.so.5 libpthread.so.0"
RDEPENDS_xz = "bash glibc grep xz-libs"
RPM_SONAME_REQ_xz-devel = "liblzma.so.5"
RPROVIDES_xz-devel = "xz-dev (= 5.2.4)"
RDEPENDS_xz-devel = "pkgconf-pkg-config xz-libs"
RPM_SONAME_PROV_xz-libs = "liblzma.so.5"
RPM_SONAME_REQ_xz-libs = "libc.so.6 libpthread.so.0"
RDEPENDS_xz-libs = "glibc"
RPM_SONAME_REQ_xz-lzma-compat = "libc.so.6 liblzma.so.5 libpthread.so.0"
RDEPENDS_xz-lzma-compat = "glibc xz xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xz-5.2.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xz-devel-5.2.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xz-libs-5.2.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xz-lzma-compat-5.2.4-3.el8.x86_64.rpm \
          "

SRC_URI[xz.sha256sum] = "02f10beaf61212427e0cd57140d050948eea0b533cf432d7bc4c10266c8b33db"
SRC_URI[xz-devel.sha256sum] = "5e8619fa53179d3931836171aa87c4cfc3759064d5c7fad4cd412f62a1d825e6"
SRC_URI[xz-libs.sha256sum] = "61553db2c5d1da168da53ec285de14d00ce91bb02dd902a1688725cf37a7b1a2"
SRC_URI[xz-lzma-compat.sha256sum] = "3a02c50be9027ef409db6ce3799b6e86c3bbdee80d829d7154573869b03f6b49"
