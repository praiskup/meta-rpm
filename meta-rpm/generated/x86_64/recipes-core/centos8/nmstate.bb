SUMMARY = "generated recipe based on nmstate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_nmstate = "platform-python python3-libnmstate python3-setuptools"
RDEPENDS_python3-libnmstate = "NetworkManager-libnm platform-python platform-python-setuptools python3-dbus python3-gobject-base python3-jsonschema python3-pyyaml"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nmstate-0.2.10-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-libnmstate-0.2.10-1.el8.noarch.rpm \
          "

SRC_URI[nmstate.sha256sum] = "761d0efb11452889a82c1a7aff8c2b49830eb0be9788baeffa9ef66a523b3d5d"
SRC_URI[python3-libnmstate.sha256sum] = "3e9438cf1364eac0a9d39bd2a8a279981b6296f1774abb229c1172bf09b225b3"
