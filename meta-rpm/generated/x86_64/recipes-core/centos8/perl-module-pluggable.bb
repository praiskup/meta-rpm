SUMMARY = "generated recipe based on perl-Module-Pluggable srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Pluggable = "perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Module-Pluggable-5.2-7.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Pluggable.sha256sum] = "1c0043eb972fd088c7c0ca809e122966cfc2bde5c505ef306c7984ff55709b42"
