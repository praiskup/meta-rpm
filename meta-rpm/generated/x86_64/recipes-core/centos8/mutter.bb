SUMMARY = "generated recipe based on mutter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig gdk-pixbuf glib-2.0 gnome-desktop3 gsettings-desktop-schemas gtk+3 json-glib libcanberra libdrm libgcc libglvnd libgudev libice libinput libsm libwacom libx11 libxcb libxcomposite libxcursor libxdamage libxext libxfixes libxi libxinerama libxkbcommon libxkbfile libxrandr libxtst mesa pango pipewire pkgconfig-native startup-notification systemd-libs wayland"
RPM_SONAME_PROV_mutter = "libdefault.so libmutter-4.so.0 libmutter-clutter-4.so.0 libmutter-cogl-4.so.0 libmutter-cogl-gles2-4.so.0 libmutter-cogl-pango-4.so.0 libmutter-cogl-path-4.so.0"
RPM_SONAME_REQ_mutter = "libEGL.so.1 libGL.so.1 libGLESv2.so.2 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXtst.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcanberra.so.0 libdl.so.2 libdrm.so.2 libfontconfig.so.1 libgbm.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libgudev-1.0.so.0 libinput.so.10 libjson-glib-1.0.so.0 libm.so.6 libmutter-4.so.0 libmutter-clutter-4.so.0 libmutter-cogl-4.so.0 libmutter-cogl-gles2-4.so.0 libmutter-cogl-pango-4.so.0 libmutter-cogl-path-4.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpipewire-0.2.so.1 libpthread.so.0 libstartup-notification-1.so.0 libsystemd.so.0 libudev.so.1 libwacom.so.2 libwayland-server.so.0 libxcb-randr.so.0 libxcb-res.so.0 libxcb.so.1 libxkbcommon-x11.so.0 libxkbcommon.so.0 libxkbfile.so.1"
RDEPENDS_mutter = "atk cairo cairo-gobject dbus fontconfig gdk-pixbuf2 glib2 glibc gnome-control-center-filesystem gnome-desktop3 gsettings-desktop-schemas gtk3 json-glib libICE libSM libX11 libX11-xcb libXcomposite libXcursor libXdamage libXext libXfixes libXi libXinerama libXrandr libXtst libcanberra libdrm libgcc libglvnd-egl libglvnd-gles libglvnd-glx libgudev libinput libwacom libwayland-server libxcb libxkbcommon libxkbcommon-x11 libxkbfile mesa-libgbm pango pipewire pipewire-libs startup-notification systemd-libs zenity"
RPM_SONAME_REQ_mutter-devel = "libmutter-4.so.0"
RPROVIDES_mutter-devel = "mutter-dev (= 3.32.2)"
RDEPENDS_mutter-devel = "atk-devel cairo-devel cairo-gobject-devel glib2-devel gsettings-desktop-schemas-devel gtk3-devel json-glib-devel libX11-devel libXfixes-devel libXi-devel libglvnd-devel mutter pango-devel pkgconf-pkg-config wayland-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mutter-3.32.2-36.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mutter-devel-3.32.2-36.el8_2.x86_64.rpm \
          "

SRC_URI[mutter.sha256sum] = "c0446bc040538eaf0a6d9d062ae3bb96bb4773a5a8c5c788621c7f36f606795a"
SRC_URI[mutter-devel.sha256sum] = "2f8de5a8090b658b4f2e417f3c09b59757c0b22f68dbe3491a6c9e7d0f285f5c"
