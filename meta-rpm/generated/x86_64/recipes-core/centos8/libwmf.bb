SUMMARY = "generated recipe based on libwmf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype gdk-pixbuf glib-2.0 gtk2 libjpeg-turbo libpng libx11 libxml2 pkgconfig-native xz zlib"
RPM_SONAME_PROV_libwmf = "libwmf-0.2.so.7"
RPM_SONAME_REQ_libwmf = "libX11.so.6 libc.so.6 libdl.so.2 libfreetype.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 liblzma.so.5 libm.so.6 libpng16.so.16 libpthread.so.0 libwmf-0.2.so.7 libwmflite-0.2.so.7 libxml2.so.2 libz.so.1"
RDEPENDS_libwmf = "bash freetype gdk-pixbuf2 glib2 glibc libX11 libjpeg-turbo libpng libwmf-lite libxml2 urw-base35-fonts xz-libs zlib"
RPM_SONAME_REQ_libwmf-devel = "libwmf-0.2.so.7 libwmflite-0.2.so.7"
RPROVIDES_libwmf-devel = "libwmf-dev (= 0.2.9)"
RDEPENDS_libwmf-devel = "bash gtk2-devel libjpeg-turbo-devel libwmf libwmf-lite libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libwmf-lite = "libwmflite-0.2.so.7"
RPM_SONAME_REQ_libwmf-lite = "libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_libwmf-lite = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwmf-0.2.9-8.el8_0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwmf-lite-0.2.9-8.el8_0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwmf-devel-0.2.9-8.el8_0.x86_64.rpm \
          "

SRC_URI[libwmf.sha256sum] = "802b1e6b37fa6d0de68c55a43295c61c82424c5e10924e7e0baa4e222d79339a"
SRC_URI[libwmf-devel.sha256sum] = "51163c8529a6562b910f8b0b347b57ad0de4201b39027e8509e0c867c2e4d982"
SRC_URI[libwmf-lite.sha256sum] = "e539923a2a5b5f484264007524cf13bf9731c27454784ef2b05d516115cd7bf3"
