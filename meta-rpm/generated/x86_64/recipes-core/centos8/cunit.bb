SUMMARY = "generated recipe based on CUnit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_CUnit = "libcunit.so.1"
RPM_SONAME_REQ_CUnit = "libc.so.6"
RDEPENDS_CUnit = "glibc"
RPM_SONAME_REQ_CUnit-devel = "libcunit.so.1"
RPROVIDES_CUnit-devel = "CUnit-dev (= 2.1.3)"
RDEPENDS_CUnit-devel = "CUnit pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/CUnit-2.1.3-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/CUnit-devel-2.1.3-17.el8.x86_64.rpm \
          "

SRC_URI[CUnit.sha256sum] = "bfda50aa82a7f92ce38b127acdd25e8fc0351365853455dea02ca76f94fccef1"
SRC_URI[CUnit-devel.sha256sum] = "2943e3a0fb08a854f5b18bc4abbb8a8cf4ad25263ebf4c40284b54660bc6a93c"
