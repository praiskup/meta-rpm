SUMMARY = "generated recipe based on perl-Encode-Detect srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc perl pkgconfig-native"
RPM_SONAME_REQ_perl-Encode-Detect = "libc.so.6 libgcc_s.so.1 libperl.so.5.26 libpthread.so.0 libstdc++.so.6"
RDEPENDS_perl-Encode-Detect = "glibc libgcc libstdc++ perl-Encode perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Encode-Detect-1.01-28.el8.x86_64.rpm \
          "

SRC_URI[perl-Encode-Detect.sha256sum] = "9a8c84ef379ef2ee58e1b2923fd290d663d86d5be9ef7ef3a88c5f5b983727b7"
