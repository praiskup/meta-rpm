SUMMARY = "generated recipe based on perl-MIME-Charset srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-MIME-Charset = "perl-Carp perl-Encode perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-MIME-Charset-1.012.2-4.el8.noarch.rpm \
          "

SRC_URI[perl-MIME-Charset.sha256sum] = "82a7dd36bf632f4e6838af2c9398593b195d63b0a7610793101b240c4c6ceb65"
