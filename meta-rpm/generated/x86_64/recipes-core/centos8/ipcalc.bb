SUMMARY = "generated recipe based on ipcalc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ipcalc = "libc.so.6 libdl.so.2"
RDEPENDS_ipcalc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ipcalc-0.2.4-4.el8.x86_64.rpm \
          "

SRC_URI[ipcalc.sha256sum] = "dea18976861575d40ffca814dee08a225376c7828a5afc9e5d0a383edd3d8907"
