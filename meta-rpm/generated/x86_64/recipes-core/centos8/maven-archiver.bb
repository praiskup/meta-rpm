SUMMARY = "generated recipe based on maven-archiver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-archiver = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-utils plexus-archiver plexus-interpolation plexus-utils"
RDEPENDS_maven-archiver-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-archiver-3.2.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-archiver-javadoc-3.2.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-archiver.sha256sum] = "13fbfe839ef6dac669bacccc71ff97aea3cb52bfff26d059fe61883e7a14e1f4"
SRC_URI[maven-archiver-javadoc.sha256sum] = "b03c06f691466920b8a35dfa21aa7b4654be1bed559d97fa478607780831a656"
