SUMMARY = "generated recipe based on groff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_groff = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_groff = "bash coreutils gawk glibc groff-base info libgcc libstdc++"
RPM_SONAME_REQ_groff-base = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_groff-base = "bash glibc libgcc libstdc++ sed"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/groff-base-1.22.3-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/groff-1.22.3-18.el8.x86_64.rpm \
          "

SRC_URI[groff.sha256sum] = "6ab6860878a70ad6b4a7f43aa5b351613749b80b28255c84229b093e5eec34f8"
SRC_URI[groff-base.sha256sum] = "b00855013100d3796e9ed6d82b1ab2d4dc7f4a3a3fa2e186f6de8523577974a0"
