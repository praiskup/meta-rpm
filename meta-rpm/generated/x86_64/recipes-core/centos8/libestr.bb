SUMMARY = "generated recipe based on libestr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libestr = "libestr.so.0"
RPM_SONAME_REQ_libestr = "libc.so.6"
RDEPENDS_libestr = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libestr-0.1.10-1.el8.x86_64.rpm \
          "

SRC_URI[libestr.sha256sum] = "7bc2e2a25b04b52a4ec5de2858e75eb07f16495d4de5f7ce926777130302cfe3"
