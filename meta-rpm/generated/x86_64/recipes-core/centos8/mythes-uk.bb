SUMMARY = "generated recipe based on mythes-uk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-uk = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-uk-1.6.5-14.el8.noarch.rpm \
          "

SRC_URI[mythes-uk.sha256sum] = "fa24d199df55acb4e0259183ecdff532a7013e6e4904728344c6e539be598304"
