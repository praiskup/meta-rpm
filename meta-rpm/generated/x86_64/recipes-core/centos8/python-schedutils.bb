SUMMARY = "generated recipe based on python-schedutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-schedutils = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-schedutils = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-schedutils-0.6-6.el8.x86_64.rpm \
          "

SRC_URI[python3-schedutils.sha256sum] = "bafc2680bd298f0fac70854224ef03b7098d4c46ee35e270f6fd58d2e812ff1b"
