SUMMARY = "generated recipe based on rubygem-mysql2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "mariadb-connector-c openssl pkgconfig-native ruby zlib"
RPM_SONAME_REQ_rubygem-mysql2 = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libpthread.so.0 libruby.so.2.5 libssl.so.1.1 libz.so.1"
RDEPENDS_rubygem-mysql2 = "glibc mariadb-connector-c openssl-libs ruby-libs rubygem-bigdecimal rubygems zlib"
RDEPENDS_rubygem-mysql2-doc = "rubygem-mysql2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-mysql2-0.4.10-4.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-mysql2-doc-0.4.10-4.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-mysql2.sha256sum] = "03aec49eeb88f61c462c4dabcfe60aa0dba1463749d47d557ed82020347a461d"
SRC_URI[rubygem-mysql2-doc.sha256sum] = "7a5b4c891acf9c494abec48733b2d49e0898a5ac920b97a16e4f44d79f442b8d"
