SUMMARY = "generated recipe based on hunspell-ar srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ar = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ar-3.5-7.el8.noarch.rpm \
          "

SRC_URI[hunspell-ar.sha256sum] = "439cb607e3a6feb376f77f7105d90ecf6278bc42598d03a8c863340c3dd9c03b"
