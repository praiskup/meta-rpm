SUMMARY = "generated recipe based on adwaita-icon-theme srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_adwaita-icon-theme = "adwaita-cursor-theme bash"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/adwaita-cursor-theme-3.28.0-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/adwaita-icon-theme-3.28.0-2.el8.noarch.rpm \
          "

SRC_URI[adwaita-cursor-theme.sha256sum] = "d9bddc6e8fcf384b62eba02aa61615fd11ac313dc4cf88aa2dda2954c938ca91"
SRC_URI[adwaita-icon-theme.sha256sum] = "3a05ff88858c6f6a6cab24ca233c8cba66686254ca0bbabeccff01c12128feda"
