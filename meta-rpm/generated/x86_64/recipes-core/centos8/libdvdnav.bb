SUMMARY = "generated recipe based on libdvdnav srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdvdread pkgconfig-native"
RPM_SONAME_PROV_libdvdnav = "libdvdnav.so.4"
RPM_SONAME_REQ_libdvdnav = "libc.so.6 libdvdread.so.4 libpthread.so.0"
RDEPENDS_libdvdnav = "glibc libdvdread"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdvdnav-5.0.3-8.el8.x86_64.rpm \
          "

SRC_URI[libdvdnav.sha256sum] = "c86333ba93db8483001d5b916e21d6270cd2d247487807ced1fbe21e171d0d6e"
