SUMMARY = "generated recipe based on perl-NetAddr-IP srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-NetAddr-IP = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-NetAddr-IP = "glibc perl-Carp perl-Exporter perl-Math-BigInt perl-Socket perl-Socket6 perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-NetAddr-IP-4.079-7.el8.x86_64.rpm \
          "

SRC_URI[perl-NetAddr-IP.sha256sum] = "b7da9d01c300c27b02d155b7082dfb40731b9a456c0b723c9c6b6ba6f55cb5e8"
