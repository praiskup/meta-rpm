SUMMARY = "generated recipe based on perl-Text-Diff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Diff = "perl-Algorithm-Diff perl-Carp perl-Exporter perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Text-Diff-1.45-2.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Diff.sha256sum] = "15b87e079458628fdc3148d21c80e4e72f59ffc244467d34a3260dcb2c6863d0"
