SUMMARY = "generated recipe based on hunspell-ast srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ast = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ast-0.02-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-ast.sha256sum] = "e6bad41baa456eeb65eb5aad4c94ac934ad930dace43aaaecdbe8ff97dc4ae28"
