SUMMARY = "generated recipe based on conntrack-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl libnetfilter-conntrack libnetfilter-cthelper libnetfilter-cttimeout libnetfilter-queue libnfnetlink pkgconfig-native systemd-libs"
RPM_SONAME_REQ_conntrack-tools = "libc.so.6 libdl.so.2 libmnl.so.0 libnetfilter_conntrack.so.3 libnetfilter_cthelper.so.0 libnetfilter_cttimeout.so.1 libnetfilter_queue.so.1 libnfnetlink.so.0 libsystemd.so.0"
RDEPENDS_conntrack-tools = "bash glibc libmnl libnetfilter_conntrack libnetfilter_cthelper libnetfilter_cttimeout libnetfilter_queue libnfnetlink systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/conntrack-tools-1.4.4-10.el8.x86_64.rpm \
          "

SRC_URI[conntrack-tools.sha256sum] = "a077f5a786a1c2f61da812a32de865ae51bc74f5f08d6328cf67ece4f7ce10de"
