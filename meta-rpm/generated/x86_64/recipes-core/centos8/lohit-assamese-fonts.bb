SUMMARY = "generated recipe based on lohit-assamese-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-assamese-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-assamese-fonts-2.91.5-3.el8.noarch.rpm \
          "

SRC_URI[lohit-assamese-fonts.sha256sum] = "e40f899f91aee1034bc62a63d9c412bc9dcc07b1e760387bb9c1c5d70c34c919"
