SUMMARY = "generated recipe based on perl-Sub-Install srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Install = "perl-Carp perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Sub-Install-0.928-14.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Install.sha256sum] = "37c6272d20e1fa67ea3414d13db66854b052b0e619aab9f8db4f6ee32448dd1c"
