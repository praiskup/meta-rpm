SUMMARY = "generated recipe based on fltk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig libgcc libglvnd libice libjpeg-turbo libpng libsm libx11 libxcursor libxext libxfixes libxft libxinerama libxrender libxt mesa-libglu pkgconfig-native zlib"
RPM_SONAME_PROV_fltk = "libfltk.so.1.3 libfltk_forms.so.1.3 libfltk_gl.so.1.3 libfltk_images.so.1.3"
RPM_SONAME_REQ_fltk = "libGL.so.1 libGLU.so.1 libX11.so.6 libXcursor.so.1 libXext.so.6 libXfixes.so.3 libXft.so.2 libXinerama.so.1 libXrender.so.1 libc.so.6 libdl.so.2 libfltk.so.1.3 libfontconfig.so.1 libgcc_s.so.1 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_fltk = "fontconfig glibc libX11 libXcursor libXext libXfixes libXft libXinerama libXrender libgcc libglvnd-glx libjpeg-turbo libpng libstdc++ mesa-libGLU zlib"
RPM_SONAME_REQ_fltk-devel = "libfltk.so.1.3 libfltk_forms.so.1.3 libfltk_gl.so.1.3 libfltk_images.so.1.3"
RPROVIDES_fltk-devel = "fltk-dev (= 1.3.4)"
RDEPENDS_fltk-devel = "bash fltk libICE-devel libSM-devel libX11-devel libXft-devel libXt-devel libglvnd-devel libstdc++-devel mesa-libGLU-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fltk-1.3.4-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fltk-devel-1.3.4-5.el8.x86_64.rpm \
          "

SRC_URI[fltk.sha256sum] = "41c1ec377e8b8e5dfb50addb0ee69529a84ed9b771a40c8c24dbed16b2a5d53f"
SRC_URI[fltk-devel.sha256sum] = "243047bc5bea5e48663d453d5a62231b9809474b416d06c74b5780830ab49f48"
