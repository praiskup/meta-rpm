SUMMARY = "generated recipe based on readline srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_PROV_readline = "libhistory.so.7 libreadline.so.7"
RPM_SONAME_REQ_readline = "libc.so.6 libtinfo.so.6"
RDEPENDS_readline = "bash glibc info ncurses-libs"
RPM_SONAME_REQ_readline-devel = "libhistory.so.7 libreadline.so.7"
RPROVIDES_readline-devel = "readline-dev (= 7.0)"
RDEPENDS_readline-devel = "bash info ncurses-devel readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/readline-7.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/readline-devel-7.0-10.el8.x86_64.rpm \
          "

SRC_URI[readline.sha256sum] = "fea868a7d82a7b6f392260ed4afb472dc4428fd71eab1456319f423a845b5084"
SRC_URI[readline-devel.sha256sum] = "66e16c4c18c3e44a6deaf515b947366baa50bc4bf1a4df06e4680f3cb6e7c193"
