SUMMARY = "generated recipe based on gdbm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_gdbm = "libc.so.6 libgdbm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_gdbm = "gdbm-libs glibc ncurses-libs readline"
RPM_SONAME_REQ_gdbm-devel = "libgdbm.so.6 libgdbm_compat.so.4"
RPROVIDES_gdbm-devel = "gdbm-dev (= 1.18)"
RDEPENDS_gdbm-devel = "bash gdbm gdbm-libs info"
RPM_SONAME_PROV_gdbm-libs = "libgdbm.so.6 libgdbm_compat.so.4"
RPM_SONAME_REQ_gdbm-libs = "ld-linux-x86-64.so.2 libc.so.6 libgdbm.so.6"
RDEPENDS_gdbm-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gdbm-1.18-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gdbm-devel-1.18-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gdbm-libs-1.18-1.el8.x86_64.rpm \
          "

SRC_URI[gdbm.sha256sum] = "76d81e433a5291df491d2e289de9b33d4e5b98dcf48fd0a003c2767415d3e0aa"
SRC_URI[gdbm-devel.sha256sum] = "1262db6fba398a1008f76972ac9365be09fa32b964c454cd82ddedc8dd72e658"
SRC_URI[gdbm-libs.sha256sum] = "3a3cb5a11f8e844cd1bf7c0e7bb6c12cc63e743029df50916ce7e6a9f8a4e169"
