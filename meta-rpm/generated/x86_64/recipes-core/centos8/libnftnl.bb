SUMMARY = "generated recipe based on libnftnl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_PROV_libnftnl = "libnftnl.so.11"
RPM_SONAME_REQ_libnftnl = "libc.so.6 libmnl.so.0"
RDEPENDS_libnftnl = "glibc libmnl"
RPM_SONAME_REQ_libnftnl-devel = "libnftnl.so.11"
RPROVIDES_libnftnl-devel = "libnftnl-dev (= 1.1.5)"
RDEPENDS_libnftnl-devel = "libmnl-devel libnftnl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnftnl-1.1.5-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnftnl-devel-1.1.5-4.el8.x86_64.rpm \
          "

SRC_URI[libnftnl.sha256sum] = "c1bb77ed45ae47dc068445c6dfa4b70b273a3daf8cd82b9fa7a50e3d59abe3c1"
SRC_URI[libnftnl-devel.sha256sum] = "dd4e1bdb3d7768e5fd8148b4684ee67c4a9668cdb590b37237e7863812cc6989"
