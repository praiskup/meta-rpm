SUMMARY = "generated recipe based on alsa-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib fftw libgcc libsamplerate0 ncurses pkgconfig-native"
RPM_SONAME_REQ_alsa-utils = "libasound.so.2 libatopology.so.2 libc.so.6 libdl.so.2 libformw.so.6 libm.so.6 libmenuw.so.6 libncursesw.so.6 libpanelw.so.6 libpthread.so.0 librt.so.1 libsamplerate.so.0 libtinfo.so.6"
RDEPENDS_alsa-utils = "alsa-lib bash glibc libsamplerate ncurses-libs systemd"
RPM_SONAME_REQ_alsa-utils-alsabat = "libasound.so.2 libatopology.so.2 libc.so.6 libdl.so.2 libfftw3f.so.3 libgcc_s.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_alsa-utils-alsabat = "alsa-lib bash fftw-libs-single glibc libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-utils-1.2.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-utils-alsabat-1.2.1-2.el8.x86_64.rpm \
          "

SRC_URI[alsa-utils.sha256sum] = "179a19bc5269834d1cc1ba2ed6cdf40627fe5ee94b74cd36eea3fe242804fd54"
SRC_URI[alsa-utils-alsabat.sha256sum] = "01ab93b7be63445d04644a369672a426a7cd7e689d719f96e38756e52b66f5bc"
