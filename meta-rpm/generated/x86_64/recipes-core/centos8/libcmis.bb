SUMMARY = "generated recipe based on libcmis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost curl libgcc libxml2 pkgconfig-native"
RPM_SONAME_PROV_libcmis = "libcmis-0.5.so.5 libcmis-c-0.5.so.5"
RPM_SONAME_REQ_libcmis = "libboost_date_time.so.1.66.0 libc.so.6 libcmis-0.5.so.5 libcurl.so.4 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libxml2.so.2"
RDEPENDS_libcmis = "boost-date-time glibc libcurl libgcc libstdc++ libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libcmis-0.5.1-13.el8.x86_64.rpm \
          "

SRC_URI[libcmis.sha256sum] = "46c56d9377ac3831837c66ad3bf67fdb46c04513149dadfa99cce1aecfb6f59b"
