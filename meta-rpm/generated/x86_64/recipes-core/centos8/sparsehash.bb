SUMMARY = "generated recipe based on sparsehash srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_sparsehash-devel = "sparsehash-dev (= 2.0.2)"
RDEPENDS_sparsehash-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sparsehash-devel-2.0.2-8.el8.x86_64.rpm \
          "

SRC_URI[sparsehash-devel.sha256sum] = "1d0f51fe7b34f877d4a2fd01a45ea8249b82cec1672c4e20cad6ff8c1e8499ba"
