SUMMARY = "generated recipe based on hunspell-ss srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ss = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ss-0.20091030-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-ss.sha256sum] = "70fba97b8188039d2273d189606151fad8d14aafb71e237f724d3c8cb825fd84"
