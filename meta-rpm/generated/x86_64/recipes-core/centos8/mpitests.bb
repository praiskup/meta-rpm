SUMMARY = "generated recipe based on mpitests srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc mpich mvapich2 openmpi pkgconfig-native"
RPM_SONAME_REQ_mpitests-mpich = "libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-mpich = "glibc libgcc libstdc++ mpich"
RPM_SONAME_REQ_mpitests-mvapich2 = "libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-mvapich2 = "glibc libgcc libstdc++ mvapich2"
RPM_SONAME_REQ_mpitests-mvapich2-psm2 = "libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-mvapich2-psm2 = "glibc libgcc libstdc++ mvapich2 mvapich2-psm2"
RPM_SONAME_REQ_mpitests-openmpi = "libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-openmpi = "glibc libgcc libstdc++ openmpi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpitests-mpich-5.4.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpitests-mvapich2-5.4.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpitests-mvapich2-psm2-5.4.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpitests-openmpi-5.4.2-4.el8.x86_64.rpm \
          "

SRC_URI[mpitests-mpich.sha256sum] = "cf87fc1f5808ee27e5bbf40aafb67e7489496a091ff40a04c0d47e02b193b39b"
SRC_URI[mpitests-mvapich2.sha256sum] = "80ebaf825aa9615d5aa271b42339b7af58f4eaf1b1e78f62c1305085e98bdc29"
SRC_URI[mpitests-mvapich2-psm2.sha256sum] = "a104d904cb8ed81b05d5f6434045029046b5fafda1e739f0d9d601b46725d3ba"
SRC_URI[mpitests-openmpi.sha256sum] = "81b065a012a807bdf0145c8af6d5f17c9e4ea39c4fd330887c0194f6c37ea1f1"
