SUMMARY = "generated recipe based on libdwarf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native zlib"
RPM_SONAME_PROV_libdwarf = "libdwarf.so.1"
RPM_SONAME_REQ_libdwarf = "libc.so.6 libelf.so.1 libz.so.1"
RDEPENDS_libdwarf = "elfutils-libelf glibc zlib"
RPM_SONAME_REQ_libdwarf-devel = "libdwarf.so.1"
RPROVIDES_libdwarf-devel = "libdwarf-dev (= 20180129)"
RDEPENDS_libdwarf-devel = "libdwarf"
RDEPENDS_libdwarf-static = "libdwarf-devel"
RPM_SONAME_REQ_libdwarf-tools = "libc.so.6 libdwarf.so.1 libelf.so.1 libz.so.1"
RDEPENDS_libdwarf-tools = "elfutils-libelf glibc libdwarf zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdwarf-20180129-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdwarf-devel-20180129-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdwarf-static-20180129-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdwarf-tools-20180129-4.el8.x86_64.rpm \
          "

SRC_URI[libdwarf.sha256sum] = "3ebcd99ece30517541bfd38a56f3f8cd85d337824b33d18d0126907d3a19cde7"
SRC_URI[libdwarf-devel.sha256sum] = "fcbd98f34d05c1b640858b540665a02dc5505d2eb7ea1bd7701138e201c84c73"
SRC_URI[libdwarf-static.sha256sum] = "b7a17928e41168e2350bff65e1496c9864f47a2ff71e13c1a440a089665858fc"
SRC_URI[libdwarf-tools.sha256sum] = "d86a5ab987a0b8d78f4945aa2476bbf6168f8a537ac4e96210a6c71387956229"
