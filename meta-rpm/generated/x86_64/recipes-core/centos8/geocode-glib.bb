SUMMARY = "generated recipe based on geocode-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib libgcc libsoup-2.4 pkgconfig-native"
RPM_SONAME_PROV_geocode-glib = "libgeocode-glib.so.0"
RPM_SONAME_REQ_geocode-glib = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libsoup-2.4.so.1"
RDEPENDS_geocode-glib = "glib2 glibc json-glib libgcc libsoup"
RPM_SONAME_REQ_geocode-glib-devel = "libgeocode-glib.so.0"
RPROVIDES_geocode-glib-devel = "geocode-glib-dev (= 3.26.0)"
RDEPENDS_geocode-glib-devel = "geocode-glib glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geocode-glib-3.26.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geocode-glib-devel-3.26.0-1.el8.x86_64.rpm \
          "

SRC_URI[geocode-glib.sha256sum] = "0598c855df3658c1c06402c20a998d6b17f8f01ad5eb1b256791cc615fae92b3"
SRC_URI[geocode-glib-devel.sha256sum] = "fdf430523c2d3b27e82a490dff56b21c43b2b49814c8b8084fd6e14411fd38fd"
