SUMMARY = "generated recipe based on liburing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liburing = "liburing.so.1"
RPM_SONAME_REQ_liburing = "libc.so.6"
RDEPENDS_liburing = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liburing-0.2-2.el8.x86_64.rpm \
          "

SRC_URI[liburing.sha256sum] = "147a0eceddba0c7d0d6ec7b7fc7d0a1bc96c7b3addab5c958c18d665b08661e7"
