SUMMARY = "generated recipe based on libdazzle srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gdk-pixbuf glib-2.0 gtk+3 libgcc pango pkgconfig-native"
RPM_SONAME_PROV_libdazzle = "libdazzle-1.0.so.0"
RPM_SONAME_REQ_libdazzle = "libc.so.6 libcairo.so.2 libdazzle-1.0.so.0 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_libdazzle = "cairo gdk-pixbuf2 glib2 glibc gtk3 libgcc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdazzle-3.28.5-1.el8.x86_64.rpm \
          "

SRC_URI[libdazzle.sha256sum] = "ec486958c8e753ee76060c23e8225a935b532fef654ca4d2afee121bdb80b1b6"
