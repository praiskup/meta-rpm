SUMMARY = "generated recipe based on libatomic_ops srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libatomic_ops = "libatomic_ops.so.1 libatomic_ops_gpl.so.1"
RPM_SONAME_REQ_libatomic_ops = "libatomic_ops.so.1 libc.so.6"
RDEPENDS_libatomic_ops = "glibc"
RPM_SONAME_REQ_libatomic_ops-devel = "libatomic_ops.so.1 libatomic_ops_gpl.so.1"
RPROVIDES_libatomic_ops-devel = "libatomic_ops-dev (= 7.6.2)"
RDEPENDS_libatomic_ops-devel = "libatomic_ops pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libatomic_ops-7.6.2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libatomic_ops-devel-7.6.2-3.el8.x86_64.rpm \
          "

SRC_URI[libatomic_ops.sha256sum] = "605145b1755d7ae4d8dddb0296ec3cb176cd771a8bbc1ccbb3e5f19a850cc517"
SRC_URI[libatomic_ops-devel.sha256sum] = "10b938243a6f06e5f719e30f4641f7802d600958aeb356e45b4915f27df3480e"
