SUMMARY = "generated recipe based on foomatic-db srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_foomatic-db = "foomatic-db-filesystem foomatic-db-ppds"
RDEPENDS_foomatic-db-ppds = "cups foomatic-db-filesystem sed"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/foomatic-db-4.0-57.20180102.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/foomatic-db-filesystem-4.0-57.20180102.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/foomatic-db-ppds-4.0-57.20180102.el8.noarch.rpm \
          "

SRC_URI[foomatic-db.sha256sum] = "7142b4e0eb58e2f060b51b53228e20e06756a1d4b6ddbbe6d35d54cad1f75750"
SRC_URI[foomatic-db-filesystem.sha256sum] = "5869c262eb00d9514c065467089d40f143697016c18621845073ffd2063ab145"
SRC_URI[foomatic-db-ppds.sha256sum] = "4d0267795974db493f58bfbe6ed7166d36a9b7701dd4b923242ca87484f59c56"
