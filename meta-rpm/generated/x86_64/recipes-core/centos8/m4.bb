SUMMARY = "generated recipe based on m4 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_m4 = "libc.so.6"
RDEPENDS_m4 = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/m4-1.4.18-7.el8.x86_64.rpm \
          "

SRC_URI[m4.sha256sum] = "72e3897215da0991ca4f10af35fc60906a0da2cae6cd4da8c00f9aed1beee5ae"
