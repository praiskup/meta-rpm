SUMMARY = "generated recipe based on perl-Config-Perl-V srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Config-Perl-V = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Config-Perl-V-0.30-1.el8.noarch.rpm \
          "

SRC_URI[perl-Config-Perl-V.sha256sum] = "56eefdc7e9349b188b1d94959e1c6e0345d8c44e95930b236ca50c2285d75d81"
