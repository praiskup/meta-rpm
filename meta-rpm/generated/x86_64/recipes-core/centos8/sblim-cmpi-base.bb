SUMMARY = "generated recipe based on sblim-cmpi-base srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sblim-indication-helper"
RPM_SONAME_PROV_sblim-cmpi-base = "libcmpiOSBase_BaseBoardProvider.so libcmpiOSBase_CSBaseBoardProvider.so libcmpiOSBase_CSProcessorProvider.so libcmpiOSBase_Common.so.0 libcmpiOSBase_ComputerSystemProvider.so libcmpiOSBase_OSProcessProvider.so libcmpiOSBase_OperatingSystemProvider.so libcmpiOSBase_OperatingSystemStatisticalDataProvider.so libcmpiOSBase_OperatingSystemStatisticsProvider.so libcmpiOSBase_ProcessorProvider.so libcmpiOSBase_RunningOSProvider.so libcmpiOSBase_UnixProcessProvider.so libdmiinfo.so.0"
RPM_SONAME_REQ_sblim-cmpi-base = "libc.so.6 libcmpiOSBase_Common.so.0 libdmiinfo.so.0 libind_helper.so.0 libpthread.so.0"
RDEPENDS_sblim-cmpi-base = "bash glibc sblim-indication_helper sblim-sfcb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sblim-cmpi-base-1.6.4-14.el8.x86_64.rpm \
          "

SRC_URI[sblim-cmpi-base.sha256sum] = "1ab3d874501cd313e74d051bc26acf96a7fdb5b948a2df02743cbfe82ed668a6"
