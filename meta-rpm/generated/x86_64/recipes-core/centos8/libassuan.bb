SUMMARY = "generated recipe based on libassuan srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libassuan = "libassuan.so.0"
RPM_SONAME_REQ_libassuan = "libc.so.6 libgpg-error.so.0"
RDEPENDS_libassuan = "glibc libgpg-error"
RPM_SONAME_REQ_libassuan-devel = "libassuan.so.0"
RPROVIDES_libassuan-devel = "libassuan-dev (= 2.5.1)"
RDEPENDS_libassuan-devel = "bash info libassuan"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libassuan-2.5.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libassuan-devel-2.5.1-3.el8.x86_64.rpm \
          "

SRC_URI[libassuan.sha256sum] = "b49e8c674e462e3f494e825c5fca64002008cbf7a47bf131aa98b7f41678a6eb"
SRC_URI[libassuan-devel.sha256sum] = "ad322e361dc814444676fa2470b1c6b9e3eeafd1d30037a5a225d206a9789f3f"
