SUMMARY = "generated recipe based on xorg-x11-xauth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxau libxext libxmu pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-xauth = "libX11.so.6 libXau.so.6 libXext.so.6 libXmuu.so.1 libc.so.6"
RDEPENDS_xorg-x11-xauth = "glibc libX11 libXau libXext libXmu"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-xauth-1.0.9-12.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-xauth.sha256sum] = "874eb3143adc04aff168f3d026a584af8f4812ac436a8c29f89d015d3c0c4e81"
