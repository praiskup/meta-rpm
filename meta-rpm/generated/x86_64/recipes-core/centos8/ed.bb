SUMMARY = "generated recipe based on ed srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ed = "libc.so.6"
RDEPENDS_ed = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ed-1.14.2-4.el8.x86_64.rpm \
          "

SRC_URI[ed.sha256sum] = "5164c6ac64ae78ff8ccbfd0fcdcd114663fa21ea43f535c7e0bda38e4d1c8a3e"
