SUMMARY = "generated recipe based on perl-LDAP srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-LDAP = "perl-Authen-SASL perl-Carp perl-Convert-ASN1 perl-Encode perl-Exporter perl-HTTP-Message perl-HTTP-Negotiate perl-IO perl-IO-Socket-SSL perl-JSON perl-LWP-MediaTypes perl-MIME-Base64 perl-Socket perl-Text-Soundex perl-Time-Local perl-constant perl-interpreter perl-libs perl-libwww-perl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-LDAP-0.66-7.el8.noarch.rpm \
          "

SRC_URI[perl-LDAP.sha256sum] = "86f187dd517ed2ac8a49ba5122c19f596de44876f5a5b21106ab09da14e142e4"
