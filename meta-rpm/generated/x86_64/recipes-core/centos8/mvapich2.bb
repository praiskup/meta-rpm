SUMMARY = "generated recipe based on mvapich2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc hwloc libgcc libpsm2 pkgconfig-native rdma-core"
RPM_SONAME_PROV_mvapich2 = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPM_SONAME_REQ_mvapich2 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgfortran.so.5 libhwloc.so.5 libibmad.so.5 libibumad.so.3 libibverbs.so.1 libm.so.6 libmpi.so.12 libpthread.so.0 libquadmath.so.0 librdmacm.so.1 librt.so.1 libstdc++.so.6"
RDEPENDS_mvapich2 = "environment-modules glibc hwloc-libs infiniband-diags libgcc libgfortran libibumad libibverbs libquadmath librdmacm libstdc++ perl-interpreter platform-python"
RPM_SONAME_PROV_mvapich2-psm2 = "libmpi.so.12 libmpicxx.so.12 libmpifort.so.12"
RPM_SONAME_REQ_mvapich2-psm2 = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libgfortran.so.5 libhwloc.so.5 libibverbs.so.1 libm.so.6 libmpi.so.12 libpsm2.so.2 libpthread.so.0 libquadmath.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_mvapich2-psm2 = "environment-modules glibc hwloc-libs libgcc libgfortran libibverbs libpsm2 libquadmath libstdc++ perl-interpreter platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mvapich2-2.3.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mvapich2-psm2-2.3.2-2.el8.x86_64.rpm \
          "

SRC_URI[mvapich2.sha256sum] = "ec0c9de31fde6286b675ccd3a2bbbb4a1ac0a760e092d77059305dd521c1a8f4"
SRC_URI[mvapich2-psm2.sha256sum] = "b91be0cf145f7e61ec5ac466fc12a0d91d8c6a90e1f11ef3ae14d7d5b01d8ed4"
