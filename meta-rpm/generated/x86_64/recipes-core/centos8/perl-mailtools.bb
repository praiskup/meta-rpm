SUMMARY = "generated recipe based on perl-MailTools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-MailTools = "perl-Carp perl-Exporter perl-IO perl-Net-SMTP-SSL perl-TimeDate perl-interpreter perl-libnet perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-MailTools-2.20-2.el8.noarch.rpm \
          "

SRC_URI[perl-MailTools.sha256sum] = "ffa431832aaf83ec022a4acee61d8172edb9daf385b9708997d1f707c418e800"
