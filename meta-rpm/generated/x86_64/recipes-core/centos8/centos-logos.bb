SUMMARY = "generated recipe based on centos-logos srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_centos-backgrounds = "centos-logos"
RDEPENDS_centos-logos = "bash coreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/centos-backgrounds-80.5-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/centos-logos-ipa-80.5-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-logos-80.5-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-logos-httpd-80.5-2.el8.noarch.rpm \
          "

SRC_URI[centos-backgrounds.sha256sum] = "f380577daa9c8e9d0930a0ae649c48b015056b238296d3be0763d02c4cf99ead"
SRC_URI[centos-logos.sha256sum] = "5e48c3d0307e708276b7dc2a243e73d83c439ce0c0772b4ddd22ff1218359caa"
SRC_URI[centos-logos-httpd.sha256sum] = "5717f6fb0a3d0a4cb327a19f5a9ac7114eb7fa251f969dfa386f729c4a37a3d1"
SRC_URI[centos-logos-ipa.sha256sum] = "22eaeb0c9401b53779a766b5044ea7f174fcf3fce41c706dc11aaa04f9fca24c"
