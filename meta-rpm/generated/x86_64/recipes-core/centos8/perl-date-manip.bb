SUMMARY = "generated recipe based on perl-Date-Manip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Date-Manip = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-IO perl-PathTools perl-Storable perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Date-Manip-6.60-2.el8.noarch.rpm \
          "

SRC_URI[perl-Date-Manip.sha256sum] = "756d8c63b30999c8f661e3b845b3b1e4ebd145371af861d59cea02a85fc51e19"
