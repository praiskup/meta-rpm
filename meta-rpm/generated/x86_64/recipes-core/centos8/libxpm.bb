SUMMARY = "generated recipe based on libXpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxt pkgconfig-native"
RPM_SONAME_PROV_libXpm = "libXpm.so.4"
RPM_SONAME_REQ_libXpm = "libX11.so.6 libc.so.6"
RDEPENDS_libXpm = "glibc libX11"
RPM_SONAME_REQ_libXpm-devel = "libX11.so.6 libXext.so.6 libXpm.so.4 libXt.so.6 libc.so.6"
RPROVIDES_libXpm-devel = "libXpm-dev (= 3.5.12)"
RDEPENDS_libXpm-devel = "glibc libX11 libX11-devel libXext libXpm libXt pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXpm-3.5.12-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXpm-devel-3.5.12-8.el8.x86_64.rpm \
          "

SRC_URI[libXpm.sha256sum] = "81b7139bb8d8252d4b4d711a875c9bc37962774c8e912a8175643c995eaafe94"
SRC_URI[libXpm-devel.sha256sum] = "a0e31346d9f7b38239c7f8ab887f39243d46c9f99544490f9576e818f2038c84"
