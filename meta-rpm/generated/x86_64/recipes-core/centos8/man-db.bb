SUMMARY = "generated recipe based on man-db srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm libpipeline pkgconfig-native zlib"
RPM_SONAME_PROV_man-db = "libman-2.7.6.1.so libmandb-2.7.6.1.so"
RPM_SONAME_REQ_man-db = "libc.so.6 libgdbm.so.6 libman-2.7.6.1.so libmandb-2.7.6.1.so libpipeline.so.1 libz.so.1"
RDEPENDS_man-db = "bash coreutils gdbm-libs glibc grep groff-base gzip less libpipeline zlib"
RDEPENDS_man-db-cron = "bash crontabs man-db"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/man-db-2.7.6.1-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/man-db-cron-2.7.6.1-17.el8.noarch.rpm \
          "

SRC_URI[man-db.sha256sum] = "a0b6a8d5530f5003f5c8d56211246cd1130a5b4dc1396dc50da70ce0e128b02c"
SRC_URI[man-db-cron.sha256sum] = "59cf51d25f07d4a38c51884b3ddc3555c5bc8ddf9c7d1f2b62647c41365b4d95"
