SUMMARY = "generated recipe based on a52dec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liba52 = "liba52.so.0"
RPM_SONAME_REQ_liba52 = "libc.so.6 libm.so.6"
RDEPENDS_liba52 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liba52-0.7.4-32.el8.x86_64.rpm \
          "

SRC_URI[liba52.sha256sum] = "bb7180f7b246c0f353102d0750061adedc6cbf97c9fe3b5c123e28c011eb0c38"
