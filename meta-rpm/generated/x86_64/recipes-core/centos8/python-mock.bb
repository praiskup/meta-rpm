SUMMARY = "generated recipe based on python-mock srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-mock = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-mock-2.0.0-11.el8.noarch.rpm \
          "

SRC_URI[python3-mock.sha256sum] = "62e125a0c55cfa69ac8000d7fecab0574509663de326450f5c66bd36f7308b2c"
