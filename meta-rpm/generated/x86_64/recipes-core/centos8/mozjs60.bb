SUMMARY = "generated recipe based on mozjs60 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_mozjs60 = "libmozjs-60.so.0"
RPM_SONAME_REQ_mozjs60 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mozjs60 = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_mozjs60-devel = "libmozjs-60.so.0"
RPROVIDES_mozjs60-devel = "mozjs60-dev (= 60.9.0)"
RDEPENDS_mozjs60-devel = "mozjs60 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mozjs60-60.9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mozjs60-devel-60.9.0-4.el8.x86_64.rpm \
          "

SRC_URI[mozjs60.sha256sum] = "03b50a4ea5cf5655c67e2358fabb6e563eec4e7929e7fc6c4e92c92694f60fa0"
SRC_URI[mozjs60-devel.sha256sum] = "79225e8e9feba3a18abbd9a451e16955e208d1f54cc59947a445ccfce417482d"
