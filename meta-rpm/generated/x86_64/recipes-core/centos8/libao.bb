SUMMARY = "generated recipe based on libao srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native pulseaudio"
RPM_SONAME_PROV_libao = "libalsa.so libao.so.4 liboss.so libpulse.so"
RPM_SONAME_REQ_libao = "libao.so.4 libasound.so.2 libc.so.6 libdl.so.2 libpthread.so.0 libpulse-simple.so.0 libpulse.so.0"
RDEPENDS_libao = "alsa-lib glibc pulseaudio-libs"
RPM_SONAME_REQ_libao-devel = "libao.so.4"
RPROVIDES_libao-devel = "libao-dev (= 1.2.0)"
RDEPENDS_libao-devel = "libao pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libao-1.2.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libao-devel-1.2.0-10.el8.x86_64.rpm \
          "

SRC_URI[libao.sha256sum] = "037301a1b883ea0fa19cbe1336f8946c1b8de9fe6c18f097a1d50f6d14ef2b8a"
SRC_URI[libao-devel.sha256sum] = "73805d4be52398895be676e32b0f64c13da7ba064c2a3c13eaedd11821db88e5"
