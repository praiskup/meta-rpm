SUMMARY = "generated recipe based on sbc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sbc = "libsbc.so.1"
RPM_SONAME_REQ_sbc = "libc.so.6"
RDEPENDS_sbc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sbc-1.3-9.el8.x86_64.rpm \
          "

SRC_URI[sbc.sha256sum] = "8e3fc1308cca65e918af46bcf903a995295aab0b6a8b4976bc152d06efdeba15"
