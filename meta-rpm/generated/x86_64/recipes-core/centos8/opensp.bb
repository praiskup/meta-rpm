SUMMARY = "generated recipe based on opensp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_opensp = "libosp.so.5"
RPM_SONAME_REQ_opensp = "libc.so.6 libgcc_s.so.1 libm.so.6 libosp.so.5 libpthread.so.0 libstdc++.so.6"
RDEPENDS_opensp = "glibc libgcc libstdc++ sgml-common"
RPM_SONAME_REQ_opensp-devel = "libosp.so.5"
RPROVIDES_opensp-devel = "opensp-dev (= 1.5.2)"
RDEPENDS_opensp-devel = "opensp"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opensp-1.5.2-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opensp-devel-1.5.2-28.el8.x86_64.rpm \
          "

SRC_URI[opensp.sha256sum] = "e1caa8010a955bdc65ef9ab1fd228eb0456855c1c6bf88f5c32ccb8294942e98"
SRC_URI[opensp-devel.sha256sum] = "9137705c896a458765e0d9ab1bf20671cf2777f8c528c045c2e3ec4ec5738b05"
