SUMMARY = "generated recipe based on lohit-bengali-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-bengali-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-bengali-fonts-2.91.5-3.el8.noarch.rpm \
          "

SRC_URI[lohit-bengali-fonts.sha256sum] = "d0097994334b5f77b4caedd63014c75f87c272b9738dd7a50ff940d43ebd587a"
