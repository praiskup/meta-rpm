SUMMARY = "generated recipe based on mpg123 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native pulseaudio"
RPM_SONAME_REQ_mpg123 = "libasound.so.2 libc.so.6 libdl.so.2 libm.so.6 libmpg123.so.0 libout123.so.0"
RDEPENDS_mpg123 = "alsa-lib glibc mpg123-libs"
RPM_SONAME_REQ_mpg123-devel = "libmpg123.so.0 libout123.so.0"
RPROVIDES_mpg123-devel = "mpg123-dev (= 1.25.10)"
RDEPENDS_mpg123-devel = "mpg123-libs pkgconf-pkg-config"
RPM_SONAME_PROV_mpg123-libs = "libmpg123.so.0 libout123.so.0"
RPM_SONAME_REQ_mpg123-libs = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mpg123-libs = "glibc"
RPM_SONAME_REQ_mpg123-plugins-pulseaudio = "libc.so.6 libdl.so.2 libm.so.6 libpulse-simple.so.0 libpulse.so.0"
RDEPENDS_mpg123-plugins-pulseaudio = "glibc mpg123 pulseaudio-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpg123-1.25.10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpg123-libs-1.25.10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpg123-plugins-pulseaudio-1.25.10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mpg123-devel-1.25.10-2.el8.x86_64.rpm \
          "

SRC_URI[mpg123.sha256sum] = "7d4a367094797cdbacddb3fa48ab0ee2befd3bc824e5debb5d5df896d6e63e09"
SRC_URI[mpg123-devel.sha256sum] = "d9a5f8ab09805094e51ac74196809c71e65b3c63ae2e1846257f6e4bd8831209"
SRC_URI[mpg123-libs.sha256sum] = "1149d924ba49674a21beeca8415a2bacf0f92b3283903a3107563cd350e87c8e"
SRC_URI[mpg123-plugins-pulseaudio.sha256sum] = "aa1152dce6d50e96ec1cce760ecf7e668878bed1012e92ca6860b286acbf7e39"
