SUMMARY = "generated recipe based on frei0r-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gavl libgcc opencv pkgconfig-native"
RPROVIDES_frei0r-devel = "frei0r-dev (= 1.6.1)"
RDEPENDS_frei0r-devel = "frei0r-plugins pkgconf-pkg-config"
RPM_SONAME_REQ_frei0r-plugins = "libc.so.6 libcairo.so.2 libgavl.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_frei0r-plugins = "cairo gavl glibc libgcc libstdc++"
RPM_SONAME_REQ_frei0r-plugins-opencv = "libc.so.6 libgcc_s.so.1 libm.so.6 libopencv_aruco.so.3.4 libopencv_bgsegm.so.3.4 libopencv_bioinspired.so.3.4 libopencv_calib3d.so.3.4 libopencv_ccalib.so.3.4 libopencv_core.so.3.4 libopencv_cvv.so.3.4 libopencv_datasets.so.3.4 libopencv_dpm.so.3.4 libopencv_face.so.3.4 libopencv_features2d.so.3.4 libopencv_flann.so.3.4 libopencv_freetype.so.3.4 libopencv_fuzzy.so.3.4 libopencv_hfs.so.3.4 libopencv_highgui.so.3.4 libopencv_img_hash.so.3.4 libopencv_imgcodecs.so.3.4 libopencv_imgproc.so.3.4 libopencv_line_descriptor.so.3.4 libopencv_ml.so.3.4 libopencv_objdetect.so.3.4 libopencv_optflow.so.3.4 libopencv_phase_unwrapping.so.3.4 libopencv_photo.so.3.4 libopencv_plot.so.3.4 libopencv_reg.so.3.4 libopencv_rgbd.so.3.4 libopencv_saliency.so.3.4 libopencv_shape.so.3.4 libopencv_stereo.so.3.4 libopencv_stitching.so.3.4 libopencv_structured_light.so.3.4 libopencv_superres.so.3.4 libopencv_surface_matching.so.3.4 libopencv_tracking.so.3.4 libopencv_video.so.3.4 libopencv_videoio.so.3.4 libopencv_videostab.so.3.4 libopencv_ximgproc.so.3.4 libopencv_xobjdetect.so.3.4 libopencv_xphoto.so.3.4 libstdc++.so.6"
RDEPENDS_frei0r-plugins-opencv = "frei0r-plugins glibc libgcc libstdc++ opencv-contrib opencv-core"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/frei0r-plugins-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/frei0r-plugins-opencv-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/frei0r-devel-1.6.1-6.el8.x86_64.rpm \
          "

SRC_URI[frei0r-devel.sha256sum] = "c3b2726c7f4de2a61861711ff8154933edfdb0aac5c02d8604244ce0c812ece3"
SRC_URI[frei0r-plugins.sha256sum] = "86f2cef23557eb0181b8a9a82f6ce0ea8d81fea9b6a9b976c8a21b568a6436cf"
SRC_URI[frei0r-plugins-opencv.sha256sum] = "4c041859589e90d1d8c7f84ad8ce2e4762bc92aea3c6d6a8c9fcf4d7c23cf237"
