SUMMARY = "generated recipe based on soundtouch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_soundtouch = "libSoundTouch.so.2"
RPM_SONAME_REQ_soundtouch = "libSoundTouch.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_soundtouch = "glibc libgcc libstdc++"
RPM_SONAME_REQ_soundtouch-devel = "libSoundTouch.so.2"
RPROVIDES_soundtouch-devel = "soundtouch-dev (= 2.0.0)"
RDEPENDS_soundtouch-devel = "pkgconf-pkg-config soundtouch"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/soundtouch-2.0.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/soundtouch-devel-2.0.0-2.el8.x86_64.rpm \
          "

SRC_URI[soundtouch.sha256sum] = "ccce2b7c60067a3b1daef0bbf3c2a1ab34617f753568c9518c0079d0b6a6d98d"
SRC_URI[soundtouch-devel.sha256sum] = "6fb4ee1727cd16cf959ebe5d1377ce3f045afab1ebe4967df3cd93cf3688e070"
