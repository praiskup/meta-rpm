SUMMARY = "generated recipe based on perl-Term-Cap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Cap = "ncurses perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Term-Cap-1.17-395.el8.noarch.rpm \
          "

SRC_URI[perl-Term-Cap.sha256sum] = "6bbb721dd2c411c85c75f7477b14c54c776d78ee9b93557615e919ef47577440"
