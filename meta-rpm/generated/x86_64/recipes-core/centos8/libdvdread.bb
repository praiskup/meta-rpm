SUMMARY = "generated recipe based on libdvdread srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdvdread = "libdvdread.so.4"
RPM_SONAME_REQ_libdvdread = "libc.so.6 libdl.so.2"
RDEPENDS_libdvdread = "glibc"
RPM_SONAME_REQ_libdvdread-devel = "libdvdread.so.4"
RPROVIDES_libdvdread-devel = "libdvdread-dev (= 5.0.3)"
RDEPENDS_libdvdread-devel = "libdvdread pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdvdread-5.0.3-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdvdread-devel-5.0.3-9.el8.x86_64.rpm \
          "

SRC_URI[libdvdread.sha256sum] = "9b4009f3093690dda28a9294dc6d9a8ac79781135b604894046591238ab7353f"
SRC_URI[libdvdread-devel.sha256sum] = "e38644caa74a0d5c91ac83271488540209c68516f27d1b41d40f1dd29942f1f4"
