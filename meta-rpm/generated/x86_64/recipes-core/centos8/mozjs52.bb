SUMMARY = "generated recipe based on mozjs52 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native readline zlib"
RPM_SONAME_PROV_mozjs52 = "libmozjs-52.so.0"
RPM_SONAME_REQ_mozjs52 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mozjs52 = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_mozjs52-devel = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmozjs-52.so.0 libpthread.so.0 libreadline.so.7 libstdc++.so.6 libz.so.1"
RPROVIDES_mozjs52-devel = "mozjs52-dev (= 52.9.0)"
RDEPENDS_mozjs52-devel = "glibc libgcc libstdc++ mozjs52 pkgconf-pkg-config readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mozjs52-52.9.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mozjs52-devel-52.9.0-2.el8.x86_64.rpm \
          "

SRC_URI[mozjs52.sha256sum] = "a9b785560b2129af299a1baa00fceeeb7bcba99ce1b0e97e5277c672cc35cd75"
SRC_URI[mozjs52-devel.sha256sum] = "4fadf50faa046a4b132d94c25e1c9dfa4d00eacef630d24e7bec521111a9a787"
