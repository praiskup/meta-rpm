SUMMARY = "generated recipe based on paratype-pt-sans-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_paratype-pt-sans-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/paratype-pt-sans-caption-fonts-20141121-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/paratype-pt-sans-fonts-20141121-6.el8.noarch.rpm \
          "

SRC_URI[paratype-pt-sans-caption-fonts.sha256sum] = "f17e0b44a5f57eb55fb9bb4c3fa118c075256569fc11965f9ca047ef9179f385"
SRC_URI[paratype-pt-sans-fonts.sha256sum] = "2b0631ae933c52fb0e8469c061e6855c730deced8f2e26ad5298cb326b942ec8"
