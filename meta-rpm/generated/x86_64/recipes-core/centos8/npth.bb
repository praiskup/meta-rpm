SUMMARY = "generated recipe based on npth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_npth = "libnpth.so.0"
RPM_SONAME_REQ_npth = "libc.so.6 libpthread.so.0"
RDEPENDS_npth = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/npth-1.5-4.el8.x86_64.rpm \
          "

SRC_URI[npth.sha256sum] = "168ab5dbc86b836b8742b2e63eee51d074f1d790728e3d30b0c59fff93cf1d8d"
