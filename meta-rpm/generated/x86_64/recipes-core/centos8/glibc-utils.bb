SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gd libpng pkgconfig-native zlib"
RPM_SONAME_REQ_glibc-utils = "libc.so.6 libgd.so.3 libm.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_glibc-utils = "bash gd glibc libpng perl-interpreter zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glibc-utils-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-utils.sha256sum] = "2e2c400ccf7fc39cd25d9238dfae313741e467e1491d9ed39d42b91435578d88"
