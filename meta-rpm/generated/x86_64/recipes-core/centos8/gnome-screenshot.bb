SUMMARY = "generated recipe based on gnome-screenshot srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gdk-pixbuf glib-2.0 gtk+3 libcanberra libx11 libxext pkgconfig-native"
RPM_SONAME_REQ_gnome-screenshot = "libX11.so.6 libXext.so.6 libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6"
RDEPENDS_gnome-screenshot = "cairo gdk-pixbuf2 glib2 glibc gtk3 libX11 libXext libcanberra libcanberra-gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-screenshot-3.26.0-3.el8.x86_64.rpm \
          "

SRC_URI[gnome-screenshot.sha256sum] = "044ab126345465d96584ab66c33c4e19b757fae5298459dcc565b2c6dcbd697f"
