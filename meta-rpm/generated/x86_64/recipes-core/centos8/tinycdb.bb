SUMMARY = "generated recipe based on tinycdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_tinycdb = "libcdb.so.1"
RPM_SONAME_REQ_tinycdb = "libc.so.6 libcdb.so.1"
RDEPENDS_tinycdb = "glibc"
RPM_SONAME_REQ_tinycdb-devel = "libcdb.so.1"
RPROVIDES_tinycdb-devel = "tinycdb-dev (= 0.78)"
RDEPENDS_tinycdb-devel = "pkgconf-pkg-config tinycdb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tinycdb-0.78-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tinycdb-devel-0.78-9.el8.x86_64.rpm \
          "

SRC_URI[tinycdb.sha256sum] = "a869e080a4b4f6b94e0be04f25d8f0483518d4af43742d3f1d9caae5f49a28be"
SRC_URI[tinycdb-devel.sha256sum] = "23827f129be3ea146ad135ad8d7825aa88d24b2393f71a8d53c56ce8f749704f"
