SUMMARY = "generated recipe based on skkdic srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/skkdic-20170102-4.T1100.el8.noarch.rpm \
          "

SRC_URI[skkdic.sha256sum] = "114a604321a9661775fd9611763a830f68c36f0eae64abbc9e03d363209c62ca"
