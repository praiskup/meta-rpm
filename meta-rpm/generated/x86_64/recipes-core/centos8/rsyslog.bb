SUMMARY = "generated recipe based on rsyslog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl gnutls krb5-libs libestr libfastjson libgcc libgcrypt libgpg-error liblognorm libpq librdkafka librelp libuuid mariadb-connector-c net-snmp openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_rsyslog = "libc.so.6 libcurl.so.4 libdl.so.2 libestr.so.0 libfastjson.so.4 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libsystemd.so.0 libuuid.so.1 libz.so.1"
RDEPENDS_rsyslog = "bash glibc libcurl libestr libfastjson libgcc libuuid logrotate systemd systemd-libs zlib"
RPM_SONAME_REQ_rsyslog-crypto = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0"
RDEPENDS_rsyslog-crypto = "glibc libgcrypt libgpg-error rsyslog"
RPM_SONAME_REQ_rsyslog-elasticsearch = "libc.so.6 libcurl.so.4 libm.so.6"
RDEPENDS_rsyslog-elasticsearch = "glibc libcurl rsyslog"
RPM_SONAME_REQ_rsyslog-gnutls = "libc.so.6 libgnutls.so.30"
RDEPENDS_rsyslog-gnutls = "glibc gnutls rsyslog"
RPM_SONAME_REQ_rsyslog-gssapi = "libc.so.6 libgssapi_krb5.so.2"
RDEPENDS_rsyslog-gssapi = "glibc krb5-libs rsyslog"
RPM_SONAME_REQ_rsyslog-kafka = "libc.so.6 librdkafka.so.1"
RDEPENDS_rsyslog-kafka = "glibc librdkafka rsyslog"
RPM_SONAME_REQ_rsyslog-mmaudit = "libc.so.6"
RDEPENDS_rsyslog-mmaudit = "glibc rsyslog"
RPM_SONAME_REQ_rsyslog-mmjsonparse = "libc.so.6 libfastjson.so.4"
RDEPENDS_rsyslog-mmjsonparse = "glibc libfastjson rsyslog"
RPM_SONAME_REQ_rsyslog-mmkubernetes = "libc.so.6 libcurl.so.4 libfastjson.so.4 liblognorm.so.5"
RDEPENDS_rsyslog-mmkubernetes = "glibc libcurl libfastjson liblognorm rsyslog"
RPM_SONAME_REQ_rsyslog-mmnormalize = "libc.so.6 libfastjson.so.4 liblognorm.so.5"
RDEPENDS_rsyslog-mmnormalize = "glibc libfastjson liblognorm rsyslog"
RPM_SONAME_REQ_rsyslog-mmsnmptrapd = "libc.so.6"
RDEPENDS_rsyslog-mmsnmptrapd = "glibc rsyslog"
RPM_SONAME_REQ_rsyslog-mysql = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_rsyslog-mysql = "glibc mariadb-connector-c openssl-libs rsyslog zlib"
RPM_SONAME_REQ_rsyslog-pgsql = "libc.so.6 libpq.so.5"
RDEPENDS_rsyslog-pgsql = "glibc libpq rsyslog"
RPM_SONAME_REQ_rsyslog-relp = "libc.so.6 librelp.so.0"
RDEPENDS_rsyslog-relp = "glibc librelp rsyslog"
RPM_SONAME_REQ_rsyslog-snmp = "libc.so.6 libnetsnmp.so.35"
RDEPENDS_rsyslog-snmp = "glibc net-snmp-libs rsyslog"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-crypto-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-doc-8.1911.0-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-elasticsearch-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-gnutls-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-gssapi-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-kafka-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-mmaudit-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-mmjsonparse-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-mmkubernetes-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-mmnormalize-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-mmsnmptrapd-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-mysql-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-pgsql-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-relp-8.1911.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rsyslog-snmp-8.1911.0-3.el8.x86_64.rpm \
          "

SRC_URI[rsyslog.sha256sum] = "6cbbc1d3d8ce3db95f451fb066e62c15aaf50acd74747f3cdf81e3536e0db2f8"
SRC_URI[rsyslog-crypto.sha256sum] = "8fdf181d2e7626acdd65a9cf06cda5ef438407da1f2b05863c3581da4e14e840"
SRC_URI[rsyslog-doc.sha256sum] = "36d0e89ef9f5573401ae9d9c1d25c9f71cf4da8f88334fded0be1a5e68ac64ee"
SRC_URI[rsyslog-elasticsearch.sha256sum] = "2ce6a12aea3cac8a911b9e8af41540ee4bb823d79cb6b9badc55f36e8235f3ea"
SRC_URI[rsyslog-gnutls.sha256sum] = "dd2315c960929a98d94cb760b03825ae644e0c7f6600c7f380b8dcf529369508"
SRC_URI[rsyslog-gssapi.sha256sum] = "9442d9dcf6f3d5a86c6735b0e5507c85ccbba22ee7ef338c18657efa9a269f52"
SRC_URI[rsyslog-kafka.sha256sum] = "ee809b45b1b1e3db684526314e22bc4909cac3e944bd934880abd89a26e635b4"
SRC_URI[rsyslog-mmaudit.sha256sum] = "a36f2fd48ea9cc9cb975f117dd934e5b835825b792332921f092504838ba38ce"
SRC_URI[rsyslog-mmjsonparse.sha256sum] = "64f6f4bc1b92e2dddf5f2bd8e2c27850e94e8ca00ce790673b43e6b3c1e45e15"
SRC_URI[rsyslog-mmkubernetes.sha256sum] = "f2aee8c98cdc6457aebcb84b3b8880a14c59dd95032f23237325cb649345862a"
SRC_URI[rsyslog-mmnormalize.sha256sum] = "fbf94613c9fedf5f02d3baaa2a77e75a74f0dc38a51d870a0f2ffa2bb7701944"
SRC_URI[rsyslog-mmsnmptrapd.sha256sum] = "b55e36309ec37b864def68aa3bdac1250e1ca1a971fb9b026ccb592b48efe52a"
SRC_URI[rsyslog-mysql.sha256sum] = "9cbd7c39ed1359fef41f2ef05a4293756af0b79c32b0c73999fcdec82ede9cbf"
SRC_URI[rsyslog-pgsql.sha256sum] = "dc6f87a765a41ddd49a1f93e498a4288f10aa94ccf293aefc84ea8b6bbcf4dac"
SRC_URI[rsyslog-relp.sha256sum] = "c7e8afc4f7b8d1245701c1962f447522621bc600daf06e5c67a670d5e8ae9da1"
SRC_URI[rsyslog-snmp.sha256sum] = "e12d09d5db26a00948bf04ec101937d862f0aa5b9a304d3b99a34bcdb3121011"
