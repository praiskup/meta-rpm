SUMMARY = "generated recipe based on libsndfile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "flac gsm libogg libvorbis pkgconfig-native"
RPM_SONAME_PROV_libsndfile = "libsndfile.so.1"
RPM_SONAME_REQ_libsndfile = "libFLAC.so.8 libc.so.6 libgsm.so.1 libm.so.6 libogg.so.0 libvorbis.so.0 libvorbisenc.so.2"
RDEPENDS_libsndfile = "flac-libs glibc gsm libogg libvorbis"
RPM_SONAME_REQ_libsndfile-devel = "libsndfile.so.1"
RPROVIDES_libsndfile-devel = "libsndfile-dev (= 1.0.28)"
RDEPENDS_libsndfile-devel = "libsndfile pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsndfile-1.0.28-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsndfile-devel-1.0.28-10.el8.x86_64.rpm \
          "

SRC_URI[libsndfile.sha256sum] = "92cdeebb8bdaa534e63c7d1d89ad2095da2e08d3138df51588c4210be9187be4"
SRC_URI[libsndfile-devel.sha256sum] = "6ae0513fe1011918e3953ea2376a694e02a7fc4330b4275db21ef74fbda34471"
