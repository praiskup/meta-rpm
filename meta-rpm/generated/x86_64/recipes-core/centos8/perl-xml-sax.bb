SUMMARY = "generated recipe based on perl-XML-SAX srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-SAX = "bash perl-Carp perl-Encode perl-Exporter perl-File-Temp perl-IO perl-PathTools perl-XML-NamespaceSupport perl-XML-SAX-Base perl-constant perl-interpreter perl-libs perl-libwww-perl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-XML-SAX-1.00-1.el8.noarch.rpm \
          "

SRC_URI[perl-XML-SAX.sha256sum] = "8b989251c00b0a311493a44fa27b4ae8adb4398ef3a44936861729596da19fe1"
