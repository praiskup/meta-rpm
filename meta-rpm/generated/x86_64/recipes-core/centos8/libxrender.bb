SUMMARY = "generated recipe based on libXrender srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXrender = "libXrender.so.1"
RPM_SONAME_REQ_libXrender = "libX11.so.6 libc.so.6"
RDEPENDS_libXrender = "glibc libX11"
RPM_SONAME_REQ_libXrender-devel = "libXrender.so.1"
RPROVIDES_libXrender-devel = "libXrender-dev (= 0.9.10)"
RDEPENDS_libXrender-devel = "libX11-devel libXrender pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXrender-0.9.10-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXrender-devel-0.9.10-7.el8.x86_64.rpm \
          "

SRC_URI[libXrender.sha256sum] = "11ac209220f3a53a762adebb4eeb43190e02ef7cdad2c54bcb474b206f7eb6f2"
SRC_URI[libXrender-devel.sha256sum] = "c82c8fca7ee2734ba22067446fe83ef8d3da478cc8846e9c1a7e0f3073ddac1c"
