SUMMARY = "generated recipe based on gperf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gperf = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gperf = "bash glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gperf-3.1-5.el8.x86_64.rpm \
          "

SRC_URI[gperf.sha256sum] = "ddf09cf0962cde7e9bb08ca09bc268b803d73998372c95b151384cbfe1799948"
