SUMMARY = "generated recipe based on libcap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libcap = "libcap.so.2"
RPM_SONAME_REQ_libcap = "libc.so.6 libcap.so.2"
RDEPENDS_libcap = "glibc"
RPM_SONAME_REQ_libcap-devel = "libcap.so.2"
RPROVIDES_libcap-devel = "libcap-dev (= 2.26)"
RDEPENDS_libcap-devel = "libcap pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcap-2.26-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcap-devel-2.26-3.el8.x86_64.rpm \
          "

SRC_URI[libcap.sha256sum] = "f0fdadb1de8a971856e29261597811710ad2f69b6629d09dfcf88437b64c6f35"
SRC_URI[libcap-devel.sha256sum] = "089da960050577e87c2530b7cfd2b62b9bd915978283463a05dc552143f8eb5c"
