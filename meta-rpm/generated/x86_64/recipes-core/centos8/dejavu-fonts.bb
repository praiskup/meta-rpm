SUMMARY = "generated recipe based on dejavu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_dejavu-fonts-common = "fontpackages-filesystem"
RDEPENDS_dejavu-sans-fonts = "dejavu-fonts-common"
RDEPENDS_dejavu-sans-mono-fonts = "dejavu-fonts-common"
RDEPENDS_dejavu-serif-fonts = "dejavu-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dejavu-fonts-common-2.35-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dejavu-sans-fonts-2.35-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dejavu-sans-mono-fonts-2.35-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dejavu-serif-fonts-2.35-6.el8.noarch.rpm \
          "

SRC_URI[dejavu-fonts-common.sha256sum] = "0a5aed36d8e0077014bc8bf6b750dd1bf91aa89e603a1c3bc0f6833250b4a86a"
SRC_URI[dejavu-sans-fonts.sha256sum] = "cf8331f00896887011b25e3f3a604022ab45cdc29491c81348ab82d9bc8acfd0"
SRC_URI[dejavu-sans-mono-fonts.sha256sum] = "2a90749400b27bf558e60d1a64d69e76fe7f9168b9213d12525e0f5bad0e1179"
SRC_URI[dejavu-serif-fonts.sha256sum] = "4ab1450b73454cfcbfb3b0d3d99ee6ed8413f0e2091ab1f35a6e99ef0bc6a847"
