SUMMARY = "generated recipe based on hyphen-ia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ia = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-ia-0.20050628-14.el8.noarch.rpm \
          "

SRC_URI[hyphen-ia.sha256sum] = "8ab86c49c8261cdb859f4f4cf01c9e4a40d182f78976ccd4f01f2af1ec089a7c"
