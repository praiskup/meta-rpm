SUMMARY = "generated recipe based on perl-LWP-MediaTypes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-LWP-MediaTypes = "mailcap perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-LWP-MediaTypes-6.02-14.el8.noarch.rpm \
          "

SRC_URI[perl-LWP-MediaTypes.sha256sum] = "46beca0a72ea64cafee2dc6c620422a79422aa4662fd5f76f00c62f939c5b704"
