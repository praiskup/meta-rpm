SUMMARY = "generated recipe based on hunspell-as srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-as = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-as-1.0.3-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-as.sha256sum] = "4725698b92ee51530fd47083d8e116947af1131f0675f590b409f4efc230eb6f"
