SUMMARY = "generated recipe based on qt5-qtsvg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase zlib"
RPM_SONAME_PROV_qt5-qtsvg = "libQt5Svg.so.5 libqsvg.so libqsvgicon.so"
RPM_SONAME_REQ_qt5-qtsvg = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qtsvg = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui zlib"
RPM_SONAME_REQ_qt5-qtsvg-devel = "libQt5Svg.so.5"
RPROVIDES_qt5-qtsvg-devel = "qt5-qtsvg-dev (= 5.12.5)"
RDEPENDS_qt5-qtsvg-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtsvg"
RPM_SONAME_REQ_qt5-qtsvg-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtsvg-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtsvg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtsvg-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtsvg-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtsvg-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtsvg.sha256sum] = "32a70b994a0342102ed3f8d5b2d108495d1c5d81ff45fb2b0174c3de53f6fd1f"
SRC_URI[qt5-qtsvg-devel.sha256sum] = "874f55c0fb37a42d844de7d327b22af860e2aabe8c5c11663a1c711508f9a72d"
SRC_URI[qt5-qtsvg-examples.sha256sum] = "800e8b4ca7537f3aecbe57ea8537048de516d1fce1eb1094dcebd004ea4f0114"
