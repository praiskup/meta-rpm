SUMMARY = "generated recipe based on perl-DateTime srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-DateTime = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-DateTime = "glibc perl-Carp perl-DateTime-Locale perl-DateTime-TimeZone perl-Dist-CheckConflicts perl-Params-ValidationCompiler perl-Scalar-List-Utils perl-Specio perl-Try-Tiny perl-interpreter perl-libs perl-namespace-autoclean perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-1.50-1.el8.x86_64.rpm \
          "

SRC_URI[perl-DateTime.sha256sum] = "e80b901d3a6cf4a3bf92b7580cf0ecd181465fe29ee64327aaff682d77cfde9a"
