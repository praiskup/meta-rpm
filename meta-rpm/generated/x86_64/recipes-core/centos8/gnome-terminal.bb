SUMMARY = "generated recipe based on gnome-terminal srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dconf gdk-pixbuf glib-2.0 gnutls gtk+3 libgcc libpcre2 libuuid libx11 nautilus pango pkgconfig-native vte291 zlib"
RPM_SONAME_REQ_gnome-terminal = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdconf.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpcre2-8.so.0 libpthread.so.0 libuuid.so.1 libvte-2.91.so.0 libz.so.1"
RDEPENDS_gnome-terminal = "atk cairo cairo-gobject dbus-x11 dconf gdk-pixbuf2 glib2 glibc gnutls gsettings-desktop-schemas gtk3 libX11 libgcc libuuid pango pcre2 vte291 zlib"
RPM_SONAME_PROV_gnome-terminal-nautilus = "libterminal-nautilus.so"
RPM_SONAME_REQ_gnome-terminal-nautilus = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnautilus-extension.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gnome-terminal-nautilus = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnome-terminal gtk3 libgcc nautilus-extensions pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-terminal-3.28.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-terminal-nautilus-3.28.3-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-terminal.sha256sum] = "691dee162d385c11a4913b4c9e764742d23e224288d36f5b250da3cb96420039"
SRC_URI[gnome-terminal-nautilus.sha256sum] = "4ba323630144c47a4c5b8e2ca437572a6a0a8ad9737849868823d3abb82d730d"
