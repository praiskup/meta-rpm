SUMMARY = "generated recipe based on zip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 pkgconfig-native"
RPM_SONAME_REQ_zip = "libbz2.so.1 libc.so.6"
RDEPENDS_zip = "bzip2-libs glibc unzip"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/zip-3.0-23.el8.x86_64.rpm \
          "

SRC_URI[zip.sha256sum] = "2cb5001e7ee5d9ad7b957dcae806739d7d6482c4ae4dd30c5ac506c66149eee2"
