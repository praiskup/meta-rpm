SUMMARY = "generated recipe based on libyaml srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libyaml = "libyaml-0.so.2"
RPM_SONAME_REQ_libyaml = "libc.so.6"
RDEPENDS_libyaml = "glibc"
RPROVIDES_libyaml-devel = "libyaml-dev (= 0.1.7)"
RDEPENDS_libyaml-devel = "libyaml pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libyaml-0.1.7-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libyaml-devel-0.1.7-5.el8.x86_64.rpm \
          "

SRC_URI[libyaml.sha256sum] = "00d537a434b1c2896dada83deb359d71fd005772031c73499c72f2cbd34521c5"
SRC_URI[libyaml-devel.sha256sum] = "1a82747d05d8cd782d940fd108758149b796902eb1e53c35987c0d1de3e9b59f"
