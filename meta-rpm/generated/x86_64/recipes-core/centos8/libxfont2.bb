SUMMARY = "generated recipe based on libXfont2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libfontenc pkgconfig-native xorg-x11-proto-devel zlib"
RPM_SONAME_PROV_libXfont2 = "libXfont2.so.2"
RPM_SONAME_REQ_libXfont2 = "libc.so.6 libfontenc.so.1 libfreetype.so.6 libm.so.6 libz.so.1"
RDEPENDS_libXfont2 = "freetype glibc libfontenc zlib"
RPM_SONAME_REQ_libXfont2-devel = "libXfont2.so.2"
RPROVIDES_libXfont2-devel = "libXfont2-dev (= 2.0.3)"
RDEPENDS_libXfont2-devel = "freetype-devel libXfont2 libfontenc-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXfont2-2.0.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libXfont2-devel-2.0.3-2.el8.x86_64.rpm \
          "

SRC_URI[libXfont2.sha256sum] = "5f8656a50db1561fef06ef5dcb5ce10fb400b8294794dc61d097b7f133c78277"
SRC_URI[libXfont2-devel.sha256sum] = "2e649481831f89e5cd9ea722636f2984ca14131ce428e5a971533722b79519d0"
