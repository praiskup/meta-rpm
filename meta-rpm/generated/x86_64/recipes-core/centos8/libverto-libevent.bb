SUMMARY = "generated recipe based on libverto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevent pkgconfig-native"
RPM_SONAME_PROV_libverto-libevent = "libverto-libevent.so.1"
RPM_SONAME_REQ_libverto-libevent = "libc.so.6 libdl.so.2 libevent-2.1.so.6 libpthread.so.0 libverto.so.1"
RDEPENDS_libverto-libevent = "glibc libevent libverto"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libverto-libevent-0.3.0-5.el8.x86_64.rpm \
          "

SRC_URI[libverto-libevent.sha256sum] = "538899f9a39e085ef14bd34f84327c12b862981df1428bb1353ef4082ddd3a4c"
