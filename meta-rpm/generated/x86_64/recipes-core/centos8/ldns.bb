SUMMARY = "generated recipe based on ldns srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_ldns = "libldns.so.2"
RPM_SONAME_REQ_ldns = "libc.so.6 libcrypto.so.1.1 libssl.so.1.1"
RDEPENDS_ldns = "ca-certificates glibc openssl-libs"
RPM_SONAME_REQ_ldns-devel = "libldns.so.2"
RPROVIDES_ldns-devel = "ldns-dev (= 1.7.0)"
RDEPENDS_ldns-devel = "bash ldns openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ldns-1.7.0-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ldns-devel-1.7.0-21.el8.x86_64.rpm \
          "

SRC_URI[ldns.sha256sum] = "27a2e1408b5b76355e36e9171d0c9d990f359ee8de451a157ebf85767f0bf261"
SRC_URI[ldns-devel.sha256sum] = "25a09a5cddc4727728d30bb46f5914eac316d9d820e56f51131c513adfad4ce1"
