SUMMARY = "generated recipe based on stratisd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs libgcc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_stratisd = "ld-linux-x86-64.so.2 libc.so.6 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libudev.so.1"
RDEPENDS_stratisd = "bash dbus-libs device-mapper-persistent-data glibc libgcc systemd-libs xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/stratisd-2.0.0-4.el8.x86_64.rpm \
          "

SRC_URI[stratisd.sha256sum] = "7bdf6eded70bbe2e49e01729c94d2a5b9f6f0c3864eeb38c12fdffa03f08c515"
