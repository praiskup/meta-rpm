SUMMARY = "generated recipe based on bind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db e2fsprogs krb5-libs libcap libidn2 libmaxminddb libpq libxml2 mariadb-connector-c openldap openssl pkgconfig-native sqlite3 xz zlib"
RPM_SONAME_REQ_bind = "libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libisccc.so.161 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblwres.so.161 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind = "bash bind-libs bind-libs-lite coreutils glibc glibc-common grep krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs shadow-utils systemd xz-libs zlib"
RDEPENDS_bind-chroot = "bash bind grep"
RPM_SONAME_REQ_bind-devel = "libbind9.so.161 libisccc.so.161 liblwres.so.161"
RPROVIDES_bind-devel = "bind-dev (= 9.11.13)"
RDEPENDS_bind-devel = "bash bind-libs bind-lite-devel"
RPM_SONAME_REQ_bind-export-devel = "libdns-export.so.1107 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163"
RPROVIDES_bind-export-devel = "bind-export-dev (= 9.11.13)"
RDEPENDS_bind-export-devel = "bash bind-export-libs libcap-devel openssl-devel"
RPM_SONAME_PROV_bind-export-libs = "libdns-export.so.1107 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163"
RPM_SONAME_REQ_bind-export-libs = "libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdns-export.so.1107 libgssapi_krb5.so.2 libisc-export.so.1104 libk5crypto.so.3 libkrb5.so.3"
RDEPENDS_bind-export-libs = "glibc krb5-libs libcap libcom_err openssl-libs"
RPM_SONAME_PROV_bind-libs = "libbind9.so.161 libisccc.so.161 liblwres.so.161"
RPM_SONAME_REQ_bind-libs = "libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-libs = "bind-libs-lite bind-license glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_PROV_bind-libs-lite = "libdns.so.1107 libirs.so.161 libisc.so.1104 libisccfg.so.163"
RPM_SONAME_REQ_bind-libs-lite = "libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-libs-lite = "bind-license glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_REQ_bind-lite-devel = "libdns.so.1107 libirs.so.161 libisc.so.1104 libisccfg.so.163"
RPROVIDES_bind-lite-devel = "bind-lite-dev (= 9.11.13)"
RDEPENDS_bind-lite-devel = "bind-libs-lite libmaxminddb-devel libxml2-devel openssl-devel"
RPM_SONAME_REQ_bind-pkcs11 = "libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns-pkcs11.so.1107 libdns.so.1107 libgssapi_krb5.so.2 libisc-pkcs11.so.1104 libisc.so.1104 libisccc.so.161 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblwres.so.161 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-pkcs11 = "bash bind bind-libs bind-libs-lite bind-pkcs11-libs glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs systemd xz-libs zlib"
RPM_SONAME_REQ_bind-pkcs11-devel = "libdns-pkcs11.so.1107 libisc-pkcs11.so.1104"
RPROVIDES_bind-pkcs11-devel = "bind-pkcs11-dev (= 9.11.13)"
RDEPENDS_bind-pkcs11-devel = "bind-lite-devel bind-pkcs11-libs"
RPM_SONAME_PROV_bind-pkcs11-libs = "libdns-pkcs11.so.1107 libisc-pkcs11.so.1104"
RPM_SONAME_REQ_bind-pkcs11-libs = "libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libisc-pkcs11.so.1104 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-pkcs11-libs = "bind-libs bind-license glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_REQ_bind-pkcs11-utils = "libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns-pkcs11.so.1107 libgssapi_krb5.so.2 libisc-pkcs11.so.1104 libk5crypto.so.3 libkrb5.so.3 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-pkcs11-utils = "bind-pkcs11-libs glibc krb5-libs libcap libcom_err libmaxminddb libxml2 openssl-libs xz-libs zlib"
RPM_SONAME_REQ_bind-sdb = "libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libisc.so.1104 libisccc.so.161 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 liblwres.so.161 liblzma.so.5 libm.so.6 libmariadb.so.3 libmaxminddb.so.0 libpq.so.5 libpthread.so.0 libsqlite3.so.0 libssl.so.1.1 libxml2.so.2 libz.so.1"
RDEPENDS_bind-sdb = "bash bind bind-libs bind-libs-lite glibc krb5-libs libcap libcom_err libdb libmaxminddb libpq libxml2 mariadb-connector-c openldap openssl-libs sqlite-libs systemd xz-libs zlib"
RDEPENDS_bind-sdb-chroot = "bash bind-sdb grep"
RPM_SONAME_REQ_bind-utils = "libbind9.so.161 libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libdns.so.1107 libgssapi_krb5.so.2 libidn2.so.0 libirs.so.161 libisc.so.1104 libisccfg.so.163 libk5crypto.so.3 libkrb5.so.3 liblwres.so.161 liblzma.so.5 libm.so.6 libmaxminddb.so.0 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_bind-utils = "bind-libs bind-libs-lite glibc krb5-libs libcap libcom_err libidn2 libmaxminddb libxml2 openssl-libs platform-python python3-bind xz-libs zlib"
RDEPENDS_python3-bind = "bind-license platform-python python3-ply"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-chroot-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-devel-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-libs-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-libs-lite-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-license-9.11.13-6.el8_2.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-lite-devel-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-pkcs11-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-pkcs11-devel-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-pkcs11-libs-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-pkcs11-utils-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-sdb-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-sdb-chroot-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bind-utils-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-bind-9.11.13-6.el8_2.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bind-export-devel-9.11.13-6.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bind-export-libs-9.11.13-6.el8_2.1.x86_64.rpm \
          "

SRC_URI[bind.sha256sum] = "001c25b6f506824b58f5580ba6d93866893f77b91aeaa1fb51c5c7ed9826925a"
SRC_URI[bind-chroot.sha256sum] = "da8605e2c765c021d90e157fb58985ee00ef16579319bebc7239cd94e2e30fbc"
SRC_URI[bind-devel.sha256sum] = "e14472a6606a1c6da783816c68e715cbb95f6a408dd863fc6b8ad7ab21157c8e"
SRC_URI[bind-export-devel.sha256sum] = "7e9c10e35da2b5cab4a4d9b617460875a942bc604254ff9ff667f0ca841ff205"
SRC_URI[bind-export-libs.sha256sum] = "22e92191454b9cb71de37654394cb3b56bb1588866fd95eae6e9fa56f1eef513"
SRC_URI[bind-libs.sha256sum] = "03a2ae4395cfc2c97726705d07cbe682136ac8d6b4a1d932c2332eecea90a7d5"
SRC_URI[bind-libs-lite.sha256sum] = "f762c71a48c3a75840ad201be7bfedc0627d87ba5e33b6224535cb3e2a135035"
SRC_URI[bind-license.sha256sum] = "c8e1df6b56b388b06f5c05e01a5dae27086253295918e16d9d64932b9fbe28dc"
SRC_URI[bind-lite-devel.sha256sum] = "90d29a722145079642341de5da18c3864757c49975abea4c7cc7e588616ff5b1"
SRC_URI[bind-pkcs11.sha256sum] = "64b748b0df6e7fd0838e6c1c9dd23e00a66c54d056823258a357b432b74eb480"
SRC_URI[bind-pkcs11-devel.sha256sum] = "e622f796dc5f86ce1e372fd384e86fc7eb9cbc8dc747de764901d276bb022772"
SRC_URI[bind-pkcs11-libs.sha256sum] = "50e96be911a4040235a6db76b09bfc32ba14873eb03bdd6d27c1cac5d6b8a216"
SRC_URI[bind-pkcs11-utils.sha256sum] = "bacbadf2c148ea4bd43fa54fa554f31b46c52a6285a69a5d6ef38739cafe432d"
SRC_URI[bind-sdb.sha256sum] = "7a72fad82d93e5684eb61464e75c20cfa6e8f25846e0ae9e766cc32134fcb5e9"
SRC_URI[bind-sdb-chroot.sha256sum] = "e4c3243885c5da97da53a38b7b16287e1ff5a723a42df95f60afdc7284ca2ef9"
SRC_URI[bind-utils.sha256sum] = "f131e3d87b2fb5780528c9b6da07588c79e28f3a616e996418c8b9118e83a29d"
SRC_URI[python3-bind.sha256sum] = "c157d3483665cc48c6650cc6f16b849a4271ab34acd68768b9dd680f152a7688"
