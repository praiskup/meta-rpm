SUMMARY = "generated recipe based on opencryptoki srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openldap openssl pkgconfig-native trousers"
RPM_SONAME_REQ_opencryptoki = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0"
RDEPENDS_opencryptoki = "bash coreutils glibc libitm opencryptoki-libs opencryptoki-swtok openldap openssl-libs systemd"
RPROVIDES_opencryptoki-devel = "opencryptoki-dev (= 3.12.1)"
RDEPENDS_opencryptoki-devel = "opencryptoki-libs"
RPM_SONAME_PROV_opencryptoki-icsftok = "libpkcs11_icsf.so.0"
RPM_SONAME_REQ_opencryptoki-icsftok = "libc.so.6 libcrypto.so.1.1 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpkcs11_icsf.so.0 libpthread.so.0 librt.so.1 libssl.so.1.1"
RDEPENDS_opencryptoki-icsftok = "glibc libitm opencryptoki-libs openldap openssl-libs"
RPM_SONAME_PROV_opencryptoki-libs = "libopencryptoki.so.0"
RPM_SONAME_REQ_opencryptoki-libs = "libc.so.6 libdl.so.2 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libopencryptoki.so.0 libpthread.so.0"
RDEPENDS_opencryptoki-libs = "bash glibc libitm openldap shadow-utils"
RPM_SONAME_PROV_opencryptoki-swtok = "libpkcs11_sw.so.0"
RPM_SONAME_REQ_opencryptoki-swtok = "libc.so.6 libcrypto.so.1.1 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpkcs11_sw.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_opencryptoki-swtok = "glibc libitm opencryptoki-libs openldap openssl-libs"
RPM_SONAME_PROV_opencryptoki-tpmtok = "libpkcs11_tpm.so.0"
RPM_SONAME_REQ_opencryptoki-tpmtok = "libc.so.6 libcrypto.so.1.1 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpkcs11_tpm.so.0 libpthread.so.0 librt.so.1 libtspi.so.1"
RDEPENDS_opencryptoki-tpmtok = "glibc libitm opencryptoki-libs openldap openssl-libs trousers-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opencryptoki-3.12.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opencryptoki-icsftok-3.12.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opencryptoki-libs-3.12.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opencryptoki-swtok-3.12.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opencryptoki-tpmtok-3.12.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opencryptoki-devel-3.12.1-2.el8.x86_64.rpm \
          "

SRC_URI[opencryptoki.sha256sum] = "e766a35d78c06787701027087ca9e903cd58ab2453af83d5bdc825ebc8d3ad4e"
SRC_URI[opencryptoki-devel.sha256sum] = "3d83801a903ef059c7d52e5d1e7fdfcf53cf111eac6c17aa3aee741a89ae7e50"
SRC_URI[opencryptoki-icsftok.sha256sum] = "8f85898d08bfc17edb708447cf16cf4b7764131db33147a89d7cad68b57c6adc"
SRC_URI[opencryptoki-libs.sha256sum] = "cd12da87c9f09e8df641d407655cb902c70f77ed9e5dc280f4efcd37959620a4"
SRC_URI[opencryptoki-swtok.sha256sum] = "591c388803d409ec62c87f4026dc71f7314ad95b302de3fdd1273f0a76d0d75f"
SRC_URI[opencryptoki-tpmtok.sha256sum] = "d8541ed263cc9729febc52e69fc50057d7c2087d6aecc1e6d3295c68a12ecd68"
