SUMMARY = "generated recipe based on hyphen-or srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-or = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-or-0.7.0-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-or.sha256sum] = "d130127f547353a59f4086192e81cb9b780cd4de89069088c13071a30b5843f2"
