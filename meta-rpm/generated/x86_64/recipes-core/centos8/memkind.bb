SUMMARY = "generated recipe based on memkind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "numactl pkgconfig-native"
RPM_SONAME_PROV_memkind = "libautohbw.so.0 libmemkind.so.0"
RPM_SONAME_REQ_memkind = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libmemkind.so.0 libnuma.so.1 libpthread.so.0"
RDEPENDS_memkind = "glibc numactl-libs"
RPM_SONAME_REQ_memkind-devel = "libautohbw.so.0 libmemkind.so.0"
RPROVIDES_memkind-devel = "memkind-dev (= 1.9.0)"
RDEPENDS_memkind-devel = "memkind pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/memkind-1.9.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/memkind-devel-1.9.0-1.el8.x86_64.rpm \
          "

SRC_URI[memkind.sha256sum] = "042517c17f4b5695f0ecce164647eaae7e3f651be4516e4d348d877f05d806fe"
SRC_URI[memkind-devel.sha256sum] = "53c6e97196d841773154da4a72c320a59ab6f6fa1a419bb58783212e8072d731"
