SUMMARY = "generated recipe based on bpftrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bcc clang elfutils libgcc llvm pkgconfig-native"
RPM_SONAME_REQ_bpftrace = "libLLVM-9.so libbcc.so.0 libc.so.6 libclang.so.9 libelf.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_bpftrace = "bcc clang-libs elfutils-libelf glibc libgcc libstdc++ llvm-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpftrace-0.9.2-1.el8.x86_64.rpm \
          "

SRC_URI[bpftrace.sha256sum] = "0ae991250faeeeac0b41a694afc5482224d6ceec67c2dbcf093c89e1d260ceae"
