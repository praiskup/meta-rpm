SUMMARY = "generated recipe based on gl-manpages srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gl-manpages-1.1-15.20161227.el8.noarch.rpm \
          "

SRC_URI[gl-manpages.sha256sum] = "76839b90d6a4bd76327614c7ca21852239a92920a839644dcb082a29c50f500d"
