SUMMARY = "generated recipe based on libXi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxfixes pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXi = "libXi.so.6"
RPM_SONAME_REQ_libXi = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXi = "glibc libX11 libXext"
RPM_SONAME_REQ_libXi-devel = "libXi.so.6"
RPROVIDES_libXi-devel = "libXi-dev (= 1.7.9)"
RDEPENDS_libXi-devel = "libX11-devel libXext-devel libXfixes-devel libXi pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXi-1.7.9-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXi-devel-1.7.9-7.el8.x86_64.rpm \
          "

SRC_URI[libXi.sha256sum] = "d89b7f2d2e872f72d6f25752ab0eb923876ada8e80f22a7e2367c6fe844b71df"
SRC_URI[libXi-devel.sha256sum] = "a6aa3aa44cf30633c8cce02e9af92da9a81c48242b0fb308fb76bcadf6cde595"
