SUMMARY = "generated recipe based on less srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_less = "libc.so.6 libtinfo.so.6"
RDEPENDS_less = "bash glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/less-530-1.el8.x86_64.rpm \
          "

SRC_URI[less.sha256sum] = "f94172554b8ceeab97b560d0b05c2e2df4b2e737471adce6eca82fd3209be254"
