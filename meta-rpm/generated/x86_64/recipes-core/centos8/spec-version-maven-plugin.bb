SUMMARY = "generated recipe based on spec-version-maven-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_spec-version-maven-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib plexus-resources"
RDEPENDS_spec-version-maven-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/spec-version-maven-plugin-1.2-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/spec-version-maven-plugin-javadoc-1.2-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[spec-version-maven-plugin.sha256sum] = "6e1ddec982b6a9222f7bf44f87731300eef787a9155c0ee850f6aee61b10aa7b"
SRC_URI[spec-version-maven-plugin-javadoc.sha256sum] = "e2fdcf958392bfbc0dbf1d48994167b1de9bf8f59f84cea0fe5b967b8bd646de"
