SUMMARY = "generated recipe based on gnome-logs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native systemd-libs"
RPM_SONAME_REQ_gnome-logs = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_gnome-logs = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gsettings-desktop-schemas gtk3 pango systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-logs-3.28.5-3.el8.x86_64.rpm \
          "

SRC_URI[gnome-logs.sha256sum] = "57a80f00496d55fa6c0ead5043d5850b0aec04b5d34174a8feec9e2a806b8928"
