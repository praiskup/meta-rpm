SUMMARY = "generated recipe based on leptonica srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "giflib libjpeg-turbo libpng libwebp pkgconfig-native tiff zlib"
RPM_SONAME_PROV_leptonica = "liblept.so.5"
RPM_SONAME_REQ_leptonica = "libc.so.6 libgif.so.7 libjpeg.so.62 libm.so.6 libpng16.so.16 libtiff.so.5 libwebp.so.7 libz.so.1"
RDEPENDS_leptonica = "giflib glibc libjpeg-turbo libpng libtiff libwebp zlib"
RPM_SONAME_REQ_leptonica-devel = "liblept.so.5"
RPROVIDES_leptonica-devel = "leptonica-dev (= 1.76.0)"
RDEPENDS_leptonica-devel = "leptonica pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/leptonica-1.76.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/leptonica-devel-1.76.0-2.el8.x86_64.rpm \
          "

SRC_URI[leptonica.sha256sum] = "df86bd9516bdf3bd3e04f1b20bd8930a7040a09d6e5dbe5dfeabb4020682d357"
SRC_URI[leptonica-devel.sha256sum] = "60861be6a1f33de30dfc3978c2e4b95401a5362896e5e5c9868cf1ad0ff03224"
