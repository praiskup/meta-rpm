SUMMARY = "generated recipe based on psacct srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_psacct = "libc.so.6 libm.so.6"
RDEPENDS_psacct = "bash chkconfig coreutils glibc info systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/psacct-6.6.3-4.el8.x86_64.rpm \
          "

SRC_URI[psacct.sha256sum] = "f50793dddea3f794abfe33f2e0a2ae0aac97fafdb55599fa2c8897970980be65"
