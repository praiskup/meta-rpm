SUMMARY = "generated recipe based on mt-st srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mt-st = "libc.so.6"
RDEPENDS_mt-st = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mt-st-1.1-24.el8.x86_64.rpm \
          "

SRC_URI[mt-st.sha256sum] = "8d20460c2a9b9b36a2836f05694f724271ed4d223e0e161930cc5501d38711e1"
