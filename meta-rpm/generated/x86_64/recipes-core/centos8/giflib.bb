SUMMARY = "generated recipe based on giflib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_giflib = "libgif.so.7"
RPM_SONAME_REQ_giflib = "libc.so.6"
RDEPENDS_giflib = "glibc"
RPM_SONAME_REQ_giflib-devel = "libgif.so.7"
RPROVIDES_giflib-devel = "giflib-dev (= 5.1.4)"
RDEPENDS_giflib-devel = "giflib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/giflib-5.1.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/giflib-devel-5.1.4-3.el8.x86_64.rpm \
          "

SRC_URI[giflib.sha256sum] = "4f8c167bb6d5acd9d172072eab7594c3f6f624cdb9de77ac31162e92de073d9b"
SRC_URI[giflib-devel.sha256sum] = "e1d3fbe89cf1248593c846f5db7bc83cce89aa21d1d0d9c4e62551f1b15dde11"
