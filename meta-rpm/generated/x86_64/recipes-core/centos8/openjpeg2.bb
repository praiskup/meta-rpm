SUMMARY = "generated recipe based on openjpeg2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lcms2 libpng pkgconfig-native tiff zlib"
RPM_SONAME_PROV_openjpeg2 = "libopenjp2.so.7"
RPM_SONAME_REQ_openjpeg2 = "libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_openjpeg2 = "glibc"
RPM_SONAME_REQ_openjpeg2-devel = "libopenjp2.so.7"
RPROVIDES_openjpeg2-devel = "openjpeg2-dev (= 2.3.1)"
RDEPENDS_openjpeg2-devel = "openjpeg2 openjpeg2-tools pkgconf-pkg-config"
RPM_SONAME_REQ_openjpeg2-tools = "libc.so.6 liblcms2.so.2 libm.so.6 libopenjp2.so.7 libpng16.so.16 libpthread.so.0 librt.so.1 libtiff.so.5 libz.so.1"
RDEPENDS_openjpeg2-tools = "glibc lcms2 libpng libtiff openjpeg2 zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openjpeg2-2.3.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openjpeg2-devel-docs-2.3.1-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openjpeg2-tools-2.3.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openjpeg2-devel-2.3.1-6.el8.x86_64.rpm \
          "

SRC_URI[openjpeg2.sha256sum] = "0d145fb07cab391978769555fcc1c5e8631762dd228e2b73948d69ed8564e830"
SRC_URI[openjpeg2-devel.sha256sum] = "0153038dcaca8449154db71a844da6d0faff976e97f0be93c4f6c2439f5acd34"
SRC_URI[openjpeg2-devel-docs.sha256sum] = "9a40317d1eccfb66639e65008c3d201df5cfc53225a45c9770dfd2edd398bf09"
SRC_URI[openjpeg2-tools.sha256sum] = "b9e43abce5df1c11a6451324ca28800053c93886807c27574a304cff809e73cc"
