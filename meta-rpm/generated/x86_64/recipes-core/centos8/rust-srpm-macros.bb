SUMMARY = "generated recipe based on rust-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rust-srpm-macros-5-2.el8.noarch.rpm \
          "

SRC_URI[rust-srpm-macros.sha256sum] = "c96962e21d09c28993b3a37c0c808e63486c3c06b1a22b749ca938b4bfaa2e7a"
