SUMMARY = "generated recipe based on freerdp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib cups-libs glib-2.0 gsm gstreamer1.0 gstreamer1.0-plugins-base libjpeg-turbo libx11 libxcursor libxext libxfixes libxi libxinerama libxkbcommon libxkbfile libxrandr libxrender libxv openssl pkgconfig-native pulseaudio systemd-libs wayland"
RPM_SONAME_REQ_freerdp = "libX11.so.6 libXcursor.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libXv.so.1 libc.so.6 libfreerdp-client2.so.2 libfreerdp2.so.2 libm.so.6 librt.so.1 libuwac0.so.0 libwinpr-tools2.so.2 libwinpr2.so.2"
RDEPENDS_freerdp = "freerdp-libs glibc libX11 libXcursor libXext libXfixes libXi libXinerama libXrandr libXrender libXv libwinpr"
RPM_SONAME_REQ_freerdp-devel = "libfreerdp-client2.so.2 libfreerdp2.so.2 libuwac0.so.0"
RPROVIDES_freerdp-devel = "freerdp-dev (= 2.0.0)"
RDEPENDS_freerdp-devel = "cmake cmake-filesystem freerdp-libs libwinpr-devel libxkbcommon-devel openssl-devel pkgconf-pkg-config wayland-devel"
RPM_SONAME_PROV_freerdp-libs = "libaudin-client-alsa.so libaudin-client-oss.so libaudin-client-pulse.so libaudin-client.so libdisp-client.so libdrive-client.so libecho-client.so libfreerdp-client2.so.2 libfreerdp2.so.2 libgeometry-client.so libparallel-client.so libprinter-client.so librdpei-client.so librdpgfx-client.so librdpsnd-client-alsa.so librdpsnd-client-fake.so librdpsnd-client-oss.so librdpsnd-client-pulse.so libserial-client.so libtsmf-client-alsa-audio.so libtsmf-client-gstreamer-decoder.so libtsmf-client-oss-audio.so libtsmf-client-pulse-audio.so libtsmf-client.so libuwac0.so.0 libvideo-client.so"
RPM_SONAME_REQ_freerdp-libs = "ld-linux-x86-64.so.2 libX11.so.6 libXext.so.6 libasound.so.2 libc.so.6 libcrypto.so.1.1 libcups.so.2 libfreerdp2.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libgsm.so.1 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libjpeg.so.62 libpthread.so.0 libpulse.so.0 librt.so.1 libssl.so.1.1 libwayland-client.so.0 libwinpr2.so.2 libxkbcommon.so.0 libxkbfile.so.1"
RDEPENDS_freerdp-libs = "alsa-lib cups-libs glib2 glibc gsm gstreamer1 gstreamer1-plugins-base libX11 libXext libjpeg-turbo libwayland-client libwinpr libxkbcommon libxkbfile openssl-libs pulseaudio-libs"
RPM_SONAME_PROV_libwinpr = "libwinpr-tools2.so.2 libwinpr2.so.2"
RPM_SONAME_REQ_libwinpr = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libsystemd.so.0 libwinpr2.so.2"
RDEPENDS_libwinpr = "glibc openssl-libs systemd-libs"
RPM_SONAME_REQ_libwinpr-devel = "libwinpr-tools2.so.2 libwinpr2.so.2"
RPROVIDES_libwinpr-devel = "libwinpr-dev (= 2.0.0)"
RDEPENDS_libwinpr-devel = "cmake cmake-filesystem libwinpr openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/freerdp-2.0.0-46.rc4.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/freerdp-libs-2.0.0-46.rc4.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwinpr-2.0.0-46.rc4.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwinpr-devel-2.0.0-46.rc4.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/freerdp-devel-2.0.0-46.rc4.el8_2.2.x86_64.rpm \
          "

SRC_URI[freerdp.sha256sum] = "028441225db6bc467ef335b3929e55aa7afd53eaca3bce9d92b97214267123ac"
SRC_URI[freerdp-devel.sha256sum] = "04396e354d60589a4ffd346947ee80be9ebd7ee46ecf5ef397b88a82847b3d99"
SRC_URI[freerdp-libs.sha256sum] = "02e588642b8681fe303ccc5cb7f5fa3194560136d3de61fee6e118e4ccd60655"
SRC_URI[libwinpr.sha256sum] = "29a992cd41226c0539df3258386a8eb2872d1d8cab19dcf4f80556ad96570c17"
SRC_URI[libwinpr-devel.sha256sum] = "c688cf3fe508f1a4502c4abe6de78fb444b734d2d7354640aa5c3b9c991eef9e"
