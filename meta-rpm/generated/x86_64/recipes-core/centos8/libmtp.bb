SUMMARY = "generated recipe based on libmtp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcrypt libusb1 pkgconfig-native"
RPM_SONAME_PROV_libmtp = "libmtp.so.9"
RPM_SONAME_REQ_libmtp = "libc.so.6 libgcrypt.so.20 libmtp.so.9 libusb-1.0.so.0"
RDEPENDS_libmtp = "glibc libgcrypt libusbx systemd-udev"
RPM_SONAME_REQ_libmtp-devel = "libmtp.so.9"
RPROVIDES_libmtp-devel = "libmtp-dev (= 1.1.14)"
RDEPENDS_libmtp-devel = "libgcrypt-devel libmtp libusbx-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmtp-1.1.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmtp-devel-1.1.14-3.el8.x86_64.rpm \
          "

SRC_URI[libmtp.sha256sum] = "0174082a2b8c3aeb9e2103a5ab0819d7462530728637b3ec96e51b019a0c3269"
SRC_URI[libmtp-devel.sha256sum] = "dbb5b4c8b161ce48ef119318abdd148385268d58c7bf9206bec18b04c7ecce36"
