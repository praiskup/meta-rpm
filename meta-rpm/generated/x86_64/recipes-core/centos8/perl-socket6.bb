SUMMARY = "generated recipe based on perl-Socket6 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Socket6 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Socket6 = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Socket6-0.28-6.el8.x86_64.rpm \
          "

SRC_URI[perl-Socket6.sha256sum] = "5b44c94cecc1fb39965c72a6f0536be21e43271a10d6dc46db541ca86fcd9493"
