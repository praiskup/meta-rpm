SUMMARY = "generated recipe based on libpaper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpaper = "libpaper.so.1"
RPM_SONAME_REQ_libpaper = "libc.so.6 libpaper.so.1"
RDEPENDS_libpaper = "bash glibc"
RPM_SONAME_REQ_libpaper-devel = "libpaper.so.1"
RPROVIDES_libpaper-devel = "libpaper-dev (= 1.1.24)"
RDEPENDS_libpaper-devel = "libpaper"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpaper-1.1.24-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpaper-devel-1.1.24-22.el8.x86_64.rpm \
          "

SRC_URI[libpaper.sha256sum] = "14273ff4fd098be0813294091a04a2bee309eb644d3f3b98fa7b31faaf6d22d8"
SRC_URI[libpaper-devel.sha256sum] = "b1809b4e4db94921538ded10b6a9ac915ffdf0f38eadbf978980e79822bc928b"
