SUMMARY = "generated recipe based on shim-unsigned-x64 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/shim-unsigned-x64-15-8.el8.x86_64.rpm \
          "

SRC_URI[shim-unsigned-x64.sha256sum] = "065c79b10bf3593848f3db84163510cee0689335e37bb39f3a5b01e5580d2be5"
