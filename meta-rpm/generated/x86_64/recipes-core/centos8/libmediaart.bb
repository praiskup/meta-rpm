SUMMARY = "generated recipe based on libmediaart srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libmediaart = "libmediaart-2.0.so.0"
RPM_SONAME_REQ_libmediaart = "libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libmediaart = "gdk-pixbuf2 glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmediaart-1.9.4-4.el8.x86_64.rpm \
          "

SRC_URI[libmediaart.sha256sum] = "024354dad295009709b2bc188220b2605c2ef3a8c3585a810de879c5690df416"
