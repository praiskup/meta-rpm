SUMMARY = "generated recipe based on spice-streaming-agent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libdrm libgcc libjpeg-turbo libx11 libxfixes libxrandr pkgconfig-native"
RPM_SONAME_REQ_spice-streaming-agent = "libX11.so.6 libXfixes.so.3 libXrandr.so.2 libc.so.6 libdl.so.2 libdrm.so.2 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libjpeg.so.62 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_spice-streaming-agent = "bash glib2 glibc gstreamer1 gstreamer1-plugins-base libX11 libXfixes libXrandr libdrm libgcc libjpeg-turbo libstdc++ policycoreutils-python-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-streaming-agent-0.3-2.el8.x86_64.rpm \
          "

SRC_URI[spice-streaming-agent.sha256sum] = "30051cbfb788d8c05b7327123476b520e7b628949464280804481b98b5553501"
