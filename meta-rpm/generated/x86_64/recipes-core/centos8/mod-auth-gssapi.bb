SUMMARY = "generated recipe based on mod_auth_gssapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs openssl pkgconfig-native"
RPM_SONAME_REQ_mod_auth_gssapi = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0 libssl.so.1.1"
RDEPENDS_mod_auth_gssapi = "glibc httpd krb5-libs libcom_err openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_auth_gssapi-1.6.1-6.el8.x86_64.rpm \
          "

SRC_URI[mod_auth_gssapi.sha256sum] = "9808b3d02906f5e886421b0c8b0f5ca1aa208f33ae7d9a2e446c08f4032dfb51"
