SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgcc = "libgcc_s.so.1"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgcc-8.3.1-5.el8.0.2.x86_64.rpm \
          "

SRC_URI[libgcc.sha256sum] = "1a87eb290ef40a4b71c57a1a5ea94a179e51fc0b03e7cec6d197f6d51ef29259"
