SUMMARY = "generated recipe based on hyphen-cy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-cy = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-cy-0.20110620-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-cy.sha256sum] = "3fca169aa4dd16543d29963d3ca1309dff679759ff5891537c8258145c20af65"
