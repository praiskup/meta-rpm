SUMMARY = "generated recipe based on usbmuxd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libimobiledevice libplist libusb1 pkgconfig-native"
RPM_SONAME_REQ_usbmuxd = "libc.so.6 libimobiledevice.so.6 libplist.so.3 libpthread.so.0 libusb-1.0.so.0"
RDEPENDS_usbmuxd = "bash glibc libimobiledevice libplist libusbx shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/usbmuxd-1.1.0-13.el8.x86_64.rpm \
          "

SRC_URI[usbmuxd.sha256sum] = "93ca5833169ae7e18f8cfa2e2d880f0ddbcfa8f011e071ab14bd6c9e91fbbd23"
