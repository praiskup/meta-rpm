SUMMARY = "generated recipe based on libtheora srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libogg libpng libvorbis pkgconfig-native sdl zlib"
RPM_SONAME_PROV_libtheora = "libtheora.so.0 libtheoradec.so.1 libtheoraenc.so.1"
RPM_SONAME_REQ_libtheora = "libc.so.6 libogg.so.0"
RDEPENDS_libtheora = "glibc libogg"
RPM_SONAME_REQ_libtheora-devel = "libtheora.so.0 libtheoradec.so.1 libtheoraenc.so.1"
RPROVIDES_libtheora-devel = "libtheora-dev (= 1.1.1)"
RDEPENDS_libtheora-devel = "libogg-devel libtheora pkgconf-pkg-config"
RPM_SONAME_REQ_theora-tools = "libSDL-1.2.so.0 libc.so.6 libm.so.6 libogg.so.0 libpng16.so.16 libpthread.so.0 libtheoradec.so.1 libtheoraenc.so.1 libvorbis.so.0 libvorbisenc.so.2 libz.so.1"
RDEPENDS_theora-tools = "SDL glibc libogg libpng libtheora libvorbis zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtheora-1.1.1-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/theora-tools-1.1.1-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libtheora-devel-1.1.1-21.el8.x86_64.rpm \
          "

SRC_URI[libtheora.sha256sum] = "c69987e10c401be766c0a73ade99478d69bad4a2b10688ce9e80295f3f9dae26"
SRC_URI[libtheora-devel.sha256sum] = "f98ec9929f88528e623f2400284bac950b90927d5c09c54b6679a2623171128c"
SRC_URI[theora-tools.sha256sum] = "70ac167bd5677188190aa2e6560658e01dc403d17d47488665a4770b24f83bb1"
