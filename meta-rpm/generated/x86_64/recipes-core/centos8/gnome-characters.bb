SUMMARY = "generated recipe based on gnome-characters srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gtk+3 libunistring pango pkgconfig-native"
RPM_SONAME_PROV_gnome-characters = "libgc.so"
RPM_SONAME_REQ_gnome-characters = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libunistring.so.2"
RDEPENDS_gnome-characters = "gjs glib2 glibc gtk3 libunistring pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-characters-3.28.2-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-characters.sha256sum] = "27531bc601f9b482ec7d06d5bb49909c4ed5615250d721c865b3c3431f4bd2b1"
