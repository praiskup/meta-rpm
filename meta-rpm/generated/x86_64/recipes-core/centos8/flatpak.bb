SUMMARY = "generated recipe based on flatpak srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dconf fuse gdk-pixbuf glib-2.0 gpgme json-glib libappstream-glib libarchive libgcc libseccomp libsoup-2.4 libxau libxml2 ostree pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_flatpak = "libXau.so.6 libappstream-glib.so.8 libarchive.so.13 libc.so.6 libdconf.so.1 libfuse.so.2 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpgme.so.11 libjson-glib-1.0.so.0 libostree-1.so.1 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libseccomp.so.2 libsoup-2.4.so.1 libsystemd.so.0 libxml2.so.2"
RDEPENDS_flatpak = "bash bubblewrap dconf flatpak-session-helper fuse-libs gdk-pixbuf2 glib2 glibc gpgme json-glib libXau libappstream-glib libarchive libgcc librsvg2 libseccomp libsoup libxml2 ostree-libs platform-python polkit-libs systemd systemd-libs"
RPM_SONAME_PROV_flatpak-libs = "libflatpak.so.0"
RPM_SONAME_REQ_flatpak-libs = "libXau.so.6 libarchive.so.13 libc.so.6 libdconf.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpgme.so.11 libjson-glib-1.0.so.0 libostree-1.so.1 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libseccomp.so.2 libsoup-2.4.so.1 libsystemd.so.0 libxml2.so.2"
RDEPENDS_flatpak-libs = "bubblewrap dconf glib2 glibc gpgme json-glib libXau libarchive libgcc libseccomp libsoup libxml2 ostree ostree-libs polkit-libs shadow-utils systemd-libs"
RDEPENDS_flatpak-selinux = "bash libselinux-utils policycoreutils policycoreutils-python-utils selinux-policy selinux-policy-minimum"
RPM_SONAME_REQ_flatpak-session-helper = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_flatpak-session-helper = "glib2 glibc libgcc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flatpak-1.6.2-3.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flatpak-libs-1.6.2-3.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flatpak-selinux-1.6.2-3.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flatpak-session-helper-1.6.2-3.el8_2.x86_64.rpm \
          "

SRC_URI[flatpak.sha256sum] = "d1824d1204523cbf1e7c8365356dfa85ccbe643df10ddb121d03b95bdf0aa4b3"
SRC_URI[flatpak-libs.sha256sum] = "a9ce6d18086f8a8d2916ed7f575b1042042a9720a3974dd6d29014833e38af2b"
SRC_URI[flatpak-selinux.sha256sum] = "3140c4600b5ea24ce6b52443dc67b36fed0fa63fb463508430a71f00b250115d"
SRC_URI[flatpak-session-helper.sha256sum] = "c22149b8815b9e1b74f07bb0698288d72745f101703da4c77b8038323d69f00b"
