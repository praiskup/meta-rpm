SUMMARY = "generated recipe based on libmusicbrainz5 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libxml2 neon pkgconfig-native"
RPM_SONAME_PROV_libmusicbrainz5 = "libmusicbrainz5.so.1"
RPM_SONAME_REQ_libmusicbrainz5 = "libc.so.6 libgcc_s.so.1 libm.so.6 libneon.so.27 libstdc++.so.6 libxml2.so.2"
RDEPENDS_libmusicbrainz5 = "glibc libgcc libstdc++ libxml2 neon"
RPM_SONAME_REQ_libmusicbrainz5-devel = "libmusicbrainz5.so.1"
RPROVIDES_libmusicbrainz5-devel = "libmusicbrainz5-dev (= 5.1.0)"
RDEPENDS_libmusicbrainz5-devel = "libmusicbrainz5 libxml2-devel neon-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmusicbrainz5-5.1.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmusicbrainz5-devel-5.1.0-10.el8.x86_64.rpm \
          "

SRC_URI[libmusicbrainz5.sha256sum] = "eaa8444b784f9e2d1a2fb55a27e6942403f040d0fa51ebed57ee238086d3623f"
SRC_URI[libmusicbrainz5-devel.sha256sum] = "c03b68ae23740efb3795623a331f9deaf534a6467852fa2f592d7853896e35a5"
