SUMMARY = "generated recipe based on java-atk-wrapper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "at-spi2-atk at-spi2-core atk cairo dbus-libs gdk-pixbuf glib-2.0 gtk+3 gtk2 pango pkgconfig-native"
RPM_SONAME_PROV_java-atk-wrapper = "libatk-wrapper.so.5"
RPM_SONAME_REQ_java-atk-wrapper = "libatk-1.0.so.0 libatk-bridge-2.0.so.0 libatspi.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libgdk-3.so.0 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_java-atk-wrapper = "at-spi2-atk at-spi2-core atk cairo cairo-gobject dbus-libs gdk-pixbuf2 glib2 glibc gtk2 gtk3 java-1.8.0-openjdk pango xorg-x11-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-atk-wrapper-0.33.2-6.el8.x86_64.rpm \
          "

SRC_URI[java-atk-wrapper.sha256sum] = "54a9e78af600a6158377e99daf945aacc59472d64e9dc8d565f63a42d1f6b798"
