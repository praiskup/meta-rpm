SUMMARY = "generated recipe based on gobject-introspection srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libffi pkgconfig-native"
RPM_SONAME_PROV_gobject-introspection = "libgirepository-1.0.so.1"
RPM_SONAME_REQ_gobject-introspection = "libc.so.6 libdl.so.2 libffi.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0"
RDEPENDS_gobject-introspection = "glib2 glibc libffi"
RPM_SONAME_REQ_gobject-introspection-devel = "libc.so.6 libdl.so.2 libffi.so.6 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0"
RPROVIDES_gobject-introspection-devel = "gobject-introspection-dev (= 1.56.1)"
RDEPENDS_gobject-introspection-devel = "glib2 glib2-devel glibc gobject-introspection libffi libffi-devel libtool pkgconf-pkg-config platform-python python3-mako"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gobject-introspection-1.56.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gobject-introspection-devel-1.56.1-1.el8.x86_64.rpm \
          "

SRC_URI[gobject-introspection.sha256sum] = "7e2804a4494d4179ed50c0c99da1e30c3a6abf8db889c1412b458943cff0e3e5"
SRC_URI[gobject-introspection-devel.sha256sum] = "00e99c413ee371fa2be41e925738221c5d14f0146a4ac98c81c04e2c6efd1e80"
