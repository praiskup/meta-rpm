SUMMARY = "generated recipe based on perl-Clone srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Clone = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Clone = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Clone-0.39-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Clone.sha256sum] = "b8fc54dc34e1ad78d9aad8eae4c505c99bd27c2d2d656e8ab6da8fb0a265e2c8"
