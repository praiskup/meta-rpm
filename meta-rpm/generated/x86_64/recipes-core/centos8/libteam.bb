SUMMARY = "generated recipe based on libteam srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs jansson libdaemon libnl pkgconfig-native"
RPM_SONAME_PROV_libteam = "libteam.so.5"
RPM_SONAME_REQ_libteam = "libc.so.6 libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libteam.so.5"
RDEPENDS_libteam = "glibc libnl3 libnl3-cli"
RDEPENDS_libteam-doc = "libteam"
RDEPENDS_network-scripts-team = "bash network-scripts"
RPM_SONAME_PROV_teamd = "libteamdctl.so.0"
RPM_SONAME_REQ_teamd = "libc.so.6 libdaemon.so.0 libdbus-1.so.3 libjansson.so.4 libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libteam.so.5 libteamdctl.so.0"
RDEPENDS_teamd = "bash dbus-libs glibc jansson libdaemon libnl3 libnl3-cli libteam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libteam-1.29-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libteam-doc-1.29-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/network-scripts-team-1.29-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/teamd-1.29-1.el8_2.2.x86_64.rpm \
          "

SRC_URI[libteam.sha256sum] = "3edac13122fdac91c1347664dbd0f00dc7ae28fcaef008a7e7d76fb0ac4366d5"
SRC_URI[libteam-doc.sha256sum] = "5f1f0a047af909282e66c49d078c8a4bd2a152fa05c7add07a6fa81969478cfa"
SRC_URI[network-scripts-team.sha256sum] = "9c87e8f964344c5ea9af50e000fc4c49f55d7605a777012bb5cd682422f088a8"
SRC_URI[teamd.sha256sum] = "cc7d50e80296842212bb4ac3adc72c4badb53d0dd636b6e6ddc3d6464c86f986"
