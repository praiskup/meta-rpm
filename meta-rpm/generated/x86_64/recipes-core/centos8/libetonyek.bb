SUMMARY = "generated recipe based on libetonyek srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc liblangtag librevenge libxml2 pkgconfig-native zlib"
RPM_SONAME_PROV_libetonyek = "libetonyek-0.1.so.1"
RPM_SONAME_REQ_libetonyek = "libc.so.6 libgcc_s.so.1 liblangtag.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libetonyek = "glibc libgcc liblangtag librevenge libstdc++ libxml2 zlib"
RPM_SONAME_REQ_libetonyek-devel = "libetonyek-0.1.so.1"
RPROVIDES_libetonyek-devel = "libetonyek-dev (= 0.1.8)"
RDEPENDS_libetonyek-devel = "libetonyek liblangtag-devel librevenge-devel libxml2-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libetonyek-0.1.8-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libetonyek-devel-0.1.8-1.el8.x86_64.rpm \
          "

SRC_URI[libetonyek.sha256sum] = "a4f76529c974d370066e8951b85f18b1a931668837ffe8dcf7fda198f62ac275"
SRC_URI[libetonyek-devel.sha256sum] = "9ac3a6a08814c1e602e5f39385f0c65474d742b7b5bb8d550273ca475350fc6f"
