SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libuuid pkgconfig-native"
RPM_SONAME_PROV_libblkid = "libblkid.so.1"
RPM_SONAME_REQ_libblkid = "ld-linux-x86-64.so.2 libc.so.6 libuuid.so.1"
RDEPENDS_libblkid = "bash coreutils glibc libuuid"
RPM_SONAME_REQ_libblkid-devel = "libblkid.so.1"
RPROVIDES_libblkid-devel = "libblkid-dev (= 2.32.1)"
RDEPENDS_libblkid-devel = "libblkid libuuid-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libblkid-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libblkid-devel-2.32.1-22.el8.x86_64.rpm \
          "

SRC_URI[libblkid.sha256sum] = "bc3b5199341d709798462069ada434cc284579da9fad269d5c384626a3f35271"
SRC_URI[libblkid-devel.sha256sum] = "88db188b967e0857d7191dd55243dd31bc7af3ddee7b21bd8a2dbce3c1e87d75"
