SUMMARY = "generated recipe based on gnome-desktop3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gdk-pixbuf glib-2.0 gsettings-desktop-schemas gtk+3 iso-codes libgcc libseccomp libx11 pkgconfig-native systemd systemd-libs xkeyboard-config"
RPM_SONAME_PROV_gnome-desktop3 = "libgnome-desktop-3.so.17"
RPM_SONAME_REQ_gnome-desktop3 = "libX11.so.6 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 librt.so.1 libseccomp.so.2 libudev.so.1"
RDEPENDS_gnome-desktop3 = "bubblewrap cairo gdk-pixbuf2 glib2 glibc gnome-themes-standard gsettings-desktop-schemas gtk3 libX11 libgcc libseccomp systemd-libs"
RPM_SONAME_REQ_gnome-desktop3-devel = "libgnome-desktop-3.so.17"
RPROVIDES_gnome-desktop3-devel = "gnome-desktop3-dev (= 3.32.2)"
RDEPENDS_gnome-desktop3-devel = "gdk-pixbuf2-devel glib2-devel gnome-desktop3 gsettings-desktop-schemas-devel gtk3-devel iso-codes-devel libX11-devel libseccomp-devel pkgconf-pkg-config systemd-devel xkeyboard-config-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-desktop3-3.32.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-desktop3-devel-3.32.2-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-desktop3.sha256sum] = "5c51c35e7a83a7a21393812618983f889eed14e490c447498c69006603c1bdd3"
SRC_URI[gnome-desktop3-devel.sha256sum] = "769e8ca00ad228e830737e7e2f88b19468780d407a0df90edd72b2faeece2af0"
