SUMMARY = "generated recipe based on gcc-toolset-9-elfutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 elfutils pkgconfig-native xz zlib"
RPM_SONAME_REQ_gcc-toolset-9-elfutils = "ld-linux-x86-64.so.2 libasm.so.dts.1 libc.so.6 libdl.so.2 libdw.so.dts.1 libelf.so.dts.1 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-elfutils = "bash gcc-toolset-9-elfutils-libelf gcc-toolset-9-elfutils-libs gcc-toolset-9-runtime glibc libstdc++"
RPROVIDES_gcc-toolset-9-elfutils-devel = "gcc-toolset-9-elfutils-dev (= 0.176)"
RDEPENDS_gcc-toolset-9-elfutils-devel = "bzip2-devel elfutils-libelf-devel gcc-toolset-9-elfutils-libelf-devel gcc-toolset-9-runtime pkgconf-pkg-config xz-devel zlib-devel"
RPM_SONAME_PROV_gcc-toolset-9-elfutils-libelf = "libelf.so.dts.1"
RPM_SONAME_REQ_gcc-toolset-9-elfutils-libelf = "ld-linux-x86-64.so.2 libc.so.6 libz.so.1"
RDEPENDS_gcc-toolset-9-elfutils-libelf = "gcc-toolset-9-runtime glibc zlib"
RPROVIDES_gcc-toolset-9-elfutils-libelf-devel = "gcc-toolset-9-elfutils-libelf-dev (= 0.176)"
RDEPENDS_gcc-toolset-9-elfutils-libelf-devel = "gcc-toolset-9-runtime pkgconf-pkg-config zlib-devel"
RPM_SONAME_PROV_gcc-toolset-9-elfutils-libs = "libasm.so.dts.1 libdw.so.dts.1"
RPM_SONAME_REQ_gcc-toolset-9-elfutils-libs = "ld-linux-x86-64.so.2 libbz2.so.1 libc.so.6 libdl.so.2 libdw.so.dts.1 libelf.so.dts.1 liblzma.so.5 libz.so.1"
RDEPENDS_gcc-toolset-9-elfutils-libs = "bzip2-libs gcc-toolset-9-elfutils-libelf gcc-toolset-9-runtime glibc xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-elfutils-0.176-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-elfutils-devel-0.176-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-elfutils-libelf-0.176-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-elfutils-libelf-devel-0.176-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-elfutils-libs-0.176-5.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-elfutils.sha256sum] = "554fc7c9bf65824d44137ae4519290ddd1c900b78ce735967418a5b2ef95081c"
SRC_URI[gcc-toolset-9-elfutils-devel.sha256sum] = "fab0629b1c2567ce1887f2e5e093bfbceecfbdf585bc3b35060c2fb56ce28ff6"
SRC_URI[gcc-toolset-9-elfutils-libelf.sha256sum] = "2ba169aa8cdb84b0aedbb3b9e4758b6d86aa3ce5769a18fc2f667823e6c8e3ca"
SRC_URI[gcc-toolset-9-elfutils-libelf-devel.sha256sum] = "50a047f69b59e856bdb2bbdb65e477ca35d3c586580733952c40efe80f09451a"
SRC_URI[gcc-toolset-9-elfutils-libs.sha256sum] = "76451a619a60148bbbd54503643f2456e7365af6834240a5d69f4a293579d11e"
