SUMMARY = "generated recipe based on xcb-util-keysyms srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util-keysyms = "libxcb-keysyms.so.1"
RPM_SONAME_REQ_xcb-util-keysyms = "libc.so.6 libxcb.so.1"
RDEPENDS_xcb-util-keysyms = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-keysyms-devel = "libxcb-keysyms.so.1"
RPROVIDES_xcb-util-keysyms-devel = "xcb-util-keysyms-dev (= 0.4.0)"
RDEPENDS_xcb-util-keysyms-devel = "libxcb-devel pkgconf-pkg-config xcb-util-keysyms"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xcb-util-keysyms-0.4.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xcb-util-keysyms-devel-0.4.0-7.el8.x86_64.rpm \
          "

SRC_URI[xcb-util-keysyms.sha256sum] = "72c023a7d875e3c44df6f3110cb085f00b02fb2268f061fc5beacc7a9249e92f"
SRC_URI[xcb-util-keysyms-devel.sha256sum] = "e79ddc816564c4a8ddf8f563909074be76121171b8385aeb7bb09b103dc57e2d"
