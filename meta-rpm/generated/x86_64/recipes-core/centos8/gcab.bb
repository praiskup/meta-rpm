SUMMARY = "generated recipe based on gcab srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_libgcab1 = "libgcab-1.0.so.0"
RPM_SONAME_REQ_libgcab1 = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libz.so.1"
RDEPENDS_libgcab1 = "glib2 glibc libgcc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgcab1-1.1-1.el8.x86_64.rpm \
          "

SRC_URI[libgcab1.sha256sum] = "d9d8c21ad40e59b78008f1a569039462e5654a9d4bb08d84cf032e34cf7bde62"
