SUMMARY = "generated recipe based on hexedit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_hexedit = "libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_hexedit = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hexedit-1.2.13-12.el8.x86_64.rpm \
          "

SRC_URI[hexedit.sha256sum] = "4538e44d3ebff3f9323b59171767bca2b7f5244dd90141de101856ad4f4643f5"
