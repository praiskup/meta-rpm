SUMMARY = "generated recipe based on lzo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_lzo = "liblzo2.so.2"
RPM_SONAME_REQ_lzo = "libc.so.6"
RDEPENDS_lzo = "glibc"
RPM_SONAME_REQ_lzo-devel = "liblzo2.so.2 libminilzo.so.0"
RPROVIDES_lzo-devel = "lzo-dev (= 2.08)"
RDEPENDS_lzo-devel = "lzo lzo-minilzo zlib-devel"
RPM_SONAME_PROV_lzo-minilzo = "libminilzo.so.0"
RPM_SONAME_REQ_lzo-minilzo = "libc.so.6"
RDEPENDS_lzo-minilzo = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lzo-2.08-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lzo-devel-2.08-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lzo-minilzo-2.08-14.el8.x86_64.rpm \
          "

SRC_URI[lzo.sha256sum] = "5c68635cb03533a38d4a42f6547c21a1d5f9952351bb01f3cf865d2621a6e634"
SRC_URI[lzo-devel.sha256sum] = "59601c94f9020553c63df4f92b5bbebf78fa94b668129ec0ac853f088cb20423"
SRC_URI[lzo-minilzo.sha256sum] = "c1fc2f5cf6a2e84a84c68d8bf4f03d9b8c77b98cc73efb3bba28718b0b884782"
