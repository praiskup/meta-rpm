SUMMARY = "generated recipe based on hwloc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo libpciaccess libtool libx11 libxext libxml2 libxnvctrl ncurses numactl pkgconfig-native rdma-core"
RPM_SONAME_REQ_hwloc = "libc.so.6 libhwloc.so.5 libltdl.so.7 libm.so.6 libnuma.so.1 libtinfo.so.6"
RDEPENDS_hwloc = "bash glibc hwloc-libs libtool-ltdl ncurses-libs numactl-libs systemd"
RPM_SONAME_REQ_hwloc-devel = "libhwloc.so.5"
RPROVIDES_hwloc-devel = "hwloc-dev (= 1.11.9)"
RDEPENDS_hwloc-devel = "hwloc-libs pkgconf-pkg-config rdma-core-devel"
RPM_SONAME_REQ_hwloc-gui = "libX11.so.6 libc.so.6 libcairo.so.2 libhwloc.so.5 libltdl.so.7 libm.so.6 libnuma.so.1 libtinfo.so.6"
RDEPENDS_hwloc-gui = "cairo glibc hwloc-libs libX11 libtool-ltdl ncurses-libs numactl-libs"
RPM_SONAME_PROV_hwloc-libs = "libhwloc.so.5"
RPM_SONAME_REQ_hwloc-libs = "libc.so.6 libltdl.so.7 libm.so.6 libnuma.so.1"
RDEPENDS_hwloc-libs = "glibc libtool-ltdl numactl-libs"
RPM_SONAME_REQ_hwloc-plugins = "libX11.so.6 libXNVCtrl.so.0 libXext.so.6 libc.so.6 libltdl.so.7 libpciaccess.so.0 libxml2.so.2"
RDEPENDS_hwloc-plugins = "glibc libX11 libXNVCtrl libXext libpciaccess libtool-ltdl libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hwloc-gui-1.11.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hwloc-plugins-1.11.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/hwloc-1.11.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/hwloc-libs-1.11.9-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/hwloc-devel-1.11.9-3.el8.x86_64.rpm \
          "

SRC_URI[hwloc.sha256sum] = "81ac80dd4200b560aef9065cc3b5d762bda054c9f397c517c4069894d941e4de"
SRC_URI[hwloc-devel.sha256sum] = "bfe4fc10929c9591e63736d42e8e0ed55b9c42fb3e99444a8d40fc4dfb57c90c"
SRC_URI[hwloc-gui.sha256sum] = "981e308a2b079f784a0fa4490064ac438601b3cc8009913b154ed761db1ac64a"
SRC_URI[hwloc-libs.sha256sum] = "afc87c02bea633d500c48dc7f46180df5c5def48fb152841db261db84904be7f"
SRC_URI[hwloc-plugins.sha256sum] = "089bd55fe51fccae0c3ce67f4deb3fd406682d8718af1c107eb76bcf34ca82ce"
