SUMMARY = "generated recipe based on dbus-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus dbus-libs expat glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_dbus-glib-devel = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libexpat.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RPROVIDES_dbus-glib-devel = "dbus-glib-dev (= 0.110)"
RDEPENDS_dbus-glib-devel = "dbus-devel dbus-glib dbus-libs expat glib2 glib2-devel glibc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dbus-glib-devel-0.110-2.el8.x86_64.rpm \
          "

SRC_URI[dbus-glib-devel.sha256sum] = "c94a0e859b52c103f6b8ff8d168e7e846404c7b130d86f7134cc16451cd20d1a"
