SUMMARY = "generated recipe based on redland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db libtool pkgconfig-native raptor2 rasqal sqlite3"
RPM_SONAME_PROV_redland = "librdf.so.0 librdf_storage_sqlite.so"
RPM_SONAME_REQ_redland = "libc.so.6 libdb-5.3.so libdl.so.2 libltdl.so.7 libraptor2.so.0 librasqal.so.3 librdf.so.0 libsqlite3.so.0"
RDEPENDS_redland = "glibc libdb libtool-ltdl raptor2 rasqal sqlite-libs"
RPM_SONAME_REQ_redland-devel = "librdf.so.0"
RPROVIDES_redland-devel = "redland-dev (= 1.0.17)"
RDEPENDS_redland-devel = "bash pkgconf-pkg-config raptor2-devel rasqal-devel redland"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redland-1.0.17-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/redland-devel-1.0.17-14.el8.x86_64.rpm \
          "

SRC_URI[redland.sha256sum] = "a32053e79dabe07ab75d49c5bd1c720450c590bc398338702d3094fb6da0e849"
SRC_URI[redland-devel.sha256sum] = "c238815da511b34ef4af5f7f2af4812526c225ac01755db2d20e67b7e5106a4d"
