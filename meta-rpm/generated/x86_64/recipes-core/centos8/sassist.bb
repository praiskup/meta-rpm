SUMMARY = "generated recipe based on sassist srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sassist = "bash freeipmi sos systemd zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sassist-0.8.5-4.el8.noarch.rpm \
          "

SRC_URI[sassist.sha256sum] = "2aba6c080714ee64c91b4ecc041a33e3afe0f330082be6410059542457a8fde8"
