SUMMARY = "generated recipe based on dbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-libs expat libcap-ng libselinux libx11 pkgconfig-native systemd-libs"
RDEPENDS_dbus = "dbus-daemon systemd"
RPM_SONAME_REQ_dbus-daemon = "libaudit.so.1 libc.so.6 libcap-ng.so.0 libdbus-1.so.3 libexpat.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0"
RDEPENDS_dbus-daemon = "audit-libs bash dbus-common dbus-libs dbus-tools expat glibc libcap-ng libselinux shadow-utils systemd-libs"
RPM_SONAME_REQ_dbus-devel = "libdbus-1.so.3"
RPROVIDES_dbus-devel = "dbus-dev (= 1.12.8)"
RDEPENDS_dbus-devel = "cmake-filesystem dbus-daemon dbus-libs pkgconf-pkg-config xml-common"
RPM_SONAME_REQ_dbus-tools = "libc.so.6 libdbus-1.so.3 libpthread.so.0 libsystemd.so.0"
RDEPENDS_dbus-tools = "dbus-libs glibc systemd-libs"
RPM_SONAME_REQ_dbus-x11 = "libX11.so.6 libc.so.6 libdbus-1.so.3 libpthread.so.0 libsystemd.so.0"
RDEPENDS_dbus-x11 = "bash dbus-daemon dbus-libs glibc libX11 systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dbus-devel-1.12.8-10.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dbus-x11-1.12.8-10.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbus-1.12.8-10.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbus-common-1.12.8-10.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbus-daemon-1.12.8-10.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbus-tools-1.12.8-10.el8_2.x86_64.rpm \
          "

SRC_URI[dbus.sha256sum] = "24c7f4bc0582758eb2e8bc0e4a1c7adb9dec01463300be8ad6e67ac0390ef773"
SRC_URI[dbus-common.sha256sum] = "488c91557b55bcd1ef629d1a7ed7a6eaba939a05a319e07166f310d79fe39184"
SRC_URI[dbus-daemon.sha256sum] = "ac46552dacfe0fb74d9ff2f21672a5f8565921a69c9854f0b7f3fe24a0bf6685"
SRC_URI[dbus-devel.sha256sum] = "75deb863c495465eac5c5e96ffdc9c11ad7dae460bd5d0b31d53e33de765d204"
SRC_URI[dbus-tools.sha256sum] = "38ae143169a03f2090c85f5abd2aca4dbea6ec8c127a4b414307119896430c6c"
SRC_URI[dbus-x11.sha256sum] = "bf37032ef218c9381e211409f6e57f35bd59cd9e9ad09d32fc7c872e8b8fd4c9"
