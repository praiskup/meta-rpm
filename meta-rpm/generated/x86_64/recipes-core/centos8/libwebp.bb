SUMMARY = "generated recipe based on libwebp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libwebp = "libwebp.so.7 libwebpdecoder.so.3 libwebpdemux.so.2 libwebpmux.so.3"
RPM_SONAME_REQ_libwebp = "libc.so.6 libm.so.6 libpthread.so.0 libwebp.so.7"
RDEPENDS_libwebp = "glibc"
RPM_SONAME_REQ_libwebp-devel = "libwebp.so.7 libwebpdecoder.so.3 libwebpdemux.so.2 libwebpmux.so.3"
RPROVIDES_libwebp-devel = "libwebp-dev (= 1.0.0)"
RDEPENDS_libwebp-devel = "libwebp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwebp-1.0.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwebp-devel-1.0.0-1.el8.x86_64.rpm \
          "

SRC_URI[libwebp.sha256sum] = "47a853bfdef174bb319783f2bb1c73332da293c948170941a6899bcfb57896d6"
SRC_URI[libwebp-devel.sha256sum] = "818e308823bb67f74e4fe2c8199b427569b057d4a6f6e5e2b2a6848ef4c83bb4"
