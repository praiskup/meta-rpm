SUMMARY = "generated recipe based on perl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm libxcrypt perl-podlators pkgconfig-native systemtap-sdt-devel"
RDEPENDS_perl = "perl-Archive-Tar perl-Attribute-Handlers perl-B-Debug perl-CPAN perl-CPAN-Meta perl-CPAN-Meta-Requirements perl-CPAN-Meta-YAML perl-Carp perl-Compress-Raw-Bzip2 perl-Compress-Raw-Zlib perl-Config-Perl-V perl-DB_File perl-Data-Dumper perl-Devel-PPPort perl-Devel-Peek perl-Devel-SelfStubber perl-Digest perl-Digest-MD5 perl-Digest-SHA perl-Encode perl-Encode-devel perl-Env perl-Errno perl-Exporter perl-ExtUtils-CBuilder perl-ExtUtils-Command perl-ExtUtils-Embed perl-ExtUtils-Install perl-ExtUtils-MakeMaker perl-ExtUtils-Manifest perl-ExtUtils-Miniperl perl-ExtUtils-ParseXS perl-File-Fetch perl-File-Path perl-File-Temp perl-Filter perl-Filter-Simple perl-Getopt-Long perl-HTTP-Tiny perl-IO perl-IO-Compress perl-IO-Socket-IP perl-IO-Zlib perl-IPC-Cmd perl-IPC-SysV perl-JSON-PP perl-Locale-Codes perl-Locale-Maketext perl-Locale-Maketext-Simple perl-MIME-Base64 perl-Math-BigInt perl-Math-BigInt-FastCalc perl-Math-BigRat perl-Math-Complex perl-Memoize perl-Module-CoreList perl-Module-CoreList-tools perl-Module-Load perl-Module-Load-Conditional perl-Module-Loaded perl-Module-Metadata perl-Net-Ping perl-Params-Check perl-PathTools perl-Perl-OSType perl-PerlIO-via-QuotedPrint perl-Pod-Checker perl-Pod-Escapes perl-Pod-Html perl-Pod-Parser perl-Pod-Perldoc perl-Pod-Simple perl-Pod-Usage perl-Scalar-List-Utils perl-SelfLoader perl-Socket perl-Storable perl-Sys-Syslog perl-Term-ANSIColor perl-Term-Cap perl-Test perl-Test-Harness perl-Test-Simple perl-Text-Balanced perl-Text-ParseWords perl-Text-Tabs+Wrap perl-Thread-Queue perl-Time-HiRes perl-Time-Local perl-Time-Piece perl-Unicode-Collate perl-Unicode-Normalize perl-autodie perl-bignum perl-constant perl-devel perl-encoding perl-experimental perl-interpreter perl-libnet perl-libnetcfg perl-libs perl-macros perl-open perl-parent perl-perlfaq perl-podlators perl-threads perl-threads-shared perl-utils perl-version"
RDEPENDS_perl-Attribute-Handlers = "perl-Carp perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-Devel-Peek = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Peek = "glibc perl-Exporter perl-libs"
RDEPENDS_perl-Devel-SelfStubber = "perl-PathTools perl-SelfLoader perl-libs"
RDEPENDS_perl-Errno = "perl-Carp perl-Exporter perl-interpreter perl-libs"
RDEPENDS_perl-ExtUtils-Embed = "perl-Exporter perl-PathTools perl-devel perl-interpreter perl-libs"
RDEPENDS_perl-ExtUtils-Miniperl = "perl-Exporter perl-ExtUtils-Embed perl-devel perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-IO = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-IO = "glibc perl-Carp perl-Errno perl-Exporter perl-PathTools perl-Socket perl-interpreter perl-libs"
RDEPENDS_perl-IO-Zlib = "perl-Carp perl-IO-Compress perl-interpreter perl-libs"
RDEPENDS_perl-Locale-Maketext-Simple = "perl-Locale-Maketext perl-interpreter perl-libs"
RDEPENDS_perl-Math-Complex = "perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"
RDEPENDS_perl-Memoize = "perl-Carp perl-Exporter perl-Storable perl-interpreter perl-libs"
RDEPENDS_perl-Module-Loaded = "perl-Carp perl-interpreter perl-libs"
RDEPENDS_perl-Net-Ping = "perl-Carp perl-Exporter perl-Socket perl-Time-HiRes perl-constant perl-interpreter perl-libs"
RDEPENDS_perl-Pod-Html = "perl-Carp perl-Exporter perl-Getopt-Long perl-PathTools perl-Pod-Simple perl-interpreter perl-libs perl-parent"
RDEPENDS_perl-SelfLoader = "perl-Carp perl-Exporter perl-IO perl-interpreter perl-libs"
RDEPENDS_perl-Test = "perl-Carp perl-Exporter perl-File-Temp perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-Time-Piece = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Time-Piece = "glibc perl-Carp perl-Exporter perl-Time-Local perl-constant perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-devel = "libperl.so.5.26"
RPROVIDES_perl-devel = "perl-dev (= 5.26.3)"
RDEPENDS_perl-devel = "perl-ExtUtils-Install perl-ExtUtils-ParseXS perl-File-Path perl-Getopt-Long perl-PathTools perl-Text-Tabs+Wrap perl-interpreter perl-libs redhat-rpm-config systemtap-sdt-devel"
RPM_SONAME_REQ_perl-interpreter = "libc.so.6 libcrypt.so.1 libdl.so.2 libgdbm.so.6 libgdbm_compat.so.4 libm.so.6 libperl.so.5.26 libpthread.so.0 libresolv.so.2 libutil.so.1"
RDEPENDS_perl-interpreter = "gdbm-libs glibc libxcrypt perl-Carp perl-Exporter perl-File-Path perl-IO perl-PathTools perl-Scalar-List-Utils perl-Text-Tabs+Wrap perl-Unicode-Normalize perl-constant perl-libs perl-macros perl-parent perl-threads perl-threads-shared"
RDEPENDS_perl-libnetcfg = "perl-ExtUtils-MakeMaker perl-IO perl-PathTools perl-interpreter perl-libs"
RPM_SONAME_PROV_perl-libs = "libperl.so.5.26"
RPM_SONAME_REQ_perl-libs = "libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libperl.so.5.26 libpthread.so.0 libresolv.so.2 libutil.so.1"
RDEPENDS_perl-libs = "glibc libxcrypt perl-Carp perl-Exporter"
RDEPENDS_perl-macros = "perl-libs"
RDEPENDS_perl-open = "perl-Carp perl-Encode perl-encoding perl-libs"
RDEPENDS_perl-tests = "perl perl-libs"
RDEPENDS_perl-utils = "perl-Carp perl-File-Path perl-PathTools perl-Text-Tabs+Wrap perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-5.26.3-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Attribute-Handlers-0.99-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Devel-Peek-1.26-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Devel-SelfStubber-1.06-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-ExtUtils-Embed-1.34-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-ExtUtils-Miniperl-1.06-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IO-Zlib-1.10-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Locale-Maketext-Simple-0.21-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Memoize-1.03-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Module-Loaded-0.08-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Net-Ping-2.55-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Pod-Html-1.22.02-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-SelfLoader-1.23-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Test-1.30-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Time-Piece-1.31-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-devel-5.26.3-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-libnetcfg-5.26.3-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-open-1.11-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-tests-5.26.3-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-utils-5.26.3-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Errno-1.28-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-IO-1.38-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Math-Complex-1.59-416.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-interpreter-5.26.3-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-libs-5.26.3-416.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-macros-5.26.3-416.el8.x86_64.rpm \
          "

SRC_URI[perl.sha256sum] = "6350d5fb4962effa1e5a56dd0126fc042bcab3ec52e7e11492ac0fd11b246c75"
SRC_URI[perl-Attribute-Handlers.sha256sum] = "bedc72c978f53d3a8597ad050e3901f6aa641d451be453a2767d35eec3fd2d8b"
SRC_URI[perl-Devel-Peek.sha256sum] = "c77d0a47fdcc19f45b4115c001cbb4d803b9aaa77b0bdb0d06e065fb71c1b044"
SRC_URI[perl-Devel-SelfStubber.sha256sum] = "d7f9487bd11cd51935b3dce183ae6368013e62c08fe7617ee69328b955e74237"
SRC_URI[perl-Errno.sha256sum] = "1f870ed28d3eaff0295e3aa00a4d6ed95dd1b31fa6c70678ee4de498976d60a1"
SRC_URI[perl-ExtUtils-Embed.sha256sum] = "89892ac4fe8310fc52ff00831e715e27dce3aa755ba132ae8ed78218c18062f6"
SRC_URI[perl-ExtUtils-Miniperl.sha256sum] = "891c54e5cf8858d2f325fed5656db9a2d6d8703612dd992e58575008cac181f1"
SRC_URI[perl-IO.sha256sum] = "e4d5dbbb7c6daecf5880f2ba40ef87a6b290b2c047d2cedc53c13a3e41676e33"
SRC_URI[perl-IO-Zlib.sha256sum] = "c2286565bdb7d2278fc2b28623c7def8c3c3f7a536499bed3dca6caa220bc7b9"
SRC_URI[perl-Locale-Maketext-Simple.sha256sum] = "1e84df31c864b37cde1ed52201c8fc86c9775ec48ebf5c3c32822d3f3dcb2143"
SRC_URI[perl-Math-Complex.sha256sum] = "0359f3c75729c97086e952c51e7aef3ee55c0fb09e78975c1f159c75b09b5e41"
SRC_URI[perl-Memoize.sha256sum] = "87f2e0d231dad9cc9ea223ccf1662fb7e8f8b4cbf412a85f4c910fd5d02129dd"
SRC_URI[perl-Module-Loaded.sha256sum] = "92203d4f5183a84cfef625d335c99b94b93fa8d3b21fac6d98a7064f7842b960"
SRC_URI[perl-Net-Ping.sha256sum] = "9bbaa6cca3f02ce0ad3f5b1d11183660409716243343e791366405c81b808bd7"
SRC_URI[perl-Pod-Html.sha256sum] = "9d56e14ef32343e05b97d107c4e78e519dad1a81e484406160a4855d5cec8aaa"
SRC_URI[perl-SelfLoader.sha256sum] = "c01ccb24242027b7a2ce9df923e0cbc3721a76c5f8862a510d909d21eb641d98"
SRC_URI[perl-Test.sha256sum] = "e44e874937805f84fba810a5b20ea1f7bfbbd07da14a922fa512a8bb8d3d2a55"
SRC_URI[perl-Time-Piece.sha256sum] = "3f2ab39907f9ed55d6a6f4a56c3cb8a7125facb78f3a1bb20f1ea0f21a2b2bcb"
SRC_URI[perl-devel.sha256sum] = "cfdd1f29c935ea40bc8cdcc249d0a0e2623a111731b66358a0c93c4b8815e4e0"
SRC_URI[perl-interpreter.sha256sum] = "d4e036e397cf9605f5bad84b74e574d2637dff51d680eb4db82d88868922f9a9"
SRC_URI[perl-libnetcfg.sha256sum] = "89f5ce271d1c7dc05ec798201d507a55a6facbea6a7378f51730566119019729"
SRC_URI[perl-libs.sha256sum] = "94fece2a6e096a688b6fbca4e4d0d7d5c2a1e7065292cc7248b3539d321eee64"
SRC_URI[perl-macros.sha256sum] = "149073d633c02f62d112a1a13debc9c7e9c086f3c1d93a55241f6fab5b7639e0"
SRC_URI[perl-open.sha256sum] = "a207982973a313fb5abea822c5a7c2531ae59064d3fc1e1e2cc972ba2333ac02"
SRC_URI[perl-tests.sha256sum] = "cd1e76fbf1a0a0ff46003c4be3312d853f0cb1db4b5b72e339baba945708aeb7"
SRC_URI[perl-utils.sha256sum] = "436114971b6e0c06187ef28dd0c5d5754cb250ef5f0c21dc7e86bfac2b37eddf"
