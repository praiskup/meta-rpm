SUMMARY = "generated recipe based on sg3_utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_sg3_utils = "libc.so.6 libgcc_s.so.1 libpthread.so.0 libsgutils2.so.2"
RDEPENDS_sg3_utils = "bash glibc libgcc sg3_utils-libs"
RPM_SONAME_REQ_sg3_utils-devel = "libsgutils2.so.2"
RPROVIDES_sg3_utils-devel = "sg3_utils-dev (= 1.44)"
RDEPENDS_sg3_utils-devel = "sg3_utils-libs"
RPM_SONAME_PROV_sg3_utils-libs = "libsgutils2.so.2"
RPM_SONAME_REQ_sg3_utils-libs = "libc.so.6"
RDEPENDS_sg3_utils-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sg3_utils-1.44-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sg3_utils-libs-1.44-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sg3_utils-devel-1.44-5.el8.x86_64.rpm \
          "

SRC_URI[sg3_utils.sha256sum] = "a1d1bac6c4710e0a1a6e8a507b06a630dda6687f12642be0847768c14ce7271e"
SRC_URI[sg3_utils-devel.sha256sum] = "36e4b149026853ba1365346463d87774ec0d0d4ed78c604b4e0d41759e9fa893"
SRC_URI[sg3_utils-libs.sha256sum] = "774d214a379c0b729d7e989f9d5094fdfda7df68c1e792ba9200542032f04c91"
