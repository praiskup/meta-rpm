SUMMARY = "generated recipe based on peripety srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_peripety = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libsystemd.so.0"
RDEPENDS_peripety = "bash glibc libgcc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/peripety-0.1.2-3.el8.x86_64.rpm \
          "

SRC_URI[peripety.sha256sum] = "6aa7dabafbff37a113283deb896162e7590ec1562b31403ad453c6ce13059941"
