SUMMARY = "generated recipe based on libformula srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_libformula = "apache-commons-logging java-1.8.0-openjdk-headless javapackages-tools libbase"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libformula-1.1.3-18.el8.noarch.rpm \
          "

SRC_URI[libformula.sha256sum] = "60054dacb02c78f9196323467ec6a9c7cf34660b8c4e8d4a377ecb73f1e75cd3"
