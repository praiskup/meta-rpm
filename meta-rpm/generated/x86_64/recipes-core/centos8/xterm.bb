SUMMARY = "generated recipe based on xterm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libice libutempter libx11 libxaw libxft libxmu libxpm libxt ncurses pkgconfig-native"
RPM_SONAME_REQ_xterm = "libICE.so.6 libX11.so.6 libXaw.so.7 libXft.so.2 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libtinfo.so.6 libutempter.so.0"
RDEPENDS_xterm = "bash fontconfig freetype glibc libICE libX11 libXaw libXft libXmu libXpm libXt libutempter ncurses-libs xterm-resize"
RPM_SONAME_REQ_xterm-resize = "libc.so.6 libtinfo.so.6"
RDEPENDS_xterm-resize = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xterm-331-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xterm-resize-331-1.el8.x86_64.rpm \
          "

SRC_URI[xterm.sha256sum] = "8cbe09125f2ca43b3801fc7fff35510ee4dc0f4f47f4b7c740083ea01e094537"
SRC_URI[xterm-resize.sha256sum] = "885c64c406fc30d97066f002c87e09ef88a9d10670f5e90453213521c933e3b7"
