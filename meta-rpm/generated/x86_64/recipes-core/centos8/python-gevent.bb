SUMMARY = "generated recipe based on python-gevent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "c-ares libev pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-gevent = "libc.so.6 libcares.so.2 libev.so.4 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-gevent = "c-ares glibc libev platform-python python3-greenlet python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-gevent-1.2.2-4.el8.x86_64.rpm \
          "

SRC_URI[python3-gevent.sha256sum] = "1f7e3279ca14dd000041e8198399b33ed361eff6f9bf980de72840308afcc0a9"
