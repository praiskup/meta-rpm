SUMMARY = "generated recipe based on lua-filesystem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lua-filesystem = "libc.so.6"
RDEPENDS_lua-filesystem = "glibc lua"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lua-filesystem-1.6.3-7.el8.x86_64.rpm \
          "

SRC_URI[lua-filesystem.sha256sum] = "330feb94371911cd9c573722372d80b98bbeca20cda27872e196664f8d8929ef"
