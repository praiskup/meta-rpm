SUMMARY = "generated recipe based on osgi-annotation srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_osgi-annotation = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_osgi-annotation-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/osgi-annotation-6.0.0-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/osgi-annotation-javadoc-6.0.0-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[osgi-annotation.sha256sum] = "6b6ed5f0354c57ad11d8861ab39f4df477ccdb1b85a850a5050e74c7a3f11ea8"
SRC_URI[osgi-annotation-javadoc.sha256sum] = "803a51f34b56c07b82c55bd42d426aa1df4c765c631a941527c58b881df980ab"
