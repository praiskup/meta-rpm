SUMMARY = "generated recipe based on hyphen-mi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-mi = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-mi-0.20080630-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-mi.sha256sum] = "20640f0b73c079ea403b4967341dd9c3652df69cfc8b3d18a24638058dc89984"
