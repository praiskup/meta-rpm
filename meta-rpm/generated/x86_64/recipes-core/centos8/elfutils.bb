SUMMARY = "generated recipe based on elfutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 curl gcc-runtime pkgconfig-native xz zlib"
RPM_SONAME_REQ_elfutils = "libasm.so.1 libc.so.6 libdw.so.1 libelf.so.1 libstdc++.so.6"
RDEPENDS_elfutils = "bash elfutils-libelf elfutils-libs glibc libstdc++"
RPM_SONAME_PROV_elfutils-debuginfod-client = "libdebuginfod.so.1"
RPM_SONAME_REQ_elfutils-debuginfod-client = "libc.so.6 libcurl.so.4 libdebuginfod.so.1"
RDEPENDS_elfutils-debuginfod-client = "glibc libcurl"
RPM_SONAME_REQ_elfutils-debuginfod-client-devel = "libdebuginfod.so.1"
RPROVIDES_elfutils-debuginfod-client-devel = "elfutils-debuginfod-client-dev (= 0.178)"
RDEPENDS_elfutils-debuginfod-client-devel = "elfutils-debuginfod-client pkgconf-pkg-config"
RDEPENDS_elfutils-default-yama-scope = "bash"
RPM_SONAME_REQ_elfutils-devel = "libasm.so.1 libdw.so.1"
RPROVIDES_elfutils-devel = "elfutils-dev (= 0.178)"
RDEPENDS_elfutils-devel = "elfutils-libelf-devel elfutils-libs pkgconf-pkg-config xz-devel zlib-devel"
RDEPENDS_elfutils-devel-static = "elfutils-devel elfutils-libelf-devel-static"
RPM_SONAME_PROV_elfutils-libelf = "libelf.so.1"
RPM_SONAME_REQ_elfutils-libelf = "ld-linux-x86-64.so.2 libc.so.6 libz.so.1"
RDEPENDS_elfutils-libelf = "glibc zlib"
RPM_SONAME_REQ_elfutils-libelf-devel = "libelf.so.1"
RPROVIDES_elfutils-libelf-devel = "elfutils-libelf-dev (= 0.178)"
RDEPENDS_elfutils-libelf-devel = "elfutils-libelf pkgconf-pkg-config zlib-devel"
RDEPENDS_elfutils-libelf-devel-static = "elfutils-libelf-devel"
RPM_SONAME_PROV_elfutils-libs = "libasm.so.1 libdw.so.1"
RPM_SONAME_REQ_elfutils-libs = "ld-linux-x86-64.so.2 libbz2.so.1 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 liblzma.so.5 libpthread.so.0 libz.so.1"
RDEPENDS_elfutils-libs = "bzip2-libs elfutils-default-yama-scope elfutils-libelf glibc xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/elfutils-debuginfod-client-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/elfutils-debuginfod-client-devel-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/elfutils-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/elfutils-default-yama-scope-0.178-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/elfutils-devel-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/elfutils-libelf-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/elfutils-libelf-devel-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/elfutils-libs-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/elfutils-devel-static-0.178-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/elfutils-libelf-devel-static-0.178-7.el8.x86_64.rpm \
          "

SRC_URI[elfutils.sha256sum] = "a1bf86136b9adb115b0838e4344e34cd9bf6822fd9d8480d64b742e82744aadd"
SRC_URI[elfutils-debuginfod-client.sha256sum] = "5a04a2a797a181b0faffd086f21d71ec1b8e8ea99182625262b6bcbfb472ce1d"
SRC_URI[elfutils-debuginfod-client-devel.sha256sum] = "8b757d9eab47a7990ff6e2d16bb018e9567e0797bbd06a3d3b04a152ab7be98d"
SRC_URI[elfutils-default-yama-scope.sha256sum] = "25157a7103a91e2c40ff8b4bf43dfd4cf2faf37c98698dc2ff032c240babe4e2"
SRC_URI[elfutils-devel.sha256sum] = "48e749af712a848ed8da8dd40a750a719113a303692dfc7af230d437012c1563"
SRC_URI[elfutils-devel-static.sha256sum] = "fc40521a58d8135fa225af409e94c57d4a5c9f8747ee2f35922064738b8c4187"
SRC_URI[elfutils-libelf.sha256sum] = "dfe7c7e136991cd16443c6a8436522a5b9940a501a28a0409740f24a23e83079"
SRC_URI[elfutils-libelf-devel.sha256sum] = "1749b417f25bff58474c852d7829c3c9f4f973d88ee431b1c143c2e601a60b37"
SRC_URI[elfutils-libelf-devel-static.sha256sum] = "76ed2a9f163100b9569f4ce49cf42b2d6e66a81c63867a983efdd91b956ee8f7"
SRC_URI[elfutils-libs.sha256sum] = "eb3284ccccf4c86afbd7ecf7b00f450274e9274ef3dbe54ec50c3ebef3cc671c"
