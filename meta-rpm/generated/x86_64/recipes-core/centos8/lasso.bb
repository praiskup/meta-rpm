SUMMARY = "generated recipe based on lasso srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libtool libxml2 libxslt openssl pkgconfig-native xmlsec1 zlib"
RPM_SONAME_PROV_lasso = "liblasso.so.3"
RPM_SONAME_REQ_lasso = "libc.so.6 libcrypto.so.1.1 libglib-2.0.so.0 libgobject-2.0.so.0 libltdl.so.7 libm.so.6 libssl.so.1.1 libxml2.so.2 libxmlsec1-openssl.so.1 libxmlsec1.so.1 libxslt.so.1 libz.so.1"
RDEPENDS_lasso = "glib2 glibc libtool-ltdl libxml2 libxslt openssl-libs xmlsec1 xmlsec1-openssl zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lasso-2.6.0-8.el8.x86_64.rpm \
          "

SRC_URI[lasso.sha256sum] = "7e8cb131b411bc79fda4ef4fd7bf7358bbaa9759dbfe68f12b92817a98fbc79c"
