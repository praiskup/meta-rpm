SUMMARY = "generated recipe based on vte291 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gnutls gtk+3 libgcc libpcre2 pango pkgconfig-native zlib"
RPM_SONAME_PROV_vte291 = "libvte-2.91.so.0"
RPM_SONAME_REQ_vte291 = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpcre2-8.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_vte291 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnutls gtk3 libgcc libstdc++ pango pcre2 vte-profile zlib"
RPM_SONAME_REQ_vte291-devel = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libstdc++.so.6 libvte-2.91.so.0"
RPROVIDES_vte291-devel = "vte291-dev (= 0.52.2)"
RDEPENDS_vte291-devel = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glib2-devel glibc gnutls-devel gtk3 gtk3-devel libgcc libstdc++ pango pango-devel pcre2-devel pkgconf-pkg-config vte291 zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vte-profile-0.52.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vte291-0.52.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/vte291-devel-0.52.2-2.el8.x86_64.rpm \
          "

SRC_URI[vte-profile.sha256sum] = "3a7ad3e5625769acbcfc1d269a9ae286889e32e948133a61f0b2a44c13d7422b"
SRC_URI[vte291.sha256sum] = "21a10793646d8c2b02ed2badaea031d6c4e7935fd6424d83da508bf4dd867a5f"
SRC_URI[vte291-devel.sha256sum] = "65a44cbf6e42629d8f679cae91935ae804303ae93e2dc75370c9287310466080"
