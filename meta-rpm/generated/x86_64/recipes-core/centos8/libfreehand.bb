SUMMARY = "generated recipe based on libfreehand srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lcms2 libgcc librevenge pkgconfig-native zlib"
RPM_SONAME_PROV_libfreehand = "libfreehand-0.1.so.1"
RPM_SONAME_REQ_libfreehand = "libc.so.6 libgcc_s.so.1 liblcms2.so.2 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_libfreehand = "glibc lcms2 libgcc librevenge libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libfreehand-0.1.2-2.el8.x86_64.rpm \
          "

SRC_URI[libfreehand.sha256sum] = "094b25e9c59ef59c9f41b2b0d8357c20ceb4ac497d1fca7e9149bb4c99be9d5c"
