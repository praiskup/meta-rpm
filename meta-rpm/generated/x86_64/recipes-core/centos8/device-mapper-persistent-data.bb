SUMMARY = "generated recipe based on device-mapper-persistent-data srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libaio libgcc pkgconfig-native"
RPM_SONAME_REQ_device-mapper-persistent-data = "libaio.so.1 libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_device-mapper-persistent-data = "expat glibc libaio libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-persistent-data-0.8.5-3.el8.x86_64.rpm \
          "

SRC_URI[device-mapper-persistent-data.sha256sum] = "555f7e278531c7e82f8a32ae4e7e6ad2eb34acc2debfae8d5dbd21025f03fea7"
