SUMMARY = "generated recipe based on setserial srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_setserial = "libc.so.6"
RDEPENDS_setserial = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/setserial-2.17-45.el8.x86_64.rpm \
          "

SRC_URI[setserial.sha256sum] = "8ac083cbc432df1db9e6f3a34b426946625a3bb03601e492bceb490d5366736d"
