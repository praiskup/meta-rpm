SUMMARY = "generated recipe based on xmlrpc-c srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc openssl pkgconfig-native"
RPM_SONAME_PROV_xmlrpc-c = "libxmlrpc.so.3 libxmlrpc_abyss.so.3 libxmlrpc_openssl.so.1 libxmlrpc_server.so.3 libxmlrpc_server_abyss.so.3 libxmlrpc_server_cgi.so.3 libxmlrpc_util.so.4 libxmlrpc_xmlparse.so.3 libxmlrpc_xmltok.so.3"
RPM_SONAME_REQ_xmlrpc-c = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libpthread.so.0 libssl.so.1.1 libxmlrpc.so.3 libxmlrpc_abyss.so.3 libxmlrpc_server.so.3 libxmlrpc_util.so.4 libxmlrpc_xmlparse.so.3 libxmlrpc_xmltok.so.3"
RDEPENDS_xmlrpc-c = "glibc libgcc openssl-libs"
RPM_SONAME_PROV_xmlrpc-c-c++ = "libxmlrpc++.so.8 libxmlrpc_abyss++.so.8 libxmlrpc_cpp.so.8 libxmlrpc_packetsocket.so.8 libxmlrpc_server++.so.8 libxmlrpc_server_abyss++.so.8 libxmlrpc_server_cgi++.so.8 libxmlrpc_server_pstream++.so.8 libxmlrpc_util++.so.8"
RPM_SONAME_REQ_xmlrpc-c-c++ = "libc.so.6 libgcc_s.so.1 libstdc++.so.6 libxmlrpc++.so.8 libxmlrpc.so.3 libxmlrpc_abyss++.so.8 libxmlrpc_abyss.so.3 libxmlrpc_packetsocket.so.8 libxmlrpc_server++.so.8 libxmlrpc_server.so.3 libxmlrpc_server_abyss.so.3 libxmlrpc_util++.so.8 libxmlrpc_util.so.4"
RDEPENDS_xmlrpc-c-c++ = "glibc libgcc libstdc++ xmlrpc-c"
RPM_SONAME_PROV_xmlrpc-c-client = "libxmlrpc_client.so.3"
RPM_SONAME_REQ_xmlrpc-c-client = "libc.so.6 libcurl.so.4 libxmlrpc.so.3 libxmlrpc_util.so.4"
RDEPENDS_xmlrpc-c-client = "glibc libcurl xmlrpc-c"
RPM_SONAME_PROV_xmlrpc-c-client++ = "libxmlrpc_client++.so.8"
RPM_SONAME_REQ_xmlrpc-c-client++ = "libc.so.6 libgcc_s.so.1 libstdc++.so.6 libxmlrpc++.so.8 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_packetsocket.so.8 libxmlrpc_util++.so.8 libxmlrpc_util.so.4"
RDEPENDS_xmlrpc-c-client++ = "glibc libgcc libstdc++ xmlrpc-c xmlrpc-c-c++ xmlrpc-c-client"
RPM_SONAME_REQ_xmlrpc-c-devel = "libxmlrpc++.so.8 libxmlrpc.so.3 libxmlrpc_abyss++.so.8 libxmlrpc_abyss.so.3 libxmlrpc_client++.so.8 libxmlrpc_client.so.3 libxmlrpc_cpp.so.8 libxmlrpc_openssl.so.1 libxmlrpc_packetsocket.so.8 libxmlrpc_server++.so.8 libxmlrpc_server.so.3 libxmlrpc_server_abyss++.so.8 libxmlrpc_server_abyss.so.3 libxmlrpc_server_cgi++.so.8 libxmlrpc_server_cgi.so.3 libxmlrpc_server_pstream++.so.8 libxmlrpc_util++.so.8 libxmlrpc_util.so.4 libxmlrpc_xmlparse.so.3 libxmlrpc_xmltok.so.3"
RPROVIDES_xmlrpc-c-devel = "xmlrpc-c-dev (= 1.51.0)"
RDEPENDS_xmlrpc-c-devel = "bash libcurl-devel openssl-devel pkgconf-pkg-config xmlrpc-c xmlrpc-c-c++ xmlrpc-c-client xmlrpc-c-client++"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xmlrpc-c-1.51.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xmlrpc-c-client-1.51.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlrpc-c-c++-1.51.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlrpc-c-client++-1.51.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlrpc-c-devel-1.51.0-5.el8.x86_64.rpm \
          "

SRC_URI[xmlrpc-c.sha256sum] = "60b280ae2df0245d57053ea113b3ea5c17ca8178ddb57ecac3bbcbf87cbc881b"
SRC_URI[xmlrpc-c-c++.sha256sum] = "84fdbe009aa29c04ca75e90fd41d42e641fcf6d7f4feb3f24a1dea12f8311514"
SRC_URI[xmlrpc-c-client.sha256sum] = "f634f746b09ebbf791ff00370dd2cc0006fdc4fd4f257ee5cae40d74b2f9a119"
SRC_URI[xmlrpc-c-client++.sha256sum] = "533919cf2813387a5b2cd8dd6705888c5384640f55090473443bd39b7d1e93a6"
SRC_URI[xmlrpc-c-devel.sha256sum] = "30e5a2d3515f0532b265b060731a66544c1cecfc72fd27d491cfd2cce921a8a3"
