SUMMARY = "generated recipe based on libXcursor srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxfixes libxrender pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXcursor = "libXcursor.so.1"
RPM_SONAME_REQ_libXcursor = "libX11.so.6 libXfixes.so.3 libXrender.so.1 libc.so.6"
RDEPENDS_libXcursor = "glibc libX11 libXfixes libXrender"
RPM_SONAME_REQ_libXcursor-devel = "libXcursor.so.1"
RPROVIDES_libXcursor-devel = "libXcursor-dev (= 1.1.15)"
RDEPENDS_libXcursor-devel = "libX11-devel libXcursor libXfixes-devel libXrender-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXcursor-1.1.15-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXcursor-devel-1.1.15-3.el8.x86_64.rpm \
          "

SRC_URI[libXcursor.sha256sum] = "6d916871352bd0448efdfed35f3597f3f3a04591ca9f97e47af30ee1fddd5418"
SRC_URI[libXcursor-devel.sha256sum] = "8ffa3851c01ff570957166cf55c213d11bd4eebdd11ba3bcb4d1918551c4c59d"
