SUMMARY = "generated recipe based on ibus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus dbus-libs dconf fontconfig freetype gdk-pixbuf glib-2.0 gobject-introspection gtk+3 gtk2 libnotify libx11 libxi libxkbcommon pango pkgconfig-native wayland"
RPM_SONAME_REQ_ibus = "libX11.so.6 libXi.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdconf.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libibus-1.0.so.5 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus = "atk bash cairo cairo-gobject chkconfig dbus-x11 dconf desktop-file-utils gdk-pixbuf2 glib2 glibc gtk3 ibus-gtk2 ibus-gtk3 ibus-libs ibus-setup iso-codes libX11 libXi libnotify pango platform-python python3-gobject xorg-x11-xinit xorg-x11-xkb-utils"
RPM_SONAME_REQ_ibus-devel = "libibus-1.0.so.5"
RPROVIDES_ibus-devel = "ibus-dev (= 1.5.19)"
RDEPENDS_ibus-devel = "dbus-devel glib2-devel gobject-introspection-devel ibus-libs pkgconf-pkg-config vala"
RPM_SONAME_REQ_ibus-gtk2 = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdbus-1.so.3 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libibus-1.0.so.5 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus-gtk2 = "atk cairo dbus-libs fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 ibus-libs pango"
RPM_SONAME_REQ_ibus-gtk3 = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libibus-1.0.so.5 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus-gtk3 = "atk cairo cairo-gobject dbus-libs gdk-pixbuf2 glib2 glibc gtk3 ibus-libs pango"
RPM_SONAME_PROV_ibus-libs = "libibus-1.0.so.5"
RPM_SONAME_REQ_ibus-libs = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_ibus-libs = "dbus glib2 glibc gobject-introspection"
RDEPENDS_ibus-setup = "bash ibus platform-python python3-gobject"
RPM_SONAME_REQ_ibus-wayland = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 libpthread.so.0 libwayland-client.so.0 libxkbcommon.so.0"
RDEPENDS_ibus-wayland = "glib2 glibc ibus-libs libwayland-client libxkbcommon"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-1.5.19-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-gtk2-1.5.19-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-gtk3-1.5.19-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-libs-1.5.19-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-setup-1.5.19-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-wayland-1.5.19-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ibus-devel-1.5.19-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ibus-devel-docs-1.5.19-11.el8.noarch.rpm \
          "

SRC_URI[ibus.sha256sum] = "675ab004caef1c893d2aa4679d3cc737dc087d266b977dc387f3425dab8c7937"
SRC_URI[ibus-devel.sha256sum] = "767ee80586ae541f73f91713041a7c264e23cfc0fbe9e8b8e52ac1cb3b3a39d7"
SRC_URI[ibus-devel-docs.sha256sum] = "162d8e3ff2e630b982eb9e1f8da5dabef099e46f05bf93c5cc98cbce129d1e1a"
SRC_URI[ibus-gtk2.sha256sum] = "9e84d7b19b47ed78e40a40040e24dd4ec0f61e398c23b28b8a9bbee29c192c71"
SRC_URI[ibus-gtk3.sha256sum] = "7d56a193f43b35579552b5538d9ce4c9aab3f98df8f504923bde9903df5f1b5e"
SRC_URI[ibus-libs.sha256sum] = "2be39c5686b16bdc3d8f4b840886920d6a7fae5bd8eb283db2efe85fdbf9c2d3"
SRC_URI[ibus-setup.sha256sum] = "53f2f2be07182f9b09ba0c6967a3d2d217278ac62075cda20eada6e808f451ea"
SRC_URI[ibus-wayland.sha256sum] = "a4c4e869df1a2f915b542c0a5506c983f104ab4c4a212561f89da18c30b7de4c"
