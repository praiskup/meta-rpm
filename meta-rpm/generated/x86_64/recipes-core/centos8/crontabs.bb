SUMMARY = "generated recipe based on crontabs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_crontabs = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/crontabs-1.11-16.20150630git.el8.noarch.rpm \
          "

SRC_URI[crontabs.sha256sum] = "57a0a20b081bf0e24fb60146e3a004ff64ab5d55ea0d491c3920c76d4ddac2fe"
