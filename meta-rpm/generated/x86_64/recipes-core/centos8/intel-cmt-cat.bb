SUMMARY = "generated recipe based on intel-cmt-cat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_intel-cmt-cat = "libpqos.so.2"
RPM_SONAME_REQ_intel-cmt-cat = "libc.so.6 libpqos.so.2 libpthread.so.0"
RDEPENDS_intel-cmt-cat = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/intel-cmt-cat-2.0.0-2.el8.x86_64.rpm \
          "

SRC_URI[intel-cmt-cat.sha256sum] = "52cf5bd88f69a7cf81f10d1d2a958955973b458ce901e512999f70b535d9bf81"
