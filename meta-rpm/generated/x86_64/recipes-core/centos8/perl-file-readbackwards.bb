SUMMARY = "generated recipe based on perl-File-ReadBackwards srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-ReadBackwards = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-File-ReadBackwards-1.05-11.el8.noarch.rpm \
          "

SRC_URI[perl-File-ReadBackwards.sha256sum] = "b8083272492f3f84526da5d1b421649efccfd6304c0f42d12ef124fab68fb585"
