SUMMARY = "generated recipe based on virt-who srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_virt-who = "bash openssl platform-python platform-python-setuptools python3-cryptography python3-libvirt python3-pyyaml python3-requests python3-six python3-subscription-manager-rhsm python3-suds python3-systemd systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/virt-who-0.27.6-1.el8.noarch.rpm \
          "

SRC_URI[virt-who.sha256sum] = "faba64d114f80c35c18db2d3d328487d11ee7b088766d68eba8295b9c8234e7d"
