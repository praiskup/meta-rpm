SUMMARY = "generated recipe based on mcelog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mcelog = "libc.so.6"
RDEPENDS_mcelog = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mcelog-165-0.el8.x86_64.rpm \
          "

SRC_URI[mcelog.sha256sum] = "2a5e19fb87282d8774884999d03c48abbe83189bc1a87ef074b0f912e404bf75"
