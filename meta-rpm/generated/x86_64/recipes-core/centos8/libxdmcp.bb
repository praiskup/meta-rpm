SUMMARY = "generated recipe based on libXdmcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xdmcp"
DEPENDS = "pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXdmcp = "libXdmcp.so.6"
RPM_SONAME_REQ_libXdmcp = "libc.so.6"
RDEPENDS_libXdmcp = "glibc"
RPM_SONAME_REQ_libXdmcp-devel = "libXdmcp.so.6"
RPROVIDES_libXdmcp-devel = "libXdmcp-dev (= 1.1.2)"
RDEPENDS_libXdmcp-devel = "libXdmcp pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXdmcp-1.1.2-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libXdmcp-devel-1.1.2-11.el8.x86_64.rpm \
          "

SRC_URI[libXdmcp.sha256sum] = "334c9c7807c0f5dd57e74f3c2919487794f403d943c6f8e68251d8a9a31f3f73"
SRC_URI[libXdmcp-devel.sha256sum] = "1e2b7ca0db5463293c94728f41f4e8dbaae78b89378b07ad53a2462d10943d94"
