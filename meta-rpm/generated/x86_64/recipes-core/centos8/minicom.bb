SUMMARY = "generated recipe based on minicom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lockdev ncurses pkgconfig-native"
RPM_SONAME_REQ_minicom = "libc.so.6 liblockdev.so.1 libtinfo.so.6"
RDEPENDS_minicom = "bash glibc lockdev lrzsz ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/minicom-2.7.1-9.el8.x86_64.rpm \
          "

SRC_URI[minicom.sha256sum] = "922d4b30120a44a6aed05011c36682d7424f451e2109bc326317641205f10a4c"
