SUMMARY = "generated recipe based on p11-kit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi libtasn1 pkgconfig-native systemd-libs"
RPM_SONAME_PROV_p11-kit = "libp11-kit.so.0"
RPM_SONAME_REQ_p11-kit = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libffi.so.6 libp11-kit.so.0 libpthread.so.0"
RDEPENDS_p11-kit = "glibc libffi"
RPM_SONAME_REQ_p11-kit-devel = "libp11-kit.so.0"
RPROVIDES_p11-kit-devel = "p11-kit-dev (= 0.23.14)"
RDEPENDS_p11-kit-devel = "p11-kit pkgconf-pkg-config"
RPM_SONAME_REQ_p11-kit-server = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libffi.so.6 libp11-kit.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_p11-kit-server = "glibc libffi p11-kit systemd-libs"
RPM_SONAME_REQ_p11-kit-trust = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libffi.so.6 libp11-kit.so.0 libpthread.so.0 libtasn1.so.6"
RDEPENDS_p11-kit-trust = "bash chkconfig glibc libffi libtasn1 p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/p11-kit-0.23.14-5.el8_0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/p11-kit-devel-0.23.14-5.el8_0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/p11-kit-server-0.23.14-5.el8_0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/p11-kit-trust-0.23.14-5.el8_0.x86_64.rpm \
          "

SRC_URI[p11-kit.sha256sum] = "9480d4e918a56c484bcb329fa4dad0f208b1222467a2de7e1b67fcc4a1068c6f"
SRC_URI[p11-kit-devel.sha256sum] = "a9df10d0d41b7cf280e4c7a7610b6e0b533b5a05e80667337c8aa2cead3eec3c"
SRC_URI[p11-kit-server.sha256sum] = "003be01fbd00c5155dd6c6fe7335a87f737283905bc82179bfee2680da64976b"
SRC_URI[p11-kit-trust.sha256sum] = "297f3b3f427214d996f9a3c9d0a5e2ace1b1f30da770f69ca2e56bb074e5d434"
