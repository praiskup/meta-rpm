SUMMARY = "generated recipe based on perl-Test-Deep srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Deep = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Deep-1.127-4.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Deep.sha256sum] = "d814f242114e7166ff3e47706c316b9c0bfb5f63dc5e2892f6c837cd1cffb253"
