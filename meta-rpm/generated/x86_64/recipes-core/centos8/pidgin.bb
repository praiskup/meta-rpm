SUMMARY = "generated recipe based on pidgin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk avahi avahi-libs cairo cyrus-sasl-lib dbus dbus-glib dbus-glib-devel dbus-libs farstream02 fontconfig freetype gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gtk2 gtkspell libice libidn libsm libx11 libxext libxml2 libxscrnsaver meanwhile nspr nss pango pkgconfig-native"
RPM_SONAME_PROV_libpurple = "libpurple-client.so.0 libpurple.so.0"
RPM_SONAME_REQ_libpurple = "libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libfarstream-0.2.so.5 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libidn.so.11 libm.so.6 libmeanwhile.so.1 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libpurple-client.so.0 libresolv.so.2 libsasl2.so.3 libsmime3.so libssl3.so libxml2.so.2"
RDEPENDS_libpurple = "avahi-glib avahi-libs bash ca-certificates cyrus-sasl-lib cyrus-sasl-md5 cyrus-sasl-plain cyrus-sasl-scram dbus-glib dbus-libs farstream02 glib2 glibc gstreamer1 gstreamer1-plugins-base libidn libxml2 meanwhile nspr nss nss-util platform-python python3-dbus"
RPM_SONAME_REQ_libpurple-devel = "libpurple-client.so.0 libpurple.so.0"
RPROVIDES_libpurple-devel = "libpurple-dev (= 2.13.0)"
RDEPENDS_libpurple-devel = "dbus-devel dbus-glib-devel glib2-devel libpurple pkgconf-pkg-config"
RPM_SONAME_REQ_pidgin = "libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXss.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdbus-1.so.3 libdbus-glib-1.so.2 libfarstream-0.2.so.5 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libgtkspell.so.0 libidn.so.11 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libpurple.so.0 libresolv.so.2 libxml2.so.2"
RDEPENDS_pidgin = "GConf2 atk bash cairo dbus-glib dbus-libs farstream02 fontconfig freetype gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-bad-free gstreamer1-plugins-base gstreamer1-plugins-good gtk2 gtkspell libICE libSM libX11 libXScrnSaver libXext libidn libpurple libxml2 pango xdg-utils"
RPROVIDES_pidgin-devel = "pidgin-dev (= 2.13.0)"
RDEPENDS_pidgin-devel = "gtk2-devel libpurple-devel pidgin pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpurple-2.13.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pidgin-2.13.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpurple-devel-2.13.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pidgin-devel-2.13.0-5.el8.x86_64.rpm \
          "

SRC_URI[libpurple.sha256sum] = "ecd8175accd82880585895d5a8364c167212e70c97bcc74aa8b23ed649cc8a94"
SRC_URI[libpurple-devel.sha256sum] = "b0361ddc9c020044a2f0a66d1e3e5c8dd886e91b0604e6f2a9c99d3a0bdd9c78"
SRC_URI[pidgin.sha256sum] = "8e4ee8a194c53db12af75954fd4ad49112ef79d9467cce304536365df3f13d58"
SRC_URI[pidgin-devel.sha256sum] = "ed1fc7754310a9d2322377f0061faa5f25c87290a52cdc71e8692136367f97bb"
