SUMMARY = "generated recipe based on perl-Class-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Tiny = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Tiny-1.006-6.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Tiny.sha256sum] = "176b5fcffb752fd689ec786b856f7f8c6eebd57012bbccc99e3bb42711e2a101"
