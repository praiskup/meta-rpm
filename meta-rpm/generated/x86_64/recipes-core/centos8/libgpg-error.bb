SUMMARY = "generated recipe based on libgpg-error srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgpg-error = "libgpg-error.so.0"
RPM_SONAME_REQ_libgpg-error = "libc.so.6 libgpg-error.so.0"
RDEPENDS_libgpg-error = "glibc"
RPM_SONAME_REQ_libgpg-error-devel = "libc.so.6 libgpg-error.so.0"
RPROVIDES_libgpg-error-devel = "libgpg-error-dev (= 1.31)"
RDEPENDS_libgpg-error-devel = "bash glibc info libgpg-error"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgpg-error-1.31-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgpg-error-devel-1.31-1.el8.x86_64.rpm \
          "

SRC_URI[libgpg-error.sha256sum] = "845a0732d9d7a01b909124cd8293204764235c2d856227c7a74dfa0e38113e34"
SRC_URI[libgpg-error-devel.sha256sum] = "e8f901f5d7a9ab6a312d183ec537127b94b58b16947e73df93ae282c727cd7c4"
