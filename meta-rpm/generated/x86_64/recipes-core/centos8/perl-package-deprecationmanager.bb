SUMMARY = "generated recipe based on perl-Package-DeprecationManager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Package-DeprecationManager = "perl-Carp perl-Package-Stash perl-Params-Util perl-Scalar-List-Utils perl-Sub-Install perl-Sub-Name perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Package-DeprecationManager-0.17-5.el8.noarch.rpm \
          "

SRC_URI[perl-Package-DeprecationManager.sha256sum] = "dcd63425e2a56f3fb174a9a7301e97a75c5ede03ad2a4295fcde6b87cec42946"
