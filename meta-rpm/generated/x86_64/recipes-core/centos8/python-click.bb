SUMMARY = "generated recipe based on python-click srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-click = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-click-6.7-8.el8.noarch.rpm \
          "

SRC_URI[python3-click.sha256sum] = "30d6fd875b7c5a1a684c76ef71254c127edd7b85e863bc122f71d74f87e5cf70"
