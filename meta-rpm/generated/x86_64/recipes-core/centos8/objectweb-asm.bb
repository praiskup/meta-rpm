SUMMARY = "generated recipe based on objectweb-asm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_objectweb-asm = "bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"
RDEPENDS_objectweb-asm-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/objectweb-asm-6.2-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/objectweb-asm-javadoc-6.2-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[objectweb-asm.sha256sum] = "45e3cb4215868cf131673604983653478d4689d3f6696223405c318bfc3d605b"
SRC_URI[objectweb-asm-javadoc.sha256sum] = "22d3adad7dd231ad06ebae10e6b35005830994f7e25f85a83713c0700693935d"
