SUMMARY = "generated recipe based on libedit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_PROV_libedit = "libedit.so.0"
RPM_SONAME_REQ_libedit = "libc.so.6 libtinfo.so.6"
RDEPENDS_libedit = "glibc ncurses-libs"
RPM_SONAME_REQ_libedit-devel = "libedit.so.0"
RPROVIDES_libedit-devel = "libedit-dev (= 3.1)"
RDEPENDS_libedit-devel = "libedit ncurses-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libedit-3.1-23.20170329cvs.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libedit-devel-3.1-23.20170329cvs.el8.x86_64.rpm \
          "

SRC_URI[libedit.sha256sum] = "08b76b0af07db4d3b27776a96bb7d0dc0f02b7219569676ac79036190d731d5b"
SRC_URI[libedit-devel.sha256sum] = "701e4fa0254fc64fdafe2e0a4cbc01d8bd31972e8d7e9ddeb817c25ec5dbbd9c"
