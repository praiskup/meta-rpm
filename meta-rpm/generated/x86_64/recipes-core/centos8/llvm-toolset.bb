SUMMARY = "generated recipe based on llvm-toolset srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_llvm-toolset = "clang lld lldb llvm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-toolset-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
          "

SRC_URI[llvm-toolset.sha256sum] = "a8b219cd1f852f2ee515c753b4cd52f3f757ba4708e93e66f1057b3dca80f7de"
