SUMMARY = "generated recipe based on gnome-backgrounds srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-backgrounds-3.28.0-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-backgrounds-extras-3.28.0-1.el8.noarch.rpm \
          "

SRC_URI[gnome-backgrounds.sha256sum] = "618beff1118b57c01e2d02dc4c17e633a3faf633c0d9ac209707d4ea1c49ff8f"
SRC_URI[gnome-backgrounds-extras.sha256sum] = "789673355c71d788531743879a75827b2552558231921b8f30cf31a5833978e0"
