SUMMARY = "generated recipe based on hunspell-es srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-es = "hunspell hunspell-es-AR hunspell-es-BO hunspell-es-CL hunspell-es-CO hunspell-es-CR hunspell-es-CU hunspell-es-DO hunspell-es-EC hunspell-es-ES hunspell-es-GT hunspell-es-HN hunspell-es-MX hunspell-es-NI hunspell-es-PA hunspell-es-PE hunspell-es-PR hunspell-es-PY hunspell-es-SV hunspell-es-US hunspell-es-UY hunspell-es-VE"
RDEPENDS_hunspell-es-AR = "hunspell"
RDEPENDS_hunspell-es-BO = "hunspell"
RDEPENDS_hunspell-es-CL = "hunspell"
RDEPENDS_hunspell-es-CO = "hunspell"
RDEPENDS_hunspell-es-CR = "hunspell"
RDEPENDS_hunspell-es-CU = "hunspell"
RDEPENDS_hunspell-es-DO = "hunspell"
RDEPENDS_hunspell-es-EC = "hunspell"
RDEPENDS_hunspell-es-ES = "hunspell"
RDEPENDS_hunspell-es-GT = "hunspell"
RDEPENDS_hunspell-es-HN = "hunspell"
RDEPENDS_hunspell-es-MX = "hunspell"
RDEPENDS_hunspell-es-NI = "hunspell"
RDEPENDS_hunspell-es-PA = "hunspell"
RDEPENDS_hunspell-es-PE = "hunspell"
RDEPENDS_hunspell-es-PR = "hunspell"
RDEPENDS_hunspell-es-PY = "hunspell"
RDEPENDS_hunspell-es-SV = "hunspell"
RDEPENDS_hunspell-es-US = "hunspell"
RDEPENDS_hunspell-es-UY = "hunspell"
RDEPENDS_hunspell-es-VE = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-AR-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-BO-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-CL-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-CO-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-CR-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-CU-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-DO-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-EC-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-ES-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-GT-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-HN-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-MX-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-NI-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-PA-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-PE-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-PR-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-PY-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-SV-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-US-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-UY-2.3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-es-VE-2.3-2.el8.noarch.rpm \
          "

SRC_URI[hunspell-es.sha256sum] = "84dbbda5736cf5809bd0f5d5fb4fe909e849dd7199904e544da5f94f86c5274c"
SRC_URI[hunspell-es-AR.sha256sum] = "ac521cc783de567d0f496a998eb9e022b06c4b1d76cd9b42af72d6bf3de4b951"
SRC_URI[hunspell-es-BO.sha256sum] = "d7448a6093343d5be71f94bd9a28cf8d8515170eebd17f47d7d0f6be69920bd2"
SRC_URI[hunspell-es-CL.sha256sum] = "2f94214555ee6394ab9d391cba7c236f40e72583ba8b655190a7da8ed6c384cc"
SRC_URI[hunspell-es-CO.sha256sum] = "4f3f4b5003acabe196affc5aadfd8c9eaadb8af2c7850eec28343eca043b32d9"
SRC_URI[hunspell-es-CR.sha256sum] = "3c671b05103870a7855dbf6c5bf67d62912404f46274785baa8786c84169e19b"
SRC_URI[hunspell-es-CU.sha256sum] = "03cd4046b76ddd73d45e17229009dfd9b8c405b276fe3e356c8126beb0983fc6"
SRC_URI[hunspell-es-DO.sha256sum] = "d5310063ac426905858f6540f3f644d6ef231307fb84dfe6351f5ece48370640"
SRC_URI[hunspell-es-EC.sha256sum] = "3a77ca42efb7af8b1f5021c5f9843495603d443fa205551bacfdbdf1154f63cd"
SRC_URI[hunspell-es-ES.sha256sum] = "6b4b0bb878e7ac7e03ac0ecb1e7d3d684cdbba48dddae6d8d1be96d7d1221a7d"
SRC_URI[hunspell-es-GT.sha256sum] = "cbbf97f85a1c23523752882c41b9d85eb8a6909c9e5e69ef2831133fdc597f31"
SRC_URI[hunspell-es-HN.sha256sum] = "5c4444b40e8f57f5a02614cdb0169963143ab56b2b5e2f6b196e2961efb15fdc"
SRC_URI[hunspell-es-MX.sha256sum] = "b900401fd322ca240678390ade1cd5555c6b7a1c0225395f250444246aa532b5"
SRC_URI[hunspell-es-NI.sha256sum] = "f5e9c6784bea51e2ccb87dd0b6c975cbb717717d8ace4f9ee1f3968e617b914a"
SRC_URI[hunspell-es-PA.sha256sum] = "f18ef6bd92e61a545ab539b47b889849afeb77a85dca84b15207227c0352041a"
SRC_URI[hunspell-es-PE.sha256sum] = "c6b619fd18c5cf0e3be1aca504545877197344e53bfea0d18b1aba584ae90812"
SRC_URI[hunspell-es-PR.sha256sum] = "056e3ab35d0d100e292a9a7575d32313ee44502b16dda4df8fd909778a970270"
SRC_URI[hunspell-es-PY.sha256sum] = "b072184c4844d6f7e045a57e24cc3d033f32baf88482802498c9724fc587f5d6"
SRC_URI[hunspell-es-SV.sha256sum] = "017c4091d993d923e181346c70b178fc4f874c1999d63a044d06c101ae1c7a27"
SRC_URI[hunspell-es-US.sha256sum] = "5e44669ebc03ae6bd0f02a46bdbcf4dfec047b1ff8723f2b100671f7bd34d657"
SRC_URI[hunspell-es-UY.sha256sum] = "d10f57307010a112ba7ab5394200aca7d6f37a3960c0ff46471dbf4c9109f2a7"
SRC_URI[hunspell-es-VE.sha256sum] = "686efe42d5b7974f3e3f4d8d937a751116606bb8e8cd07540892a0bf51376872"
