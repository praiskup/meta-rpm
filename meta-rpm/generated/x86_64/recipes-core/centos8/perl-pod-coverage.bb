SUMMARY = "generated recipe based on perl-Pod-Coverage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Coverage = "perl-Devel-Symdump perl-Pod-Parser perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Pod-Coverage-0.23-14.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Coverage.sha256sum] = "9d318d36b329a8aea39b971dc1d776d413dd9747274ca3971ed2cecef25dfdf1"
