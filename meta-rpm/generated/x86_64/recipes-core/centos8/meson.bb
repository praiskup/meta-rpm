SUMMARY = "generated recipe based on meson srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ninja pkgconfig-native"
RDEPENDS_meson = "ninja-build platform-python python36-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/meson-0.49.2-1.el8.noarch.rpm \
          "

SRC_URI[meson.sha256sum] = "90ec7222a5080135dad9eec1880deb2a7c5e91678305d8db30aad8aea3f0e3b9"
