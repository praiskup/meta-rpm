SUMMARY = "generated recipe based on enca srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_enca = "libenca.so.0"
RPM_SONAME_REQ_enca = "libc.so.6 libenca.so.0 libm.so.6"
RDEPENDS_enca = "glibc"
RPM_SONAME_REQ_enca-devel = "libenca.so.0"
RPROVIDES_enca-devel = "enca-dev (= 1.19)"
RDEPENDS_enca-devel = "enca pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/enca-1.19-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/enca-devel-1.19-1.el8.x86_64.rpm \
          "

SRC_URI[enca.sha256sum] = "4f18d8dc92c3f2080389c016f3c0026af7b1efcf8d38a7cf90b44df2b6acc7ac"
SRC_URI[enca-devel.sha256sum] = "4e7d0c2d1b8aba91a174f7cc63394f7bff2642a9242a4644205dd44aea82660a"
