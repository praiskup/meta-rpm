SUMMARY = "generated recipe based on xorg-x11-drv-nouveau srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libdrm pkgconfig-native systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-nouveau = "libc.so.6 libdrm.so.2 libdrm_nouveau.so.2 libudev.so.1"
RDEPENDS_xorg-x11-drv-nouveau = "glibc libdrm systemd-libs xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-nouveau-1.0.15-4.el8.1.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-nouveau.sha256sum] = "9fe666ad3a177ac56e647ce2d31a5bc64af4a59e17464d3715788a8df3c0f278"
