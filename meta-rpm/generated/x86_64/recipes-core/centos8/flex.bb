SUMMARY = "generated recipe based on flex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_flex = "libc.so.6 libm.so.6"
RDEPENDS_flex = "bash glibc info m4"
RPROVIDES_flex-devel = "flex-dev (= 2.6.1)"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flex-2.6.1-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flex-doc-2.6.1-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/flex-devel-2.6.1-9.el8.x86_64.rpm \
          "

SRC_URI[flex.sha256sum] = "ac1749d762df1d181facd3ffce898899ebf7b9bb9836988f520f9302c52582e0"
SRC_URI[flex-devel.sha256sum] = "55352342f23932c7a5053f3155f9ab29f5d59ae105f13ea3d714b5a0598b94ac"
SRC_URI[flex-doc.sha256sum] = "bec02bcdbaa49680014e49ba8c2f31a35def30e678eb40e317c33cdfc4cf3c33"
