SUMMARY = "generated recipe based on libdv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdv = "libdv.so.4"
RPM_SONAME_REQ_libdv = "libc.so.6 libm.so.6"
RDEPENDS_libdv = "glibc"
RPM_SONAME_REQ_libdv-devel = "libdv.so.4"
RPROVIDES_libdv-devel = "libdv-dev (= 1.0.0)"
RDEPENDS_libdv-devel = "libdv pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdv-1.0.0-27.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdv-devel-1.0.0-27.el8.x86_64.rpm \
          "

SRC_URI[libdv.sha256sum] = "c3f17c047e3899b712864a6c2bcdd425145837d50f2cef0ec662fd3f98bdb82e"
SRC_URI[libdv-devel.sha256sum] = "4487f76f6badd1de2213982166e30767f66041760f6a2d620e7d5d69972f246f"
