SUMMARY = "generated recipe based on gnome-shell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "at-spi2-atk atk cairo evolution-data-server gcr gdk-pixbuf gjs glib-2.0 gobject-introspection gstreamer1.0 gtk+3 libcroco libgcc libglvnd libical libsecret libx11 libxfixes mutter networkmanager pango pkgconfig-native polkit pulseaudio systemd-libs"
RPM_SONAME_PROV_gnome-shell = "libgnome-shell-menu.so libgnome-shell.so libgvc.so libst-1.0.so"
RPM_SONAME_REQ_gnome-shell = "libGLESv2.so.2 libX11.so.6 libXfixes.so.3 libatk-1.0.so.0 libatk-bridge-2.0.so.0 libc.so.6 libcairo.so.2 libcroco-0.6.so.3 libecal-1.2.so.19 libedataserver-1.2.so.23 libgcc_s.so.1 libgcr-base-3.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libgjs.so.0 libglib-2.0.so.0 libgnome-shell-menu.so libgnome-shell.so libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgtk-3.so.0 libical.so.3 libm.so.6 libmutter-4.so.0 libmutter-clutter-4.so.0 libmutter-cogl-4.so.0 libmutter-cogl-pango-4.so.0 libnm.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsecret-1.so.0 libst-1.0.so libsystemd.so.0"
RDEPENDS_gnome-shell = "NetworkManager-libnm accountsservice-libs at-spi2-atk atk bash bolt cairo evolution-data-server gcr gdk-pixbuf2 gdm geoclue2-libs gjs glib2 glibc gnome-bluetooth gnome-control-center gnome-desktop3 gnome-session-xsession gobject-introspection gsettings-desktop-schemas gstreamer1 gtk3 ibus libX11 libXfixes libcroco libgcc libglvnd-gles libgweather libical libnma librsvg2 libsecret mutter pango platform-python polkit polkit-libs pulseaudio-libs pulseaudio-libs-glib2 python36 switcheroo-control systemd-libs upower xdg-desktop-portal-gtk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-shell-3.32.2-14.el8.x86_64.rpm \
          "

SRC_URI[gnome-shell.sha256sum] = "003648e074cf199be3caaa56e60a239d90d75ae56de8791546ff923580ce021d"
