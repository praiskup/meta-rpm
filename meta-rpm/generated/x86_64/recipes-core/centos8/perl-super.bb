SUMMARY = "generated recipe based on perl-SUPER srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-SUPER = "perl-Carp perl-Scalar-List-Utils perl-Sub-Identify perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-SUPER-1.20141117-10.el8.noarch.rpm \
          "

SRC_URI[perl-SUPER.sha256sum] = "216cf14692a627097f595e48fe3606a4335bee8ee09e7554e2bc10edd9d118d4"
