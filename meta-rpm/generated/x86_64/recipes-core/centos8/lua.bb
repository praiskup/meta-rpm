SUMMARY = "generated recipe based on lua srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_lua = "libc.so.6 libdl.so.2 liblua-5.3.so libm.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_lua = "glibc lua-libs ncurses-libs readline"
RPROVIDES_lua-devel = "lua-dev (= 5.3.4)"
RDEPENDS_lua-devel = "lua pkgconf-pkg-config"
RPM_SONAME_PROV_lua-libs = "liblua-5.3.so"
RPM_SONAME_REQ_lua-libs = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_lua-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lua-5.3.4-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lua-libs-5.3.4-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lua-devel-5.3.4-11.el8.x86_64.rpm \
          "

SRC_URI[lua.sha256sum] = "4df0182698fc1f4d8cc467c50e64a8327ae2787f81c3f19bdc74c3ffc1e2a857"
SRC_URI[lua-devel.sha256sum] = "29160c9f523527cfff3e13c3eb86185430b0714582c180fc7a4665fd3d2c2fac"
SRC_URI[lua-libs.sha256sum] = "98a5f610c2ca116fa63f98302036eaa8ca725c1e8fd7afae4a285deb50605b35"
