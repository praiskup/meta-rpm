SUMMARY = "generated recipe based on perl-Ref-Util-XS srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Ref-Util-XS = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Ref-Util-XS = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Ref-Util-XS-0.117-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Ref-Util-XS.sha256sum] = "205f633b235679b21bd9386760595aa4d5a8224c354537fbf38d83c099a02c20"
