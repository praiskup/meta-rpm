SUMMARY = "generated recipe based on xmlsec1 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcrypt libgpg-error libtool libxml2 libxslt nspr nss openssl pkgconfig-native"
RPM_SONAME_PROV_xmlsec1 = "libxmlsec1.so.1"
RPM_SONAME_REQ_xmlsec1 = "libc.so.6 libltdl.so.7 libm.so.6 libxml2.so.2 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1 = "glibc libtool-ltdl libxml2 libxslt"
RPM_SONAME_REQ_xmlsec1-devel = "libxmlsec1.so.1"
RPROVIDES_xmlsec1-devel = "xmlsec1-dev (= 1.2.25)"
RDEPENDS_xmlsec1-devel = "bash libxml2-devel libxslt-devel openssl-devel pkgconf-pkg-config xmlsec1"
RPM_SONAME_PROV_xmlsec1-gcrypt = "libxmlsec1-gcrypt.so.1"
RPM_SONAME_REQ_xmlsec1-gcrypt = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libltdl.so.7 libm.so.6 libxml2.so.2 libxmlsec1-gcrypt.so.1 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-gcrypt = "glibc libgcrypt libgpg-error libtool-ltdl libxml2 libxslt xmlsec1"
RPM_SONAME_PROV_xmlsec1-gnutls = "libxmlsec1-gnutls.so.1"
RPM_SONAME_REQ_xmlsec1-gnutls = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgnutls.so.30 libgpg-error.so.0 libltdl.so.7 libm.so.6 libxml2.so.2 libxmlsec1-gcrypt.so.1 libxmlsec1-gnutls.so.1 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-gnutls = "glibc gnutls libgcrypt libgpg-error libtool-ltdl libxml2 libxslt xmlsec1 xmlsec1-gcrypt"
RPROVIDES_xmlsec1-gnutls-devel = "xmlsec1-gnutls-dev (= 1.2.25)"
RDEPENDS_xmlsec1-gnutls-devel = "gnutls-devel libgcrypt-devel libxml2-devel libxslt-devel pkgconf-pkg-config xmlsec1-devel xmlsec1-openssl-devel"
RPM_SONAME_PROV_xmlsec1-nss = "libxmlsec1-nss.so.1"
RPM_SONAME_REQ_xmlsec1-nss = "libc.so.6 libdl.so.2 libltdl.so.7 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libxml2.so.2 libxmlsec1-nss.so.1 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-nss = "glibc libtool-ltdl libxml2 libxslt nspr nss nss-util xmlsec1"
RPM_SONAME_PROV_xmlsec1-openssl = "libxmlsec1-openssl.so.1"
RPM_SONAME_REQ_xmlsec1-openssl = "libc.so.6 libcrypto.so.1.1 libltdl.so.7 libm.so.6 libssl.so.1.1 libxml2.so.2 libxmlsec1-openssl.so.1 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-openssl = "glibc libtool-ltdl libxml2 libxslt openssl-libs xmlsec1"
RPROVIDES_xmlsec1-openssl-devel = "xmlsec1-openssl-dev (= 1.2.25)"
RDEPENDS_xmlsec1-openssl-devel = "libxml2-devel libxslt-devel pkgconf-pkg-config xmlsec1-devel xmlsec1-openssl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xmlsec1-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xmlsec1-nss-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xmlsec1-openssl-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlsec1-devel-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlsec1-gcrypt-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlsec1-gnutls-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlsec1-gnutls-devel-1.2.25-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlsec1-openssl-devel-1.2.25-4.el8.x86_64.rpm \
          "

SRC_URI[xmlsec1.sha256sum] = "7dbe1fb48dad46fe75609974503f5647b644a9dc2c88b785aabd83b4eda0b881"
SRC_URI[xmlsec1-devel.sha256sum] = "35108e135289fa973ede0ff83c44e35d2739a71f4eb2c0dad8271f68a100635c"
SRC_URI[xmlsec1-gcrypt.sha256sum] = "3ce84a3ad5a2ef0616c283d1784201924f513e7e4fa80b595af4985650960ac6"
SRC_URI[xmlsec1-gnutls.sha256sum] = "5ad3b45790abf8a67412ed5dfaff222eecc93a8ed47a7db0b05b058346071614"
SRC_URI[xmlsec1-gnutls-devel.sha256sum] = "85fccca80732a9b71c960bcecd047fc7ef2307b211d12eefc1550498add8fd08"
SRC_URI[xmlsec1-nss.sha256sum] = "d839faf3755c4185e4bce5cfa93cf62a3d56fbcb214407813baafbacbfc1e0d7"
SRC_URI[xmlsec1-openssl.sha256sum] = "9ed3ad948a0dada7710d4b8b2053d4716dc0754f5648a74f4d78b59b737529dc"
SRC_URI[xmlsec1-openssl-devel.sha256sum] = "23c8128d610a18fd07553ea6cac057ced1e75d24f6e3de97b1264876f8287c29"
