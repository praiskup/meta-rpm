SUMMARY = "generated recipe based on mingw-zlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-zlib = "mingw32-crt mingw32-filesystem mingw32-pkg-config"
RDEPENDS_mingw32-zlib-static = "mingw32-zlib"
RDEPENDS_mingw64-zlib = "mingw64-crt mingw64-filesystem mingw64-pkg-config"
RDEPENDS_mingw64-zlib-static = "mingw64-zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-zlib-1.2.8-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-zlib-static-1.2.8-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-zlib-1.2.8-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-zlib-static-1.2.8-9.el8.noarch.rpm \
          "

SRC_URI[mingw32-zlib.sha256sum] = "9bbe37a679a2eb60777bb5125254ca4f4a275dea66de8c760971d6e5259d69ba"
SRC_URI[mingw32-zlib-static.sha256sum] = "c9e5e5933f2002aeadc2cbf8bad7a898100dc39effc3af107148b474893dac0d"
SRC_URI[mingw64-zlib.sha256sum] = "6ba9fd58945f131a6929e9f6e40789978b7432017545e450474414da963d3a66"
SRC_URI[mingw64-zlib-static.sha256sum] = "f767ea6e84f399201af21daf735ad570ad4dca3ff61e2db4624749c8597f7c6c"
