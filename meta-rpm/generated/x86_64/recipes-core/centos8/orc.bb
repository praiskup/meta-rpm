SUMMARY = "generated recipe based on orc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_orc = "liborc-0.4.so.0 liborc-test-0.4.so.0"
RPM_SONAME_REQ_orc = "libc.so.6 libm.so.6 liborc-0.4.so.0 liborc-test-0.4.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_orc = "glibc"
RPM_SONAME_REQ_orc-compiler = "libc.so.6 libm.so.6 liborc-0.4.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_orc-compiler = "glibc orc pkgconf-pkg-config"
RPM_SONAME_REQ_orc-devel = "liborc-0.4.so.0 liborc-test-0.4.so.0"
RPROVIDES_orc-devel = "orc-dev (= 0.4.28)"
RDEPENDS_orc-devel = "orc orc-compiler pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/orc-0.4.28-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/orc-compiler-0.4.28-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/orc-devel-0.4.28-3.el8.x86_64.rpm \
          "

SRC_URI[orc.sha256sum] = "7552ad64b02a15a3b91524f9858afeb228ef45148204539ad33524f7d7bc5c67"
SRC_URI[orc-compiler.sha256sum] = "3931bbd497904a9861ac197f8da767ccb331fc16d481c4e22f2d814b66c5c1e6"
SRC_URI[orc-devel.sha256sum] = "32324ddf06da140e135464b30920739bd0cf0ed6d7333cd4c6adebeb791ee556"
