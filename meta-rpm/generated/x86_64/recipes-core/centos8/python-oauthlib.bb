SUMMARY = "generated recipe based on python-oauthlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-oauthlib = "platform-python python3-cryptography python3-jwt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-oauthlib-2.1.0-1.el8.noarch.rpm \
          "

SRC_URI[python3-oauthlib.sha256sum] = "20874a9d40b12125c9e5b97fe4ccda3f94acbc03da1dec7299123901f5b56631"
