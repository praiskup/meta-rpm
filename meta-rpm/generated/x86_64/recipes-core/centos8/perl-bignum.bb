SUMMARY = "generated recipe based on perl-bignum srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-bignum = "perl-Carp perl-Exporter perl-Math-BigInt perl-Math-BigRat perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-bignum-0.49-2.el8.noarch.rpm \
          "

SRC_URI[perl-bignum.sha256sum] = "a751a4f0f88b52988235b61b72d4fb0d24547c4952425987ef3653d9d159f6ad"
