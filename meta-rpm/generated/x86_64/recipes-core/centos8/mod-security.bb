SUMMARY = "generated recipe based on mod_security srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "apr apr-util curl db expat libpcre libxml2 lua openldap pkgconfig-native xz yajl zlib"
RPM_SONAME_REQ_mod_security = "libapr-1.so.0 libaprutil-1.so.0 libc.so.6 libcurl.so.4 libdb-5.3.so libdl.so.2 libexpat.so.1 liblber-2.4.so.2 libldap_r-2.4.so.2 liblua-5.3.so liblzma.so.5 libm.so.6 libpcre.so.1 libpthread.so.0 libxml2.so.2 libyajl.so.2 libz.so.1"
RDEPENDS_mod_security = "apr apr-util expat glibc httpd httpd-filesystem libcurl libdb libxml2 lua-libs openldap pcre xz-libs yajl zlib"
RPM_SONAME_REQ_mod_security-mlogc = "libapr-1.so.0 libc.so.6 libcurl.so.4 libdl.so.2 libpcre.so.1 libpthread.so.0"
RDEPENDS_mod_security-mlogc = "apr glibc httpd-filesystem libcurl mod_security pcre perl-Digest-MD5 perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_security-2.9.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_security-mlogc-2.9.2-8.el8.x86_64.rpm \
          "

SRC_URI[mod_security.sha256sum] = "bdd83abc5956dbda19a646b08ab796a22de44211932354150b629d861833d481"
SRC_URI[mod_security-mlogc.sha256sum] = "bf083b677dd81bbc6bf559ee91afb9217716c43503c220638f42f5e7ae98fd6c"
