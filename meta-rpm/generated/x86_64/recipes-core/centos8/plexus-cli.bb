SUMMARY = "generated recipe based on plexus-cli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-cli = "apache-commons-cli java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default plexus-utils"
RDEPENDS_plexus-cli-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-cli-1.6-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-cli-javadoc-1.6-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-cli.sha256sum] = "7be3ba4a5ef4627b3c01963e9e07c23760cb96531f88022b032a74fa21e06bd9"
SRC_URI[plexus-cli-javadoc.sha256sum] = "1391f3876cdb8dcca2b9343e44aaf028315098959dacf65ba7e8ccd78f278447"
