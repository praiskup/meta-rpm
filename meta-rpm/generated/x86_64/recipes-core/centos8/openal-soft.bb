SUMMARY = "generated recipe based on openal-soft srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_openal-soft = "libopenal.so.1"
RPM_SONAME_REQ_openal-soft = "libatomic.so.1 libc.so.6 libdl.so.2 libm.so.6 libopenal.so.1 libpthread.so.0 librt.so.1"
RDEPENDS_openal-soft = "glibc libatomic"
RPM_SONAME_REQ_openal-soft-devel = "libc.so.6 libm.so.6 libopenal.so.1"
RPROVIDES_openal-soft-devel = "openal-soft-dev (= 1.18.2)"
RDEPENDS_openal-soft-devel = "cmake-filesystem glibc openal-soft pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openal-soft-1.18.2-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openal-soft-devel-1.18.2-7.el8.x86_64.rpm \
          "

SRC_URI[openal-soft.sha256sum] = "c58c2370f7f218a81e704a28d14fd6929524b9f6297f927de6d1e1c01de80cda"
SRC_URI[openal-soft-devel.sha256sum] = "226a0ee6098951823e084fbfb8d262f36dc24a091c78979cd90d647d5c74432c"
