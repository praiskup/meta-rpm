SUMMARY = "generated recipe based on mingw-harfbuzz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-harfbuzz = "mingw32-cairo mingw32-crt mingw32-filesystem mingw32-freetype mingw32-gcc mingw32-glib2 mingw32-icu mingw32-pkg-config"
RDEPENDS_mingw32-harfbuzz-static = "mingw32-glib2-static mingw32-harfbuzz"
RDEPENDS_mingw64-harfbuzz = "mingw64-cairo mingw64-crt mingw64-filesystem mingw64-freetype mingw64-gcc mingw64-glib2 mingw64-icu mingw64-pkg-config"
RDEPENDS_mingw64-harfbuzz-static = "mingw64-glib2-static mingw64-harfbuzz"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-harfbuzz-1.4.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-harfbuzz-static-1.4.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-harfbuzz-1.4.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-harfbuzz-static-1.4.8-3.el8.noarch.rpm \
          "

SRC_URI[mingw32-harfbuzz.sha256sum] = "2a2d292e2386b327ad0ef4841d5c65f41e3c1f690c4d16144cb7ce013c40963a"
SRC_URI[mingw32-harfbuzz-static.sha256sum] = "2e2a5bee24476911b340c22dea09f71024d1e741b752dbf5bb8203e682358553"
SRC_URI[mingw64-harfbuzz.sha256sum] = "986d63c96fc6ad9a0b5291ab1fc2a23b1bbf8e49d8ca5a4db99b923dddf86fd2"
SRC_URI[mingw64-harfbuzz-static.sha256sum] = "b1cb98e21bac24a9b1adc787d0f9e0f8c65c7507211a852a2eb73a59c2c39301"
