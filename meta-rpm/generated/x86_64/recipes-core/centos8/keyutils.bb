SUMMARY = "generated recipe based on keyutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_keyutils = "libc.so.6 libkeyutils.so.1 libresolv.so.2"
RDEPENDS_keyutils = "bash glibc keyutils-libs"
RPM_SONAME_PROV_keyutils-libs = "libkeyutils.so.1"
RPM_SONAME_REQ_keyutils-libs = "libc.so.6 libdl.so.2"
RDEPENDS_keyutils-libs = "glibc"
RPM_SONAME_REQ_keyutils-libs-devel = "libkeyutils.so.1"
RPROVIDES_keyutils-libs-devel = "keyutils-libs-dev (= 1.5.10)"
RDEPENDS_keyutils-libs-devel = "keyutils-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/keyutils-1.5.10-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/keyutils-libs-1.5.10-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/keyutils-libs-devel-1.5.10-6.el8.x86_64.rpm \
          "

SRC_URI[keyutils.sha256sum] = "f602f145a2deb352013006744cd47e2e1ff9a740ddbeb9412e50e801b5dbe02a"
SRC_URI[keyutils-libs.sha256sum] = "ae82944445464108e4932d18d4f915cc5e0b4763feb7337b2db7a3fdb77839e3"
SRC_URI[keyutils-libs-devel.sha256sum] = "cc0d2268f61a51e0c27de08d602db8e8604afc7c1c98fab1d8918dcd0ea56588"
