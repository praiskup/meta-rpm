SUMMARY = "generated recipe based on perl-Text-Soundex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Text-Soundex = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Text-Soundex = "glibc perl-Carp perl-Exporter perl-Text-Unidecode perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Text-Soundex-3.05-8.el8.x86_64.rpm \
          "

SRC_URI[perl-Text-Soundex.sha256sum] = "f386a60ceeec0a1308f2ccaa14406bb11a91a699680fc3581108f2fc581da0bf"
