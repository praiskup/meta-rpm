SUMMARY = "generated recipe based on flac srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libogg pkgconfig-native"
RPM_SONAME_REQ_flac = "libFLAC.so.8 libc.so.6 libm.so.6 libogg.so.0"
RDEPENDS_flac = "flac-libs glibc libogg"
RPM_SONAME_REQ_flac-devel = "libFLAC++.so.6 libFLAC.so.8"
RPROVIDES_flac-devel = "flac-dev (= 1.3.2)"
RDEPENDS_flac-devel = "flac-libs libogg-devel pkgconf-pkg-config"
RPM_SONAME_PROV_flac-libs = "libFLAC++.so.6 libFLAC.so.8"
RPM_SONAME_REQ_flac-libs = "libFLAC.so.8 libc.so.6 libgcc_s.so.1 libm.so.6 libogg.so.0 libstdc++.so.6"
RDEPENDS_flac-libs = "glibc libgcc libogg libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flac-libs-1.3.2-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/flac-1.3.2-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/flac-devel-1.3.2-9.el8.x86_64.rpm \
          "

SRC_URI[flac.sha256sum] = "27b7cce22cb194b5dc9b9582e2df9c412d3012afceaa511fcf57408feef6d9db"
SRC_URI[flac-devel.sha256sum] = "880a58fee2ac2791f1366a819aa517e588bce20bfc0e9378a7f96cb3c5c04398"
SRC_URI[flac-libs.sha256sum] = "21537df31efbfd5061c85c64ebc0a5ecb0711600abc72e47bc99e5943aaaaec8"
