SUMMARY = "generated recipe based on pmdk-convert srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_pmdk-convert = "libpmem-convert.so"
RPM_SONAME_REQ_pmdk-convert = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libpmem-convert.so libpthread.so.0"
RDEPENDS_pmdk-convert = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pmdk-convert-1.7-1.el8.x86_64.rpm \
          "

SRC_URI[pmdk-convert.sha256sum] = "ab1badd71cd4b1565afd8be0a71caf050c24f4b7e461c0ef624ee4ac2816be28"
