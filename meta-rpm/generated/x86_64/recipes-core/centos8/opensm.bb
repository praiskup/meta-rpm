SUMMARY = "generated recipe based on opensm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native rdma-core"
RPM_SONAME_REQ_opensm = "libc.so.6 libdl.so.2 libibumad.so.3 libopensm.so.9 libosmcomp.so.5 libosmvendor.so.5 libpthread.so.0"
RDEPENDS_opensm = "bash glibc libibumad logrotate opensm-libs rdma-core systemd"
RPM_SONAME_REQ_opensm-devel = "libopensm.so.9 libosmcomp.so.5 libosmvendor.so.5"
RPROVIDES_opensm-devel = "opensm-dev (= 3.3.22)"
RDEPENDS_opensm-devel = "opensm-libs"
RPM_SONAME_PROV_opensm-libs = "libopensm.so.9 libosmcomp.so.5 libosmvendor.so.5"
RPM_SONAME_REQ_opensm-libs = "libc.so.6 libdl.so.2 libgcc_s.so.1 libibumad.so.3 libopensm.so.9 libosmcomp.so.5 libpthread.so.0"
RDEPENDS_opensm-libs = "glibc libgcc libibumad"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opensm-3.3.22-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opensm-libs-3.3.22-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opensm-devel-3.3.22-2.el8.x86_64.rpm \
          "

SRC_URI[opensm.sha256sum] = "fe2482649b358a5239ced21147f8b8c075e665b7474a5008ad99002b6f89eeeb"
SRC_URI[opensm-devel.sha256sum] = "b60e6da965cb1e206cc02c5de9172d6da108ed96f747af92acaa8f5385ae019a"
SRC_URI[opensm-libs.sha256sum] = "01d939b1d89d3b2a0bc25d3dc7d0490f53f97df531d176bd4f51b67282d40f63"
