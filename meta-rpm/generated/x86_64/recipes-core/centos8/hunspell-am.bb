SUMMARY = "generated recipe based on hunspell-am srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-am = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-am-0.20090704-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-am.sha256sum] = "c24c1637bc6b94148dffbe7363c670cdd3ab202fa789a85730af5c4a60b311cc"
