SUMMARY = "generated recipe based on libwps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libwps = "libwps-0.4.so.4"
RPM_SONAME_REQ_libwps = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libwps = "glibc libgcc librevenge libstdc++"
RPM_SONAME_REQ_libwps-devel = "libwps-0.4.so.4"
RPROVIDES_libwps-devel = "libwps-dev (= 0.4.9)"
RDEPENDS_libwps-devel = "librevenge-devel libwps pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwps-0.4.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwps-devel-0.4.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwps-doc-0.4.9-1.el8.noarch.rpm \
          "

SRC_URI[libwps.sha256sum] = "a84ada51c47bdf003adeb609e49e53b56fcb18d5e7e98cae52bebbb5d160241b"
SRC_URI[libwps-devel.sha256sum] = "6fb9aa42e22b48079a9b8ea9bfe12d62907051f295e3520168e5579abdcfb8c3"
SRC_URI[libwps-doc.sha256sum] = "b48b388c2af589a229c8ef1581d1a78709c38a5fc89d53ab359e7f72d0ce70b3"
