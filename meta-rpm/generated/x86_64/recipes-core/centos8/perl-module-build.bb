SUMMARY = "generated recipe based on perl-Module-Build srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Build = "perl-CPAN-Meta perl-Carp perl-Data-Dumper perl-ExtUtils-CBuilder perl-ExtUtils-Install perl-ExtUtils-MakeMaker perl-ExtUtils-Manifest perl-ExtUtils-ParseXS perl-File-Path perl-Getopt-Long perl-Module-Metadata perl-PathTools perl-Perl-OSType perl-Pod-Html perl-Software-License perl-Test-Harness perl-Text-ParseWords perl-inc-latest perl-interpreter perl-libs perl-podlators perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Module-Build-0.42.24-5.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Build.sha256sum] = "9019c69420574c5b3674011a37c23f1bc281f01ff4f14870262de2ea17c30052"
