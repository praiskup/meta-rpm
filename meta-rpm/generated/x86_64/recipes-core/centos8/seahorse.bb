SUMMARY = "generated recipe based on seahorse srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gcr gdk-pixbuf glib-2.0 gtk+3 libsecret p11-kit pango pkgconfig-native"
RPM_SONAME_REQ_seahorse = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsecret-1.so.0"
RDEPENDS_seahorse = "atk cairo cairo-gobject gcr gdk-pixbuf2 glib2 glibc gtk3 libsecret p11-kit pango pinentry-gtk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/seahorse-3.20.0-9.el8.x86_64.rpm \
          "

SRC_URI[seahorse.sha256sum] = "f3c1418b70de68712d7a131f7f74ad94fc516cff9fdad0b2164d6241289b624f"
