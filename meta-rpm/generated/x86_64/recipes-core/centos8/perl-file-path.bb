SUMMARY = "generated recipe based on perl-File-Path srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Path = "perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-File-Path-2.15-2.el8.noarch.rpm \
          "

SRC_URI[perl-File-Path.sha256sum] = "e83928bd4552ecdf8e71d283e2358c7eccd006d284ba31fbc9c89e407989fd60"
