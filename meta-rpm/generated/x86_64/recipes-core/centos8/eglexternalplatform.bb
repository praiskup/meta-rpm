SUMMARY = "generated recipe based on eglexternalplatform srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_eglexternalplatform-devel = "eglexternalplatform-dev (= 1.1)"
RDEPENDS_eglexternalplatform-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/eglexternalplatform-devel-1.1-0.1.20180916git7c8f8e2.el8.noarch.rpm \
          "

SRC_URI[eglexternalplatform-devel.sha256sum] = "f47ab97aab4f80b2e57228a0d0ea1b80144ef159b0f8dcc84af8382cb098c13b"
