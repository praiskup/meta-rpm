SUMMARY = "generated recipe based on webrtc-audio-processing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_webrtc-audio-processing = "libwebrtc_audio_processing.so.1"
RPM_SONAME_REQ_webrtc-audio-processing = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_webrtc-audio-processing = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/webrtc-audio-processing-0.3-8.el8.x86_64.rpm \
          "

SRC_URI[webrtc-audio-processing.sha256sum] = "81fc2c9d1aa5b27b1e38bffde98035cba592fa63bf63032ece107b3a07dd5aab"
