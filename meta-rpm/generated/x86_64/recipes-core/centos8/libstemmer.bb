SUMMARY = "generated recipe based on libstemmer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libstemmer = "libstemmer.so.0"
RPM_SONAME_REQ_libstemmer = "libc.so.6"
RDEPENDS_libstemmer = "glibc"
RPM_SONAME_REQ_libstemmer-devel = "libstemmer.so.0"
RPROVIDES_libstemmer-devel = "libstemmer-dev (= 0)"
RDEPENDS_libstemmer-devel = "libstemmer"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstemmer-0-10.585svn.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libstemmer-devel-0-10.585svn.el8.x86_64.rpm \
          "

SRC_URI[libstemmer.sha256sum] = "9b121a03314f1f0822a58059efba09944df192c35e22267f39137f70a28cda1c"
SRC_URI[libstemmer-devel.sha256sum] = "050916e4dfa5842579f7f1c0f5160501b127e080b021652f0180d780fef7d5ec"
