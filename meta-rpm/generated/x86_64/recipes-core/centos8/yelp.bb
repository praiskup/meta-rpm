SUMMARY = "generated recipe based on yelp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk bzip2 cairo gdk-pixbuf glib-2.0 gtk+3 libgcrypt libgpg-error libsoup-2.4 libxml2 libxslt pango pkgconfig-native sqlite3 webkit2gtk3 xz"
RPM_SONAME_REQ_yelp = "libatk-1.0.so.0 libbz2.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 liblzma.so.5 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libwebkit2gtk-4.0.so.37 libxml2.so.2 libxslt.so.1 libyelp.so.0"
RDEPENDS_yelp = "atk bzip2-libs cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcrypt libgpg-error libsoup libxml2 libxslt pango sqlite-libs webkit2gtk3 webkit2gtk3-jsc xz-libs yelp-libs yelp-xsl"
RPM_SONAME_REQ_yelp-devel = "libyelp.so.0"
RPROVIDES_yelp-devel = "yelp-dev (= 3.28.1)"
RDEPENDS_yelp-devel = "yelp-libs"
RPM_SONAME_PROV_yelp-libs = "libyelp.so.0 libyelpwebextension.so"
RPM_SONAME_REQ_yelp-libs = "libbz2.so.1 libc.so.6 libexslt.so.0 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 liblzma.so.5 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libwebkit2gtk-4.0.so.37 libxml2.so.2 libxslt.so.1"
RDEPENDS_yelp-libs = "bzip2-libs glib2 glibc gtk3 libsoup libxml2 libxslt sqlite-libs webkit2gtk3 xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/yelp-3.28.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/yelp-libs-3.28.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/yelp-devel-3.28.1-3.el8.x86_64.rpm \
          "

SRC_URI[yelp.sha256sum] = "8d3364feb9b52f455736bf8137441c63463432443946cca5a43690e819e0b7c0"
SRC_URI[yelp-devel.sha256sum] = "da48ff254204fa5d5942beb825804fc27210990791dbba8413e64382b3c532c0"
SRC_URI[yelp-libs.sha256sum] = "7c287b69a2b01fb9e72db25f3d564d1b3e0aaaf561ae1a405e066b6f5e466f68"
