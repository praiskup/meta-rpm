SUMMARY = "generated recipe based on libmwaw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libmwaw = "libmwaw-0.3.so.3"
RPM_SONAME_REQ_libmwaw = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libmwaw = "glibc libgcc librevenge libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmwaw-0.3.14-1.el8.x86_64.rpm \
          "

SRC_URI[libmwaw.sha256sum] = "fb6f3c0be53b912d0b09f06b3fff18b2c959941802454c324e64ce32584e645e"
