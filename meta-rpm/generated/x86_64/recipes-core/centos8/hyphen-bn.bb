SUMMARY = "generated recipe based on hyphen-bn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-bn = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-bn-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-bn.sha256sum] = "9464dcf5f98f15e9d32eff5a94b2354be121d94caa416fd0dcbbbe62b9c574c9"
