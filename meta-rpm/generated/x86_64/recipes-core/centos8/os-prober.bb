SUMMARY = "generated recipe based on os-prober srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_os-prober = "libc.so.6"
RDEPENDS_os-prober = "bash coreutils device-mapper glibc grep kmod sed systemd-udev util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/os-prober-1.74-6.el8.x86_64.rpm \
          "

SRC_URI[os-prober.sha256sum] = "91eb3bdf183ca4211207f1691be56b036d07442b421981fcf2a4a37c8b52b0f2"
