SUMMARY = "generated recipe based on rootfiles srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rootfiles = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rootfiles-8.1-22.el8.noarch.rpm \
          "

SRC_URI[rootfiles.sha256sum] = "d967d6bbb33bf7a295a64807f81875c84e82a8ecc59b6c72fe955141b1660d39"
