SUMMARY = "generated recipe based on maven-common-artifact-filters srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-common-artifact-filters = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-utils"
RDEPENDS_maven-common-artifact-filters-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-common-artifact-filters-3.0.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-common-artifact-filters-javadoc-3.0.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-common-artifact-filters.sha256sum] = "67a1abc74b14b9826a0ecfe53c61828e4b7683100ea5426d8cb5751e12593f53"
SRC_URI[maven-common-artifact-filters-javadoc.sha256sum] = "c4767ecc89a34e475ec5c922f6635f57db708f4ec89fc45b4af632846f48e93f"
