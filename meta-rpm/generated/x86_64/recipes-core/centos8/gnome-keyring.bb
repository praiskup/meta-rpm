SUMMARY = "generated recipe based on gnome-keyring srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gcr gdk-pixbuf glib-2.0 gtk+3 libcap-ng libgcrypt libgpg-error libselinux p11-kit pam pango pkgconfig-native"
RPM_SONAME_REQ_gnome-keyring = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcap-ng.so.0 libdl.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gnome-keyring = "atk cairo cairo-gobject gcr gdk-pixbuf2 glib2 glibc gtk3 libcap-ng libgcrypt libgpg-error openssh-clients p11-kit pango"
RPM_SONAME_REQ_gnome-keyring-pam = "libc.so.6 libpam.so.0 libselinux.so.1"
RDEPENDS_gnome-keyring-pam = "glibc gnome-keyring libselinux pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-keyring-3.28.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-keyring-pam-3.28.2-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-keyring.sha256sum] = "e498eea3190d59a8c161ec2931a610b24274aa4b3611edb26719fe3cdf49dd9b"
SRC_URI[gnome-keyring-pam.sha256sum] = "7c4ae026eb59e1b0ae22552de24d0defbb48647b7b9ee1ac2e3407f6f712dda3"
