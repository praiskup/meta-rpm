SUMMARY = "generated recipe based on hunspell-az srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-az = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-az-0.20040827-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-az.sha256sum] = "4773e7d5d38d5aa9534a173c312d25f1748d9c7f19131826c007d4ae393632b3"
