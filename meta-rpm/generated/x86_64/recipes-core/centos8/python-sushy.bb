SUMMARY = "generated recipe based on python-sushy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sushy = "platform-python python3-requests python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-sushy-1.3.1-3.el8.noarch.rpm \
          "

SRC_URI[python3-sushy.sha256sum] = "61e7adf258d009f2f1a1e3a61b0dc27a45851aab8f5b02fb18e7901c02394940"
