SUMMARY = "generated recipe based on fipscheck srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_fipscheck = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1"
RDEPENDS_fipscheck = "fipscheck-lib glibc openssl-libs"
RPM_SONAME_REQ_fipscheck-devel = "libfipscheck.so.1"
RPROVIDES_fipscheck-devel = "fipscheck-dev (= 1.5.0)"
RDEPENDS_fipscheck-devel = "fipscheck-lib"
RPM_SONAME_PROV_fipscheck-lib = "libfipscheck.so.1"
RPM_SONAME_REQ_fipscheck-lib = "libc.so.6 libdl.so.2"
RDEPENDS_fipscheck-lib = "fipscheck glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fipscheck-1.5.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fipscheck-lib-1.5.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fipscheck-devel-1.5.0-4.el8.x86_64.rpm \
          "

SRC_URI[fipscheck.sha256sum] = "e7a2551f914754b3d9b773b5392ab5c27737c780c5516afa65f4256882e3030b"
SRC_URI[fipscheck-devel.sha256sum] = "59ca4e509528aae35a2da3c734cd8d65f422f9640ba2ae52d3991aae2ed3bd9c"
SRC_URI[fipscheck-lib.sha256sum] = "515bbab5ad60677df8c4bf5cc1ba4d4b177096f3a5a2428ab68ccbc30375cb2e"
