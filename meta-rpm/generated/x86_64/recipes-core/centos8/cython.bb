SUMMARY = "generated recipe based on Cython srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-Cython = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-Cython = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-Cython-0.28.1-3.el8.x86_64.rpm \
          "

SRC_URI[python3-Cython.sha256sum] = "a3ae75ebe1caaa011c1936fac8d1b457c4d197a2e507f7ecd6804b8a66566b2b"
