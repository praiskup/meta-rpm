SUMMARY = "generated recipe based on guile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gc gmp libffi libtool libunistring libxcrypt ncurses pkgconfig-native readline"
RPM_SONAME_PROV_guile = "libguile-2.0.so.22 libguilereadline-v-18.so.18"
RPM_SONAME_REQ_guile = "ld-linux-x86-64.so.2 libc.so.6 libcrypt.so.1 libffi.so.6 libgc.so.1 libgmp.so.10 libguile-2.0.so.22 libguilereadline-v-18.so.18 libltdl.so.7 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libtinfo.so.6 libunistring.so.2"
RDEPENDS_guile = "bash coreutils gc glibc gmp info libffi libtool-ltdl libunistring libxcrypt ncurses-libs readline"
RPM_SONAME_REQ_guile-devel = "libguile-2.0.so.22"
RPROVIDES_guile-devel = "guile-dev (= 2.0.14)"
RDEPENDS_guile-devel = "bash gc-devel gmp-devel guile pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/guile-2.0.14-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/guile-devel-2.0.14-7.el8.x86_64.rpm \
          "

SRC_URI[guile.sha256sum] = "e5edef871e38b7b4b51b672a68d27a98a743b8e53ec28683e8242f405b63c9f5"
SRC_URI[guile-devel.sha256sum] = "bd41cf2148b767d8f068db893085436585eedc97633a5e91ba83aa6d00b18661"
