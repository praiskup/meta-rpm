SUMMARY = "generated recipe based on xdelta srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native xz"
RPM_SONAME_REQ_xdelta = "libc.so.6 liblzma.so.5 libm.so.6"
RDEPENDS_xdelta = "glibc xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xdelta-3.1.0-4.el8.x86_64.rpm \
          "

SRC_URI[xdelta.sha256sum] = "cd78877b6f62e97537752dc3504ac5075d680746f8c9dd0bb02c1abb7db30ca7"
