SUMMARY = "generated recipe based on python-requests-file srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-requests-file = "platform-python python3-requests python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-requests-file-1.4.3-5.el8.noarch.rpm \
          "

SRC_URI[python3-requests-file.sha256sum] = "a344d36cc3077db9b22452308595455f02d192b30e14a4ed7e7d827e1d6a84aa"
