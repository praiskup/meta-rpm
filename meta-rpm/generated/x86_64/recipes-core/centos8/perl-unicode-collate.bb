SUMMARY = "generated recipe based on perl-Unicode-Collate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-Collate = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-Collate = "glibc perl-Carp perl-PathTools perl-Unicode-Normalize perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Unicode-Collate-1.25-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Unicode-Collate.sha256sum] = "6f39c0df2bc133453f365700f7fb69cf527e267861c7b411f800da937f149b14"
