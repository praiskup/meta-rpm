SUMMARY = "generated recipe based on kexec-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 elfutils libgcc lzo ncurses pkgconfig-native snappy xz zlib"
RPM_SONAME_REQ_kexec-tools = "libbz2.so.1 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 liblzma.so.5 liblzo2.so.2 libpthread.so.0 libsnappy.so.1 libtinfo.so.6 libz.so.1"
RDEPENDS_kexec-tools = "bash bzip2-libs coreutils dracut dracut-network dracut-squash elfutils-libelf elfutils-libs ethtool glibc libgcc lzo ncurses-libs sed snappy systemd xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kexec-tools-2.0.20-14.el8.x86_64.rpm \
          "

SRC_URI[kexec-tools.sha256sum] = "6e84783df2760c84cacce7bcd9d936a393e075ee345efd9989fe3ef4546e77d2"
