SUMMARY = "generated recipe based on perl-Archive-Tar srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Archive-Tar = "perl-Carp perl-Data-Dumper perl-Exporter perl-File-Path perl-Getopt-Long perl-IO perl-IO-Compress perl-IO-Zlib perl-PathTools perl-Pod-Usage perl-Text-Diff perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Archive-Tar-2.30-1.el8.noarch.rpm \
          "

SRC_URI[perl-Archive-Tar.sha256sum] = "d04923e5ed0e05cb9a91e4b6e0ea8d73680bae1265ec573428f505ca67313954"
