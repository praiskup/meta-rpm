SUMMARY = "generated recipe based on stunnel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_stunnel = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libssl.so.1.1 libutil.so.1"
RDEPENDS_stunnel = "bash glibc openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/stunnel-5.48-5.el8.0.1.x86_64.rpm \
          "

SRC_URI[stunnel.sha256sum] = "02264f0f274acf5da25006279d7b45a103b83a80633cdd36dcf6ae73228c1e50"
