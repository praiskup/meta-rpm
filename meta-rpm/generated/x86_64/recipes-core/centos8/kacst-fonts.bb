SUMMARY = "generated recipe based on kacst-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_kacst-art-fonts = "kacst-fonts-common"
RDEPENDS_kacst-book-fonts = "kacst-fonts-common"
RDEPENDS_kacst-decorative-fonts = "kacst-fonts-common"
RDEPENDS_kacst-digital-fonts = "kacst-fonts-common"
RDEPENDS_kacst-farsi-fonts = "kacst-fonts-common"
RDEPENDS_kacst-fonts-common = "fontpackages-filesystem"
RDEPENDS_kacst-letter-fonts = "kacst-fonts-common"
RDEPENDS_kacst-naskh-fonts = "kacst-fonts-common"
RDEPENDS_kacst-office-fonts = "kacst-fonts-common"
RDEPENDS_kacst-one-fonts = "kacst-fonts-common"
RDEPENDS_kacst-pen-fonts = "kacst-fonts-common"
RDEPENDS_kacst-poster-fonts = "kacst-fonts-common"
RDEPENDS_kacst-qurn-fonts = "kacst-fonts-common"
RDEPENDS_kacst-screen-fonts = "kacst-fonts-common"
RDEPENDS_kacst-title-fonts = "kacst-fonts-common"
RDEPENDS_kacst-titlel-fonts = "kacst-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-art-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-book-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-decorative-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-digital-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-farsi-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-fonts-common-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-letter-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-naskh-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-office-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-one-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-pen-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-poster-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-qurn-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-screen-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-title-fonts-2.0-19.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kacst-titlel-fonts-2.0-19.el8.noarch.rpm \
          "

SRC_URI[kacst-art-fonts.sha256sum] = "8d4c7f044c5014f621097a76862b1d23c9ab08ba807493636063af668bdab224"
SRC_URI[kacst-book-fonts.sha256sum] = "e4e5658300b390b19563bff7cf109eb4c56e24134e24045bc73c393192106baa"
SRC_URI[kacst-decorative-fonts.sha256sum] = "1d7fbb00b0d9162fdf9888bd319b340c684799d4862bcabb38e44786dfccd5e8"
SRC_URI[kacst-digital-fonts.sha256sum] = "741672950f04b85ba665c096877e80d8bae3260111713bad04c59c0413656015"
SRC_URI[kacst-farsi-fonts.sha256sum] = "25608b00ba65b55cb6138bd297c8ef100c83205f6f850c0ce6fa063286275827"
SRC_URI[kacst-fonts-common.sha256sum] = "c669221221dd4116e3032df5030cb77e907379de4f8bbf2c8a8d60d1f8532bee"
SRC_URI[kacst-letter-fonts.sha256sum] = "3b3e677a30608ee0bf18f18d42c70b13438d92e476692d71754eb65af7842975"
SRC_URI[kacst-naskh-fonts.sha256sum] = "34d7e7f56aed7e49b8e6e31848476f5f465449bc75852055dc6d160564461f17"
SRC_URI[kacst-office-fonts.sha256sum] = "f2b6201d2c119dbbab0319ba8973493a6061c118c349deaebeddeffc607e8db0"
SRC_URI[kacst-one-fonts.sha256sum] = "30c1f6a4b735fe5bca3540a2a77c8f89e278725faf4611fe1fb3d95924669b07"
SRC_URI[kacst-pen-fonts.sha256sum] = "f656925acc8c8ee7fa8cae9b4470503898236d1cdd6591ccf082e48ad7d3ab9a"
SRC_URI[kacst-poster-fonts.sha256sum] = "ebd50b5336d8a240bd71e007d60bf2ea0d909dd0ce1a195aaa50bd99701507cf"
SRC_URI[kacst-qurn-fonts.sha256sum] = "586cc4662ac3773022a48f13a2a701c51599ad6bfa182b5e881ee0980dec34d2"
SRC_URI[kacst-screen-fonts.sha256sum] = "1ac1d208f6e3d4ad935ef6ff89c4865e22f5a0e9c4fbca19282215cd52691c73"
SRC_URI[kacst-title-fonts.sha256sum] = "81d17fdfac6028ce224993b22b2a1ea58294108bf2887ebb338b1f3338a42ccc"
SRC_URI[kacst-titlel-fonts.sha256sum] = "c236b602761a3a0f06c93ee96dc69f3f8f590ad031e3fd14307929e860c2ea12"
