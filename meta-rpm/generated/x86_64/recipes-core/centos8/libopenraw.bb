SUMMARY = "generated recipe based on libopenraw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdk-pixbuf glib-2.0 libgcc libjpeg-turbo pkgconfig-native"
RPM_SONAME_PROV_libopenraw = "libopenraw.so.7"
RPM_SONAME_REQ_libopenraw = "libc.so.6 libgcc_s.so.1 libjpeg.so.62 libstdc++.so.6"
RDEPENDS_libopenraw = "glibc libgcc libjpeg-turbo libstdc++"
RPM_SONAME_REQ_libopenraw-devel = "libopenraw.so.7"
RPROVIDES_libopenraw-devel = "libopenraw-dev (= 0.1.2)"
RDEPENDS_libopenraw-devel = "libopenraw pkgconf-pkg-config"
RPM_SONAME_PROV_libopenraw-gnome = "libopenrawgnome.so.7"
RPM_SONAME_REQ_libopenraw-gnome = "libc.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libopenraw.so.7"
RDEPENDS_libopenraw-gnome = "gdk-pixbuf2 glib2 glibc libopenraw"
RPM_SONAME_REQ_libopenraw-gnome-devel = "libopenrawgnome.so.7"
RPROVIDES_libopenraw-gnome-devel = "libopenraw-gnome-dev (= 0.1.2)"
RDEPENDS_libopenraw-gnome-devel = "libopenraw-devel libopenraw-gnome pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libopenraw-0.1.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libopenraw-devel-0.1.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libopenraw-gnome-0.1.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libopenraw-gnome-devel-0.1.2-4.el8.x86_64.rpm \
          "

SRC_URI[libopenraw.sha256sum] = "f8adcfa981b2a780060c31e2a20ba8c78298b64bef3ee521160b97fdf5ba3d3d"
SRC_URI[libopenraw-devel.sha256sum] = "d891cc567bb87f1368dc56d29907a30289bdb7dd51592a6a55ac1b9fb809d878"
SRC_URI[libopenraw-gnome.sha256sum] = "6836a1d3d64fe2eabd3bf9508c4a38ab5c9d238572357e758c9cf25678e790b4"
SRC_URI[libopenraw-gnome-devel.sha256sum] = "fa2a9a2ec17b6167e6476209b8e43be203158ed1781921a531c9d02aeb80ab47"
