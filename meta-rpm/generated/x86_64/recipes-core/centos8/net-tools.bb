SUMMARY = "generated recipe based on net-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux pkgconfig-native"
RPM_SONAME_REQ_net-tools = "libc.so.6 libselinux.so.1"
RDEPENDS_net-tools = "bash glibc libselinux systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/net-tools-2.0-0.51.20160912git.el8.x86_64.rpm \
          "

SRC_URI[net-tools.sha256sum] = "fc4fee729d5ada68e367b7c12ee5ceee775991f877a5bc62d615223df80c1f7e"
