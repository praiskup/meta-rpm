SUMMARY = "generated recipe based on python-psycopg2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpq pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-psycopg2 = "libc.so.6 libpq.so.5 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-psycopg2 = "glibc libpq platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-psycopg2-2.7.5-7.el8.x86_64.rpm \
          "

SRC_URI[python3-psycopg2.sha256sum] = "e4c29340d74a11430306a86564ef4045db48f0ee964217b5ecec5afa0fe295c3"
