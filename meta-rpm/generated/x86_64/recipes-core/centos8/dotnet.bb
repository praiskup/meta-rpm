SUMMARY = "generated recipe based on dotnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl krb5-libs libgcc lttng-ust openssl pkgconfig-native zlib"
RPM_SONAME_REQ_dotnet-host-fxr-2.1 = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_dotnet-host-fxr-2.1 = "dotnet-host glibc libgcc libstdc++"
RPM_SONAME_REQ_dotnet-runtime-2.1 = "ld-linux-x86-64.so.2 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 liblttng-ust.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_dotnet-runtime-2.1 = "dotnet-host-fxr-2.1 glibc krb5-libs libcurl libgcc libicu libstdc++ lttng-ust openssl-libs zlib"
RDEPENDS_dotnet-sdk-2.1 = "dotnet-sdk-2.1.5xx"
RPM_SONAME_REQ_dotnet-sdk-2.1.5xx = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dotnet-sdk-2.1.5xx = "bash dotnet-runtime-2.1 glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-host-fxr-2.1-2.1.21-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-runtime-2.1-2.1.21-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-sdk-2.1-2.1.517-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-sdk-2.1.5xx-2.1.517-1.el8_2.x86_64.rpm \
          "

SRC_URI[dotnet-host-fxr-2.1.sha256sum] = "b9bc11441b4111702f51fde84eb61bcd412784ee931ce6fbd658d59e95aac1f5"
SRC_URI[dotnet-runtime-2.1.sha256sum] = "9e895a24bd6a28be1c64b97a224dfc3ffce129ef60de72256a8f62e0756efd2f"
SRC_URI[dotnet-sdk-2.1.sha256sum] = "14f3e64462c4165de53347caf22c5ad4ba243b6a69d8114885c1c1d7cec7bf77"
SRC_URI[dotnet-sdk-2.1.5xx.sha256sum] = "ddaa021bdbf8b16f0cee038d80696a4363c395526b245903d19dc61112f15a25"
