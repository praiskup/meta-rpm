SUMMARY = "generated recipe based on suitesparse srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atlas libgcc pkgconfig-native tbb"
RPM_SONAME_PROV_suitesparse = "libamd.so.2 libbtf.so.1 libcamd.so.2 libccolamd.so.2 libcholmod.so.3 libcolamd.so.2 libcxsparse.so.3 libklu.so.1 libldl.so.2 librbio.so.2 libspqr.so.2 libsuitesparseconfig.so.4 libumfpack.so.5"
RPM_SONAME_REQ_suitesparse = "libamd.so.2 libbtf.so.1 libc.so.6 libcamd.so.2 libccolamd.so.2 libcholmod.so.3 libcolamd.so.2 libgcc_s.so.1 libm.so.6 libsatlas.so.3 libstdc++.so.6 libsuitesparseconfig.so.4 libtbb.so.2"
RDEPENDS_suitesparse = "atlas glibc libgcc libstdc++ tbb"
RPM_SONAME_REQ_suitesparse-devel = "libamd.so.2 libbtf.so.1 libcamd.so.2 libccolamd.so.2 libcholmod.so.3 libcolamd.so.2 libcxsparse.so.3 libklu.so.1 libldl.so.2 librbio.so.2 libspqr.so.2 libsuitesparseconfig.so.4 libumfpack.so.5"
RPROVIDES_suitesparse-devel = "suitesparse-dev (= 4.4.6)"
RDEPENDS_suitesparse-devel = "suitesparse"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/suitesparse-4.4.6-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/suitesparse-devel-4.4.6-11.el8.x86_64.rpm \
          "

SRC_URI[suitesparse.sha256sum] = "20c4442e89077d4c4e534e7a2f26e49fde9b63f22e29fc76f95d0f3a2538eaff"
SRC_URI[suitesparse-devel.sha256sum] = "03fbfd3c8a7d52a9fe960d320d973de5f0715deb4ad0caab708e331ff86dc6db"
