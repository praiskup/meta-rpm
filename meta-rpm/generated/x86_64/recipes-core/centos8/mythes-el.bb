SUMMARY = "generated recipe based on mythes-el srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-el = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-el-0.20070412-19.el8.noarch.rpm \
          "

SRC_URI[mythes-el.sha256sum] = "c2b3a9afdc2e047cbe23b86bf706124d8c52c76939eceee264e608cc04ab86b8"
