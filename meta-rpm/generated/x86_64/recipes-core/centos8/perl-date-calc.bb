SUMMARY = "generated recipe based on perl-Date-Calc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Date-Calc = "perl-Bit-Vector perl-Carp-Clan perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Date-Calc-6.4-9.el8.noarch.rpm \
          "

SRC_URI[perl-Date-Calc.sha256sum] = "eee6c8b35e56390474dcb59374fc8f3880a3b48d618e3d81b0eec77e703be904"
