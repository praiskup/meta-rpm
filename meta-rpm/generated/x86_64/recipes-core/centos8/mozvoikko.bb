SUMMARY = "generated recipe based on mozvoikko srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mozvoikko = "libvoikko mozilla-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mozvoikko-2.1-5.el8.noarch.rpm \
          "

SRC_URI[mozvoikko.sha256sum] = "e184b17a7e3bc2312dadc47a0dde80f3e53a5050d6762100230358e69f3f8ab0"
