SUMMARY = "generated recipe based on pyserial srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyserial = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pyserial-3.1.1-8.el8.noarch.rpm \
          "

SRC_URI[python3-pyserial.sha256sum] = "82ce159a9e4e4b6b4fdce9ad954aa507f67108430bd261a4dcd826e44d0152a4"
