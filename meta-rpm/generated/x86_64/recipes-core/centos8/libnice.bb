SUMMARY = "generated recipe based on libnice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gnutls gstreamer1.0 gupnp gupnp-igd pkgconfig-native"
RPM_SONAME_PROV_libnice = "libnice.so.10"
RPM_SONAME_REQ_libnice = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libgupnp-1.0.so.4 libgupnp-igd-1.0.so.4 libpthread.so.0 librt.so.1"
RDEPENDS_libnice = "glib2 glibc gnutls gupnp gupnp-igd"
RPM_SONAME_REQ_libnice-devel = "libnice.so.10"
RPROVIDES_libnice-devel = "libnice-dev (= 0.1.14)"
RDEPENDS_libnice-devel = "glib2-devel gnutls-devel gupnp-igd-devel libnice pkgconf-pkg-config"
RPM_SONAME_PROV_libnice-gstreamer1 = "libgstnice.so"
RPM_SONAME_REQ_libnice-gstreamer1 = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgthread-2.0.so.0 libgupnp-1.0.so.4 libgupnp-igd-1.0.so.4 libnice.so.10 libpthread.so.0 librt.so.1"
RDEPENDS_libnice-gstreamer1 = "glib2 glibc gnutls gstreamer1 gupnp gupnp-igd libnice"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libnice-0.1.14-7.20180504git34d6044.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libnice-gstreamer1-0.1.14-7.20180504git34d6044.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnice-devel-0.1.14-7.20180504git34d6044.el8.x86_64.rpm \
          "

SRC_URI[libnice.sha256sum] = "1984d478045fdd77baaa83d026207735e0477a4d2b0ca955a57bbafd6a1b2aeb"
SRC_URI[libnice-devel.sha256sum] = "298c3297521222f643069b14647a0b3cbb35a05a9ecb2d9853307fc42d71e771"
SRC_URI[libnice-gstreamer1.sha256sum] = "d1d613d76d9b765a47ed83ce3751368dd9a1a7a478dd182ce0d54bda4cf2930e"
