SUMMARY = "generated recipe based on cloud-init srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_cloud-init = "bash e2fsprogs iproute net-tools platform-python procps-ng python3-configobj python3-jinja2 python3-jsonpatch python3-jsonschema python3-libselinux python3-oauthlib python3-policycoreutils python3-prettytable python3-pyserial python3-pyyaml python3-requests python3-six shadow-utils systemd util-linux xfsprogs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cloud-init-19.4-1.el8.7.noarch.rpm \
          "

SRC_URI[cloud-init.sha256sum] = "11afe168bcba20a0b44587d35565af53ea31f5d3fdf51fe0203601f575508a39"
