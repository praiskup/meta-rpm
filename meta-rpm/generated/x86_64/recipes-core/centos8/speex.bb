SUMMARY = "generated recipe based on speex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_speex = "libspeex.so.1"
RPM_SONAME_REQ_speex = "libc.so.6 libm.so.6"
RDEPENDS_speex = "glibc"
RPM_SONAME_REQ_speex-devel = "libspeex.so.1"
RPROVIDES_speex-devel = "speex-dev (= 1.2.0)"
RDEPENDS_speex-devel = "pkgconf-pkg-config speex"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/speex-1.2.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/speex-devel-1.2.0-1.el8.x86_64.rpm \
          "

SRC_URI[speex.sha256sum] = "7dcb5b92d5cc8e548d948a7b13a68d930fb8d0a76c2596f31e2c461cbff2dc3c"
SRC_URI[speex-devel.sha256sum] = "0c9b90bfbc1f2a39dcac46d9b27ce4b29cb12fa7cf34939feb19817525d13b1e"
