SUMMARY = "generated recipe based on totem srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo clutter clutter-gst3 clutter-gtk cogl gdk-pixbuf glib-2.0 gnome-desktop3 gobject-introspection grilo gstreamer1.0 gstreamer1.0-plugins-base gtk+3 libpeas libx11 libxml2 nautilus pango pkgconfig-native totem-pl-parser wayland"
RPM_SONAME_PROV_totem = "libapple-trailers.so libautoload-subtitles.so libbrasero-disc-recorder.so libgromit.so libmedia_player_keys.so libmovie-properties.so libontop.so librecent.so librotation.so libsave-file.so libscreensaver.so libscreenshot.so libskipto.so libtotem-im-status.so libtotem.so.0 libvariable-rate.so libvimeo.so"
RPM_SONAME_REQ_totem = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libclutter-gst-3.0.so.0 libclutter-gtk-1.0.so.0 libcogl.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgrilo-0.3.so.0 libgrlpls-0.3.so.0 libgstaudio-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libgstvideo-1.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0 libtotem-plparser.so.18 libtotem.so.0 libwayland-server.so.0 libxml2.so.2"
RDEPENDS_totem = "atk cairo cairo-gobject clutter clutter-gst3 clutter-gtk cogl gdk-pixbuf2 glib2 glibc gnome-desktop3 gobject-introspection grilo grilo-plugins gsettings-desktop-schemas gstreamer1 gstreamer1-plugins-base gstreamer1-plugins-good gstreamer1-plugins-ugly-free gtk3 gvfs-fuse iso-codes libX11 libpeas libpeas-gtk libpeas-loader-python3 libwayland-server libxml2 pango python3-gobject totem-pl-parser"
RPM_SONAME_PROV_totem-nautilus = "libtotem-properties-page.so"
RPM_SONAME_REQ_totem-nautilus = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgtk-3.so.0 libnautilus-extension.so.1 libpthread.so.0"
RDEPENDS_totem-nautilus = "glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 nautilus-extensions totem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/totem-3.26.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/totem-nautilus-3.26.2-1.el8.x86_64.rpm \
          "

SRC_URI[totem.sha256sum] = "ebe8a16d8685e07abcac990a1fbbfbead127ea68c50d6cc2cf3f9a83dc9a493f"
SRC_URI[totem-nautilus.sha256sum] = "b5d328c77a56d1b674062ede9f7cd4570ecb4d949d2455cc2bdf3061509f80da"
