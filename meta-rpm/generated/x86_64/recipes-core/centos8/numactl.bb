SUMMARY = "generated recipe based on numactl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_numactl = "libc.so.6 libm.so.6 libnuma.so.1 librt.so.1"
RDEPENDS_numactl = "glibc numactl-libs"
RPM_SONAME_REQ_numactl-devel = "libnuma.so.1"
RPROVIDES_numactl-devel = "numactl-dev (= 2.0.12)"
RDEPENDS_numactl-devel = "numactl-libs pkgconf-pkg-config"
RPM_SONAME_PROV_numactl-libs = "libnuma.so.1"
RPM_SONAME_REQ_numactl-libs = "ld-linux-x86-64.so.2 libc.so.6"
RDEPENDS_numactl-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/numactl-2.0.12-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/numactl-devel-2.0.12-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/numactl-libs-2.0.12-9.el8.x86_64.rpm \
          "

SRC_URI[numactl.sha256sum] = "297872fbb11f162782c1af3c75d1a5e00ecc453e7a19f2866ee8432518e911f5"
SRC_URI[numactl-devel.sha256sum] = "981864edab2b00b1cb47bd74cdabf0ace51d6c44b9c5db4e6b65e0877fca35ad"
SRC_URI[numactl-libs.sha256sum] = "c9c49e5c9367775a6feed029543f5735459f09717a63c616d454b58b2091fd24"
