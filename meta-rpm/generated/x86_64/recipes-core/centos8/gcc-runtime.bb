SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libatomic = "libatomic.so.1"
RPM_SONAME_REQ_libatomic = "libc.so.6 libpthread.so.0"
RDEPENDS_libatomic = "glibc info"
RDEPENDS_libatomic-static = "libatomic"
RPM_SONAME_PROV_libgomp = "libgomp.so.1"
RPM_SONAME_REQ_libgomp = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_libgomp = "bash glibc info"
RPM_SONAME_PROV_libgomp-offload-nvptx = "libgomp-plugin-nvptx.so.1"
RPM_SONAME_REQ_libgomp-offload-nvptx = "libc.so.6 libdl.so.2 libgomp.so.1 libpthread.so.0"
RDEPENDS_libgomp-offload-nvptx = "glibc libgomp"
RPM_SONAME_PROV_libitm = "libitm.so.1"
RPM_SONAME_REQ_libitm = "libc.so.6 libpthread.so.0"
RDEPENDS_libitm = "bash glibc info"
RPROVIDES_libitm-devel = "libitm-dev (= 8.3.1)"
RDEPENDS_libitm-devel = "gcc libitm"
RPM_SONAME_PROV_libquadmath = "libquadmath.so.0"
RPM_SONAME_REQ_libquadmath = "libc.so.6 libm.so.6"
RDEPENDS_libquadmath = "bash glibc info"
RPROVIDES_libquadmath-devel = "libquadmath-dev (= 8.3.1)"
RDEPENDS_libquadmath-devel = "gcc libquadmath"
RPM_SONAME_PROV_libstdc++ = "libstdc++.so.6"
RPM_SONAME_REQ_libstdc++ = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6"
RDEPENDS_libstdc++ = "glibc libgcc"
RPROVIDES_libstdc++-devel = "libstdc++-dev (= 8.3.1)"
RDEPENDS_libstdc++-devel = "libstdc++"
RDEPENDS_libstdc++-static = "libstdc++-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libitm-devel-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libquadmath-devel-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libstdc++-devel-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libstdc++-docs-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libatomic-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libatomic-static-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgomp-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgomp-offload-nvptx-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libitm-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libquadmath-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstdc++-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libstdc++-static-8.3.1-5.el8.0.2.x86_64.rpm \
          "

SRC_URI[libatomic.sha256sum] = "aeebacb87d9c14debb74ac43409a02d97256753ba359b59e4e2f3d77595829de"
SRC_URI[libatomic-static.sha256sum] = "0d932dff63119527e3999d4e28c2b647b91f30ef50727f5070f95c4fd3236ed5"
SRC_URI[libgomp.sha256sum] = "e97f452c481f00f2841355dd1f4369ed92e7f5317d062a65696e303b0bafa972"
SRC_URI[libgomp-offload-nvptx.sha256sum] = "1e5f7b1a60f4cb5f81f39ae65267f966f238e3aa945992955697e56cc2973268"
SRC_URI[libitm.sha256sum] = "22b183459dcb4b3d12bf54a2b2daee745400e866660ed5561fd0b3bafe3af3b4"
SRC_URI[libitm-devel.sha256sum] = "23b29de716b6b4c1ebf2287336fb00729ebf9b291dadc3fba923982d474151ee"
SRC_URI[libquadmath.sha256sum] = "ab704ad42101146ec2f71b1294c2a6561eb3e81d0c994ceb5ac037ac2200f298"
SRC_URI[libquadmath-devel.sha256sum] = "767f5c8737e49d5ebdd36edecf15ff6a537c8531fa23b2240787cb8afbcf4659"
SRC_URI[libstdc++.sha256sum] = "cf3823561b6f60e300de49fa92adbd23b01281e4eb01bde9f62c10cf949d23e6"
SRC_URI[libstdc++-devel.sha256sum] = "b259c90eb6dd379e7272513872d5ca4728e98747226c065cc550de54c23103a0"
SRC_URI[libstdc++-docs.sha256sum] = "c13133a0a4bde3f52b3c4b5bdb3b535f4161dd94cc57ff13629f5e2b61f82424"
SRC_URI[libstdc++-static.sha256sum] = "9fb383bcda85b285a1a115907347e01a48a79cfa6035a9d3907b06d49f2b9d6c"
