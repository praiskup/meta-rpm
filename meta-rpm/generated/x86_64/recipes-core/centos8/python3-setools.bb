SUMMARY = "generated recipe based on setools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux libsepol pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-setools = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_python3-setools = "glibc libselinux libsepol platform-python platform-python-setuptools python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-setools-4.2.2-2.el8.x86_64.rpm \
          "

SRC_URI[python3-setools.sha256sum] = "aced77c509147fd25323e468b75a96bd8b7d3e226382c1a9aef0b1d3c57117a5"
