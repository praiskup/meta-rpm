SUMMARY = "generated recipe based on libusbmuxd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libplist pkgconfig-native"
RPM_SONAME_PROV_libusbmuxd = "libusbmuxd.so.4"
RPM_SONAME_REQ_libusbmuxd = "libc.so.6 libgcc_s.so.1 libplist.so.3 libpthread.so.0"
RDEPENDS_libusbmuxd = "glibc libgcc libplist"
RPM_SONAME_REQ_libusbmuxd-devel = "libusbmuxd.so.4"
RPROVIDES_libusbmuxd-devel = "libusbmuxd-dev (= 1.0.10)"
RDEPENDS_libusbmuxd-devel = "libusbmuxd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libusbmuxd-1.0.10-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libusbmuxd-devel-1.0.10-9.el8.x86_64.rpm \
          "

SRC_URI[libusbmuxd.sha256sum] = "d84fed0ec04c003c12780a1aac6e074484ea93da7c4793da4383868514f56671"
SRC_URI[libusbmuxd-devel.sha256sum] = "f757530912610396a650dbdd42b662ca0dee53959205807582dfdb46200153ac"
