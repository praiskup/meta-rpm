SUMMARY = "generated recipe based on sisu-mojos srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sisu-mojos = "java-1.8.0-openjdk-headless javapackages-filesystem maven-common-artifact-filters maven-lib plexus-utils sisu-inject slf4j"
RDEPENDS_sisu-mojos-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sisu-mojos-0.3.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sisu-mojos-javadoc-0.3.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[sisu-mojos.sha256sum] = "c96df16eb9de45ff1004905cb0a5571a32a06293f2fb6e1e65a5bb4c9e49aa7c"
SRC_URI[sisu-mojos-javadoc.sha256sum] = "10be616b63befda8627bd1abca569544869d30bf3c928f5b0b1013bb228b0bd5"
