SUMMARY = "generated recipe based on libcdr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu lcms2 libgcc librevenge pkgconfig-native zlib"
RPM_SONAME_PROV_libcdr = "libcdr-0.1.so.1"
RPM_SONAME_REQ_libcdr = "libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 liblcms2.so.2 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_libcdr = "glibc lcms2 libgcc libicu librevenge libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libcdr-0.1.4-4.el8.x86_64.rpm \
          "

SRC_URI[libcdr.sha256sum] = "2b69ee3cac3d0931c49bd22f8a6e0480d16a0b3f4dd640eb065c1f9afa7b6313"
