SUMMARY = "generated recipe based on compiler-rt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_compiler-rt = "libclang_rt.asan-x86_64.so libclang_rt.dyndd-x86_64.so libclang_rt.hwasan-x86_64.so libclang_rt.scudo-x86_64.so libclang_rt.scudo_minimal-x86_64.so libclang_rt.ubsan_minimal-x86_64.so libclang_rt.ubsan_standalone-x86_64.so"
RPM_SONAME_REQ_compiler-rt = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_compiler-rt = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/compiler-rt-9.0.1-2.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
          "

SRC_URI[compiler-rt.sha256sum] = "912b24c4a239bae4cd5e305b006139dcb6eb5d42c99fca2e036c259f044e4211"
