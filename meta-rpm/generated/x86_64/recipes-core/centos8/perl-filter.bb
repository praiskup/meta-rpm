SUMMARY = "generated recipe based on perl-Filter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Filter = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Filter = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Filter-1.58-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Filter.sha256sum] = "758a793abe09e11c66fb56945e9e342f8521a9beadbf95d65033899f1facaf9a"
