SUMMARY = "generated recipe based on libstoragemgmt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libconfig libgcc libxml2 openssl pkgconfig-native platform-python3 sqlite3 systemd-libs"
RPM_SONAME_PROV_libstoragemgmt = "libstoragemgmt.so.1"
RPM_SONAME_REQ_libstoragemgmt = "libc.so.6 libconfig.so.9 libcrypto.so.1.1 libgcc_s.so.1 libglib-2.0.so.0 libm.so.6 librt.so.1 libsqlite3.so.0 libstdc++.so.6 libstoragemgmt.so.1 libudev.so.1 libxml2.so.2"
RDEPENDS_libstoragemgmt = "bash glib2 glibc libconfig libgcc libstdc++ libxml2 openssl-libs platform-python python3-libstoragemgmt sqlite-libs systemd systemd-libs"
RDEPENDS_libstoragemgmt-arcconf-plugin = "bash platform-python python3-libstoragemgmt"
RDEPENDS_libstoragemgmt-hpsa-plugin = "bash platform-python python3-libstoragemgmt"
RDEPENDS_libstoragemgmt-local-plugin = "bash platform-python python3-libstoragemgmt"
RDEPENDS_libstoragemgmt-megaraid-plugin = "bash platform-python python3-libstoragemgmt"
RDEPENDS_libstoragemgmt-nfs-plugin = "bash libstoragemgmt-nfs-plugin-clibs nfs-utils platform-python python3-libstoragemgmt"
RPM_SONAME_REQ_libstoragemgmt-nfs-plugin-clibs = "libc.so.6 libglib-2.0.so.0 libpython3.6m.so.1.0 libstoragemgmt.so.1 libudev.so.1 libxml2.so.2"
RDEPENDS_libstoragemgmt-nfs-plugin-clibs = "glib2 glibc libstoragemgmt libxml2 platform-python python3-libs systemd-libs"
RDEPENDS_libstoragemgmt-nstor-plugin = "bash platform-python python3-libstoragemgmt"
RDEPENDS_libstoragemgmt-smis-plugin = "bash platform-python python3-libstoragemgmt python3-pywbem"
RPM_SONAME_REQ_libstoragemgmt-udev = "libc.so.6"
RDEPENDS_libstoragemgmt-udev = "glibc"
RDEPENDS_python3-libstoragemgmt = "libstoragemgmt platform-python python3-libstoragemgmt-clibs"
RPM_SONAME_REQ_python3-libstoragemgmt-clibs = "libc.so.6 libglib-2.0.so.0 libpython3.6m.so.1.0 libstoragemgmt.so.1 libudev.so.1 libxml2.so.2"
RDEPENDS_python3-libstoragemgmt-clibs = "glib2 glibc libstoragemgmt libxml2 platform-python python3-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-1.8.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-arcconf-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-hpsa-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-local-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-megaraid-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-nfs-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-nfs-plugin-clibs-1.8.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-nstor-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-smis-plugin-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libstoragemgmt-udev-1.8.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libstoragemgmt-1.8.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libstoragemgmt-clibs-1.8.3-1.el8.x86_64.rpm \
          "

SRC_URI[libstoragemgmt.sha256sum] = "58596f9b8b90deb632fd5be839fe37a3e61ecaad1af8d36e07d199aab0630974"
SRC_URI[libstoragemgmt-arcconf-plugin.sha256sum] = "fa4a5476fc6811a806244aa18821c7747a0d92fbc7463c9528f7bc6d024e69aa"
SRC_URI[libstoragemgmt-hpsa-plugin.sha256sum] = "7355628b61ef5da0c025f7e5d4d9f302e747e9b7a64d7bbff493e4bd0f6dfeba"
SRC_URI[libstoragemgmt-local-plugin.sha256sum] = "06a195e98920d28362e19f0b45abeaf1d25ccdf96e5154d20144b9958d6a3ea6"
SRC_URI[libstoragemgmt-megaraid-plugin.sha256sum] = "90a0f05d0b6dc1aa00fcaa690057294e57849af0fe774bf16f8d73faa7990a43"
SRC_URI[libstoragemgmt-nfs-plugin.sha256sum] = "40388c706a4f3bc62ffe9fff116c8f96b8b336fc0e4025c158593894ed136224"
SRC_URI[libstoragemgmt-nfs-plugin-clibs.sha256sum] = "c1be4d0e5a3f2eba784d0f7a07d1c89b277f76cc1254ce3494eba9cdc18b3a1f"
SRC_URI[libstoragemgmt-nstor-plugin.sha256sum] = "8ec0314bbb9703c7e9edee056f5d706b174c5e445020eaa9c0110e19c3ce3a8f"
SRC_URI[libstoragemgmt-smis-plugin.sha256sum] = "48d64676761013a2116cdcee64ed7c9928ba19d259c35ee1b4d74c5dce923680"
SRC_URI[libstoragemgmt-udev.sha256sum] = "3c4f64a4e3830cd9575077a44d74592c938a06df8c0ac2543f1e63f3b5385511"
SRC_URI[python3-libstoragemgmt.sha256sum] = "0e0e7bbf0fa5d2c0e745c3f8d34969b9c3500a307d58fd35ac1c7446412bfb7c"
SRC_URI[python3-libstoragemgmt-clibs.sha256sum] = "fa29518afac3462ca43688ae1e01c48ed39f603e0f635a02a973848932e8731f"
