SUMMARY = "generated recipe based on libfontenc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libfontenc = "libfontenc.so.1"
RPM_SONAME_REQ_libfontenc = "libc.so.6 libz.so.1"
RDEPENDS_libfontenc = "glibc zlib"
RPM_SONAME_REQ_libfontenc-devel = "libfontenc.so.1"
RPROVIDES_libfontenc-devel = "libfontenc-dev (= 1.1.3)"
RDEPENDS_libfontenc-devel = "libfontenc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libfontenc-1.1.3-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libfontenc-devel-1.1.3-8.el8.x86_64.rpm \
          "

SRC_URI[libfontenc.sha256sum] = "720ff7efaff831952a1933270ce9df4dce61d5c718d447e3f24ba52945ce8212"
SRC_URI[libfontenc-devel.sha256sum] = "00df6d926fcad728806f898ce6907298b5999372304ff121db04181bf88d1ae0"
