SUMMARY = "generated recipe based on libsmi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsmi = "libsmi.so.2"
RPM_SONAME_REQ_libsmi = "libc.so.6 libm.so.6 libsmi.so.2"
RDEPENDS_libsmi = "bash gawk glibc wget"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsmi-0.4.8-22.el8.x86_64.rpm \
          "

SRC_URI[libsmi.sha256sum] = "69056d4d0f910b55b11262d4c34e6d69b9b2c744264116c41a47b37ae897534f"
