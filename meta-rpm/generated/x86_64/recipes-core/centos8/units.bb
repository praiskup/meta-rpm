SUMMARY = "generated recipe based on units srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native readline"
RPM_SONAME_REQ_units = "libc.so.6 libm.so.6 libreadline.so.7"
RDEPENDS_units = "bash glibc info platform-python readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/units-2.17-5.el8.x86_64.rpm \
          "

SRC_URI[units.sha256sum] = "a62db0abe6accf2fc14411242c7ea09aced8ba941701416c2ff422d6d39bbe3d"
