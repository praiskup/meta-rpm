SUMMARY = "generated recipe based on cifs-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "keyutils krb5-libs libcap-ng libtalloc pam pkgconfig-native samba"
RPM_SONAME_REQ_cifs-utils = "libc.so.6 libcap-ng.so.0 libdl.so.2 libkeyutils.so.1 libkrb5.so.3 libtalloc.so.2 libwbclient.so.0"
RDEPENDS_cifs-utils = "bash chkconfig glibc keyutils keyutils-libs krb5-libs libcap-ng libtalloc libwbclient"
RPM_SONAME_REQ_pam_cifscreds = "libc.so.6 libkeyutils.so.1 libpam.so.0"
RDEPENDS_pam_cifscreds = "glibc keyutils-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cifs-utils-6.8-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pam_cifscreds-6.8-3.el8.x86_64.rpm \
          "

SRC_URI[cifs-utils.sha256sum] = "c74cbec7b2d94dd58b96938a783575dda4009a3f59cea9bc27a957da7271fc5c"
SRC_URI[pam_cifscreds.sha256sum] = "00e2e8e2a77e8193088bf2109032e9c114b65df9dadeb858c74dd3cc19e813cf"
