SUMMARY = "generated recipe based on tzdata srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tzdata-java-2020d-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tzdata-2020d-1.el8.noarch.rpm \
          "

SRC_URI[tzdata.sha256sum] = "ab0715afb855849938c9c910a1292f59c82bcfa9ce92d9bf1c965ec540e0f5bd"
SRC_URI[tzdata-java.sha256sum] = "9df378a77f4eb314164292ead9c222ba4581552311b2c39ea91f130a383a69f9"
