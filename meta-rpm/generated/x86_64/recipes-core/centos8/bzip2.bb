SUMMARY = "generated recipe based on bzip2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_bzip2 = "libbz2.so.1 libc.so.6"
RDEPENDS_bzip2 = "bash bzip2-libs glibc"
RPM_SONAME_REQ_bzip2-devel = "libbz2.so.1"
RPROVIDES_bzip2-devel = "bzip2-dev (= 1.0.6)"
RDEPENDS_bzip2-devel = "bzip2-libs pkgconf-pkg-config"
RPM_SONAME_PROV_bzip2-libs = "libbz2.so.1"
RPM_SONAME_REQ_bzip2-libs = "libc.so.6"
RDEPENDS_bzip2-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bzip2-1.0.6-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bzip2-devel-1.0.6-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bzip2-libs-1.0.6-26.el8.x86_64.rpm \
          "

SRC_URI[bzip2.sha256sum] = "78596f457c3d737a97a4edfe9a03a01f593606379c281701ab7f7eba13ecaf18"
SRC_URI[bzip2-devel.sha256sum] = "8017441845e1aceb9c86a4787467bb970c2e9ee146d770a19319ff65dc59b6a5"
SRC_URI[bzip2-libs.sha256sum] = "19d66d152b745dbd49cea9d21c52aec0ec4d4321edef97a342acd3542404fa31"
