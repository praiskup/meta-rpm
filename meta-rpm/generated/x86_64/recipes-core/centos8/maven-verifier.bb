SUMMARY = "generated recipe based on maven-verifier srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-verifier = "java-1.8.0-openjdk-headless javapackages-filesystem junit maven-shared-utils"
RDEPENDS_maven-verifier-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-verifier-1.6-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-verifier-javadoc-1.6-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-verifier.sha256sum] = "01609810eeb2aa5cbd25f24ef02cb3bf1841c89213caed48f33a1b7bf9ed9dd2"
SRC_URI[maven-verifier-javadoc.sha256sum] = "a3b277b6f5e26336e7d2cf46bdb77700f2652b4a806c027244a45f0359367956"
