SUMMARY = "generated recipe based on perl-Unicode-LineBreak srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libthai perl pkgconfig-native sombok"
RPM_SONAME_REQ_perl-Unicode-LineBreak = "libc.so.6 libperl.so.5.26 libpthread.so.0 libsombok.so.3 libthai.so.0"
RDEPENDS_perl-Unicode-LineBreak = "glibc libthai perl-Carp perl-Encode perl-Exporter perl-MIME-Charset perl-constant perl-interpreter perl-libs sombok"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Unicode-LineBreak-2017.004-6.el8.x86_64.rpm \
          "

SRC_URI[perl-Unicode-LineBreak.sha256sum] = "78f9b5272ffeefb75b025b941bd0edda34279d6189e394cebde9b4fb232127da"
