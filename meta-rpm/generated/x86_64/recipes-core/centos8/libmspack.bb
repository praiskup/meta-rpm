SUMMARY = "generated recipe based on libmspack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmspack = "libmspack.so.0"
RPM_SONAME_REQ_libmspack = "libc.so.6"
RDEPENDS_libmspack = "glibc"
RPM_SONAME_REQ_libmspack-devel = "libmspack.so.0"
RPROVIDES_libmspack-devel = "libmspack-dev (= 0.7)"
RDEPENDS_libmspack-devel = "libmspack pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmspack-0.7-0.3.alpha.el8.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmspack-devel-0.7-0.3.alpha.el8.4.x86_64.rpm \
          "

SRC_URI[libmspack.sha256sum] = "02598f73a65df569906ad68fc51e6e2525e35797c7e814fc6cf42b787f1d1389"
SRC_URI[libmspack-devel.sha256sum] = "0dec3a7910f56360a63210d22aa5e910d05cc3a7f96f90c5d7d78311fc977fbb"
