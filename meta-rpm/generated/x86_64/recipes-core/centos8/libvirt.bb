SUMMARY = "generated recipe based on libvirt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl audit avahi-libs ceph curl cyrus-sasl-lib dbus-libs device-mapper-libs glusterfs gnutls libblkid libcap-ng libgcc libnl libpcap libpciaccess libselinux libssh libtirpc libuuid libxml2 netcf numactl parted pkgconfig-native readline sanlock systemd-libs yajl"
RDEPENDS_libvirt = "bash libvirt-client libvirt-daemon libvirt-daemon-config-network libvirt-daemon-config-nwfilter libvirt-daemon-driver-interface libvirt-daemon-driver-network libvirt-daemon-driver-nodedev libvirt-daemon-driver-nwfilter libvirt-daemon-driver-qemu libvirt-daemon-driver-secret libvirt-daemon-driver-storage libvirt-libs"
RPM_SONAME_REQ_libvirt-admin = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libreadline.so.7 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt-admin.so.0 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-admin = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-bash-completion libvirt-libs libxml2 numactl-libs readline yajl"
RPM_SONAME_REQ_libvirt-client = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libreadline.so.7 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-client = "audit-libs avahi-libs bash cyrus-sasl-lib dbus-libs device-mapper-libs gettext glibc gnutls gnutls-utils libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-bash-completion libvirt-libs libxml2 ncurses numactl-libs readline yajl"
RPM_SONAME_REQ_libvirt-daemon = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon = "audit-libs avahi-libs bash cyrus-sasl-lib dbus dbus-libs device-mapper-libs dmidecode glibc gnutls iproute iproute-tc kmod libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-libs libxml2 numactl-libs numad polkit shadow-utils systemd yajl"
RDEPENDS_libvirt-daemon-config-network = "bash libvirt-daemon libvirt-daemon-driver-network"
RDEPENDS_libvirt-daemon-config-nwfilter = "bash libvirt-daemon libvirt-daemon-driver-nwfilter"
RPM_SONAME_PROV_libvirt-daemon-driver-interface = "libvirt_driver_interface.so"
RPM_SONAME_REQ_libvirt-daemon-driver-interface = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnetcf.so.1 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libudev.so.1 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-interface = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 netcf-libs numactl-libs systemd-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-network = "libvirt_driver_network.so"
RPM_SONAME_REQ_libvirt-daemon-driver-network = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-network = "audit-libs avahi-libs bash cyrus-sasl-lib dbus-libs device-mapper-libs dnsmasq glibc gnutls iptables libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs radvd yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-nodedev = "libvirt_driver_nodedev.so"
RPM_SONAME_REQ_libvirt-daemon-driver-nodedev = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpciaccess.so.0 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libudev.so.1 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-nodedev = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libpciaccess libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs systemd systemd-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-nwfilter = "libvirt_driver_nwfilter.so"
RPM_SONAME_REQ_libvirt-daemon-driver-nwfilter = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpcap.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-nwfilter = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls iptables iptables-ebtables libacl libcap-ng libcurl libnl3 libpcap libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-qemu = "libvirt_driver_qemu.so"
RPM_SONAME_REQ_libvirt-daemon-driver-qemu = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-qemu = "audit-libs avahi-libs bash bzip2 cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls gzip libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-daemon-driver-network libvirt-daemon-driver-storage-core libvirt-libs libxml2 lzop numactl-libs qemu-img systemd-container xz yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-secret = "libvirt_driver_secret.so"
RPM_SONAME_REQ_libvirt-daemon-driver-secret = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-secret = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs yajl"
RDEPENDS_libvirt-daemon-driver-storage = "libvirt-daemon-driver-storage-core libvirt-daemon-driver-storage-disk libvirt-daemon-driver-storage-gluster libvirt-daemon-driver-storage-iscsi libvirt-daemon-driver-storage-logical libvirt-daemon-driver-storage-mpath libvirt-daemon-driver-storage-rbd libvirt-daemon-driver-storage-scsi"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-core = "libvirt_driver_storage.so libvirt_storage_backend_fs.so libvirt_storage_file_fs.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-core = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libblkid.so.1 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libparted.so.2 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-core = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libblkid libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 nfs-utils numactl-libs parted qemu-img util-linux yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-disk = "libvirt_storage_backend_disk.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-disk = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-disk = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs parted yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-gluster = "libvirt_storage_backend_gluster.so libvirt_storage_file_gluster.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-gluster = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgfapi.so.0 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libuuid.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-gluster = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc glusterfs-api glusterfs-cli glusterfs-libs gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libuuid libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-iscsi = "libvirt_storage_backend_iscsi.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-iscsi = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-iscsi = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls iscsi-initiator-utils libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-logical = "libvirt_storage_backend_logical.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-logical = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-logical = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 lvm2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-mpath = "libvirt_storage_backend_mpath.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-mpath = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-mpath = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-rbd = "libvirt_storage_backend_rbd.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-rbd = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 librados.so.2 librbd.so.1 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-rbd = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 librados2 librbd1 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RPM_SONAME_PROV_libvirt-daemon-driver-storage-scsi = "libvirt_storage_backend_scsi.so"
RPM_SONAME_REQ_libvirt-daemon-driver-storage-scsi = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-daemon-driver-storage-scsi = "audit-libs avahi-libs cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon-driver-storage-core libvirt-libs libxml2 numactl-libs yajl"
RDEPENDS_libvirt-daemon-kvm = "libvirt-daemon libvirt-daemon-driver-interface libvirt-daemon-driver-network libvirt-daemon-driver-nodedev libvirt-daemon-driver-nwfilter libvirt-daemon-driver-qemu libvirt-daemon-driver-secret libvirt-daemon-driver-storage qemu-kvm"
RPM_SONAME_REQ_libvirt-devel = "libvirt-admin.so.0 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0"
RPROVIDES_libvirt-devel = "libvirt-dev (= 4.5.0)"
RDEPENDS_libvirt-devel = "libvirt-libs pkgconf-pkg-config"
RPM_SONAME_PROV_libvirt-libs = "libvirt-admin.so.0 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0"
RPM_SONAME_REQ_libvirt-libs = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-libs = "audit-libs avahi-libs cyrus-sasl cyrus-sasl-gssapi cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libgcc libnl3 libselinux libssh libtirpc libxml2 nmap-ncat numactl-libs yajl"
RPM_SONAME_REQ_libvirt-lock-sanlock = "libacl.so.1 libaudit.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcap-ng.so.0 libcurl.so.4 libdbus-1.so.3 libdevmapper.so.1.02 libdl.so.2 libgnutls.so.30 libnl-3.so.200 libnl-route-3.so.200 libnuma.so.1 libpthread.so.0 libsanlock_client.so.1 libsasl2.so.3 libselinux.so.1 libssh.so.4 libtirpc.so.3 libutil.so.1 libvirt.so.0 libxml2.so.2 libyajl.so.2"
RDEPENDS_libvirt-lock-sanlock = "audit-libs augeas avahi-libs bash cyrus-sasl-lib dbus-libs device-mapper-libs glibc gnutls libacl libcap-ng libcurl libnl3 libselinux libssh libtirpc libvirt-daemon libvirt-libs libxml2 numactl-libs sanlock sanlock-lib yajl"
RPM_SONAME_PROV_libvirt-nss = "libnss_libvirt.so.2 libnss_libvirt_guest.so.2"
RPM_SONAME_REQ_libvirt-nss = "libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 libtirpc.so.3 libutil.so.1 libyajl.so.2"
RDEPENDS_libvirt-nss = "glibc libgcc libtirpc libvirt-daemon-driver-network yajl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-admin-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-bash-completion-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-client-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-config-network-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-config-nwfilter-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-interface-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-network-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-nodedev-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-nwfilter-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-qemu-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-secret-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-core-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-disk-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-gluster-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-iscsi-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-logical-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-mpath-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-rbd-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-driver-storage-scsi-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-daemon-kvm-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-devel-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-docs-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-libs-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-lock-sanlock-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-nss-4.5.0-42.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[libvirt.sha256sum] = "5da952a7ec975b325d18823b6403b99c896b5413e70901c7e99fb54b431b6e36"
SRC_URI[libvirt-admin.sha256sum] = "057c82a8eeacf1eaaea7d079bb80d63cc398eb382c83933bf1ba828a99fca579"
SRC_URI[libvirt-bash-completion.sha256sum] = "dd5d36a6f74c486e0bd4e5965f64bfda208dcfc305ed4cd1efe5122f01c8634a"
SRC_URI[libvirt-client.sha256sum] = "e84cf8b67108b11f5de0419e14b3c99ae1479beed3fd6d6432b05b28d16e7ffb"
SRC_URI[libvirt-daemon.sha256sum] = "8503bcd5e8d257f36d4d5db916b0a513061e8aa202d10cc5530be562585e564d"
SRC_URI[libvirt-daemon-config-network.sha256sum] = "cfc9de49ac41bd26b41f31298c4750dd99392ded8eaa79fe9733c9b6e0fe4a85"
SRC_URI[libvirt-daemon-config-nwfilter.sha256sum] = "b3e67c3ff8519825f204211291ba360ebcd9188df64b4d84c675b3f705a0d141"
SRC_URI[libvirt-daemon-driver-interface.sha256sum] = "849d5154920773b94aa01925716a8ebff6684eed1903477265070a4459c00ad9"
SRC_URI[libvirt-daemon-driver-network.sha256sum] = "fdb11bab74f42fecf304f932374d93c7323e890f165c0f0b4673824a5d43804c"
SRC_URI[libvirt-daemon-driver-nodedev.sha256sum] = "53059e22a0f27b00052bc9fcd1b9dbd15d23dc7a3d12e8c3762da76606bef18f"
SRC_URI[libvirt-daemon-driver-nwfilter.sha256sum] = "2f2792f5922044babf82aa813a1986c7e96880150fe17ada75d4532aa8689e36"
SRC_URI[libvirt-daemon-driver-qemu.sha256sum] = "cb81eea79d9c5220c55bdc5e1063091668c68a95e94166bacadf5d4859a03502"
SRC_URI[libvirt-daemon-driver-secret.sha256sum] = "b03919563e4964f9dbee49a8dff69295b82c668f0172d25c2c3249043e266739"
SRC_URI[libvirt-daemon-driver-storage.sha256sum] = "57735118183c9077e2de504565faaf8bd29a296ceb33b50229a66e44774f6645"
SRC_URI[libvirt-daemon-driver-storage-core.sha256sum] = "6355e14400c82efa706212c125bfc0136739c885c9a23bd6d80136c35a730bd0"
SRC_URI[libvirt-daemon-driver-storage-disk.sha256sum] = "bbb7001f9a692852494f2f15416c88f2964bccfb3d1eb1833792fbf33d7bbaba"
SRC_URI[libvirt-daemon-driver-storage-gluster.sha256sum] = "e2fddad2022d813ff75224065da2b34bee124ac911fc0f2d804cf7b9c9e2a0b6"
SRC_URI[libvirt-daemon-driver-storage-iscsi.sha256sum] = "e507ef100cb5b0076c1386a07c426ebe4afcbd6c74bf32d3473501419ee03149"
SRC_URI[libvirt-daemon-driver-storage-logical.sha256sum] = "2d9aac3d3287f2b8ecfa8703c5a6111880add98aa4588a1ad2a86d37cd41050e"
SRC_URI[libvirt-daemon-driver-storage-mpath.sha256sum] = "1966099abd435e3b011ca99a23d28be3a06edfd3ff1bcb6238dcc0f294c188a3"
SRC_URI[libvirt-daemon-driver-storage-rbd.sha256sum] = "8e065d6ee14d53f3187b2b7d875fd3344373e2c3016c7653f76ab3070fb878ef"
SRC_URI[libvirt-daemon-driver-storage-scsi.sha256sum] = "6828e1e779ce8c0be0e705730f4a60c831039ec4fd9b1849e995a8b7e9bea22f"
SRC_URI[libvirt-daemon-kvm.sha256sum] = "e5decd47ba25bb18a9ae7f5fc2361ef7ab43e7af81b26a6d01da9655f3599b3f"
SRC_URI[libvirt-devel.sha256sum] = "36fb8d82f8592644284a244f7c51224bd592375ebce4042e1ac573cc393fd6f9"
SRC_URI[libvirt-docs.sha256sum] = "3fc45c37367dc62e7edb4e54f92e898e4f456c6291ef571a67ee47a060b726f8"
SRC_URI[libvirt-libs.sha256sum] = "0fdfd98176d1328078f86d3950feff12a9782122e927f0407f2880e60b313c29"
SRC_URI[libvirt-lock-sanlock.sha256sum] = "792fa7964eae3a3856d817f49641f022207c7b9ef51db16068f48fd890b82f09"
SRC_URI[libvirt-nss.sha256sum] = "d4b422de2773ab648f990e78ec55f1d4641be8331ed4bd958d931bf991803ea0"
