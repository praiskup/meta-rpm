SUMMARY = "generated recipe based on apache-commons-codec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-codec = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-codec-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-codec-1.11-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-codec-javadoc-1.11-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-codec.sha256sum] = "4f3ff3d2e3a8ff68fb8df8fe5738aeaf7a36b274698b7366540dcac752d47e47"
SRC_URI[apache-commons-codec-javadoc.sha256sum] = "2170efd30d67423ecf11cb6e0e284531663314e8df960ff3a8ad688cb382295c"
