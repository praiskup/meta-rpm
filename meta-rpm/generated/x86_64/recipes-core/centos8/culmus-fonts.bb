SUMMARY = "generated recipe based on culmus-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_culmus-aharoni-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-caladings-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-david-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-drugulin-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-ellinia-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-fonts-common = "fontpackages-filesystem"
RDEPENDS_culmus-frank-ruehl-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-hadasim-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-keteryg-fonts = "culmus-fonts-common"
RDEPENDS_culmus-miriam-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-miriam-mono-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-nachlieli-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-shofar-fonts = "culmus-fonts-common"
RDEPENDS_culmus-simple-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-stamashkenaz-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-stamsefarad-clm-fonts = "culmus-fonts-common"
RDEPENDS_culmus-yehuda-clm-fonts = "culmus-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-aharoni-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-caladings-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-david-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-drugulin-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-ellinia-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-fonts-common-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-frank-ruehl-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-hadasim-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-keteryg-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-miriam-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-miriam-mono-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-nachlieli-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-shofar-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-simple-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-stamashkenaz-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-stamsefarad-clm-fonts-0.130-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/culmus-yehuda-clm-fonts-0.130-12.el8.noarch.rpm \
          "

SRC_URI[culmus-aharoni-clm-fonts.sha256sum] = "61e3977365093b96f32b1d4e6ec752ac473e26ea35e31f4ab5c2adb9c8418177"
SRC_URI[culmus-caladings-clm-fonts.sha256sum] = "3d335048c9ca709fc19734b2fa2fd2266d34894afc397453e524d13a607f4219"
SRC_URI[culmus-david-clm-fonts.sha256sum] = "d4ceb080b240be8197a7a1e03a7cf710025d88bca34315eccce1bd3e318c4577"
SRC_URI[culmus-drugulin-clm-fonts.sha256sum] = "c33eb3cc8d1953ad6234f567edf9fac9ec5b6f06a32e1ed17ef686d3ade104a4"
SRC_URI[culmus-ellinia-clm-fonts.sha256sum] = "18390a3f039819d74a36f835c42fe090eec0d16caf66974adc787dffdf6e2113"
SRC_URI[culmus-fonts-common.sha256sum] = "71dd28e805e2d469930f1358d33cbe2d1a2c55d16aa946c859a6c0bdd9abbdbc"
SRC_URI[culmus-frank-ruehl-clm-fonts.sha256sum] = "d5d36e883ed572c187159f278d89022b07bc799bc6afe36b23ccb5fd54581bf9"
SRC_URI[culmus-hadasim-clm-fonts.sha256sum] = "bed30d1d17f3d0c51a30142f02a4b17c5d5c78034a480dca432a9becaf17350e"
SRC_URI[culmus-keteryg-fonts.sha256sum] = "0f2979e6602a4bd9eb52af282bbb19c1539631fc83e656a596dbf21d006a5778"
SRC_URI[culmus-miriam-clm-fonts.sha256sum] = "e91095adb816e8b9c8e23bf795d45cff0d628f4368e32453ab2325c743bafd06"
SRC_URI[culmus-miriam-mono-clm-fonts.sha256sum] = "602e0042e8a8512bba76db88df7ad5bbd0be556941e443d9c03cda966f69a453"
SRC_URI[culmus-nachlieli-clm-fonts.sha256sum] = "c9435dbbfb78c50d6ac6c414e4ed681b1942225df7159a7ccf9ff6109a6754c5"
SRC_URI[culmus-shofar-fonts.sha256sum] = "4011e4a17726ba7114eeeb6178908fd1026863900a35a15efd70b893a11ed50c"
SRC_URI[culmus-simple-clm-fonts.sha256sum] = "3a26620955858a95b817fb313c8bdfa6fe88945a3e7a6c2ba8f85698c971175b"
SRC_URI[culmus-stamashkenaz-clm-fonts.sha256sum] = "8da937269ca65b8f6d00723d25a3270aa1d259f81947f7877de5e5826d2247a3"
SRC_URI[culmus-stamsefarad-clm-fonts.sha256sum] = "0bba05331a1cad16140376f6c53718aaa0321f1d60246f977e5c016b1822a457"
SRC_URI[culmus-yehuda-clm-fonts.sha256sum] = "d4a00f6b3e57b02251bdb835a96995a0b542062d8f654e6f6bde88dc7d1805f5"
