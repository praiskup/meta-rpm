SUMMARY = "generated recipe based on libnetfilter_cttimeout srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_cttimeout = "libnetfilter_cttimeout.so.1"
RPM_SONAME_REQ_libnetfilter_cttimeout = "libc.so.6 libmnl.so.0"
RDEPENDS_libnetfilter_cttimeout = "glibc libmnl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnetfilter_cttimeout-1.0.0-11.el8.x86_64.rpm \
          "

SRC_URI[libnetfilter_cttimeout.sha256sum] = "1ec9b84708a45c425a19e7112643686906a7529ce3648e902b341e3172e733c9"
