SUMMARY = "generated recipe based on ladspa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_ladspa = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_ladspa = "glibc libgcc libstdc++"
RPROVIDES_ladspa-devel = "ladspa-dev (= 1.13)"
RDEPENDS_ladspa-devel = "ladspa"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ladspa-1.13-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ladspa-devel-1.13-20.el8.x86_64.rpm \
          "

SRC_URI[ladspa.sha256sum] = "e0dd23a0999a0c7e55d4940f43d744717932553716be7b52a35abc0d32059a44"
SRC_URI[ladspa-devel.sha256sum] = "d6b3d94431153c480f2cd12c7ec078df231c8560d8060f1f7b89d27e471569ba"
