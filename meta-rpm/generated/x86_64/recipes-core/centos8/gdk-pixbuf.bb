SUMMARY = "generated recipe based on gdk-pixbuf2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 jasper libjpeg-turbo libpng libx11 pkgconfig-native tiff zlib"
RPM_SONAME_PROV_gdk-pixbuf2 = "libgdk_pixbuf-2.0.so.0"
RPM_SONAME_REQ_gdk-pixbuf2 = "libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_gdk-pixbuf2 = "bash glib2 glibc libpng shared-mime-info zlib"
RPM_SONAME_REQ_gdk-pixbuf2-devel = "libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RPROVIDES_gdk-pixbuf2-devel = "gdk-pixbuf2-dev (= 2.36.12)"
RDEPENDS_gdk-pixbuf2-devel = "gdk-pixbuf2 glib2 glib2-devel glibc libpng libpng-devel pkgconf-pkg-config zlib"
RPM_SONAME_PROV_gdk-pixbuf2-modules = "libpixbufloader-ani.so libpixbufloader-bmp.so libpixbufloader-gif.so libpixbufloader-icns.so libpixbufloader-ico.so libpixbufloader-jasper.so libpixbufloader-jpeg.so libpixbufloader-pnm.so libpixbufloader-qtif.so libpixbufloader-tga.so libpixbufloader-tiff.so libpixbufloader-xbm.so libpixbufloader-xpm.so"
RPM_SONAME_REQ_gdk-pixbuf2-modules = "libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjasper.so.4 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libtiff.so.5 libz.so.1"
RDEPENDS_gdk-pixbuf2-modules = "gdk-pixbuf2 glib2 glibc jasper-libs libjpeg-turbo libpng libtiff zlib"
RPM_SONAME_PROV_gdk-pixbuf2-xlib = "libgdk_pixbuf_xlib-2.0.so.0"
RPM_SONAME_REQ_gdk-pixbuf2-xlib = "libX11.so.6 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_gdk-pixbuf2-xlib = "gdk-pixbuf2 glib2 glibc libX11 libpng zlib"
RPM_SONAME_REQ_gdk-pixbuf2-xlib-devel = "libgdk_pixbuf_xlib-2.0.so.0"
RPROVIDES_gdk-pixbuf2-xlib-devel = "gdk-pixbuf2-xlib-dev (= 2.36.12)"
RDEPENDS_gdk-pixbuf2-xlib-devel = "gdk-pixbuf2-devel gdk-pixbuf2-xlib glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gdk-pixbuf2-devel-2.36.12-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gdk-pixbuf2-modules-2.36.12-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gdk-pixbuf2-2.36.12-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gdk-pixbuf2-xlib-2.36.12-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gdk-pixbuf2-xlib-devel-2.36.12-5.el8.x86_64.rpm \
          "

SRC_URI[gdk-pixbuf2.sha256sum] = "94cb8dceb47a5b01e3c0542ea3b48601d720325da28e6e6d89ae529e4fddcd97"
SRC_URI[gdk-pixbuf2-devel.sha256sum] = "cb57dca67a2a2ada0da5941d60447de7e76fddc7feba8ddf25efa34e7fb8372e"
SRC_URI[gdk-pixbuf2-modules.sha256sum] = "f689bcf0759fcf467ef007f8a640e2829c05879a0a1ff7d3a3fd36130230f2c9"
SRC_URI[gdk-pixbuf2-xlib.sha256sum] = "14cc3a9bfd31208244e6b5b93fb5092e6788b3a43a0b7d5fc7bf4ff9acc57208"
SRC_URI[gdk-pixbuf2-xlib-devel.sha256sum] = "f3f22f8a470e80823a34ae5685438a80932cebfa519c69b4c7da1549415e9d18"
