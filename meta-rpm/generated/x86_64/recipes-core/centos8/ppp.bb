SUMMARY = "generated recipe based on ppp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libpcap libxcrypt openssl pam pkgconfig-native systemd-libs"
RPM_SONAME_REQ_ppp = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libglib-2.0.so.0 libpam.so.0 libpcap.so.1 libresolv.so.2 libssl.so.1.1 libudev.so.1 libutil.so.1"
RDEPENDS_ppp = "bash glib2 glibc glibc-common libpcap libxcrypt openssl-libs pam shadow-utils systemd systemd-libs"
RPROVIDES_ppp-devel = "ppp-dev (= 2.4.7)"
RDEPENDS_ppp-devel = "ppp"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ppp-2.4.7-26.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ppp-devel-2.4.7-26.el8_1.x86_64.rpm \
          "

SRC_URI[ppp.sha256sum] = "90dc7074268382b30131afe0121e814e6badff5cda1506fd9bb0d08a63a632ac"
SRC_URI[ppp-devel.sha256sum] = "cca654b8244f318cc80010444211fa72ed481a9342953a19449ba3c9ec3ed24a"
