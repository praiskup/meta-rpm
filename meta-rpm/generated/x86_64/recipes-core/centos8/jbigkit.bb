SUMMARY = "generated recipe based on jbigkit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_jbigkit-devel = "libjbig.so.2.1 libjbig85.so.2.1"
RPROVIDES_jbigkit-devel = "jbigkit-dev (= 2.1)"
RDEPENDS_jbigkit-devel = "jbigkit-libs"
RPM_SONAME_PROV_jbigkit-libs = "libjbig.so.2.1 libjbig85.so.2.1"
RPM_SONAME_REQ_jbigkit-libs = "libc.so.6"
RDEPENDS_jbigkit-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jbigkit-libs-2.1-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jbigkit-devel-2.1-14.el8.x86_64.rpm \
          "

SRC_URI[jbigkit-devel.sha256sum] = "68992aa6b64308cae177228524d4dd24eb160ff6be436c27302bca221a4cce70"
SRC_URI[jbigkit-libs.sha256sum] = "d5714bdf7c4e3b66540a43c5d58328dcef4f2360e58c578f2c5bbfce66651ad7"
