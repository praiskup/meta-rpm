SUMMARY = "generated recipe based on libhangul srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libhangul = "libhangul.so.1"
RPM_SONAME_REQ_libhangul = "libc.so.6 libhangul.so.1"
RDEPENDS_libhangul = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libhangul-0.1.0-16.el8.x86_64.rpm \
          "

SRC_URI[libhangul.sha256sum] = "52994d2924b968ef35b4901c0cfb13a5f73789f1d8c8bf89da3a08e81818a47d"
