SUMMARY = "generated recipe based on xmlunit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xmlunit = "java-1.8.0-openjdk-headless javapackages-filesystem junit xalan-j2 xml-commons-apis"
RDEPENDS_xmlunit-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlunit-1.6-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmlunit-javadoc-1.6-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xmlunit.sha256sum] = "f112222ba58c7e8b7c9fadb870c95e65cf07c8604449a3b3786d245d5a442d38"
SRC_URI[xmlunit-javadoc.sha256sum] = "687b59da45dea313a0393472bbe968753f25c82033a48fd947a0409fe2266b13"
