SUMMARY = "generated recipe based on kabi-dw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_kabi-dw = "libc.so.6 libdw.so.1 libelf.so.1"
RDEPENDS_kabi-dw = "elfutils-libelf elfutils-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kabi-dw-0-0.8.20181112git6fbd644.el8.x86_64.rpm \
          "

SRC_URI[kabi-dw.sha256sum] = "f539691e42c0e7c722e762d9706280a0dc7c4a0486575b8ba4befa61eeedb395"
