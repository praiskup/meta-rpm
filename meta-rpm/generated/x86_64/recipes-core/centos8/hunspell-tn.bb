SUMMARY = "generated recipe based on hunspell-tn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tn = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-tn-0.20150904-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-tn.sha256sum] = "099769a50e428e9cf37308310225fbdb822bcb2ecfffd87cf0530c37a0b15ece"
