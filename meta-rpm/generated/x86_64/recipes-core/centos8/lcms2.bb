SUMMARY = "generated recipe based on lcms2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lcms2 = "liblcms2.so.2"
RPM_SONAME_REQ_lcms2 = "libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_lcms2 = "glibc"
RPM_SONAME_REQ_lcms2-devel = "liblcms2.so.2"
RPROVIDES_lcms2-devel = "lcms2-dev (= 2.9)"
RDEPENDS_lcms2-devel = "lcms2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lcms2-2.9-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lcms2-devel-2.9-2.el8.x86_64.rpm \
          "

SRC_URI[lcms2.sha256sum] = "7ff6c7d3b55a8ea9c33e4e50e0420184a27f91badbd20ca7a24432b5ac60371c"
SRC_URI[lcms2-devel.sha256sum] = "c8e5118916dd924d45e54a5745012fc36aed42347c515b2a245b0d6fef206829"
