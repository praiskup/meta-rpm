SUMMARY = "generated recipe based on mpfr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp pkgconfig-native"
RPM_SONAME_PROV_mpfr = "libmpfr.so.4"
RPM_SONAME_REQ_mpfr = "ld-linux-x86-64.so.2 libc.so.6 libgmp.so.10"
RDEPENDS_mpfr = "glibc gmp"
RPM_SONAME_REQ_mpfr-devel = "libmpfr.so.4"
RPROVIDES_mpfr-devel = "mpfr-dev (= 3.1.6)"
RDEPENDS_mpfr-devel = "bash gmp-devel info mpfr"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mpfr-devel-3.1.6-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mpfr-3.1.6-1.el8.x86_64.rpm \
          "

SRC_URI[mpfr.sha256sum] = "e7f0c34f83c1ec2abb22951779e84d51e234c4ba0a05252e4ffd8917461891a5"
SRC_URI[mpfr-devel.sha256sum] = "0b65e6154433d132f1ed7d4f623cc29303e5e3090b36ac38a5e71a7097ec6757"
