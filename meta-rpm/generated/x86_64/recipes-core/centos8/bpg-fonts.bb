SUMMARY = "generated recipe based on bpg-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_bpg-algeti-fonts = "bpg-fonts-common"
RDEPENDS_bpg-chveulebrivi-fonts = "bpg-fonts-common"
RDEPENDS_bpg-classic-fonts = "bpg-fonts-common"
RDEPENDS_bpg-courier-fonts = "bpg-fonts-common"
RDEPENDS_bpg-courier-s-fonts = "bpg-fonts-common"
RDEPENDS_bpg-dedaena-block-fonts = "bpg-fonts-common"
RDEPENDS_bpg-dejavu-sans-fonts = "bpg-fonts-common"
RDEPENDS_bpg-elite-fonts = "bpg-fonts-common"
RDEPENDS_bpg-excelsior-caps-fonts = "bpg-fonts-common"
RDEPENDS_bpg-excelsior-condenced-fonts = "bpg-fonts-common"
RDEPENDS_bpg-excelsior-fonts = "bpg-fonts-common"
RDEPENDS_bpg-fonts-common = "fontpackages-filesystem"
RDEPENDS_bpg-glaho-fonts = "bpg-fonts-common"
RDEPENDS_bpg-gorda-fonts = "bpg-fonts-common"
RDEPENDS_bpg-ingiri-fonts = "bpg-fonts-common"
RDEPENDS_bpg-irubaqidze-fonts = "bpg-fonts-common"
RDEPENDS_bpg-mikhail-stephan-fonts = "bpg-fonts-common"
RDEPENDS_bpg-mrgvlovani-caps-fonts = "bpg-fonts-common"
RDEPENDS_bpg-mrgvlovani-fonts = "bpg-fonts-common"
RDEPENDS_bpg-nateli-caps-fonts = "bpg-fonts-common"
RDEPENDS_bpg-nateli-condenced-fonts = "bpg-fonts-common"
RDEPENDS_bpg-nateli-fonts = "bpg-fonts-common"
RDEPENDS_bpg-nino-medium-cond-fonts = "bpg-fonts-common"
RDEPENDS_bpg-nino-medium-fonts = "bpg-fonts-common"
RDEPENDS_bpg-sans-fonts = "bpg-fonts-common"
RDEPENDS_bpg-sans-medium-fonts = "bpg-fonts-common"
RDEPENDS_bpg-sans-modern-fonts = "bpg-fonts-common"
RDEPENDS_bpg-sans-regular-fonts = "bpg-fonts-common"
RDEPENDS_bpg-serif-fonts = "bpg-fonts-common"
RDEPENDS_bpg-serif-modern-fonts = "bpg-fonts-common"
RDEPENDS_bpg-ucnobi-fonts = "bpg-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-algeti-fonts-2.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-chveulebrivi-fonts-3.002-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-classic-fonts-8.500-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-courier-fonts-4.002-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-courier-s-fonts-4.000-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-dedaena-block-fonts-3.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-dejavu-sans-fonts-2.28-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-elite-fonts-3.000-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-excelsior-caps-fonts-2.003-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-excelsior-condenced-fonts-2.003-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-excelsior-fonts-2.03-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-fonts-common-20120413-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-glaho-fonts-9.000-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-gorda-fonts-2.003-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-ingiri-fonts-4.000-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-irubaqidze-fonts-1.000-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-mikhail-stephan-fonts-2.500-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-mrgvlovani-caps-fonts-1.002-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-mrgvlovani-fonts-1.002-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-nateli-caps-fonts-2.003-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-nateli-condenced-fonts-2.003-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-nateli-fonts-2.003-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-nino-medium-cond-fonts-4.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-nino-medium-fonts-4.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-sans-fonts-1.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-sans-medium-fonts-1.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-sans-modern-fonts-2.025-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-sans-regular-fonts-1.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-serif-fonts-1.005-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-serif-modern-fonts-2.028-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpg-ucnobi-fonts-3.300-10.el8.noarch.rpm \
          "

SRC_URI[bpg-algeti-fonts.sha256sum] = "05fb715f2018e0c77c29afa2cb26920301c75165d8462ae44086ec4f2fb79633"
SRC_URI[bpg-chveulebrivi-fonts.sha256sum] = "4ac2ef309d4fdd486ab40e8bd3ccb581f47242dfc8043b127d03ac13f50fb9aa"
SRC_URI[bpg-classic-fonts.sha256sum] = "bc5b5c9a5df9dcae5e59d849986dae0a5d19b6bc10204004406b1ee709900412"
SRC_URI[bpg-courier-fonts.sha256sum] = "cce4acc5a91f9e30b941cf499f0a5ee788fac76527adc7da48f08db746086ba6"
SRC_URI[bpg-courier-s-fonts.sha256sum] = "21db78809feff7b7227160753c059faea5a3950fb2729bb6b3dcaf90994ff9b1"
SRC_URI[bpg-dedaena-block-fonts.sha256sum] = "74f6c892ed15e7f696713a06591fb2d0950b3812f5d513bad42801207c88c8e7"
SRC_URI[bpg-dejavu-sans-fonts.sha256sum] = "1875ff3da8dba3e410aeea2f96ad11db072d497109aa4794d2d2b6768812ed21"
SRC_URI[bpg-elite-fonts.sha256sum] = "5a163c958597b2158653c47174f952d78401cbf87cfc49c32ab2a1cd477447e1"
SRC_URI[bpg-excelsior-caps-fonts.sha256sum] = "d2f37c8dda8091d0e493dbaf7bf4f4bdfc4f8657749d451de05e95da846d1a33"
SRC_URI[bpg-excelsior-condenced-fonts.sha256sum] = "b19049083b58f5162ab29481f8d7a7776bf16fd59af346ea4360642a68803591"
SRC_URI[bpg-excelsior-fonts.sha256sum] = "149d23dc5be03c5e55fb20c279c60a2a05d52cec719a92a07fb5eb5e173f9f78"
SRC_URI[bpg-fonts-common.sha256sum] = "b885033ba2f4e7a72fa68aec2d73ca5edccdd735899f563150b2705af662cf60"
SRC_URI[bpg-glaho-fonts.sha256sum] = "1bb72119849ab4c183d09ad91f553366745aab65221273dadc909d3f054846a3"
SRC_URI[bpg-gorda-fonts.sha256sum] = "06465aa504b891154ffdebeac78797c2483c1c0b761cf3ea5325cc25e03754a0"
SRC_URI[bpg-ingiri-fonts.sha256sum] = "e861401d0ca34abae4c1ffff206b60b0505ae2984fa19e0e6a57501f1c516798"
SRC_URI[bpg-irubaqidze-fonts.sha256sum] = "e0f27019c7c6cb30d6df630063630fb9fa270c5fe94c4f5f5f95204cdcd8d7bc"
SRC_URI[bpg-mikhail-stephan-fonts.sha256sum] = "a6dda3f27b8eeb452b6faed4be3135a45d873f4a1c9adf4986b48373325cf496"
SRC_URI[bpg-mrgvlovani-caps-fonts.sha256sum] = "f11c1104b7edcfe36153699710cba4a66c02cca9f95f8afb2cc744afdc028cf7"
SRC_URI[bpg-mrgvlovani-fonts.sha256sum] = "4ad5dc0b60d0cf4733906fde2abc2f502aeeb2f683a47f13b3d3698307e4dce9"
SRC_URI[bpg-nateli-caps-fonts.sha256sum] = "e94f14bca832a53eefd92536699b082f16fd33e6dddfa781107e05384ffe39b0"
SRC_URI[bpg-nateli-condenced-fonts.sha256sum] = "a0321155dadc1672c672ebaa8dfb53fa9224d14ca19dc89628c30189dd59ea19"
SRC_URI[bpg-nateli-fonts.sha256sum] = "f59d369291cb52d09e65e1bf151118dd803c17e0acde87cfc4ee50c128d4b828"
SRC_URI[bpg-nino-medium-cond-fonts.sha256sum] = "96c6aaf375499ef2f2381679ac5a37f32e7d4f3a47c2268922daafe7cd01f129"
SRC_URI[bpg-nino-medium-fonts.sha256sum] = "8c63e87177102b0e26e1e629153caf039d72fef6278ec542ae6f8661f1103d84"
SRC_URI[bpg-sans-fonts.sha256sum] = "ce281bb3fd68222838054ad8d634a72712d066e4f3881d411ba2e027b7fea374"
SRC_URI[bpg-sans-medium-fonts.sha256sum] = "8c1a7643ca1031b305e9cda9584ff7ad2251a8a95b1ba58d26ab02d16f764614"
SRC_URI[bpg-sans-modern-fonts.sha256sum] = "231672b2ea748ef67505c319b0c62c20bdf47ce2c88ea093360e9881760f67e1"
SRC_URI[bpg-sans-regular-fonts.sha256sum] = "d79e11709d18601ec0fe2e43572cf7a926462c56c291d89d70a3ace3ac8ee2a6"
SRC_URI[bpg-serif-fonts.sha256sum] = "70e0e750dc14e7a8ae12e7c66840916eec69eba61a8c85e2e225a271fa6ff09d"
SRC_URI[bpg-serif-modern-fonts.sha256sum] = "20b9aac03d6432e07172576076e8ec6c068bba67c9d5b7f04e015f29d5747561"
SRC_URI[bpg-ucnobi-fonts.sha256sum] = "2df06994aaea9e467f25d26b9508fecfad7218b91b3a1455ed68340146156752"
