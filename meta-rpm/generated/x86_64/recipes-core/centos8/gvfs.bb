SUMMARY = "generated recipe based on gvfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs fuse gcr glib-2.0 gnome-online-accounts json-glib libarchive libcap libcdio libcdio-paranoia libgcrypt libgdata libgpg-error libgphoto2 libgudev libimobiledevice libmtp libplist libsecret libsoup-2.4 libusb1 libxml2 p11-kit pkgconfig-native polkit samba systemd-libs udisks2"
RPM_SONAME_PROV_gvfs = "libgvfsdaemon.so"
RPM_SONAME_REQ_gvfs = "libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcap.so.2 libcdio.so.18 libcdio_cdda.so.2 libcdio_paranoia.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libgvfscommon.so libgvfsdaemon.so libm.so.6 libp11-kit.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libsystemd.so.0 libudisks2.so.0 libutil.so.1 libxml2.so.2"
RDEPENDS_gvfs = "avahi-glib avahi-libs bash desktop-file-utils gcr glib2 glibc gsettings-desktop-schemas gvfs-client libcap libcdio libcdio-paranoia libgudev libsecret libsoup libudisks2 libxml2 p11-kit polkit-libs systemd-libs udisks2"
RPM_SONAME_REQ_gvfs-afc = "libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libimobiledevice.so.6 libp11-kit.so.0 libplist.so.3 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-afc = "bash gcr glib2 glibc gvfs gvfs-client libimobiledevice libplist libsecret p11-kit usbmuxd"
RPM_SONAME_REQ_gvfs-afp = "libc.so.6 libdl.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgvfscommon.so libgvfsdaemon.so libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-afp = "bash gcr glib2 glibc gvfs gvfs-client libgcrypt libgpg-error libsecret p11-kit"
RPM_SONAME_REQ_gvfs-archive = "libarchive.so.13 libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-archive = "bash gcr glib2 glibc gvfs gvfs-client libarchive libsecret p11-kit"
RPM_SONAME_PROV_gvfs-client = "libgioremote-volume-monitor.so libgvfscommon.so libgvfsdbus.so"
RPM_SONAME_REQ_gvfs-client = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libpthread.so.0 libutil.so.1"
RDEPENDS_gvfs-client = "glib2 glibc"
RPROVIDES_gvfs-devel = "gvfs-dev (= 1.36.2)"
RDEPENDS_gvfs-devel = "gvfs-client"
RPM_SONAME_REQ_gvfs-fuse = "libc.so.6 libfuse.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libpthread.so.0 libutil.so.1"
RDEPENDS_gvfs-fuse = "fuse fuse-libs glib2 glibc gvfs gvfs-client"
RPM_SONAME_REQ_gvfs-goa = "libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgdata.so.22 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libjson-glib-1.0.so.0 libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libutil.so.1 libxml2.so.2"
RDEPENDS_gvfs-goa = "bash gcr glib2 glibc gnome-online-accounts gvfs gvfs-client json-glib libgdata libsecret libsoup libxml2 p11-kit"
RPM_SONAME_REQ_gvfs-gphoto2 = "libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgphoto2.so.6 libgphoto2_port.so.12 libgudev-1.0.so.0 libgvfscommon.so libgvfsdaemon.so libm.so.6 libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libutil.so.1"
RDEPENDS_gvfs-gphoto2 = "bash gcr glib2 glibc gvfs gvfs-client libgphoto2 libgudev libsecret p11-kit"
RPM_SONAME_REQ_gvfs-mtp = "libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libgvfscommon.so libgvfsdaemon.so libmtp.so.9 libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libusb-1.0.so.0 libutil.so.1"
RDEPENDS_gvfs-mtp = "bash gcr glib2 glibc gvfs gvfs-client libgudev libmtp libsecret libusbx p11-kit"
RPM_SONAME_REQ_gvfs-smb = "libc.so.6 libgck-1.so.0 libgcr-base-3.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgvfscommon.so libgvfsdaemon.so libp11-kit.so.0 libpthread.so.0 libsecret-1.so.0 libsmbclient.so.0 libutil.so.1"
RDEPENDS_gvfs-smb = "bash gcr glib2 glibc gvfs gvfs-client libsecret libsmbclient p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-afc-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-afp-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-archive-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-client-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-devel-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-fuse-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-goa-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-gphoto2-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-mtp-1.36.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvfs-smb-1.36.2-8.el8.x86_64.rpm \
          "

SRC_URI[gvfs.sha256sum] = "fdb2e3d3b6abf8ff1399210938ddb66c84868f6ae44acfdf71624a2e7faa77ac"
SRC_URI[gvfs-afc.sha256sum] = "c032ad528417e9c9a2e7f4fc31e8557cad433c2c09fe7005abc83b991756dbe9"
SRC_URI[gvfs-afp.sha256sum] = "70d4a2ed2bd7cfc518d96cb23eca2861c26b4657cbbb4f2e974fea1323ef0de3"
SRC_URI[gvfs-archive.sha256sum] = "95a572527c668a1cd355ebd9c9fe7098bccea9270909e72b9587d6b79b0d2761"
SRC_URI[gvfs-client.sha256sum] = "d05e877a1e78f7a4fcffe34c1408e66d010da2357b2fad49497e377af630e17b"
SRC_URI[gvfs-devel.sha256sum] = "c2d7d73c8f1beada4f7fadc9a25f47fa68499f3398f6a1271c88e4cdc3aed59a"
SRC_URI[gvfs-fuse.sha256sum] = "93b50984a32fd3dbdb20a2239e469151bd15e7e0771c6cb57f3c0da3ab1f66be"
SRC_URI[gvfs-goa.sha256sum] = "c244b18fed3e0da0afe3216804df09f32fe7802d3221e2d0eed60a06ea2ea552"
SRC_URI[gvfs-gphoto2.sha256sum] = "46266fd918939f093484e45bd3fb5e59fee4c0da70a792c27d5c6d8cf6a1ea9b"
SRC_URI[gvfs-mtp.sha256sum] = "87b807b85ee19949bc312bb783283be279e2a4bd899b892bb72571c750b4c941"
SRC_URI[gvfs-smb.sha256sum] = "c38e8e384729669e5c82a21f617d86972b95e10565700b0a4943834e49fa733e"
