SUMMARY = "generated recipe based on gcc-toolset-9-dwz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-dwz = "libc.so.6 libelf.so.1"
RDEPENDS_gcc-toolset-9-dwz = "elfutils-libelf gcc-toolset-9-runtime glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-dwz-0.12-1.1.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-dwz.sha256sum] = "507e1f327cb8fa1a495f909b7a0753a941b7a654ec595cb544227cad31d1eea6"
