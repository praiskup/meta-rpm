SUMMARY = "generated recipe based on hunspell-mos srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mos = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mos-0.20101130-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-mos.sha256sum] = "272053bd69c1908cb4d7e1324956faa36e035880dbe024ea67a5c3be22d7772f"
