SUMMARY = "generated recipe based on pam srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit cracklib db libnsl2 libselinux libtirpc libxcrypt pkgconfig-native"
RPM_SONAME_PROV_pam = "libpam.so.0 libpam_misc.so.0 libpamc.so.0"
RPM_SONAME_REQ_pam = "libaudit.so.1 libc.so.6 libcrack.so.2 libcrypt.so.1 libdb-5.3.so libdl.so.2 libnsl.so.2 libpam.so.0 libselinux.so.1 libtirpc.so.3 libutil.so.1"
RDEPENDS_pam = "audit-libs bash coreutils cracklib glibc libdb libnsl2 libpwquality libselinux libtirpc libxcrypt"
RPM_SONAME_REQ_pam-devel = "libpam.so.0 libpam_misc.so.0 libpamc.so.0"
RPROVIDES_pam-devel = "pam-dev (= 1.3.1)"
RDEPENDS_pam-devel = "pam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pam-1.3.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pam-devel-1.3.1-8.el8.x86_64.rpm \
          "

SRC_URI[pam.sha256sum] = "20c9fc8cb38ca75667713ff9a02ad30c41f93c2dd42fe9704d88925b5fd898e0"
SRC_URI[pam-devel.sha256sum] = "f715add25133ad6414b280ef17eb86870607da651b063eb520379297652b5445"
