SUMMARY = "generated recipe based on perl-Test-Pod-Coverage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Pod-Coverage = "perl-Pod-Coverage perl-Test-Simple perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Pod-Coverage-1.10-10.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Pod-Coverage.sha256sum] = "da62c9a87e54d20cc0c20839aafbe7e455f93c54942013cb700e88a63370f4fe"
