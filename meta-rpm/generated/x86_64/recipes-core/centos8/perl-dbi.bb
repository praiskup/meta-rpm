SUMMARY = "generated recipe based on perl-DBI srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-DBI = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-DBI = "glibc perl-Carp perl-Data-Dumper perl-Errno perl-Exporter perl-Getopt-Long perl-IO perl-Math-BigInt perl-PathTools perl-Scalar-List-Utils perl-Storable perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-DBI-1.641-1.el8.x86_64.rpm \
          "

SRC_URI[perl-DBI.sha256sum] = "bcaaf78801a163160dfbc8a1b12b4aefde57d154734a3c54749628294ad63c5b"
