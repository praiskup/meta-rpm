SUMMARY = "generated recipe based on libgovirt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libsoup-2.4 libxml2 pkgconfig-native rest"
RPM_SONAME_PROV_libgovirt = "libgovirt.so.2"
RPM_SONAME_REQ_libgovirt = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 librest-0.7.so.0 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_libgovirt = "glib2 glibc libsoup libxml2 rest"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgovirt-0.3.4-12.el8_2.x86_64.rpm \
          "

SRC_URI[libgovirt.sha256sum] = "a285c7b151d44a01507275853534f143ea1739f1a13435aa84e39918ad6db62e"
