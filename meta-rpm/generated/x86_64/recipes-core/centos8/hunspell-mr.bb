SUMMARY = "generated recipe based on hunspell-mr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mr = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mr-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-mr.sha256sum] = "48ca735bd4dca162a652b3db653e04576eaa32551e0a8e8890e03517173f5395"
