SUMMARY = "generated recipe based on yasm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_yasm = "libc.so.6"
RDEPENDS_yasm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/yasm-1.3.0-7.el8.x86_64.rpm \
          "

SRC_URI[yasm.sha256sum] = "fc3420f64828319a8b7872400396454c2ed64ff5b3086ae0b47e68be5e314d97"
