SUMMARY = "generated recipe based on libgudev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libgudev = "libgudev-1.0.so.0"
RPM_SONAME_REQ_libgudev = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libudev.so.1"
RDEPENDS_libgudev = "glib2 glibc systemd-libs"
RPM_SONAME_REQ_libgudev-devel = "libgudev-1.0.so.0"
RPROVIDES_libgudev-devel = "libgudev-dev (= 232)"
RDEPENDS_libgudev-devel = "glib2-devel libgudev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgudev-232-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgudev-devel-232-4.el8.x86_64.rpm \
          "

SRC_URI[libgudev.sha256sum] = "cb6b773c2ac78142736abd8a8f9eaac122f4647aefd3f003a615baf79abbefbb"
SRC_URI[libgudev-devel.sha256sum] = "3d81897a596863f3ada57bacf2033b3047fe6e397c221bdc2dd9fd913a053df6"
