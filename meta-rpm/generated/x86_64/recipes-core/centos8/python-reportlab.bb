SUMMARY = "generated recipe based on python-reportlab srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-reportlab = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-reportlab = "dejavu-sans-fonts glibc platform-python python3-libs python3-pillow"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-reportlab-3.4.0-8.el8.x86_64.rpm \
          "

SRC_URI[python3-reportlab.sha256sum] = "842e652e064cab2b9b038615f6da490827ad8ceeaff9ba088e976079b27c1983"
