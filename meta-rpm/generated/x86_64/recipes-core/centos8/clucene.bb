SUMMARY = "generated recipe based on clucene srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_clucene-contribs-lib = "libclucene-contribs-lib.so.1"
RPM_SONAME_REQ_clucene-contribs-lib = "libc.so.6 libclucene-core.so.1 libclucene-shared.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_clucene-contribs-lib = "clucene-core glibc libgcc libstdc++ zlib"
RPM_SONAME_PROV_clucene-core = "libclucene-core.so.1 libclucene-shared.so.1"
RPM_SONAME_REQ_clucene-core = "libc.so.6 libclucene-shared.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_clucene-core = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_clucene-core-devel = "libclucene-contribs-lib.so.1 libclucene-core.so.1 libclucene-shared.so.1"
RPROVIDES_clucene-core-devel = "clucene-core-dev (= 2.3.3.4)"
RDEPENDS_clucene-core-devel = "clucene-contribs-lib clucene-core pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clucene-contribs-lib-2.3.3.4-31.20130812.e8e3d20git.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clucene-core-2.3.3.4-31.20130812.e8e3d20git.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/clucene-core-devel-2.3.3.4-31.20130812.e8e3d20git.el8.x86_64.rpm \
          "

SRC_URI[clucene-contribs-lib.sha256sum] = "330e0f3311eb33e61385dc1ee6f4514711f72361fd2721ef3bbdbf2e927679ad"
SRC_URI[clucene-core.sha256sum] = "4abccbb242bdaa670ff4613ba8ff38ff3681dee7455417b99ba8a7fe0f996efd"
SRC_URI[clucene-core-devel.sha256sum] = "571d6d973fcc120b0e2a71bc7a5af09ba6a42e4cb2297a7a0490dff18993f94c"
