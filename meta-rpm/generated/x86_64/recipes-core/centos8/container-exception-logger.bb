SUMMARY = "generated recipe based on container-exception-logger srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_container-exception-logger = "libc.so.6"
RDEPENDS_container-exception-logger = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/container-exception-logger-1.0.2-3.el8.x86_64.rpm \
          "

SRC_URI[container-exception-logger.sha256sum] = "2e93a99c8cad4d0bd0ff0d5881445d71a60ea003c7995d25c176ae6232157ae7"
