SUMMARY = "generated recipe based on python-httplib2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-httplib2 = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-httplib2-0.10.3-4.el8.noarch.rpm \
          "

SRC_URI[python3-httplib2.sha256sum] = "657be8a5bcb7be85bccb88732bc118bbc616ffa34b6587bdb3708ce1b943cbf3"
