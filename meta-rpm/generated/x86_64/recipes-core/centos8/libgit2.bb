SUMMARY = "generated recipe based on libgit2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl http-parser openssl pkgconfig-native zlib"
RPM_SONAME_PROV_libgit2 = "libgit2.so.26"
RPM_SONAME_REQ_libgit2 = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libhttp_parser.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1 libz.so.1"
RDEPENDS_libgit2 = "glibc http-parser libcurl openssl-libs zlib"
RPM_SONAME_REQ_libgit2-devel = "libgit2.so.26"
RPROVIDES_libgit2-devel = "libgit2-dev (= 0.26.8)"
RDEPENDS_libgit2-devel = "libgit2 openssl-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgit2-0.26.8-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgit2-devel-0.26.8-1.el8.x86_64.rpm \
          "

SRC_URI[libgit2.sha256sum] = "fd28e567158034290cf1bbbb26f76ebc43f0464c97c62017910d2345f9c44610"
SRC_URI[libgit2-devel.sha256sum] = "70fad72836d8325f610519fffcb5a79e77feac9145ea1be5050923af928670cf"
