SUMMARY = "generated recipe based on ksh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ksh = "libc.so.6 libdl.so.2 libm.so.6 libutil.so.1"
RDEPENDS_ksh = "bash chkconfig coreutils diffutils glibc grep sed systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ksh-20120801-254.el8.x86_64.rpm \
          "

SRC_URI[ksh.sha256sum] = "495d37fea6b6b4b816a0d2856824eb2dd8e6af22ae488a0fcdc692ea37227635"
