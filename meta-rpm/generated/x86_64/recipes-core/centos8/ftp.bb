SUMMARY = "generated recipe based on ftp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_ftp = "libc.so.6 libncurses.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_ftp = "glibc ncurses-libs readline"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ftp-0.17-78.el8.x86_64.rpm \
          "

SRC_URI[ftp.sha256sum] = "1fdbc9c12778c7e17b7ce9b65a47687a8dd14fccf432ea8f2287390e4d1c6a34"
