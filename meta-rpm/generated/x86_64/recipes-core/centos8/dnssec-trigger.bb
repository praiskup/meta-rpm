SUMMARY = "generated recipe based on dnssec-trigger srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 ldns openssl pango pkgconfig-native"
RPM_SONAME_REQ_dnssec-trigger = "libc.so.6 libcrypto.so.1.1 libldns.so.2 libpthread.so.0 libssl.so.1.1"
RDEPENDS_dnssec-trigger = "NetworkManager NetworkManager-libnm bash e2fsprogs glibc ldns openssl openssl-libs platform-python systemd unbound"
RPM_SONAME_REQ_dnssec-trigger-panel = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcrypto.so.1.1 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libldns.so.2 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libssl.so.1.1"
RDEPENDS_dnssec-trigger-panel = "atk cairo dnssec-trigger fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 ldns openssl-libs pango xdg-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dnssec-trigger-0.15-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dnssec-trigger-panel-0.15-4.el8.x86_64.rpm \
          "

SRC_URI[dnssec-trigger.sha256sum] = "c620b45fe0bfbec9dd5704f2ab80a5192535b4b41aa7ebd878908e67626987a1"
SRC_URI[dnssec-trigger-panel.sha256sum] = "e40394ff2df1de061dc8fdd21d5b4e5eafc0e7f45c9faa8bfeeea8957d8660b6"
