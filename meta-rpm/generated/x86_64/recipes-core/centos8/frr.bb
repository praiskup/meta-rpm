SUMMARY = "generated recipe based on frr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "c-ares json-c libcap libgcc libxcrypt libyang net-snmp openssl pkgconfig-native readline rpm systemd-libs"
RPM_SONAME_PROV_frr = "libfrr.so.0 libfrrsnmp.so.0"
RPM_SONAME_REQ_frr = "libc.so.6 libcap.so.2 libcares.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfrr.so.0 libfrrsnmp.so.0 libgcc_s.so.1 libjson-c.so.4 libm.so.6 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 librt.so.1 libsystemd.so.0 libyang.so.0.16"
RDEPENDS_frr = "bash c-ares glibc info json-c libcap libgcc libxcrypt libyang ncurses net-snmp net-snmp-agent-libs net-snmp-libs openssl-libs platform-python readline rpm-libs systemd systemd-libs"
RDEPENDS_frr-contrib = "frr"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/frr-7.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/frr-contrib-7.0-5.el8.x86_64.rpm \
          "

SRC_URI[frr.sha256sum] = "df1ce818df30520e94debb703f0529dfc6cea0fb589e91628f39afb5f30f7f04"
SRC_URI[frr-contrib.sha256sum] = "49f2947afd27077269b3d18087810a718c2284b64a12c555df162bf228cb242c"
