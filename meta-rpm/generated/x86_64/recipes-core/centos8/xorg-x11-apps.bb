SUMMARY = "generated recipe based on xorg-x11-apps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libfontenc libpng libsm libx11 libxaw libxcursor libxext libxft libxkbfile libxmu libxrender libxt libxxf86vm pkgconfig-native zlib"
RPM_SONAME_REQ_xorg-x11-apps = "libSM.so.6 libX11.so.6 libXaw.so.7 libXcursor.so.1 libXext.so.6 libXft.so.2 libXmu.so.6 libXmuu.so.1 libXrender.so.1 libXt.so.6 libXxf86vm.so.1 libc.so.6 libfontconfig.so.1 libfontenc.so.1 libfreetype.so.6 libm.so.6 libpng16.so.16 libutil.so.1 libxkbfile.so.1 libz.so.1"
RDEPENDS_xorg-x11-apps = "bash fontconfig freetype glibc libSM libX11 libXaw libXcursor libXext libXft libXmu libXrender libXt libXxf86vm libfontenc libpng libxkbfile xorg-x11-fonts-misc xorg-x11-xbitmaps zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xorg-x11-apps-7.7-21.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-apps.sha256sum] = "8f799cd2453e0737e4cf1027d237c5673df4f2b7bb75b2246bcbedf966f77f99"
