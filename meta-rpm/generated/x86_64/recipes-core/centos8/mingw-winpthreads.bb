SUMMARY = "generated recipe based on mingw-winpthreads srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-winpthreads = "mingw32-crt mingw32-filesystem"
RDEPENDS_mingw32-winpthreads-static = "mingw32-winpthreads"
RDEPENDS_mingw64-winpthreads = "mingw64-crt mingw64-filesystem"
RDEPENDS_mingw64-winpthreads-static = "mingw64-winpthreads"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-winpthreads-5.0.2-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-winpthreads-static-5.0.2-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-winpthreads-5.0.2-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-winpthreads-static-5.0.2-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-winpthreads.sha256sum] = "0ea974b55c4ee01c619f3544d6534f020814fecb53435169eff05302040a5235"
SRC_URI[mingw32-winpthreads-static.sha256sum] = "4159e710c760c91793a50be0c5d8beaddfbe25f6dd47d974822c0df51730a0b8"
SRC_URI[mingw64-winpthreads.sha256sum] = "381318a03afcfd1ca6b6e699c085de5896c17742a7ffd880e6515a51e292e8fd"
SRC_URI[mingw64-winpthreads-static.sha256sum] = "537bf86a5d46b8e6afd13abc5da6e04ccf7fbb11266d593653accf41eb4887a8"
