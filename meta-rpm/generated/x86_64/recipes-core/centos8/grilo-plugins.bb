SUMMARY = "generated recipe based on grilo-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs glib-2.0 gnome-online-accounts gom grilo gstreamer1.0 json-glib libarchive libdmapsharing libgdata libmediaart liboauth libsoup-2.4 libxml2 lua pkgconfig-native sqlite3 totem-pl-parser tracker"
RPM_SONAME_PROV_grilo-plugins = "libgrlbookmarks.so libgrlchromaprint.so libgrldaap.so libgrldleyna.so libgrldpap.so libgrlfilesystem.so libgrlflickr.so libgrlfreebox.so libgrlgravatar.so libgrljamendo.so libgrllocalmetadata.so libgrlluafactory.so libgrlmagnatune.so libgrlmetadatastore.so libgrlopensubtitles.so libgrlopticalmedia.so libgrlpodcasts.so libgrlraitv.so libgrlthetvdb.so libgrltmdb.so libgrltracker.so libgrlvimeo.so libgrlyoutube.so"
RPM_SONAME_REQ_grilo-plugins = "libarchive.so.13 libavahi-client.so.3 libavahi-glib.so.1 libc.so.6 libdmapsharing-3.0.so.2 libgdata.so.22 libgio-2.0.so.0 libglib-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgom-1.0.so.0 libgrilo-0.3.so.0 libgrlnet-0.3.so.0 libgrlpls-0.3.so.0 libgstreamer-1.0.so.0 libjson-glib-1.0.so.0 liblua-5.3.so libmediaart-2.0.so.0 liboauth.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libtotem-plparser-mini.so.18 libtotem-plparser.so.18 libtracker-sparql-2.0.so.0 libxml2.so.2"
RDEPENDS_grilo-plugins = "avahi-glib avahi-libs dleyna-server glib2 glibc gnome-online-accounts gom grilo gstreamer1 json-glib libarchive libdmapsharing libgdata libmediaart liboauth libsoup libxml2 lua-libs pkgconf-pkg-config sqlite-libs totem-pl-parser tracker"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grilo-plugins-0.3.8-1.el8.x86_64.rpm \
          "

SRC_URI[grilo-plugins.sha256sum] = "0887fefd52beee1e1626ed7fa37f5f1da2b80b77f3228cf5dee7dc4c13a89c18"
