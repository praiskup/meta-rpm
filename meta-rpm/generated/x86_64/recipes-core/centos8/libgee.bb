SUMMARY = "generated recipe based on libgee srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libgee = "libgee-0.8.so.2"
RPM_SONAME_REQ_libgee = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libgee = "glib2 glibc"
RPM_SONAME_REQ_libgee-devel = "libgee-0.8.so.2"
RPROVIDES_libgee-devel = "libgee-dev (= 0.20.1)"
RDEPENDS_libgee-devel = "glib2-devel libgee pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgee-0.20.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgee-devel-0.20.1-1.el8.x86_64.rpm \
          "

SRC_URI[libgee.sha256sum] = "73d5826c0668e998e72d4d6d6fec115ab2e7f536d045c3748f65b82748909440"
SRC_URI[libgee-devel.sha256sum] = "f4a16fb3263dc3123ab3a2dc5a9c27f055fbed253c75ae440a4f8c743f4e6c26"
