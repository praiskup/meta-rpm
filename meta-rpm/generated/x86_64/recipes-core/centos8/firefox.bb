SUMMARY = "generated recipe based on firefox srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-glib dbus-libs fontconfig freetype gdk-pixbuf glib-2.0 gtk+3 gtk2 libffi libgcc libx11 libxcb libxcomposite libxcursor libxdamage libxext libxfixes libxi libxrender libxt nspr nss pango pipewire pkgconfig-native zlib"
RPM_SONAME_PROV_firefox = "libclearkey.so liblgpllibs.so libmozavcodec.so libmozavutil.so libmozgtk.so libmozsandbox.so libmozsqlite3.so libmozwayland.so libxul.so"
RPM_SONAME_REQ_firefox = "ld-linux-x86-64.so.2 libX11-xcb.so.1 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrender.so.1 libXt.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libffi.so.6 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgtk-x11-2.0.so.0 liblgpllibs.so libm.so.6 libmozavutil.so libmozgtk.so libmozsandbox.so libmozsqlite3.so libmozwayland.so libnspr4.so libnss3.so libnssutil3.so libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpipewire-0.2.so.1 libplc4.so libplds4.so libpthread.so.0 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6 libxcb-shm.so.0 libxcb.so.1 libxul.so libz.so.1"
RDEPENDS_firefox = "atk bash cairo cairo-gobject centos-indexhtml dbus-glib dbus-libs fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 gtk3 libX11 libX11-xcb libXcomposite libXcursor libXdamage libXext libXfixes libXi libXrender libXt liberation-fonts-common liberation-sans-fonts libffi libgcc libstdc++ libxcb mozilla-filesystem nspr nss nss-util p11-kit-trust pango pipewire-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/firefox-78.4.0-1.el8_2.x86_64.rpm \
          "

SRC_URI[firefox.sha256sum] = "7e7216eb2350a536ea902944073cc89eaf20f1a5e3f0328491b6312a9c02cdf3"
