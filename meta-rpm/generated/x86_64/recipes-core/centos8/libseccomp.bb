SUMMARY = "generated recipe based on libseccomp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libseccomp = "libseccomp.so.2"
RPM_SONAME_REQ_libseccomp = "libc.so.6"
RDEPENDS_libseccomp = "glibc"
RPM_SONAME_REQ_libseccomp-devel = "libc.so.6 libseccomp.so.2"
RPROVIDES_libseccomp-devel = "libseccomp-dev (= 2.4.1)"
RDEPENDS_libseccomp-devel = "glibc libseccomp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libseccomp-devel-2.4.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libseccomp-2.4.1-1.el8.x86_64.rpm \
          "

SRC_URI[libseccomp.sha256sum] = "0596152afee19368fb13122e4a9869ea7600a566cf2b07f0427e67d1c1052099"
SRC_URI[libseccomp-devel.sha256sum] = "09a95c6e0f9256f2666fbc354ec5e5e6296b91688ab4bbb212b3f78ae53adc2e"
