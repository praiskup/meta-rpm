SUMMARY = "generated recipe based on libXvMC srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxv pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXvMC = "libXvMC.so.1 libXvMCW.so.1"
RPM_SONAME_REQ_libXvMC = "libX11.so.6 libXext.so.6 libXv.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libXvMC = "glibc libX11 libXext libXv"
RPM_SONAME_REQ_libXvMC-devel = "libXvMC.so.1 libXvMCW.so.1"
RPROVIDES_libXvMC-devel = "libXvMC-dev (= 1.0.10)"
RDEPENDS_libXvMC-devel = "libX11-devel libXext-devel libXv-devel libXvMC pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXvMC-1.0.10-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libXvMC-devel-1.0.10-7.el8.x86_64.rpm \
          "

SRC_URI[libXvMC.sha256sum] = "6b8b36dcbb17de20107d7e01ab5e5f0f2d0f12828b46b770d494c207a2bbd37b"
SRC_URI[libXvMC-devel.sha256sum] = "0b445be827b182cfd0e1d007eae9313cc7b3228c87bdec0b534ae2e15a6eb4a2"
