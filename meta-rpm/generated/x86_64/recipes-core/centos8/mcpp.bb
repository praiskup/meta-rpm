SUMMARY = "generated recipe based on mcpp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmcpp = "libmcpp.so.0"
RPM_SONAME_REQ_libmcpp = "libc.so.6"
RDEPENDS_libmcpp = "glibc"
RPM_SONAME_REQ_mcpp = "libc.so.6 libmcpp.so.0"
RDEPENDS_mcpp = "glibc libmcpp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmcpp-2.7.2-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mcpp-2.7.2-20.el8.x86_64.rpm \
          "

SRC_URI[libmcpp.sha256sum] = "5dfc0a48ede553a0f66860d714ca5194b281e13e0fde4ad086687c2620dcfdb0"
SRC_URI[mcpp.sha256sum] = "b40668a9a68b0956f753324aaae48e35dab2e2aee265086ad9a745a5be977d0b"
