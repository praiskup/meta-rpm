SUMMARY = "generated recipe based on malaga-suomi-voikko srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/malaga-suomi-voikko-1.19-5.el8.x86_64.rpm \
          "

SRC_URI[malaga-suomi-voikko.sha256sum] = "d156104b98aa03c87637fe89b3beb80b7c259a72964800e0f11944d023da81cb"
