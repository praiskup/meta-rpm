SUMMARY = "generated recipe based on mingw-icu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-icu = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-gcc-c++ mingw32-pkg-config"
RDEPENDS_mingw64-icu = "mingw64-crt mingw64-filesystem mingw64-gcc mingw64-gcc-c++ mingw64-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-icu-57.1-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-icu-57.1-5.el8.noarch.rpm \
          "

SRC_URI[mingw32-icu.sha256sum] = "a2b96f5c60db94480264e43c4205806fa846fbaffba001f907e51ec5e192535a"
SRC_URI[mingw64-icu.sha256sum] = "c2b62e09e19f473c4dbba03f7d4c60244ae83d73e76f426c005496004698f53c"
