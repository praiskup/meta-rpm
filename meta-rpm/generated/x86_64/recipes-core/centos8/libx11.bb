SUMMARY = "generated recipe based on libX11 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/libx11"
DEPENDS = "libxcb pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libX11 = "libX11.so.6"
RPM_SONAME_REQ_libX11 = "libc.so.6 libdl.so.2 libxcb.so.1"
RPROVIDES_libX11 = "libx11 (= 1.6.8)"
RDEPENDS_libX11 = "glibc libX11-common libxcb"
RPROVIDES_libX11-common = "libx11-locale (= 1.6.8)"
RPM_SONAME_REQ_libX11-devel = "libX11-xcb.so.1 libX11.so.6"
RPROVIDES_libX11-devel = "libX11-dev (= 1.6.8) libx11-dev (= 1.6.8)"
RDEPENDS_libX11-devel = "libX11 libX11-xcb libxcb-devel pkgconf-pkg-config xorg-x11-proto-devel"
RPM_SONAME_PROV_libX11-xcb = "libX11-xcb.so.1"
RPM_SONAME_REQ_libX11-xcb = "libc.so.6 libdl.so.2"
RDEPENDS_libX11-xcb = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libX11-1.6.8-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libX11-common-1.6.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libX11-devel-1.6.8-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libX11-xcb-1.6.8-3.el8.x86_64.rpm \
          "

SRC_URI[libX11.sha256sum] = "0243d8e1b78c1c059dffd5abb4c3e8830ee71e22897fee8663da427ad0473d17"
SRC_URI[libX11-common.sha256sum] = "d002c253767fe455571cd55f871d7bd9f6fc0742c0a1673f19946f30b413bb4e"
SRC_URI[libX11-devel.sha256sum] = "517f367199554849fcf9c308c4c5f3d791842d9b8815d1626b695faaee1e10b5"
SRC_URI[libX11-xcb.sha256sum] = "56645a8799750d0988c145ba5560673f4d2b0e371932dccf03cdae8d0fc4942e"
