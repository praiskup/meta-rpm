SUMMARY = "generated recipe based on highlight srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd lua pkgconfig-native qt5-qtbase"
RPM_SONAME_REQ_highlight = "libc.so.6 libdl.so.2 libgcc_s.so.1 liblua-5.3.so libm.so.6 libstdc++.so.6"
RDEPENDS_highlight = "glibc libgcc libstdc++ lua-libs"
RPM_SONAME_REQ_highlight-gui = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libc.so.6 libdl.so.2 libgcc_s.so.1 liblua-5.3.so libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_highlight-gui = "glibc highlight libgcc libglvnd-glx libstdc++ lua-libs qt5-qtbase qt5-qtbase-gui"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/highlight-3.42-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/highlight-gui-3.42-3.el8.x86_64.rpm \
          "

SRC_URI[highlight.sha256sum] = "270927d2f6012661c07a01e20404336af7f3a0ec067b33800a9d122b741fb814"
SRC_URI[highlight-gui.sha256sum] = "ec5c857b5eeda9e7a14e5788d63b138b041f344dc05f0c28eefeaee8fffe365b"
