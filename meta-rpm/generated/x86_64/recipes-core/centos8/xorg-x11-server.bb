SUMMARY = "generated recipe based on xorg-x11-server srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/xserver-xf86"
DEPENDS = "audit dbus-libs libdmx libdrm libepoxy libglvnd libpciaccess libselinux libx11 libxau libxaw libxcb libxdmcp libxext libxfixes libxfont2 libxi libxmu libxpm libxrender libxshmfence libxt mesa openssl pixman pkgconfig-native systemd-libs wayland xcb-util xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm xorg-x11-proto-devel"
RPM_SONAME_REQ_xorg-x11-server-Xdmx = "libX11.so.6 libXau.so.6 libXaw.so.7 libXdmcp.so.6 libXext.so.6 libXfixes.so.3 libXfont2.so.2 libXi.so.6 libXmu.so.6 libXmuu.so.1 libXpm.so.4 libXrender.so.1 libXt.so.6 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdmx.so.1 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xdmx = "audit-libs glibc libX11 libXau libXaw libXdmcp libXext libXfixes libXfont2 libXi libXmu libXpm libXrender libXt libdmx libselinux libxshmfence openssl-libs pixman systemd-libs xorg-x11-server-common"
RPM_SONAME_REQ_xorg-x11-server-Xephyr = "libGL.so.1 libX11-xcb.so.1 libX11.so.6 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libdrm.so.2 libepoxy.so.0 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libudev.so.1 libxcb-glx.so.0 libxcb-icccm.so.4 libxcb-image.so.0 libxcb-keysyms.so.1 libxcb-randr.so.0 libxcb-render-util.so.0 libxcb-render.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-util.so.1 libxcb-xf86dri.so.0 libxcb-xkb.so.1 libxcb-xv.so.0 libxcb.so.1 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xephyr = "audit-libs dbus-libs glibc libX11 libX11-xcb libXau libXdmcp libXfont2 libdrm libepoxy libglvnd-glx libselinux libxcb libxshmfence openssl-libs pixman systemd-libs xcb-util xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm xorg-x11-server-common"
RPM_SONAME_REQ_xorg-x11-server-Xnest = "libGL.so.1 libX11.so.6 libXau.so.6 libXdmcp.so.6 libXext.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xnest = "audit-libs glibc libX11 libXau libXdmcp libXext libXfont2 libglvnd-glx libselinux libxshmfence openssl-libs pixman systemd-libs xorg-x11-server-common"
RPM_SONAME_PROV_xorg-x11-server-Xorg = "libexa.so libfb.so libfbdevhw.so libglamoregl.so libglx.so libint10.so libshadow.so libshadowfb.so libvbe.so libvgahw.so libwfb.so"
RPM_SONAME_REQ_xorg-x11-server-Xorg = "libGL.so.1 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libdrm.so.2 libepoxy.so.0 libgbm.so.1 libm.so.6 libpciaccess.so.0 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libudev.so.1 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xorg = "audit-libs bash dbus-libs glibc libXau libXdmcp libXfont2 libdrm libepoxy libglvnd-egl libglvnd-glx libpciaccess libselinux libxshmfence mesa-libgbm openssl-libs pixman systemd systemd-libs xorg-x11-drv-fbdev xorg-x11-drv-libinput xorg-x11-drv-vesa xorg-x11-server-common"
RPM_SONAME_REQ_xorg-x11-server-Xvfb = "libGL.so.1 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xvfb = "audit-libs bash glibc libXau libXdmcp libXfont2 libglvnd-glx libselinux libxshmfence openssl-libs pixman systemd-libs xorg-x11-server-common xorg-x11-xauth"
RPM_SONAME_REQ_xorg-x11-server-Xwayland = "libEGL.so.1 libGL.so.1 libXau.so.6 libXdmcp.so.6 libXfont2.so.2 libaudit.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libdrm.so.2 libepoxy.so.0 libgbm.so.1 libm.so.6 libpixman-1.so.0 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libwayland-client.so.0 libxshmfence.so.1"
RDEPENDS_xorg-x11-server-Xwayland = "audit-libs glibc libXau libXdmcp libXfont2 libdrm libepoxy libglvnd-egl libglvnd-glx libselinux libwayland-client libxshmfence mesa-libgbm openssl-libs pixman systemd-libs xorg-x11-server-common"
RDEPENDS_xorg-x11-server-common = "pixman xkeyboard-config xorg-x11-xkb-utils"
RPROVIDES_xorg-x11-server-devel = "xorg-x11-server-dev (= 1.20.6)"
RDEPENDS_xorg-x11-server-devel = "bash libXfont2-devel libpciaccess-devel mesa-libGL-devel pixman-devel pkgconf-pkg-config xorg-x11-proto-devel xorg-x11-util-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xdmx-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xephyr-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xnest-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xorg-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xvfb-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xwayland-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-common-1.20.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xorg-x11-server-devel-1.20.6-3.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-server-Xdmx.sha256sum] = "664cac936eb548b4eaa40aeb25f52a6bc6b65394eb3f4f56519c1db7c1c5f576"
SRC_URI[xorg-x11-server-Xephyr.sha256sum] = "3018b7c97fb32f154043bffde9d51660acfb1d966aaefade1734ac903d0394ae"
SRC_URI[xorg-x11-server-Xnest.sha256sum] = "37a3487aa9ab6f296fed6ad0d95b5c221525b07074f3f8252ed240b473bc8742"
SRC_URI[xorg-x11-server-Xorg.sha256sum] = "4296213f26d2e7a9b876976a062aaacd6ac30718d8b003d5445ab94804596373"
SRC_URI[xorg-x11-server-Xvfb.sha256sum] = "df79f71810012ae61cc17fba87b75ba686e782d3541877b52a2b984a1d0c4999"
SRC_URI[xorg-x11-server-Xwayland.sha256sum] = "e27882675b93e426e49a34e5b2a004d7e2cf0484109644c60733d4f44c6094b8"
SRC_URI[xorg-x11-server-common.sha256sum] = "bd9b4a1350b32ee9848fb54642cedbb6b61f85c63699642d40bc94a2a8402d81"
SRC_URI[xorg-x11-server-devel.sha256sum] = "4e65e949d82f03b7b0b6f1cc7e47acfc7be9963a90fc4ba9879b0b0da478a4d6"
