SUMMARY = "generated recipe based on rest srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libsoup-2.4 libxml2 pkgconfig-native"
RPM_SONAME_PROV_rest = "librest-0.7.so.0 librest-extras-0.7.so.0"
RPM_SONAME_REQ_rest = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 librest-0.7.so.0 libsoup-2.4.so.1 libsoup-gnome-2.4.so.1 libxml2.so.2"
RDEPENDS_rest = "glib2 glibc libsoup libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rest-0.8.1-2.el8.x86_64.rpm \
          "

SRC_URI[rest.sha256sum] = "000221caf7172bad19efd8f8bca6d8b0cdf3174cc4601e07916e5e58caca7df9"
