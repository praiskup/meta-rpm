SUMMARY = "generated recipe based on popt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_popt = "libpopt.so.0"
RPM_SONAME_REQ_popt = "libc.so.6"
RDEPENDS_popt = "glibc"
RPM_SONAME_REQ_popt-devel = "libpopt.so.0"
RPROVIDES_popt-devel = "popt-dev (= 1.16)"
RDEPENDS_popt-devel = "pkgconf-pkg-config popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/popt-1.16-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/popt-devel-1.16-14.el8.x86_64.rpm \
          "

SRC_URI[popt.sha256sum] = "2f0cdb6874b7b8c1f86749ffc7f0fe90038dacd3859adea611fc685e234f992e"
SRC_URI[popt-devel.sha256sum] = "a5fae8be60522e4aeca84b1f54e42010fac67bd08f5b0c92d63c25c57113bdd0"
