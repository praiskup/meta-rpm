SUMMARY = "generated recipe based on taglib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_taglib = "libtag.so.1 libtag_c.so.0"
RPM_SONAME_REQ_taglib = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libtag.so.1 libz.so.1"
RDEPENDS_taglib = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_taglib-devel = "libtag.so.1 libtag_c.so.0"
RPROVIDES_taglib-devel = "taglib-dev (= 1.11.1)"
RDEPENDS_taglib-devel = "bash pkgconf-pkg-config taglib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/taglib-1.11.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/taglib-devel-1.11.1-8.el8.x86_64.rpm \
          "

SRC_URI[taglib.sha256sum] = "45e8da2e935acb2431f6def40bc81e8164b2c6f70b48d1f078e7e732e53b6829"
SRC_URI[taglib-devel.sha256sum] = "c79ddc3f2e647a298f88272662f4297c10b2f001f0e73137d9b8a5cb3b30dd16"
