SUMMARY = "generated recipe based on compat-libgfortran-48 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_compat-libgfortran-48 = "libgfortran.so.3"
RPM_SONAME_REQ_compat-libgfortran-48 = "libc.so.6 libgcc_s.so.1 libm.so.6 libquadmath.so.0"
RDEPENDS_compat-libgfortran-48 = "binutils glibc glibc-devel libgcc libquadmath"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/compat-libgfortran-48-4.8.5-36.1.el8.x86_64.rpm \
          "

SRC_URI[compat-libgfortran-48.sha256sum] = "7b400e2d116d50af9fb978d0e0a59b69010470041c969f41965e4fbd3d8a7c22"
