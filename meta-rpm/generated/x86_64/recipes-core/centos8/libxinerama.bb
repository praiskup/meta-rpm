SUMMARY = "generated recipe based on libXinerama srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xinerama"
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXinerama = "libXinerama.so.1"
RPM_SONAME_REQ_libXinerama = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXinerama = "glibc libX11 libXext"
RPM_SONAME_REQ_libXinerama-devel = "libXinerama.so.1"
RPROVIDES_libXinerama-devel = "libXinerama-dev (= 1.1.4)"
RDEPENDS_libXinerama-devel = "libX11-devel libXext-devel libXinerama pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXinerama-1.1.4-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXinerama-devel-1.1.4-1.el8.x86_64.rpm \
          "

SRC_URI[libXinerama.sha256sum] = "f07f41680f3d3a29981415c5c6d83e40da2958abbadc2417be6fbc467259be9b"
SRC_URI[libXinerama-devel.sha256sum] = "d36f22e23d8cc152a50cf607c2d2ef0efb46422921746e52fbdffa24e6902269"
