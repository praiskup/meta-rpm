SUMMARY = "generated recipe based on libutempter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libutempter = "libutempter.so.0"
RPM_SONAME_REQ_libutempter = "libc.so.6"
RDEPENDS_libutempter = "bash glibc shadow-utils"
RPM_SONAME_REQ_libutempter-devel = "libutempter.so.0"
RPROVIDES_libutempter-devel = "libutempter-dev (= 1.1.6)"
RDEPENDS_libutempter-devel = "libutempter"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libutempter-1.1.6-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libutempter-devel-1.1.6-14.el8.x86_64.rpm \
          "

SRC_URI[libutempter.sha256sum] = "c8c54c56bff9ca416c3ba6bccac483fb66c81a53d93a19420088715018ed5169"
SRC_URI[libutempter-devel.sha256sum] = "798442abd98f97edf7af6036fd68273467db5ff336cff067eaaf78672d445099"
