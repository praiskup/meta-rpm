SUMMARY = "generated recipe based on jose srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jansson libgcc openssl pkgconfig-native zlib"
RPM_SONAME_REQ_jose = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libjansson.so.4 libjose.so.0 libpthread.so.0 libz.so.1"
RDEPENDS_jose = "glibc jansson libgcc libjose openssl-libs zlib"
RPM_SONAME_PROV_libjose = "libjose.so.0"
RPM_SONAME_REQ_libjose = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libjansson.so.4 libpthread.so.0 libz.so.1"
RDEPENDS_libjose = "glibc jansson libgcc openssl-libs zlib"
RPM_SONAME_REQ_libjose-devel = "libjose.so.0"
RPROVIDES_libjose-devel = "libjose-dev (= 10)"
RDEPENDS_libjose-devel = "jansson-devel libjose openssl-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jose-10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libjose-10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libjose-devel-10-2.el8.x86_64.rpm \
          "

SRC_URI[jose.sha256sum] = "3fe3333b7effc712958de6bda1fe92042c7c81e5c8f325d0d84cea41067e0cbd"
SRC_URI[libjose.sha256sum] = "60cdae13504dbb8ad77f4dec925d22020c8796d231d326e5b23ed717e4a70669"
SRC_URI[libjose-devel.sha256sum] = "f3601abe7659ac3731ca7ec0d9ead23f35b2e70a382b5f40bd85b50494162c6b"
