SUMMARY = "generated recipe based on libsepol srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsepol = "libsepol.so.1"
RPM_SONAME_REQ_libsepol = "libc.so.6"
RDEPENDS_libsepol = "bash glibc"
RPM_SONAME_REQ_libsepol-devel = "libsepol.so.1"
RPROVIDES_libsepol-devel = "libsepol-dev (= 2.9)"
RDEPENDS_libsepol-devel = "libsepol pkgconf-pkg-config"
RDEPENDS_libsepol-static = "libsepol-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsepol-2.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsepol-devel-2.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsepol-static-2.9-1.el8.x86_64.rpm \
          "

SRC_URI[libsepol.sha256sum] = "8d330c13456fd777dcc4722ee9da640d8e8e3cf6957e85bf4f7ea3a9f178d05c"
SRC_URI[libsepol-devel.sha256sum] = "aa764a96facf2ab1a8f3d5eaee8388555485a6ea8dd7196b96a10fc56f2b3179"
SRC_URI[libsepol-static.sha256sum] = "efc282adab79b9a55a11ec0633df927207fccdbdf0950e8ea8c5218cf7e71491"
