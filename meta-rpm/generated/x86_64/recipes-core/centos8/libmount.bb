SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libblkid libselinux libuuid pkgconfig-native"
RPM_SONAME_PROV_libmount = "libmount.so.1"
RPM_SONAME_REQ_libmount = "ld-linux-x86-64.so.2 libblkid.so.1 libc.so.6 librt.so.1 libselinux.so.1 libuuid.so.1"
RDEPENDS_libmount = "glibc libblkid libselinux libuuid"
RPM_SONAME_REQ_libmount-devel = "libmount.so.1"
RPROVIDES_libmount-devel = "libmount-dev (= 2.32.1)"
RDEPENDS_libmount-devel = "libblkid-devel libmount pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmount-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmount-devel-2.32.1-22.el8.x86_64.rpm \
          "

SRC_URI[libmount.sha256sum] = "14079a26f1000ed28e6c8a513b4bdf0b1f9e0f49518d4fe4379793b70d88c356"
SRC_URI[libmount-devel.sha256sum] = "93409bf4112deb92674c69c9d137c1c46f878a3e55896737a091be4f26d918a3"
