SUMMARY = "generated recipe based on chrpath srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_chrpath = "libc.so.6"
RDEPENDS_chrpath = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/chrpath-0.16-7.el8.x86_64.rpm \
          "

SRC_URI[chrpath.sha256sum] = "fdf6cb013c08092d4b63933b402ace9097d895d360f60c537887a74a7c3bb2d3"
