SUMMARY = "generated recipe based on wayland srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libffi libxml2 pkgconfig-native"
RPM_SONAME_PROV_libwayland-client = "libwayland-client.so.0"
RPM_SONAME_REQ_libwayland-client = "libc.so.6 libffi.so.6 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_libwayland-client = "glibc libffi"
RPM_SONAME_PROV_libwayland-cursor = "libwayland-cursor.so.0"
RPM_SONAME_REQ_libwayland-cursor = "libc.so.6 libffi.so.6 libm.so.6 libpthread.so.0 librt.so.1 libwayland-client.so.0"
RDEPENDS_libwayland-cursor = "glibc libffi libwayland-client"
RPM_SONAME_PROV_libwayland-egl = "libwayland-egl.so.1"
RPM_SONAME_REQ_libwayland-egl = "libc.so.6"
RDEPENDS_libwayland-egl = "glibc"
RPM_SONAME_PROV_libwayland-server = "libwayland-server.so.0"
RPM_SONAME_REQ_libwayland-server = "libc.so.6 libffi.so.6 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_libwayland-server = "glibc libffi"
RPM_SONAME_REQ_wayland-devel = "libc.so.6 libexpat.so.1 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxml2.so.2"
RPROVIDES_wayland-devel = "wayland-dev (= 1.17.0)"
RDEPENDS_wayland-devel = "expat glibc libwayland-client libwayland-cursor libwayland-egl libwayland-server libxml2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwayland-client-1.17.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwayland-cursor-1.17.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwayland-egl-1.17.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwayland-server-1.17.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wayland-devel-1.17.0-1.el8.x86_64.rpm \
          "

SRC_URI[libwayland-client.sha256sum] = "7909040a533d96cb18a827667501fb09eca1dbde51a0565c27f7883651d6ffed"
SRC_URI[libwayland-cursor.sha256sum] = "5d43d2bdee44cd9f6d7f3473cc053646639bafbd9e05138afadc71e5f59079b5"
SRC_URI[libwayland-egl.sha256sum] = "19accb632517ce4ec8f951cf2d41e69cd95f186faadaae9349f94d61fad24cf4"
SRC_URI[libwayland-server.sha256sum] = "75a410fa502894079dc2f1dde6190a78817677f04f6f8abba39e0eaba6e227cc"
SRC_URI[wayland-devel.sha256sum] = "b53a1de93b8a9baf0811a4afb031dabb1de43e161a3927c6f30edd6a6f82d585"
