SUMMARY = "generated recipe based on targetcli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_targetcli = "platform-python python3-configshell python3-dbus python3-gobject-base python3-rtslib python3-six target-restore"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/targetcli-2.1.51-4.el8_2.noarch.rpm \
          "

SRC_URI[targetcli.sha256sum] = "c0a02cbdefb8d519ecb856a375660f20563478454c372ed518a5bee4a5a938ba"
