SUMMARY = "generated recipe based on gutenprint srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo cups-libs e2fsprogs fontconfig freetype gdk-pixbuf gimp glib-2.0 gtk2 krb5-libs libusb1 libxcrypt pango pkgconfig-native zlib"
RPM_SONAME_REQ_gutenprint = "libc.so.6 libdl.so.2 libgutenprint.so.2 libm.so.6"
RDEPENDS_gutenprint = "glibc gutenprint-libs"
RPM_SONAME_REQ_gutenprint-cups = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libdl.so.2 libgssapi_krb5.so.2 libgutenprint.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libusb-1.0.so.0 libz.so.1"
RDEPENDS_gutenprint-cups = "bash cups cups-libs glibc gutenprint gutenprint-libs krb5-libs libcom_err libusbx libxcrypt platform-python zlib"
RPM_SONAME_PROV_gutenprint-libs = "libgutenprint.so.2"
RPM_SONAME_REQ_gutenprint-libs = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_gutenprint-libs = "glibc"
RPM_SONAME_PROV_gutenprint-libs-ui = "libgutenprintui2.so.1"
RPM_SONAME_REQ_gutenprint-libs-ui = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libgutenprint.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gutenprint-libs-ui = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 gutenprint-libs pango"
RPM_SONAME_REQ_gutenprint-plugin = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgimp-2.0.so.0 libgimpbase-2.0.so.0 libgimpcolor-2.0.so.0 libgimpconfig-2.0.so.0 libgimpmath-2.0.so.0 libgimpmodule-2.0.so.0 libgimpui-2.0.so.0 libgimpwidgets-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libgutenprint.so.2 libgutenprintui2.so.1 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gutenprint-plugin = "atk cairo fontconfig freetype gdk-pixbuf2 gimp gimp-libs glib2 glibc gtk2 gutenprint gutenprint-libs gutenprint-libs-ui pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gutenprint-5.2.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gutenprint-cups-5.2.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gutenprint-doc-5.2.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gutenprint-libs-5.2.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gutenprint-libs-ui-5.2.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gutenprint-plugin-5.2.14-3.el8.x86_64.rpm \
          "

SRC_URI[gutenprint.sha256sum] = "0cfce0913358fc622ed85bf56f1a703ef5c06bc46faf3441e742dde253b45c74"
SRC_URI[gutenprint-cups.sha256sum] = "66ba3bf72b1af3f517757b00a22ae2f254f4476d31c016142b9f80b7def88ec0"
SRC_URI[gutenprint-doc.sha256sum] = "6915db1487898cf4f60d20b902771e78ded4820dded994ee3e6cc626865908a2"
SRC_URI[gutenprint-libs.sha256sum] = "85b597fe849a1e40eb78500c418dab6d6bc66b986ca3b6f82eb69852da26656e"
SRC_URI[gutenprint-libs-ui.sha256sum] = "d9c7ff920d694cee39c41d41fdf8d6acc777ebdd0020298f7e97afeac7ad6df5"
SRC_URI[gutenprint-plugin.sha256sum] = "0545f7ef6b4dff7b97fff2723925508b341dff2858a9ad79e7c70e0407ef701e"
