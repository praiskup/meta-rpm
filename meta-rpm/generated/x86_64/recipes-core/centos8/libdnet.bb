SUMMARY = "generated recipe based on libdnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdnet = "libdnet.so.1"
RPM_SONAME_REQ_libdnet = "libc.so.6"
RDEPENDS_libdnet = "glibc"
RPM_SONAME_REQ_libdnet-devel = "libdnet.so.1"
RPROVIDES_libdnet-devel = "libdnet-dev (= 1.12)"
RDEPENDS_libdnet-devel = "bash libdnet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdnet-1.12-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdnet-devel-1.12-26.el8.x86_64.rpm \
          "

SRC_URI[libdnet.sha256sum] = "917964c6ff0a1c1e2d061d7055f4e965b9e8d385fe74e55ef332c65cb933443b"
SRC_URI[libdnet-devel.sha256sum] = "1b557ce4b22d81f655e1eac73f3a8a2d471252bd20d4cfa3282e23ea48372c05"
