SUMMARY = "generated recipe based on vulkan-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libx11 libxcb libxext pkgconfig-native vulkan-loader wayland"
RPM_SONAME_REQ_vulkan-tools = "libX11.so.6 libXext.so.6 libc.so.6 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6 libvulkan.so.1 libwayland-client.so.0 libxcb.so.1"
RDEPENDS_vulkan-tools = "glibc libX11 libXext libgcc libstdc++ libwayland-client libxcb vulkan-loader"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vulkan-tools-1.2.131.1-1.el8.x86_64.rpm \
          "

SRC_URI[vulkan-tools.sha256sum] = "089d11d5660c6a2074accce75306d1ca890dbe3196ae8d1bf457863c71369763"
