SUMMARY = "generated recipe based on openscap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl bzip2 curl dbus-libs gconf glib-2.0 libblkid libcap libgcc libgcrypt libpcre libselinux libxml2 libxslt pkgconfig-native platform-python3 rpm"
RPM_SONAME_PROV_openscap = "libopenscap.so.25"
RPM_SONAME_REQ_openscap = "libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgcc_s.so.1 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libpcre.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap = "GConf2 bash bzip2-libs dbus dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcc libgcrypt libselinux libxml2 libxslt openldap pcre popt procps-ng rpm-libs"
RPM_SONAME_REQ_openscap-devel = "libopenscap.so.25"
RPROVIDES_openscap-devel = "openscap-dev (= 1.3.2)"
RDEPENDS_openscap-devel = "libxml2-devel openscap pkgconf-pkg-config"
RPM_SONAME_PROV_openscap-engine-sce = "libopenscap_sce.so.25"
RPM_SONAME_REQ_openscap-engine-sce = "libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libopenscap.so.25 libpcre.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap-engine-sce = "GConf2 bash bzip2-libs dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcrypt libselinux libxml2 libxslt openscap pcre rpm-libs"
RPM_SONAME_REQ_openscap-engine-sce-devel = "libopenscap_sce.so.25"
RPROVIDES_openscap-engine-sce-devel = "openscap-engine-sce-dev (= 1.3.2)"
RDEPENDS_openscap-engine-sce-devel = "openscap-devel openscap-engine-sce pkgconf-pkg-config"
RPM_SONAME_REQ_openscap-python3 = "libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libopenscap.so.25 libpcre.so.1 libpthread.so.0 libpython3.6m.so.1.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap-python3 = "GConf2 bzip2-libs dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcrypt libselinux libxml2 libxslt openscap pcre platform-python python3-libs rpm-libs"
RPM_SONAME_REQ_openscap-scanner = "libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libopenscap.so.25 libpcre.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap-scanner = "GConf2 bash bzip2-libs dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcrypt libselinux libxml2 libxslt openscap pcre rpm-libs"
RDEPENDS_openscap-utils = "bash openscap openscap-scanner platform-python rpm-build rpmdevtools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openscap-1.3.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openscap-devel-1.3.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openscap-engine-sce-1.3.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openscap-python3-1.3.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openscap-scanner-1.3.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openscap-utils-1.3.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openscap-engine-sce-devel-1.3.2-6.el8.x86_64.rpm \
          "

SRC_URI[openscap.sha256sum] = "a32b993c6c626e62305d9651889f36d897e35c4f06a6b8c60c0452a88e1c660a"
SRC_URI[openscap-devel.sha256sum] = "dd1d0b09fc7e29ba80a3f187b5be39b168c089e1bceaafe269419053f0b3b638"
SRC_URI[openscap-engine-sce.sha256sum] = "5e35b282811217f307fa9311586e43fd53817c7c221a956eca81c6b67b104411"
SRC_URI[openscap-engine-sce-devel.sha256sum] = "36e1d1d45889e92cc125323cc16ff97bd79e6a7a736eb3228c9c67599bca7193"
SRC_URI[openscap-python3.sha256sum] = "4917e86f469a13977579323fe798d081da8309a147322128cfeebf91fdb3b605"
SRC_URI[openscap-scanner.sha256sum] = "e46d92f5b9daf314f44bc4034f5db2410a1bdc0d220aec864b96a8a609d2267d"
SRC_URI[openscap-utils.sha256sum] = "a4fe38436ba5b010e243ad2b3448c49c157b2cb2fde280bc98355b8413b33ac7"
