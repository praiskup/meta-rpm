SUMMARY = "generated recipe based on mythes-sl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-sl = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-sl-0.20130130-11.el8.noarch.rpm \
          "

SRC_URI[mythes-sl.sha256sum] = "8bb66acada457312f764c7d5ead28232349f67550676025bf5ee7269f2db75fd"
