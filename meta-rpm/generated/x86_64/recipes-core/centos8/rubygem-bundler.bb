SUMMARY = "generated recipe based on rubygem-bundler srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-bundler = "ruby rubygem-io-console rubygems"
RDEPENDS_rubygem-bundler-doc = "rubygem-bundler"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-bundler-1.16.1-3.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-bundler-doc-1.16.1-3.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-bundler.sha256sum] = "b4a8951ad1b56e9d0f896278f20906cc7b39a827bfb7866484bacf4b7ce4659a"
SRC_URI[rubygem-bundler-doc.sha256sum] = "bd4e352442fbde4eb000a823924402884bd3aeff6cb6d54e4ffa687526fb600a"
