SUMMARY = "generated recipe based on evince srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gnome-desktop3 gtk+3 libarchive libgcc libgxps libsecret libspectre libxml2 nautilus pango pkgconfig-native poppler tiff zlib"
RPM_SONAME_REQ_evince = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libevdocument3.so.4 libevview3.so.3 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsecret-1.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_evince = "atk cairo cairo-gobject evince-libs gdk-pixbuf2 glib2 glibc gnome-desktop3 gtk3 libsecret libxml2 pango zlib"
RPM_SONAME_PROV_evince-browser-plugin = "libevbrowserplugin.so"
RPM_SONAME_REQ_evince-browser-plugin = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libevdocument3.so.4 libevview3.so.3 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_evince-browser-plugin = "atk cairo cairo-gobject evince evince-libs gdk-pixbuf2 glib2 glibc gtk3 libgcc libstdc++ mozilla-filesystem pango zlib"
RPM_SONAME_PROV_evince-libs = "libcomicsdocument.so libevdocument3.so.4 libevview3.so.3 libpdfdocument.so libpsdocument.so libtiffdocument.so libxpsdocument.so"
RPM_SONAME_REQ_evince-libs = "libarchive.so.13 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libevdocument3.so.4 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgxps.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpoppler-glib.so.8 libpthread.so.0 libspectre.so.1 libstdc++.so.6 libtiff.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_evince-libs = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libarchive libgcc libgxps libspectre libstdc++ libtiff libxml2 pango poppler-glib zlib"
RPM_SONAME_PROV_evince-nautilus = "libevince-properties-page.so"
RPM_SONAME_REQ_evince-nautilus = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libevdocument3.so.4 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libm.so.6 libnautilus-extension.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libz.so.1"
RDEPENDS_evince-nautilus = "atk cairo cairo-gobject evince evince-libs gdk-pixbuf2 glib2 glibc gtk3 nautilus nautilus-extensions pango zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evince-3.28.4-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evince-browser-plugin-3.28.4-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evince-libs-3.28.4-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evince-nautilus-3.28.4-4.el8.x86_64.rpm \
          "

SRC_URI[evince.sha256sum] = "86133d08bab5584249e06864eb97b6e2383ae39504d433a56ca5efd80a2cb2b8"
SRC_URI[evince-browser-plugin.sha256sum] = "f5c2f956feaeddeb9ef8da113bb86c814d2e0072a1041580afe02d716ee4b3a0"
SRC_URI[evince-libs.sha256sum] = "a0fdb28db04146ba23c2f6dd0b4f1af30346b5b3ee5d941a1f017260429876bd"
SRC_URI[evince-nautilus.sha256sum] = "616ec5fceed16a03b5aaba22ce823ad70a1050268560683a8f462cf95f73940d"
