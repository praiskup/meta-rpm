SUMMARY = "generated recipe based on gnupg2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 gnutls libassuan libgcrypt libgpg-error libksba libusb1 npth openldap pkgconfig-native readline sqlite3 zlib"
RPM_SONAME_REQ_gnupg2 = "libassuan.so.0 libbz2.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgnutls.so.30 libgpg-error.so.0 libksba.so.8 liblber-2.4.so.2 libldap-2.4.so.2 libnpth.so.0 libpthread.so.0 libreadline.so.7 libresolv.so.2 libsqlite3.so.0 libusb-1.0.so.0 libz.so.1"
RDEPENDS_gnupg2 = "bash bzip2-libs glibc gnutls libassuan libgcrypt libgpg-error libksba libusbx npth openldap readline sqlite-libs zlib"
RPM_SONAME_REQ_gnupg2-smime = "libassuan.so.0 libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libksba.so.8 libreadline.so.7"
RDEPENDS_gnupg2-smime = "glibc gnupg2 libassuan libgcrypt libgpg-error libksba readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gnupg2-2.2.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gnupg2-smime-2.2.9-1.el8.x86_64.rpm \
          "

SRC_URI[gnupg2.sha256sum] = "1526b8fe3b26371f6cfe57206321c5dad5b842a77df63ab6523c980be461701e"
SRC_URI[gnupg2-smime.sha256sum] = "9120a674f07261b55ceb204609f83ea41c3d1b3d58df9d35be75dd8cd180716c"
