SUMMARY = "generated recipe based on ocaml-extlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-extlib = "libc.so.6"
RDEPENDS_ocaml-extlib = "glibc ocaml-runtime"
RPROVIDES_ocaml-extlib-devel = "ocaml-extlib-dev (= 1.7.5)"
RDEPENDS_ocaml-extlib-devel = "ocaml-extlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-extlib-1.7.5-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-extlib-devel-1.7.5-3.el8.x86_64.rpm \
          "

SRC_URI[ocaml-extlib.sha256sum] = "f8a4cded2f05dba0724d8f61d16e338f5a856f4fa3ea1056eb23d2fb8620ce4f"
SRC_URI[ocaml-extlib-devel.sha256sum] = "1fe6ff80575878480159f7b63cd2fd18656a6b92351b2019dfa82d36cd066201"
