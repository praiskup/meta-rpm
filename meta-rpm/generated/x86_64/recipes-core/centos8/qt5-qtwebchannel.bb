SUMMARY = "generated recipe based on qt5-qtwebchannel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative qt5-qtwebsockets"
RPM_SONAME_PROV_qt5-qtwebchannel = "libQt5WebChannel.so.5 libdeclarative_webchannel.so"
RPM_SONAME_REQ_qt5-qtwebchannel = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5WebChannel.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebchannel = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtwebchannel-devel = "libQt5WebChannel.so.5"
RPROVIDES_qt5-qtwebchannel-devel = "qt5-qtwebchannel-dev (= 5.12.5)"
RDEPENDS_qt5-qtwebchannel-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtwebchannel"
RPM_SONAME_REQ_qt5-qtwebchannel-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5WebChannel.so.5 libQt5WebSockets.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebchannel-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtwebchannel qt5-qtwebsockets"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwebchannel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwebchannel-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwebchannel-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtwebchannel.sha256sum] = "03338334be606a9558fa2ea3324f40c7025313e9f4e78f2a146bf0d31da4a479"
SRC_URI[qt5-qtwebchannel-devel.sha256sum] = "afbae48eb7c8190cfb69e4de179c798fa140edfeeeb6f923f57e6b4ecdf8c8a5"
SRC_URI[qt5-qtwebchannel-examples.sha256sum] = "507e844ec530357070a5658f99dd8167ce7d1fea075317916a337b927c82d5f8"
