SUMMARY = "generated recipe based on mingw-cairo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-cairo = "mingw32-crt mingw32-filesystem mingw32-fontconfig mingw32-freetype mingw32-gcc mingw32-glib2 mingw32-libpng mingw32-pixman mingw32-pkg-config mingw32-zlib pkgconf-pkg-config"
RDEPENDS_mingw64-cairo = "mingw64-crt mingw64-filesystem mingw64-fontconfig mingw64-freetype mingw64-gcc mingw64-glib2 mingw64-libpng mingw64-pixman mingw64-pkg-config mingw64-zlib pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-cairo-1.14.10-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-cairo-1.14.10-4.el8.noarch.rpm \
          "

SRC_URI[mingw32-cairo.sha256sum] = "2bb38e8bd6ebff2b7a19dd1a6452cab5b1946490438976fa39e736ffa9ba4614"
SRC_URI[mingw64-cairo.sha256sum] = "ce141d3f4a27ba191952816771b19f3b21f7ce3a6766650e15f0b79e1ba8d62f"
