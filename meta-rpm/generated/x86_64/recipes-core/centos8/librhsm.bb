SUMMARY = "generated recipe based on librhsm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 json-glib libgcc openssl pkgconfig-native"
RPM_SONAME_PROV_librhsm = "librhsm.so.0"
RPM_SONAME_REQ_librhsm = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0"
RDEPENDS_librhsm = "glib2 glibc json-glib libgcc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/librhsm-0.0.3-3.el8.x86_64.rpm \
          "

SRC_URI[librhsm.sha256sum] = "04fa9c9f09e539ba37ce8b13093a56276f6f3c0d1c88601df71eb7770ad6c599"
