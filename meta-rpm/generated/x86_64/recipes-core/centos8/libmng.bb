SUMMARY = "generated recipe based on libmng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lcms2 libjpeg-turbo pkgconfig-native zlib"
RPM_SONAME_PROV_libmng = "libmng.so.2"
RPM_SONAME_REQ_libmng = "libc.so.6 libjpeg.so.62 liblcms2.so.2 libm.so.6 libz.so.1"
RDEPENDS_libmng = "glibc lcms2 libjpeg-turbo zlib"
RPM_SONAME_REQ_libmng-devel = "libmng.so.2"
RPROVIDES_libmng-devel = "libmng-dev (= 2.0.3)"
RDEPENDS_libmng-devel = "libjpeg-turbo-devel libmng pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmng-2.0.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmng-devel-2.0.3-7.el8.x86_64.rpm \
          "

SRC_URI[libmng.sha256sum] = "2e7eee8ef66c0280bcab4b25ec4e6b703ef07b0f7cf9aee1edac8c9356565186"
SRC_URI[libmng-devel.sha256sum] = "0aad54fd80422d7bf986f379812e42f02fa4ceff9ee5c04c2a2c23a58bf42687"
