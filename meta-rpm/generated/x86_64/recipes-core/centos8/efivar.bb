SUMMARY = "generated recipe based on efivar srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_efivar = "libc.so.6 libdl.so.2 libefivar.so.1"
RDEPENDS_efivar = "efivar-libs glibc"
RPM_SONAME_PROV_efivar-libs = "libefiboot.so.1 libefivar.so.1"
RPM_SONAME_REQ_efivar-libs = "libc.so.6 libdl.so.2 libefivar.so.1"
RDEPENDS_efivar-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/efivar-36-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/efivar-libs-36-1.el8.x86_64.rpm \
          "

SRC_URI[efivar.sha256sum] = "d58a180583863d841ea9113aa2e84defc8f21ac1df0bc719b5f6cd318ac307ef"
SRC_URI[efivar-libs.sha256sum] = "bb33b102d9d09f7dc4b128009894e64a0da39f75d23722fb7c5f5b4fdde5cf12"
