SUMMARY = "generated recipe based on sblim-sfcCommon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sblim-sfcCommon = "libsfcUtil.so.0"
RPM_SONAME_REQ_sblim-sfcCommon = "libc.so.6 libdl.so.2"
RDEPENDS_sblim-sfcCommon = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sblim-sfcCommon-1.0.1-13.el8.x86_64.rpm \
          "

SRC_URI[sblim-sfcCommon.sha256sum] = "d1f4662f92fd5af84b87a9a03e845e8efe7789d5941490a7d7bb077c4f37c864"
