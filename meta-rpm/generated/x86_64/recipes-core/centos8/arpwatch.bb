SUMMARY = "generated recipe based on arpwatch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcap pkgconfig-native"
RPM_SONAME_REQ_arpwatch = "libc.so.6 libpcap.so.1"
RDEPENDS_arpwatch = "bash glibc libpcap postfix shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/arpwatch-2.1a15-44.el8.x86_64.rpm \
          "

SRC_URI[arpwatch.sha256sum] = "acc0cac172dd4fcfed725bab77901a521902c30a66525f3273d2d9b7c7076fdc"
