SUMMARY = "generated recipe based on open-vm-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atkmm cairomm fuse glib-2.0 glibmm24 gtk+3 gtkmm30 libgcc libmspack libsigc++20 libtirpc libtool libx11 libxcrypt libxext libxi libxinerama libxml2 libxrandr libxslt libxtst openssl pkgconfig-native xmlsec1 xz zlib"
RPM_SONAME_PROV_open-vm-tools = "libDeployPkg.so.0 libappInfo.so libdeployPkgPlugin.so libguestInfo.so libguestlib.so.0 libhgfs.so.0 libhgfsServer.so libpowerOps.so libresolutionKMS.so libtimeSync.so libvgauth.so.0 libvix.so libvmbackup.so libvmtools.so.0"
RPM_SONAME_REQ_open-vm-tools = "ld-linux-x86-64.so.2 libDeployPkg.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfuse.so.2 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libguestlib.so.0 libhgfs.so.0 libltdl.so.7 liblzma.so.5 libm.so.6 libmspack.so.0 libpthread.so.0 librt.so.1 libssl.so.1.1 libtirpc.so.3 libvgauth.so.0 libvmtools.so.0 libxml2.so.2 libxmlsec1.so.1 libxslt.so.1 libz.so.1"
RDEPENDS_open-vm-tools = "bash coreutils fuse fuse-libs glib2 glibc grep iproute libdrm libgcc libmspack libtirpc libtool-ltdl libxcrypt libxml2 libxslt openssl-libs pciutils sed systemd systemd-libs tar which xmlsec1 xmlsec1-openssl xz-libs zlib"
RPM_SONAME_PROV_open-vm-tools-desktop = "libdesktopEvents.so libdndcp.so libresolutionSet.so"
RPM_SONAME_REQ_open-vm-tools-desktop = "libX11.so.6 libXext.so.6 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXtst.so.6 libatkmm-1.6.so.1 libc.so.6 libcairomm-1.0.so.1 libdl.so.2 libfuse.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdkmm-3.0.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libgtk-3.so.0 libgtkmm-3.0.so.1 libhgfs.so.0 libpthread.so.0 libsigc-2.0.so.0 libstdc++.so.6 libtirpc.so.3 libvmtools.so.0"
RDEPENDS_open-vm-tools-desktop = "atkmm bash cairomm fuse-libs glib2 glibc glibmm24 gtk3 gtkmm30 libX11 libXext libXi libXinerama libXrandr libXtst libgcc libsigc++20 libstdc++ libtirpc open-vm-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/open-vm-tools-11.0.5-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/open-vm-tools-desktop-11.0.5-3.el8.x86_64.rpm \
          "

SRC_URI[open-vm-tools.sha256sum] = "f9fde32fe5a5c242a6d83044e88d34e34899206ed20e8349d3c5a406250a375d"
SRC_URI[open-vm-tools-desktop.sha256sum] = "ae4e3514ed4015905a925a6ab544f8d01e2f8369d194dbc69d27926c3081f8f3"
