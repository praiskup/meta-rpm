SUMMARY = "generated recipe based on SDL2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd libx11 mesa pkgconfig-native"
RPM_SONAME_PROV_SDL2 = "libSDL2-2.0.so.0"
RPM_SONAME_REQ_SDL2 = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_SDL2 = "glibc"
RPM_SONAME_REQ_SDL2-devel = "libSDL2-2.0.so.0"
RPROVIDES_SDL2-devel = "SDL2-dev (= 2.0.10)"
RDEPENDS_SDL2-devel = "SDL2 bash libX11-devel libglvnd-devel mesa-libEGL-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/SDL2-2.0.10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/SDL2-devel-2.0.10-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/SDL2-static-2.0.10-2.el8.x86_64.rpm \
          "

SRC_URI[SDL2.sha256sum] = "59e42fa5fdf2a3cc66370d11899adcda60b0623c766fb63093be7e6f4bb8c9e8"
SRC_URI[SDL2-devel.sha256sum] = "d69cfe4baf1afa7d299168862ba4d374e22863f578d156a7a1e612101a13ca3b"
SRC_URI[SDL2-static.sha256sum] = "e3bb45eaedd0e57cb04ddb1dd0e0a964303485bb1d90f123c9d3dc42f214d48b"
