SUMMARY = "generated recipe based on libmspub srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc librevenge pkgconfig-native zlib"
RPM_SONAME_PROV_libmspub = "libmspub-0.1.so.1"
RPM_SONAME_REQ_libmspub = "libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_libmspub = "glibc libgcc libicu librevenge libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmspub-0.1.4-1.el8.x86_64.rpm \
          "

SRC_URI[libmspub.sha256sum] = "8c3dc18332b1a35e3d0f574d3de7d9c2698bfe88522a1dd738fd201c8f3bf064"
