SUMMARY = "generated recipe based on trace-cmd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 libxml2 pango pkgconfig-native"
RPM_SONAME_REQ_kernelshark = "ld-linux-x86-64.so.2 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libxml2.so.2"
RDEPENDS_kernelshark = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libxml2 pango trace-cmd"
RPM_SONAME_REQ_trace-cmd = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2"
RDEPENDS_trace-cmd = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kernelshark-2.7-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/trace-cmd-2.7-8.el8.x86_64.rpm \
          "

SRC_URI[kernelshark.sha256sum] = "d6ea4db97bf72d67cea2efacfcd1c3d13ad683b9b341e68e284c15aa38354a79"
SRC_URI[trace-cmd.sha256sum] = "95ea8632d37a35bbcb09e9ed879bcbb0f3710b40f0c69d1a124dda9dd3fcc912"
