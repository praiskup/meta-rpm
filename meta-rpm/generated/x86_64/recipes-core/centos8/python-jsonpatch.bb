SUMMARY = "generated recipe based on python-jsonpatch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jsonpatch = "platform-python python3-jsonpointer"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-jsonpatch-1.21-2.el8.noarch.rpm \
          "

SRC_URI[python3-jsonpatch.sha256sum] = "85eb614fc608f3b3f4bbd08d4a3b0ff6af188677ea4e009be021680c521cedae"
