SUMMARY = "generated recipe based on libnl3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native platform-python3"
RPM_SONAME_PROV_libnl3 = "libnl-3.so.200 libnl-genl-3.so.200 libnl-idiag-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libnl-xfrm-3.so.200"
RPM_SONAME_REQ_libnl3 = "libc.so.6 libgcc_s.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0"
RDEPENDS_libnl3 = "glibc libgcc"
RPM_SONAME_PROV_libnl3-cli = "libnl-cli-3.so.200"
RPM_SONAME_REQ_libnl3-cli = "libc.so.6 libdl.so.2 libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-idiag-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libpthread.so.0"
RDEPENDS_libnl3-cli = "glibc libnl3"
RPM_SONAME_REQ_libnl3-devel = "libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-idiag-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libnl-xfrm-3.so.200"
RPROVIDES_libnl3-devel = "libnl3-dev (= 3.5.0)"
RDEPENDS_libnl3-devel = "kernel-headers libnl3 libnl3-cli pkgconf-pkg-config"
RDEPENDS_libnl3-doc = "libnl3"
RPM_SONAME_REQ_python3-libnl3 = "libc.so.6 libnl-3.so.200 libnl-genl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-libnl3 = "glibc libnl3 platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnl3-3.5.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnl3-cli-3.5.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnl3-devel-3.5.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnl3-doc-3.5.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libnl3-3.5.0-1.el8.x86_64.rpm \
          "

SRC_URI[libnl3.sha256sum] = "21c65dbf3b506a37828b13c205077f4b70fddb4b1d1c929dec01661238108059"
SRC_URI[libnl3-cli.sha256sum] = "2e8d4ee76fa8678b0e4d6c0297b16f96e0116201bf96b36e8924b1b080abcddd"
SRC_URI[libnl3-devel.sha256sum] = "1c34c0e0f51ad76e25eb1077090473928bc17a70229a743a5de4fbc4882384d2"
SRC_URI[libnl3-doc.sha256sum] = "9f5064e55238ca9e0e45c7a6f66329ccbbc78bf1b456085d0d2000c180f24534"
SRC_URI[python3-libnl3.sha256sum] = "c7c5fb941f91deb75fa7754c3c1d757ddfdb58ee1d5d631e3e9a78fcffc7b650"
