SUMMARY = "generated recipe based on unixODBC srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtool pkgconfig-native readline"
RPM_SONAME_PROV_unixODBC = "libesoobS.so.2 libmimerS.so.2 libnn.so.2 libodbc.so.2 libodbccr.so.2 libodbcdrvcfg1S.so.2 libodbcdrvcfg2S.so.2 libodbcinst.so.2 libodbcminiS.so.2 libodbcmyS.so.2 libodbcnnS.so.2 libodbcpsqlS.so.2 libodbctxtS.so.2 liboplodbcS.so.2 liboraodbcS.so.2 libsapdbS.so.2 libtdsS.so.2 libtemplate.so.2"
RPM_SONAME_REQ_unixODBC = "libc.so.6 libltdl.so.7 libodbc.so.2 libodbcinst.so.2 libodbcmyS.so.2 libodbcpsqlS.so.2 libpthread.so.0 libreadline.so.7 libtdsS.so.2"
RDEPENDS_unixODBC = "glibc libtool-ltdl readline"
RPM_SONAME_REQ_unixODBC-devel = "libesoobS.so.2 libmimerS.so.2 libnn.so.2 libodbccr.so.2 libodbcdrvcfg1S.so.2 libodbcdrvcfg2S.so.2 libodbcminiS.so.2 libodbcnnS.so.2 libodbctxtS.so.2 liboplodbcS.so.2 liboraodbcS.so.2 libsapdbS.so.2 libtemplate.so.2"
RPROVIDES_unixODBC-devel = "unixODBC-dev (= 2.3.7)"
RDEPENDS_unixODBC-devel = "pkgconf-pkg-config unixODBC"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/unixODBC-2.3.7-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/unixODBC-devel-2.3.7-1.el8.x86_64.rpm \
          "

SRC_URI[unixODBC.sha256sum] = "3324b770cdd837ca950189d66510ca91514706a19ac9ded7d0d95318296ae284"
SRC_URI[unixODBC-devel.sha256sum] = "65cec0e2a4a3cf5f36988704dab1f1571c6543263de131d175e3d1c4f33cb4fe"
