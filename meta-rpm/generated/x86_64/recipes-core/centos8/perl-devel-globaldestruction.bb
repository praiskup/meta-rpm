SUMMARY = "generated recipe based on perl-Devel-GlobalDestruction srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-GlobalDestruction = "perl-Sub-Exporter-Progressive perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-GlobalDestruction-0.14-5.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-GlobalDestruction.sha256sum] = "a247e8f1643b42c9ef6ceb7160c58d67663fe0fc1d18a2bfc5cdc86226f89402"
