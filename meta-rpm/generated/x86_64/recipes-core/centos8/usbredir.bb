SUMMARY = "generated recipe based on usbredir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_PROV_usbredir = "libusbredirhost.so.1 libusbredirparser.so.1"
RPM_SONAME_REQ_usbredir = "libc.so.6 libusb-1.0.so.0 libusbredirparser.so.1"
RDEPENDS_usbredir = "glibc libusbx"
RPM_SONAME_REQ_usbredir-devel = "libusbredirhost.so.1 libusbredirparser.so.1"
RPROVIDES_usbredir-devel = "usbredir-dev (= 0.8.0)"
RDEPENDS_usbredir-devel = "libusbx-devel pkgconf-pkg-config usbredir"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/usbredir-0.8.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/usbredir-devel-0.8.0-1.el8.x86_64.rpm \
          "

SRC_URI[usbredir.sha256sum] = "359290c30476453554d970c0f5360b6039e8b92fb72018a65b7e56b38f260bda"
SRC_URI[usbredir-devel.sha256sum] = "373dd29d122e63062ce512f6363fa0263c7298bae6a14fccdce1a346d3a83148"
