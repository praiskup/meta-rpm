SUMMARY = "generated recipe based on tbb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-tbb = "libc.so.6 libgcc_s.so.1 libirml.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6 libtbb.so.2"
RDEPENDS_python3-tbb = "glibc libgcc libstdc++ platform-python python3-libs tbb"
RPM_SONAME_PROV_tbb = "libirml.so.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RPM_SONAME_REQ_tbb = "libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0 librt.so.1 libstdc++.so.6 libtbbmalloc.so.2"
RDEPENDS_tbb = "glibc libgcc libstdc++"
RPM_SONAME_REQ_tbb-devel = "libirml.so.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RPROVIDES_tbb-devel = "tbb-dev (= 2018.2)"
RDEPENDS_tbb-devel = "pkgconf-pkg-config tbb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-tbb-2018.2-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tbb-2018.2-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tbb-devel-2018.2-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tbb-doc-2018.2-9.el8.x86_64.rpm \
          "

SRC_URI[python3-tbb.sha256sum] = "21fabdb111ed551a1d2f577b5231a66c4b06265280ac29505ac5cbffd57b5364"
SRC_URI[tbb.sha256sum] = "8f6efb37a1fb6c89b8acf715eb1ffea585c79a6fbf46774ae12dc85d02d4ed99"
SRC_URI[tbb-devel.sha256sum] = "f08b7f67ceb54724d4615c72f8f021451f4c51b3a916bfffa94681b4572e2900"
SRC_URI[tbb-doc.sha256sum] = "82af50758bc0bdcfa4f52a7442637603ced79c69142baf39fa16c9b181dd7de0"
