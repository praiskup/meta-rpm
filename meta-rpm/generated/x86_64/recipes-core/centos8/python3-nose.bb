SUMMARY = "generated recipe based on python-nose srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-nose = "bash platform-python python3-setuptools python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python-nose-docs-1.3.7-30.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-nose-1.3.7-30.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python-nose-docs.sha256sum] = "d6b0401c643f72292b65ecfe452c6c66830f7641ffe50962932cffa0ed5f6e0a"
SRC_URI[python3-nose.sha256sum] = "cae836bc7eb4ce98a8d551b83b3ff3705d1835b9f1b4ca9d1d9e989d73eb07bd"
