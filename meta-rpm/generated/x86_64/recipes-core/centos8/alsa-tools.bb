SUMMARY = "generated recipe based on alsa-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib pkgconfig-native"
RPM_SONAME_REQ_alsa-tools-firmware = "libasound.so.2 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_alsa-tools-firmware = "alsa-firmware alsa-lib bash fxload glibc systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-tools-firmware-1.1.6-1.el8.x86_64.rpm \
          "

SRC_URI[alsa-tools-firmware.sha256sum] = "fdeea775d50537c4de841a208fadf184c3c3ff15f4f6b61de5a9f7bcdd5736fa"
