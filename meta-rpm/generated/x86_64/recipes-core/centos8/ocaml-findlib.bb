SUMMARY = "generated recipe based on ocaml-findlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-findlib = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-findlib = "bash glibc ocaml ocaml-compiler-libs ocaml-runtime"
RPROVIDES_ocaml-findlib-devel = "ocaml-findlib-dev (= 1.8.0)"
RDEPENDS_ocaml-findlib-devel = "ocaml-findlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-findlib-1.8.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-findlib-devel-1.8.0-4.el8.x86_64.rpm \
          "

SRC_URI[ocaml-findlib.sha256sum] = "838807468b1ebd111878761836a112d1f0017da2fa3d6c70968b120f15492e32"
SRC_URI[ocaml-findlib-devel.sha256sum] = "31af11253993e0e4106011a5679d193733eea5b2e9ef198ec9336af361b1a5e4"
