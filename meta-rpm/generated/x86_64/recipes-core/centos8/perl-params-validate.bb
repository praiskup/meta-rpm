SUMMARY = "generated recipe based on perl-Params-Validate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Validate = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Validate = "glibc perl-Carp perl-Exporter perl-Module-Implementation perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Params-Validate-1.29-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Params-Validate.sha256sum] = "c35b54a8208dd05fa567cde52fa5e262ca6588c0f999004f6280e928f007b208"
