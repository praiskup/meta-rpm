SUMMARY = "generated recipe based on transfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng libxpm pkgconfig-native"
RPM_SONAME_REQ_transfig = "libXpm.so.4 libc.so.6 libm.so.6 libpng16.so.16"
RDEPENDS_transfig = "bash bc ghostscript glibc libXpm libpng netpbm-progs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/transfig-3.2.6a-4.el8.x86_64.rpm \
          "

SRC_URI[transfig.sha256sum] = "fd8edfa7d0458d9d49c2b462e3a0f440125849b4bd9a8a0f7120ac3c9b4b58a6"
