SUMMARY = "generated recipe based on mythes-bg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-bg = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-bg-4.3-12.el8.noarch.rpm \
          "

SRC_URI[mythes-bg.sha256sum] = "1a27ccb9364a9aa047fd8f5d2c4517436ae4d1ab17f2b3e019170d770a747442"
