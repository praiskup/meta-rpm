SUMMARY = "generated recipe based on dos2unix srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dos2unix = "libc.so.6"
RDEPENDS_dos2unix = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dos2unix-7.4.0-3.el8.x86_64.rpm \
          "

SRC_URI[dos2unix.sha256sum] = "6f7afb5059730bb99701fb78852e55c6b3ab1e2afcd743c5b6433a9d094c0642"
