SUMMARY = "generated recipe based on perl-File-BaseDir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-BaseDir = "perl-Carp perl-Exporter perl-PathTools perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-File-BaseDir-0.08-1.el8.noarch.rpm \
          "

SRC_URI[perl-File-BaseDir.sha256sum] = "71a9977ad15e1a877ee6bcc8c367fe4a5e03e1991c2956cf0df61a4579ea36d0"
