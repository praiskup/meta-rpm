SUMMARY = "generated recipe based on xorg-x11-font-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "freetype libfontenc pkgconfig-native zlib"
RPM_SONAME_REQ_xorg-x11-font-utils = "libc.so.6 libfontenc.so.1 libfreetype.so.6 libm.so.6 libz.so.1"
RDEPENDS_xorg-x11-font-utils = "bash freetype glibc libfontenc pkgconf-pkg-config zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-font-utils-7.5-40.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-font-utils.sha256sum] = "b15e5fef01e82f629e6fadea0e1a1eadcc1092b600e087dda23cfb46cb9122a4"
