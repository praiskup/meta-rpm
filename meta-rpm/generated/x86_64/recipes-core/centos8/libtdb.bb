SUMMARY = "generated recipe based on libtdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcrypt pkgconfig-native platform-python3"
RPM_SONAME_PROV_libtdb = "libtdb.so.1"
RPM_SONAME_REQ_libtdb = "libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0"
RDEPENDS_libtdb = "glibc libxcrypt"
RPM_SONAME_REQ_libtdb-devel = "libtdb.so.1"
RPROVIDES_libtdb-devel = "libtdb-dev (= 1.4.2)"
RDEPENDS_libtdb-devel = "libtdb pkgconf-pkg-config"
RPM_SONAME_REQ_python3-tdb = "libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libtdb.so.1 libutil.so.1"
RDEPENDS_python3-tdb = "glibc libtdb libxcrypt platform-python python3-libs"
RPM_SONAME_REQ_tdb-tools = "libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0 libtdb.so.1"
RDEPENDS_tdb-tools = "glibc libtdb libxcrypt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtdb-1.4.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtdb-devel-1.4.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-tdb-1.4.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tdb-tools-1.4.2-2.el8.x86_64.rpm \
          "

SRC_URI[libtdb.sha256sum] = "4286c2a19f684bcd7c95235514f62e16d32f8e5042bc3dbc001af4644ab00180"
SRC_URI[libtdb-devel.sha256sum] = "c3c506ddd07007f6cddd09dd9f63a6c38235fe3ff9626066d5a6c2899d52f3ef"
SRC_URI[python3-tdb.sha256sum] = "2b92f1de17f365babd6bdd6174c58ca5ca28b0d59fa2ddc2d5c8c34395d5bf23"
SRC_URI[tdb-tools.sha256sum] = "41c8abbc37bb327fb086b1afa60590f9d1270e4dd77e6262b2b5091f1c459a77"
