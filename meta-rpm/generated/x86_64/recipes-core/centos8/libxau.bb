SUMMARY = "generated recipe based on libXau srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xau"
DEPENDS = "pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXau = "libXau.so.6"
RPM_SONAME_REQ_libXau = "libc.so.6"
RDEPENDS_libXau = "glibc"
RPM_SONAME_REQ_libXau-devel = "libXau.so.6"
RPROVIDES_libXau-devel = "libXau-dev (= 1.0.8)"
RDEPENDS_libXau-devel = "libXau pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXau-1.0.8-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXau-devel-1.0.8-13.el8.x86_64.rpm \
          "

SRC_URI[libXau.sha256sum] = "b8e561c3256847079d474be17b6d4c1d0c52f54b9235dff42878af786984f183"
SRC_URI[libXau-devel.sha256sum] = "09ccccaee57f0f6238780971e33773da94cc1b9ba2984c267e481b48a69d00c1"
