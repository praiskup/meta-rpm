SUMMARY = "generated recipe based on mod_lookup_identity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs pkgconfig-native"
RPM_SONAME_REQ_mod_lookup_identity = "libc.so.6 libdbus-1.so.3"
RDEPENDS_mod_lookup_identity = "dbus-libs glibc httpd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_lookup_identity-1.0.0-4.el8.x86_64.rpm \
          "

SRC_URI[mod_lookup_identity.sha256sum] = "8d68bddeba1ed522201f918b1f1724c1e3550c27712c7575ecfb7f09fba9e4e1"
