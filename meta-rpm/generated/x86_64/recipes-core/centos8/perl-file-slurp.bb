SUMMARY = "generated recipe based on perl-File-Slurp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Slurp = "perl-Carp perl-Errno perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-File-Slurp-9999.19-19.el8.noarch.rpm \
          "

SRC_URI[perl-File-Slurp.sha256sum] = "a9b68be3e50de0f2f9be1ea8a828003c6aa874088405b0520ad8573d811ef3aa"
