SUMMARY = "generated recipe based on dleyna-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gupnp pkgconfig-native"
RPM_SONAME_PROV_dleyna-core = "libdleyna-core-1.0.so.5"
RPM_SONAME_REQ_dleyna-core = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgupnp-1.0.so.4 libpthread.so.0"
RDEPENDS_dleyna-core = "glib2 glibc gupnp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dleyna-core-0.6.0-2.el8.x86_64.rpm \
          "

SRC_URI[dleyna-core.sha256sum] = "fecf79fdf1f0dc6db1681133085c5e71719e1fdcf85fbc73852ec14af2e43983"
