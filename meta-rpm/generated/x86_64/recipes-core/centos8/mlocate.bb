SUMMARY = "generated recipe based on mlocate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mlocate = "libc.so.6"
RDEPENDS_mlocate = "bash glibc grep sed shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mlocate-0.26-20.el8.x86_64.rpm \
          "

SRC_URI[mlocate.sha256sum] = "902cdfbf959cb68d0f1b7a6630a2cc7efa697dbe2ee5eec32b0a6ee21a90dec9"
