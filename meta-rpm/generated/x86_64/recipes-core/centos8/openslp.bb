SUMMARY = "generated recipe based on openslp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native zlib"
RPM_SONAME_PROV_openslp = "libslp.so.1"
RPM_SONAME_REQ_openslp = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpthread.so.0 libresolv.so.2 libslp.so.1 libz.so.1"
RDEPENDS_openslp = "glibc openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openslp-2.0.0-18.el8.x86_64.rpm \
          "

SRC_URI[openslp.sha256sum] = "68acc0fe02bcc4f98f77bcf0e8fa3d9d80781ea8277675422fe9cdbf2cb63962"
