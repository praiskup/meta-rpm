SUMMARY = "generated recipe based on libieee1284 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libieee1284 = "libieee1284.so.3"
RPM_SONAME_REQ_libieee1284 = "libc.so.6 libieee1284.so.3"
RDEPENDS_libieee1284 = "glibc"
RPM_SONAME_REQ_libieee1284-devel = "libieee1284.so.3"
RPROVIDES_libieee1284-devel = "libieee1284-dev (= 0.2.11)"
RDEPENDS_libieee1284-devel = "libieee1284"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libieee1284-0.2.11-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libieee1284-devel-0.2.11-28.el8.x86_64.rpm \
          "

SRC_URI[libieee1284.sha256sum] = "bf2be416a728dbd386ddfb692d30bbf67fd1b0f1f21cfab1a04a00d45979b929"
SRC_URI[libieee1284-devel.sha256sum] = "8301a1fe2fe40c86ad79063320ce94ed947220f7e4beb158fc169bf28ce0f8b2"
