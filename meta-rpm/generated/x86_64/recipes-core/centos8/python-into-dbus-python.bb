SUMMARY = "generated recipe based on python-into-dbus-python srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-into-dbus-python = "platform-python python3-dbus python3-dbus-signature-pyparsing"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-into-dbus-python-0.06-2.el8.noarch.rpm \
          "

SRC_URI[python3-into-dbus-python.sha256sum] = "fb861d5c01b708d059e334b8f308aa56673d902b735282e1f7afa108fdc81f14"
