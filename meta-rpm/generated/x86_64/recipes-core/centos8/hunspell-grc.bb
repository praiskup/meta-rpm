SUMMARY = "generated recipe based on hunspell-grc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-grc = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-grc-2.1.5-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-grc.sha256sum] = "d47be1c5409156a4d8d19b9420f155ed8c8ff32e46e38c5a95fc7ffb790a3ff4"
