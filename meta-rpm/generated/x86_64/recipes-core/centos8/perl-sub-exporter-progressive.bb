SUMMARY = "generated recipe based on perl-Sub-Exporter-Progressive srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Exporter-Progressive = "perl-Carp perl-Exporter perl-Sub-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Sub-Exporter-Progressive-0.001013-5.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Exporter-Progressive.sha256sum] = "487feb97bac5263197426a30ccd0a24e8537640541507f3539477acb999dbdf5"
