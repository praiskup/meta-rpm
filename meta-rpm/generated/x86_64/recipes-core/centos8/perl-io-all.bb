SUMMARY = "generated recipe based on perl-IO-All srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-All = "perl-Carp perl-File-MimeInfo perl-File-Path perl-File-ReadBackwards perl-IO perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-IO-All-0.87-6.el8.noarch.rpm \
          "

SRC_URI[perl-IO-All.sha256sum] = "989810bfadb0bc1fc15dd000d7002af357b2dc8044708feca0ecf0fd8c9adc3d"
