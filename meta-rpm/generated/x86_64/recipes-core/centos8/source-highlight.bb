SUMMARY = "generated recipe based on source-highlight srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost libgcc pkgconfig-native"
RPM_SONAME_PROV_source-highlight = "libsource-highlight.so.4"
RPM_SONAME_REQ_source-highlight = "libboost_regex.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libsource-highlight.so.4 libstdc++.so.6"
RDEPENDS_source-highlight = "bash boost-regex ctags glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/source-highlight-3.1.8-16.el8.x86_64.rpm \
          "

SRC_URI[source-highlight.sha256sum] = "90c855cfb057c5546cdcda64f2ec9f69f22d5d8aafa7b1fc7d99847640975af9"
