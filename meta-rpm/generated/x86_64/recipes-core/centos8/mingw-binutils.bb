SUMMARY = "generated recipe based on mingw-binutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mingw-binutils-generic = "libc.so.6 libdl.so.2"
RDEPENDS_mingw-binutils-generic = "glibc"
RPM_SONAME_REQ_mingw32-binutils = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mingw32-binutils = "glibc mingw-binutils-generic mingw32-filesystem"
RPM_SONAME_REQ_mingw64-binutils = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mingw64-binutils = "glibc mingw-binutils-generic mingw64-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw-binutils-generic-2.30-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-binutils-2.30-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-binutils-2.30-1.el8.x86_64.rpm \
          "

SRC_URI[mingw-binutils-generic.sha256sum] = "284cc9a33d673b86f954ece122333170f0a47bcd565b0f611cfe04d762d347ea"
SRC_URI[mingw32-binutils.sha256sum] = "7bbfeceb9e2d4557e9ed644e5ae0248d381f51bae9e4e91ac002789588e201e3"
SRC_URI[mingw64-binutils.sha256sum] = "3da181c70e369d8d3a3683c890ef9e112eb98f78db8168ffac54786c75a0e0f0"
