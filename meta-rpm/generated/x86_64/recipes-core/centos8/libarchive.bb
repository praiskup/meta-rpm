SUMMARY = "generated recipe based on libarchive srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl bzip2 libxml2 lz4 openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_bsdtar = "libacl.so.1 libarchive.so.13 libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblz4.so.1 liblzma.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_bsdtar = "bzip2-libs glibc libacl libarchive libxml2 lz4-libs openssl-libs xz-libs zlib"
RPM_SONAME_PROV_libarchive = "libarchive.so.13"
RPM_SONAME_REQ_libarchive = "libacl.so.1 libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblz4.so.1 liblzma.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_libarchive = "bzip2-libs glibc libacl libxml2 lz4-libs openssl-libs xz-libs zlib"
RPM_SONAME_REQ_libarchive-devel = "libarchive.so.13"
RPROVIDES_libarchive-devel = "libarchive-dev (= 3.3.2)"
RDEPENDS_libarchive-devel = "libarchive pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bsdtar-3.3.2-8.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libarchive-3.3.2-8.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libarchive-devel-3.3.2-8.el8_1.x86_64.rpm \
          "

SRC_URI[bsdtar.sha256sum] = "6230d683edf72347541ac6a1117a9d2c8c8cd60d5df13161ee642fe21b232642"
SRC_URI[libarchive.sha256sum] = "aeb3d9b4b2877a07c39984c997f601a53cbf9f4cc31cfb2b1fe77ded0066209d"
SRC_URI[libarchive-devel.sha256sum] = "d7f9af83484412966b1b8766098ebc92d6dfaef18cd8ca5f74696428b44ec86f"
