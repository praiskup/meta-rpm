SUMMARY = "generated recipe based on libgxps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo freetype glib-2.0 lcms2 libarchive libjpeg-turbo libpng pkgconfig-native tiff"
RPM_SONAME_PROV_libgxps = "libgxps.so.2"
RPM_SONAME_REQ_libgxps = "libarchive.so.13 libc.so.6 libcairo.so.2 libfreetype.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpng16.so.16 libtiff.so.5"
RDEPENDS_libgxps = "cairo freetype glib2 glibc lcms2 libarchive libjpeg-turbo libpng libtiff"
RPM_SONAME_REQ_libgxps-devel = "libgxps.so.2"
RPROVIDES_libgxps-devel = "libgxps-dev (= 0.3.0)"
RDEPENDS_libgxps-devel = "cairo-devel glib2-devel libarchive-devel libgxps pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgxps-0.3.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgxps-devel-0.3.0-5.el8.x86_64.rpm \
          "

SRC_URI[libgxps.sha256sum] = "64d7f7ba40c8b2c427cad133beb5e99ac11ff980f7623ce4c975d9bcca272d10"
SRC_URI[libgxps-devel.sha256sum] = "0ed2a6af2875eb91e92ac0d669a4eaa3619606c977f82cd89512dde0964dfa05"
