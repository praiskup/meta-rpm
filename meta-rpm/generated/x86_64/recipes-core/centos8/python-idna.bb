SUMMARY = "generated recipe based on python-idna srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-idna = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-idna-2.5-5.el8.noarch.rpm \
          "

SRC_URI[python3-idna.sha256sum] = "78c43d8a15ca018de1803e4baac5819e1baab1963fd31f1e44e54e8a573acbfc"
