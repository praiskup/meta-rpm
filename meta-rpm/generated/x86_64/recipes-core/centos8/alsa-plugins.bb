SUMMARY = "generated recipe based on alsa-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib dbus-libs libsamplerate0 pkgconfig-native pulseaudio speexdsp"
RPM_SONAME_PROV_alsa-plugins-arcamav = "libasound_module_ctl_arcam_av.so"
RPM_SONAME_REQ_alsa-plugins-arcamav = "libasound.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_alsa-plugins-arcamav = "alsa-lib glibc"
RPM_SONAME_PROV_alsa-plugins-maemo = "libasound_module_ctl_dsp_ctl.so libasound_module_pcm_alsa_dsp.so"
RPM_SONAME_REQ_alsa-plugins-maemo = "libasound.so.2 libc.so.6 libdbus-1.so.3 libpthread.so.0"
RDEPENDS_alsa-plugins-maemo = "alsa-lib dbus-libs glibc"
RPM_SONAME_PROV_alsa-plugins-oss = "libasound_module_ctl_oss.so libasound_module_pcm_oss.so"
RPM_SONAME_REQ_alsa-plugins-oss = "libasound.so.2 libc.so.6"
RDEPENDS_alsa-plugins-oss = "alsa-lib alsa-utils glibc"
RPM_SONAME_PROV_alsa-plugins-pulseaudio = "libasound_module_conf_pulse.so libasound_module_ctl_pulse.so libasound_module_pcm_pulse.so"
RPM_SONAME_REQ_alsa-plugins-pulseaudio = "libasound.so.2 libc.so.6 libpulse.so.0"
RDEPENDS_alsa-plugins-pulseaudio = "alsa-lib alsa-utils glibc pulseaudio pulseaudio-libs"
RPM_SONAME_PROV_alsa-plugins-samplerate = "libasound_module_rate_samplerate.so"
RPM_SONAME_REQ_alsa-plugins-samplerate = "libasound.so.2 libasound_module_rate_samplerate.so libc.so.6 libsamplerate.so.0"
RDEPENDS_alsa-plugins-samplerate = "alsa-lib alsa-utils glibc libsamplerate"
RPM_SONAME_PROV_alsa-plugins-speex = "libasound_module_pcm_speex.so libasound_module_rate_speexrate.so"
RPM_SONAME_REQ_alsa-plugins-speex = "libasound.so.2 libasound_module_rate_speexrate.so libc.so.6 libspeexdsp.so.1"
RDEPENDS_alsa-plugins-speex = "alsa-lib glibc speex speexdsp"
RPM_SONAME_PROV_alsa-plugins-upmix = "libasound_module_pcm_upmix.so"
RPM_SONAME_REQ_alsa-plugins-upmix = "libasound.so.2 libc.so.6"
RDEPENDS_alsa-plugins-upmix = "alsa-lib alsa-utils glibc"
RPM_SONAME_PROV_alsa-plugins-usbstream = "libasound_module_pcm_usb_stream.so"
RPM_SONAME_REQ_alsa-plugins-usbstream = "libasound.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_alsa-plugins-usbstream = "alsa-lib glibc"
RPM_SONAME_PROV_alsa-plugins-vdownmix = "libasound_module_pcm_vdownmix.so"
RPM_SONAME_REQ_alsa-plugins-vdownmix = "libasound.so.2 libc.so.6"
RDEPENDS_alsa-plugins-vdownmix = "alsa-lib alsa-utils glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-arcamav-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-maemo-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-oss-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-pulseaudio-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-samplerate-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-speex-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-upmix-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-usbstream-1.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-plugins-vdownmix-1.1.9-1.el8.x86_64.rpm \
          "

SRC_URI[alsa-plugins-arcamav.sha256sum] = "bb7c8b0f0e3288fb71ea2a91dca33145c93b338bccc113eb2721d821d798bc7d"
SRC_URI[alsa-plugins-maemo.sha256sum] = "62b69a5c33aedf15ac5a34d4af5638d67e60930f2686692673adb4caa1997f03"
SRC_URI[alsa-plugins-oss.sha256sum] = "b65195f767144a5ee0e67ed6505c1debb4c5cfab5f68fbad189f990575f21816"
SRC_URI[alsa-plugins-pulseaudio.sha256sum] = "a870db3bceeeba7f96a9f04265b8c8359629f0bb3066e68464e399d88001ae52"
SRC_URI[alsa-plugins-samplerate.sha256sum] = "f4551d494a5a31bb0611654306b5f69d8d4bc05c8d69f5935eb5516235f0aca8"
SRC_URI[alsa-plugins-speex.sha256sum] = "87a2eed051ff45ca23fbffb788abdb980386dbaec82571401a38bb66c07045ed"
SRC_URI[alsa-plugins-upmix.sha256sum] = "eeac18234ff7e919658b7510e2b7e2299b7e7e1dbc2896e980f3bca622e89859"
SRC_URI[alsa-plugins-usbstream.sha256sum] = "57015e5305c435ff632b330ff5ba43a44aac8000eeb50af7887ce0fbb0a563bf"
SRC_URI[alsa-plugins-vdownmix.sha256sum] = "a32a8c6a29dbb49c06da9939ab56d5bf9fccb302fe3b3a385066627d326307a6"
