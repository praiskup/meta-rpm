SUMMARY = "generated recipe based on rrdtool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo glib-2.0 libpng libxml2 lua pango perl pkgconfig-native platform-python3 ruby"
RPM_SONAME_REQ_python3-rrdtool = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 librrd.so.8"
RDEPENDS_python3-rrdtool = "glibc platform-python python3-libs rrdtool"
RPM_SONAME_PROV_rrdtool = "librrd.so.8"
RPM_SONAME_REQ_rrdtool = "libc.so.6 libcairo.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librrd.so.8 libxml2.so.2"
RDEPENDS_rrdtool = "bash cairo dejavu-sans-mono-fonts glib2 glibc libpng libxml2 pango systemd"
RPM_SONAME_REQ_rrdtool-devel = "librrd.so.8"
RPROVIDES_rrdtool-devel = "rrdtool-dev (= 1.7.0)"
RDEPENDS_rrdtool-devel = "pkgconf-pkg-config rrdtool"
RPM_SONAME_REQ_rrdtool-lua = "libc.so.6 libcairo.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 liblua-5.3.so libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librrd.so.8 libxml2.so.2"
RDEPENDS_rrdtool-lua = "cairo glib2 glibc libpng libxml2 lua-libs pango rrdtool"
RPM_SONAME_REQ_rrdtool-perl = "libc.so.6 libperl.so.5.26 libpthread.so.0 librrd.so.8"
RDEPENDS_rrdtool-perl = "glibc perl-Carp perl-IO perl-interpreter perl-libs rrdtool"
RPM_SONAME_REQ_rrdtool-ruby = "libc.so.6 libruby.so.2.5"
RDEPENDS_rrdtool-ruby = "glibc rrdtool ruby-libs"
RPM_SONAME_REQ_rrdtool-tcl = "libc.so.6 libm.so.6 librrd.so.8"
RDEPENDS_rrdtool-tcl = "glibc rrdtool tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rrdtool-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rrdtool-perl-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-rrdtool-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rrdtool-devel-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rrdtool-doc-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rrdtool-lua-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rrdtool-ruby-1.7.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rrdtool-tcl-1.7.0-16.el8.x86_64.rpm \
          "

SRC_URI[python3-rrdtool.sha256sum] = "bc6b15611b9cee1c974950fbcb79fa6d37a5c3d3c135ccc8db442e8623347a5e"
SRC_URI[rrdtool.sha256sum] = "8414a0a3be27946f488d1e84cc171e1c5ce12d0b7e19c22162ee35f32b2de5a0"
SRC_URI[rrdtool-devel.sha256sum] = "19b052e8e90a2699ab244d6009de3c5d31eec8def9b40b8046ad0f1b8f1a0259"
SRC_URI[rrdtool-doc.sha256sum] = "63add1873f6ccc8d56120007f2cd32e51396e2cbd884c649b630f822eb9a6cd5"
SRC_URI[rrdtool-lua.sha256sum] = "6aae978bdbc9b3f7f6178897568433795a0dc2327f296e1d1e4e11489cda2a9c"
SRC_URI[rrdtool-perl.sha256sum] = "a411dcdd0837b8135237d45c70fba04a0da00994e55ce2b95526165cec0f589e"
SRC_URI[rrdtool-ruby.sha256sum] = "a28863f14891eaa3d6150440c22539d62ff7953e4636c69c43d9387b84616349"
SRC_URI[rrdtool-tcl.sha256sum] = "0138787c888341fa54fe7517c595a3a8b1ea6b3e3ea965e79cb169c17cecf378"
