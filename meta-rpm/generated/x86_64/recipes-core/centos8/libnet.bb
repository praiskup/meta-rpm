SUMMARY = "generated recipe based on libnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libnet = "libnet.so.1"
RPM_SONAME_REQ_libnet = "libc.so.6"
RDEPENDS_libnet = "glibc"
RPM_SONAME_REQ_libnet-devel = "libnet.so.1"
RPROVIDES_libnet-devel = "libnet-dev (= 1.1.6)"
RDEPENDS_libnet-devel = "bash libnet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libnet-1.1.6-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnet-devel-1.1.6-15.el8.x86_64.rpm \
          "

SRC_URI[libnet.sha256sum] = "9bc5626feb9cccf0778c1377a9701e3a0419feff34e127d3ad0fba0fc849950c"
SRC_URI[libnet-devel.sha256sum] = "a099b70e13ea27932134dd3f9366a8b810f98b8528cf6f566bf5b83feeedbc78"
