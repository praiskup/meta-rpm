SUMMARY = "generated recipe based on libdmx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libdmx = "libdmx.so.1"
RPM_SONAME_REQ_libdmx = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libdmx = "glibc libX11 libXext"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdmx-1.1.4-3.el8.x86_64.rpm \
          "

SRC_URI[libdmx.sha256sum] = "a4a15b73638d802dd3878bfc51e50e4d39050d7fdd05f80d19389bcff98b2844"
