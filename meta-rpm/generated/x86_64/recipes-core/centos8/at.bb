SUMMARY = "generated recipe based on at srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux pam pkgconfig-native"
RPM_SONAME_REQ_at = "libc.so.6 libpam.so.0 libpam_misc.so.0 librt.so.1 libselinux.so.1"
RDEPENDS_at = "bash glibc libselinux pam systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/at-3.1.20-11.el8.x86_64.rpm \
          "

SRC_URI[at.sha256sum] = "4f9ac6dba172a6075bff90549767a671be43ed7bbfa847d747380e97e7fa254b"
