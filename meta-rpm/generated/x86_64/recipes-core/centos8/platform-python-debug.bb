SUMMARY = "generated recipe based on python3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 expat gdbm libffi libnsl2 libtirpc libx11 libxcrypt ncurses openssl pkgconfig-native readline sqlite3 tcl tk xz zlib"
RPM_SONAME_PROV_platform-python-debug = "libpython3.6dm.so.1.0"
RPM_SONAME_REQ_platform-python-debug = "libX11.so.6 libbz2.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libexpat.so.1 libffi.so.6 libgdbm.so.6 libgdbm_compat.so.4 liblzma.so.5 libm.so.6 libncursesw.so.6 libnsl.so.2 libpanelw.so.6 libpthread.so.0 libpython3.6dm.so.1.0 libreadline.so.7 libsqlite3.so.0 libssl.so.1.1 libtcl8.6.so libtinfo.so.6 libtirpc.so.3 libtk8.6.so libutil.so.1 libz.so.1"
RDEPENDS_platform-python-debug = "bash bzip2-libs expat gdbm-libs glibc libX11 libffi libnsl2 libtirpc libxcrypt ncurses-libs openssl-libs pkgconf-pkg-config platform-python platform-python-devel python3-idle python3-libs python3-test python3-tkinter readline sqlite-libs tcl tk xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/platform-python-debug-3.6.8-23.el8.x86_64.rpm \
          "

SRC_URI[platform-python-debug.sha256sum] = "f6ce1e2807647076b88b671ffc78bb2d94f1173ef90aaf8a40a15ef44da2e8fe"
