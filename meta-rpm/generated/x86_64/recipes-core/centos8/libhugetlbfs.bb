SUMMARY = "generated recipe based on libhugetlbfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libhugetlbfs = "libhugetlbfs.so libhugetlbfs_privutils.so"
RPM_SONAME_REQ_libhugetlbfs = "libc.so.6"
RDEPENDS_libhugetlbfs = "bash glibc"
RPROVIDES_libhugetlbfs-devel = "libhugetlbfs-dev (= 2.21)"
RDEPENDS_libhugetlbfs-devel = "libhugetlbfs"
RPM_SONAME_REQ_libhugetlbfs-utils = "libc.so.6"
RDEPENDS_libhugetlbfs-utils = "glibc libhugetlbfs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libhugetlbfs-2.21-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libhugetlbfs-devel-2.21-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libhugetlbfs-utils-2.21-12.el8.x86_64.rpm \
          "

SRC_URI[libhugetlbfs.sha256sum] = "3c687849bd3a0fee5cfdc7f489c2eead7ca398fb2e41dffadce73d89212a31b1"
SRC_URI[libhugetlbfs-devel.sha256sum] = "fc99c1eadd8afff789785b65a9ba3d9a2f01b6f0227b6c06e3b054d93911058a"
SRC_URI[libhugetlbfs-utils.sha256sum] = "03bacd5cffa38df8bf43e07f4c1153897b75a594ddab03876868ffcf8e568681"
