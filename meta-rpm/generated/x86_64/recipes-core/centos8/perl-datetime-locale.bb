SUMMARY = "generated recipe based on perl-DateTime-Locale srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Locale = "perl-Carp perl-Dist-CheckConflicts perl-Exporter perl-File-ShareDir perl-Params-ValidationCompiler perl-Scalar-List-Utils perl-Specio perl-libs perl-namespace-autoclean"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-Locale-1.17-2.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Locale.sha256sum] = "b021ad0860fd08ebf7c0afeb02753b3047064d2fa98331c4f57cb2cf68cf4f59"
