SUMMARY = "generated recipe based on perl-Term-Table srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Table = "perl-Carp perl-Importer perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Term-Table-0.012-2.el8.noarch.rpm \
          "

SRC_URI[perl-Term-Table.sha256sum] = "8c76a472e23c5ce86569803717e871428d412b0a6ff3c73416b678e958ea26e7"
