SUMMARY = "generated recipe based on perl-Module-Load srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Load = "perl-PathTools perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Module-Load-0.32-395.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Load.sha256sum] = "b9231ba23f032fe210cd63e2e3f63e6020be05f8b7e977c616d37adf0413268c"
