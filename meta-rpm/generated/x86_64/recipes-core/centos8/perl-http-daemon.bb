SUMMARY = "generated recipe based on perl-HTTP-Daemon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Daemon = "perl-Carp perl-HTTP-Date perl-HTTP-Message perl-IO-Socket-IP perl-LWP-MediaTypes perl-Socket perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-HTTP-Daemon-6.01-23.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Daemon.sha256sum] = "d64e37f0a9f7729d63ee867cfa92221216cedc5360562a442a7914065541fad7"
