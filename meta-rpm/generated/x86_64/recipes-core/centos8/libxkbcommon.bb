SUMMARY = "generated recipe based on libxkbcommon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_libxkbcommon = "libxkbcommon.so.0"
RPM_SONAME_REQ_libxkbcommon = "libc.so.6"
RDEPENDS_libxkbcommon = "glibc xkeyboard-config"
RPM_SONAME_REQ_libxkbcommon-devel = "libxkbcommon.so.0"
RPROVIDES_libxkbcommon-devel = "libxkbcommon-dev (= 0.9.1)"
RDEPENDS_libxkbcommon-devel = "libxkbcommon pkgconf-pkg-config"
RPM_SONAME_PROV_libxkbcommon-x11 = "libxkbcommon-x11.so.0"
RPM_SONAME_REQ_libxkbcommon-x11 = "libc.so.6 libxcb-xkb.so.1 libxcb.so.1 libxkbcommon.so.0"
RDEPENDS_libxkbcommon-x11 = "glibc libxcb libxkbcommon"
RPM_SONAME_REQ_libxkbcommon-x11-devel = "libxkbcommon-x11.so.0"
RPROVIDES_libxkbcommon-x11-devel = "libxkbcommon-x11-dev (= 0.9.1)"
RDEPENDS_libxkbcommon-x11-devel = "libxcb-devel libxkbcommon-devel libxkbcommon-x11 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxkbcommon-0.9.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxkbcommon-devel-0.9.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxkbcommon-x11-0.9.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libxkbcommon-x11-devel-0.9.1-1.el8.x86_64.rpm \
          "

SRC_URI[libxkbcommon.sha256sum] = "e03d462995326a4477dcebc8c12eae3c1776ce2f095617ace253c0c492c89082"
SRC_URI[libxkbcommon-devel.sha256sum] = "1c2d902bc73951f80081ca9bddcad4c00129da263c09ed23db3614af34c19fe8"
SRC_URI[libxkbcommon-x11.sha256sum] = "2b4ae72021189492aad9a43e317263863be41f698862c5ec000b887727fe7cdc"
SRC_URI[libxkbcommon-x11-devel.sha256sum] = "2872db8a71491791a391fe53d7bc4680d9607ce095e28c78b188fe2ac7d44822"
