SUMMARY = "generated recipe based on libpng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconf pkgconfig-native zlib"
RPM_SONAME_PROV_libpng = "libpng16.so.16"
RPM_SONAME_REQ_libpng = "libc.so.6 libm.so.6 libz.so.1"
RDEPENDS_libpng = "glibc zlib"
RPM_SONAME_REQ_libpng-devel = "libc.so.6 libm.so.6 libpng16.so.16 libz.so.1"
RPROVIDES_libpng-devel = "libpng-dev (= 1.6.34)"
RDEPENDS_libpng-devel = "bash glibc libpng pkgconf-pkg-config zlib zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpng-1.6.34-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpng-devel-1.6.34-5.el8.x86_64.rpm \
          "

SRC_URI[libpng.sha256sum] = "cc2f054cf7ef006faf0b179701838ff8632c3ac5f45a0199a13f9c237f632b82"
SRC_URI[libpng-devel.sha256sum] = "a8cbe67a2f2ccb36bb765408686f9266204dea472a8b7b9120f23bbbc2c732a4"
