SUMMARY = "generated recipe based on chkconfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/update-alternatives"
DEPENDS = "libnewt libselinux libsepol pkgconfig-native popt"
RPM_SONAME_REQ_chkconfig = "libc.so.6 libpopt.so.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_chkconfig = "glibc libselinux libsepol popt"
RPM_SONAME_REQ_ntsysv = "libc.so.6 libnewt.so.0.52 libpopt.so.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_ntsysv = "chkconfig glibc libselinux libsepol newt popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/chkconfig-1.11-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ntsysv-1.11-1.el8.x86_64.rpm \
          "

SRC_URI[chkconfig.sha256sum] = "69bdf0e650a4a1dde5e7f217be7f082d10d14e2692859805b7732cb3daf02a9d"
SRC_URI[ntsysv.sha256sum] = "8e69a6ee9fe914593ed2b54eda455d12a57d6da6d614c49cda23db9010e65581"
