SUMMARY = "generated recipe based on tuna srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_tuna = "platform-python python3-ethtool python3-linux-procfs python3-schedutils"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tuna-0.14-4.el8.noarch.rpm \
          "

SRC_URI[tuna.sha256sum] = "e65fa2bbc4b9049d71d4d1bd3272f7bbb3d72debf56a0f02d686712dc07f7f49"
