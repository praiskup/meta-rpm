SUMMARY = "generated recipe based on perl-Text-Template srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Template = "perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Text-Template-1.51-1.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Template.sha256sum] = "e08a91a671d285d9508a18fca38e362bff72e561337b9cbab66eb1b76d5faa2c"
