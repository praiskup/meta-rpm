SUMMARY = "generated recipe based on iw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native"
RPM_SONAME_REQ_iw = "libc.so.6 libnl-3.so.200 libnl-genl-3.so.200"
RDEPENDS_iw = "glibc libnl3"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iw-4.14-5.el8.x86_64.rpm \
          "

SRC_URI[iw.sha256sum] = "ccb353529f5802dcbbc21de0b4bf2ba8a12a8b75c3d0d8ffd63d965647399d46"
