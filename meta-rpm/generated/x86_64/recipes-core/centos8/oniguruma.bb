SUMMARY = "generated recipe based on oniguruma srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_oniguruma = "libonig.so.5"
RPM_SONAME_REQ_oniguruma = "libc.so.6"
RDEPENDS_oniguruma = "glibc"
RPM_SONAME_REQ_oniguruma-devel = "libonig.so.5"
RPROVIDES_oniguruma-devel = "oniguruma-dev (= 6.8.2)"
RDEPENDS_oniguruma-devel = "bash oniguruma pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/oniguruma-6.8.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/oniguruma-devel-6.8.2-1.el8.x86_64.rpm \
          "

SRC_URI[oniguruma.sha256sum] = "e80fc50af1041571029cdcd2ad9e1e38edec72d776d1ed5b30a405cfd3ac95cc"
SRC_URI[oniguruma-devel.sha256sum] = "2348fe349d0fd8010a8c29a5a9ae4bf53d5cb7a74e003009e4bbac60282a3aab"
