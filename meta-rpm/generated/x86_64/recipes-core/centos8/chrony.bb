SUMMARY = "generated recipe based on chrony srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap libedit libseccomp nettle pkgconfig-native"
RPM_SONAME_REQ_chrony = "libc.so.6 libcap.so.2 libedit.so.0 libm.so.6 libnettle.so.6 libpthread.so.0 libseccomp.so.2"
RDEPENDS_chrony = "bash glibc libcap libedit libseccomp nettle shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/chrony-3.5-1.el8.x86_64.rpm \
          "

SRC_URI[chrony.sha256sum] = "5f3d3d3b27b7df75738d1ee31112c60d39c54b8d7f92b6900606187f6cffce1d"
