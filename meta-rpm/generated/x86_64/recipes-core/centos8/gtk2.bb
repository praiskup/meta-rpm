SUMMARY = "generated recipe based on gtk2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo cups-libs fontconfig freetype gdk-pixbuf glib-2.0 libx11 libxcomposite libxcursor libxdamage libxext libxfixes libxi libxinerama libxrandr libxrender pango pkgconfig-native"
RPM_SONAME_PROV_gtk2 = "libgailutil.so.18 libgdk-x11-2.0.so.0 libgtk-x11-2.0.so.0"
RPM_SONAME_REQ_gtk2 = "libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcups.so.2 libfontconfig.so.1 libfreetype.so.6 libgailutil.so.18 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk2 = "atk bash cairo cups-libs fontconfig freetype gdk-pixbuf2 gdk-pixbuf2-modules glib2 glibc gtk-update-icon-cache hicolor-icon-theme libX11 libXcomposite libXcursor libXdamage libXext libXfixes libXi libXinerama libXrandr libXrender libtiff pango"
RPM_SONAME_REQ_gtk2-devel = "libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgailutil.so.18 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RPROVIDES_gtk2-devel = "gtk2-dev (= 2.24.32)"
RDEPENDS_gtk2-devel = "atk atk-devel cairo cairo-devel fontconfig freetype gdk-pixbuf2 gdk-pixbuf2-devel glib2 glib2-devel glibc gtk2 libX11 libX11-devel libXcomposite libXcomposite-devel libXcursor libXcursor-devel libXdamage libXext libXext-devel libXfixes libXfixes-devel libXi libXi-devel libXinerama libXinerama-devel libXrandr libXrandr-devel libXrender pango pango-devel pkgconf-pkg-config platform-python"
RDEPENDS_gtk2-devel-docs = "gtk2"
RPM_SONAME_REQ_gtk2-immodule-xim = "libX11.so.6 libc.so.6 libgdk-x11-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk2-immodule-xim = "glib2 glibc gtk2 libX11 pango"
RPM_SONAME_REQ_gtk2-immodules = "libc.so.6 libgdk-x11-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk2-immodules = "glib2 glibc gtk2 pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtk2-2.24.32-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtk2-devel-2.24.32-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtk2-devel-docs-2.24.32-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtk2-immodule-xim-2.24.32-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtk2-immodules-2.24.32-4.el8.x86_64.rpm \
          "

SRC_URI[gtk2.sha256sum] = "2eda2904c9e683b590f6dd4fd8db69e06809aeb066181d4e9ca093b912d6d702"
SRC_URI[gtk2-devel.sha256sum] = "13b48468033c5162d465efde0a7b61e7667f53825ae6ceab8280934bf808acdd"
SRC_URI[gtk2-devel-docs.sha256sum] = "b4cd66a2f7340f59f46a8f9c7f3b4bff32f91f885dd7cffeea68ee8231275049"
SRC_URI[gtk2-immodule-xim.sha256sum] = "16eef61280ce611616b8b74f7fcafce21d63b5da73af08f839d822ef9ce74511"
SRC_URI[gtk2-immodules.sha256sum] = "3dbd4f9377952d2fa03125106e4685594e89c1eeb40c019392a9d76a854d0d84"
