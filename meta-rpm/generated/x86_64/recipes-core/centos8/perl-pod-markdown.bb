SUMMARY = "generated recipe based on perl-Pod-Markdown srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Markdown = "perl-Encode perl-Getopt-Long perl-Pod-Simple perl-Pod-Usage perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Pod-Markdown-3.005-6.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Markdown.sha256sum] = "d29f7a8ad70d5b574941fbf5cab8405050d0024315a55cc2a1db8a6246a70337"
