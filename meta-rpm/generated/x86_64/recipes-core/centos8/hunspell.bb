SUMMARY = "generated recipe based on hunspell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc ncurses pkgconfig-native"
RPM_SONAME_PROV_hunspell = "libhunspell-1.6.so.0"
RPM_SONAME_REQ_hunspell = "libc.so.6 libgcc_s.so.1 libhunspell-1.6.so.0 libm.so.6 libncursesw.so.6 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_hunspell = "glibc hunspell-en-US libgcc libstdc++ ncurses-libs"
RPM_SONAME_REQ_hunspell-devel = "libc.so.6 libgcc_s.so.1 libhunspell-1.6.so.0 libm.so.6 libstdc++.so.6"
RPROVIDES_hunspell-devel = "hunspell-dev (= 1.6.2)"
RDEPENDS_hunspell-devel = "bash glibc hunspell libgcc libstdc++ perl-Getopt-Long perl-interpreter pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-1.6.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-devel-1.6.2-1.el8.x86_64.rpm \
          "

SRC_URI[hunspell.sha256sum] = "ad4d0bb25b1fe754f288a854c4ee24845ff1207d34ea5345b5dba322d5dfc862"
SRC_URI[hunspell-devel.sha256sum] = "7929243b71e519e0553cccb906ef4b1762185b309862f067c816f84a380c73cd"
