SUMMARY = "generated recipe based on python-justbases srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-justbases = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-justbases-0.9-6.el8.noarch.rpm \
          "

SRC_URI[python3-justbases.sha256sum] = "5bb1ac7b4a9273274c8c7fa36588b4a97ed9adb85f9d77e7a52f8a0227bb5bdc"
