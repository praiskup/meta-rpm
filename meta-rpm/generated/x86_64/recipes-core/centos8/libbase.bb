SUMMARY = "generated recipe based on libbase srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_libbase = "apache-commons-logging java-1.8.0-openjdk-headless javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libbase-1.1.3-18.el8.noarch.rpm \
          "

SRC_URI[libbase.sha256sum] = "34c4b0a9756ca3da0afb5d7a345e820cdb23b4bc12e45d86e6021b6c95ab0f37"
