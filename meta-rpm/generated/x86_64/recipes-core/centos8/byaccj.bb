SUMMARY = "generated recipe based on byaccj srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_byaccj = "libc.so.6"
RDEPENDS_byaccj = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/byaccj-1.15-17.module_el8.0.0+30+832da3a1.x86_64.rpm \
          "

SRC_URI[byaccj.sha256sum] = "48c11fe485643110e15d38412b9c33e7e31e2c0436fabb5a972630ea72c4b082"
