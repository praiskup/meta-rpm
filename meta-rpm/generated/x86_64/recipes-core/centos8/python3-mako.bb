SUMMARY = "generated recipe based on python-mako srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-mako = "platform-python python3-markupsafe"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-mako-1.0.6-13.el8.noarch.rpm \
          "

SRC_URI[python3-mako.sha256sum] = "9dc5c484c7821cc737f872973764203376106d643b9adc4ef66bece1b0cc8fdc"
