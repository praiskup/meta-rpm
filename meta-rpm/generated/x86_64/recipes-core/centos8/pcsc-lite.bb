SUMMARY = "generated recipe based on pcsc-lite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_pcsc-lite = "libc.so.6 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 librt.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_pcsc-lite = "bash glib2 glibc pcsc-lite-ccid pcsc-lite-libs polkit polkit-libs systemd systemd-libs"
RPM_SONAME_PROV_pcsc-lite-devel = "libpcscspy.so.0"
RPM_SONAME_REQ_pcsc-lite-devel = "libc.so.6 libdl.so.2 libpcsclite.so.1 libpcscspy.so.0 librt.so.1"
RPROVIDES_pcsc-lite-devel = "pcsc-lite-dev (= 1.8.23)"
RDEPENDS_pcsc-lite-devel = "glibc pcsc-lite-libs pkgconf-pkg-config platform-python"
RDEPENDS_pcsc-lite-doc = "pcsc-lite-libs"
RPM_SONAME_PROV_pcsc-lite-libs = "libpcsclite.so.1"
RPM_SONAME_REQ_pcsc-lite-libs = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_pcsc-lite-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcsc-lite-1.8.23-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcsc-lite-doc-1.8.23-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pcsc-lite-libs-1.8.23-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pcsc-lite-devel-1.8.23-3.el8.x86_64.rpm \
          "

SRC_URI[pcsc-lite.sha256sum] = "83bc70118145830161301d2bd4995c97cdc8d951637405caf908133282e86c87"
SRC_URI[pcsc-lite-devel.sha256sum] = "c71c181b1cc9f020845be0515521f92e602e8821c804b9c0d1eddf14955ee592"
SRC_URI[pcsc-lite-doc.sha256sum] = "683982b7e6db44d488bb46b878771d9274ecad9fe3e31305abdeb2fb3335aae7"
SRC_URI[pcsc-lite-libs.sha256sum] = "f48b0ba7c69e7ea25eb8700195769f8a846cd5d6af9439d74924cfcc27ca552c"
