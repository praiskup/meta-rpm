SUMMARY = "generated recipe based on perl-Encode-Locale srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Encode-Locale = "perl-Encode perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Encode-Locale-1.05-9.el8.noarch.rpm \
          "

SRC_URI[perl-Encode-Locale.sha256sum] = "5e1927ff5521cc9a8b261e4051b23c4db252ede78fdc6632a8b95af9771e7054"
