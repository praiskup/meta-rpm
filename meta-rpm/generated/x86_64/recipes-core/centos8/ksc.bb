SUMMARY = "generated recipe based on ksc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ksc = "binutils kernel-abi-whitelists kernel-devel kmod platform-python python3-magic python3-requests"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ksc-1.6-2.el8.noarch.rpm \
          "

SRC_URI[ksc.sha256sum] = "b814023af9a6f1cba8eedf5a38e556fc4a75df75e6be12a7ee373a1ce0eef515"
