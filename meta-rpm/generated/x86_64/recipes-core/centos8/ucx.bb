SUMMARY = "generated recipe based on ucx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc numactl pkgconfig-native"
RPM_SONAME_PROV_ucx = "libucm.so.0 libucp.so.0 libucs.so.0 libuct.so.0"
RPM_SONAME_REQ_ucx = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgomp.so.1 libm.so.6 libnuma.so.1 libpthread.so.0 librt.so.1 libucm.so.0 libucp.so.0 libucs.so.0 libuct.so.0"
RDEPENDS_ucx = "glibc libgcc libgomp numactl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ucx-1.6.1-1.el8.x86_64.rpm \
          "

SRC_URI[ucx.sha256sum] = "df6f36a9b66cf9f7dea7484a9782a80a9b4695ca3adbf2de2663250789536cd0"
