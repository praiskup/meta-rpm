SUMMARY = "generated recipe based on libkeepalive srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libkeepalive = "libkeepalive.so"
RPM_SONAME_REQ_libkeepalive = "libc.so.6 libdl.so.2"
RDEPENDS_libkeepalive = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libkeepalive-0.3-8.el8.x86_64.rpm \
          "

SRC_URI[libkeepalive.sha256sum] = "c0c5bac09667f7ebc2bd7f2024be3c803104ea6dfc2b40e7298cdcc3fc9bced3"
