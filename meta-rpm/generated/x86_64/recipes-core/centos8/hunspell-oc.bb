SUMMARY = "generated recipe based on hunspell-oc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-oc = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-oc-0.6.2-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-oc.sha256sum] = "91d0e43bdca519ad05b6a60472ec24f20b730ad5cced6169937b0a54abeac126"
