SUMMARY = "generated recipe based on mythes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_mythes = "libmythes-1.2.so.0"
RPM_SONAME_REQ_mythes = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_mythes = "glibc libgcc libstdc++"
RPM_SONAME_REQ_mythes-devel = "libmythes-1.2.so.0"
RPROVIDES_mythes-devel = "mythes-dev (= 1.2.4)"
RDEPENDS_mythes-devel = "mythes perl-interpreter pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-1.2.4-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mythes-devel-1.2.4-9.el8.x86_64.rpm \
          "

SRC_URI[mythes.sha256sum] = "2b3759f7f084ac3fcabef456eb9dc781ac8a44917bb612971a686a9bf5234464"
SRC_URI[mythes-devel.sha256sum] = "d6ca8a285ac0e6551df0868d1aae5aa42006536e8b2fb3653e89b9d294559580"
