SUMMARY = "generated recipe based on cgdcbxd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcgroup libmnl pkgconfig-native"
RPM_SONAME_REQ_cgdcbxd = "libc.so.6 libcgroup.so.1 libmnl.so.0 librt.so.1"
RDEPENDS_cgdcbxd = "bash glibc libcgroup libmnl systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cgdcbxd-1.0.2-9.el8.x86_64.rpm \
          "

SRC_URI[cgdcbxd.sha256sum] = "09ce6c46ec03b0c2b8f3487907e0c8402f594935dd26e31b3db3834d0c4e3f2a"
