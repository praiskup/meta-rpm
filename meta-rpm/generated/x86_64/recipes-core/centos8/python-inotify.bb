SUMMARY = "generated recipe based on python-inotify srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-inotify = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-inotify-0.9.6-13.el8.noarch.rpm \
          "

SRC_URI[python3-inotify.sha256sum] = "109d6db30e5515601b00954b2fd8750c421730a18afc12e1fa7781e24e5b0863"
