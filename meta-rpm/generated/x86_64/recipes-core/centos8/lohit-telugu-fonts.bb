SUMMARY = "generated recipe based on lohit-telugu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-telugu-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-telugu-fonts-2.5.5-3.el8.noarch.rpm \
          "

SRC_URI[lohit-telugu-fonts.sha256sum] = "ff29144a302bff8939e77ee9d489d32ed97819df92188106f3e7cd0cccc1a17a"
