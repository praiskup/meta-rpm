SUMMARY = "generated recipe based on libmodulemd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "file glib-2.0 libgcc libyaml pkgconfig-native rpm"
RPM_SONAME_PROV_libmodulemd = "libmodulemd.so.2"
RPM_SONAME_REQ_libmodulemd = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libmagic.so.1 libmodulemd.so.2 librpmio.so.8 libyaml-0.so.2"
RDEPENDS_libmodulemd = "file-libs glib2 glibc libgcc libyaml rpm-libs"
RPM_SONAME_REQ_libmodulemd-devel = "libmodulemd.so.2"
RPROVIDES_libmodulemd-devel = "libmodulemd-dev (= 2.8.2)"
RDEPENDS_libmodulemd-devel = "glib2-devel libmodulemd libyaml-devel pkgconf-pkg-config rpm-devel"
RPM_SONAME_PROV_libmodulemd1 = "libmodulemd.so.1"
RPM_SONAME_REQ_libmodulemd1 = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libmodulemd.so.1 libyaml-0.so.2"
RDEPENDS_libmodulemd1 = "glib2 glibc libgcc libyaml"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmodulemd-2.8.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmodulemd1-1.8.16-0.2.8.2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmodulemd-devel-2.8.2-1.el8.x86_64.rpm \
          "

SRC_URI[libmodulemd.sha256sum] = "ce97a04b86ac76eba658f5281ca37aa61c0c91c005ea789dbd686b3a089555b6"
SRC_URI[libmodulemd-devel.sha256sum] = "22be2973aeb06f2caec13e8a280ac0b6a78a350828f49583a5773017f5918554"
SRC_URI[libmodulemd1.sha256sum] = "0f6c6f87909eabe38cdf000612eb978b870d5fa172ac0eb95ef7104c5d7415b0"
