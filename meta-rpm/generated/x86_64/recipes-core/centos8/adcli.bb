SUMMARY = "generated recipe based on adcli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs openldap pkgconfig-native"
RPM_SONAME_REQ_adcli = "libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libkrb5support.so.0 liblber-2.4.so.2 libldap-2.4.so.2 libresolv.so.2"
RDEPENDS_adcli = "cyrus-sasl-gssapi glibc krb5-libs libcom_err openldap"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/adcli-0.8.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/adcli-doc-0.8.2-5.el8.noarch.rpm \
          "

SRC_URI[adcli.sha256sum] = "90a1bbcb93489a2518daf933d9755c11633fc071c70016cc6e479dda649378dc"
SRC_URI[adcli-doc.sha256sum] = "350124969ca8af37ca97c6d25677c04ec8b962a85bcf9eec9e9ed1fdbe6df268"
