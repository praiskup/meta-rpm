SUMMARY = "generated recipe based on xdg-desktop-portal-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcc pango pkgconfig-native"
RPM_SONAME_REQ_xdg-desktop-portal-gtk = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_xdg-desktop-portal-gtk = "atk bash cairo cairo-gobject dbus gdk-pixbuf2 glib2 glibc gtk3 libgcc pango systemd xdg-desktop-portal"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xdg-desktop-portal-gtk-1.4.0-1.el8.x86_64.rpm \
          "

SRC_URI[xdg-desktop-portal-gtk.sha256sum] = "d447f8e6f69fc45e0a80a612e4a3e62874a814b574e3e46f21e337e52338a1cf"
