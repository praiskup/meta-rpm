SUMMARY = "generated recipe based on gnome-online-miners srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gfbgraph glib-2.0 gnome-online-accounts grilo json-glib libgdata libsoup-2.4 libxml2 pkgconfig-native rest tracker"
RPM_SONAME_REQ_gnome-online-miners = "libc.so.6 libgdata.so.22 libgfbgraph-0.2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgrilo-0.3.so.0 libjson-glib-1.0.so.0 libpthread.so.0 librest-0.7.so.0 libsoup-2.4.so.1 libtracker-miner-2.0.so.0 libtracker-sparql-2.0.so.0 libxml2.so.2"
RDEPENDS_gnome-online-miners = "dbus gfbgraph glib2 glibc gnome-online-accounts grilo grilo-plugins gvfs json-glib libgdata libsoup libxml2 rest tracker"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-online-miners-3.26.0-3.el8.x86_64.rpm \
          "

SRC_URI[gnome-online-miners.sha256sum] = "51eb9ab3ce30e3ca72905f9086d81d7388814b683aa36c7c62a25e4a73248e39"
