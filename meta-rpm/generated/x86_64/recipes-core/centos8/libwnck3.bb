SUMMARY = "generated recipe based on libwnck3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libx11 libxext libxrender libxres pango pkgconfig-native startup-notification"
RPM_SONAME_PROV_libwnck3 = "libwnck-3.so.0"
RPM_SONAME_REQ_libwnck3 = "libX11.so.6 libXRes.so.1 libXext.so.6 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libstartup-notification-1.so.0 libwnck-3.so.0"
RDEPENDS_libwnck3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libXext libXrender libXres pango startup-notification"
RPM_SONAME_REQ_libwnck3-devel = "libX11.so.6 libXRes.so.1 libXext.so.6 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libstartup-notification-1.so.0 libwnck-3.so.0"
RPROVIDES_libwnck3-devel = "libwnck3-dev (= 3.24.1)"
RDEPENDS_libwnck3-devel = "atk cairo cairo-devel cairo-gobject gdk-pixbuf2 glib2 glib2-devel glibc gtk3 gtk3-devel libX11 libX11-devel libXext libXrender libXres libXres-devel libwnck3 pango pango-devel pkgconf-pkg-config startup-notification startup-notification-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwnck3-3.24.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwnck3-devel-3.24.1-2.el8.x86_64.rpm \
          "

SRC_URI[libwnck3.sha256sum] = "750cf000523dbc4545a69505c2d4600d056fbd1e590fd1eb5b7aa1978e6d8c94"
SRC_URI[libwnck3-devel.sha256sum] = "d2696813588326d10fc7b90b861ca4d481ffa90e55587505f4320b744a28897a"
