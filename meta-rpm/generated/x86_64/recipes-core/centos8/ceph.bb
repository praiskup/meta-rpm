SUMMARY = "generated recipe based on ceph srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost libblkid libgcc nspr nss pkgconfig-native systemd-libs"
RPM_SONAME_REQ_libcephfs-devel = "libcephfs.so.2"
RPROVIDES_libcephfs-devel = "libcephfs-dev (= 12.2.7)"
RDEPENDS_libcephfs-devel = "libcephfs2 librados-devel"
RPM_SONAME_PROV_libcephfs2 = "libcephfs.so.2"
RPM_SONAME_REQ_libcephfs2 = "ld-linux-x86-64.so.2 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_libcephfs2 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util"
RPM_SONAME_REQ_librados-devel = "libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 librados.so.2 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RPROVIDES_librados-devel = "librados-dev (= 12.2.7)"
RDEPENDS_librados-devel = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util"
RPM_SONAME_PROV_librados2 = "libceph-common.so.0 librados.so.2"
RPM_SONAME_REQ_librados2 = "ld-linux-x86-64.so.2 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_librados2 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc libstdc++ nspr nss nss-util"
RPM_SONAME_REQ_libradosstriper-devel = "libradosstriper.so.1"
RPROVIDES_libradosstriper-devel = "libradosstriper-dev (= 12.2.7)"
RDEPENDS_libradosstriper-devel = "librados-devel libradosstriper1"
RPM_SONAME_PROV_libradosstriper1 = "libradosstriper.so.1"
RPM_SONAME_REQ_libradosstriper1 = "ld-linux-x86-64.so.2 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 librados.so.2 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_libradosstriper1 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util"
RPM_SONAME_REQ_librbd-devel = "librbd.so.1"
RPROVIDES_librbd-devel = "librbd-dev (= 12.2.7)"
RDEPENDS_librbd-devel = "librados-devel librbd1"
RPM_SONAME_PROV_librbd1 = "librbd.so.1"
RPM_SONAME_REQ_librbd1 = "ld-linux-x86-64.so.2 libblkid.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libceph-common.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 librados.so.2 libresolv.so.2 librt.so.1 libsmime3.so libssl3.so libstdc++.so.6 libudev.so.1"
RDEPENDS_librbd1 = "boost-atomic boost-chrono boost-date-time boost-iostreams boost-program-options boost-random boost-regex boost-system boost-thread glibc libblkid libgcc librados2 libstdc++ nspr nss nss-util systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librados2-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librbd1-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcephfs-devel-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcephfs2-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librados-devel-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libradosstriper-devel-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libradosstriper1-12.2.7-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librbd-devel-12.2.7-9.el8.x86_64.rpm \
          "

SRC_URI[libcephfs-devel.sha256sum] = "e45f05a7a19d60507a8989ed2e502ff4dbd1ae0cbc2322613b173029285f3b96"
SRC_URI[libcephfs2.sha256sum] = "90f2e5a1860db659e9fd30bed29b30f13b3de9f603214c2b4fd87b1165cc0614"
SRC_URI[librados-devel.sha256sum] = "339c933702a461677b28e96dbb08a31b0067ec80bdb3037e241c7855194f1078"
SRC_URI[librados2.sha256sum] = "26fc737517bc0b60150e662337000007299d7579376370bc9b907a7fe446a3f0"
SRC_URI[libradosstriper-devel.sha256sum] = "dc3fb3ee745f60f23b2889f7a80cc93167f971cef53b0d56841d89f0b5ab4d19"
SRC_URI[libradosstriper1.sha256sum] = "71b62fa410973a2d7a24ff0b7966429c1b6b256b09cf2533eea4431110be8325"
SRC_URI[librbd-devel.sha256sum] = "1cec503eb59bf65163ca1dde6b760f801a076fad7bb822b4e53be5b6bcf05ecd"
SRC_URI[librbd1.sha256sum] = "f149e46f0f6a31f1af8bdc52385098c66c4c9fa538b5087ed98c357077463128"
