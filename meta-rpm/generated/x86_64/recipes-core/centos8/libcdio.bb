SUMMARY = "generated recipe based on libcdio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc ncurses pkgconfig-native"
RPM_SONAME_PROV_libcdio = "libcdio++.so.1 libcdio.so.18 libiso9660++.so.0 libiso9660.so.11 libudf.so.0"
RPM_SONAME_REQ_libcdio = "libc.so.6 libcdio.so.18 libgcc_s.so.1 libiso9660.so.11 libm.so.6 libncurses.so.6 libstdc++.so.6 libtinfo.so.6 libudf.so.0"
RDEPENDS_libcdio = "bash glibc info libgcc libstdc++ ncurses-libs"
RPM_SONAME_REQ_libcdio-devel = "libcdio++.so.1 libcdio.so.18 libiso9660++.so.0 libiso9660.so.11 libudf.so.0"
RPROVIDES_libcdio-devel = "libcdio-dev (= 2.0.0)"
RDEPENDS_libcdio-devel = "libcdio pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libcdio-2.0.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcdio-devel-2.0.0-3.el8.x86_64.rpm \
          "

SRC_URI[libcdio.sha256sum] = "38045680781e2dbb932645d85c43373d98877c96d6902056fe46ce7241572e6e"
SRC_URI[libcdio-devel.sha256sum] = "205748e916dcdce25227a17e475b77c5a51405ba542f657c48ec55a635f5cd33"
