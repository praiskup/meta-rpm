SUMMARY = "generated recipe based on traceroute srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_traceroute = "libc.so.6 libm.so.6"
RDEPENDS_traceroute = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/traceroute-2.1.0-6.el8.x86_64.rpm \
          "

SRC_URI[traceroute.sha256sum] = "0c8c13de9ca90dcdc2722ab0d72691afc04e6d5823ad57709035d57a15e3689b"
