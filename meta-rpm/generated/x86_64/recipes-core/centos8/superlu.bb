SUMMARY = "generated recipe based on SuperLU srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atlas pkgconfig-native"
RPM_SONAME_PROV_SuperLU = "libsuperlu.so.5.1"
RPM_SONAME_REQ_SuperLU = "libc.so.6 libsatlas.so.3"
RDEPENDS_SuperLU = "atlas glibc"
RPM_SONAME_REQ_SuperLU-devel = "libsuperlu.so.5.1"
RPROVIDES_SuperLU-devel = "SuperLU-dev (= 5.2.0)"
RDEPENDS_SuperLU-devel = "SuperLU"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/SuperLU-5.2.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/SuperLU-devel-5.2.0-7.el8.x86_64.rpm \
          "

SRC_URI[SuperLU.sha256sum] = "e6506f96cf6968327265f73891490d8e506cdcba9ddda39c84f750ac74378069"
SRC_URI[SuperLU-devel.sha256sum] = "007469f8f155db579047876937b70c25a4d065d88616892ef6b825eb8a956769"
