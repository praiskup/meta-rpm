SUMMARY = "generated recipe based on lttng-ust srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemtap-sdt-devel userspace-rcu"
RPM_SONAME_PROV_lttng-ust = "liblttng-ust-ctl.so.2 liblttng-ust-cyg-profile-fast.so.0 liblttng-ust-cyg-profile.so.0 liblttng-ust-dl.so.0 liblttng-ust-fork.so.0 liblttng-ust-libc-wrapper.so.0 liblttng-ust-pthread-wrapper.so.0 liblttng-ust-python-agent.so.0 liblttng-ust-tracepoint.so.0 liblttng-ust.so.0"
RPM_SONAME_REQ_lttng-ust = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 liblttng-ust-tracepoint.so.0 liblttng-ust.so.0 libpthread.so.0 librt.so.1 liburcu-bp.so.6 liburcu-cds.so.6"
RDEPENDS_lttng-ust = "glibc userspace-rcu"
RPM_SONAME_REQ_lttng-ust-devel = "liblttng-ust-ctl.so.2 liblttng-ust-cyg-profile-fast.so.0 liblttng-ust-cyg-profile.so.0 liblttng-ust-dl.so.0 liblttng-ust-fork.so.0 liblttng-ust-libc-wrapper.so.0 liblttng-ust-pthread-wrapper.so.0 liblttng-ust-python-agent.so.0 liblttng-ust-tracepoint.so.0 liblttng-ust.so.0"
RPROVIDES_lttng-ust-devel = "lttng-ust-dev (= 2.8.1)"
RDEPENDS_lttng-ust-devel = "lttng-ust pkgconf-pkg-config platform-python systemtap-sdt-devel userspace-rcu-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lttng-ust-2.8.1-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lttng-ust-devel-2.8.1-11.el8.x86_64.rpm \
          "

SRC_URI[lttng-ust.sha256sum] = "d729c76c7de8bbd1dc137cfeb1d09ac937d786693cb035d55aa0a773dcc3464e"
SRC_URI[lttng-ust-devel.sha256sum] = "83ed76e7cd3942bace4aa96818f197c44aacf52b575a8930c597142091595916"
