SUMMARY = "generated recipe based on hunspell-pt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-pt = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-pt-0.20130125-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-pt.sha256sum] = "899e56ec05a5ddfb72b55c7af6cdb65ec77037c61eb33dc07db1cca1e7d09ce8"
