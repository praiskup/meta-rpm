SUMMARY = "generated recipe based on maven-compiler-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-compiler-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-incremental maven-shared-utils plexus-compiler plexus-languages"
RDEPENDS_maven-compiler-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-compiler-plugin-3.7.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-compiler-plugin-javadoc-3.7.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-compiler-plugin.sha256sum] = "72f330d5c13ce2b6fbc20c9b7d8d6e727c7ce9b969b6f8217ea1314a9421da69"
SRC_URI[maven-compiler-plugin-javadoc.sha256sum] = "7fc32de703f4a41f81260e5cda42f5fa2698b8b050127f6839234d6a40f80c07"
