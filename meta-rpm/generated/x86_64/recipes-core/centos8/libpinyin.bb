SUMMARY = "generated recipe based on libpinyin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 kyotocabinet libgcc pkgconfig-native"
RPM_SONAME_PROV_libpinyin = "libpinyin.so.13"
RPM_SONAME_REQ_libpinyin = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libkyotocabinet.so.16 libm.so.6 libstdc++.so.6"
RDEPENDS_libpinyin = "glib2 glibc kyotocabinet-libs libgcc libpinyin-data libstdc++"
RDEPENDS_libpinyin-data = "libpinyin"
RPM_SONAME_PROV_libzhuyin = "libzhuyin.so.13"
RPM_SONAME_REQ_libzhuyin = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libkyotocabinet.so.16 libm.so.6 libstdc++.so.6"
RDEPENDS_libzhuyin = "glib2 glibc kyotocabinet-libs libgcc libpinyin libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpinyin-2.2.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpinyin-data-2.2.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libzhuyin-2.2.0-1.el8.x86_64.rpm \
          "

SRC_URI[libpinyin.sha256sum] = "cc1705406c15d4b6465f5f4700ead0bb8544dc7f0168849061e123a672dd9965"
SRC_URI[libpinyin-data.sha256sum] = "0ecaf92a23bcd8659722c00fa993fceb18a76e4e882559953224476cd2694c18"
SRC_URI[libzhuyin.sha256sum] = "cd659b1443e8ea46600de2571f4bb5c9c4323e400ac8f3ec574b0f5c9c420b6e"
