SUMMARY = "generated recipe based on libhbaapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libhbaapi = "libHBAAPI.so.2"
RPM_SONAME_REQ_libhbaapi = "libc.so.6 libdl.so.2"
RDEPENDS_libhbaapi = "glibc"
RPM_SONAME_REQ_libhbaapi-devel = "libHBAAPI.so.2"
RPROVIDES_libhbaapi-devel = "libhbaapi-dev (= 2.2.9)"
RDEPENDS_libhbaapi-devel = "libhbaapi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libhbaapi-2.2.9-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libhbaapi-devel-2.2.9-13.el8.x86_64.rpm \
          "

SRC_URI[libhbaapi.sha256sum] = "e7e017d8a2e87c29bef6e54c435310d7a085d9d71ce424ac3be9fb13dcc90229"
SRC_URI[libhbaapi-devel.sha256sum] = "a6ae09d408874a9ff567f468ed9bdc63632c5e236006bad0f49cda4f7ad3ddf6"
