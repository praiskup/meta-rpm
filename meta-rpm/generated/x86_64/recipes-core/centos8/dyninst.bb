SUMMARY = "generated recipe based on dyninst srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "boost elfutils libgcc pkgconfig-native tbb"
RPM_SONAME_PROV_dyninst = "libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libinstructionAPI.so.10.1 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPM_SONAME_REQ_dyninst = "ld-linux-x86-64.so.2 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libcommon.so.10.1 libdl.so.2 libdw.so.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libelf.so.1 libgcc_s.so.1 libgomp.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libpthread.so.0 libstackwalk.so.10.1 libstdc++.so.6 libsymtabAPI.so.10.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_dyninst = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer elfutils-libelf elfutils-libs glibc libgcc libgomp libstdc++ tbb"
RPM_SONAME_PROV_dyninst-devel = "libInst.so"
RPM_SONAME_REQ_dyninst-devel = "libc.so.6 libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libgcc_s.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libstdc++.so.6 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPROVIDES_dyninst-devel = "dyninst-dev (= 10.1.0)"
RDEPENDS_dyninst-devel = "boost-devel cmake-filesystem dyninst glibc libgcc libstdc++ tbb-devel"
RDEPENDS_dyninst-static = "dyninst-devel"
RPM_SONAME_REQ_dyninst-testsuite = "libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libcommon.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libgcc_s.so.1 libgomp.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libpthread.so.0 libstackwalk.so.10.1 libstdc++.so.6 libsymtabAPI.so.10.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_dyninst-testsuite = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer dyninst dyninst-devel dyninst-static glibc libgcc libgomp libstdc++ tbb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dyninst-10.1.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dyninst-devel-10.1.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dyninst-doc-10.1.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dyninst-static-10.1.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dyninst-testsuite-10.1.0-4.el8.x86_64.rpm \
          "

SRC_URI[dyninst.sha256sum] = "de0449fef4dfa1edaf49a73fa3d32e6278717c3ece7dd7df3239c0df7f892968"
SRC_URI[dyninst-devel.sha256sum] = "274fc9cfaadba6715f8ea92e4efb47d76c2a6a6c73ec182050d3339475e3310d"
SRC_URI[dyninst-doc.sha256sum] = "b4417aae83546f6ba7128458d17b8a7f1e3c1e908c98ccd5b57da157a88e958a"
SRC_URI[dyninst-static.sha256sum] = "e9c8694f08c221a6e3735c4118466b1c2e8e4b68dcbd9963bf207337f101387c"
SRC_URI[dyninst-testsuite.sha256sum] = "23e829a58aefd8c30b1f09fc682650a0d24fecfda5c6c105e321df946ba0a018"
