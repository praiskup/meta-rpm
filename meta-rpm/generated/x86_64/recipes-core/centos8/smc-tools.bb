SUMMARY = "generated recipe based on smc-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native"
RPM_SONAME_PROV_smc-tools = "libsmc-preload.so.1"
RPM_SONAME_REQ_smc-tools = "libc.so.6 libdl.so.2 libnl-3.so.200 libnl-genl-3.so.200"
RDEPENDS_smc-tools = "bash glibc libnl3"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/smc-tools-1.2.2-3.el8.x86_64.rpm \
          "

SRC_URI[smc-tools.sha256sum] = "51d52c414b572082b4411b03969fcd88f801564325093bf83f1bdafa33f0612d"
