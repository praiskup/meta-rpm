SUMMARY = "generated recipe based on lld srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc llvm pkgconfig-native"
RPM_SONAME_REQ_lld = "libLLVM-9.so libc.so.6 libgcc_s.so.1 liblldCOFF.so.9 liblldCommon.so.9 liblldDriver.so.9 liblldELF.so.9 liblldMinGW.so.9 liblldWasm.so.9 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_lld = "glibc libgcc libstdc++ lld-libs llvm-libs"
RPM_SONAME_REQ_lld-devel = "liblldCOFF.so.9 liblldCommon.so.9 liblldCore.so.9 liblldDriver.so.9 liblldELF.so.9 liblldMachO.so.9 liblldMinGW.so.9 liblldReaderWriter.so.9 liblldWasm.so.9 liblldYAML.so.9"
RPROVIDES_lld-devel = "lld-dev (= 9.0.1)"
RDEPENDS_lld-devel = "lld-libs"
RPM_SONAME_PROV_lld-libs = "liblldCOFF.so.9 liblldCommon.so.9 liblldCore.so.9 liblldDriver.so.9 liblldELF.so.9 liblldMachO.so.9 liblldMinGW.so.9 liblldReaderWriter.so.9 liblldWasm.so.9 liblldYAML.so.9"
RPM_SONAME_REQ_lld-libs = "ld-linux-x86-64.so.2 libLLVM-9.so libc.so.6 libgcc_s.so.1 liblldCOFF.so.9 liblldCommon.so.9 liblldCore.so.9 liblldMachO.so.9 liblldReaderWriter.so.9 liblldYAML.so.9 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_lld-libs = "glibc libgcc libstdc++ llvm-libs"
RPM_SONAME_REQ_lld-test = "libLLVM-9.so libc.so.6 libgcc_s.so.1 liblldDriver.so.9 liblldMachO.so.9 liblldYAML.so.9 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_lld-test = "bash glibc libgcc libstdc++ lld lld-libs llvm-libs llvm-test python3-lit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lld-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lld-devel-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lld-libs-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lld-test-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
          "

SRC_URI[lld.sha256sum] = "ae0623f2f018a1457353d64d4768c30b3bb2b796bd0e861794922c60428383d0"
SRC_URI[lld-devel.sha256sum] = "50d891830eb965205eccce268cd523375acf0ace438c6a6383c7bb8ef02ec5a5"
SRC_URI[lld-libs.sha256sum] = "ad62d8ae0fe2bfffc458e139d4cd4d740fa510d423eaf0ceb917f864c9137553"
SRC_URI[lld-test.sha256sum] = "a7a41f76821486754fa6e702ff6f43f3b5450bd65cba0293b402495645516f2d"
