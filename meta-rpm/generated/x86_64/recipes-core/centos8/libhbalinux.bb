SUMMARY = "generated recipe based on libhbalinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpciaccess pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libhbalinux = "libhbalinux.so.2"
RPM_SONAME_REQ_libhbalinux = "libc.so.6 libpciaccess.so.0 libudev.so.1"
RDEPENDS_libhbalinux = "bash glibc grep libhbaapi libpciaccess systemd-libs"
RPM_SONAME_REQ_libhbalinux-devel = "libhbalinux.so.2"
RPROVIDES_libhbalinux-devel = "libhbalinux-dev (= 1.0.17)"
RDEPENDS_libhbalinux-devel = "libhbalinux pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libhbalinux-1.0.17-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libhbalinux-devel-1.0.17-7.el8.x86_64.rpm \
          "

SRC_URI[libhbalinux.sha256sum] = "3b0ddad2475b9af554ff26a0f599170ec4547eeaed03e8aca588164f8378eb50"
SRC_URI[libhbalinux-devel.sha256sum] = "d086a91abd4e71da37d7ab00fed51415cdeb6a96990a425a062e4101691a1caa"
