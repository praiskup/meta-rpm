SUMMARY = "generated recipe based on fontconfig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat freetype libuuid pkgconfig-native"
RPM_SONAME_PROV_fontconfig = "libfontconfig.so.1"
RPM_SONAME_REQ_fontconfig = "libc.so.6 libexpat.so.1 libfontconfig.so.1 libfreetype.so.6 libpthread.so.0 libuuid.so.1"
RDEPENDS_fontconfig = "bash coreutils expat fontpackages-filesystem freetype glibc grep libuuid"
RPM_SONAME_REQ_fontconfig-devel = "libfontconfig.so.1"
RPROVIDES_fontconfig-devel = "fontconfig-dev (= 2.13.1)"
RDEPENDS_fontconfig-devel = "expat-devel fontconfig freetype-devel gettext libuuid-devel pkgconf-pkg-config"
RDEPENDS_fontconfig-devel-doc = "fontconfig-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fontconfig-2.13.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fontconfig-devel-2.13.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fontconfig-devel-doc-2.13.1-3.el8.noarch.rpm \
          "

SRC_URI[fontconfig.sha256sum] = "960f841e4a286b6b9511d47e6736753e4d1f0fd05395a078bacccfb1c8b164aa"
SRC_URI[fontconfig-devel.sha256sum] = "567749bc393f9561f11ba75123db97f83f7a5832d1f20c866e1e3c9aea826d8f"
SRC_URI[fontconfig-devel-doc.sha256sum] = "dae94a7165fa5d4004ce478c37226bb316e79c2ae1e3df8d5275d19e7f39569d"
