SUMMARY = "generated recipe based on gtkspell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo enchant fontconfig freetype gdk-pixbuf glib-2.0 gtk2 pango pkgconfig-native"
RPM_SONAME_PROV_gtkspell = "libgtkspell.so.0"
RPM_SONAME_REQ_gtkspell = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libenchant.so.1 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gtkspell = "atk cairo enchant fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 pango"
RPM_SONAME_REQ_gtkspell-devel = "libgtkspell.so.0"
RPROVIDES_gtkspell-devel = "gtkspell-dev (= 2.0.16)"
RDEPENDS_gtkspell-devel = "gtk2-devel gtkspell pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtkspell-2.0.16-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtkspell-devel-2.0.16-15.el8.x86_64.rpm \
          "

SRC_URI[gtkspell.sha256sum] = "9fe695f9eac2d4c728eb895f4367374ed0e38c07ceb4de6f61762ced0568929e"
SRC_URI[gtkspell-devel.sha256sum] = "0456350e1fac97842efe15570a0a53b899585529601d2bf07fb4e65d431a0d71"
