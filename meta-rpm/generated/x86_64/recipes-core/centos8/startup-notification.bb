SUMMARY = "generated recipe based on startup-notification srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxcb pkgconfig-native xcb-util"
RPM_SONAME_PROV_startup-notification = "libstartup-notification-1.so.0"
RPM_SONAME_REQ_startup-notification = "libX11-xcb.so.1 libX11.so.6 libc.so.6 libxcb-util.so.1 libxcb.so.1"
RDEPENDS_startup-notification = "glibc libX11 libX11-xcb libxcb xcb-util"
RPM_SONAME_REQ_startup-notification-devel = "libstartup-notification-1.so.0"
RPROVIDES_startup-notification-devel = "startup-notification-dev (= 0.12)"
RDEPENDS_startup-notification-devel = "libX11-devel pkgconf-pkg-config startup-notification"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/startup-notification-0.12-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/startup-notification-devel-0.12-15.el8.x86_64.rpm \
          "

SRC_URI[startup-notification.sha256sum] = "23656e93bf9f86d1b3335a62bb26f2dcee353aa4f591da9542f871982dd9563a"
SRC_URI[startup-notification-devel.sha256sum] = "c7061688e7ad7eeb76199f4c1006d9e8bb6069c436d7f07a63f1bcf9e11168d2"
