SUMMARY = "generated recipe based on mtx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mtx = "libc.so.6"
RDEPENDS_mtx = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mtx-1.3.12-17.el8.x86_64.rpm \
          "

SRC_URI[mtx.sha256sum] = "e4e2ab98854af699509e8a7fbe70f27d9b41f2bbe2bba1f6688ff1333d52da4f"
