SUMMARY = "generated recipe based on dbus-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-glib-devel dbus-libs expat glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_dbus-glib = "libdbus-glib-1.so.2"
RPM_SONAME_REQ_dbus-glib = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libexpat.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_dbus-glib = "dbus-libs expat glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbus-glib-0.110-2.el8.x86_64.rpm \
          "

SRC_URI[dbus-glib.sha256sum] = "f86fec6c6a844fbbfbf7c806d79dd7e72e4eef9c804472547a6d3ecf34cddca6"
