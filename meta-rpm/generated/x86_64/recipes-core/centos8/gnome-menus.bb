SUMMARY = "generated recipe based on gnome-menus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_gnome-menus = "libgnome-menu-3.so.0"
RPM_SONAME_REQ_gnome-menus = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_gnome-menus = "glib2 glibc"
RPM_SONAME_REQ_gnome-menus-devel = "libgnome-menu-3.so.0"
RPROVIDES_gnome-menus-devel = "gnome-menus-dev (= 3.13.3)"
RDEPENDS_gnome-menus-devel = "glib2-devel gnome-menus pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-menus-3.13.3-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gnome-menus-devel-3.13.3-11.el8.x86_64.rpm \
          "

SRC_URI[gnome-menus.sha256sum] = "69437c9c59ede4ee445d2bc7c105f1dc0fe6edb6b760c509064121c12b98d399"
SRC_URI[gnome-menus-devel.sha256sum] = "1dfd6141f525e509be629e4a7571e4034e27d1182937cfa22685ec4131d03318"
