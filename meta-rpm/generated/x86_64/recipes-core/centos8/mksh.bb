SUMMARY = "generated recipe based on mksh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mksh = "libc.so.6"
RDEPENDS_mksh = "bash chkconfig glibc grep"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mksh-56c-5.el8.x86_64.rpm \
          "

SRC_URI[mksh.sha256sum] = "68805aac1e31987748566f5470ce57d910f1a776f8a8d8d25652c6acc6bef2b5"
