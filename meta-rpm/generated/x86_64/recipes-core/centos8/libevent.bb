SUMMARY = "generated recipe based on libevent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_libevent = "libevent-2.1.so.6 libevent_core-2.1.so.6 libevent_extra-2.1.so.6 libevent_openssl-2.1.so.6 libevent_pthreads-2.1.so.6"
RPM_SONAME_REQ_libevent = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 libssl.so.1.1"
RDEPENDS_libevent = "glibc openssl-libs"
RPM_SONAME_REQ_libevent-devel = "libevent-2.1.so.6 libevent_core-2.1.so.6 libevent_extra-2.1.so.6 libevent_openssl-2.1.so.6 libevent_pthreads-2.1.so.6"
RPROVIDES_libevent-devel = "libevent-dev (= 2.1.8)"
RDEPENDS_libevent-devel = "libevent pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libevent-devel-2.1.8-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libevent-2.1.8-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libevent-doc-2.1.8-5.el8.noarch.rpm \
          "

SRC_URI[libevent.sha256sum] = "746bac6bb011a586d42bd82b2f8b25bac72c9e4bbd4c19a34cf88eadb1d83873"
SRC_URI[libevent-devel.sha256sum] = "d78150f18130013f26f16986e39ca9ec8ac3f90eed35f4f06b963c97e0d882f7"
SRC_URI[libevent-doc.sha256sum] = "7444164b9d09e77d738327cb8b5044c13a1a33f0d88154f28768c2a7694c83ee"
