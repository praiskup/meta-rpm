SUMMARY = "generated recipe based on linuxptp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_linuxptp = "libc.so.6 libm.so.6 librt.so.1"
RDEPENDS_linuxptp = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/linuxptp-2.0-4.el8.x86_64.rpm \
          "

SRC_URI[linuxptp.sha256sum] = "b1c0aa4267c4324d124b49e3f46b5e89c5afcf2234f393e044ea24e34abbbdba"
