SUMMARY = "generated recipe based on libnfnetlink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libnfnetlink = "libnfnetlink.so.0"
RPM_SONAME_REQ_libnfnetlink = "libc.so.6"
RDEPENDS_libnfnetlink = "glibc"
RPM_SONAME_REQ_libnfnetlink-devel = "libnfnetlink.so.0"
RPROVIDES_libnfnetlink-devel = "libnfnetlink-dev (= 1.0.1)"
RDEPENDS_libnfnetlink-devel = "kernel-headers libnfnetlink pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnfnetlink-1.0.1-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnfnetlink-devel-1.0.1-13.el8.x86_64.rpm \
          "

SRC_URI[libnfnetlink.sha256sum] = "cec98aa5fbefcb99715921b493b4f92d34c4eeb823e9c8741aa75e280def89f1"
SRC_URI[libnfnetlink-devel.sha256sum] = "ee99a86ddaf52327c3e0c8e88ace92873b54a019b58fe9ea1b90cad5346ab698"
