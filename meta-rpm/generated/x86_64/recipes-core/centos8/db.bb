SUMMARY = "generated recipe based on libdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc openssl pkgconfig-native"
RPM_SONAME_PROV_libdb = "libdb-5.3.so"
RPM_SONAME_REQ_libdb = "libc.so.6 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_libdb = "glibc openssl-libs"
RPM_SONAME_PROV_libdb-cxx = "libdb_cxx-5.3.so"
RPM_SONAME_REQ_libdb-cxx = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libdb-cxx = "glibc libdb libgcc libstdc++ openssl-libs"
RPROVIDES_libdb-cxx-devel = "libdb-cxx-dev (= 5.3.28)"
RDEPENDS_libdb-cxx-devel = "libdb-cxx libdb-devel"
RPROVIDES_libdb-devel = "libdb-dev (= 5.3.28)"
RDEPENDS_libdb-devel = "libdb"
RDEPENDS_libdb-devel-doc = "libdb libdb-devel"
RPM_SONAME_PROV_libdb-sql = "libdb_sql-5.3.so"
RPM_SONAME_REQ_libdb-sql = "libc.so.6 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_libdb-sql = "glibc libdb openssl-libs"
RPM_SONAME_REQ_libdb-sql-devel = "libc.so.6 libcrypto.so.1.1 libdb_sql-5.3.so libdl.so.2 libpthread.so.0"
RPROVIDES_libdb-sql-devel = "libdb-sql-dev (= 5.3.28)"
RDEPENDS_libdb-sql-devel = "glibc libdb-sql openssl-libs"
RPM_SONAME_REQ_libdb-utils = "libc.so.6 libcrypto.so.1.1 libdb-5.3.so libpthread.so.0"
RDEPENDS_libdb-utils = "glibc libdb openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdb-devel-5.3.28-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libdb-5.3.28-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libdb-utils-5.3.28-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdb-cxx-5.3.28-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdb-cxx-devel-5.3.28-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdb-devel-doc-5.3.28-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdb-sql-5.3.28-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdb-sql-devel-5.3.28-37.el8.x86_64.rpm \
          "

SRC_URI[libdb.sha256sum] = "f45993f87be2c9c7f2408a5479418cb9a0c2a28a9f316db0d82cff1518470776"
SRC_URI[libdb-cxx.sha256sum] = "1b9fa77fefbb4d80c120ac90794145a1ef326025faffe1027f0b9a7464fe8e4d"
SRC_URI[libdb-cxx-devel.sha256sum] = "6bdc241a16d9ac232c9260fb5f29c1101d46e05779cedd0929b703cf4384f67d"
SRC_URI[libdb-devel.sha256sum] = "eef76c38f7e7b5cc807ad2e6b45a878f905fe5dcb39c51e67e7fbfc66ff4c441"
SRC_URI[libdb-devel-doc.sha256sum] = "9766e7612e003692b61a686c7bd53a25ebadc467ae3bc8e01dca0a0b64fa891c"
SRC_URI[libdb-sql.sha256sum] = "72702a14b1cf9d794107c6e6e766f5166f0553714c94df9fcdd9b8e8e709f963"
SRC_URI[libdb-sql-devel.sha256sum] = "9bf77e91ee82befebb29d978b09bd4707a29cc9f8379c1e3d18e93d031ba3f70"
SRC_URI[libdb-utils.sha256sum] = "93e5537fdce10d06c0649cd85f5e29bf31e2d5486104cdfb236f9b0b0c35b280"
