SUMMARY = "generated recipe based on libsamplerate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib libsndfile2 pkgconfig-native"
RPM_SONAME_PROV_libsamplerate = "libsamplerate.so.0"
RPM_SONAME_REQ_libsamplerate = "libasound.so.2 libc.so.6 libm.so.6 libsamplerate.so.0 libsndfile.so.1"
RDEPENDS_libsamplerate = "alsa-lib glibc libsndfile"
RPM_SONAME_REQ_libsamplerate-devel = "libsamplerate.so.0"
RPROVIDES_libsamplerate-devel = "libsamplerate-dev (= 0.1.9)"
RDEPENDS_libsamplerate-devel = "libsamplerate pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsamplerate-0.1.9-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsamplerate-devel-0.1.9-1.el8.x86_64.rpm \
          "

SRC_URI[libsamplerate.sha256sum] = "fdede6aa06c49f9807d6854e98913c450fef00ce906a61ef9df16de6690f04c7"
SRC_URI[libsamplerate-devel.sha256sum] = "8e95936e9465bf0e165413eefaadd427c0c13c981ac9ca55b6582b0a3196a1a9"
