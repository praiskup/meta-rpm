SUMMARY = "generated recipe based on sharutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_sharutils = "libc.so.6"
RDEPENDS_sharutils = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sharutils-4.15.2-11.el8.x86_64.rpm \
          "

SRC_URI[sharutils.sha256sum] = "4719d7e338d67191d7f6bceab7d9b95ad89ca3c9f809b43b71ef9abb01f1a537"
