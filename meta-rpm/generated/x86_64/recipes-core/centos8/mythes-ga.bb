SUMMARY = "generated recipe based on mythes-ga srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-ga = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-ga-0.20071001-19.el8.noarch.rpm \
          "

SRC_URI[mythes-ga.sha256sum] = "0bb146e6bc598cbc3f04a2ed53c81b102da331a3fdf3e1a2ecdf555fbfafc875"
