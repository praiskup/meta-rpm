SUMMARY = "generated recipe based on python-cffi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cffi = "ld-linux-x86-64.so.2 libc.so.6 libffi.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-cffi = "glibc libffi platform-python python3-libs python3-pycparser"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-cffi-1.11.5-5.el8.x86_64.rpm \
          "

SRC_URI[python3-cffi.sha256sum] = "07ed1209e898552e6aeac0e6d148271d562c576f7d903cb7a99a697643068f58"
