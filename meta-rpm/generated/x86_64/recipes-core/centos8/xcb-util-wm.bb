SUMMARY = "generated recipe based on xcb-util-wm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util-wm = "libxcb-ewmh.so.2 libxcb-icccm.so.4"
RPM_SONAME_REQ_xcb-util-wm = "libc.so.6 libxcb.so.1"
RDEPENDS_xcb-util-wm = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-wm-devel = "libxcb-ewmh.so.2 libxcb-icccm.so.4"
RPROVIDES_xcb-util-wm-devel = "xcb-util-wm-dev (= 0.4.1)"
RDEPENDS_xcb-util-wm-devel = "libxcb-devel pkgconf-pkg-config xcb-util-wm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xcb-util-wm-0.4.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xcb-util-wm-devel-0.4.1-12.el8.x86_64.rpm \
          "

SRC_URI[xcb-util-wm.sha256sum] = "098e1fdd07f5720d6f9b86033334759abfeadde0b00281d45680ef72bc3179fc"
SRC_URI[xcb-util-wm-devel.sha256sum] = "149a5bd75b3805c8a320fe1c1c3f79e564127fed5f91efcfbcba90520802ad4c"
