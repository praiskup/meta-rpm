SUMMARY = "generated recipe based on xorg-x11-drv-evdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libevdev mtdev pkgconfig-native systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-evdev = "libc.so.6 libevdev.so.2 libmtdev.so.1 libudev.so.1"
RDEPENDS_xorg-x11-drv-evdev = "glibc libevdev mtdev systemd-libs xkeyboard-config xorg-x11-server-Xorg"
RPROVIDES_xorg-x11-drv-evdev-devel = "xorg-x11-drv-evdev-dev (= 2.10.6)"
RDEPENDS_xorg-x11-drv-evdev-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-evdev-2.10.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-evdev-devel-2.10.6-2.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-evdev.sha256sum] = "91638ffca6fff48cab1fa7e7a7402ecf4204b2f5ee15fc3cac9c498888eab896"
SRC_URI[xorg-x11-drv-evdev-devel.sha256sum] = "50cee1567ba59c585e5df66b0cc302c99d4e2c670150248d2b52328642a5b7d6"
