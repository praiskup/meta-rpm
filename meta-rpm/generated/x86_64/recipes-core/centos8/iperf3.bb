SUMMARY = "generated recipe based on iperf3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_iperf3 = "libiperf.so.0"
RPM_SONAME_REQ_iperf3 = "libc.so.6 libiperf.so.0 libm.so.6"
RDEPENDS_iperf3 = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/iperf3-3.5-3.el8.x86_64.rpm \
          "

SRC_URI[iperf3.sha256sum] = "62985d1342cb34ff480ca12d2ad08e6e2c76a34c22740aaf76aff93d8fd47442"
