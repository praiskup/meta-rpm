SUMMARY = "generated recipe based on perl-Number-Compare srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Number-Compare = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Number-Compare-0.03-19.el8.noarch.rpm \
          "

SRC_URI[perl-Number-Compare.sha256sum] = "a70013793d7440970c0944a43c9ba68380f5411a22b943afa1fc902a38714eb5"
