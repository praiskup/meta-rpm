SUMMARY = "generated recipe based on jna srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi pkgconfig-native"
RPM_SONAME_REQ_jna = "libc.so.6 libffi.so.6"
RDEPENDS_jna = "glibc java-1.8.0-openjdk-headless javapackages-filesystem libffi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jna-4.5.1-5.el8.x86_64.rpm \
          "

SRC_URI[jna.sha256sum] = "b7b58b4c9094c95d60dcfd79426547e187a7a6922aee057a1e9502a037e551d4"
