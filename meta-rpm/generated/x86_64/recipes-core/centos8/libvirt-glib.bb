SUMMARY = "generated recipe based on libvirt-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libvirt libxml2 pkgconfig-native"
RPM_SONAME_PROV_libvirt-gconfig = "libvirt-gconfig-1.0.so.0"
RPM_SONAME_REQ_libvirt-gconfig = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libxml2.so.2"
RDEPENDS_libvirt-gconfig = "glib2 glibc libxml2"
RPM_SONAME_PROV_libvirt-glib = "libvirt-glib-1.0.so.0"
RPM_SONAME_REQ_libvirt-glib = "libc.so.6 libglib-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libvirt.so.0"
RDEPENDS_libvirt-glib = "glib2 glibc libvirt-libs"
RPM_SONAME_PROV_libvirt-gobject = "libvirt-gobject-1.0.so.0"
RPM_SONAME_REQ_libvirt-gobject = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libvirt-gconfig-1.0.so.0 libvirt-glib-1.0.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_libvirt-gobject = "glib2 glibc libvirt-gconfig libvirt-glib libvirt-libs libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-gconfig-2.0.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-glib-2.0.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-gobject-2.0.0-1.el8.x86_64.rpm \
          "

SRC_URI[libvirt-gconfig.sha256sum] = "32f4f0cd9db2903ca1990f28ef72bd1370eb204026e03ac44d6bd9f261feb0f3"
SRC_URI[libvirt-glib.sha256sum] = "b9ca261f1d36e7e01bb156bda09d52fa3109c2f7c9058bbc1a4b94bd1aa1356f"
SRC_URI[libvirt-gobject.sha256sum] = "777a521c6ccfdb55daa7cdb318d8c801439df40b0b23da92152b8fe0c8a947f4"
