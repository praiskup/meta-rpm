SUMMARY = "generated recipe based on libXNVCtrl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libXNVCtrl = "libXNVCtrl.so.0"
RPM_SONAME_REQ_libXNVCtrl = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXNVCtrl = "glibc libX11 libXext"
RPM_SONAME_REQ_libXNVCtrl-devel = "libXNVCtrl.so.0"
RPROVIDES_libXNVCtrl-devel = "libXNVCtrl-dev (= 352.21)"
RDEPENDS_libXNVCtrl-devel = "libX11-devel libXNVCtrl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXNVCtrl-352.21-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libXNVCtrl-devel-352.21-9.el8.x86_64.rpm \
          "

SRC_URI[libXNVCtrl.sha256sum] = "e055faa94d603bff6354b60bf744d462e52aa5827f606dc58412f01376202f6f"
SRC_URI[libXNVCtrl-devel.sha256sum] = "9ff7631cc9a118b70781f69b73145eb05e45223c9e9efe80e0c7584dc7b759a3"
