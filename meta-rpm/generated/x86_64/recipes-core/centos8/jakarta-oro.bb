SUMMARY = "generated recipe based on jakarta-oro srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jakarta-oro = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jakarta-oro-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jakarta-oro-2.0.8-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jakarta-oro-javadoc-2.0.8-23.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jakarta-oro.sha256sum] = "bfdd962c56d4af267f4bd53eb67999e21dc79b4b9925bee344f943ea567d58e2"
SRC_URI[jakarta-oro-javadoc.sha256sum] = "e8cfce79e9b16fd3394881a3673007f7323f729882ce7920c803f5ea321e39cb"
