SUMMARY = "generated recipe based on perl-B-Lint srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-B-Lint = "perl-Carp perl-Module-Pluggable perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-B-Lint-1.20-11.el8.noarch.rpm \
          "

SRC_URI[perl-B-Lint.sha256sum] = "9ae66704f7209e7b84d30055cc928f905bbfee24ee4997031fc8926abf0e7f5e"
