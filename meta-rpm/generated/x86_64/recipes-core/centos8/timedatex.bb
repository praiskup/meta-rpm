SUMMARY = "generated recipe based on timedatex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libselinux pkgconfig-native"
RPM_SONAME_REQ_timedatex = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libselinux.so.1"
RDEPENDS_timedatex = "bash glib2 glibc libselinux polkit systemd util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/timedatex-0.5-3.el8.x86_64.rpm \
          "

SRC_URI[timedatex.sha256sum] = "6fc3208be9c78bf1f843f9f7d4d3f04096f116dc056285142235bf76472ba382"
