SUMMARY = "generated recipe based on perl-version srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-version = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-version = "glibc perl-Carp perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-version-0.99.24-1.el8.x86_64.rpm \
          "

SRC_URI[perl-version.sha256sum] = "dd0b1b4e1c80ac39e9626d63c953b3311f6ce4b216533ad40c69873d1a0e09d0"
