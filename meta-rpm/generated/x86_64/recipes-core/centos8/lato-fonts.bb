SUMMARY = "generated recipe based on lato-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lato-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lato-fonts-2.015-5.el8.noarch.rpm \
          "

SRC_URI[lato-fonts.sha256sum] = "fbffd5efd635754cabbad344a0960fbd6e0e0c64767f5f235a69e86683af8395"
