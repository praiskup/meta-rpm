SUMMARY = "generated recipe based on perl-Devel-StackTrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-StackTrace = "perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-StackTrace-2.03-2.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-StackTrace.sha256sum] = "98f25020d793bf624271f40f1c6597c176442a5c29132095651065540d661f27"
