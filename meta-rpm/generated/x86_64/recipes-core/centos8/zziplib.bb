SUMMARY = "generated recipe based on zziplib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native sdl zlib"
RPM_SONAME_PROV_zziplib = "libzzip-0.so.13 libzzipfseeko-0.so.13 libzzipmmapped-0.so.13 libzzipwrap-0.so.13"
RPM_SONAME_REQ_zziplib = "libc.so.6 libz.so.1 libzzip-0.so.13"
RDEPENDS_zziplib = "glibc zlib"
RPM_SONAME_REQ_zziplib-devel = "libzzip-0.so.13 libzzipfseeko-0.so.13 libzzipmmapped-0.so.13 libzzipwrap-0.so.13"
RPROVIDES_zziplib-devel = "zziplib-dev (= 0.13.68)"
RDEPENDS_zziplib-devel = "SDL-devel pkgconf-pkg-config zlib-devel zziplib"
RPM_SONAME_REQ_zziplib-utils = "libc.so.6 libz.so.1 libzzip-0.so.13 libzzipfseeko-0.so.13 libzzipmmapped-0.so.13"
RDEPENDS_zziplib-utils = "glibc zlib zziplib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/zziplib-0.13.68-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/zziplib-utils-0.13.68-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/zziplib-devel-0.13.68-8.el8.x86_64.rpm \
          "

SRC_URI[zziplib.sha256sum] = "dc8f24535267c830fb3f13ee98005b2e3b0c6aefcdab4b62900457db18ef8222"
SRC_URI[zziplib-devel.sha256sum] = "3d039e6962ab47f798b96a2b5d5f2562426028eedee8b7eb3b891c25993cafb2"
SRC_URI[zziplib-utils.sha256sum] = "15b5f9e22690c0abab9899febc9b71e58f808b46134bfe45ff387881af403534"
