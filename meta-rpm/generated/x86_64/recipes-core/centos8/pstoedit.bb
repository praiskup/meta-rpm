SUMMARY = "generated recipe based on pstoedit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gd libemf libgcc pkgconfig-native plotutils"
RPM_SONAME_PROV_pstoedit = "libp2edrvlplot.so.0 libp2edrvstd.so.0 libp2edrvwmf.so.0 libpstoedit.so.0"
RPM_SONAME_REQ_pstoedit = "libEMF.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgd.so.3 libm.so.6 libp2edrvlplot.so.0 libp2edrvstd.so.0 libp2edrvwmf.so.0 libplotter.so.2 libpstoedit.so.0 libstdc++.so.6"
RDEPENDS_pstoedit = "gd ghostscript glibc libEMF libgcc libstdc++ plotutils"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pstoedit-3.70-9.el8.x86_64.rpm \
          "

SRC_URI[pstoedit.sha256sum] = "3e9733f5db88594f1e0398d3e7b896cfbe6466995b13667b639dbc8b9228dded"
