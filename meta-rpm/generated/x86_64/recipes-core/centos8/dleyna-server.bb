SUMMARY = "generated recipe based on dleyna-server srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dleyna-core glib-2.0 gssdp gupnp gupnp-av gupnp-dlna libsoup-2.4 libxml2 pkgconfig-native"
RPM_SONAME_PROV_dleyna-server = "libdleyna-server-1.0.so.1"
RPM_SONAME_REQ_dleyna-server = "libc.so.6 libdleyna-core-1.0.so.5 libdleyna-server-1.0.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssdp-1.0.so.3 libgupnp-1.0.so.4 libgupnp-av-1.0.so.2 libgupnp-dlna-2.0.so.3 libpthread.so.0 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_dleyna-server = "dbus dleyna-connector-dbus dleyna-core glib2 glibc gssdp gupnp gupnp-av gupnp-dlna libsoup libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dleyna-server-0.6.0-2.el8.x86_64.rpm \
          "

SRC_URI[dleyna-server.sha256sum] = "b3976df39c63da4a31d24208e245e1a53ba47007c56c602dc88b9247646d9d75"
