SUMMARY = "generated recipe based on jboss-logging srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-logging = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jboss-logging-3.3.0-5.el8.noarch.rpm \
          "

SRC_URI[jboss-logging.sha256sum] = "758d18a73df25c0f527996e9e2ec5602a6b348e79fa158c11e8f8750dc62b308"
