SUMMARY = "generated recipe based on perl-Unicode-UTF8 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-UTF8 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-UTF8 = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Unicode-UTF8-0.62-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Unicode-UTF8.sha256sum] = "f8358a96a614ead65ecf924b24b054d841214fe3c4acb6b0290458eeada715e9"
