SUMMARY = "generated recipe based on fftw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_fftw = "libc.so.6 libfftw3.so.3 libfftw3_threads.so.3 libfftw3f.so.3 libfftw3f_threads.so.3 libfftw3l.so.3 libfftw3l_threads.so.3 libfftw3q.so.3 libfftw3q_threads.so.3 libm.so.6 libpthread.so.0 libquadmath.so.0"
RDEPENDS_fftw = "bash fftw-libs-double fftw-libs-long fftw-libs-quad fftw-libs-single glibc info libquadmath"
RPM_SONAME_REQ_fftw-devel = "libfftw3.so.3 libfftw3_omp.so.3 libfftw3_threads.so.3 libfftw3f.so.3 libfftw3f_omp.so.3 libfftw3f_threads.so.3 libfftw3l.so.3 libfftw3l_omp.so.3 libfftw3l_threads.so.3 libfftw3q.so.3 libfftw3q_omp.so.3 libfftw3q_threads.so.3"
RPROVIDES_fftw-devel = "fftw-dev (= 3.3.5)"
RDEPENDS_fftw-devel = "bash fftw fftw-libs fftw-libs-double fftw-libs-long fftw-libs-quad fftw-libs-single pkgconf-pkg-config"
RDEPENDS_fftw-libs = "fftw-libs-double fftw-libs-long fftw-libs-quad fftw-libs-single"
RPM_SONAME_PROV_fftw-libs-double = "libfftw3.so.3 libfftw3_omp.so.3 libfftw3_threads.so.3"
RPM_SONAME_REQ_fftw-libs-double = "libc.so.6 libfftw3.so.3 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_fftw-libs-double = "glibc libgcc libgomp"
RPM_SONAME_PROV_fftw-libs-long = "libfftw3l.so.3 libfftw3l_omp.so.3 libfftw3l_threads.so.3"
RPM_SONAME_REQ_fftw-libs-long = "libc.so.6 libfftw3l.so.3 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_fftw-libs-long = "glibc libgcc libgomp"
RPM_SONAME_PROV_fftw-libs-quad = "libfftw3q.so.3 libfftw3q_omp.so.3 libfftw3q_threads.so.3"
RPM_SONAME_REQ_fftw-libs-quad = "libc.so.6 libfftw3q.so.3 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0 libquadmath.so.0"
RDEPENDS_fftw-libs-quad = "glibc libgcc libgomp libquadmath"
RPM_SONAME_PROV_fftw-libs-single = "libfftw3f.so.3 libfftw3f_omp.so.3 libfftw3f_threads.so.3"
RPM_SONAME_REQ_fftw-libs-single = "libc.so.6 libfftw3f.so.3 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_fftw-libs-single = "glibc libgcc libgomp"
RDEPENDS_fftw-static = "fftw-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-devel-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-libs-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-libs-double-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-libs-long-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-libs-quad-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-libs-single-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fftw-static-3.3.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fftw-doc-3.3.5-11.el8.noarch.rpm \
          "

SRC_URI[fftw.sha256sum] = "97c07fbe3b19d484f90a799612b3dc8c968bccdcfa6b75e417fe6fd54302deed"
SRC_URI[fftw-devel.sha256sum] = "190c4e3417d65018c9af15987ce35448dba2b1e844a495cb27493d66a5f80c4d"
SRC_URI[fftw-doc.sha256sum] = "d20b4d8314ec79369bac8e9fb624666dcd995b40b89a8b1f48a6c26b8135135c"
SRC_URI[fftw-libs.sha256sum] = "92c8ac2ec67438fd0cdf0fe893e90499f69b71b4d935fc85e64e7e5ab9cf6691"
SRC_URI[fftw-libs-double.sha256sum] = "ed2767829f8545739e9861254cc4c729a43f0e67b981af3102649b4732feaeff"
SRC_URI[fftw-libs-long.sha256sum] = "eb600d23d9ceceb41271bbc87a51600411ff8c2a0a222da34cef5da87b9372d0"
SRC_URI[fftw-libs-quad.sha256sum] = "b9217edfa34516ad80a98872565e47277959dbf94b8039b5d72333215866de03"
SRC_URI[fftw-libs-single.sha256sum] = "27b982d8a589de492acbfac23793158a04a5155a200566d3cf1817fd56b67c64"
SRC_URI[fftw-static.sha256sum] = "326fff99fe35faa68d20099e4d9c3705de2056babf458797261dddb76d54751c"
