SUMMARY = "generated recipe based on gstreamer1 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "check glib-2.0 libcap libgcc libxml2 pkgconfig-native"
RPM_SONAME_PROV_gstreamer1 = "libgstbase-1.0.so.0 libgstcheck-1.0.so.0 libgstcontroller-1.0.so.0 libgstcoreelements.so libgstcoretracers.so libgstnet-1.0.so.0 libgstreamer-1.0.so.0"
RPM_SONAME_REQ_gstreamer1 = "libc.so.6 libcap.so.2 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_gstreamer1 = "bash glib2 glibc libcap libgcc"
RPM_SONAME_REQ_gstreamer1-devel = "libgstbase-1.0.so.0 libgstcheck-1.0.so.0 libgstcontroller-1.0.so.0 libgstnet-1.0.so.0 libgstreamer-1.0.so.0"
RPROVIDES_gstreamer1-devel = "gstreamer1-dev (= 1.16.1)"
RDEPENDS_gstreamer1-devel = "check-devel glib2-devel gstreamer1 libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gstreamer1-1.16.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gstreamer1-devel-1.16.1-2.el8.x86_64.rpm \
          "

SRC_URI[gstreamer1.sha256sum] = "f15ce668cd55f1d5df62902d98ade38a057e3c782549dca3c45ce038b9ae2968"
SRC_URI[gstreamer1-devel.sha256sum] = "47b9349c9f38d2e34b709a9b2fcc24f3107864fc425e0e2345612727ea8984ed"
