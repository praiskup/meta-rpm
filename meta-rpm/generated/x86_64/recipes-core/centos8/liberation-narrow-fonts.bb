SUMMARY = "generated recipe based on liberation-narrow-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_liberation-narrow-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/liberation-narrow-fonts-1.07.5-2.el8.noarch.rpm \
          "

SRC_URI[liberation-narrow-fonts.sha256sum] = "2e3a9449ceaa3b176ea7cd0ec01631b71d99a4c535082d46d9945652fc49a917"
