SUMMARY = "generated recipe based on esc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_esc = "dbus gjs glib2 gobject-introspection gtk3 nspr nss opensc pcsc-lite"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/esc-1.1.2-11.el8.x86_64.rpm \
          "

SRC_URI[esc.sha256sum] = "6dcf5fadb38541bc6af8a351dbb4f4e0960de7cc520cce4acac7737d04b5fac7"
