SUMMARY = "generated recipe based on vhostmd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt libxml2 pkgconfig-native"
RPM_SONAME_REQ_vhostmd = "libc.so.6 libm.so.6 libpthread.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_vhostmd = "bash glibc libvirt-libs libxml2 systemd"
RPM_SONAME_PROV_vm-dump-metrics = "libmetrics.so.0"
RPM_SONAME_REQ_vm-dump-metrics = "libc.so.6 libmetrics.so.0 libxml2.so.2"
RDEPENDS_vm-dump-metrics = "glibc libxml2"
RPM_SONAME_REQ_vm-dump-metrics-devel = "libmetrics.so.0"
RPROVIDES_vm-dump-metrics-devel = "vm-dump-metrics-dev (= 1.1)"
RDEPENDS_vm-dump-metrics-devel = "pkgconf-pkg-config vm-dump-metrics"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vhostmd-1.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/vm-dump-metrics-1.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/vm-dump-metrics-devel-1.1-4.el8.x86_64.rpm \
          "

SRC_URI[vhostmd.sha256sum] = "9e4cb3d04c0b5b2d246e8cab93dd29ee4de7d321fef74602d88cfff507063f57"
SRC_URI[vm-dump-metrics.sha256sum] = "f8c1b60e2089335af1232345edf629107d14f1c5ac4eae831da0c0486355ae9e"
SRC_URI[vm-dump-metrics-devel.sha256sum] = "c8db93d53b4e53fd08f4cb710f67458d50f02d25fc777ed220a872b74c46d1e6"
