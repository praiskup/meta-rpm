SUMMARY = "generated recipe based on spice-vdagent-win srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-vdagent-win-x64-0.10.0-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-vdagent-win-x86-0.10.0-2.el8.noarch.rpm \
          "

SRC_URI[spice-vdagent-win-x64.sha256sum] = "b453db1f388d725d7d3be81171c78c6546aaa7a902942e47bf3dbbc3eaceb9da"
SRC_URI[spice-vdagent-win-x86.sha256sum] = "70e748bd6f6cc7b7ddd7c5af707f0cbe656bf0ea4c4d4d079f62eaeb5f0803dd"
