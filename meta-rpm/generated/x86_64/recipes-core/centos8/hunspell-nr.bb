SUMMARY = "generated recipe based on hunspell-nr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-nr = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-nr-0.20091030-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-nr.sha256sum] = "5e04f0e7faa4c74ec07356c17c06bb64355d554eacaefd1529d3116989121a93"
