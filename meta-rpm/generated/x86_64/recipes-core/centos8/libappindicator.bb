SUMMARY = "generated recipe based on libappindicator srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus-glib-devel gdk-pixbuf glib-2.0 gtk+3 libdbusmenu libindicator pango pkgconfig-native"
RPM_SONAME_PROV_libappindicator-gtk3 = "libappindicator3.so.1"
RPM_SONAME_REQ_libappindicator-gtk3 = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbusmenu-glib.so.4 libdbusmenu-gtk3.so.4 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libindicator3.so.7 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_libappindicator-gtk3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libdbusmenu libdbusmenu-gtk3 libindicator-gtk3 pango"
RPM_SONAME_REQ_libappindicator-gtk3-devel = "libappindicator3.so.1"
RPROVIDES_libappindicator-gtk3-devel = "libappindicator-gtk3-dev (= 12.10.0)"
RDEPENDS_libappindicator-gtk3-devel = "dbus-glib-devel gtk3-devel libappindicator-gtk3 libdbusmenu-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libappindicator-gtk3-12.10.0-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libappindicator-gtk3-devel-12.10.0-19.el8.x86_64.rpm \
          "

SRC_URI[libappindicator-gtk3.sha256sum] = "8c0c475eefa059be9bb49c08d9865619512b1ee3e06daf9f7d41829889fbfd50"
SRC_URI[libappindicator-gtk3-devel.sha256sum] = "0fa10b45d20ccd8512051231403f656dc8b6499c54fb5ddf9ac869535b2071d1"
