SUMMARY = "generated recipe based on perl-libnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-libnet = "perl-Carp perl-Digest-MD5 perl-Errno perl-Exporter perl-IO perl-IO-Socket-IP perl-Socket perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-libnet-3.11-3.el8.noarch.rpm \
          "

SRC_URI[perl-libnet.sha256sum] = "627d792d7cca1e97c121be5f0808c9da96f54d6c986622af246c60354b9c316e"
