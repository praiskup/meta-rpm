SUMMARY = "generated recipe based on snappy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_snappy = "libsnappy.so.1"
RPM_SONAME_REQ_snappy = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_snappy = "glibc libgcc libstdc++"
RPM_SONAME_REQ_snappy-devel = "libsnappy.so.1"
RPROVIDES_snappy-devel = "snappy-dev (= 1.1.7)"
RDEPENDS_snappy-devel = "cmake-filesystem pkgconf-pkg-config snappy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/snappy-1.1.7-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/snappy-devel-1.1.7-5.el8.x86_64.rpm \
          "

SRC_URI[snappy.sha256sum] = "e78c64740cfa1837905fd5e4848d2bebc3c50149da76c979ecd8702f523dc902"
SRC_URI[snappy-devel.sha256sum] = "412d4901a30f3341b2041b2b3887b56c993f923c066a4db0b4a7fb18b76049b6"
