SUMMARY = "generated recipe based on sblim-cmpi-devel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libcmpiCppImpl0 = "libcmpiCppImpl.so.0"
RPM_SONAME_REQ_libcmpiCppImpl0 = "libc.so.6 libcmpiCppImpl.so.0 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libcmpiCppImpl0 = "glibc libgcc libstdc++"
RPROVIDES_sblim-cmpi-devel = "sblim-cmpi-dev (= 2.0.3)"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libcmpiCppImpl0-2.0.3-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sblim-cmpi-devel-2.0.3-15.el8.x86_64.rpm \
          "

SRC_URI[libcmpiCppImpl0.sha256sum] = "eda101be2b3a7eec96ac1bb60b8836ed571d44d42f95105897db1448f853eba8"
SRC_URI[sblim-cmpi-devel.sha256sum] = "b66ec2e647b58950dc21bdf5ce25d94642167bb90163d8aede52fcf84dd438ca"
