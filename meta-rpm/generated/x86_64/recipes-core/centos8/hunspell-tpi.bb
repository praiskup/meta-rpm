SUMMARY = "generated recipe based on hunspell-tpi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tpi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-tpi-0.07-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-tpi.sha256sum] = "79687f12216e42314361553db9fd5ae27ea4f126ee306ae5bdcc577cf5a5790b"
