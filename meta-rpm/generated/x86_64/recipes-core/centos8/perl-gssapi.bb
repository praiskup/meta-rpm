SUMMARY = "generated recipe based on perl-GSSAPI srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs perl pkgconfig-native"
RPM_SONAME_REQ_perl-GSSAPI = "libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-GSSAPI = "glibc krb5-libs libcom_err perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-GSSAPI-0.28-23.el8.x86_64.rpm \
          "

SRC_URI[perl-GSSAPI.sha256sum] = "299305ae35d644cef509ea2465895079d3c9d51df35a932f44785cc8a9bf20aa"
