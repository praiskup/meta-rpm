SUMMARY = "generated recipe based on hardlink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre2 pkgconfig-native"
RPM_SONAME_REQ_hardlink = "libc.so.6 libpcre2-8.so.0"
RDEPENDS_hardlink = "glibc pcre2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/hardlink-1.3-6.el8.x86_64.rpm \
          "

SRC_URI[hardlink.sha256sum] = "c019e648a047a07284cb870b5fe4bc2a4b65937dbbf0c51699144ee305297bba"
