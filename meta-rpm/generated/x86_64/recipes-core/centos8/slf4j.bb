SUMMARY = "generated recipe based on slf4j srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jcl-over-slf4j = "java-1.8.0-openjdk-headless javapackages-filesystem slf4j"
RDEPENDS_jul-to-slf4j = "java-1.8.0-openjdk-headless javapackages-filesystem slf4j"
RDEPENDS_log4j-over-slf4j = "java-1.8.0-openjdk-headless javapackages-filesystem slf4j"
RDEPENDS_slf4j = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_slf4j-ext = "java-1.8.0-openjdk-headless javapackages-filesystem slf4j"
RDEPENDS_slf4j-javadoc = "javapackages-filesystem"
RDEPENDS_slf4j-jcl = "apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem slf4j"
RDEPENDS_slf4j-jdk14 = "java-1.8.0-openjdk-headless javapackages-filesystem slf4j"
RDEPENDS_slf4j-log4j12 = "java-1.8.0-openjdk-headless javapackages-filesystem log4j12 slf4j"
RDEPENDS_slf4j-sources = "apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem log4j12 slf4j"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jcl-over-slf4j-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jul-to-slf4j-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/log4j-over-slf4j-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-ext-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-javadoc-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-jcl-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-jdk14-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-log4j12-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-manual-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/slf4j-sources-1.7.25-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jcl-over-slf4j.sha256sum] = "c9bfa0b54ec98c0720588215921333e99885ac73f6d68bf8225b1b6222b87ea3"
SRC_URI[jul-to-slf4j.sha256sum] = "740c22e1c922724ed64d40cee0e1fa8087e61f443b63ae7f0784a973264a42bd"
SRC_URI[log4j-over-slf4j.sha256sum] = "1abc2371c63898edc9e2c2fa0492e8caf991e6aa1fc1eec4c75282f6823376fc"
SRC_URI[slf4j.sha256sum] = "f1d32d51d28f5b4acbcd158a55c2e4cc2c7c388fe05bdf3f9ed5a8e9b7d75fad"
SRC_URI[slf4j-ext.sha256sum] = "2661b58d143056ed88abfe98906f6a0d83ae7bfbf1dcda1018223849befdef5d"
SRC_URI[slf4j-javadoc.sha256sum] = "77a7158d70e9daf177e58c271f8dd341a08eb38135ddee730bc452c4a38ff48e"
SRC_URI[slf4j-jcl.sha256sum] = "b91491fdd2964c3cc97926dbc0260157d859b09ea4e64741bb8ea0f10ed70c58"
SRC_URI[slf4j-jdk14.sha256sum] = "1310d90f2b26d656dd0338c0c81b8b85a22b318bfd0ed576882a6abae91bb739"
SRC_URI[slf4j-log4j12.sha256sum] = "e63acde361eb0bd47b7473708ff6bcfb63892eff45d13efdf7898b3bdb468dd9"
SRC_URI[slf4j-manual.sha256sum] = "8ab6ed78cf1c51bf85089ae93008c6a5b8f34201f217f85242e1b3094d7a4db7"
SRC_URI[slf4j-sources.sha256sum] = "f5ab84aa3edf65f0b03a97730fd6e1bc304e87c07cef88739591d2433c465b5b"
