SUMMARY = "generated recipe based on perl-Import-Into srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Import-Into = "perl-Module-Runtime perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Import-Into-1.002005-7.el8.noarch.rpm \
          "

SRC_URI[perl-Import-Into.sha256sum] = "69735be04a11806b1e11d216cbfd114e7c1e449ba510234e1c289ef61c882888"
