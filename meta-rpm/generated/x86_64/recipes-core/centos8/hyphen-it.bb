SUMMARY = "generated recipe based on hyphen-it srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-it = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-it-0.20071127-18.el8.noarch.rpm \
          "

SRC_URI[hyphen-it.sha256sum] = "b79db065bd47c32a7d66a043a07a15a41137ed59983ec602432d1f4fda64bef9"
