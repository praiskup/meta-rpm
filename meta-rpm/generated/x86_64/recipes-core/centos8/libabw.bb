SUMMARY = "generated recipe based on libabw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge libxml2 pkgconfig-native zlib"
RPM_SONAME_PROV_libabw = "libabw-0.1.so.1"
RPM_SONAME_REQ_libabw = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libabw = "glibc libgcc librevenge libstdc++ libxml2 zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libabw-0.1.2-2.el8.x86_64.rpm \
          "

SRC_URI[libabw.sha256sum] = "562b8620b8c5eab2fe43eed540211329d746ec3f690d7fd5b928a06df2ef336c"
