SUMMARY = "generated recipe based on elinks srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 e2fsprogs expat gpm krb5-libs libidn2 lua openssl pkgconfig-native zlib"
RPM_SONAME_REQ_elinks = "libbz2.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libexpat.so.1 libgpm.so.2 libgssapi_krb5.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 liblua-5.3.so libm.so.6 libssl.so.1.1 libz.so.1"
RDEPENDS_elinks = "bash bzip2-libs chkconfig coreutils expat glibc gpm-libs krb5-libs libcom_err libidn2 lua-libs openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/elinks-0.12-0.58.pre6.el8.x86_64.rpm \
          "

SRC_URI[elinks.sha256sum] = "df5761fb479174c71f2ecfa6fdb348c8cbeb6134a3cef34fdf764ce5c9f085bf"
