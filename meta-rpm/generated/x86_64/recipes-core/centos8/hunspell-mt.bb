SUMMARY = "generated recipe based on hunspell-mt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mt = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mt-0.20110414-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-mt.sha256sum] = "808999ba7e8d06e9781a7c737dbf14dbd07a90ca7f6edf30d38b254eb7439b27"
