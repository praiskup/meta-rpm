SUMMARY = "generated recipe based on ibus-libzhuyin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 ibus libgcc libpinyin pkgconfig-native"
RPM_SONAME_REQ_ibus-libzhuyin = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 libm.so.6 libpthread.so.0 libstdc++.so.6 libzhuyin.so.13"
RDEPENDS_ibus-libzhuyin = "bash glib2 glibc ibus ibus-libs libgcc libstdc++ libzhuyin"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-libzhuyin-1.8.93-1.el8.x86_64.rpm \
          "

SRC_URI[ibus-libzhuyin.sha256sum] = "542517ad09ccc5c6ed70bef3cca08154e9335917e52f43a3545da6165be410a4"
