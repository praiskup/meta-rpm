SUMMARY = "generated recipe based on libreswan srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit curl fipscheck ldns libcap-ng libevent libseccomp libselinux libxcrypt nspr nss openldap pam pkgconfig-native systemd-libs unbound"
RPM_SONAME_REQ_libreswan = "libaudit.so.1 libc.so.6 libcap-ng.so.0 libcrypt.so.1 libcurl.so.4 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libfipscheck.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libldns.so.2 libnspr4.so libnss3.so libnssutil3.so libpam.so.0 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsmime3.so libsystemd.so.0 libunbound.so.2"
RDEPENDS_libreswan = "audit-libs bash coreutils fipscheck fipscheck-lib glibc iproute ldns libcap-ng libcurl libevent libseccomp libselinux libxcrypt nspr nss nss-softokn nss-tools nss-util openldap pam platform-python systemd systemd-libs unbound-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreswan-3.29-7.el8_2.x86_64.rpm \
          "

SRC_URI[libreswan.sha256sum] = "edb4d363da8e1f6696043d5a0c087955cb7622f2a28b22f8dbd8a862b11c0e91"
