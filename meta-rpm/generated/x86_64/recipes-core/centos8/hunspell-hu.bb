SUMMARY = "generated recipe based on hunspell-hu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-hu-1.6.1-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-hu.sha256sum] = "86addea1419dda3a2d619d691ce900f05b6620c6f41779f332122f5db7c0bbfb"
