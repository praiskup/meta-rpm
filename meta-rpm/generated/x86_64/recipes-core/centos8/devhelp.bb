SUMMARY = "generated recipe based on devhelp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gsettings-desktop-schemas gtk+3 libsoup-2.4 pango pkgconfig-native webkit2gtk3"
RPM_SONAME_REQ_devhelp = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdevhelp-3.so.5 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libwebkit2gtk-4.0.so.37"
RDEPENDS_devhelp = "atk cairo cairo-gobject devhelp-libs gdk-pixbuf2 glib2 glibc gtk3 libsoup pango webkit2gtk3 webkit2gtk3-jsc"
RPM_SONAME_REQ_devhelp-devel = "libdevhelp-3.so.5"
RPROVIDES_devhelp-devel = "devhelp-dev (= 3.28.1)"
RDEPENDS_devhelp-devel = "devhelp devhelp-libs glib2-devel gsettings-desktop-schemas-devel gtk3-devel pkgconf-pkg-config webkit2gtk3-devel"
RPM_SONAME_PROV_devhelp-libs = "libdevhelp-3.so.5"
RPM_SONAME_REQ_devhelp-libs = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libwebkit2gtk-4.0.so.37"
RDEPENDS_devhelp-libs = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libsoup pango webkit2gtk3 webkit2gtk3-jsc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/devhelp-3.28.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/devhelp-libs-3.28.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/devhelp-devel-3.28.1-5.el8.x86_64.rpm \
          "

SRC_URI[devhelp.sha256sum] = "08a147afeb41d46e28602dd0447105246b8e59caee295eba07b0b05317054244"
SRC_URI[devhelp-devel.sha256sum] = "9cd10961bb4b0bf5bd08ee22356cb17803f33fd8a73f3954fb5f172060b59e1f"
SRC_URI[devhelp-libs.sha256sum] = "7d4e7710bffe8a3754e6f40d989ce4a2edad23f70e16972bf04f8c517f62b009"
