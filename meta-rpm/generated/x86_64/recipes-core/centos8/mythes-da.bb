SUMMARY = "generated recipe based on mythes-da srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-da = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-da-0.20100629.15.16-14.el8.noarch.rpm \
          "

SRC_URI[mythes-da.sha256sum] = "cf9aa51ccd720dd1b790edac68c76cc8c621f47d3bb6a8f57ccf35e5ac793910"
