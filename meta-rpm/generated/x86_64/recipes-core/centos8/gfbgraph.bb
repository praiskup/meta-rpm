SUMMARY = "generated recipe based on gfbgraph srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gnome-online-accounts json-glib libsoup-2.4 pkgconfig-native rest"
RPM_SONAME_PROV_gfbgraph = "libgfbgraph-0.2.so.0"
RPM_SONAME_REQ_gfbgraph = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpthread.so.0 librest-0.7.so.0 libsoup-2.4.so.1"
RDEPENDS_gfbgraph = "glib2 glibc gnome-online-accounts gobject-introspection json-glib libsoup rest"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gfbgraph-0.2.3-6.el8.x86_64.rpm \
          "

SRC_URI[gfbgraph.sha256sum] = "49b39e8107ccb06235b3e56ea36732a1f58365b9282df1f91ff9c7bb05109833"
