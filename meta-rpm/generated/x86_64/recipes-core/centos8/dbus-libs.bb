SUMMARY = "generated recipe based on dbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemd-libs"
RPM_SONAME_PROV_dbus-libs = "libdbus-1.so.3"
RPM_SONAME_REQ_dbus-libs = "libc.so.6 libpthread.so.0 libsystemd.so.0"
RDEPENDS_dbus-libs = "glibc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dbus-libs-1.12.8-10.el8_2.x86_64.rpm \
          "

SRC_URI[dbus-libs.sha256sum] = "0009b1a0081e4d33c4581103309adac4fb87fbb588af2c12c6fde6be39573ca3"
