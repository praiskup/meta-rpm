SUMMARY = "generated recipe based on perl-Sys-CPU srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-CPU = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-CPU = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Sys-CPU-0.61-14.el8.x86_64.rpm \
          "

SRC_URI[perl-Sys-CPU.sha256sum] = "c232dd6e6a076181bf9429920abb8db1ead265bded05ebc7f6deff94129cf19f"
