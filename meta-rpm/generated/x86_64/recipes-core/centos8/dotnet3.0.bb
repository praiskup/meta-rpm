SUMMARY = "generated recipe based on dotnet3.0 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl krb5-libs libgcc lttng-ust openssl pkgconfig-native zlib"
RDEPENDS_aspnetcore-runtime-3.0 = "dotnet-runtime-3.0"
RDEPENDS_aspnetcore-targeting-pack-3.0 = "dotnet-host"
RPM_SONAME_PROV_dotnet-apphost-pack-3.0 = "libnethost.so"
RPM_SONAME_REQ_dotnet-apphost-pack-3.0 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dotnet-apphost-pack-3.0 = "dotnet-host glibc libgcc libstdc++"
RPM_SONAME_REQ_dotnet-hostfxr-3.0 = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_dotnet-hostfxr-3.0 = "dotnet-host glibc libgcc libstdc++"
RPM_SONAME_REQ_dotnet-runtime-3.0 = "ld-linux-x86-64.so.2 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 liblttng-ust.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_dotnet-runtime-3.0 = "dotnet-hostfxr-3.0 glibc krb5-libs libcurl libgcc libicu libstdc++ lttng-ust openssl-libs zlib"
RPM_SONAME_REQ_dotnet-sdk-3.0 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dotnet-sdk-3.0 = "aspnetcore-runtime-3.0 aspnetcore-targeting-pack-3.0 dotnet-apphost-pack-3.0 dotnet-runtime-3.0 dotnet-targeting-pack-3.0 dotnet-templates-3.0 glibc libgcc libstdc++ netstandard-targeting-pack-2.1"
RDEPENDS_dotnet-targeting-pack-3.0 = "dotnet-host"
RDEPENDS_dotnet-templates-3.0 = "dotnet-host"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aspnetcore-runtime-3.0-3.0.3-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aspnetcore-targeting-pack-3.0-3.0.3-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-apphost-pack-3.0-3.0.3-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-hostfxr-3.0-3.0.3-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-runtime-3.0-3.0.3-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-sdk-3.0-3.0.103-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-targeting-pack-3.0-3.0.3-1.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-templates-3.0-3.0.103-1.el8_1.x86_64.rpm \
          "

SRC_URI[aspnetcore-runtime-3.0.sha256sum] = "e7334cfc3df87f034c84930946d919f3d9ddce8aedfd4f3a07874d8b2ed1dfa9"
SRC_URI[aspnetcore-targeting-pack-3.0.sha256sum] = "8fe9f4221abcc79e5ae4a7e906bb84c62faffd27a580aded29d2456f6276af77"
SRC_URI[dotnet-apphost-pack-3.0.sha256sum] = "317a95b853c341bb72cf3cb2d846d02d666c1229b8df686eeb25eee007b5e7d2"
SRC_URI[dotnet-hostfxr-3.0.sha256sum] = "6293a80de89d311801437cb36450a33fbb2d1658e947856094230655cc92a680"
SRC_URI[dotnet-runtime-3.0.sha256sum] = "caf8890f5ce396f41415153b96fa8b9b06903317c48215e31c137b19477be9b3"
SRC_URI[dotnet-sdk-3.0.sha256sum] = "c771ca60f4cd76920dc45ff0eab778c8dab1d0f3f8be3709ca6fb770703b4962"
SRC_URI[dotnet-targeting-pack-3.0.sha256sum] = "3031625d029533f6eb046b35b2ef09268b069880be1c877b63bfc01383b2b723"
SRC_URI[dotnet-templates-3.0.sha256sum] = "2bc2ffa71dbc178e151645ab646d1a74d3625c175c16129ab48618af4d2da5cb"
