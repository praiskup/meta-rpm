SUMMARY = "generated recipe based on irqbalance srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libcap-ng ncurses numactl pkgconfig-native"
RPM_SONAME_REQ_irqbalance = "libc.so.6 libcap-ng.so.0 libglib-2.0.so.0 libm.so.6 libncursesw.so.6 libnuma.so.1 libtinfo.so.6"
RDEPENDS_irqbalance = "bash glib2 glibc libcap-ng ncurses-libs numactl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/irqbalance-1.4.0-4.el8.x86_64.rpm \
          "

SRC_URI[irqbalance.sha256sum] = "3c8d219ba276be8c07a4ccc7323f745fd785f9d1372b9d817b75dd57a6fd990f"
