SUMMARY = "generated recipe based on python-evdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-evdev = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-evdev = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-evdev-1.1.2-3.el8.x86_64.rpm \
          "

SRC_URI[python3-evdev.sha256sum] = "9144786f7d84adcbd27b71d0176179b91347852b27936de48dd0dfe7a8fa5e7f"
