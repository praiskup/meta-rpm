SUMMARY = "generated recipe based on cmake srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl expat libarchive libgcc libuv pkgconfig-native qt5-qtbase zlib"
RPM_SONAME_REQ_cmake-gui = "libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libarchive.so.13 libc.so.6 libcurl.so.4 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libuv.so.1 libz.so.1"
RDEPENDS_cmake-gui = "bash cmake expat glibc hicolor-icon-theme libarchive libcurl libgcc libstdc++ libuv qt5-qtbase qt5-qtbase-gui shared-mime-info zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cmake-gui-3.11.4-7.el8.x86_64.rpm \
          "

SRC_URI[cmake-gui.sha256sum] = "a0be758fb2d35615b8706cf65c40127384fcd4f33b3e5741c42e57111c10015c"
