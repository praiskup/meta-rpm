SUMMARY = "generated recipe based on fuse-sshfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fuse glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_fuse-sshfs = "libc.so.6 libfuse.so.2 libglib-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0"
RDEPENDS_fuse-sshfs = "fuse fuse-libs glib2 glibc openssh-clients"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fuse-sshfs-2.8-5.el8.x86_64.rpm \
          "

SRC_URI[fuse-sshfs.sha256sum] = "d9d54ade0bc77a95aeb39a25ac60e52c89832c4f5c1dfdec6f81f0a69a4e1f47"
