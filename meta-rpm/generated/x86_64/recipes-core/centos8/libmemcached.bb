SUMMARY = "generated recipe based on libmemcached srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib libevent libgcc pkgconfig-native"
RPM_SONAME_REQ_libmemcached = "libc.so.6 libdl.so.2 libevent-2.1.so.6 libgcc_s.so.1 libm.so.6 libmemcached.so.11 libmemcachedutil.so.2 libpthread.so.0 libsasl2.so.3 libstdc++.so.6"
RDEPENDS_libmemcached = "cyrus-sasl-lib glibc libevent libgcc libmemcached-libs libstdc++"
RPM_SONAME_REQ_libmemcached-devel = "libhashkit.so.2 libmemcached.so.11 libmemcachedprotocol.so.0 libmemcachedutil.so.2"
RPROVIDES_libmemcached-devel = "libmemcached-dev (= 1.0.18)"
RDEPENDS_libmemcached-devel = "cyrus-sasl-devel libmemcached libmemcached-libs pkgconf-pkg-config"
RPM_SONAME_PROV_libmemcached-libs = "libhashkit.so.2 libmemcached.so.11 libmemcachedprotocol.so.0 libmemcachedutil.so.2"
RPM_SONAME_REQ_libmemcached-libs = "libc.so.6 libdl.so.2 libevent-2.1.so.6 libgcc_s.so.1 libm.so.6 libmemcached.so.11 libpthread.so.0 libsasl2.so.3 libstdc++.so.6"
RDEPENDS_libmemcached-libs = "cyrus-sasl-lib glibc libevent libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmemcached-1.0.18-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmemcached-libs-1.0.18-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmemcached-devel-1.0.18-15.el8.x86_64.rpm \
          "

SRC_URI[libmemcached.sha256sum] = "a98237988be726539deb640103644929fcb0a3ffe5a4fcff1d488b866374b253"
SRC_URI[libmemcached-devel.sha256sum] = "5f903c230ff885fa0a1885ed3ec1a1270881af1381a2add2b414e56b0eb83fdf"
SRC_URI[libmemcached-libs.sha256sum] = "412be2f692dbd7b761ba2cf35f6bbdf5c6af7ad862689f49399e3a176744f870"
