SUMMARY = "generated recipe based on librabbitmq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_librabbitmq = "librabbitmq.so.4"
RPM_SONAME_REQ_librabbitmq = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 librt.so.1 libssl.so.1.1"
RDEPENDS_librabbitmq = "glibc openssl-libs"
RPM_SONAME_REQ_librabbitmq-devel = "librabbitmq.so.4"
RPROVIDES_librabbitmq-devel = "librabbitmq-dev (= 0.9.0)"
RDEPENDS_librabbitmq-devel = "librabbitmq openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/librabbitmq-0.9.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librabbitmq-devel-0.9.0-1.el8.x86_64.rpm \
          "

SRC_URI[librabbitmq.sha256sum] = "0fd2ebe0111c7c6dc36c8e1505ba1ce474ee7a142b2145d6059038c9537c33d8"
SRC_URI[librabbitmq-devel.sha256sum] = "482ae87aaa1c4a9372738a5e3def086c72a85e4f53df3cb339753e55636ae226"
