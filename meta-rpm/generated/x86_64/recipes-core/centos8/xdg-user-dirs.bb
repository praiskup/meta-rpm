SUMMARY = "generated recipe based on xdg-user-dirs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xdg-user-dirs = "libc.so.6"
RDEPENDS_xdg-user-dirs = "bash filesystem glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xdg-user-dirs-0.17-1.el8.x86_64.rpm \
          "

SRC_URI[xdg-user-dirs.sha256sum] = "4e35793ef9f267ade56e8776929459bf0c012fe7380f8ce955c1b6d9f7231135"
