SUMMARY = "generated recipe based on libxklavier srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libx11 libxi libxkbfile libxml2 pkgconfig-native"
RPM_SONAME_PROV_libxklavier = "libxklavier.so.16"
RPM_SONAME_REQ_libxklavier = "libX11.so.6 libXi.so.6 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libxkbfile.so.1 libxml2.so.2"
RDEPENDS_libxklavier = "glib2 glibc iso-codes libX11 libXi libxkbfile libxml2"
RPM_SONAME_REQ_libxklavier-devel = "libxklavier.so.16"
RPROVIDES_libxklavier-devel = "libxklavier-dev (= 5.4)"
RDEPENDS_libxklavier-devel = "glib2-devel libxklavier libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxklavier-5.4-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libxklavier-devel-5.4-11.el8.x86_64.rpm \
          "

SRC_URI[libxklavier.sha256sum] = "2b8f844580fbb39eb371c4a986c100c0fb1e985c1bcbb84e9c0b94c34b9b4bbb"
SRC_URI[libxklavier-devel.sha256sum] = "7200dea21f4de6ed60ddf9223759fcccf2da9d60537dc09575d66fc820701148"
