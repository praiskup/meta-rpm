SUMMARY = "generated recipe based on paktype-naqsh-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_paktype-naqsh-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/paktype-naqsh-fonts-4.1-8.el8.noarch.rpm \
          "

SRC_URI[paktype-naqsh-fonts.sha256sum] = "e33a96ba0d7349689c284200ad192040c395e589f66bcf8d2cb5549ebdb20b2c"
