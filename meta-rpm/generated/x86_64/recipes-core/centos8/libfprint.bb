SUMMARY = "generated recipe based on libfprint srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libgusb nss pixman pkgconfig-native"
RPM_SONAME_PROV_libfprint = "libfprint.so.2"
RPM_SONAME_REQ_libfprint = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgusb.so.2 libm.so.6 libnss3.so libpixman-1.so.0"
RDEPENDS_libfprint = "glib2 glibc libgcc libgusb nss pixman"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libfprint-1.90.0-4.el8.x86_64.rpm \
          "

SRC_URI[libfprint.sha256sum] = "33be16d31ecce80032493d550b78e75c2f73866c60046a86c407ecefe82503df"
