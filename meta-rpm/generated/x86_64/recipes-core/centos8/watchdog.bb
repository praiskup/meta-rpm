SUMMARY = "generated recipe based on watchdog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtirpc pkgconfig-native"
RPM_SONAME_REQ_watchdog = "libc.so.6 libtirpc.so.3"
RDEPENDS_watchdog = "bash glibc libtirpc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/watchdog-5.15-1.el8.x86_64.rpm \
          "

SRC_URI[watchdog.sha256sum] = "26f49aff1b2fb2eb88f81f616aa1a8bc5280671b4d8ea04c273b73c9c93cf5a1"
