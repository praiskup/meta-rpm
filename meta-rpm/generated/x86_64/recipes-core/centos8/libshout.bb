SUMMARY = "generated recipe based on libshout srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libogg libtheora libvorbis pkgconfig-native speex"
RPM_SONAME_PROV_libshout = "libshout.so.3"
RPM_SONAME_REQ_libshout = "libc.so.6 libogg.so.0 libpthread.so.0 libspeex.so.1 libtheora.so.0 libvorbis.so.0"
RDEPENDS_libshout = "glibc libogg libtheora libvorbis speex"
RPM_SONAME_REQ_libshout-devel = "libshout.so.3"
RPROVIDES_libshout-devel = "libshout-dev (= 2.2.2)"
RDEPENDS_libshout-devel = "libogg-devel libshout libtheora-devel libvorbis-devel pkgconf-pkg-config speex-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libshout-2.2.2-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libshout-devel-2.2.2-19.el8.x86_64.rpm \
          "

SRC_URI[libshout.sha256sum] = "61902f4afbbfa046dc61f570fc5420f8991d3e159cc33783aa63d4ec28e6e183"
SRC_URI[libshout-devel.sha256sum] = "965fb9572d564fceef37ec61bb8eaec98f4c3b52ba3afb4d22b44b713a35b7be"
