SUMMARY = "generated recipe based on centos-release srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_centos-release = "centos-repos"
RDEPENDS_centos-repos = "centos-gpg-keys centos-release"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-gpg-keys-8.2-2.2004.0.2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-release-8.2-2.2004.0.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-repos-8.2-2.2004.0.2.el8.x86_64.rpm \
          "

SRC_URI[centos-gpg-keys.sha256sum] = "5678556b2344231639ffaa7f43563237dae3862f5652d23ce282f71637ec7255"
SRC_URI[centos-release.sha256sum] = "d7b190a06c8ace7b26486e7315bdefa747a883961518a3f1c7726f3c28b9e425"
SRC_URI[centos-repos.sha256sum] = "e718e6bd95bc2d186e8e2a3e563efe142d3d17fe4d5ab2c6779e1f03e64846fe"
