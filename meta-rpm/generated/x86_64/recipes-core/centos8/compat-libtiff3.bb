SUMMARY = "generated recipe based on compat-libtiff3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libjpeg-turbo pkgconfig-native zlib"
RPM_SONAME_PROV_compat-libtiff3 = "libtiff.so.3 libtiffxx.so.3"
RPM_SONAME_REQ_compat-libtiff3 = "libc.so.6 libgcc_s.so.1 libjpeg.so.62 libm.so.6 libstdc++.so.6 libtiff.so.3 libz.so.1"
RDEPENDS_compat-libtiff3 = "glibc libgcc libjpeg-turbo libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/compat-libtiff3-3.9.4-13.el8.x86_64.rpm \
          "

SRC_URI[compat-libtiff3.sha256sum] = "c331abbbf8a6fe35a408da2ad337f7a7974a157ca44c737b4c20dfdf70312409"
