SUMMARY = "generated recipe based on perl-MIME-Base64 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-MIME-Base64 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-MIME-Base64 = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-MIME-Base64-3.15-396.el8.x86_64.rpm \
          "

SRC_URI[perl-MIME-Base64.sha256sum] = "5642297bf32bb174173917dd10fd2a3a2ef7277c599f76c0669c5c448f10bdaf"
