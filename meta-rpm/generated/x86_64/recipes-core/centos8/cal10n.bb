SUMMARY = "generated recipe based on cal10n srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_cal10n = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_cal10n-javadoc = "javapackages-filesystem"
RDEPENDS_maven-cal10n-plugin = "cal10n java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact-manager maven-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cal10n-0.8.1-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cal10n-javadoc-0.8.1-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-cal10n-plugin-0.8.1-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[cal10n.sha256sum] = "2153ac806f931fe7c09abaff92ca3f6fe66c99e554cc34f6e063d26a13de5054"
SRC_URI[cal10n-javadoc.sha256sum] = "b26d78719f0234edc7049d942fe73c795791e0d7ed85b6591464bdafaecf5818"
SRC_URI[maven-cal10n-plugin.sha256sum] = "2806462201c218366883c10476e80feea4b3da6ab0a0180caa79b415dd0caf70"
