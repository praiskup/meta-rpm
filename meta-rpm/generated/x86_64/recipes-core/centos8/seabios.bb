SUMMARY = "generated recipe based on seabios srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_seabios = "seabios-bin seavgabios-bin"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/seabios-1.11.1-4.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/seabios-bin-1.11.1-4.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/seavgabios-bin-1.11.1-4.module_el8.2.0+320+13f867d7.noarch.rpm \
          "

SRC_URI[seabios.sha256sum] = "d544062b73b7bbb88bcaea110cdd105b257c4bc0bf2c68a379cac710ea428ff6"
SRC_URI[seabios-bin.sha256sum] = "dc7776fcc322c8059a1fdb04bb7cd8c456db037ace76ea0565135db2aed8255a"
SRC_URI[seavgabios-bin.sha256sum] = "e98b7b2258fc2cf1a8a9bba2e75ee1511e0b81e291b5dad476693dcd0b84b232"
