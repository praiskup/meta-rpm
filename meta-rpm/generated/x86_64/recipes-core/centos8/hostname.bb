SUMMARY = "generated recipe based on hostname srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_hostname = "libc.so.6"
RDEPENDS_hostname = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/hostname-3.20-6.el8.x86_64.rpm \
          "

SRC_URI[hostname.sha256sum] = "7a7963e2052bfb45b8569f64619dc5cb92fe8aeb78c754b7cf948b9784646785"
