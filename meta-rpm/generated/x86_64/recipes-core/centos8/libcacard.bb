SUMMARY = "generated recipe based on libcacard srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 nspr nss pkgconfig-native"
RPM_SONAME_PROV_libcacard = "libcacard.so.0"
RPM_SONAME_REQ_libcacard = "libc.so.6 libglib-2.0.so.0 libnspr4.so libnss3.so"
RDEPENDS_libcacard = "glib2 glibc nspr nss"
RPM_SONAME_REQ_libcacard-devel = "libcacard.so.0"
RPROVIDES_libcacard-devel = "libcacard-dev (= 2.7.0)"
RDEPENDS_libcacard-devel = "glib2-devel libcacard nss-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libcacard-2.7.0-2.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcacard-devel-2.7.0-2.el8_1.x86_64.rpm \
          "

SRC_URI[libcacard.sha256sum] = "f1278a5d0bd7267c8f53f40c30ef6f33ae3696407f5ee8c1617d31cdeee3ee03"
SRC_URI[libcacard-devel.sha256sum] = "37b1b4a51fbf37b3cedc42b6fb6a79b3d80a5b389e487878e82420120f13100a"
