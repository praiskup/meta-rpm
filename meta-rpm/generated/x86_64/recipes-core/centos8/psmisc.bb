SUMMARY = "generated recipe based on psmisc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux ncurses pkgconfig-native"
RPM_SONAME_REQ_psmisc = "libc.so.6 libselinux.so.1 libtinfo.so.6"
RDEPENDS_psmisc = "glibc libselinux ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/psmisc-23.1-4.el8.x86_64.rpm \
          "

SRC_URI[psmisc.sha256sum] = "e12b1fbdfb203c0c925eab4f90c2a49ed80b40ff7e1daa698671f0a29a1b7316"
