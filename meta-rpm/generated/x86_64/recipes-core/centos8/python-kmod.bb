SUMMARY = "generated recipe based on python-kmod srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "kmod pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-kmod = "libc.so.6 libkmod.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-kmod = "glibc kmod-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-kmod-0.9-20.el8.x86_64.rpm \
          "

SRC_URI[python3-kmod.sha256sum] = "497988e0659eaca480e1f92fbaa025357ab8b47c6df0cd1bb6b6c41a8e703c84"
