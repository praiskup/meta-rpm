SUMMARY = "generated recipe based on libtiff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jbigkit libgcc libjpeg-turbo pkgconf pkgconfig-native zlib"
RPM_SONAME_PROV_libtiff = "libtiff.so.5 libtiffxx.so.5"
RPM_SONAME_REQ_libtiff = "libc.so.6 libgcc_s.so.1 libjbig.so.2.1 libjpeg.so.62 libm.so.6 libstdc++.so.6 libtiff.so.5 libz.so.1"
RDEPENDS_libtiff = "glibc jbigkit-libs libgcc libjpeg-turbo libstdc++ zlib"
RPM_SONAME_REQ_libtiff-devel = "libtiff.so.5 libtiffxx.so.5"
RPROVIDES_libtiff-devel = "libtiff-dev (= 4.0.9)"
RDEPENDS_libtiff-devel = "libtiff pkgconf-pkg-config"
RPM_SONAME_REQ_libtiff-tools = "libc.so.6 libjbig.so.2.1 libjpeg.so.62 libm.so.6 libtiff.so.5 libz.so.1"
RDEPENDS_libtiff-tools = "glibc jbigkit-libs libjpeg-turbo libtiff zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtiff-4.0.9-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtiff-devel-4.0.9-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libtiff-tools-4.0.9-17.el8.x86_64.rpm \
          "

SRC_URI[libtiff.sha256sum] = "2ef49bdb9bd52ce75c03161bbb73dcb63ce12f78000dc58c253da478312d67e1"
SRC_URI[libtiff-devel.sha256sum] = "605386f3deb111045cd466a2629d747c4139d7d0a66f021f3e1029d341f58554"
SRC_URI[libtiff-tools.sha256sum] = "c350849001636a5c5229abe5b1f0b5e98e2662bcaf6c1579063501d4f081a7d1"
