SUMMARY = "generated recipe based on GConf2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus dbus-glib dbus-libs glib-2.0 libxml2 pkgconfig-native polkit"
RPM_SONAME_PROV_GConf2 = "libgconf-2.so.4 libgconfbackend-oldxml.so libgconfbackend-xml.so libgsettingsgconfbackend.so"
RPM_SONAME_REQ_GConf2 = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libgconf-2.so.4 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libxml2.so.2"
RDEPENDS_GConf2 = "bash dbus dbus-glib dbus-libs glib2 glibc libxml2 polkit-libs psmisc"
RPM_SONAME_REQ_GConf2-devel = "libgconf-2.so.4"
RPROVIDES_GConf2-devel = "GConf2-dev (= 3.2.6)"
RDEPENDS_GConf2-devel = "GConf2 automake dbus-devel glib2-devel libxml2-devel pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/GConf2-3.2.6-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/GConf2-devel-3.2.6-22.el8.x86_64.rpm \
          "

SRC_URI[GConf2.sha256sum] = "9d62a41fdb82c1e7564bc0228e046dbc013f420705712aff1b873db8d617004c"
SRC_URI[GConf2-devel.sha256sum] = "f2c9754900699754502110d4094339145b20475d0cb25cd396c40096e013480c"
