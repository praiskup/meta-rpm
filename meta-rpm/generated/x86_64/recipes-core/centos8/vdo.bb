SUMMARY = "generated recipe based on vdo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-event device-mapper-libs libuuid pkgconfig-native zlib"
RPM_SONAME_REQ_vdo = "libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1 libuuid.so.1 libz.so.1"
RDEPENDS_vdo = "bash device-mapper-event-libs device-mapper-libs glibc kmod-kvdo libuuid lvm2 platform-python python3-pyyaml systemd util-linux zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/vdo-6.2.2.117-13.el8.x86_64.rpm \
          "

SRC_URI[vdo.sha256sum] = "44be752d660e1f9a24abda3232ab46cddf5e3cf50fba20c685a6f543d1c9ffbc"
