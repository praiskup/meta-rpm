SUMMARY = "generated recipe based on perl-Digest-SHA srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-SHA = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-SHA = "glibc perl-Carp perl-Digest perl-Exporter perl-Getopt-Long perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Digest-SHA-6.02-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Digest-SHA.sha256sum] = "4606c33595cf9d39cf9127ef3d21e634b99805ff22968dc15b5c36a9ffdaf243"
