SUMMARY = "generated recipe based on apr-util srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "apr db expat libpq libuuid libxcrypt mariadb-connector-c openldap openssl pkgconfig-native sqlite3 unixodbc zlib"
RPM_SONAME_PROV_apr-util = "libaprutil-1.so.0"
RPM_SONAME_REQ_apr-util = "libapr-1.so.0 libc.so.6 libcrypt.so.1 libdl.so.2 libexpat.so.1 libpthread.so.0 libuuid.so.1"
RDEPENDS_apr-util = "apr expat glibc libuuid libxcrypt"
RPM_SONAME_REQ_apr-util-bdb = "libc.so.6 libdb-5.3.so libpthread.so.0"
RDEPENDS_apr-util-bdb = "apr-util glibc libdb"
RPM_SONAME_REQ_apr-util-devel = "libaprutil-1.so.0"
RPROVIDES_apr-util-devel = "apr-util-dev (= 1.6.1)"
RDEPENDS_apr-util-devel = "apr-devel apr-util bash expat-devel libdb-devel openldap-devel pkgconf-pkg-config"
RPM_SONAME_REQ_apr-util-ldap = "libc.so.6 liblber-2.4.so.2 libldap_r-2.4.so.2 libpthread.so.0"
RDEPENDS_apr-util-ldap = "apr-util glibc openldap"
RPM_SONAME_REQ_apr-util-mysql = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_apr-util-mysql = "apr-util glibc mariadb-connector-c openssl-libs zlib"
RPM_SONAME_REQ_apr-util-odbc = "libc.so.6 libodbc.so.2 libpthread.so.0"
RDEPENDS_apr-util-odbc = "apr-util glibc unixODBC"
RPM_SONAME_REQ_apr-util-openssl = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 libssl.so.1.1"
RDEPENDS_apr-util-openssl = "apr-util glibc openssl-libs"
RPM_SONAME_REQ_apr-util-pgsql = "libc.so.6 libpq.so.5 libpthread.so.0"
RDEPENDS_apr-util-pgsql = "apr-util glibc libpq"
RPM_SONAME_REQ_apr-util-sqlite = "libc.so.6 libpthread.so.0 libsqlite3.so.0"
RDEPENDS_apr-util-sqlite = "apr-util glibc sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-bdb-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-devel-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-ldap-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-mysql-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-odbc-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-openssl-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-pgsql-1.6.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-util-sqlite-1.6.1-6.el8.x86_64.rpm \
          "

SRC_URI[apr-util.sha256sum] = "127a5b732740676361ff9442155367cac8f4a28e635254c73db0e68cbd573463"
SRC_URI[apr-util-bdb.sha256sum] = "1df020861fb756af3aa6e4eb23f420d25313819019f2ebd625c20f59362e2a68"
SRC_URI[apr-util-devel.sha256sum] = "5296eecc8a3092b2b8a4cd801aa280451f19ef7053fb050cf93f1b4dc1f1c139"
SRC_URI[apr-util-ldap.sha256sum] = "c09691c43bed17ff019ec6e2ee697ef7d0815ac7ad930c59539df28861a9ef77"
SRC_URI[apr-util-mysql.sha256sum] = "9f6390a33df2f42c9576c3e76c86744b669f1c24219a898315f85e8d22fe44b9"
SRC_URI[apr-util-odbc.sha256sum] = "cc2c1a8eae12314f1ff37ad76c2e9c42b80b7954abf2208128d0fe595a2de6fc"
SRC_URI[apr-util-openssl.sha256sum] = "9418d2f512c78ca94b018750571e122a9840507e2701b37f0a787157cfc6a448"
SRC_URI[apr-util-pgsql.sha256sum] = "ed8c0602e5b12ddba0b1d5474840d72d386053fe6d1d6af5edbe17d6168d5246"
SRC_URI[apr-util-sqlite.sha256sum] = "9b8944d9e03eaaade0fc237240bbc15c0bf884d3564ac83f7c7442fb9e53b8da"
