SUMMARY = "generated recipe based on libqb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libqb = "libqb.so.0"
RPM_SONAME_REQ_libqb = "libc.so.6 libdl.so.2 libpthread.so.0 libqb.so.0"
RDEPENDS_libqb = "glibc"
RPM_SONAME_REQ_libqb-devel = "libqb.so.0"
RPROVIDES_libqb-devel = "libqb-dev (= 1.0.3)"
RDEPENDS_libqb-devel = "libqb pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libqb-1.0.3-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libqb-devel-1.0.3-10.el8.x86_64.rpm \
          "

SRC_URI[libqb.sha256sum] = "2556edde4d5ce0eda31e6f811be3353c84471c95e50f8c4a423cab9914ebc92f"
SRC_URI[libqb-devel.sha256sum] = "92afb369c9a4e8b846c3ddc77a90362286eec7a88360099e9fe7daa556a75f6b"
