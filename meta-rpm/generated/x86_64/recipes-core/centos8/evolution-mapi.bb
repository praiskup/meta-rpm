SUMMARY = "generated recipe based on evolution-mapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo evolution evolution-data-server gdk-pixbuf glib-2.0 gtk+3 libical libsecret libsoup-2.4 libtalloc libtevent libxml2 nspr nss openchange pango pkgconfig-native samba sqlite3 webkit2gtk3"
RPM_SONAME_PROV_evolution-mapi = "libcamelmapi-priv.so libcamelmapi.so libebookbackendmapi.so libecalbackendmapi.so libevolution-mapi.so"
RPM_SONAME_REQ_evolution-mapi = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcamel-1.2.so.61 libcamelmapi-priv.so libdcerpc-binding.so.0 libdcerpc.so.0 libdl.so.2 libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libecal-1.2.so.19 libedata-book-1.2.so.25 libedata-cal-1.2.so.28 libedataserver-1.2.so.23 libedataserverui-1.2.so.2 libemail-engine.so libevolution-mail-composer.so libevolution-mail-formatter.so libevolution-mail.so libevolution-mapi.so libevolution-shell.so libevolution-util.so libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libical.so.3 libicalss.so.3 libicalvcal.so.3 libjavascriptcoregtk-4.0.so.18 libmapi-openchange.so.0 libndr.so.0 libnspr4.so libnss3.so libnssutil3.so libpango-1.0.so.0 libpangocairo-1.0.so.0 libplc4.so libplds4.so libpthread.so.0 libsamba-util.so.0 libsecret-1.so.0 libsmime3.so libsoup-2.4.so.1 libsqlite3.so.0 libssl3.so libtalloc.so.2 libtevent.so.0 libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_evolution-mapi = "atk cairo cairo-gobject evolution evolution-data-server evolution-mapi-langpacks gdk-pixbuf2 glib2 glibc gtk3 libical libsecret libsoup libtalloc libtevent libxml2 nspr nss nss-util openchange pango samba-client-libs sqlite-libs webkit2gtk3 webkit2gtk3-jsc"
RDEPENDS_evolution-mapi-langpacks = "evolution-mapi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evolution-mapi-3.28.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/evolution-mapi-langpacks-3.28.3-2.el8.noarch.rpm \
          "

SRC_URI[evolution-mapi.sha256sum] = "b040a83365ed32a4fa1bcdc0279910225d5b56c28adab1539aedeee7d7db9b5b"
SRC_URI[evolution-mapi-langpacks.sha256sum] = "8f191b77eecb39e6e115e8eb624263abf4d11bc94bcef8013086aed43b22730f"
