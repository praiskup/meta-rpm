SUMMARY = "generated recipe based on perl-Scalar-List-Utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Scalar-List-Utils = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Scalar-List-Utils = "glibc perl-Carp perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Scalar-List-Utils-1.49-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Scalar-List-Utils.sha256sum] = "3db0d05ca5ba00981312f3a3ddcbabf466c2f1fc639cbf29482bb2cd952df456"
