SUMMARY = "generated recipe based on python-packaging srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-packaging = "platform-python python3-pyparsing python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-packaging-16.8-9.el8.noarch.rpm \
          "

SRC_URI[python3-packaging.sha256sum] = "4249be2c0617052955a2897d5694ab88b057dd4325c5fd97cfbaaa5bddd352e9"
