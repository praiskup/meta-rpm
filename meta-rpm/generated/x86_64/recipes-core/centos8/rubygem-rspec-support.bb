SUMMARY = "generated recipe based on rubygem-rspec-support srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec-support = "rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rubygem-rspec-support-3.7.1-2.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec-support.sha256sum] = "ce2123d27870f6c0121a275dc250fe85f2d63486a68104c22246589f5a5aa692"
