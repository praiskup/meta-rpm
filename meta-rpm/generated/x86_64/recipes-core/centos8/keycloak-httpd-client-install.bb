SUMMARY = "generated recipe based on keycloak-httpd-client-install srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_keycloak-httpd-client-install = "python3-keycloak-httpd-client-install"
RDEPENDS_python3-keycloak-httpd-client-install = "keycloak-httpd-client-install platform-python python3-jinja2 python3-requests python3-requests-oauthlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/keycloak-httpd-client-install-1.0-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-keycloak-httpd-client-install-1.0-2.el8.noarch.rpm \
          "

SRC_URI[keycloak-httpd-client-install.sha256sum] = "c6287b6909b1b843e03450880590d24e3b842902663ef553af9fe08b8a60f10c"
SRC_URI[python3-keycloak-httpd-client-install.sha256sum] = "3f3af534597693d33a4a5e677dc1cf4aa61b800639a426dc43e3ddf5e39f2a94"
