SUMMARY = "generated recipe based on python-pycparser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pycparser = "platform-python python3-ply"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pycparser-2.14-14.el8.noarch.rpm \
          "

SRC_URI[python3-pycparser.sha256sum] = "8891a9a4707611c13a5693b195201dd940254ffdb03cf5742952329282bb8cb7"
