SUMMARY = "generated recipe based on python-ldap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openldap pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-ldap = "libc.so.6 liblber-2.4.so.2 libldap_r-2.4.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-ldap = "bash glibc openldap platform-python platform-python-setuptools python3-libs python3-pyasn1 python3-pyasn1-modules"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-ldap-3.1.0-5.el8.x86_64.rpm \
          "

SRC_URI[python3-ldap.sha256sum] = "7090a6565a276734a4efd5ec5c1d99c581dc71e98b0fff8a02d54b176214da5d"
