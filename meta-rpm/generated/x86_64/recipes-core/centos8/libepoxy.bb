SUMMARY = "generated recipe based on libepoxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libglvnd pkgconfig-native"
RPM_SONAME_PROV_libepoxy = "libepoxy.so.0"
RPM_SONAME_REQ_libepoxy = "libc.so.6 libdl.so.2"
RDEPENDS_libepoxy = "glibc"
RPM_SONAME_REQ_libepoxy-devel = "libepoxy.so.0"
RPROVIDES_libepoxy-devel = "libepoxy-dev (= 1.5.3)"
RDEPENDS_libepoxy-devel = "libepoxy libglvnd-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libepoxy-1.5.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libepoxy-devel-1.5.3-1.el8.x86_64.rpm \
          "

SRC_URI[libepoxy.sha256sum] = "6200ef78c4e21a34056c28d8729d6e876da302f7ef181a1b9bfad28970e3a366"
SRC_URI[libepoxy-devel.sha256sum] = "cbab1c5522f03d873a3e92b2871d1aaf30aa44f2c448bbae6325dd570caf5c8c"
