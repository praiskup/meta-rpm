SUMMARY = "generated recipe based on graphite2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_graphite2 = "libgraphite2.so.3"
RPM_SONAME_REQ_graphite2 = "libc.so.6 libgcc_s.so.1 libgraphite2.so.3 libm.so.6 libstdc++.so.6"
RDEPENDS_graphite2 = "glibc libgcc libstdc++"
RPM_SONAME_REQ_graphite2-devel = "libgraphite2.so.3"
RPROVIDES_graphite2-devel = "graphite2-dev (= 1.3.10)"
RDEPENDS_graphite2-devel = "graphite2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/graphite2-1.3.10-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/graphite2-devel-1.3.10-10.el8.x86_64.rpm \
          "

SRC_URI[graphite2.sha256sum] = "0f9c3ee5f54ed296f99219bd70fa4f869c4c9986e3766d813a76a0cc5ecee24e"
SRC_URI[graphite2-devel.sha256sum] = "7dc165e7913e7d8fc18a5e98595fdd0b2f0723b6582b10394075550202771f0f"
