SUMMARY = "generated recipe based on hunspell-mi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mi-0.20080630-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-mi.sha256sum] = "a83f9289be0473c75965a93ba64a80cf8f6b014464af93f2338d6b928f3b46fb"
