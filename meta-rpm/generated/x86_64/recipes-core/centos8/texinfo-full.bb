SUMMARY = "generated recipe based on texinfo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_texinfo = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-Getopt-Long perl-MIME-Base64 perl-PathTools perl-Pod-Simple perl-Storable perl-Text-Unidecode perl-Unicode-EastAsianWidth perl-Unicode-Normalize perl-interpreter perl-libintl-perl perl-libs"
RDEPENDS_texinfo-tex = "bash texinfo texlive-collection-basic texlive-epsf texlive-tetex"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/texinfo-6.5-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/texinfo-tex-6.5-6.el8.x86_64.rpm \
          "

SRC_URI[texinfo.sha256sum] = "7baeb953c3d54bd891fc6192b1d24d60632b925fb0dda9990170506b05c629f2"
SRC_URI[texinfo-tex.sha256sum] = "1d9eac49a8c41114b77cd399db28fb1b461d71cfb1153db9527ce7d1fcf6f7ff"
