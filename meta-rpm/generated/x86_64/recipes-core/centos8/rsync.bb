SUMMARY = "generated recipe based on rsync srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr pkgconfig-native popt"
RPM_SONAME_REQ_rsync = "libacl.so.1 libattr.so.1 libc.so.6 libpopt.so.0"
RDEPENDS_rsync = "glibc libacl libattr popt"
RDEPENDS_rsync-daemon = "bash rsync systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rsync-3.1.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rsync-daemon-3.1.3-7.el8.noarch.rpm \
          "

SRC_URI[rsync.sha256sum] = "1b45e50c97a7bd8ad283998b8131ab24d5c140e4824f46891244645e69237f1d"
SRC_URI[rsync-daemon.sha256sum] = "03bbac04acad4b1a45b3f8a3c33045df5a3804a0ee3be2b2f04d79e598d14b4e"
