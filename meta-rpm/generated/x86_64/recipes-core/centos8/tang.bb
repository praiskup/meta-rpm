SUMMARY = "generated recipe based on tang srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "http-parser jansson jose libgcc pkgconfig-native"
RPM_SONAME_REQ_tang = "libc.so.6 libgcc_s.so.1 libhttp_parser.so.2 libjansson.so.4 libjose.so.0"
RDEPENDS_tang = "bash coreutils glibc grep http-parser jansson jose libgcc libjose sed shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tang-7-5.el8.x86_64.rpm \
          "

SRC_URI[tang.sha256sum] = "fdf99862dbab31e5f753c88801f8da3eef41be5ad239864415807fc9c9121b64"
