SUMMARY = "generated recipe based on autotrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng pkgconfig-native pstoedit zlib"
RPM_SONAME_PROV_autotrace = "libautotrace.so.3"
RPM_SONAME_REQ_autotrace = "libautotrace.so.3 libc.so.6 libdl.so.2 libm.so.6 libpng16.so.16 libpstoedit.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_autotrace = "glibc libpng libstdc++ pstoedit zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/autotrace-0.31.1-52.el8.x86_64.rpm \
          "

SRC_URI[autotrace.sha256sum] = "4cfd0b2d3bb42823768a9b81e043e32944e68d8ed4bf98f3d127a5d0dca44a30"
