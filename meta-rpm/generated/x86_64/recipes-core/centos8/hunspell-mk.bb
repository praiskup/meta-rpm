SUMMARY = "generated recipe based on hunspell-mk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mk-0.20051126-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-mk.sha256sum] = "a23af44545cc31918b2b18977d028987361547e7fbacd62ca13b5cd8e80f4fa5"
