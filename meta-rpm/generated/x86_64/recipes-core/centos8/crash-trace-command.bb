SUMMARY = "generated recipe based on crash-trace-command srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-trace-command = "libc.so.6"
RDEPENDS_crash-trace-command = "crash glibc trace-cmd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/crash-trace-command-2.0-15.el8.x86_64.rpm \
          "

SRC_URI[crash-trace-command.sha256sum] = "ca6da8f6bba19c8be007e6499cfbdff070dd5f241a2a20418837d28dbd74a0fd"
