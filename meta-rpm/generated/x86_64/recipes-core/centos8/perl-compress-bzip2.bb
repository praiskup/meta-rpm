SUMMARY = "generated recipe based on perl-Compress-Bzip2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 perl pkgconfig-native"
RPM_SONAME_REQ_perl-Compress-Bzip2 = "libbz2.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Compress-Bzip2 = "bzip2-libs glibc perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Compress-Bzip2-2.26-6.el8.x86_64.rpm \
          "

SRC_URI[perl-Compress-Bzip2.sha256sum] = "28a53dd2baed10919974c355ee6d9704f971cd9aa9541fb707135721a02dde70"
