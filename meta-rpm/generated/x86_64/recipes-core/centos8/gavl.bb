SUMMARY = "generated recipe based on gavl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgdither pkgconfig-native"
RPM_SONAME_PROV_gavl = "libgavl.so.1"
RPM_SONAME_REQ_gavl = "libc.so.6 libgdither.so.1 libm.so.6"
RDEPENDS_gavl = "glibc libgdither"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gavl-1.4.0-12.el8.x86_64.rpm \
          "

SRC_URI[gavl.sha256sum] = "e7ee68ffd9ec624dd687926f49f367421466706762d49fa08e75e88f62d9dd9c"
