SUMMARY = "generated recipe based on maven-shared-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-shared-utils = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_maven-shared-utils-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-shared-utils-3.2.1-0.1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-shared-utils-javadoc-3.2.1-0.1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-shared-utils.sha256sum] = "c42db60156b27aae143af15693ef0efaa460e6d9fc9b760b616ad98255b610a9"
SRC_URI[maven-shared-utils-javadoc.sha256sum] = "70af638bbb9d1a19ef1d4a9e8aaf90bf4a091911395f8a38e522828874e7d703"
