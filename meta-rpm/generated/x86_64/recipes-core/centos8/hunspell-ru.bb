SUMMARY = "generated recipe based on hunspell-ru srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ru = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ru-0.99g5-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-ru.sha256sum] = "0a48c4ce4fa777373bf1e83867e46bc983152a3c1b5cd480a3c715a09c43dcea"
