SUMMARY = "generated recipe based on ncurses srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_ncurses = "libc.so.6 libtic.so.6 libtinfo.so.6"
RDEPENDS_ncurses = "glibc ncurses-libs"
RPM_SONAME_PROV_ncurses-c++-libs = "libncurses++.so.6 libncurses++w.so.6"
RPM_SONAME_REQ_ncurses-c++-libs = "libc.so.6 libform.so.6 libformw.so.6 libgcc_s.so.1 libm.so.6 libmenu.so.6 libmenuw.so.6 libncurses.so.6 libncursesw.so.6 libpanel.so.6 libpanelw.so.6 libstdc++.so.6"
RDEPENDS_ncurses-c++-libs = "glibc libgcc libstdc++ ncurses-libs"
RPM_SONAME_PROV_ncurses-compat-libs = "libform.so.5 libformw.so.5 libmenu.so.5 libmenuw.so.5 libncurses++.so.5 libncurses++w.so.5 libncurses.so.5 libncursesw.so.5 libpanel.so.5 libpanelw.so.5 libtic.so.5 libtinfo.so.5"
RPM_SONAME_REQ_ncurses-compat-libs = "libc.so.6 libdl.so.2 libform.so.5 libformw.so.5 libgcc_s.so.1 libm.so.6 libmenu.so.5 libmenuw.so.5 libncurses.so.5 libncursesw.so.5 libpanel.so.5 libpanelw.so.5 libstdc++.so.6 libtinfo.so.5"
RDEPENDS_ncurses-compat-libs = "glibc libgcc libstdc++ ncurses-base"
RPM_SONAME_REQ_ncurses-devel = "libform.so.6 libformw.so.6 libmenu.so.6 libmenuw.so.6 libncurses++.so.6 libncurses++w.so.6 libpanel.so.6 libpanelw.so.6 libtic.so.6 libtinfo.so.6"
RPROVIDES_ncurses-devel = "ncurses-dev (= 6.1)"
RDEPENDS_ncurses-devel = "bash ncurses-c++-libs ncurses-libs pkgconf-pkg-config"
RPM_SONAME_PROV_ncurses-libs = "libform.so.6 libformw.so.6 libmenu.so.6 libmenuw.so.6 libncurses.so.6 libncursesw.so.6 libpanel.so.6 libpanelw.so.6 libtic.so.6 libtinfo.so.6"
RPM_SONAME_REQ_ncurses-libs = "libc.so.6 libdl.so.2 libncurses.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_ncurses-libs = "glibc ncurses-base"
RDEPENDS_ncurses-term = "ncurses-base"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-6.1-7.20180224.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-base-6.1-7.20180224.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-c++-libs-6.1-7.20180224.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-compat-libs-6.1-7.20180224.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-devel-6.1-7.20180224.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-libs-6.1-7.20180224.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ncurses-term-6.1-7.20180224.el8.noarch.rpm \
          "
SRC_URI = "file://ncurses-6.1-7.20180224.el8.patch"

SRC_URI[ncurses.sha256sum] = "1531c2e9ae6ba19c1595480761d4ab2634ab8901d35d06994dfb144f1c5bf862"
SRC_URI[ncurses-base.sha256sum] = "1dfed63c2b675064c87d3e95c433a1c4515b24c28a7815e643e6f3d84814e79d"
SRC_URI[ncurses-c++-libs.sha256sum] = "3fd54892e07b631dbc328ed8617a18c1f0ade5e8d2ec882f6a5cfc761a2e89e2"
SRC_URI[ncurses-compat-libs.sha256sum] = "e4fa6494d821d29775a3d7c15d8401ec2289d6804fc85f1d38891d5e798ab686"
SRC_URI[ncurses-devel.sha256sum] = "3f3350db9421f55fa2c4449ae54a71e709f8d35d91d53f7b6432c88992476cff"
SRC_URI[ncurses-libs.sha256sum] = "440ef0473d6f85c6f173020dab18d376d466b7b960876fba54772f88d4bfe050"
SRC_URI[ncurses-term.sha256sum] = "355b2914ac4393437af8ee1e594b757e68864d9f4489b58e0d98450cafe243f0"
