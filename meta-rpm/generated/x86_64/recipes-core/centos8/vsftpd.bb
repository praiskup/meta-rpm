SUMMARY = "generated recipe based on vsftpd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap openssl pam pkgconfig-native"
RPM_SONAME_REQ_vsftpd = "libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libpam.so.0 libssl.so.1.1"
RDEPENDS_vsftpd = "bash glibc libcap logrotate openssl-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vsftpd-3.0.3-31.el8.x86_64.rpm \
          "

SRC_URI[vsftpd.sha256sum] = "6d4051de722163d8a027b852a196d88cad67f17845f41bb7120d23ffe767bfc2"
