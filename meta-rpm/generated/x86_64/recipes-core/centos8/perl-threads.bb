SUMMARY = "generated recipe based on perl-threads srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-threads = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-threads = "glibc perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-threads-2.21-2.el8.x86_64.rpm \
          "

SRC_URI[perl-threads.sha256sum] = "2e3da17b1c1685edea9c52bdaa0d77c019d6144c765fc6b3b1c783d98f634f96"
