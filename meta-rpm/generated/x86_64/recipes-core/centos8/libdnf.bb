SUMMARY = "generated recipe based on libdnf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gpgme json-c libgcc libgpg-error libmodulemd librepo libsolv openssl pkgconfig-native platform-python3 rpm sqlite3 util-linux"
RPM_SONAME_PROV_libdnf = "libdnf.so.2"
RPM_SONAME_REQ_libdnf = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libm.so.6 libmodulemd.so.1 librepo.so.0 librpm.so.8 librpmio.so.8 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_libdnf = "glib2 glibc gpgme json-c libgcc libgpg-error libmodulemd libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs rpm-libs sqlite-libs"
RPM_SONAME_REQ_python3-hawkey = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libdnf.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libm.so.6 libmodulemd.so.1 libpython3.6m.so.1.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_python3-hawkey = "glib2 glibc gpgme json-c libdnf libgcc libgpg-error libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs platform-python python3-libdnf python3-libs rpm-libs sqlite-libs"
RPM_SONAME_REQ_python3-libdnf = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libdnf.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libm.so.6 libmodulemd.so.1 libpython3.6m.so.1.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_python3-libdnf = "glib2 glibc gpgme json-c libdnf libgcc libgpg-error libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs platform-python python3-libs rpm-libs sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libdnf-0.39.1-6.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-hawkey-0.39.1-6.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libdnf-0.39.1-6.el8_2.x86_64.rpm \
          "

SRC_URI[libdnf.sha256sum] = "8546864e32a8f3b1430b9c085dca0c153b4f09c0a7c1e8211ae3624fd9d511a2"
SRC_URI[python3-hawkey.sha256sum] = "307c94f3c978e2c2c3f86af237d08ba8237bd704ba957591d179d6f48910fb33"
SRC_URI[python3-libdnf.sha256sum] = "f94e49f5efcf6af016e3943dfb5e69ebb43e0fe6d478b464139807109aeb7954"
