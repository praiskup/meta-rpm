SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-glib dbus-libs glib-2.0 libcap-ng libpcre libselinux libsemanage libsepol pam pkgconfig-native selinux-policy"
RPM_SONAME_REQ_policycoreutils = "libaudit.so.1 libc.so.6 libselinux.so.1 libsemanage.so.1 libsepol.so.1"
RDEPENDS_policycoreutils = "audit-libs bash coreutils diffutils gawk glibc grep libselinux libselinux-utils libsemanage libsepol rpm sed util-linux"
RDEPENDS_policycoreutils-dbus = "platform-python python3-policycoreutils python3-slip-dbus"
RPM_SONAME_REQ_policycoreutils-devel = "libc.so.6"
RPROVIDES_policycoreutils-devel = "policycoreutils-dev (= 2.9)"
RDEPENDS_policycoreutils-devel = "dnf glibc make platform-python policycoreutils-python-utils selinux-policy-devel"
RPM_SONAME_REQ_policycoreutils-newrole = "libaudit.so.1 libc.so.6 libcap-ng.so.0 libpam.so.0 libpam_misc.so.0 libselinux.so.1"
RDEPENDS_policycoreutils-newrole = "audit-libs glibc libcap-ng libselinux pam policycoreutils"
RDEPENDS_policycoreutils-python-utils = "platform-python python3-policycoreutils"
RPM_SONAME_REQ_policycoreutils-restorecond = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libpcre.so.1 libselinux.so.1"
RDEPENDS_policycoreutils-restorecond = "bash dbus-glib dbus-libs glib2 glibc libselinux pcre"
RDEPENDS_python3-policycoreutils = "checkpolicy platform-python policycoreutils python3-audit python3-libselinux python3-libsemanage python3-setools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-2.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-dbus-2.9-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-devel-2.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-newrole-2.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-python-utils-2.9-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/policycoreutils-restorecond-2.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-policycoreutils-2.9-9.el8.noarch.rpm \
          "

SRC_URI[policycoreutils.sha256sum] = "646258b615f7f12e8a25cdc27ae72703282475aab9c0e5cf51c457b2e346bded"
SRC_URI[policycoreutils-dbus.sha256sum] = "8e68ceb79f0bc7e77950e32fbad71e7db9ec047f10e35988e4e2dc7ccdd4a245"
SRC_URI[policycoreutils-devel.sha256sum] = "28eedd89820eb268304d65873f635a77dc88ea0bc2d1ec13f4cf4104d5a6ef2f"
SRC_URI[policycoreutils-newrole.sha256sum] = "ab3b29493906999275839c27eff42f2b7c37da7162182bb3a15f4ae56246629a"
SRC_URI[policycoreutils-python-utils.sha256sum] = "f1b53326dec969c70f4c61da7241474ff7428d35d235301836ad1c0ce3031cab"
SRC_URI[policycoreutils-restorecond.sha256sum] = "cce9a9928d68bbf763621c9346229d79df6774f86816746a91c0109ec7ae5b91"
SRC_URI[python3-policycoreutils.sha256sum] = "f5b2e42438721527f01405474819f3346eb470315371086bc5aedcb46d34633e"
