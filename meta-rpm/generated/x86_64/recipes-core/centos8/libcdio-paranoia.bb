SUMMARY = "generated recipe based on libcdio-paranoia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcdio pkgconfig-native"
RPM_SONAME_PROV_libcdio-paranoia = "libcdio_cdda.so.2 libcdio_paranoia.so.2"
RPM_SONAME_REQ_libcdio-paranoia = "libc.so.6 libcdio.so.18 libcdio_cdda.so.2 libcdio_paranoia.so.2 libm.so.6 librt.so.1"
RDEPENDS_libcdio-paranoia = "glibc libcdio"
RPM_SONAME_REQ_libcdio-paranoia-devel = "libcdio_cdda.so.2 libcdio_paranoia.so.2"
RPROVIDES_libcdio-paranoia-devel = "libcdio-paranoia-dev (= 10.2+0.94+2)"
RDEPENDS_libcdio-paranoia-devel = "libcdio-devel libcdio-paranoia pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libcdio-paranoia-10.2+0.94+2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcdio-paranoia-devel-10.2+0.94+2-3.el8.x86_64.rpm \
          "

SRC_URI[libcdio-paranoia.sha256sum] = "35a1dc0c08641d91879db1c50ff08683207351af1ea3353004c429f8f70c95ee"
SRC_URI[libcdio-paranoia-devel.sha256sum] = "303f21aea8c6e3902249528c07a319eaeccfb367284abebecbbc10658a8a561e"
