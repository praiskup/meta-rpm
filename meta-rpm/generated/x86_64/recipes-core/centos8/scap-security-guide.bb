SUMMARY = "generated recipe based on scap-security-guide srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_scap-security-guide = "openscap-scanner xml-common"
RDEPENDS_scap-security-guide-doc = "scap-security-guide"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/scap-security-guide-0.1.48-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/scap-security-guide-doc-0.1.48-7.el8.noarch.rpm \
          "

SRC_URI[scap-security-guide.sha256sum] = "08d68127b66e3497a9b5235ec4aa5f4899c029be2694f6dce104783fb61da5e3"
SRC_URI[scap-security-guide-doc.sha256sum] = "ab9b4a715e7661fd98b3f77119a10318267c0cce47dfc0b98a7c8ff1fb1b5e19"
