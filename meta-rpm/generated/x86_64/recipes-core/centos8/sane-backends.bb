SUMMARY = "generated recipe based on sane-backends srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgphoto2 libieee1284 libjpeg-turbo libpng libusb1 pkgconfig-native systemd-libs tiff v4l-utils"
RPM_SONAME_REQ_sane-backends = "libc.so.6 libieee1284.so.3 libjpeg.so.62 libm.so.6 libpng16.so.16 libsane.so.1 libusb-1.0.so.0"
RDEPENDS_sane-backends = "bash glibc grep libieee1284 libjpeg-turbo libpng libusbx sane-backends-libs sed systemd systemd-udev"
RPM_SONAME_REQ_sane-backends-daemon = "libc.so.6 libsane.so.1 libsystemd.so.0"
RDEPENDS_sane-backends-daemon = "bash glibc sane-backends sane-backends-libs shadow-utils systemd systemd-libs"
RPM_SONAME_REQ_sane-backends-devel = "libsane.so.1"
RPROVIDES_sane-backends-devel = "sane-backends-dev (= 1.0.27)"
RDEPENDS_sane-backends-devel = "bash libieee1284-devel libjpeg-turbo-devel libtiff-devel libusbx-devel pkgconf-pkg-config sane-backends sane-backends-drivers-cameras sane-backends-drivers-scanners sane-backends-libs"
RPM_SONAME_REQ_sane-backends-drivers-cameras = "libc.so.6 libgphoto2.so.6 libgphoto2_port.so.12 libjpeg.so.62 libm.so.6"
RDEPENDS_sane-backends-drivers-cameras = "glibc libgphoto2 libjpeg-turbo sane-backends sane-backends-libs"
RPM_SONAME_REQ_sane-backends-drivers-scanners = "libc.so.6 libdl.so.2 libieee1284.so.3 libjpeg.so.62 libm.so.6 libpthread.so.0 libtiff.so.5 libusb-1.0.so.0 libv4l1.so.0"
RDEPENDS_sane-backends-drivers-scanners = "glibc libieee1284 libjpeg-turbo libtiff libusbx libv4l sane-backends sane-backends-libs"
RPM_SONAME_PROV_sane-backends-libs = "libsane.so.1"
RPM_SONAME_REQ_sane-backends-libs = "libc.so.6 libdl.so.2 libgphoto2.so.6 libgphoto2_port.so.12 libieee1284.so.3 libjpeg.so.62 libm.so.6 libpthread.so.0 libtiff.so.5 libusb-1.0.so.0 libv4l1.so.0"
RDEPENDS_sane-backends-libs = "glibc libgphoto2 libieee1284 libjpeg-turbo libtiff libusbx libv4l"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-1.0.27-19.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-daemon-1.0.27-19.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-devel-1.0.27-19.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-doc-1.0.27-19.el8_2.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-drivers-cameras-1.0.27-19.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-drivers-scanners-1.0.27-19.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-backends-libs-1.0.27-19.el8_2.1.x86_64.rpm \
          "

SRC_URI[sane-backends.sha256sum] = "b21cd54cf36f5b60765176b53489cf289e0eff9fcb5941d495673bcd39bba222"
SRC_URI[sane-backends-daemon.sha256sum] = "aabbf390549edfc2a4de17d4fa5aa768974680b3196e17a1fd950f79c74f828c"
SRC_URI[sane-backends-devel.sha256sum] = "2f5544192ab53a36456f430326a37554f2a094e438264e06fae6b4a16cb80b26"
SRC_URI[sane-backends-doc.sha256sum] = "850627ee2067694b915dcdd042651e65f2b047523bb9a039c50966f66aec06b5"
SRC_URI[sane-backends-drivers-cameras.sha256sum] = "75a5283a064a30ae31d91915f6d3b119f1833074c042191c11f6456630ecd4fa"
SRC_URI[sane-backends-drivers-scanners.sha256sum] = "5cee820791becfbb0715b65879869a7c280b2f1ca34b01dd16852b82acc088e4"
SRC_URI[sane-backends-libs.sha256sum] = "9086524f0ec6373668258a3151621037cfa7ab0efc51284371767c83c149efc8"
