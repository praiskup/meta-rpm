SUMMARY = "generated recipe based on xorg-x11-proto-devel srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_xorg-x11-proto-devel = "xorg-x11-proto-dev (= 2018.4)"
RDEPENDS_xorg-x11-proto-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-proto-devel-2018.4-1.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-proto-devel.sha256sum] = "1ab34f4842f040cb7d2202629afb93cab4a2da310b444c71acf33daebdf398c1"
