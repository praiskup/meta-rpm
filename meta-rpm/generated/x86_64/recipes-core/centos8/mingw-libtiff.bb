SUMMARY = "generated recipe based on mingw-libtiff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-libtiff = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-gcc-c++ mingw32-libjpeg-turbo mingw32-pkg-config mingw32-zlib"
RDEPENDS_mingw32-libtiff-static = "mingw32-libtiff"
RDEPENDS_mingw64-libtiff = "mingw64-crt mingw64-filesystem mingw64-gcc-c++ mingw64-libjpeg-turbo mingw64-pkg-config mingw64-zlib"
RDEPENDS_mingw64-libtiff-static = "mingw64-libtiff"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libtiff-4.0.9-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libtiff-static-4.0.9-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libtiff-4.0.9-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libtiff-static-4.0.9-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-libtiff.sha256sum] = "2015ef2e17f0c8f6d3bc4a7196e28ba7b7490a87127b5fc85cfa9cefcbc10c3f"
SRC_URI[mingw32-libtiff-static.sha256sum] = "ebef28655f78ff815ecddf16dea86fee2c4d18d6bb682c3176528df6df30dec3"
SRC_URI[mingw64-libtiff.sha256sum] = "16c03c93940edaaa0f1e8794546b7aeb0b27dbe530b1c4be9268f0c4344252b8"
SRC_URI[mingw64-libtiff-static.sha256sum] = "46ec0c7c548b3cf22ef48d9568f0c71777b6ecd517e35784e7fd87e84ca29fbe"
