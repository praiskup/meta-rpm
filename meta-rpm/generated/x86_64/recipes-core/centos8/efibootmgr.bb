SUMMARY = "generated recipe based on efibootmgr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "efivar pkgconfig-native popt"
RPM_SONAME_REQ_efibootmgr = "libc.so.6 libefiboot.so.1 libefivar.so.1 libpopt.so.0"
RDEPENDS_efibootmgr = "efivar-libs glibc popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/efibootmgr-16-1.el8.x86_64.rpm \
          "

SRC_URI[efibootmgr.sha256sum] = "b1912057e2ea0b7f2798a67fcbcccb2b9766145f5688ce10677986501e37b057"
