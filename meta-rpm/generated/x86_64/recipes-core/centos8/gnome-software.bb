SUMMARY = "generated recipe based on gnome-software srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo flatpak fwupd gdk-pixbuf glib-2.0 gnome-desktop3 gspell gtk+3 json-glib libappstream-glib libgcc libgudev libsecret libsoup-2.4 ostree packagekit pkgconfig-native polkit rpm-ostree"
RPM_SONAME_PROV_gnome-software = "libgs_plugin_appstream.so libgs_plugin_desktop-categories.so libgs_plugin_desktop-menu-path.so libgs_plugin_dpkg.so libgs_plugin_dummy.so libgs_plugin_epiphany.so libgs_plugin_fedora-pkgdb-collections.so libgs_plugin_flatpak.so libgs_plugin_fwupd.so libgs_plugin_generic-updates.so libgs_plugin_hardcoded-blacklist.so libgs_plugin_hardcoded-featured.so libgs_plugin_hardcoded-popular.so libgs_plugin_icons.so libgs_plugin_key-colors-metadata.so libgs_plugin_key-colors.so libgs_plugin_modalias.so libgs_plugin_odrs.so libgs_plugin_os-release.so libgs_plugin_packagekit-history.so libgs_plugin_packagekit-local.so libgs_plugin_packagekit-offline.so libgs_plugin_packagekit-proxy.so libgs_plugin_packagekit-refine-repos.so libgs_plugin_packagekit-refine.so libgs_plugin_packagekit-refresh.so libgs_plugin_packagekit-upgrade.so libgs_plugin_packagekit-url-to-app.so libgs_plugin_packagekit.so libgs_plugin_provenance-license.so libgs_plugin_provenance.so libgs_plugin_repos.so libgs_plugin_rewrite-resource.so libgs_plugin_rpm-ostree.so libgs_plugin_shell-extensions.so libgs_plugin_steam.so libgs_plugin_systemd-updates.so"
RPM_SONAME_REQ_gnome-software = "libappstream-glib.so.8 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libflatpak.so.0 libfwupd.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgspell-1.so.2 libgtk-3.so.0 libgudev-1.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libostree-1.so.1 libpackagekit-glib2.so.18 libpolkit-gobject-1.so.0 libpthread.so.0 librpmostree-1.so.1 libsecret-1.so.0 libsoup-2.4.so.1"
RDEPENDS_gnome-software = "PackageKit PackageKit-glib appstream-data atk cairo flatpak flatpak-libs fwupd gdk-pixbuf2 glib2 glibc gnome-desktop3 gnome-menus gsettings-desktop-schemas gspell gtk3 iso-codes json-glib libappstream-glib libgcc libgudev librsvg2 libsecret libsoup ostree-libs polkit-libs rpm-ostree-libs"
RPM_SONAME_REQ_gnome-software-editor = "libappstream-glib.so.8 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libm.so.6 libpolkit-gobject-1.so.0 libpthread.so.0 libsecret-1.so.0 libsoup-2.4.so.1"
RDEPENDS_gnome-software-editor = "atk cairo gdk-pixbuf2 glib2 glibc gnome-software gtk3 json-glib libappstream-glib libgcc libsecret libsoup polkit-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-software-3.30.6-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-software-editor-3.30.6-3.el8.x86_64.rpm \
          "

SRC_URI[gnome-software.sha256sum] = "b926b7a28e2cdb0ba466727823cae108f764185c3d7846207c537a21d484d4a5"
SRC_URI[gnome-software-editor.sha256sum] = "a7604297d4dc3156383117b072529f9f8616a2aaf396f29aafc08f550b5210a9"
