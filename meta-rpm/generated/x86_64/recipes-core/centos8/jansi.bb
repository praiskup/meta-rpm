SUMMARY = "generated recipe based on jansi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jansi = "hawtjni-runtime jansi-native java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jansi-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jansi-1.17.1-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jansi-javadoc-1.17.1-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jansi.sha256sum] = "b71dc5a21344ce8bd0cccb7f0825324e09a6bbee76c90579831afcec6407401f"
SRC_URI[jansi-javadoc.sha256sum] = "19bef6344caf8b75595e673fb2fedcdc953b08774944018379f434e7df3da7dc"
