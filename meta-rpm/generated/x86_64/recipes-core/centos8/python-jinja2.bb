SUMMARY = "generated recipe based on python-jinja2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jinja2 = "platform-python platform-python-setuptools python3-babel python3-markupsafe"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-jinja2-2.10.1-2.el8_0.noarch.rpm \
          "

SRC_URI[python3-jinja2.sha256sum] = "5f4f256128dba88a13599d6458908c24c0e9c18d8979ae44bf5734da8e7cab70"
