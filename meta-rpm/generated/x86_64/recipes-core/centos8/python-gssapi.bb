SUMMARY = "generated recipe based on python-gssapi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-gssapi = "libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-gssapi = "glibc krb5-libs libcom_err platform-python python3-decorator python3-libs python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-gssapi-1.5.1-5.el8.x86_64.rpm \
          "

SRC_URI[python3-gssapi.sha256sum] = "2124c779f1db9f0138f1619250bb1a35e7b4de2a1bebd77290cd014f82f134e4"
