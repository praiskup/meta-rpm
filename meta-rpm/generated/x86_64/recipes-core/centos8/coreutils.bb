SUMMARY = "generated recipe based on coreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl attr gmp libcap libselinux openssl pkgconfig-native"
RPM_SONAME_PROV_coreutils = "libstdbuf.so"
RPM_SONAME_REQ_coreutils = "libacl.so.1 libattr.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libgmp.so.10 libpthread.so.0 librt.so.1 libselinux.so.1"
RDEPENDS_coreutils = "coreutils-common glibc gmp libacl libattr libcap libselinux ncurses openssl-libs"
RDEPENDS_coreutils-common = "bash info"
RPM_SONAME_PROV_coreutils-single = "libstdbuf.so.single"
RPM_SONAME_REQ_coreutils-single = "libacl.so.1 libattr.so.1 libc.so.6 libcap.so.2 libpthread.so.0 librt.so.1 libselinux.so.1"
RDEPENDS_coreutils-single = "glibc libacl libattr libcap libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/coreutils-8.30-7.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/coreutils-common-8.30-7.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/coreutils-single-8.30-7.el8_2.1.x86_64.rpm \
          "

SRC_URI[coreutils.sha256sum] = "4aa34f071ff236e0f6fa4f15057ffd08a9806704a0be79ac4b2a4542c1f42de5"
SRC_URI[coreutils-common.sha256sum] = "c1d7897f012ee7e92c644d8c5f77b030100dad3517f07e6d5fe2788b0f9c013d"
SRC_URI[coreutils-single.sha256sum] = "b111ebe1f43b24c2c3cfc2d7083b5e0ef69fa0384a8e71b89940b5dc3cfb3686"
