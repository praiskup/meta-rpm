SUMMARY = "generated recipe based on perl-Digest-CRC srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-CRC = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-CRC = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Digest-CRC-0.22.2-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Digest-CRC.sha256sum] = "4f3a431c6a25a3a6fed0a1e336fcb7f2cc842150a38fde47be2904cf853b611d"
