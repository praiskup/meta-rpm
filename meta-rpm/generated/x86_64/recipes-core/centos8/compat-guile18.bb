SUMMARY = "generated recipe based on compat-guile18 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libtool libxcrypt ncurses pkgconfig-native readline"
RPM_SONAME_PROV_compat-guile18 = "libguile-srfi-srfi-1-v-3.so.3 libguile-srfi-srfi-13-14-v-3.so.3 libguile-srfi-srfi-4-v-3.so.3 libguile-srfi-srfi-60-v-2.so.2 libguile.so.17 libguilereadline-v-17.so.17"
RPM_SONAME_REQ_compat-guile18 = "ld-linux-x86-64.so.2 libc.so.6 libcrypt.so.1 libgmp.so.10 libguile-srfi-srfi-1-v-3.so.3 libguile-srfi-srfi-13-14-v-3.so.3 libguile-srfi-srfi-4-v-3.so.3 libguile-srfi-srfi-60-v-2.so.2 libguile.so.17 libguilereadline-v-17.so.17 libltdl.so.7 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libtinfo.so.6"
RDEPENDS_compat-guile18 = "bash glibc gmp libtool-ltdl libxcrypt ncurses-libs readline"
RPM_SONAME_REQ_compat-guile18-devel = "libguile.so.17"
RPROVIDES_compat-guile18-devel = "compat-guile18-dev (= 1.8.8)"
RDEPENDS_compat-guile18-devel = "bash compat-guile18 gmp-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/compat-guile18-1.8.8-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/compat-guile18-devel-1.8.8-22.el8.x86_64.rpm \
          "

SRC_URI[compat-guile18.sha256sum] = "3d1ec17eb991118bd05ab7cf23d55448b234b6dfa6d3fc7fe8a4cdf201bf1031"
SRC_URI[compat-guile18-devel.sha256sum] = "89d6d1f565bcfa0436beb51d1589e1bcd6f097f060ec9ca3b7279eacf94fb17e"
