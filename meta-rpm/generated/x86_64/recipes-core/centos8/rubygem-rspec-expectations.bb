SUMMARY = "generated recipe based on rubygem-rspec-expectations srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec-expectations = "rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rubygem-rspec-expectations-3.7.0-4.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec-expectations.sha256sum] = "84041ce43758fe26087f4350ff01d051cc7584171543d6f2f8d97eb198a25828"
