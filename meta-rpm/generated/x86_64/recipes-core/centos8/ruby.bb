SUMMARY = "generated recipe based on ruby srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm gmp libffi libxcrypt libyaml ncurses openssl pkgconfig-native readline zlib"
RPM_SONAME_REQ_ruby = "libc.so.6 libcrypt.so.1 libdl.so.2 libgmp.so.10 libm.so.6 libpthread.so.0 libruby.so.2.5"
RDEPENDS_ruby = "glibc gmp libxcrypt ruby-libs"
RPM_SONAME_REQ_ruby-devel = "libruby.so.2.5"
RPROVIDES_ruby-devel = "ruby-dev (= 2.5.5)"
RDEPENDS_ruby-devel = "pkgconf-pkg-config ruby ruby-libs rubygems"
RDEPENDS_ruby-doc = "rubygem-rdoc"
RDEPENDS_ruby-irb = "ruby ruby-libs"
RPM_SONAME_PROV_ruby-libs = "libruby.so.2.5"
RPM_SONAME_REQ_ruby-libs = "ld-linux-x86-64.so.2 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libffi.so.6 libgdbm.so.6 libgdbm_compat.so.4 libgmp.so.10 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libruby.so.2.5 libssl.so.1.1 libtinfo.so.6 libutil.so.1 libz.so.1"
RDEPENDS_ruby-libs = "gdbm-libs glibc gmp libffi libxcrypt ncurses-libs openssl-libs readline zlib"
RPM_SONAME_REQ_rubygem-bigdecimal = "libc.so.6 libruby.so.2.5"
RDEPENDS_rubygem-bigdecimal = "glibc ruby-libs rubygems"
RDEPENDS_rubygem-did_you_mean = "ruby-libs rubygems"
RPM_SONAME_REQ_rubygem-io-console = "libc.so.6 libruby.so.2.5"
RDEPENDS_rubygem-io-console = "glibc ruby-libs rubygems"
RPM_SONAME_REQ_rubygem-json = "libc.so.6 libruby.so.2.5"
RDEPENDS_rubygem-json = "glibc ruby-libs rubygems"
RDEPENDS_rubygem-minitest = "ruby-libs rubygems"
RDEPENDS_rubygem-net-telnet = "ruby-libs rubygems"
RPM_SONAME_REQ_rubygem-openssl = "libc.so.6 libcrypto.so.1.1 libruby.so.2.5 libssl.so.1.1"
RDEPENDS_rubygem-openssl = "glibc openssl-libs ruby-libs rubygems"
RDEPENDS_rubygem-power_assert = "ruby-libs rubygems"
RPM_SONAME_REQ_rubygem-psych = "libc.so.6 libruby.so.2.5 libyaml-0.so.2"
RDEPENDS_rubygem-psych = "glibc libyaml ruby-libs rubygems"
RDEPENDS_rubygem-rake = "ruby ruby-libs rubygems"
RDEPENDS_rubygem-rdoc = "ruby ruby-irb ruby-libs rubygem-io-console rubygem-json rubygems"
RDEPENDS_rubygem-test-unit = "ruby-libs rubygem-power_assert rubygems"
RDEPENDS_rubygem-xmlrpc = "ruby-libs rubygems"
RDEPENDS_rubygems = "ruby ruby-libs rubygem-openssl rubygem-psych"
RPROVIDES_rubygems-devel = "rubygems-dev (= 2.7.6.2)"
RDEPENDS_rubygems-devel = "ruby rubygem-json rubygem-rdoc rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-2.5.5-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-devel-2.5.5-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-doc-2.5.5-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-irb-2.5.5-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-libs-2.5.5-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-bigdecimal-1.3.4-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-did_you_mean-1.2.0-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-io-console-0.4.6-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-json-2.1.0-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-minitest-5.10.3-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-net-telnet-0.1.1-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-openssl-2.1.2-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-power_assert-1.1.1-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-psych-3.0.2-105.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-rake-12.3.0-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-rdoc-6.0.1-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-test-unit-3.2.7-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-xmlrpc-0.3.0-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygems-2.7.6.2-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygems-devel-2.7.6.2-105.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[ruby.sha256sum] = "b8b29097eab00ea1aa1e26625a3957e4920a152d9e816466527f2227dbcb590d"
SRC_URI[ruby-devel.sha256sum] = "cf15da699547e4a1477de19abbc6a12cf1934d2f57551de277800bdedfd11e12"
SRC_URI[ruby-doc.sha256sum] = "3eabcf315e222e8e7a7fa492b0e719f51997f6bdf97d802798f9760c12dbd80c"
SRC_URI[ruby-irb.sha256sum] = "0724f5aaf7344417ef7c0829e7cf38024da0189019a8f3c93f35515f4afde54d"
SRC_URI[ruby-libs.sha256sum] = "f9b2d927fe64c07bab926b1419f17429418e08780b6d0ee43bb318ce30458a82"
SRC_URI[rubygem-bigdecimal.sha256sum] = "4ea76040deacdbc56b1d6043f934ba91f8f50c89ed1e526fbcf8db4117b63c5d"
SRC_URI[rubygem-did_you_mean.sha256sum] = "866bf4f8c809b32430651f05404d6e8de0b515357711d965d59d6c9fdb4e2c53"
SRC_URI[rubygem-io-console.sha256sum] = "3de299f121f1ef9733e3ca6d0f00d66acbca455c0b0a7c24a5bc432a7373bd5b"
SRC_URI[rubygem-json.sha256sum] = "2b3c7196b5336d135280ced50ec3aa92a29e98362eef0bd18b36bd86916c78f4"
SRC_URI[rubygem-minitest.sha256sum] = "5e9f96f38732a36286d6307d033309f313a2d48ec308f58573511697bcc9990e"
SRC_URI[rubygem-net-telnet.sha256sum] = "89ec804540ada1556df61cca2e996dc60f67f504931e5db97cd5d609bc5b6a8f"
SRC_URI[rubygem-openssl.sha256sum] = "63a5915bb1fce6beadb8b6ad95ac2b04aae9d82bb05c00448944bd723a2f9536"
SRC_URI[rubygem-power_assert.sha256sum] = "93139b17345af0cb3942b25a822cddf90a0da8e9e1da3a71945ac4f3efe69c28"
SRC_URI[rubygem-psych.sha256sum] = "2f7ce995ac1bd968d81a59440e57333e5f97f3682edd3722098ff425f96982ab"
SRC_URI[rubygem-rake.sha256sum] = "4018bee796aece56496dee16294529e2f3bbf7cd654f244ed691295324e6546d"
SRC_URI[rubygem-rdoc.sha256sum] = "c8b91660e6b923c7a11f9179b03bce5d9451a9a4eca68ded9931ecde10c8bc57"
SRC_URI[rubygem-test-unit.sha256sum] = "5481ef6fcfc56ddccfb34781c733d666880dae6379b3b606c4057085c4f7d45a"
SRC_URI[rubygem-xmlrpc.sha256sum] = "b5549ab173374660488481cdfa17884d75f6d69865c7b282265ecddee2a66f57"
SRC_URI[rubygems.sha256sum] = "501a1eb35963c135797e349901d80a29f232569aa168e4a62df0d3df327ae4fc"
SRC_URI[rubygems-devel.sha256sum] = "1a8fb18089c0eb8de9c997f2a1c672e480e27d2aab0cfafa8574fe04c1bc244c"
