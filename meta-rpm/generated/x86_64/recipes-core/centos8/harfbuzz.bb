SUMMARY = "generated recipe based on harfbuzz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo freetype glib-2.0 graphite2 icu libgcc pkgconfig-native"
RPM_SONAME_PROV_harfbuzz = "libharfbuzz.so.0"
RPM_SONAME_REQ_harfbuzz = "libc.so.6 libfreetype.so.6 libglib-2.0.so.0 libgraphite2.so.3 libm.so.6"
RDEPENDS_harfbuzz = "freetype glib2 glibc graphite2"
RPM_SONAME_REQ_harfbuzz-devel = "libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 libglib-2.0.so.0 libharfbuzz-icu.so.0 libharfbuzz.so.0 libm.so.6 libstdc++.so.6"
RPROVIDES_harfbuzz-devel = "harfbuzz-dev (= 1.7.5)"
RDEPENDS_harfbuzz-devel = "cairo freetype glib2 glib2-devel glibc graphite2-devel harfbuzz harfbuzz-icu libgcc libicu-devel libstdc++ pkgconf-pkg-config"
RPM_SONAME_PROV_harfbuzz-icu = "libharfbuzz-icu.so.0"
RPM_SONAME_REQ_harfbuzz-icu = "libc.so.6 libgcc_s.so.1 libharfbuzz.so.0 libicudata.so.60 libicuuc.so.60 libm.so.6 libstdc++.so.6"
RDEPENDS_harfbuzz-icu = "glibc harfbuzz libgcc libicu libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/harfbuzz-1.7.5-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/harfbuzz-devel-1.7.5-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/harfbuzz-icu-1.7.5-3.el8.x86_64.rpm \
          "

SRC_URI[harfbuzz.sha256sum] = "49c652f3d967e944b9d0ad9dea63e8942626d3b9f40fde12cfb0d3e924a82053"
SRC_URI[harfbuzz-devel.sha256sum] = "24e81d612c8c51c8c811e0eb123de645344699167d9292ebe3f1f2f71284b961"
SRC_URI[harfbuzz-icu.sha256sum] = "3e3d38a2322776a8cd728296ad3777175d4c99cc81f0fa1b65071b049bb58be5"
