SUMMARY = "generated recipe based on clutter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo cogl fontconfig freetype gdk-pixbuf glib-2.0 gtk+3 json-glib libdrm libglvnd libinput libx11 libxcomposite libxdamage libxext libxfixes libxi libxkbcommon libxrandr mesa pango pkgconfig-native systemd systemd-libs wayland"
RPM_SONAME_PROV_clutter = "libclutter-1.0.so.0"
RPM_SONAME_REQ_clutter = "libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libdrm.so.2 libfontconfig.so.1 libfreetype.so.6 libgbm.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libinput.so.10 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libudev.so.1 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxkbcommon.so.0"
RDEPENDS_clutter = "atk cairo cairo-gobject cogl fontconfig freetype gdk-pixbuf2 glib2 glibc gobject-introspection gtk3 json-glib libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libdrm libglvnd-egl libinput libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon mesa-dri-drivers mesa-libgbm pango systemd-libs"
RPM_SONAME_REQ_clutter-devel = "libclutter-1.0.so.0"
RPROVIDES_clutter-devel = "clutter-dev (= 1.26.2)"
RDEPENDS_clutter-devel = "atk-devel cairo-gobject-devel clutter cogl-devel gdk-pixbuf2-devel glib2-devel gtk3-devel json-glib-devel libX11-devel libXcomposite-devel libXdamage-devel libXext-devel libXi-devel libinput-devel libxkbcommon-devel pango-devel pkgconf-pkg-config systemd-devel wayland-devel"
RDEPENDS_clutter-doc = "clutter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clutter-1.26.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/clutter-devel-1.26.2-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/clutter-doc-1.26.2-8.el8.x86_64.rpm \
          "

SRC_URI[clutter.sha256sum] = "c598d75384a08d28406842aa278cda9318170f4a668d9122fa3f79dd0c28db03"
SRC_URI[clutter-devel.sha256sum] = "fa29b378805bc281c22db21e56ce801655d0369c89525730098cf2afa9baa75d"
SRC_URI[clutter-doc.sha256sum] = "ca5a41308560a71fa8c28e2ddfadadc73a91d35e7f1c30105111623feff7e5d8"
