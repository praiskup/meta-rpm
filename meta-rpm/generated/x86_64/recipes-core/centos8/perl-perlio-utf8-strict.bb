SUMMARY = "generated recipe based on perl-PerlIO-utf8_strict srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-PerlIO-utf8_strict = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-PerlIO-utf8_strict = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-PerlIO-utf8_strict-0.007-5.el8.x86_64.rpm \
          "

SRC_URI[perl-PerlIO-utf8_strict.sha256sum] = "0e1844d653b090a2e85684eae517df52dab7004d7bf70adfddbf6452a485576b"
