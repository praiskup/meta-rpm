SUMMARY = "generated recipe based on glog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gflags libgcc pkgconfig-native"
RPM_SONAME_PROV_glog = "libglog.so.0"
RPM_SONAME_REQ_glog = "libc.so.6 libgcc_s.so.1 libgflags.so.2.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_glog = "gflags gflags-devel glibc libgcc libstdc++"
RPM_SONAME_REQ_glog-devel = "libglog.so.0"
RPROVIDES_glog-devel = "glog-dev (= 0.3.5)"
RDEPENDS_glog-devel = "glog pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glog-0.3.5-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glog-devel-0.3.5-3.el8.x86_64.rpm \
          "

SRC_URI[glog.sha256sum] = "65af05372df73ab92223792ff3730f3c2eea2862f8eca5448f90c3e884c11807"
SRC_URI[glog-devel.sha256sum] = "a7a7f56aa87fe4da32fcd046aa8c3f8ae07935cd8cb420ebd45157abaa9268c1"
