SUMMARY = "generated recipe based on perl-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-parent = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-parent-0.237-1.el8.noarch.rpm \
          "

SRC_URI[perl-parent.sha256sum] = "f5e73bbd776a2426a796971d8d38664f2e94898479fb76947dccdd28cf9fe1d0"
