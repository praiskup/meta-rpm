SUMMARY = "generated recipe based on wavpack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_wavpack = "libwavpack.so.1"
RPM_SONAME_REQ_wavpack = "libc.so.6 libm.so.6 libwavpack.so.1"
RDEPENDS_wavpack = "glibc"
RPM_SONAME_REQ_wavpack-devel = "libwavpack.so.1"
RPROVIDES_wavpack-devel = "wavpack-dev (= 5.1.0)"
RDEPENDS_wavpack-devel = "pkgconf-pkg-config wavpack"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wavpack-5.1.0-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/wavpack-devel-5.1.0-15.el8.x86_64.rpm \
          "

SRC_URI[wavpack.sha256sum] = "da257a42b78c7d7dd7c3485d62544d5361f5c77a7069feffd7b0ca04f0ce3e26"
SRC_URI[wavpack-devel.sha256sum] = "72d5caa66293d7ca076bd052f3f128cee1de962c2157372e3f99264b8b2d6966"
