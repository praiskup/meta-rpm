SUMMARY = "generated recipe based on libvisio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc librevenge libxml2 pkgconfig-native"
RPM_SONAME_PROV_libvisio = "libvisio-0.1.so.1"
RPM_SONAME_REQ_libvisio = "libc.so.6 libgcc_s.so.1 libicudata.so.60 libicuuc.so.60 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libxml2.so.2"
RDEPENDS_libvisio = "glibc libgcc libicu librevenge libstdc++ libxml2"
RPM_SONAME_REQ_libvisio-devel = "libvisio-0.1.so.1"
RPROVIDES_libvisio-devel = "libvisio-dev (= 0.1.6)"
RDEPENDS_libvisio-devel = "libicu-devel librevenge-devel libvisio libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvisio-0.1.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvisio-devel-0.1.6-2.el8.x86_64.rpm \
          "

SRC_URI[libvisio.sha256sum] = "844525e05b3d9fe917516cdb4837f0f49d605d23e242909c8812bb7fef17ffdb"
SRC_URI[libvisio-devel.sha256sum] = "7229722b9eeea3d7f3bbff49d5604121cc4a9878da19e47ae0c6cfa107164d38"
