SUMMARY = "generated recipe based on libxcb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxau pkgconfig-native"
RPM_SONAME_PROV_libxcb = "libxcb-composite.so.0 libxcb-damage.so.0 libxcb-dpms.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-glx.so.0 libxcb-present.so.0 libxcb-randr.so.0 libxcb-record.so.0 libxcb-render.so.0 libxcb-res.so.0 libxcb-screensaver.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-sync.so.1 libxcb-xf86dri.so.0 libxcb-xfixes.so.0 libxcb-xinerama.so.0 libxcb-xinput.so.0 libxcb-xkb.so.1 libxcb-xselinux.so.0 libxcb-xtest.so.0 libxcb-xv.so.0 libxcb-xvmc.so.0 libxcb.so.1"
RPM_SONAME_REQ_libxcb = "libXau.so.6 libc.so.6 libxcb.so.1"
RDEPENDS_libxcb = "glibc libXau"
RPM_SONAME_REQ_libxcb-devel = "libxcb-composite.so.0 libxcb-damage.so.0 libxcb-dpms.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-glx.so.0 libxcb-present.so.0 libxcb-randr.so.0 libxcb-record.so.0 libxcb-render.so.0 libxcb-res.so.0 libxcb-screensaver.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-sync.so.1 libxcb-xf86dri.so.0 libxcb-xfixes.so.0 libxcb-xinerama.so.0 libxcb-xinput.so.0 libxcb-xkb.so.1 libxcb-xselinux.so.0 libxcb-xtest.so.0 libxcb-xv.so.0 libxcb-xvmc.so.0 libxcb.so.1"
RPROVIDES_libxcb-devel = "libxcb-dev (= 1.13.1)"
RDEPENDS_libxcb-devel = "libXau-devel libxcb pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxcb-1.13.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxcb-devel-1.13.1-1.el8.x86_64.rpm \
          "

SRC_URI[libxcb.sha256sum] = "0221e6e3671c2bd130e9519a7b352404b7e510584b4707d38e1a733e19c7f74f"
SRC_URI[libxcb-devel.sha256sum] = "5d69d44de103a01eb5aa794e52208851a4808674edb2e89588fccdf6bb87f6f3"
