SUMMARY = "generated recipe based on crash-gcore-command srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-gcore-command = "libc.so.6"
RDEPENDS_crash-gcore-command = "crash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/crash-gcore-command-1.3.1-4.el8.x86_64.rpm \
          "

SRC_URI[crash-gcore-command.sha256sum] = "5a6f2684fa258244b2212c3599e8854d821b8df2b93cd770fc42ec33c0c0a606"
