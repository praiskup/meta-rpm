SUMMARY = "generated recipe based on lua-expat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat pkgconfig-native"
RPM_SONAME_REQ_lua-expat = "libc.so.6 libexpat.so.1"
RDEPENDS_lua-expat = "expat glibc lua-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lua-expat-1.3.0-12.el8.1.x86_64.rpm \
          "

SRC_URI[lua-expat.sha256sum] = "c05aa03a9fa90ab4cdac73c17fd7b9680ed18b5f93bc26c1cebd1a98ab779c92"
