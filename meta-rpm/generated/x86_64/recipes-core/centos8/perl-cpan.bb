SUMMARY = "generated recipe based on perl-CPAN srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-CPAN = "make perl-Archive-Tar perl-Archive-Zip perl-CPAN-Meta perl-CPAN-Meta-Requirements perl-Carp perl-Compress-Bzip2 perl-Data-Dumper perl-Devel-Size perl-Digest-MD5 perl-Digest-SHA perl-Exporter perl-ExtUtils-CBuilder perl-ExtUtils-MakeMaker perl-ExtUtils-Manifest perl-File-HomeDir perl-File-Path perl-File-Temp perl-HTTP-Tiny perl-IO-Compress perl-Module-Build perl-Net-Ping perl-PathTools perl-Text-Glob perl-Text-ParseWords perl-Text-Tabs+Wrap perl-Time-Local perl-URI perl-constant perl-interpreter perl-libnet perl-libs perl-local-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-CPAN-2.18-397.el8.noarch.rpm \
          "

SRC_URI[perl-CPAN.sha256sum] = "8851cddd5d3684b7e6c4968674fda14dd2177d76354245df090daf61e38c02ee"
