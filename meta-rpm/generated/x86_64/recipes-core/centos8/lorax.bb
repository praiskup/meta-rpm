SUMMARY = "generated recipe based on lorax srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_composer-cli = "platform-python python3-pytoml python3-urllib3"
RDEPENDS_lorax = "GConf2 cpio device-mapper dosfstools dracut e2fsprogs findutils gawk genisoimage glib2 glibc glibc-common gzip isomd5sum kmod kpartx lorax-templates-generic lorax-templates-rhel parted pigz platform-python python3-dnf python3-kickstart python3-librepo python3-libselinux python3-mako squashfs-tools syslinux util-linux xz"
RDEPENDS_lorax-composer = "anaconda-tui bash createrepo_c git glibc-common libgit2 libgit2-glib lorax platform-python python3-flask python3-gevent python3-pytoml python3-rpmfluff python3-semantic_version qemu-img shadow-utils systemd tar xz"
RDEPENDS_lorax-lmc-novirt = "anaconda-core anaconda-tui centos-logos lorax"
RDEPENDS_lorax-lmc-virt = "edk2-ovmf lorax qemu-kvm"
RDEPENDS_lorax-templates-generic = "lorax"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/composer-cli-28.14.42-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lorax-28.14.42-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lorax-composer-28.14.42-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lorax-lmc-novirt-28.14.42-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lorax-lmc-virt-28.14.42-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lorax-templates-generic-28.14.42-2.el8_2.x86_64.rpm \
          "

SRC_URI[composer-cli.sha256sum] = "0e0c22dc2339bcabbe0d4701f079ae995981436a663ea71c535a9f64bdc7abe5"
SRC_URI[lorax.sha256sum] = "63a643cb3960827edd227db77ee998f9e695693b3700f026926fc6dca541b4f6"
SRC_URI[lorax-composer.sha256sum] = "ebb9e87cb8deb8f9d542092e6c9d85ceb045be0ec5cda1b341bbc8423b0ff501"
SRC_URI[lorax-lmc-novirt.sha256sum] = "1c7346033feb8406a4829588544d5009bbdb9e46d1f0ba6ed8919445f511a8d0"
SRC_URI[lorax-lmc-virt.sha256sum] = "1a8b7983e34910726863da3bd882ece506845dcff9b17d773aacd5f812df9d30"
SRC_URI[lorax-templates-generic.sha256sum] = "fd3fc1faba84932d195039bdd3b8d74f45ac22d3bc9d1a3e5a4d81c4498f7d45"
