SUMMARY = "generated recipe based on network-manager-applet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gcr gdk-pixbuf glib-2.0 gtk+3 jansson libgcc libnotify libsecret libselinux modemmanager networkmanager pango pkgconfig-native"
RPM_SONAME_PROV_libnma = "libnma.so.0"
RPM_SONAME_REQ_libnma = "libc.so.6 libcairo.so.2 libgcc_s.so.1 libgck-1.so.0 libgcr-base-3.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnm.so.0 libpthread.so.0"
RDEPENDS_libnma = "NetworkManager-libnm cairo gcr glib2 glibc gtk3 libgcc mobile-broadband-provider-info"
RPM_SONAME_REQ_libnma-devel = "libnma.so.0"
RPROVIDES_libnma-devel = "libnma-dev (= 1.8.22)"
RDEPENDS_libnma-devel = "NetworkManager-libnm-devel gtk3-devel libnma pkgconf-pkg-config"
RPM_SONAME_REQ_network-manager-applet = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libmm-glib.so.0 libnm.so.0 libnma.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsecret-1.so.0"
RDEPENDS_network-manager-applet = "ModemManager-glib NetworkManager NetworkManager-libnm atk cairo gdk-pixbuf2 glib2 glibc gtk3 libgcc libnma libnotify libsecret nm-connection-editor pango"
RPM_SONAME_REQ_nm-connection-editor = "libc.so.6 libgcc_s.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjansson.so.4 libnm.so.0 libnma.so.0 libpthread.so.0 libselinux.so.1"
RDEPENDS_nm-connection-editor = "NetworkManager-libnm glib2 glibc gtk3 jansson libgcc libnma libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libnma-1.8.22-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/network-manager-applet-1.8.22-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nm-connection-editor-1.8.22-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnma-devel-1.8.22-2.el8.x86_64.rpm \
          "

SRC_URI[libnma.sha256sum] = "4584e7b50621a7ac65b257f6498622644d9bed87112d789476432e832f2cddc9"
SRC_URI[libnma-devel.sha256sum] = "057dd69b8b1a1d8382cd8ae86d7e31b6cb2d0b31c8d7b47ef99348a4f2ded03b"
SRC_URI[network-manager-applet.sha256sum] = "278722dd95922cf7494bacbcbec0b2844b450b1717c8658eefd3ab80e0d058aa"
SRC_URI[nm-connection-editor.sha256sum] = "4ba4567afdf36383d6ddcc77dd52ccaebd7af669df16a3cf12fd99cbe0179539"
