SUMMARY = "generated recipe based on words srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/words-3.0-28.el8.noarch.rpm \
          "

SRC_URI[words.sha256sum] = "d1bbc1d1445f84d55f7c641f6b996eb0383b52ce08a96c3a58bc9990a32178fa"
