SUMMARY = "generated recipe based on scons srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-scons = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-scons-3.0.1-8.el8.noarch.rpm \
          "

SRC_URI[python3-scons.sha256sum] = "84a41af6ae7eecb4355d4167f2458569b11e02f8b756f73047c369907bfed9d2"
