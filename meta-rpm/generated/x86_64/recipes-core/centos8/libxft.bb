SUMMARY = "generated recipe based on libXft srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xft"
DEPENDS = "fontconfig freetype libx11 libxrender pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXft = "libXft.so.2"
RPM_SONAME_REQ_libXft = "libX11.so.6 libXrender.so.1 libc.so.6 libfontconfig.so.1 libfreetype.so.6"
RDEPENDS_libXft = "fontconfig freetype glibc libX11 libXrender"
RPM_SONAME_REQ_libXft-devel = "libXft.so.2"
RPROVIDES_libXft-devel = "libXft-dev (= 2.3.2)"
RDEPENDS_libXft-devel = "fontconfig-devel freetype-devel libXft libXrender-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXft-2.3.2-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXft-devel-2.3.2-10.el8.x86_64.rpm \
          "

SRC_URI[libXft.sha256sum] = "f68ca989e45c0f23ed875d0ff4ae99af5b84565b8cc6cb2f74b5839c78e3004d"
SRC_URI[libXft-devel.sha256sum] = "8b73cafbb7cc98d0c3947c1dcfae995e4223ffcbf5d5d4a206d3ecc84440f930"
