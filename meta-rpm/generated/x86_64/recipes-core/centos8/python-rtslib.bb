SUMMARY = "generated recipe based on python-rtslib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-rtslib = "platform-python python3-kmod python3-pyudev python3-six"
RDEPENDS_target-restore = "bash platform-python python3-rtslib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-rtslib-2.1.71-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/target-restore-2.1.71-4.el8.noarch.rpm \
          "

SRC_URI[python3-rtslib.sha256sum] = "a21ea779bffe9e50cd0c4ceb365a51ea3f63071d59b0b955100bf958d701dc03"
SRC_URI[target-restore.sha256sum] = "fd0796edd80ec75e3a3c71eaa2dd4663a561969063d6285442e0732e944558f0"
