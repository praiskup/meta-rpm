SUMMARY = "generated recipe based on libiptcdata srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libiptcdata = "libiptcdata.so.0"
RPM_SONAME_REQ_libiptcdata = "libc.so.6 libiptcdata.so.0"
RDEPENDS_libiptcdata = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libiptcdata-1.0.4-21.el8.x86_64.rpm \
          "

SRC_URI[libiptcdata.sha256sum] = "75ae631487d545f8fd98439051b5a504f9958d5374b9ee968ef3aa61574080d0"
