SUMMARY = "generated recipe based on sac srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sac = "java-1.8.0-openjdk-headless javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sac-1.3-28.el8.noarch.rpm \
          "

SRC_URI[sac.sha256sum] = "6257f0f034d0bc6b20535c6884090e4f644aba1b7f2867a6563c4e5d74ab1910"
