SUMMARY = "generated recipe based on libzmf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc libpng librevenge pkgconfig-native zlib"
RPM_SONAME_PROV_libzmf = "libzmf-0.0.so.0"
RPM_SONAME_REQ_libzmf = "libc.so.6 libgcc_s.so.1 libicudata.so.60 libicuuc.so.60 libm.so.6 libpng16.so.16 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_libzmf = "glibc libgcc libicu libpng librevenge libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libzmf-0.0.2-3.el8.x86_64.rpm \
          "

SRC_URI[libzmf.sha256sum] = "63ead3697375be579a1a6894adf167f3cad1c848b0a52a62e996f8bec716c5bc"
