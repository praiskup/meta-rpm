SUMMARY = "generated recipe based on perl-HTTP-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Tiny = "perl-Carp perl-Errno perl-IO perl-MIME-Base64 perl-Socket perl-Time-Local perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-HTTP-Tiny-0.074-1.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Tiny.sha256sum] = "a1af93a1b62e8ca05b7597d5749a2b3d28735a86928f0432064fec61db1ff844"
