SUMMARY = "generated recipe based on xorg-x11-drv-vesa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-vesa = "libc.so.6"
RDEPENDS_xorg-x11-drv-vesa = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-vesa-2.4.0-3.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-vesa.sha256sum] = "b6607715c3586a3bbc15cedd4cfbaea10d53dd766a3b6b694a387bdbcdf7d68a"
