SUMMARY = "generated recipe based on bolt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native polkit systemd-libs"
RPM_SONAME_REQ_bolt = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libudev.so.1"
RDEPENDS_bolt = "bash glib2 glibc libgcc polkit-libs systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bolt-0.8-2.el8.x86_64.rpm \
          "

SRC_URI[bolt.sha256sum] = "0a2c962019d4c777fe535de44aa4900accd70a88c6b43d9773295ee03b36145c"
