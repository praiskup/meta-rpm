SUMMARY = "generated recipe based on perl-HTTP-Cookies srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Cookies = "perl-Carp perl-HTTP-Date perl-HTTP-Message perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-HTTP-Cookies-6.04-2.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Cookies.sha256sum] = "044a797823f0fc6e9af590f68626bd9d5ad5a7e3879d7c6e8ebc1557d2051c22"
