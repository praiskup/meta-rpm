SUMMARY = "generated recipe based on microdnf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libdnf libgcc libpeas pkgconfig-native util-linux"
RPM_SONAME_REQ_microdnf = "libc.so.6 libdnf.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpeas-1.0.so.0 libpthread.so.0 libsmartcols.so.1"
RDEPENDS_microdnf = "glib2 glibc libdnf libgcc libpeas libsmartcols"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/microdnf-3.0.1-8.el8.x86_64.rpm \
          "

SRC_URI[microdnf.sha256sum] = "10f2b46935f139c6324550930b79a8845b3ec511a952326d7f607faa8cd447c3"
