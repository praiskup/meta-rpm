SUMMARY = "generated recipe based on keepalived srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl net-snmp openssl pkgconfig-native"
RPM_SONAME_REQ_keepalived = "libc.so.6 libcrypto.so.1.1 libm.so.6 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libnl-3.so.200 libnl-genl-3.so.200 libssl.so.1.1"
RDEPENDS_keepalived = "bash glibc libnl3 net-snmp-agent-libs net-snmp-libs openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/keepalived-2.0.10-10.el8.x86_64.rpm \
          "

SRC_URI[keepalived.sha256sum] = "868b757a9b7e694a54edaaaa234ce1f7d18a8defc0d067c20b9f31f348398e46"
