SUMMARY = "generated recipe based on perl-B-Debug srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-B-Debug = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-B-Debug-1.26-2.el8.noarch.rpm \
          "

SRC_URI[perl-B-Debug.sha256sum] = "c08e59b912e140163e16531ff46bbc84cdf7aa95062f8f73849f8c4951452990"
