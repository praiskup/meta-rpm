SUMMARY = "generated recipe based on clutter-gst3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo clutter cogl gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 json-glib libdrm libglvnd libgudev libx11 libxcomposite libxdamage libxext libxfixes libxi libxkbcommon libxrandr mesa pango pkgconfig-native wayland"
RPM_SONAME_PROV_clutter-gst3 = "libclutter-gst-3.0.so.0"
RPM_SONAME_REQ_clutter-gst3 = "libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libdrm.so.2 libgbm.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libgstvideo-1.0.so.0 libgudev-1.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxkbcommon.so.0"
RDEPENDS_clutter-gst3 = "atk cairo cairo-gobject clutter cogl gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 json-glib libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libdrm libglvnd-egl libgudev libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon mesa-libgbm pango"
RPM_SONAME_REQ_clutter-gst3-devel = "libclutter-gst-3.0.so.0"
RPROVIDES_clutter-gst3-devel = "clutter-gst3-dev (= 3.0.26)"
RDEPENDS_clutter-gst3-devel = "clutter-devel clutter-gst3 cogl-devel glib2-devel gstreamer1-devel gstreamer1-plugins-base-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clutter-gst3-3.0.26-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/clutter-gst3-devel-3.0.26-1.el8.x86_64.rpm \
          "

SRC_URI[clutter-gst3.sha256sum] = "4c78b6b523249e213e94554f40037c4799fe1280d1b63fd7f8b9efffe0e3c683"
SRC_URI[clutter-gst3-devel.sha256sum] = "e383b4a490a15f0d763e9dce87cf7b71cc57561bf0ff90659aff24b041b2546e"
