SUMMARY = "generated recipe based on python-systemd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native platform-python3 systemd-libs"
RPM_SONAME_REQ_python3-systemd = "libc.so.6 libgcc_s.so.1 libpthread.so.0 libpython3.6m.so.1.0 libsystemd.so.0"
RDEPENDS_python3-systemd = "glibc libgcc platform-python python3-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-systemd-234-8.el8.x86_64.rpm \
          "

SRC_URI[python3-systemd.sha256sum] = "6ef6573d410f9e9dfa4bb4a4e1b2ff2f7555b50b1fc4b2ff8fb890e46b4b8d8a"
