SUMMARY = "generated recipe based on libodfgen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libodfgen = "libodfgen-0.1.so.1"
RPM_SONAME_REQ_libodfgen = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 librevenge-stream-0.0.so.0 libstdc++.so.6"
RDEPENDS_libodfgen = "glibc libgcc librevenge libstdc++"
RPM_SONAME_REQ_libodfgen-devel = "libodfgen-0.1.so.1"
RPROVIDES_libodfgen-devel = "libodfgen-dev (= 0.1.6)"
RDEPENDS_libodfgen-devel = "libodfgen librevenge-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libodfgen-0.1.6-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libodfgen-devel-0.1.6-11.el8.x86_64.rpm \
          "

SRC_URI[libodfgen.sha256sum] = "0437b5d162d3cca6b3a83883f6f1cf7e1836e2fbe245668647745735da5717ce"
SRC_URI[libodfgen-devel.sha256sum] = "860b067b43be71b8512eb896efeddc699f6291488ca73833dea1b8c01d47c3ec"
