SUMMARY = "generated recipe based on expat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_expat = "libexpat.so.1"
RPM_SONAME_REQ_expat = "libc.so.6 libexpat.so.1"
RDEPENDS_expat = "glibc"
RPM_SONAME_REQ_expat-devel = "libexpat.so.1"
RPROVIDES_expat-devel = "expat-dev (= 2.2.5)"
RDEPENDS_expat-devel = "expat pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/expat-2.2.5-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/expat-devel-2.2.5-3.el8.x86_64.rpm \
          "

SRC_URI[expat.sha256sum] = "dfe199dc03a6d4122bb5085bc3282777735ec4cfa4bd5e9ee372dcc4cf09b525"
SRC_URI[expat-devel.sha256sum] = "ebb953e277e7f718aceea4500d120d5c3a127a092e55f7bdad9478beebd7b53e"
