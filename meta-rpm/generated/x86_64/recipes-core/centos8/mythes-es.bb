SUMMARY = "generated recipe based on mythes-es srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-es = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-es-2.3-1.el8.noarch.rpm \
          "

SRC_URI[mythes-es.sha256sum] = "a465f6da39f47a049928ed9c2525f516ba0450bf710356203469b43a525e5864"
