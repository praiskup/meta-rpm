SUMMARY = "generated recipe based on perl-perlfaq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-perlfaq = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-perlfaq-5.20180605-1.el8.noarch.rpm \
          "

SRC_URI[perl-perlfaq.sha256sum] = "749d6ddca1d52398537d0a9c8f99bb6551a1f4baf725b0aa7537fbca9169959f"
