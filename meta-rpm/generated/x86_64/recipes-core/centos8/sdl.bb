SUMMARY = "generated recipe based on SDL srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib libx11 libxext libxrandr libxrender mesa mesa-libglu pkgconfig-native"
RPM_SONAME_PROV_SDL = "libSDL-1.2.so.0"
RPM_SONAME_REQ_SDL = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_SDL = "glibc"
RPM_SONAME_REQ_SDL-devel = "libSDL-1.2.so.0"
RPROVIDES_SDL-devel = "SDL-dev (= 1.2.15)"
RDEPENDS_SDL-devel = "SDL alsa-lib-devel bash libX11-devel libXext-devel libXrandr-devel libXrender-devel mesa-libGL-devel mesa-libGLU-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/SDL-1.2.15-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/SDL-devel-1.2.15-37.el8.x86_64.rpm \
          "

SRC_URI[SDL.sha256sum] = "df1a14a51a7038cfe1cbccf925f1b4dda41148419b543c1eb5e84cf11bcd681c"
SRC_URI[SDL-devel.sha256sum] = "4a64e23cbbd1a4479571e0d20cb62113bde23e1409c478f487539c8fa41cdc72"
