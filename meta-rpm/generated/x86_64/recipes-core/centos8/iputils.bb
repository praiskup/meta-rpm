SUMMARY = "generated recipe based on iputils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap libidn2 openssl pkgconfig-native"
RPM_SONAME_REQ_iputils = "libc.so.6 libcap.so.2 libcrypto.so.1.1 libidn2.so.0 libm.so.6 libresolv.so.2 librt.so.1"
RDEPENDS_iputils = "bash glibc libcap libidn2 openssl-libs systemd"
RPM_SONAME_REQ_iputils-ninfod = "libc.so.6 libcap.so.2 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_iputils-ninfod = "bash glibc iputils libcap openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iputils-20180629-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iputils-ninfod-20180629-2.el8.x86_64.rpm \
          "

SRC_URI[iputils.sha256sum] = "0833255a222fd269dd507340219ca8483924798c1ecae60da5a1db923dbad68f"
SRC_URI[iputils-ninfod.sha256sum] = "01195f4164d13312bc14eb23e66461eb8f8fc388b04a157e3b8f749bba34e461"
