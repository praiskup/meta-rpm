SUMMARY = "generated recipe based on perl-IO-Multiplex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-Multiplex = "perl-Carp perl-IO perl-Socket perl-Time-HiRes perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IO-Multiplex-1.16-9.el8.noarch.rpm \
          "

SRC_URI[perl-IO-Multiplex.sha256sum] = "3808d732d8749d24de315263b0cce5287880da4a3289c4933a3c5bc11c819f3e"
