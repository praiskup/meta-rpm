SUMMARY = "generated recipe based on at-spi2-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus dbus-libs glib-2.0 libx11 libxtst pkgconfig-native"
RPM_SONAME_PROV_at-spi2-core = "libatspi.so.0"
RPM_SONAME_REQ_at-spi2-core = "libX11.so.6 libXtst.so.6 libatspi.so.0 libc.so.6 libdbus-1.so.3 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_at-spi2-core = "dbus dbus-libs glib2 glibc libX11 libXtst"
RPM_SONAME_REQ_at-spi2-core-devel = "libatspi.so.0"
RPROVIDES_at-spi2-core-devel = "at-spi2-core-dev (= 2.28.0)"
RDEPENDS_at-spi2-core-devel = "at-spi2-core dbus-devel glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/at-spi2-core-2.28.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/at-spi2-core-devel-2.28.0-1.el8.x86_64.rpm \
          "

SRC_URI[at-spi2-core.sha256sum] = "7db76e337e1b033da9452240607ac4270030584f6cb922e4f435f56e2fecc82a"
SRC_URI[at-spi2-core-devel.sha256sum] = "054aae3ffc0e8769c4058f223b96fac2c9cd41e3d5c12c89cce8bb56b81612d6"
