SUMMARY = "generated recipe based on trousers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_trousers = "libc.so.6 libcrypto.so.1.1 libpthread.so.0"
RDEPENDS_trousers = "bash glibc openssl-libs shadow-utils systemd trousers-lib"
RPM_SONAME_REQ_trousers-devel = "libtspi.so.1"
RPROVIDES_trousers-devel = "trousers-dev (= 0.3.14)"
RDEPENDS_trousers-devel = "trousers-lib"
RPM_SONAME_PROV_trousers-lib = "libtspi.so.1"
RPM_SONAME_REQ_trousers-lib = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 libssl.so.1.1"
RDEPENDS_trousers-lib = "glibc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/trousers-0.3.14-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/trousers-lib-0.3.14-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/trousers-devel-0.3.14-4.el8.x86_64.rpm \
          "

SRC_URI[trousers.sha256sum] = "39fe21319fae9dcb381f2b519ee0d37b723c0b7b6eafa88bb8b695c55f910760"
SRC_URI[trousers-devel.sha256sum] = "74a4c648ed0e5c3b05c0c6a3f5e13cb3b66dca7c57eca8cce897d8de1f8f3861"
SRC_URI[trousers-lib.sha256sum] = "17f2253df0a2d1a379d100b10e98e3ce8ec557f53d808f16dd4eac34d5c77b84"
