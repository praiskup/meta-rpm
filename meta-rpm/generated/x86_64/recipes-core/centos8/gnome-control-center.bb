SUMMARY = "generated recipe based on gnome-control-center srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "accountsservice atk cairo cheese clutter clutter-gtk colord colord-gtk cups-libs fontconfig gdk-pixbuf glib-2.0 gnome-bluetooth gnome-desktop3 gnome-online-accounts grilo gtk+3 ibus krb5-libs libcanberra libgcc libgtop2 libgudev libpwquality libsecret libsoup-2.4 libwacom libx11 libxi libxml2 modemmanager network-manager-applet networkmanager pango pkgconfig-native polkit pulseaudio samba upower wayland"
RPM_SONAME_REQ_gnome-control-center = "libX11.so.6 libXi.so.6 libaccountsservice.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libcheese-gtk.so.25 libcheese.so.8 libclutter-1.0.so.0 libclutter-gtk-1.0.so.0 libcolord-gtk.so.1 libcolord.so.2 libcups.so.2 libfontconfig.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-bluetooth.so.13 libgnome-desktop-3.so.17 libgoa-1.0.so.0 libgoa-backend-1.0.so.1 libgobject-2.0.so.0 libgrilo-0.3.so.0 libgtk-3.so.0 libgtop-2.0.so.11 libgudev-1.0.so.0 libibus-1.0.so.5 libkrb5.so.3 libm.so.6 libmm-glib.so.0 libnm.so.0 libnma.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libpwquality.so.1 libsecret-1.so.0 libsmbclient.so.0 libsoup-2.4.so.1 libupower-glib.so.3 libwacom.so.2 libwayland-server.so.0 libxml2.so.2"
RDEPENDS_gnome-control-center = "ModemManager-glib NetworkManager-libnm accountsservice accountsservice-libs alsa-lib atk bolt cairo cairo-gobject cheese-libs clutter clutter-gtk colord colord-gtk colord-libs cups-libs cups-pk-helper dbus-x11 fontconfig gdk-pixbuf2 glib2 glibc glx-utils gnome-bluetooth gnome-bluetooth-libs gnome-control-center-filesystem gnome-desktop3 gnome-online-accounts gnome-settings-daemon grilo gsettings-desktop-schemas gtk3 ibus-libs iso-codes krb5-libs libX11 libXi libcanberra libcanberra-gtk3 libgcc libgnomekbd libgtop2 libgudev libnma libpwquality libsecret libsmbclient libsoup libwacom libwayland-server libxml2 nm-connection-editor pango pkgconf-pkg-config polkit-libs pulseaudio-libs pulseaudio-libs-glib2 switcheroo-control upower"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-control-center-3.28.2-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-control-center-filesystem-3.28.2-19.el8.noarch.rpm \
          "

SRC_URI[gnome-control-center.sha256sum] = "89190f299f8922de64f2227c61d70bf5c8b88584b9fc3867ad760e7b7f9f4005"
SRC_URI[gnome-control-center-filesystem.sha256sum] = "58b173c0e63b25664503a679cf34dd80f895a58c171926dce983e40cb81b9025"
