SUMMARY = "generated recipe based on pycairo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cairo = "libc.so.6 libcairo.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RPROVIDES_python3-cairo = "python3-pycairo (= 1.16.3)"
RDEPENDS_python3-cairo = "cairo glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-cairo-1.16.3-6.el8.x86_64.rpm \
          "

SRC_URI[python3-cairo.sha256sum] = "ce43ade7d1cb2519c12119621990ac8075c2f9e7577c739990ca949d6c42737a"
