SUMMARY = "generated recipe based on freetype srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 libpng pkgconfig-native zlib"
RPM_SONAME_PROV_freetype = "libfreetype.so.6"
RPM_SONAME_REQ_freetype = "libbz2.so.1 libc.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_freetype = "bash bzip2-libs glibc libpng zlib"
RPM_SONAME_REQ_freetype-devel = "libfreetype.so.6"
RPROVIDES_freetype-devel = "freetype-dev (= 2.9.1)"
RDEPENDS_freetype-devel = "bash bzip2-devel freetype libpng-devel pkgconf pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/freetype-2.9.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/freetype-devel-2.9.1-4.el8.x86_64.rpm \
          "

SRC_URI[freetype.sha256sum] = "d1a91a3ccc19b72f302e336b0932f92fb614a7d287cd4afa5055b3d278a28f84"
SRC_URI[freetype-devel.sha256sum] = "751376b50ba56f28fb68a5b5781baf817038eba14149eeba151cc0cf04c5e0db"
