SUMMARY = "generated recipe based on libXdamage srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xdamage"
DEPENDS = "libx11 libxfixes pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXdamage = "libXdamage.so.1"
RPM_SONAME_REQ_libXdamage = "libX11.so.6 libXfixes.so.3 libc.so.6"
RDEPENDS_libXdamage = "glibc libX11 libXfixes"
RPM_SONAME_REQ_libXdamage-devel = "libXdamage.so.1"
RPROVIDES_libXdamage-devel = "libXdamage-dev (= 1.1.4)"
RDEPENDS_libXdamage-devel = "libX11-devel libXdamage libXfixes-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXdamage-1.1.4-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXdamage-devel-1.1.4-14.el8.x86_64.rpm \
          "

SRC_URI[libXdamage.sha256sum] = "b4579c48c59ef7874191c6815b5321e7427ccd8f9cc7ac8624b7033485be82f2"
SRC_URI[libXdamage-devel.sha256sum] = "b5a20c05a7ba1451e5e82506f9ab2e77ab9f347c82b48c06bf96f617c3cd0c3d"
