SUMMARY = "generated recipe based on maven srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven = "aopalliance apache-commons-cli apache-commons-codec apache-commons-io apache-commons-lang3 apache-commons-logging atinject bash cdi-api chkconfig geronimo-annotation google-guice guava20 hawtjni-runtime httpcomponents-client httpcomponents-core jansi jansi-native java-1.8.0-openjdk-devel jcl-over-slf4j maven-lib maven-resolver-api maven-resolver-connector-basic maven-resolver-impl maven-resolver-spi maven-resolver-transport-wagon maven-resolver-util maven-shared-utils maven-wagon-file maven-wagon-http maven-wagon-http-shared maven-wagon-provider-api plexus-cipher plexus-classworlds plexus-containers-component-annotations plexus-interpolation plexus-sec-dispatcher plexus-utils sisu-inject sisu-plexus slf4j"
RDEPENDS_maven-javadoc = "javapackages-filesystem"
RDEPENDS_maven-lib = "apache-commons-cli apache-commons-lang3 atinject bash geronimo-annotation google-guice guava20 java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools maven-resolver-api maven-resolver-impl maven-resolver-spi maven-resolver-util maven-shared-utils maven-wagon-provider-api plexus-cipher plexus-classworlds plexus-containers-component-annotations plexus-interpolation plexus-sec-dispatcher plexus-utils sisu-inject sisu-plexus slf4j"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-3.5.4-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-javadoc-3.5.4-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-lib-3.5.4-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven.sha256sum] = "dd9afb4307f67d8f10c9bba201c754e64964432e58749b1cce18380bff790acc"
SRC_URI[maven-javadoc.sha256sum] = "3c1db3897016764a4ce7217032afaf5e3036b034d6fbcd6fb20a35cccdcd194f"
SRC_URI[maven-lib.sha256sum] = "cbb8b0b2d82f5898a5ed8ec17a47fc3d75dc04d9ced79ff3acfa18ad39993135"
