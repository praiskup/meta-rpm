SUMMARY = "generated recipe based on librelp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls pkgconfig-native"
RPM_SONAME_PROV_librelp = "librelp.so.0"
RPM_SONAME_REQ_librelp = "libc.so.6 libgnutls.so.30"
RDEPENDS_librelp = "bash glibc gnutls"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librelp-1.2.16-1.el8.x86_64.rpm \
          "

SRC_URI[librelp.sha256sum] = "406cc594c699048bff13406280366069943182dbc65d2053989f29c9792f2745"
