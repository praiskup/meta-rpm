SUMMARY = "generated recipe based on speexdsp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_speexdsp = "libspeexdsp.so.1"
RPM_SONAME_REQ_speexdsp = "libc.so.6 libm.so.6"
RDEPENDS_speexdsp = "glibc"
RPM_SONAME_REQ_speexdsp-devel = "libspeexdsp.so.1"
RPROVIDES_speexdsp-devel = "speexdsp-dev (= 1.2)"
RDEPENDS_speexdsp-devel = "pkgconf-pkg-config speexdsp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/speexdsp-1.2-0.13.rc3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/speexdsp-devel-1.2-0.13.rc3.el8.x86_64.rpm \
          "

SRC_URI[speexdsp.sha256sum] = "73167a14680ff832cb97ab59677b0a4ae8db9bbad59d23f51fdcd8d0f364a35d"
SRC_URI[speexdsp-devel.sha256sum] = "bf6d2a9191f9fa97db01ea0df74937ac6abe39fdf75eab0e4adb8657ffb2c5aa"
