SUMMARY = "generated recipe based on perl-Sub-Exporter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Exporter = "perl-Carp perl-Data-OptList perl-Package-Generator perl-Params-Util perl-Sub-Install perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Sub-Exporter-0.987-15.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Exporter.sha256sum] = "8aed9eefaf53b858e8d492331f3e48c350ca75e7fd95708e2c0f6e0ce9f78572"
