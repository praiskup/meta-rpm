SUMMARY = "generated recipe based on pciutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "kmod pkgconfig-native zlib"
RPM_SONAME_REQ_pciutils = "libc.so.6 libkmod.so.2 libpci.so.3"
RDEPENDS_pciutils = "bash glibc hwdata kmod-libs pciutils-libs"
RPM_SONAME_REQ_pciutils-devel = "libpci.so.3"
RPROVIDES_pciutils-devel = "pciutils-dev (= 3.5.6)"
RDEPENDS_pciutils-devel = "pciutils pciutils-libs pkgconf-pkg-config zlib-devel"
RPM_SONAME_PROV_pciutils-libs = "libpci.so.3"
RPM_SONAME_REQ_pciutils-libs = "libc.so.6 libresolv.so.2"
RDEPENDS_pciutils-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pciutils-3.5.6-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pciutils-devel-3.5.6-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pciutils-libs-3.5.6-4.el8.x86_64.rpm \
          "

SRC_URI[pciutils.sha256sum] = "9222c31fae4d4de0545f63b55a093379842e8cf42de9f130c4fe8e667db29de9"
SRC_URI[pciutils-devel.sha256sum] = "5caa82e0b086ce7980493ae57a1c276b228569f10a1d655dd8681f9b4d345b21"
SRC_URI[pciutils-libs.sha256sum] = "1c43320a43e5e590978d32962939e0d39e9151d7dc424488b79aaf245a8b64c0"
