SUMMARY = "generated recipe based on librepo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 gpgme libgcc libgpg-error libxml2 openssl pkgconfig-native platform-python3"
RPM_SONAME_PROV_librepo = "librepo.so.0"
RPM_SONAME_REQ_librepo = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libglib-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libxml2.so.2"
RDEPENDS_librepo = "glib2 glibc gpgme libcurl libgcc libgpg-error libxml2 openssl-libs"
RPM_SONAME_REQ_python3-librepo = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libglib-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libpython3.6m.so.1.0 librepo.so.0 libxml2.so.2"
RDEPENDS_python3-librepo = "glib2 glibc gpgme libcurl libgcc libgpg-error librepo libxml2 openssl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/librepo-1.11.0-3.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-librepo-1.11.0-3.el8_2.x86_64.rpm \
          "

SRC_URI[librepo.sha256sum] = "89e31ab45edf6c51ef8c7bc249a58e33970d02479692b7f21be47c2efe43d379"
SRC_URI[python3-librepo.sha256sum] = "2750887a9ae4234938d4924a761b3a2354e47ac123acafe7610fc26683f70450"
