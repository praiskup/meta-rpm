SUMMARY = "generated recipe based on libsoup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 krb5-libs libxml2 pkgconfig-native sqlite3"
RPM_SONAME_PROV_libsoup = "libsoup-2.4.so.1 libsoup-gnome-2.4.so.1"
RPM_SONAME_REQ_libsoup = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libsoup-2.4.so.1 libsqlite3.so.0 libxml2.so.2"
RDEPENDS_libsoup = "glib-networking glib2 glibc krb5-libs libxml2 sqlite-libs"
RPM_SONAME_REQ_libsoup-devel = "libsoup-2.4.so.1 libsoup-gnome-2.4.so.1"
RPROVIDES_libsoup-devel = "libsoup-dev (= 2.62.3)"
RDEPENDS_libsoup-devel = "glib2-devel libsoup libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsoup-devel-2.62.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsoup-2.62.3-1.el8.x86_64.rpm \
          "

SRC_URI[libsoup.sha256sum] = "035f0f795edaf535f68690497dee084063e64a653868083eb5962f66dfdb5866"
SRC_URI[libsoup-devel.sha256sum] = "44ff4497c95d8a4b2e31a1752c509d8a61932a7acb636caeb822a6eed5210512"
