SUMMARY = "generated recipe based on icedtea-web srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_icedtea-web = "GConf2 bash chkconfig java-1.8.0-openjdk java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools mozilla-filesystem tagsoup"
RDEPENDS_icedtea-web-javadoc = "icedtea-web javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/icedtea-web-1.7.1-17.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/icedtea-web-javadoc-1.7.1-17.el8.noarch.rpm \
          "

SRC_URI[icedtea-web.sha256sum] = "433ee438ee781cdcb979592423b85ba7af01c82911d9aa56086ba6a485cf2eaf"
SRC_URI[icedtea-web-javadoc.sha256sum] = "60dff9d894daca44bf63ba6cc8953ee17e9055f0748b7c7ec09c1da713a3b545"
