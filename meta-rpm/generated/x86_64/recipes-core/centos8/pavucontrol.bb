SUMMARY = "generated recipe based on pavucontrol srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk atkmm cairo cairomm gdk-pixbuf glib-2.0 glibmm24 gtk+3 gtkmm30 libcanberra libgcc libsigc++20 libx11 pango pangomm pkgconfig-native pulseaudio"
RPM_SONAME_REQ_pavucontrol = "libX11.so.6 libatk-1.0.so.0 libatkmm-1.6.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcairomm-1.0.so.1 libcanberra-gtk3.so.0 libcanberra.so.0 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgdkmm-3.0.so.1 libgio-2.0.so.0 libgiomm-2.4.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libgtk-3.so.0 libgtkmm-3.0.so.1 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangomm-1.4.so.1 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_pavucontrol = "atk atkmm cairo cairo-gobject cairomm gdk-pixbuf2 glib2 glibc glibmm24 gtk3 gtkmm30 libX11 libcanberra libcanberra-gtk3 libgcc libsigc++20 libstdc++ pango pangomm pulseaudio-libs pulseaudio-libs-glib2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pavucontrol-3.0-11.el8.x86_64.rpm \
          "

SRC_URI[pavucontrol.sha256sum] = "8a3e1e1917d5264ed7f10aab5484cfdd6192c6f6e8f81971ca5b99ce1b93d3e3"
