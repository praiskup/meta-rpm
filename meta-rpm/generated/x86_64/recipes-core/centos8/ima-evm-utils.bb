SUMMARY = "generated recipe based on ima-evm-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "keyutils openssl pkgconfig-native"
RPM_SONAME_PROV_ima-evm-utils = "libimaevm.so.0"
RPM_SONAME_REQ_ima-evm-utils = "libc.so.6 libcrypto.so.1.1 libimaevm.so.0 libkeyutils.so.1 libssl.so.1.1"
RDEPENDS_ima-evm-utils = "glibc keyutils-libs openssl-libs"
RPM_SONAME_REQ_ima-evm-utils-devel = "libimaevm.so.0"
RPROVIDES_ima-evm-utils-devel = "ima-evm-utils-dev (= 1.1)"
RDEPENDS_ima-evm-utils-devel = "ima-evm-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ima-evm-utils-1.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ima-evm-utils-devel-1.1-5.el8.x86_64.rpm \
          "

SRC_URI[ima-evm-utils.sha256sum] = "f913d2f31ad8565ebaea55fa577fed77aa350fb9042c409b6e63662ece3161ee"
SRC_URI[ima-evm-utils-devel.sha256sum] = "794d4f32cac3965a1a318b127da2349790197705303b4b09d5b2899550dc288f"
