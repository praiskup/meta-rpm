SUMMARY = "generated recipe based on redhat-menus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_redhat-menus = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-menus-12.0.2-12.el8.noarch.rpm \
          "

SRC_URI[redhat-menus.sha256sum] = "2ed86255c30bf6fcc921c496d9ca046e2f5e6d8c3eb3d2119ca48cf2a86c8bb5"
