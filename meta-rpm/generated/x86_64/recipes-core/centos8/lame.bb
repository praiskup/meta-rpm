SUMMARY = "generated recipe based on lame srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lame-devel = "libmp3lame.so.0"
RPROVIDES_lame-devel = "lame-dev (= 3.100)"
RDEPENDS_lame-devel = "lame-libs"
RPM_SONAME_PROV_lame-libs = "libmp3lame.so.0"
RPM_SONAME_REQ_lame-libs = "libc.so.6 libm.so.6"
RDEPENDS_lame-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lame-libs-3.100-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lame-devel-3.100-6.el8.x86_64.rpm \
          "

SRC_URI[lame-devel.sha256sum] = "b14507cab1c95901f40c76cd57c2ab666dbda1703d31baa816de4dbb90200f99"
SRC_URI[lame-libs.sha256sum] = "15cb7d9bd95cdb834624c72e3d78380a2fdfb8bc2d4ad7f16da33fe1e3ba10ba"
