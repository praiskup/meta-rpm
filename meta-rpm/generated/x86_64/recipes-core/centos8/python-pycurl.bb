SUMMARY = "generated recipe based on python-pycurl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pycurl = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_python3-pycurl = "glibc libcurl openssl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pycurl-7.43.0.2-4.el8.x86_64.rpm \
          "

SRC_URI[python3-pycurl.sha256sum] = "3d25516a38dda688a719100a261050d12e975da6890af6c8015c3e6343693994"
