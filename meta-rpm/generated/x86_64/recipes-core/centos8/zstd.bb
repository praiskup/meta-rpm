SUMMARY = "generated recipe based on zstd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libzstd = "libzstd.so.1"
RPM_SONAME_REQ_libzstd = "libc.so.6"
RDEPENDS_libzstd = "glibc"
RPM_SONAME_REQ_libzstd-devel = "libzstd.so.1"
RPROVIDES_libzstd-devel = "libzstd-dev (= 1.4.2)"
RDEPENDS_libzstd-devel = "libzstd pkgconf-pkg-config"
RPM_SONAME_REQ_zstd = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_zstd = "bash glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/zstd-1.4.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libzstd-1.4.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libzstd-devel-1.4.2-2.el8.x86_64.rpm \
          "

SRC_URI[libzstd.sha256sum] = "40851fcafb9b4d0ac3410850996d9ace46e6eaa5c11bd426d5f5696996149ee4"
SRC_URI[libzstd-devel.sha256sum] = "254eb937336a4d30496b6f15a85005acc52d050d43046f80feda7d350ff31e0f"
SRC_URI[zstd.sha256sum] = "fd44ff7a907954b29c76c72093e9535dd5d983da3226e4225189953213a4047d"
