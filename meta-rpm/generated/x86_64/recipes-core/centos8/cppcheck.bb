SUMMARY = "generated recipe based on cppcheck srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libpcre pkgconfig-native tinyxml2"
RPM_SONAME_REQ_cppcheck = "libc.so.6 libgcc_s.so.1 libm.so.6 libpcre.so.1 libstdc++.so.6 libtinyxml2.so.6"
RDEPENDS_cppcheck = "glibc libgcc libstdc++ pcre tinyxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cppcheck-1.83-1.el8.x86_64.rpm \
          "

SRC_URI[cppcheck.sha256sum] = "62b420b869fae334e81513df2c4afcbe497ce89285d89294f9f4e19f769e6ca0"
