SUMMARY = "generated recipe based on cdparanoia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cdparanoia = "libc.so.6 libcdda_interface.so.0 libcdda_paranoia.so.0 libm.so.6 librt.so.1"
RDEPENDS_cdparanoia = "cdparanoia-libs glibc"
RPM_SONAME_REQ_cdparanoia-devel = "libcdda_interface.so.0 libcdda_paranoia.so.0"
RPROVIDES_cdparanoia-devel = "cdparanoia-dev (= 10.2)"
RDEPENDS_cdparanoia-devel = "cdparanoia-libs"
RPM_SONAME_PROV_cdparanoia-libs = "libcdda_interface.so.0 libcdda_paranoia.so.0"
RPM_SONAME_REQ_cdparanoia-libs = "libc.so.6 libcdda_interface.so.0 libm.so.6 librt.so.1"
RDEPENDS_cdparanoia-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cdparanoia-10.2-27.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cdparanoia-libs-10.2-27.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cdparanoia-devel-10.2-27.el8.x86_64.rpm \
          "

SRC_URI[cdparanoia.sha256sum] = "a76ef09be403325a7d27c3ddd174ae10eea629cb4471c3483a4f7d284e168a15"
SRC_URI[cdparanoia-devel.sha256sum] = "5ae30f83e8070f0c3b4350d149cb0015e4042a4b045693d3bb9d3375588d3be3"
SRC_URI[cdparanoia-libs.sha256sum] = "58d614b26d113bbcb85ddbc2198ac9dcb640c323b0be64cb8a50151c65402778"
