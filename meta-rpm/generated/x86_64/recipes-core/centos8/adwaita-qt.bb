SUMMARY = "generated recipe based on adwaita-qt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native qt5-qtbase"
RPM_SONAME_REQ_adwaita-qt = "libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_adwaita-qt = "glibc libgcc libstdc++ qt5-qtbase qt5-qtbase-gui"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/adwaita-qt-1.0-5.el8.x86_64.rpm \
          "

SRC_URI[adwaita-qt.sha256sum] = "dc92a2801120b6b1ca1a96bc9e3173dc1a19365a6880c5acca339e2a2040c682"
