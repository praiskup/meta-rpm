SUMMARY = "generated recipe based on dhcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bind e2fsprogs krb5-libs libcap libcap-ng openldap openssl pkgconfig-native systemd-libs"
RPM_SONAME_REQ_dhcp-client = "libc.so.6 libcap-ng.so.0 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdns-export.so.1107 libgssapi_krb5.so.2 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163 libk5crypto.so.3 libkrb5.so.3 libomapi.so.0 libsystemd.so.0"
RDEPENDS_dhcp-client = "bash bind-export-libs coreutils dhcp-common dhcp-libs gawk glibc grep ipcalc iproute iputils krb5-libs libcap libcap-ng libcom_err openssl-libs sed systemd systemd-libs"
RPM_SONAME_PROV_dhcp-libs = "libdhcpctl.so.0 libomapi.so.0"
RPM_SONAME_REQ_dhcp-libs = "libc.so.6 libsystemd.so.0"
RDEPENDS_dhcp-libs = "glibc systemd-libs"
RPM_SONAME_REQ_dhcp-relay = "libc.so.6 libcap-ng.so.0 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdns-export.so.1107 libgssapi_krb5.so.2 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163 libk5crypto.so.3 libkrb5.so.3 libomapi.so.0 libsystemd.so.0"
RDEPENDS_dhcp-relay = "bash bind-export-libs dhcp-common dhcp-libs glibc grep krb5-libs libcap libcap-ng libcom_err openssl-libs sed systemd systemd-libs"
RPM_SONAME_REQ_dhcp-server = "libc.so.6 libcap.so.2 libcom_err.so.2 libcrypto.so.1.1 libdhcpctl.so.0 libdns-export.so.1107 libgssapi_krb5.so.2 libirs-export.so.161 libisc-export.so.1104 libisccfg-export.so.163 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libomapi.so.0 libsystemd.so.0"
RDEPENDS_dhcp-server = "bash bind-export-libs coreutils dhcp-common dhcp-libs glibc grep krb5-libs libcap libcom_err openldap openssl-libs sed shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dhcp-client-4.3.6-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dhcp-common-4.3.6-40.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dhcp-libs-4.3.6-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dhcp-relay-4.3.6-40.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dhcp-server-4.3.6-40.el8.x86_64.rpm \
          "

SRC_URI[dhcp-client.sha256sum] = "fb985ad03e043115c9bca125227cf2fd54811e029b488350a007ea66c2ac4a02"
SRC_URI[dhcp-common.sha256sum] = "9ab0e4ecf16648b4271db6486c5a3d8f7ece0e552317b5bac2bc008876acffcb"
SRC_URI[dhcp-libs.sha256sum] = "7f79728580d097b45100a134e8e717d98c700af7df225a71309fc20b4609af76"
SRC_URI[dhcp-relay.sha256sum] = "9598050ad261da3c003f5e5b5b19a8c2a4c1053666d1b70489a829e5e0ad8c59"
SRC_URI[dhcp-server.sha256sum] = "b0d524fb404f9e38de75361c7ee6b276cf3be78f3c4b216c248532d88b61ba56"
