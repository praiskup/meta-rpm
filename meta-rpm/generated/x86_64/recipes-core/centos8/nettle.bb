SUMMARY = "generated recipe based on nettle srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp pkgconfig-native"
RPM_SONAME_PROV_nettle = "libhogweed.so.4 libnettle.so.6"
RPM_SONAME_REQ_nettle = "libc.so.6 libgmp.so.10 libnettle.so.6"
RDEPENDS_nettle = "bash glibc gmp info"
RPM_SONAME_REQ_nettle-devel = "libhogweed.so.4 libnettle.so.6"
RPROVIDES_nettle-devel = "nettle-dev (= 3.4.1)"
RDEPENDS_nettle-devel = "gmp-devel nettle pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nettle-devel-3.4.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nettle-3.4.1-1.el8.x86_64.rpm \
          "

SRC_URI[nettle.sha256sum] = "5d84a3a0ec3c27b50e002c1c27fa97971bf706cf7493a7382d518c5cad657da1"
SRC_URI[nettle-devel.sha256sum] = "9535fb28ce113468540b6b9790c0560c3cd1b251f37bbfe19541a1d36b914ca5"
