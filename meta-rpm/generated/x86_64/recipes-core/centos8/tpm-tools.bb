SUMMARY = "generated recipe based on tpm-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native trousers"
RPM_SONAME_PROV_tpm-tools = "libtpm_unseal.so.1"
RPM_SONAME_REQ_tpm-tools = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libtpm_unseal.so.1 libtspi.so.1"
RDEPENDS_tpm-tools = "glibc openssl-libs trousers-lib"
RPM_SONAME_REQ_tpm-tools-devel = "libtpm_unseal.so.1"
RPROVIDES_tpm-tools-devel = "tpm-tools-dev (= 1.3.9)"
RDEPENDS_tpm-tools-devel = "tpm-tools"
RPM_SONAME_REQ_tpm-tools-pkcs11 = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libtspi.so.1"
RDEPENDS_tpm-tools-pkcs11 = "glibc opencryptoki-libs openssl-libs trousers-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tpm-tools-1.3.9-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tpm-tools-pkcs11-1.3.9-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tpm-tools-devel-1.3.9-7.el8.x86_64.rpm \
          "

SRC_URI[tpm-tools.sha256sum] = "568b7e48ad01579ef28a191ad6b5386b1e8b910d5ad0a8691d383bdf34aa0c9a"
SRC_URI[tpm-tools-devel.sha256sum] = "2f256b59e887758f4ba4b0ffb61a9467306ddafc069a39e81d8dc7ee34ad5038"
SRC_URI[tpm-tools-pkcs11.sha256sum] = "c43a84179ad9a1fe0f21b957a1171a8179d281f78cb521cd7b6c1d420751c025"
