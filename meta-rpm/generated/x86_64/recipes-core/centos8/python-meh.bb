SUMMARY = "generated recipe based on python-meh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-meh = "libreport-cli platform-python python3-dbus python3-libreport python3-rpm"
RDEPENDS_python3-meh-gui = "gtk3 libreport-gtk platform-python python3-gobject python3-meh"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-meh-0.47.2-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-meh-gui-0.47.2-1.el8.noarch.rpm \
          "

SRC_URI[python3-meh.sha256sum] = "6c42064ce0e4dd6724de01efe0d4c0e30a0737716fd9fa6cb96170d2792291cd"
SRC_URI[python3-meh-gui.sha256sum] = "89d573874bd9d1a34f1fbb41bc98fed99aeabb37f80a9028094fe66baa5a8405"
