SUMMARY = "generated recipe based on libpipeline srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpipeline = "libpipeline.so.1"
RPM_SONAME_REQ_libpipeline = "libc.so.6"
RDEPENDS_libpipeline = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpipeline-1.5.0-2.el8.x86_64.rpm \
          "

SRC_URI[libpipeline.sha256sum] = "9eb9c1a67c5be04487cc133bdb8498eaf260e4d930a0143d2e1aa772e3d6cf64"
