SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/libc virtual/libiconv virtual/libintl"
DEPENDS = "kernel-headers"
RPM_SONAME_PROV_glibc = "ld-linux-x86-64.so.2 libBrokenLocale.so.1 libCNS.so libGB.so libISOIR165.so libJIS.so libJISX0213.so libKSC.so libSegFault.so libanl.so.1 libc.so.6 libdl.so.2 libm.so.6 libmemusage.so libmvec.so.1 libnss_compat.so.2 libnss_dns.so.2 libnss_files.so.2 libpcprofile.so libpthread.so.0 libresolv.so.2 librt.so.1 libthread_db.so.1 libutil.so.1"
RPM_SONAME_REQ_glibc = "ld-linux-x86-64.so.2 libCNS.so libGB.so libISOIR165.so libJIS.so libJISX0213.so libKSC.so libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libresolv.so.2"
RPROVIDES_glibc = "glibc-charmap-big5 (= 2.28) glibc-charmap-cp1251 (= 2.28) glibc-charmap-euc-jp (= 2.28) glibc-charmap-euc-kr (= 2.28) glibc-charmap-iso-8859-1 (= 2.28) glibc-charmap-iso-8859-2 (= 2.28) glibc-charmap-iso-8859-9 (= 2.28) glibc-charmap-koi8-r (= 2.28) glibc-gconv-ansi_x3.110 (= 2.28) glibc-gconv-armscii-8 (= 2.28) glibc-gconv-asmo_449 (= 2.28) glibc-gconv-big5 (= 2.28) glibc-gconv-big5hkscs (= 2.28) glibc-gconv-brf (= 2.28) glibc-gconv-cp10007 (= 2.28) glibc-gconv-cp1125 (= 2.28) glibc-gconv-cp1250 (= 2.28) glibc-gconv-cp1251 (= 2.28) glibc-gconv-cp1252 (= 2.28) glibc-gconv-cp1253 (= 2.28) glibc-gconv-cp1254 (= 2.28) glibc-gconv-cp1255 (= 2.28) glibc-gconv-cp1256 (= 2.28) glibc-gconv-cp1257 (= 2.28) glibc-gconv-cp1258 (= 2.28) glibc-gconv-cp737 (= 2.28) glibc-gconv-cp770 (= 2.28) glibc-gconv-cp771 (= 2.28) glibc-gconv-cp772 (= 2.28) glibc-gconv-cp773 (= 2.28) glibc-gconv-cp774 (= 2.28) glibc-gconv-cp775 (= 2.28) glibc-gconv-cp932 (= 2.28) glibc-gconv-csn_369103 (= 2.28) glibc-gconv-cwi (= 2.28) glibc-gconv-dec-mcs (= 2.28) glibc-gconv-ebcdic-at-de (= 2.28) glibc-gconv-ebcdic-at-de-a (= 2.28) glibc-gconv-ebcdic-ca-fr (= 2.28) glibc-gconv-ebcdic-dk-no (= 2.28) glibc-gconv-ebcdic-dk-no-a (= 2.28) glibc-gconv-ebcdic-es (= 2.28) glibc-gconv-ebcdic-es-a (= 2.28) glibc-gconv-ebcdic-es-s (= 2.28) glibc-gconv-ebcdic-fi-se (= 2.28) glibc-gconv-ebcdic-fi-se-a (= 2.28) glibc-gconv-ebcdic-fr (= 2.28) glibc-gconv-ebcdic-is-friss (= 2.28) glibc-gconv-ebcdic-it (= 2.28) glibc-gconv-ebcdic-pt (= 2.28) glibc-gconv-ebcdic-uk (= 2.28) glibc-gconv-ebcdic-us (= 2.28) glibc-gconv-ecma-cyrillic (= 2.28) glibc-gconv-euc-cn (= 2.28) glibc-gconv-euc-jisx0213 (= 2.28) glibc-gconv-euc-jp (= 2.28) glibc-gconv-euc-jp-ms (= 2.28) glibc-gconv-euc-kr (= 2.28) glibc-gconv-euc-tw (= 2.28) glibc-gconv-gb18030 (= 2.28) glibc-gconv-gbbig5 (= 2.28) glibc-gconv-gbgbk (= 2.28) glibc-gconv-gbk (= 2.28) glibc-gconv-georgian-academy (= 2.28) glibc-gconv-georgian-ps (= 2.28) glibc-gconv-gost_19768-74 (= 2.28) glibc-gconv-greek-ccitt (= 2.28) glibc-gconv-greek7 (= 2.28) glibc-gconv-greek7-old (= 2.28) glibc-gconv-hp-greek8 (= 2.28) glibc-gconv-hp-roman8 (= 2.28) glibc-gconv-hp-roman9 (= 2.28) glibc-gconv-hp-thai8 (= 2.28) glibc-gconv-hp-turkish8 (= 2.28) glibc-gconv-ibm037 (= 2.28) glibc-gconv-ibm038 (= 2.28) glibc-gconv-ibm1004 (= 2.28) glibc-gconv-ibm1008 (= 2.28) glibc-gconv-ibm1008_420 (= 2.28) glibc-gconv-ibm1025 (= 2.28) glibc-gconv-ibm1026 (= 2.28) glibc-gconv-ibm1046 (= 2.28) glibc-gconv-ibm1047 (= 2.28) glibc-gconv-ibm1097 (= 2.28) glibc-gconv-ibm1112 (= 2.28) glibc-gconv-ibm1122 (= 2.28) glibc-gconv-ibm1123 (= 2.28) glibc-gconv-ibm1124 (= 2.28) glibc-gconv-ibm1129 (= 2.28) glibc-gconv-ibm1130 (= 2.28) glibc-gconv-ibm1132 (= 2.28) glibc-gconv-ibm1133 (= 2.28) glibc-gconv-ibm1137 (= 2.28) glibc-gconv-ibm1140 (= 2.28) glibc-gconv-ibm1141 (= 2.28) glibc-gconv-ibm1142 (= 2.28) glibc-gconv-ibm1143 (= 2.28) glibc-gconv-ibm1144 (= 2.28) glibc-gconv-ibm1145 (= 2.28) glibc-gconv-ibm1146 (= 2.28) glibc-gconv-ibm1147 (= 2.28) glibc-gconv-ibm1148 (= 2.28) glibc-gconv-ibm1149 (= 2.28) glibc-gconv-ibm1153 (= 2.28) glibc-gconv-ibm1154 (= 2.28) glibc-gconv-ibm1155 (= 2.28) glibc-gconv-ibm1156 (= 2.28) glibc-gconv-ibm1157 (= 2.28) glibc-gconv-ibm1158 (= 2.28) glibc-gconv-ibm1160 (= 2.28) glibc-gconv-ibm1161 (= 2.28) glibc-gconv-ibm1162 (= 2.28) glibc-gconv-ibm1163 (= 2.28) glibc-gconv-ibm1164 (= 2.28) glibc-gconv-ibm1166 (= 2.28) glibc-gconv-ibm1167 (= 2.28) glibc-gconv-ibm12712 (= 2.28) glibc-gconv-ibm1364 (= 2.28) glibc-gconv-ibm1371 (= 2.28) glibc-gconv-ibm1388 (= 2.28) glibc-gconv-ibm1390 (= 2.28) glibc-gconv-ibm1399 (= 2.28) glibc-gconv-ibm16804 (= 2.28) glibc-gconv-ibm256 (= 2.28) glibc-gconv-ibm273 (= 2.28) glibc-gconv-ibm274 (= 2.28) glibc-gconv-ibm275 (= 2.28) glibc-gconv-ibm277 (= 2.28) glibc-gconv-ibm278 (= 2.28) glibc-gconv-ibm280 (= 2.28) glibc-gconv-ibm281 (= 2.28) glibc-gconv-ibm284 (= 2.28) glibc-gconv-ibm285 (= 2.28) glibc-gconv-ibm290 (= 2.28) glibc-gconv-ibm297 (= 2.28) glibc-gconv-ibm420 (= 2.28) glibc-gconv-ibm423 (= 2.28) glibc-gconv-ibm424 (= 2.28) glibc-gconv-ibm437 (= 2.28) glibc-gconv-ibm4517 (= 2.28) glibc-gconv-ibm4899 (= 2.28) glibc-gconv-ibm4909 (= 2.28) glibc-gconv-ibm4971 (= 2.28) glibc-gconv-ibm500 (= 2.28) glibc-gconv-ibm5347 (= 2.28) glibc-gconv-ibm803 (= 2.28) glibc-gconv-ibm850 (= 2.28) glibc-gconv-ibm851 (= 2.28) glibc-gconv-ibm852 (= 2.28) glibc-gconv-ibm855 (= 2.28) glibc-gconv-ibm856 (= 2.28) glibc-gconv-ibm857 (= 2.28) glibc-gconv-ibm858 (= 2.28) glibc-gconv-ibm860 (= 2.28) glibc-gconv-ibm861 (= 2.28) glibc-gconv-ibm862 (= 2.28) glibc-gconv-ibm863 (= 2.28) glibc-gconv-ibm864 (= 2.28) glibc-gconv-ibm865 (= 2.28) glibc-gconv-ibm866 (= 2.28) glibc-gconv-ibm866nav (= 2.28) glibc-gconv-ibm868 (= 2.28) glibc-gconv-ibm869 (= 2.28) glibc-gconv-ibm870 (= 2.28) glibc-gconv-ibm871 (= 2.28) glibc-gconv-ibm874 (= 2.28) glibc-gconv-ibm875 (= 2.28) glibc-gconv-ibm880 (= 2.28) glibc-gconv-ibm891 (= 2.28) glibc-gconv-ibm901 (= 2.28) glibc-gconv-ibm902 (= 2.28) glibc-gconv-ibm903 (= 2.28) glibc-gconv-ibm9030 (= 2.28) glibc-gconv-ibm904 (= 2.28) glibc-gconv-ibm905 (= 2.28) glibc-gconv-ibm9066 (= 2.28) glibc-gconv-ibm918 (= 2.28) glibc-gconv-ibm921 (= 2.28) glibc-gconv-ibm922 (= 2.28) glibc-gconv-ibm930 (= 2.28) glibc-gconv-ibm932 (= 2.28) glibc-gconv-ibm933 (= 2.28) glibc-gconv-ibm935 (= 2.28) glibc-gconv-ibm937 (= 2.28) glibc-gconv-ibm939 (= 2.28) glibc-gconv-ibm943 (= 2.28) glibc-gconv-ibm9448 (= 2.28) glibc-gconv-iec_p27-1 (= 2.28) glibc-gconv-inis (= 2.28) glibc-gconv-inis-8 (= 2.28) glibc-gconv-inis-cyrillic (= 2.28) glibc-gconv-isiri-3342 (= 2.28) glibc-gconv-iso-2022-cn (= 2.28) glibc-gconv-iso-2022-cn-ext (= 2.28) glibc-gconv-iso-2022-jp (= 2.28) glibc-gconv-iso-2022-jp-3 (= 2.28) glibc-gconv-iso-2022-kr (= 2.28) glibc-gconv-iso-ir-197 (= 2.28) glibc-gconv-iso-ir-209 (= 2.28) glibc-gconv-iso646 (= 2.28) glibc-gconv-iso8859-1 (= 2.28) glibc-gconv-iso8859-10 (= 2.28) glibc-gconv-iso8859-11 (= 2.28) glibc-gconv-iso8859-13 (= 2.28) glibc-gconv-iso8859-14 (= 2.28) glibc-gconv-iso8859-15 (= 2.28) glibc-gconv-iso8859-16 (= 2.28) glibc-gconv-iso8859-2 (= 2.28) glibc-gconv-iso8859-3 (= 2.28) glibc-gconv-iso8859-4 (= 2.28) glibc-gconv-iso8859-5 (= 2.28) glibc-gconv-iso8859-6 (= 2.28) glibc-gconv-iso8859-7 (= 2.28) glibc-gconv-iso8859-8 (= 2.28) glibc-gconv-iso8859-9 (= 2.28) glibc-gconv-iso8859-9e (= 2.28) glibc-gconv-iso_10367-box (= 2.28) glibc-gconv-iso_11548-1 (= 2.28) glibc-gconv-iso_2033 (= 2.28) glibc-gconv-iso_5427 (= 2.28) glibc-gconv-iso_5427-ext (= 2.28) glibc-gconv-iso_5428 (= 2.28) glibc-gconv-iso_6937 (= 2.28) glibc-gconv-iso_6937-2 (= 2.28) glibc-gconv-johab (= 2.28) glibc-gconv-koi-8 (= 2.28) glibc-gconv-koi8-r (= 2.28) glibc-gconv-koi8-ru (= 2.28) glibc-gconv-koi8-t (= 2.28) glibc-gconv-koi8-u (= 2.28) glibc-gconv-latin-greek (= 2.28) glibc-gconv-latin-greek-1 (= 2.28) glibc-gconv-mac-centraleurope (= 2.28) glibc-gconv-mac-is (= 2.28) glibc-gconv-mac-sami (= 2.28) glibc-gconv-mac-uk (= 2.28) glibc-gconv-macintosh (= 2.28) glibc-gconv-mik (= 2.28) glibc-gconv-nats-dano (= 2.28) glibc-gconv-nats-sefi (= 2.28) glibc-gconv-pt154 (= 2.28) glibc-gconv-rk1048 (= 2.28) glibc-gconv-sami-ws2 (= 2.28) glibc-gconv-shift_jisx0213 (= 2.28) glibc-gconv-sjis (= 2.28) glibc-gconv-t.61 (= 2.28) glibc-gconv-tcvn5712-1 (= 2.28) glibc-gconv-tis-620 (= 2.28) glibc-gconv-tscii (= 2.28) glibc-gconv-uhc (= 2.28) glibc-gconv-unicode (= 2.28) glibc-gconv-utf-16 (= 2.28) glibc-gconv-utf-32 (= 2.28) glibc-gconv-utf-7 (= 2.28) glibc-gconv-viscii (= 2.28) ldconfig (= 2.28)"
RDEPENDS_glibc = "basesystem glibc-common glibc-minimal-langpack"
RDEPENDS_glibc-all-langpacks = "glibc glibc-common"
RPM_SONAME_REQ_glibc-common = "libc.so.6 libdl.so.2 libselinux.so.1"
RDEPENDS_glibc-common = "bash glibc libselinux tzdata"
RPM_SONAME_REQ_glibc-devel = "libBrokenLocale.so.1 libanl.so.1 libdl.so.2 libmvec.so.1 libpthread.so.0 libresolv.so.2 librt.so.1 libthread_db.so.1 libutil.so.1"
RPROVIDES_glibc-devel = "glibc-dev (= 2.28)"
RDEPENDS_glibc-devel = "bash glibc glibc-headers info libgcc libxcrypt-devel"
RDEPENDS_glibc-headers = "bash glibc kernel-headers"
RPROVIDES_glibc-langpack-aa = "locale-base-aa-dj (= 2.28) locale-base-aa-dj.utf8 (= 2.28) locale-base-aa-er (= 2.28) locale-base-aa-er@saaho (= 2.28) locale-base-aa-et (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa-dj (= 2.28) virtual-locale-aa-dj.utf8 (= 2.28) virtual-locale-aa-er (= 2.28) virtual-locale-aa-er@saaho (= 2.28) virtual-locale-aa-et (= 2.28)"
RDEPENDS_glibc-langpack-aa = "glibc glibc-common"
RPROVIDES_glibc-langpack-af = "locale-base-af-za (= 2.28) locale-base-af-za.utf8 (= 2.28) virtual-locale-af (= 2.28) virtual-locale-af (= 2.28) virtual-locale-af-za (= 2.28) virtual-locale-af-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-af = "glibc glibc-common"
RPROVIDES_glibc-langpack-agr = "locale-base-agr-pe (= 2.28) virtual-locale-agr (= 2.28) virtual-locale-agr-pe (= 2.28)"
RDEPENDS_glibc-langpack-agr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ak = "locale-base-ak-gh (= 2.28) virtual-locale-ak (= 2.28) virtual-locale-ak-gh (= 2.28)"
RDEPENDS_glibc-langpack-ak = "glibc glibc-common"
RPROVIDES_glibc-langpack-am = "locale-base-am-et (= 2.28) virtual-locale-am (= 2.28) virtual-locale-am-et (= 2.28)"
RDEPENDS_glibc-langpack-am = "glibc glibc-common"
RPROVIDES_glibc-langpack-an = "locale-base-an-es (= 2.28) locale-base-an-es.utf8 (= 2.28) virtual-locale-an (= 2.28) virtual-locale-an (= 2.28) virtual-locale-an-es (= 2.28) virtual-locale-an-es.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-an = "glibc glibc-common"
RPROVIDES_glibc-langpack-anp = "locale-base-anp-in (= 2.28) virtual-locale-anp (= 2.28) virtual-locale-anp-in (= 2.28)"
RDEPENDS_glibc-langpack-anp = "glibc glibc-common"
RPROVIDES_glibc-langpack-ar = "locale-base-ar-ae (= 2.28) locale-base-ar-ae.utf8 (= 2.28) locale-base-ar-bh (= 2.28) locale-base-ar-bh.utf8 (= 2.28) locale-base-ar-dz (= 2.28) locale-base-ar-dz.utf8 (= 2.28) locale-base-ar-eg (= 2.28) locale-base-ar-eg.utf8 (= 2.28) locale-base-ar-in (= 2.28) locale-base-ar-iq (= 2.28) locale-base-ar-iq.utf8 (= 2.28) locale-base-ar-jo (= 2.28) locale-base-ar-jo.utf8 (= 2.28) locale-base-ar-kw (= 2.28) locale-base-ar-kw.utf8 (= 2.28) locale-base-ar-lb (= 2.28) locale-base-ar-lb.utf8 (= 2.28) locale-base-ar-ly (= 2.28) locale-base-ar-ly.utf8 (= 2.28) locale-base-ar-ma (= 2.28) locale-base-ar-ma.utf8 (= 2.28) locale-base-ar-om (= 2.28) locale-base-ar-om.utf8 (= 2.28) locale-base-ar-qa (= 2.28) locale-base-ar-qa.utf8 (= 2.28) locale-base-ar-sa (= 2.28) locale-base-ar-sa.utf8 (= 2.28) locale-base-ar-sd (= 2.28) locale-base-ar-sd.utf8 (= 2.28) locale-base-ar-ss (= 2.28) locale-base-ar-sy (= 2.28) locale-base-ar-sy.utf8 (= 2.28) locale-base-ar-tn (= 2.28) locale-base-ar-tn.utf8 (= 2.28) locale-base-ar-ye (= 2.28) locale-base-ar-ye.utf8 (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar (= 2.28) virtual-locale-ar-ae (= 2.28) virtual-locale-ar-ae.utf8 (= 2.28) virtual-locale-ar-bh (= 2.28) virtual-locale-ar-bh.utf8 (= 2.28) virtual-locale-ar-dz (= 2.28) virtual-locale-ar-dz.utf8 (= 2.28) virtual-locale-ar-eg (= 2.28) virtual-locale-ar-eg.utf8 (= 2.28) virtual-locale-ar-in (= 2.28) virtual-locale-ar-iq (= 2.28) virtual-locale-ar-iq.utf8 (= 2.28) virtual-locale-ar-jo (= 2.28) virtual-locale-ar-jo.utf8 (= 2.28) virtual-locale-ar-kw (= 2.28) virtual-locale-ar-kw.utf8 (= 2.28) virtual-locale-ar-lb (= 2.28) virtual-locale-ar-lb.utf8 (= 2.28) virtual-locale-ar-ly (= 2.28) virtual-locale-ar-ly.utf8 (= 2.28) virtual-locale-ar-ma (= 2.28) virtual-locale-ar-ma.utf8 (= 2.28) virtual-locale-ar-om (= 2.28) virtual-locale-ar-om.utf8 (= 2.28) virtual-locale-ar-qa (= 2.28) virtual-locale-ar-qa.utf8 (= 2.28) virtual-locale-ar-sa (= 2.28) virtual-locale-ar-sa.utf8 (= 2.28) virtual-locale-ar-sd (= 2.28) virtual-locale-ar-sd.utf8 (= 2.28) virtual-locale-ar-ss (= 2.28) virtual-locale-ar-sy (= 2.28) virtual-locale-ar-sy.utf8 (= 2.28) virtual-locale-ar-tn (= 2.28) virtual-locale-ar-tn.utf8 (= 2.28) virtual-locale-ar-ye (= 2.28) virtual-locale-ar-ye.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ar = "glibc glibc-common"
RPROVIDES_glibc-langpack-as = "locale-base-as-in (= 2.28) virtual-locale-as (= 2.28) virtual-locale-as-in (= 2.28)"
RDEPENDS_glibc-langpack-as = "glibc glibc-common"
RPROVIDES_glibc-langpack-ast = "locale-base-ast-es (= 2.28) locale-base-ast-es.utf8 (= 2.28) virtual-locale-ast (= 2.28) virtual-locale-ast (= 2.28) virtual-locale-ast-es (= 2.28) virtual-locale-ast-es.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ast = "glibc glibc-common"
RPROVIDES_glibc-langpack-ayc = "locale-base-ayc-pe (= 2.28) virtual-locale-ayc (= 2.28) virtual-locale-ayc-pe (= 2.28)"
RDEPENDS_glibc-langpack-ayc = "glibc glibc-common"
RPROVIDES_glibc-langpack-az = "locale-base-az-az (= 2.28) locale-base-az-ir (= 2.28) virtual-locale-az (= 2.28) virtual-locale-az (= 2.28) virtual-locale-az-az (= 2.28) virtual-locale-az-ir (= 2.28)"
RDEPENDS_glibc-langpack-az = "glibc glibc-common"
RPROVIDES_glibc-langpack-be = "locale-base-be-by (= 2.28) locale-base-be-by.utf8 (= 2.28) locale-base-be-by@latin (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be-by (= 2.28) virtual-locale-be-by.utf8 (= 2.28) virtual-locale-be-by@latin (= 2.28)"
RDEPENDS_glibc-langpack-be = "glibc glibc-common"
RPROVIDES_glibc-langpack-bem = "locale-base-bem-zm (= 2.28) virtual-locale-bem (= 2.28) virtual-locale-bem-zm (= 2.28)"
RDEPENDS_glibc-langpack-bem = "glibc glibc-common"
RPROVIDES_glibc-langpack-ber = "locale-base-ber-dz (= 2.28) locale-base-ber-ma (= 2.28) virtual-locale-ber (= 2.28) virtual-locale-ber (= 2.28) virtual-locale-ber-dz (= 2.28) virtual-locale-ber-ma (= 2.28)"
RDEPENDS_glibc-langpack-ber = "glibc glibc-common"
RPROVIDES_glibc-langpack-bg = "locale-base-bg-bg (= 2.28) locale-base-bg-bg.utf8 (= 2.28) virtual-locale-bg (= 2.28) virtual-locale-bg (= 2.28) virtual-locale-bg-bg (= 2.28) virtual-locale-bg-bg.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bg = "glibc glibc-common"
RPROVIDES_glibc-langpack-bhb = "locale-base-bhb-in.utf8 (= 2.28) virtual-locale-bhb (= 2.28) virtual-locale-bhb-in.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bhb = "glibc glibc-common"
RPROVIDES_glibc-langpack-bho = "locale-base-bho-in (= 2.28) locale-base-bho-np (= 2.28) virtual-locale-bho (= 2.28) virtual-locale-bho (= 2.28) virtual-locale-bho-in (= 2.28) virtual-locale-bho-np (= 2.28)"
RDEPENDS_glibc-langpack-bho = "glibc glibc-common"
RPROVIDES_glibc-langpack-bi = "locale-base-bi-vu (= 2.28) virtual-locale-bi (= 2.28) virtual-locale-bi-vu (= 2.28)"
RDEPENDS_glibc-langpack-bi = "glibc glibc-common"
RPROVIDES_glibc-langpack-bn = "locale-base-bn-bd (= 2.28) locale-base-bn-in (= 2.28) virtual-locale-bn (= 2.28) virtual-locale-bn (= 2.28) virtual-locale-bn-bd (= 2.28) virtual-locale-bn-in (= 2.28)"
RDEPENDS_glibc-langpack-bn = "glibc glibc-common"
RPROVIDES_glibc-langpack-bo = "locale-base-bo-cn (= 2.28) locale-base-bo-in (= 2.28) virtual-locale-bo (= 2.28) virtual-locale-bo (= 2.28) virtual-locale-bo-cn (= 2.28) virtual-locale-bo-in (= 2.28)"
RDEPENDS_glibc-langpack-bo = "glibc glibc-common"
RPROVIDES_glibc-langpack-br = "locale-base-br-fr (= 2.28) locale-base-br-fr.utf8 (= 2.28) locale-base-br-fr@euro (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br-fr (= 2.28) virtual-locale-br-fr.utf8 (= 2.28) virtual-locale-br-fr@euro (= 2.28)"
RDEPENDS_glibc-langpack-br = "glibc glibc-common"
RPROVIDES_glibc-langpack-brx = "locale-base-brx-in (= 2.28) virtual-locale-brx (= 2.28) virtual-locale-brx-in (= 2.28)"
RDEPENDS_glibc-langpack-brx = "glibc glibc-common"
RPROVIDES_glibc-langpack-bs = "locale-base-bs-ba (= 2.28) locale-base-bs-ba.utf8 (= 2.28) virtual-locale-bs (= 2.28) virtual-locale-bs (= 2.28) virtual-locale-bs-ba (= 2.28) virtual-locale-bs-ba.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-bs = "glibc glibc-common"
RPROVIDES_glibc-langpack-byn = "locale-base-byn-er (= 2.28) virtual-locale-byn (= 2.28) virtual-locale-byn-er (= 2.28)"
RDEPENDS_glibc-langpack-byn = "glibc glibc-common"
RPROVIDES_glibc-langpack-ca = "locale-base-ca-ad (= 2.28) locale-base-ca-ad.utf8 (= 2.28) locale-base-ca-es (= 2.28) locale-base-ca-es.utf8 (= 2.28) locale-base-ca-es@euro (= 2.28) locale-base-ca-es@valencia (= 2.28) locale-base-ca-fr (= 2.28) locale-base-ca-fr.utf8 (= 2.28) locale-base-ca-it (= 2.28) locale-base-ca-it.utf8 (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca-ad (= 2.28) virtual-locale-ca-ad.utf8 (= 2.28) virtual-locale-ca-es (= 2.28) virtual-locale-ca-es.utf8 (= 2.28) virtual-locale-ca-es@euro (= 2.28) virtual-locale-ca-es@valencia (= 2.28) virtual-locale-ca-fr (= 2.28) virtual-locale-ca-fr.utf8 (= 2.28) virtual-locale-ca-it (= 2.28) virtual-locale-ca-it.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ca = "glibc glibc-common"
RPROVIDES_glibc-langpack-ce = "locale-base-ce-ru (= 2.28) virtual-locale-ce (= 2.28) virtual-locale-ce-ru (= 2.28)"
RDEPENDS_glibc-langpack-ce = "glibc glibc-common"
RPROVIDES_glibc-langpack-chr = "locale-base-chr-us (= 2.28) virtual-locale-chr (= 2.28) virtual-locale-chr-us (= 2.28)"
RDEPENDS_glibc-langpack-chr = "glibc glibc-common"
RPROVIDES_glibc-langpack-cmn = "locale-base-cmn-tw (= 2.28) virtual-locale-cmn (= 2.28) virtual-locale-cmn-tw (= 2.28)"
RDEPENDS_glibc-langpack-cmn = "glibc glibc-common"
RPROVIDES_glibc-langpack-crh = "locale-base-crh-ua (= 2.28) virtual-locale-crh (= 2.28) virtual-locale-crh-ua (= 2.28)"
RDEPENDS_glibc-langpack-crh = "glibc glibc-common"
RPROVIDES_glibc-langpack-cs = "locale-base-cs-cz (= 2.28) locale-base-cs-cz.utf8 (= 2.28) virtual-locale-cs (= 2.28) virtual-locale-cs (= 2.28) virtual-locale-cs-cz (= 2.28) virtual-locale-cs-cz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-cs = "glibc glibc-common"
RPROVIDES_glibc-langpack-csb = "locale-base-csb-pl (= 2.28) virtual-locale-csb (= 2.28) virtual-locale-csb-pl (= 2.28)"
RDEPENDS_glibc-langpack-csb = "glibc glibc-common"
RPROVIDES_glibc-langpack-cv = "locale-base-cv-ru (= 2.28) virtual-locale-cv (= 2.28) virtual-locale-cv-ru (= 2.28)"
RDEPENDS_glibc-langpack-cv = "glibc glibc-common"
RPROVIDES_glibc-langpack-cy = "locale-base-cy-gb (= 2.28) locale-base-cy-gb.utf8 (= 2.28) virtual-locale-cy (= 2.28) virtual-locale-cy (= 2.28) virtual-locale-cy-gb (= 2.28) virtual-locale-cy-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-cy = "glibc glibc-common"
RPROVIDES_glibc-langpack-da = "locale-base-da-dk (= 2.28) locale-base-da-dk.iso885915 (= 2.28) locale-base-da-dk.utf8 (= 2.28) virtual-locale-da (= 2.28) virtual-locale-da (= 2.28) virtual-locale-da (= 2.28) virtual-locale-da-dk (= 2.28) virtual-locale-da-dk.iso885915 (= 2.28) virtual-locale-da-dk.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-da = "glibc glibc-common"
RPROVIDES_glibc-langpack-de = "locale-base-de-at (= 2.28) locale-base-de-at.utf8 (= 2.28) locale-base-de-at@euro (= 2.28) locale-base-de-be (= 2.28) locale-base-de-be.utf8 (= 2.28) locale-base-de-be@euro (= 2.28) locale-base-de-ch (= 2.28) locale-base-de-ch.utf8 (= 2.28) locale-base-de-de (= 2.28) locale-base-de-de.utf8 (= 2.28) locale-base-de-de@euro (= 2.28) locale-base-de-it (= 2.28) locale-base-de-it.utf8 (= 2.28) locale-base-de-li.utf8 (= 2.28) locale-base-de-lu (= 2.28) locale-base-de-lu.utf8 (= 2.28) locale-base-de-lu@euro (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de-at (= 2.28) virtual-locale-de-at.utf8 (= 2.28) virtual-locale-de-at@euro (= 2.28) virtual-locale-de-be (= 2.28) virtual-locale-de-be.utf8 (= 2.28) virtual-locale-de-be@euro (= 2.28) virtual-locale-de-ch (= 2.28) virtual-locale-de-ch.utf8 (= 2.28) virtual-locale-de-de (= 2.28) virtual-locale-de-de.utf8 (= 2.28) virtual-locale-de-de@euro (= 2.28) virtual-locale-de-it (= 2.28) virtual-locale-de-it.utf8 (= 2.28) virtual-locale-de-li.utf8 (= 2.28) virtual-locale-de-lu (= 2.28) virtual-locale-de-lu.utf8 (= 2.28) virtual-locale-de-lu@euro (= 2.28)"
RDEPENDS_glibc-langpack-de = "glibc glibc-common"
RPROVIDES_glibc-langpack-doi = "locale-base-doi-in (= 2.28) virtual-locale-doi (= 2.28) virtual-locale-doi-in (= 2.28)"
RDEPENDS_glibc-langpack-doi = "glibc glibc-common"
RPROVIDES_glibc-langpack-dsb = "locale-base-dsb-de (= 2.28) virtual-locale-dsb (= 2.28) virtual-locale-dsb-de (= 2.28)"
RDEPENDS_glibc-langpack-dsb = "glibc glibc-common"
RPROVIDES_glibc-langpack-dv = "locale-base-dv-mv (= 2.28) virtual-locale-dv (= 2.28) virtual-locale-dv-mv (= 2.28)"
RDEPENDS_glibc-langpack-dv = "glibc glibc-common"
RPROVIDES_glibc-langpack-dz = "locale-base-dz-bt (= 2.28) virtual-locale-dz (= 2.28) virtual-locale-dz-bt (= 2.28)"
RDEPENDS_glibc-langpack-dz = "glibc glibc-common"
RPROVIDES_glibc-langpack-el = "locale-base-el-cy (= 2.28) locale-base-el-cy.utf8 (= 2.28) locale-base-el-gr (= 2.28) locale-base-el-gr.utf8 (= 2.28) locale-base-el-gr@euro (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el-cy (= 2.28) virtual-locale-el-cy.utf8 (= 2.28) virtual-locale-el-gr (= 2.28) virtual-locale-el-gr.utf8 (= 2.28) virtual-locale-el-gr@euro (= 2.28)"
RDEPENDS_glibc-langpack-el = "glibc glibc-common"
RPROVIDES_glibc-langpack-en = "locale-base-en-ag (= 2.28) locale-base-en-au (= 2.28) locale-base-en-au.utf8 (= 2.28) locale-base-en-bw (= 2.28) locale-base-en-bw.utf8 (= 2.28) locale-base-en-ca (= 2.28) locale-base-en-ca.utf8 (= 2.28) locale-base-en-dk (= 2.28) locale-base-en-dk.utf8 (= 2.28) locale-base-en-gb (= 2.28) locale-base-en-gb.iso885915 (= 2.28) locale-base-en-gb.utf8 (= 2.28) locale-base-en-hk (= 2.28) locale-base-en-hk.utf8 (= 2.28) locale-base-en-ie (= 2.28) locale-base-en-ie.utf8 (= 2.28) locale-base-en-ie@euro (= 2.28) locale-base-en-il (= 2.28) locale-base-en-in (= 2.28) locale-base-en-ng (= 2.28) locale-base-en-nz (= 2.28) locale-base-en-nz.utf8 (= 2.28) locale-base-en-ph (= 2.28) locale-base-en-ph.utf8 (= 2.28) locale-base-en-sc.utf8 (= 2.28) locale-base-en-sg (= 2.28) locale-base-en-sg.utf8 (= 2.28) locale-base-en-us (= 2.28) locale-base-en-us.iso885915 (= 2.28) locale-base-en-us.utf8 (= 2.28) locale-base-en-za (= 2.28) locale-base-en-za.utf8 (= 2.28) locale-base-en-zm (= 2.28) locale-base-en-zw (= 2.28) locale-base-en-zw.utf8 (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en (= 2.28) virtual-locale-en-ag (= 2.28) virtual-locale-en-au (= 2.28) virtual-locale-en-au.utf8 (= 2.28) virtual-locale-en-bw (= 2.28) virtual-locale-en-bw.utf8 (= 2.28) virtual-locale-en-ca (= 2.28) virtual-locale-en-ca.utf8 (= 2.28) virtual-locale-en-dk (= 2.28) virtual-locale-en-dk.utf8 (= 2.28) virtual-locale-en-gb (= 2.28) virtual-locale-en-gb.iso885915 (= 2.28) virtual-locale-en-gb.utf8 (= 2.28) virtual-locale-en-hk (= 2.28) virtual-locale-en-hk.utf8 (= 2.28) virtual-locale-en-ie (= 2.28) virtual-locale-en-ie.utf8 (= 2.28) virtual-locale-en-ie@euro (= 2.28) virtual-locale-en-il (= 2.28) virtual-locale-en-in (= 2.28) virtual-locale-en-ng (= 2.28) virtual-locale-en-nz (= 2.28) virtual-locale-en-nz.utf8 (= 2.28) virtual-locale-en-ph (= 2.28) virtual-locale-en-ph.utf8 (= 2.28) virtual-locale-en-sc.utf8 (= 2.28) virtual-locale-en-sg (= 2.28) virtual-locale-en-sg.utf8 (= 2.28) virtual-locale-en-us (= 2.28) virtual-locale-en-us.iso885915 (= 2.28) virtual-locale-en-us.utf8 (= 2.28) virtual-locale-en-za (= 2.28) virtual-locale-en-za.utf8 (= 2.28) virtual-locale-en-zm (= 2.28) virtual-locale-en-zw (= 2.28) virtual-locale-en-zw.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-en = "glibc glibc-common"
RDEPENDS_glibc-langpack-eo = "glibc glibc-common"
RPROVIDES_glibc-langpack-es = "locale-base-es-ar (= 2.28) locale-base-es-ar.utf8 (= 2.28) locale-base-es-bo (= 2.28) locale-base-es-bo.utf8 (= 2.28) locale-base-es-cl (= 2.28) locale-base-es-cl.utf8 (= 2.28) locale-base-es-co (= 2.28) locale-base-es-co.utf8 (= 2.28) locale-base-es-cr (= 2.28) locale-base-es-cr.utf8 (= 2.28) locale-base-es-cu (= 2.28) locale-base-es-do (= 2.28) locale-base-es-do.utf8 (= 2.28) locale-base-es-ec (= 2.28) locale-base-es-ec.utf8 (= 2.28) locale-base-es-es (= 2.28) locale-base-es-es.utf8 (= 2.28) locale-base-es-es@euro (= 2.28) locale-base-es-gt (= 2.28) locale-base-es-gt.utf8 (= 2.28) locale-base-es-hn (= 2.28) locale-base-es-hn.utf8 (= 2.28) locale-base-es-mx (= 2.28) locale-base-es-mx.utf8 (= 2.28) locale-base-es-ni (= 2.28) locale-base-es-ni.utf8 (= 2.28) locale-base-es-pa (= 2.28) locale-base-es-pa.utf8 (= 2.28) locale-base-es-pe (= 2.28) locale-base-es-pe.utf8 (= 2.28) locale-base-es-pr (= 2.28) locale-base-es-pr.utf8 (= 2.28) locale-base-es-py (= 2.28) locale-base-es-py.utf8 (= 2.28) locale-base-es-sv (= 2.28) locale-base-es-sv.utf8 (= 2.28) locale-base-es-us (= 2.28) locale-base-es-us.utf8 (= 2.28) locale-base-es-uy (= 2.28) locale-base-es-uy.utf8 (= 2.28) locale-base-es-ve (= 2.28) locale-base-es-ve.utf8 (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es (= 2.28) virtual-locale-es-ar (= 2.28) virtual-locale-es-ar.utf8 (= 2.28) virtual-locale-es-bo (= 2.28) virtual-locale-es-bo.utf8 (= 2.28) virtual-locale-es-cl (= 2.28) virtual-locale-es-cl.utf8 (= 2.28) virtual-locale-es-co (= 2.28) virtual-locale-es-co.utf8 (= 2.28) virtual-locale-es-cr (= 2.28) virtual-locale-es-cr.utf8 (= 2.28) virtual-locale-es-cu (= 2.28) virtual-locale-es-do (= 2.28) virtual-locale-es-do.utf8 (= 2.28) virtual-locale-es-ec (= 2.28) virtual-locale-es-ec.utf8 (= 2.28) virtual-locale-es-es (= 2.28) virtual-locale-es-es.utf8 (= 2.28) virtual-locale-es-es@euro (= 2.28) virtual-locale-es-gt (= 2.28) virtual-locale-es-gt.utf8 (= 2.28) virtual-locale-es-hn (= 2.28) virtual-locale-es-hn.utf8 (= 2.28) virtual-locale-es-mx (= 2.28) virtual-locale-es-mx.utf8 (= 2.28) virtual-locale-es-ni (= 2.28) virtual-locale-es-ni.utf8 (= 2.28) virtual-locale-es-pa (= 2.28) virtual-locale-es-pa.utf8 (= 2.28) virtual-locale-es-pe (= 2.28) virtual-locale-es-pe.utf8 (= 2.28) virtual-locale-es-pr (= 2.28) virtual-locale-es-pr.utf8 (= 2.28) virtual-locale-es-py (= 2.28) virtual-locale-es-py.utf8 (= 2.28) virtual-locale-es-sv (= 2.28) virtual-locale-es-sv.utf8 (= 2.28) virtual-locale-es-us (= 2.28) virtual-locale-es-us.utf8 (= 2.28) virtual-locale-es-uy (= 2.28) virtual-locale-es-uy.utf8 (= 2.28) virtual-locale-es-ve (= 2.28) virtual-locale-es-ve.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-es = "glibc glibc-common"
RPROVIDES_glibc-langpack-et = "locale-base-et-ee (= 2.28) locale-base-et-ee.iso885915 (= 2.28) locale-base-et-ee.utf8 (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et (= 2.28) virtual-locale-et-ee (= 2.28) virtual-locale-et-ee.iso885915 (= 2.28) virtual-locale-et-ee.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-et = "glibc glibc-common"
RPROVIDES_glibc-langpack-eu = "locale-base-eu-es (= 2.28) locale-base-eu-es.utf8 (= 2.28) locale-base-eu-es@euro (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu-es (= 2.28) virtual-locale-eu-es.utf8 (= 2.28) virtual-locale-eu-es@euro (= 2.28)"
RDEPENDS_glibc-langpack-eu = "glibc glibc-common"
RPROVIDES_glibc-langpack-fa = "locale-base-fa-ir (= 2.28) virtual-locale-fa (= 2.28) virtual-locale-fa-ir (= 2.28)"
RDEPENDS_glibc-langpack-fa = "glibc glibc-common"
RPROVIDES_glibc-langpack-ff = "locale-base-ff-sn (= 2.28) virtual-locale-ff (= 2.28) virtual-locale-ff-sn (= 2.28)"
RDEPENDS_glibc-langpack-ff = "glibc glibc-common"
RPROVIDES_glibc-langpack-fi = "locale-base-fi-fi (= 2.28) locale-base-fi-fi.utf8 (= 2.28) locale-base-fi-fi@euro (= 2.28) virtual-locale-fi (= 2.28) virtual-locale-fi (= 2.28) virtual-locale-fi (= 2.28) virtual-locale-fi-fi (= 2.28) virtual-locale-fi-fi.utf8 (= 2.28) virtual-locale-fi-fi@euro (= 2.28)"
RDEPENDS_glibc-langpack-fi = "glibc glibc-common"
RPROVIDES_glibc-langpack-fil = "locale-base-fil-ph (= 2.28) virtual-locale-fil (= 2.28) virtual-locale-fil-ph (= 2.28)"
RDEPENDS_glibc-langpack-fil = "glibc glibc-common"
RPROVIDES_glibc-langpack-fo = "locale-base-fo-fo (= 2.28) locale-base-fo-fo.utf8 (= 2.28) virtual-locale-fo (= 2.28) virtual-locale-fo (= 2.28) virtual-locale-fo-fo (= 2.28) virtual-locale-fo-fo.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-fo = "glibc glibc-common"
RPROVIDES_glibc-langpack-fr = "locale-base-fr-be (= 2.28) locale-base-fr-be.utf8 (= 2.28) locale-base-fr-be@euro (= 2.28) locale-base-fr-ca (= 2.28) locale-base-fr-ca.utf8 (= 2.28) locale-base-fr-ch (= 2.28) locale-base-fr-ch.utf8 (= 2.28) locale-base-fr-fr (= 2.28) locale-base-fr-fr.utf8 (= 2.28) locale-base-fr-fr@euro (= 2.28) locale-base-fr-lu (= 2.28) locale-base-fr-lu.utf8 (= 2.28) locale-base-fr-lu@euro (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr (= 2.28) virtual-locale-fr-be (= 2.28) virtual-locale-fr-be.utf8 (= 2.28) virtual-locale-fr-be@euro (= 2.28) virtual-locale-fr-ca (= 2.28) virtual-locale-fr-ca.utf8 (= 2.28) virtual-locale-fr-ch (= 2.28) virtual-locale-fr-ch.utf8 (= 2.28) virtual-locale-fr-fr (= 2.28) virtual-locale-fr-fr.utf8 (= 2.28) virtual-locale-fr-fr@euro (= 2.28) virtual-locale-fr-lu (= 2.28) virtual-locale-fr-lu.utf8 (= 2.28) virtual-locale-fr-lu@euro (= 2.28)"
RDEPENDS_glibc-langpack-fr = "glibc glibc-common"
RPROVIDES_glibc-langpack-fur = "locale-base-fur-it (= 2.28) virtual-locale-fur (= 2.28) virtual-locale-fur-it (= 2.28)"
RDEPENDS_glibc-langpack-fur = "glibc glibc-common"
RPROVIDES_glibc-langpack-fy = "locale-base-fy-de (= 2.28) locale-base-fy-nl (= 2.28) virtual-locale-fy (= 2.28) virtual-locale-fy (= 2.28) virtual-locale-fy-de (= 2.28) virtual-locale-fy-nl (= 2.28)"
RDEPENDS_glibc-langpack-fy = "glibc glibc-common"
RPROVIDES_glibc-langpack-ga = "locale-base-ga-ie (= 2.28) locale-base-ga-ie.utf8 (= 2.28) locale-base-ga-ie@euro (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga-ie (= 2.28) virtual-locale-ga-ie.utf8 (= 2.28) virtual-locale-ga-ie@euro (= 2.28)"
RDEPENDS_glibc-langpack-ga = "glibc glibc-common"
RPROVIDES_glibc-langpack-gd = "locale-base-gd-gb (= 2.28) locale-base-gd-gb.utf8 (= 2.28) virtual-locale-gd (= 2.28) virtual-locale-gd (= 2.28) virtual-locale-gd-gb (= 2.28) virtual-locale-gd-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-gd = "glibc glibc-common"
RPROVIDES_glibc-langpack-gez = "locale-base-gez-er (= 2.28) locale-base-gez-er@abegede (= 2.28) locale-base-gez-et (= 2.28) locale-base-gez-et@abegede (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez-er (= 2.28) virtual-locale-gez-er@abegede (= 2.28) virtual-locale-gez-et (= 2.28) virtual-locale-gez-et@abegede (= 2.28)"
RDEPENDS_glibc-langpack-gez = "glibc glibc-common"
RPROVIDES_glibc-langpack-gl = "locale-base-gl-es (= 2.28) locale-base-gl-es.utf8 (= 2.28) locale-base-gl-es@euro (= 2.28) virtual-locale-gl (= 2.28) virtual-locale-gl (= 2.28) virtual-locale-gl (= 2.28) virtual-locale-gl-es (= 2.28) virtual-locale-gl-es.utf8 (= 2.28) virtual-locale-gl-es@euro (= 2.28)"
RDEPENDS_glibc-langpack-gl = "glibc glibc-common"
RPROVIDES_glibc-langpack-gu = "locale-base-gu-in (= 2.28) virtual-locale-gu (= 2.28) virtual-locale-gu-in (= 2.28)"
RDEPENDS_glibc-langpack-gu = "glibc glibc-common"
RPROVIDES_glibc-langpack-gv = "locale-base-gv-gb (= 2.28) locale-base-gv-gb.utf8 (= 2.28) virtual-locale-gv (= 2.28) virtual-locale-gv (= 2.28) virtual-locale-gv-gb (= 2.28) virtual-locale-gv-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-gv = "glibc glibc-common"
RPROVIDES_glibc-langpack-ha = "locale-base-ha-ng (= 2.28) virtual-locale-ha (= 2.28) virtual-locale-ha-ng (= 2.28)"
RDEPENDS_glibc-langpack-ha = "glibc glibc-common"
RPROVIDES_glibc-langpack-hak = "locale-base-hak-tw (= 2.28) virtual-locale-hak (= 2.28) virtual-locale-hak-tw (= 2.28)"
RDEPENDS_glibc-langpack-hak = "glibc glibc-common"
RPROVIDES_glibc-langpack-he = "locale-base-he-il (= 2.28) locale-base-he-il.utf8 (= 2.28) virtual-locale-he (= 2.28) virtual-locale-he (= 2.28) virtual-locale-he-il (= 2.28) virtual-locale-he-il.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-he = "glibc glibc-common"
RPROVIDES_glibc-langpack-hi = "locale-base-hi-in (= 2.28) virtual-locale-hi (= 2.28) virtual-locale-hi-in (= 2.28)"
RDEPENDS_glibc-langpack-hi = "glibc glibc-common"
RPROVIDES_glibc-langpack-hif = "locale-base-hif-fj (= 2.28) virtual-locale-hif (= 2.28) virtual-locale-hif-fj (= 2.28)"
RDEPENDS_glibc-langpack-hif = "glibc glibc-common"
RPROVIDES_glibc-langpack-hne = "locale-base-hne-in (= 2.28) virtual-locale-hne (= 2.28) virtual-locale-hne-in (= 2.28)"
RDEPENDS_glibc-langpack-hne = "glibc glibc-common"
RPROVIDES_glibc-langpack-hr = "locale-base-hr-hr (= 2.28) locale-base-hr-hr.utf8 (= 2.28) virtual-locale-hr (= 2.28) virtual-locale-hr (= 2.28) virtual-locale-hr-hr (= 2.28) virtual-locale-hr-hr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-hr = "glibc glibc-common"
RPROVIDES_glibc-langpack-hsb = "locale-base-hsb-de (= 2.28) locale-base-hsb-de.utf8 (= 2.28) virtual-locale-hsb (= 2.28) virtual-locale-hsb (= 2.28) virtual-locale-hsb-de (= 2.28) virtual-locale-hsb-de.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-hsb = "glibc glibc-common"
RPROVIDES_glibc-langpack-ht = "locale-base-ht-ht (= 2.28) virtual-locale-ht (= 2.28) virtual-locale-ht-ht (= 2.28)"
RDEPENDS_glibc-langpack-ht = "glibc glibc-common"
RPROVIDES_glibc-langpack-hu = "locale-base-hu-hu (= 2.28) locale-base-hu-hu.utf8 (= 2.28) virtual-locale-hu (= 2.28) virtual-locale-hu (= 2.28) virtual-locale-hu-hu (= 2.28) virtual-locale-hu-hu.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-hu = "glibc glibc-common"
RPROVIDES_glibc-langpack-hy = "locale-base-hy-am (= 2.28) locale-base-hy-am.armscii8 (= 2.28) virtual-locale-hy (= 2.28) virtual-locale-hy (= 2.28) virtual-locale-hy-am (= 2.28) virtual-locale-hy-am.armscii8 (= 2.28)"
RDEPENDS_glibc-langpack-hy = "glibc glibc-common"
RPROVIDES_glibc-langpack-ia = "locale-base-ia-fr (= 2.28) virtual-locale-ia (= 2.28) virtual-locale-ia-fr (= 2.28)"
RDEPENDS_glibc-langpack-ia = "glibc glibc-common"
RPROVIDES_glibc-langpack-id = "locale-base-id-id (= 2.28) locale-base-id-id.utf8 (= 2.28) virtual-locale-id (= 2.28) virtual-locale-id (= 2.28) virtual-locale-id-id (= 2.28) virtual-locale-id-id.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-id = "glibc glibc-common"
RPROVIDES_glibc-langpack-ig = "locale-base-ig-ng (= 2.28) virtual-locale-ig (= 2.28) virtual-locale-ig-ng (= 2.28)"
RDEPENDS_glibc-langpack-ig = "glibc glibc-common"
RPROVIDES_glibc-langpack-ik = "locale-base-ik-ca (= 2.28) virtual-locale-ik (= 2.28) virtual-locale-ik-ca (= 2.28)"
RDEPENDS_glibc-langpack-ik = "glibc glibc-common"
RPROVIDES_glibc-langpack-is = "locale-base-is-is (= 2.28) locale-base-is-is.utf8 (= 2.28) virtual-locale-is (= 2.28) virtual-locale-is (= 2.28) virtual-locale-is-is (= 2.28) virtual-locale-is-is.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-is = "glibc glibc-common"
RPROVIDES_glibc-langpack-it = "locale-base-it-ch (= 2.28) locale-base-it-ch.utf8 (= 2.28) locale-base-it-it (= 2.28) locale-base-it-it.utf8 (= 2.28) locale-base-it-it@euro (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it-ch (= 2.28) virtual-locale-it-ch.utf8 (= 2.28) virtual-locale-it-it (= 2.28) virtual-locale-it-it.utf8 (= 2.28) virtual-locale-it-it@euro (= 2.28)"
RDEPENDS_glibc-langpack-it = "glibc glibc-common"
RPROVIDES_glibc-langpack-iu = "locale-base-iu-ca (= 2.28) virtual-locale-iu (= 2.28) virtual-locale-iu-ca (= 2.28)"
RDEPENDS_glibc-langpack-iu = "glibc glibc-common"
RPROVIDES_glibc-langpack-ja = "locale-base-ja-jp.eucjp (= 2.28) locale-base-ja-jp.utf8 (= 2.28) virtual-locale-ja (= 2.28) virtual-locale-ja (= 2.28) virtual-locale-ja-jp.eucjp (= 2.28) virtual-locale-ja-jp.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ja = "glibc glibc-common"
RPROVIDES_glibc-langpack-ka = "locale-base-ka-ge (= 2.28) locale-base-ka-ge.utf8 (= 2.28) virtual-locale-ka (= 2.28) virtual-locale-ka (= 2.28) virtual-locale-ka-ge (= 2.28) virtual-locale-ka-ge.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ka = "glibc glibc-common"
RPROVIDES_glibc-langpack-kab = "locale-base-kab-dz (= 2.28) virtual-locale-kab (= 2.28) virtual-locale-kab-dz (= 2.28)"
RDEPENDS_glibc-langpack-kab = "glibc glibc-common"
RPROVIDES_glibc-langpack-kk = "locale-base-kk-kz (= 2.28) locale-base-kk-kz.utf8 (= 2.28) virtual-locale-kk (= 2.28) virtual-locale-kk (= 2.28) virtual-locale-kk-kz (= 2.28) virtual-locale-kk-kz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kk = "glibc glibc-common"
RPROVIDES_glibc-langpack-kl = "locale-base-kl-gl (= 2.28) locale-base-kl-gl.utf8 (= 2.28) virtual-locale-kl (= 2.28) virtual-locale-kl (= 2.28) virtual-locale-kl-gl (= 2.28) virtual-locale-kl-gl.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kl = "glibc glibc-common"
RPROVIDES_glibc-langpack-km = "locale-base-km-kh (= 2.28) virtual-locale-km (= 2.28) virtual-locale-km-kh (= 2.28)"
RDEPENDS_glibc-langpack-km = "glibc glibc-common"
RPROVIDES_glibc-langpack-kn = "locale-base-kn-in (= 2.28) virtual-locale-kn (= 2.28) virtual-locale-kn-in (= 2.28)"
RDEPENDS_glibc-langpack-kn = "glibc glibc-common"
RPROVIDES_glibc-langpack-ko = "locale-base-ko-kr.euckr (= 2.28) locale-base-ko-kr.utf8 (= 2.28) virtual-locale-ko (= 2.28) virtual-locale-ko (= 2.28) virtual-locale-ko-kr.euckr (= 2.28) virtual-locale-ko-kr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ko = "glibc glibc-common"
RPROVIDES_glibc-langpack-kok = "locale-base-kok-in (= 2.28) virtual-locale-kok (= 2.28) virtual-locale-kok-in (= 2.28)"
RDEPENDS_glibc-langpack-kok = "glibc glibc-common"
RPROVIDES_glibc-langpack-ks = "locale-base-ks-in (= 2.28) locale-base-ks-in@devanagari (= 2.28) virtual-locale-ks (= 2.28) virtual-locale-ks (= 2.28) virtual-locale-ks-in (= 2.28) virtual-locale-ks-in@devanagari (= 2.28)"
RDEPENDS_glibc-langpack-ks = "glibc glibc-common"
RPROVIDES_glibc-langpack-ku = "locale-base-ku-tr (= 2.28) locale-base-ku-tr.utf8 (= 2.28) virtual-locale-ku (= 2.28) virtual-locale-ku (= 2.28) virtual-locale-ku-tr (= 2.28) virtual-locale-ku-tr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ku = "glibc glibc-common"
RPROVIDES_glibc-langpack-kw = "locale-base-kw-gb (= 2.28) locale-base-kw-gb.utf8 (= 2.28) virtual-locale-kw (= 2.28) virtual-locale-kw (= 2.28) virtual-locale-kw-gb (= 2.28) virtual-locale-kw-gb.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kw = "glibc glibc-common"
RPROVIDES_glibc-langpack-ky = "locale-base-ky-kg (= 2.28) virtual-locale-ky (= 2.28) virtual-locale-ky-kg (= 2.28)"
RDEPENDS_glibc-langpack-ky = "glibc glibc-common"
RPROVIDES_glibc-langpack-lb = "locale-base-lb-lu (= 2.28) virtual-locale-lb (= 2.28) virtual-locale-lb-lu (= 2.28)"
RDEPENDS_glibc-langpack-lb = "glibc glibc-common"
RPROVIDES_glibc-langpack-lg = "locale-base-lg-ug (= 2.28) locale-base-lg-ug.utf8 (= 2.28) virtual-locale-lg (= 2.28) virtual-locale-lg (= 2.28) virtual-locale-lg-ug (= 2.28) virtual-locale-lg-ug.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lg = "glibc glibc-common"
RPROVIDES_glibc-langpack-li = "locale-base-li-be (= 2.28) locale-base-li-nl (= 2.28) virtual-locale-li (= 2.28) virtual-locale-li (= 2.28) virtual-locale-li-be (= 2.28) virtual-locale-li-nl (= 2.28)"
RDEPENDS_glibc-langpack-li = "glibc glibc-common"
RPROVIDES_glibc-langpack-lij = "locale-base-lij-it (= 2.28) virtual-locale-lij (= 2.28) virtual-locale-lij-it (= 2.28)"
RDEPENDS_glibc-langpack-lij = "glibc glibc-common"
RPROVIDES_glibc-langpack-ln = "locale-base-ln-cd (= 2.28) virtual-locale-ln (= 2.28) virtual-locale-ln-cd (= 2.28)"
RDEPENDS_glibc-langpack-ln = "glibc glibc-common"
RPROVIDES_glibc-langpack-lo = "locale-base-lo-la (= 2.28) virtual-locale-lo (= 2.28) virtual-locale-lo-la (= 2.28)"
RDEPENDS_glibc-langpack-lo = "glibc glibc-common"
RPROVIDES_glibc-langpack-lt = "locale-base-lt-lt (= 2.28) locale-base-lt-lt.utf8 (= 2.28) virtual-locale-lt (= 2.28) virtual-locale-lt (= 2.28) virtual-locale-lt-lt (= 2.28) virtual-locale-lt-lt.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lt = "glibc glibc-common"
RPROVIDES_glibc-langpack-lv = "locale-base-lv-lv (= 2.28) locale-base-lv-lv.utf8 (= 2.28) virtual-locale-lv (= 2.28) virtual-locale-lv (= 2.28) virtual-locale-lv-lv (= 2.28) virtual-locale-lv-lv.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lv = "glibc glibc-common"
RPROVIDES_glibc-langpack-lzh = "locale-base-lzh-tw (= 2.28) virtual-locale-lzh (= 2.28) virtual-locale-lzh-tw (= 2.28)"
RDEPENDS_glibc-langpack-lzh = "glibc glibc-common"
RPROVIDES_glibc-langpack-mag = "locale-base-mag-in (= 2.28) virtual-locale-mag (= 2.28) virtual-locale-mag-in (= 2.28)"
RDEPENDS_glibc-langpack-mag = "glibc glibc-common"
RPROVIDES_glibc-langpack-mai = "locale-base-mai-in (= 2.28) locale-base-mai-np (= 2.28) virtual-locale-mai (= 2.28) virtual-locale-mai (= 2.28) virtual-locale-mai-in (= 2.28) virtual-locale-mai-np (= 2.28)"
RDEPENDS_glibc-langpack-mai = "glibc glibc-common"
RPROVIDES_glibc-langpack-mfe = "locale-base-mfe-mu (= 2.28) virtual-locale-mfe (= 2.28) virtual-locale-mfe-mu (= 2.28)"
RDEPENDS_glibc-langpack-mfe = "glibc glibc-common"
RPROVIDES_glibc-langpack-mg = "locale-base-mg-mg (= 2.28) locale-base-mg-mg.utf8 (= 2.28) virtual-locale-mg (= 2.28) virtual-locale-mg (= 2.28) virtual-locale-mg-mg (= 2.28) virtual-locale-mg-mg.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mg = "glibc glibc-common"
RPROVIDES_glibc-langpack-mhr = "locale-base-mhr-ru (= 2.28) virtual-locale-mhr (= 2.28) virtual-locale-mhr-ru (= 2.28)"
RDEPENDS_glibc-langpack-mhr = "glibc glibc-common"
RPROVIDES_glibc-langpack-mi = "locale-base-mi-nz (= 2.28) locale-base-mi-nz.utf8 (= 2.28) virtual-locale-mi (= 2.28) virtual-locale-mi (= 2.28) virtual-locale-mi-nz (= 2.28) virtual-locale-mi-nz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mi = "glibc glibc-common"
RPROVIDES_glibc-langpack-miq = "locale-base-miq-ni (= 2.28) virtual-locale-miq (= 2.28) virtual-locale-miq-ni (= 2.28)"
RDEPENDS_glibc-langpack-miq = "glibc glibc-common"
RPROVIDES_glibc-langpack-mjw = "locale-base-mjw-in (= 2.28) virtual-locale-mjw (= 2.28) virtual-locale-mjw-in (= 2.28)"
RDEPENDS_glibc-langpack-mjw = "glibc glibc-common"
RPROVIDES_glibc-langpack-mk = "locale-base-mk-mk (= 2.28) locale-base-mk-mk.utf8 (= 2.28) virtual-locale-mk (= 2.28) virtual-locale-mk (= 2.28) virtual-locale-mk-mk (= 2.28) virtual-locale-mk-mk.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mk = "glibc glibc-common"
RPROVIDES_glibc-langpack-ml = "locale-base-ml-in (= 2.28) virtual-locale-ml (= 2.28) virtual-locale-ml-in (= 2.28)"
RDEPENDS_glibc-langpack-ml = "glibc glibc-common"
RPROVIDES_glibc-langpack-mn = "locale-base-mn-mn (= 2.28) virtual-locale-mn (= 2.28) virtual-locale-mn-mn (= 2.28)"
RDEPENDS_glibc-langpack-mn = "glibc glibc-common"
RPROVIDES_glibc-langpack-mni = "locale-base-mni-in (= 2.28) virtual-locale-mni (= 2.28) virtual-locale-mni-in (= 2.28)"
RDEPENDS_glibc-langpack-mni = "glibc glibc-common"
RPROVIDES_glibc-langpack-mr = "locale-base-mr-in (= 2.28) virtual-locale-mr (= 2.28) virtual-locale-mr-in (= 2.28)"
RDEPENDS_glibc-langpack-mr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ms = "locale-base-ms-my (= 2.28) locale-base-ms-my.utf8 (= 2.28) virtual-locale-ms (= 2.28) virtual-locale-ms (= 2.28) virtual-locale-ms-my (= 2.28) virtual-locale-ms-my.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ms = "glibc glibc-common"
RPROVIDES_glibc-langpack-mt = "locale-base-mt-mt (= 2.28) locale-base-mt-mt.utf8 (= 2.28) virtual-locale-mt (= 2.28) virtual-locale-mt (= 2.28) virtual-locale-mt-mt (= 2.28) virtual-locale-mt-mt.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-mt = "glibc glibc-common"
RPROVIDES_glibc-langpack-my = "locale-base-my-mm (= 2.28) virtual-locale-my (= 2.28) virtual-locale-my-mm (= 2.28)"
RDEPENDS_glibc-langpack-my = "glibc glibc-common"
RPROVIDES_glibc-langpack-nan = "locale-base-nan-tw (= 2.28) locale-base-nan-tw@latin (= 2.28) virtual-locale-nan (= 2.28) virtual-locale-nan (= 2.28) virtual-locale-nan-tw (= 2.28) virtual-locale-nan-tw@latin (= 2.28)"
RDEPENDS_glibc-langpack-nan = "glibc glibc-common"
RPROVIDES_glibc-langpack-nb = "locale-base-nb-no (= 2.28) locale-base-nb-no.utf8 (= 2.28) virtual-locale-nb (= 2.28) virtual-locale-nb (= 2.28) virtual-locale-nb-no (= 2.28) virtual-locale-nb-no.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-nb = "glibc glibc-common"
RPROVIDES_glibc-langpack-nds = "locale-base-nds-de (= 2.28) locale-base-nds-nl (= 2.28) virtual-locale-nds (= 2.28) virtual-locale-nds (= 2.28) virtual-locale-nds-de (= 2.28) virtual-locale-nds-nl (= 2.28)"
RDEPENDS_glibc-langpack-nds = "glibc glibc-common"
RPROVIDES_glibc-langpack-ne = "locale-base-ne-np (= 2.28) virtual-locale-ne (= 2.28) virtual-locale-ne-np (= 2.28)"
RDEPENDS_glibc-langpack-ne = "glibc glibc-common"
RPROVIDES_glibc-langpack-nhn = "locale-base-nhn-mx (= 2.28) virtual-locale-nhn (= 2.28) virtual-locale-nhn-mx (= 2.28)"
RDEPENDS_glibc-langpack-nhn = "glibc glibc-common"
RPROVIDES_glibc-langpack-niu = "locale-base-niu-nu (= 2.28) locale-base-niu-nz (= 2.28) virtual-locale-niu (= 2.28) virtual-locale-niu (= 2.28) virtual-locale-niu-nu (= 2.28) virtual-locale-niu-nz (= 2.28)"
RDEPENDS_glibc-langpack-niu = "glibc glibc-common"
RPROVIDES_glibc-langpack-nl = "locale-base-nl-aw (= 2.28) locale-base-nl-be (= 2.28) locale-base-nl-be.utf8 (= 2.28) locale-base-nl-be@euro (= 2.28) locale-base-nl-nl (= 2.28) locale-base-nl-nl.utf8 (= 2.28) locale-base-nl-nl@euro (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl (= 2.28) virtual-locale-nl-aw (= 2.28) virtual-locale-nl-be (= 2.28) virtual-locale-nl-be.utf8 (= 2.28) virtual-locale-nl-be@euro (= 2.28) virtual-locale-nl-nl (= 2.28) virtual-locale-nl-nl.utf8 (= 2.28) virtual-locale-nl-nl@euro (= 2.28)"
RDEPENDS_glibc-langpack-nl = "glibc glibc-common"
RPROVIDES_glibc-langpack-nn = "locale-base-nn-no (= 2.28) locale-base-nn-no.utf8 (= 2.28) virtual-locale-nn (= 2.28) virtual-locale-nn (= 2.28) virtual-locale-nn-no (= 2.28) virtual-locale-nn-no.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-nn = "glibc glibc-common"
RPROVIDES_glibc-langpack-nr = "locale-base-nr-za (= 2.28) virtual-locale-nr (= 2.28) virtual-locale-nr-za (= 2.28)"
RDEPENDS_glibc-langpack-nr = "glibc glibc-common"
RPROVIDES_glibc-langpack-nso = "locale-base-nso-za (= 2.28) virtual-locale-nso (= 2.28) virtual-locale-nso-za (= 2.28)"
RDEPENDS_glibc-langpack-nso = "glibc glibc-common"
RPROVIDES_glibc-langpack-oc = "locale-base-oc-fr (= 2.28) locale-base-oc-fr.utf8 (= 2.28) virtual-locale-oc (= 2.28) virtual-locale-oc (= 2.28) virtual-locale-oc-fr (= 2.28) virtual-locale-oc-fr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-oc = "glibc glibc-common"
RPROVIDES_glibc-langpack-om = "locale-base-om-et (= 2.28) locale-base-om-ke (= 2.28) locale-base-om-ke.utf8 (= 2.28) virtual-locale-om (= 2.28) virtual-locale-om (= 2.28) virtual-locale-om (= 2.28) virtual-locale-om-et (= 2.28) virtual-locale-om-ke (= 2.28) virtual-locale-om-ke.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-om = "glibc glibc-common"
RPROVIDES_glibc-langpack-or = "locale-base-or-in (= 2.28) virtual-locale-or (= 2.28) virtual-locale-or-in (= 2.28)"
RDEPENDS_glibc-langpack-or = "glibc glibc-common"
RPROVIDES_glibc-langpack-os = "locale-base-os-ru (= 2.28) virtual-locale-os (= 2.28) virtual-locale-os-ru (= 2.28)"
RDEPENDS_glibc-langpack-os = "glibc glibc-common"
RPROVIDES_glibc-langpack-pa = "locale-base-pa-in (= 2.28) locale-base-pa-pk (= 2.28) virtual-locale-pa (= 2.28) virtual-locale-pa (= 2.28) virtual-locale-pa-in (= 2.28) virtual-locale-pa-pk (= 2.28)"
RDEPENDS_glibc-langpack-pa = "glibc glibc-common"
RPROVIDES_glibc-langpack-pap = "locale-base-pap-aw (= 2.28) locale-base-pap-cw (= 2.28) virtual-locale-pap (= 2.28) virtual-locale-pap (= 2.28) virtual-locale-pap-aw (= 2.28) virtual-locale-pap-cw (= 2.28)"
RDEPENDS_glibc-langpack-pap = "glibc glibc-common"
RPROVIDES_glibc-langpack-pl = "locale-base-pl-pl (= 2.28) locale-base-pl-pl.utf8 (= 2.28) virtual-locale-pl (= 2.28) virtual-locale-pl (= 2.28) virtual-locale-pl-pl (= 2.28) virtual-locale-pl-pl.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-pl = "glibc glibc-common"
RPROVIDES_glibc-langpack-ps = "locale-base-ps-af (= 2.28) virtual-locale-ps (= 2.28) virtual-locale-ps-af (= 2.28)"
RDEPENDS_glibc-langpack-ps = "glibc glibc-common"
RPROVIDES_glibc-langpack-pt = "locale-base-pt-br (= 2.28) locale-base-pt-br.utf8 (= 2.28) locale-base-pt-pt (= 2.28) locale-base-pt-pt.utf8 (= 2.28) locale-base-pt-pt@euro (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt-br (= 2.28) virtual-locale-pt-br.utf8 (= 2.28) virtual-locale-pt-pt (= 2.28) virtual-locale-pt-pt.utf8 (= 2.28) virtual-locale-pt-pt@euro (= 2.28)"
RDEPENDS_glibc-langpack-pt = "glibc glibc-common"
RPROVIDES_glibc-langpack-quz = "locale-base-quz-pe (= 2.28) virtual-locale-quz (= 2.28) virtual-locale-quz-pe (= 2.28)"
RDEPENDS_glibc-langpack-quz = "glibc glibc-common"
RPROVIDES_glibc-langpack-raj = "locale-base-raj-in (= 2.28) virtual-locale-raj (= 2.28) virtual-locale-raj-in (= 2.28)"
RDEPENDS_glibc-langpack-raj = "glibc glibc-common"
RPROVIDES_glibc-langpack-ro = "locale-base-ro-ro (= 2.28) locale-base-ro-ro.utf8 (= 2.28) virtual-locale-ro (= 2.28) virtual-locale-ro (= 2.28) virtual-locale-ro-ro (= 2.28) virtual-locale-ro-ro.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ro = "glibc glibc-common"
RPROVIDES_glibc-langpack-ru = "locale-base-ru-ru (= 2.28) locale-base-ru-ru.koi8r (= 2.28) locale-base-ru-ru.utf8 (= 2.28) locale-base-ru-ua (= 2.28) locale-base-ru-ua.utf8 (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru-ru (= 2.28) virtual-locale-ru-ru.koi8r (= 2.28) virtual-locale-ru-ru.utf8 (= 2.28) virtual-locale-ru-ua (= 2.28) virtual-locale-ru-ua.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ru = "glibc glibc-common"
RPROVIDES_glibc-langpack-rw = "locale-base-rw-rw (= 2.28) virtual-locale-rw (= 2.28) virtual-locale-rw-rw (= 2.28)"
RDEPENDS_glibc-langpack-rw = "glibc glibc-common"
RPROVIDES_glibc-langpack-sa = "locale-base-sa-in (= 2.28) virtual-locale-sa (= 2.28) virtual-locale-sa-in (= 2.28)"
RDEPENDS_glibc-langpack-sa = "glibc glibc-common"
RPROVIDES_glibc-langpack-sah = "locale-base-sah-ru (= 2.28) virtual-locale-sah (= 2.28) virtual-locale-sah-ru (= 2.28)"
RDEPENDS_glibc-langpack-sah = "glibc glibc-common"
RPROVIDES_glibc-langpack-sat = "locale-base-sat-in (= 2.28) virtual-locale-sat (= 2.28) virtual-locale-sat-in (= 2.28)"
RDEPENDS_glibc-langpack-sat = "glibc glibc-common"
RPROVIDES_glibc-langpack-sc = "locale-base-sc-it (= 2.28) virtual-locale-sc (= 2.28) virtual-locale-sc-it (= 2.28)"
RDEPENDS_glibc-langpack-sc = "glibc glibc-common"
RPROVIDES_glibc-langpack-sd = "locale-base-sd-in (= 2.28) locale-base-sd-in@devanagari (= 2.28) virtual-locale-sd (= 2.28) virtual-locale-sd (= 2.28) virtual-locale-sd-in (= 2.28) virtual-locale-sd-in@devanagari (= 2.28)"
RDEPENDS_glibc-langpack-sd = "glibc glibc-common"
RPROVIDES_glibc-langpack-se = "locale-base-se-no (= 2.28) virtual-locale-se (= 2.28) virtual-locale-se-no (= 2.28)"
RDEPENDS_glibc-langpack-se = "glibc glibc-common"
RPROVIDES_glibc-langpack-sgs = "locale-base-sgs-lt (= 2.28) virtual-locale-sgs (= 2.28) virtual-locale-sgs-lt (= 2.28)"
RDEPENDS_glibc-langpack-sgs = "glibc glibc-common"
RPROVIDES_glibc-langpack-shn = "locale-base-shn-mm (= 2.28) virtual-locale-shn (= 2.28) virtual-locale-shn-mm (= 2.28)"
RDEPENDS_glibc-langpack-shn = "glibc glibc-common"
RPROVIDES_glibc-langpack-shs = "locale-base-shs-ca (= 2.28) virtual-locale-shs (= 2.28) virtual-locale-shs-ca (= 2.28)"
RDEPENDS_glibc-langpack-shs = "glibc glibc-common"
RPROVIDES_glibc-langpack-si = "locale-base-si-lk (= 2.28) virtual-locale-si (= 2.28) virtual-locale-si-lk (= 2.28)"
RDEPENDS_glibc-langpack-si = "glibc glibc-common"
RPROVIDES_glibc-langpack-sid = "locale-base-sid-et (= 2.28) virtual-locale-sid (= 2.28) virtual-locale-sid-et (= 2.28)"
RDEPENDS_glibc-langpack-sid = "glibc glibc-common"
RPROVIDES_glibc-langpack-sk = "locale-base-sk-sk (= 2.28) locale-base-sk-sk.utf8 (= 2.28) virtual-locale-sk (= 2.28) virtual-locale-sk (= 2.28) virtual-locale-sk-sk (= 2.28) virtual-locale-sk-sk.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sk = "glibc glibc-common"
RPROVIDES_glibc-langpack-sl = "locale-base-sl-si (= 2.28) locale-base-sl-si.utf8 (= 2.28) virtual-locale-sl (= 2.28) virtual-locale-sl (= 2.28) virtual-locale-sl-si (= 2.28) virtual-locale-sl-si.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sl = "glibc glibc-common"
RPROVIDES_glibc-langpack-sm = "locale-base-sm-ws (= 2.28) virtual-locale-sm (= 2.28) virtual-locale-sm-ws (= 2.28)"
RDEPENDS_glibc-langpack-sm = "glibc glibc-common"
RPROVIDES_glibc-langpack-so = "locale-base-so-dj (= 2.28) locale-base-so-dj.utf8 (= 2.28) locale-base-so-et (= 2.28) locale-base-so-ke (= 2.28) locale-base-so-ke.utf8 (= 2.28) locale-base-so-so (= 2.28) locale-base-so-so.utf8 (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so (= 2.28) virtual-locale-so-dj (= 2.28) virtual-locale-so-dj.utf8 (= 2.28) virtual-locale-so-et (= 2.28) virtual-locale-so-ke (= 2.28) virtual-locale-so-ke.utf8 (= 2.28) virtual-locale-so-so (= 2.28) virtual-locale-so-so.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-so = "glibc glibc-common"
RPROVIDES_glibc-langpack-sq = "locale-base-sq-al (= 2.28) locale-base-sq-al.utf8 (= 2.28) locale-base-sq-mk (= 2.28) virtual-locale-sq (= 2.28) virtual-locale-sq (= 2.28) virtual-locale-sq (= 2.28) virtual-locale-sq-al (= 2.28) virtual-locale-sq-al.utf8 (= 2.28) virtual-locale-sq-mk (= 2.28)"
RDEPENDS_glibc-langpack-sq = "glibc glibc-common"
RPROVIDES_glibc-langpack-sr = "locale-base-sr-me (= 2.28) locale-base-sr-rs (= 2.28) locale-base-sr-rs@latin (= 2.28) virtual-locale-sr (= 2.28) virtual-locale-sr (= 2.28) virtual-locale-sr (= 2.28) virtual-locale-sr-me (= 2.28) virtual-locale-sr-rs (= 2.28) virtual-locale-sr-rs@latin (= 2.28)"
RDEPENDS_glibc-langpack-sr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ss = "locale-base-ss-za (= 2.28) virtual-locale-ss (= 2.28) virtual-locale-ss-za (= 2.28)"
RDEPENDS_glibc-langpack-ss = "glibc glibc-common"
RPROVIDES_glibc-langpack-st = "locale-base-st-za (= 2.28) locale-base-st-za.utf8 (= 2.28) virtual-locale-st (= 2.28) virtual-locale-st (= 2.28) virtual-locale-st-za (= 2.28) virtual-locale-st-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-st = "glibc glibc-common"
RPROVIDES_glibc-langpack-sv = "locale-base-sv-fi (= 2.28) locale-base-sv-fi.utf8 (= 2.28) locale-base-sv-fi@euro (= 2.28) locale-base-sv-se (= 2.28) locale-base-sv-se.iso885915 (= 2.28) locale-base-sv-se.utf8 (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv (= 2.28) virtual-locale-sv-fi (= 2.28) virtual-locale-sv-fi.utf8 (= 2.28) virtual-locale-sv-fi@euro (= 2.28) virtual-locale-sv-se (= 2.28) virtual-locale-sv-se.iso885915 (= 2.28) virtual-locale-sv-se.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-sv = "glibc glibc-common"
RPROVIDES_glibc-langpack-sw = "locale-base-sw-ke (= 2.28) locale-base-sw-tz (= 2.28) virtual-locale-sw (= 2.28) virtual-locale-sw (= 2.28) virtual-locale-sw-ke (= 2.28) virtual-locale-sw-tz (= 2.28)"
RDEPENDS_glibc-langpack-sw = "glibc glibc-common"
RPROVIDES_glibc-langpack-szl = "locale-base-szl-pl (= 2.28) virtual-locale-szl (= 2.28) virtual-locale-szl-pl (= 2.28)"
RDEPENDS_glibc-langpack-szl = "glibc glibc-common"
RPROVIDES_glibc-langpack-ta = "locale-base-ta-in (= 2.28) locale-base-ta-lk (= 2.28) virtual-locale-ta (= 2.28) virtual-locale-ta (= 2.28) virtual-locale-ta-in (= 2.28) virtual-locale-ta-lk (= 2.28)"
RDEPENDS_glibc-langpack-ta = "glibc glibc-common"
RPROVIDES_glibc-langpack-tcy = "locale-base-tcy-in.utf8 (= 2.28) virtual-locale-tcy (= 2.28) virtual-locale-tcy-in.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tcy = "glibc glibc-common"
RPROVIDES_glibc-langpack-te = "locale-base-te-in (= 2.28) virtual-locale-te (= 2.28) virtual-locale-te-in (= 2.28)"
RDEPENDS_glibc-langpack-te = "glibc glibc-common"
RPROVIDES_glibc-langpack-tg = "locale-base-tg-tj (= 2.28) locale-base-tg-tj.utf8 (= 2.28) virtual-locale-tg (= 2.28) virtual-locale-tg (= 2.28) virtual-locale-tg-tj (= 2.28) virtual-locale-tg-tj.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tg = "glibc glibc-common"
RPROVIDES_glibc-langpack-th = "locale-base-th-th (= 2.28) locale-base-th-th.utf8 (= 2.28) virtual-locale-th (= 2.28) virtual-locale-th (= 2.28) virtual-locale-th-th (= 2.28) virtual-locale-th-th.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-th = "glibc glibc-common"
RPROVIDES_glibc-langpack-the = "locale-base-the-np (= 2.28) virtual-locale-the (= 2.28) virtual-locale-the-np (= 2.28)"
RDEPENDS_glibc-langpack-the = "glibc glibc-common"
RPROVIDES_glibc-langpack-ti = "locale-base-ti-er (= 2.28) locale-base-ti-et (= 2.28) virtual-locale-ti (= 2.28) virtual-locale-ti (= 2.28) virtual-locale-ti-er (= 2.28) virtual-locale-ti-et (= 2.28)"
RDEPENDS_glibc-langpack-ti = "glibc glibc-common"
RPROVIDES_glibc-langpack-tig = "locale-base-tig-er (= 2.28) virtual-locale-tig (= 2.28) virtual-locale-tig-er (= 2.28)"
RDEPENDS_glibc-langpack-tig = "glibc glibc-common"
RPROVIDES_glibc-langpack-tk = "locale-base-tk-tm (= 2.28) virtual-locale-tk (= 2.28) virtual-locale-tk-tm (= 2.28)"
RDEPENDS_glibc-langpack-tk = "glibc glibc-common"
RPROVIDES_glibc-langpack-tl = "locale-base-tl-ph (= 2.28) locale-base-tl-ph.utf8 (= 2.28) virtual-locale-tl (= 2.28) virtual-locale-tl (= 2.28) virtual-locale-tl-ph (= 2.28) virtual-locale-tl-ph.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tl = "glibc glibc-common"
RPROVIDES_glibc-langpack-tn = "locale-base-tn-za (= 2.28) virtual-locale-tn (= 2.28) virtual-locale-tn-za (= 2.28)"
RDEPENDS_glibc-langpack-tn = "glibc glibc-common"
RPROVIDES_glibc-langpack-to = "locale-base-to-to (= 2.28) virtual-locale-to (= 2.28) virtual-locale-to-to (= 2.28)"
RDEPENDS_glibc-langpack-to = "glibc glibc-common"
RPROVIDES_glibc-langpack-tpi = "locale-base-tpi-pg (= 2.28) virtual-locale-tpi (= 2.28) virtual-locale-tpi-pg (= 2.28)"
RDEPENDS_glibc-langpack-tpi = "glibc glibc-common"
RPROVIDES_glibc-langpack-tr = "locale-base-tr-cy (= 2.28) locale-base-tr-cy.utf8 (= 2.28) locale-base-tr-tr (= 2.28) locale-base-tr-tr.utf8 (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr (= 2.28) virtual-locale-tr-cy (= 2.28) virtual-locale-tr-cy.utf8 (= 2.28) virtual-locale-tr-tr (= 2.28) virtual-locale-tr-tr.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tr = "glibc glibc-common"
RPROVIDES_glibc-langpack-ts = "locale-base-ts-za (= 2.28) virtual-locale-ts (= 2.28) virtual-locale-ts-za (= 2.28)"
RDEPENDS_glibc-langpack-ts = "glibc glibc-common"
RPROVIDES_glibc-langpack-tt = "locale-base-tt-ru (= 2.28) locale-base-tt-ru@iqtelif (= 2.28) virtual-locale-tt (= 2.28) virtual-locale-tt (= 2.28) virtual-locale-tt-ru (= 2.28) virtual-locale-tt-ru@iqtelif (= 2.28)"
RDEPENDS_glibc-langpack-tt = "glibc glibc-common"
RPROVIDES_glibc-langpack-ug = "locale-base-ug-cn (= 2.28) virtual-locale-ug (= 2.28) virtual-locale-ug-cn (= 2.28)"
RDEPENDS_glibc-langpack-ug = "glibc glibc-common"
RPROVIDES_glibc-langpack-uk = "locale-base-uk-ua (= 2.28) locale-base-uk-ua.utf8 (= 2.28) virtual-locale-uk (= 2.28) virtual-locale-uk (= 2.28) virtual-locale-uk-ua (= 2.28) virtual-locale-uk-ua.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-uk = "glibc glibc-common"
RPROVIDES_glibc-langpack-unm = "locale-base-unm-us (= 2.28) virtual-locale-unm (= 2.28) virtual-locale-unm-us (= 2.28)"
RDEPENDS_glibc-langpack-unm = "glibc glibc-common"
RPROVIDES_glibc-langpack-ur = "locale-base-ur-in (= 2.28) locale-base-ur-pk (= 2.28) virtual-locale-ur (= 2.28) virtual-locale-ur (= 2.28) virtual-locale-ur-in (= 2.28) virtual-locale-ur-pk (= 2.28)"
RDEPENDS_glibc-langpack-ur = "glibc glibc-common"
RPROVIDES_glibc-langpack-uz = "locale-base-uz-uz (= 2.28) locale-base-uz-uz.utf8 (= 2.28) locale-base-uz-uz@cyrillic (= 2.28) virtual-locale-uz (= 2.28) virtual-locale-uz (= 2.28) virtual-locale-uz (= 2.28) virtual-locale-uz-uz (= 2.28) virtual-locale-uz-uz.utf8 (= 2.28) virtual-locale-uz-uz@cyrillic (= 2.28)"
RDEPENDS_glibc-langpack-uz = "glibc glibc-common"
RPROVIDES_glibc-langpack-ve = "locale-base-ve-za (= 2.28) virtual-locale-ve (= 2.28) virtual-locale-ve-za (= 2.28)"
RDEPENDS_glibc-langpack-ve = "glibc glibc-common"
RPROVIDES_glibc-langpack-vi = "locale-base-vi-vn (= 2.28) virtual-locale-vi (= 2.28) virtual-locale-vi-vn (= 2.28)"
RDEPENDS_glibc-langpack-vi = "glibc glibc-common"
RPROVIDES_glibc-langpack-wa = "locale-base-wa-be (= 2.28) locale-base-wa-be.utf8 (= 2.28) locale-base-wa-be@euro (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa (= 2.28) virtual-locale-wa-be (= 2.28) virtual-locale-wa-be.utf8 (= 2.28) virtual-locale-wa-be@euro (= 2.28)"
RDEPENDS_glibc-langpack-wa = "glibc glibc-common"
RPROVIDES_glibc-langpack-wae = "locale-base-wae-ch (= 2.28) virtual-locale-wae (= 2.28) virtual-locale-wae-ch (= 2.28)"
RDEPENDS_glibc-langpack-wae = "glibc glibc-common"
RPROVIDES_glibc-langpack-wal = "locale-base-wal-et (= 2.28) virtual-locale-wal (= 2.28) virtual-locale-wal-et (= 2.28)"
RDEPENDS_glibc-langpack-wal = "glibc glibc-common"
RPROVIDES_glibc-langpack-wo = "locale-base-wo-sn (= 2.28) virtual-locale-wo (= 2.28) virtual-locale-wo-sn (= 2.28)"
RDEPENDS_glibc-langpack-wo = "glibc glibc-common"
RPROVIDES_glibc-langpack-xh = "locale-base-xh-za (= 2.28) locale-base-xh-za.utf8 (= 2.28) virtual-locale-xh (= 2.28) virtual-locale-xh (= 2.28) virtual-locale-xh-za (= 2.28) virtual-locale-xh-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-xh = "glibc glibc-common"
RPROVIDES_glibc-langpack-yi = "locale-base-yi-us (= 2.28) locale-base-yi-us.utf8 (= 2.28) virtual-locale-yi (= 2.28) virtual-locale-yi (= 2.28) virtual-locale-yi-us (= 2.28) virtual-locale-yi-us.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-yi = "glibc glibc-common"
RPROVIDES_glibc-langpack-yo = "locale-base-yo-ng (= 2.28) virtual-locale-yo (= 2.28) virtual-locale-yo-ng (= 2.28)"
RDEPENDS_glibc-langpack-yo = "glibc glibc-common"
RPROVIDES_glibc-langpack-yue = "locale-base-yue-hk (= 2.28) virtual-locale-yue (= 2.28) virtual-locale-yue-hk (= 2.28)"
RDEPENDS_glibc-langpack-yue = "glibc glibc-common"
RPROVIDES_glibc-langpack-yuw = "locale-base-yuw-pg (= 2.28) virtual-locale-yuw (= 2.28) virtual-locale-yuw-pg (= 2.28)"
RDEPENDS_glibc-langpack-yuw = "glibc glibc-common"
RPROVIDES_glibc-langpack-zh = "locale-base-zh-cn (= 2.28) locale-base-zh-cn.gb18030 (= 2.28) locale-base-zh-cn.gbk (= 2.28) locale-base-zh-cn.utf8 (= 2.28) locale-base-zh-hk (= 2.28) locale-base-zh-hk.utf8 (= 2.28) locale-base-zh-sg (= 2.28) locale-base-zh-sg.gbk (= 2.28) locale-base-zh-sg.utf8 (= 2.28) locale-base-zh-tw (= 2.28) locale-base-zh-tw.euctw (= 2.28) locale-base-zh-tw.utf8 (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh-cn (= 2.28) virtual-locale-zh-cn.gb18030 (= 2.28) virtual-locale-zh-cn.gbk (= 2.28) virtual-locale-zh-cn.utf8 (= 2.28) virtual-locale-zh-hk (= 2.28) virtual-locale-zh-hk.utf8 (= 2.28) virtual-locale-zh-sg (= 2.28) virtual-locale-zh-sg.gbk (= 2.28) virtual-locale-zh-sg.utf8 (= 2.28) virtual-locale-zh-tw (= 2.28) virtual-locale-zh-tw.euctw (= 2.28) virtual-locale-zh-tw.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-zh = "glibc glibc-common"
RPROVIDES_glibc-langpack-zu = "locale-base-zu-za (= 2.28) locale-base-zu-za.utf8 (= 2.28) virtual-locale-zu (= 2.28) virtual-locale-zu (= 2.28) virtual-locale-zu-za (= 2.28) virtual-locale-zu-za.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-zu = "glibc glibc-common"
RDEPENDS_glibc-locale-source = "glibc glibc-common"
RDEPENDS_glibc-minimal-langpack = "glibc glibc-common"
RPM_SONAME_REQ_glibc-nss-devel = "libnss_compat.so.2 libnss_db.so.2 libnss_dns.so.2 libnss_files.so.2 libnss_hesiod.so.2"
RPROVIDES_glibc-nss-devel = "glibc-nss-dev (= 2.28)"
RDEPENDS_glibc-nss-devel = "glibc nss_db nss_hesiod"
RDEPENDS_glibc-static = "glibc-devel libxcrypt-static"
RPM_SONAME_PROV_libnsl = "libnsl.so.1"
RPM_SONAME_REQ_libnsl = "libc.so.6"
RDEPENDS_libnsl = "glibc"
RPM_SONAME_REQ_nscd = "libaudit.so.1 libc.so.6 libcap.so.2 libpthread.so.0 libselinux.so.1"
RDEPENDS_nscd = "audit-libs bash coreutils glibc libcap libselinux shadow-utils systemd"
RPM_SONAME_PROV_nss_db = "libnss_db.so.2"
RPM_SONAME_REQ_nss_db = "libc.so.6 libnss_files.so.2"
RDEPENDS_nss_db = "glibc"
RPM_SONAME_PROV_nss_hesiod = "libnss_hesiod.so.2"
RPM_SONAME_REQ_nss_hesiod = "libc.so.6 libnss_files.so.2 libresolv.so.2"
RDEPENDS_nss_hesiod = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/compat-libpthread-nonshared-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-all-langpacks-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-common-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-devel-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-headers-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-aa-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-af-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-agr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ak-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-am-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-an-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-anp-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ar-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-as-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ast-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ayc-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-az-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-be-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bem-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ber-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bg-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bhb-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bho-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bo-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-br-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-brx-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-bs-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-byn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ca-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ce-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-chr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-cmn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-crh-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-cs-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-csb-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-cv-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-cy-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-da-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-de-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-doi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-dsb-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-dv-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-dz-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-el-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-en-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-eo-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-es-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-et-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-eu-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fa-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ff-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fil-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fo-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fur-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-fy-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ga-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-gd-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-gez-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-gl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-gu-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-gv-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ha-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hak-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-he-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hif-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hne-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hsb-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ht-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hu-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hy-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ia-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-id-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ig-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ik-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-is-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-it-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-iu-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ja-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ka-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kab-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kk-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-km-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ko-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kok-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ks-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ku-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kw-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ky-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lb-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lg-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-li-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lij-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ln-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lo-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lt-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lv-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-lzh-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mag-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mai-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mfe-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mg-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mhr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-miq-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mjw-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mk-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ml-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mni-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ms-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mt-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-my-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nan-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nb-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nds-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ne-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nhn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-niu-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nso-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-oc-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-om-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-or-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-os-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-pa-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-pap-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-pl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ps-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-pt-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-quz-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-raj-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ro-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ru-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-rw-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sa-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sah-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sat-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sc-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sd-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-se-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sgs-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-shn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-shs-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-si-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sid-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sk-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sm-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-so-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sq-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ss-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-st-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sv-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-sw-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-szl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ta-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tcy-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-te-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tg-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-th-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-the-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ti-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tig-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tk-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tn-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-to-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tpi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tr-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ts-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-tt-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ug-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-uk-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-unm-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ur-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-uz-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ve-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-vi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-wa-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-wae-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-wal-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-wo-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-xh-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-yi-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-yo-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-yue-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-yuw-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-zh-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-zu-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-locale-source-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-minimal-langpack-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnsl-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nscd-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nss_db-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glibc-nss-devel-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glibc-static-2.28-101.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/nss_hesiod-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[compat-libpthread-nonshared.sha256sum] = "651fa500a0667e57843ea1cbc5b45991151bc4c52a8512170a859e3bf3c628cc"
SRC_URI[glibc.sha256sum] = "924716f6cecdf463d0c5f60438b5cc24ee02f992d6ac6297e2b9f03f751e13ed"
SRC_URI[glibc-all-langpacks.sha256sum] = "cf06bc43798515c2edae89e35824b94717b22ac3822394eb258f532aeffa81b3"
SRC_URI[glibc-common.sha256sum] = "1f8ea72e27bbfc31bfb5aa7aa312e1f6ad968dac4a3cc10626034d8bb7205701"
SRC_URI[glibc-devel.sha256sum] = "98e50bd04aa020854987fb0a477da5e1fc2947bd51445dcd1abb3e1a6df97a70"
SRC_URI[glibc-headers.sha256sum] = "17443127978a79104dfdd2b57f9b315b9495f29e15ba0fdc6441fddd3250b64b"
SRC_URI[glibc-langpack-aa.sha256sum] = "a9d303942e4d002beeeaafc04512b1a3a4996ae5d0079f5541b818b421fbd97d"
SRC_URI[glibc-langpack-af.sha256sum] = "a680d5e1a08b501f68f09041520e3b248c2ff1cd06626b2b3bf934d8988f0cc8"
SRC_URI[glibc-langpack-agr.sha256sum] = "1ea810e63e02fe111c5025a771ddcadf15eb64c5821817121e4cf6a579d740cd"
SRC_URI[glibc-langpack-ak.sha256sum] = "4ac1fd9583e69788b0f8a7c497b889324e18b036f38b86dc4754b754fddd51b0"
SRC_URI[glibc-langpack-am.sha256sum] = "c7b7ae19508ff771ddceb9bb5f44bba9a00ff8d170f70803ca1534f7e5d4bb35"
SRC_URI[glibc-langpack-an.sha256sum] = "eea72f9c02f6ac6294aad17f075ab580b40687bdb7af7a1728eeb997228f33a3"
SRC_URI[glibc-langpack-anp.sha256sum] = "87a13680e786311c496596f855a209983a070b44acefb1d94c65f88341e6b3b3"
SRC_URI[glibc-langpack-ar.sha256sum] = "8f6e5d6df90499fb282f37633b64c4c257914aff2d26e92aeff2da604f4dc75a"
SRC_URI[glibc-langpack-as.sha256sum] = "f0468d7498e461a5aa0fc70420d1c61d728efdb0495de46eb955c54d0c82082e"
SRC_URI[glibc-langpack-ast.sha256sum] = "a2afdf63ab6257304f69c34077dfd7864c8bb62c8251e67d28df1f7f2004e696"
SRC_URI[glibc-langpack-ayc.sha256sum] = "573ebca7d001921cadc206548c793a93f38f9de3aff8a42c0240acc3d47ee5f4"
SRC_URI[glibc-langpack-az.sha256sum] = "cbff513bb3c21462d767e143c98132f21ef1104b73ae2164a13364e55fbec472"
SRC_URI[glibc-langpack-be.sha256sum] = "3838ff0bb183b2c91ee45cf8563566baa4d4121b811df38bbc739e8847c440c0"
SRC_URI[glibc-langpack-bem.sha256sum] = "1ee3b9609b4afe78dc53f147456af82db55398839cd7605c0d586e1eb262578c"
SRC_URI[glibc-langpack-ber.sha256sum] = "e0e19ac0a0e345bc72473de8458711962088726418ca7405a210bcc0bff17f84"
SRC_URI[glibc-langpack-bg.sha256sum] = "f8ccb8f3d86b94594a315da324e02051da259bdfdebf2f39fe6452243c79a421"
SRC_URI[glibc-langpack-bhb.sha256sum] = "9cdfa3780ae3d2f2a2981c7bc5ef0e644946006ef45f9f3ea1b6ec5f7f15783c"
SRC_URI[glibc-langpack-bho.sha256sum] = "bb7a5916b2eb6536c93f63b91434403717db6726a8597a531af27e2a5fd21e89"
SRC_URI[glibc-langpack-bi.sha256sum] = "0025f609c7f1af9de8918449fe110f366e7bef90763dbf6cce3dd6bf67613225"
SRC_URI[glibc-langpack-bn.sha256sum] = "abfe9738a272e401eaf5efa37418c2bc050c0e9a50c0eca371a4f54ea104626a"
SRC_URI[glibc-langpack-bo.sha256sum] = "5165a105fa0994ad25b8b5fcbefe91e18634455a0cfde212f3671d6a265be7c6"
SRC_URI[glibc-langpack-br.sha256sum] = "d84ec21a74399147a00f7f8577526beabd45c9b274d792d5bdd5ce3db74cb358"
SRC_URI[glibc-langpack-brx.sha256sum] = "8367c04d450e158289c3abbe3263c42a482023ff736a769ffc85c6b043c99f29"
SRC_URI[glibc-langpack-bs.sha256sum] = "0c5f7e60866d7f54a7e1d4b2918b20f23e9404103d68e5eb7808661b5cf8f73b"
SRC_URI[glibc-langpack-byn.sha256sum] = "7eb1cf2308db5a870b8632f61e5621bc5daccc6a6ec80b627e8304069ae14c49"
SRC_URI[glibc-langpack-ca.sha256sum] = "5c04a179359845fd6e01bf17f2254fa332ef6896e198a22f2990abc9daa6e7ab"
SRC_URI[glibc-langpack-ce.sha256sum] = "177a0c4b59fcb2c3f48712fe97383dddc7c1e8388e4469bd6abf056c3911ebc1"
SRC_URI[glibc-langpack-chr.sha256sum] = "2c8bf18788080598d502fab53f2e7e360d7e2f95d8efc2743ff9201ea5d61fb0"
SRC_URI[glibc-langpack-cmn.sha256sum] = "54648f05e20e4d99c246cd565d11ce6dd28b83f46ae104342d6a0b0b6a64a0db"
SRC_URI[glibc-langpack-crh.sha256sum] = "b5ff0cfd0891d46f2dcb20d149398e740260049a00863d29710bbcf98732676f"
SRC_URI[glibc-langpack-cs.sha256sum] = "c7d5a7cbbe07a1b1d7fe9cba18dd9b0790e067109cc42127d416474f66da28de"
SRC_URI[glibc-langpack-csb.sha256sum] = "030083a0afb7ec36a4d023b3304d6ec617e28aed143b80791709b8004052ca80"
SRC_URI[glibc-langpack-cv.sha256sum] = "9fa3a1b8a61a601574b757bf71b6b7bdd46b69a3ea4b269b951e88f68bde896f"
SRC_URI[glibc-langpack-cy.sha256sum] = "89948654c075ffc2fac59abb84ffed8fb9331b49c700bcd7f33cdd1d0267b165"
SRC_URI[glibc-langpack-da.sha256sum] = "d18efd7a5e51a9fe5e7686ef58f0657bd267cc6d1b0dd37703ac32202cf58dc0"
SRC_URI[glibc-langpack-de.sha256sum] = "0df280a5f02946a34ae4bbdc7e188251f5e7e246ccb41187a8a0786378ccd91a"
SRC_URI[glibc-langpack-doi.sha256sum] = "567f5027605528da506672832537aaf0f61cc301162d1795575335d9fb7e2bde"
SRC_URI[glibc-langpack-dsb.sha256sum] = "297e977ee40a05d367bb3e5bdf134f09e899850d4475073f9369d4023d85abc9"
SRC_URI[glibc-langpack-dv.sha256sum] = "a7c70ccaeca028e792ee259756d9c39158ded3c8e764f5eaae7ee862fe698794"
SRC_URI[glibc-langpack-dz.sha256sum] = "74412bf64dfaf6d310b0f45e729993d1d1746dbbc0494494d118dd2e82c46efd"
SRC_URI[glibc-langpack-el.sha256sum] = "98a9e500a13979cc6856aab213a011f53e37466b75a3ce72f6e743a468b9d38a"
SRC_URI[glibc-langpack-en.sha256sum] = "3c2f6156632a6bd55887244753bdc1952a923900806654023e4a996f17ac2ba2"
SRC_URI[glibc-langpack-eo.sha256sum] = "d3263b672cc157e43a3db253a2bdb8e9920568981146a4667c08e6136149f71c"
SRC_URI[glibc-langpack-es.sha256sum] = "28c2971bc7ac963d3b243bdca506e374259792fa9debb513df498acc1ffbd20c"
SRC_URI[glibc-langpack-et.sha256sum] = "7350adee0f89a307ee5c96a9e7660b9c50a18952588817a1da8e8e38671d6124"
SRC_URI[glibc-langpack-eu.sha256sum] = "c3dd542d201dfbcdb8e07ee7fb8efecf630b531400a22cb8a58d59b7ffd5dbe7"
SRC_URI[glibc-langpack-fa.sha256sum] = "952a2e747b855a90d85c55f07b2ee4aac6fd53c499dd943d2509bb68f8966f06"
SRC_URI[glibc-langpack-ff.sha256sum] = "166c9cfcace4034e94e1fff9de3b0939a2892d93fc561c21202beec292a62676"
SRC_URI[glibc-langpack-fi.sha256sum] = "8c44eb817a5108f9f272bdf7c870968715712b8828f3a6b567cd863946a44bb0"
SRC_URI[glibc-langpack-fil.sha256sum] = "f3071fedad4c01a24913b6053174f687647287413f98ec362c54ba8be43ed2e1"
SRC_URI[glibc-langpack-fo.sha256sum] = "4b9b01ea7e2c9f3b2b9656763b9e79dd0fe8831d2fa1850a712fcbdffc6f74f0"
SRC_URI[glibc-langpack-fr.sha256sum] = "9bf737d642bbbf9bdb29996b7b947fe72b58a7d1f6556254390c379c0702d377"
SRC_URI[glibc-langpack-fur.sha256sum] = "22c7741f419fa3dc3fe355f17ba0c84968578839165936d02d02aa8333af2cb9"
SRC_URI[glibc-langpack-fy.sha256sum] = "6e08bc619a4f35c408d287fb4a41de20c1d125883a874188594ecbb9275ed54a"
SRC_URI[glibc-langpack-ga.sha256sum] = "2d44a1ffa4b3dcfee3cfd95520fb5a649bba1aaa482d32d052a92a0569a0a55d"
SRC_URI[glibc-langpack-gd.sha256sum] = "22bc7f5c4ffeba77b15c1f6e5dc792ca811a994509a527a04b86be36c3e9d901"
SRC_URI[glibc-langpack-gez.sha256sum] = "6c3d61ff78f73f268a470e20e765b038936008590b4ea4c9e47c62e6dba65108"
SRC_URI[glibc-langpack-gl.sha256sum] = "15ddcd602b77f8992d625a53963b63bb3cfaf51424196bb22d0849aed2c40a6f"
SRC_URI[glibc-langpack-gu.sha256sum] = "759973d66f5ef4ab53987c2972a945980be28ab55f89b2a0e97678e7821663c5"
SRC_URI[glibc-langpack-gv.sha256sum] = "6d955cd945a04d4679d564e89465e5ec02e66bccd69f46f170c68402b417e47c"
SRC_URI[glibc-langpack-ha.sha256sum] = "1afcfd2bac563ca98829cf205e3694a975c3d33bae89a4899debb74ae2cafae1"
SRC_URI[glibc-langpack-hak.sha256sum] = "e5e2c0f0b7222801c0901c59890bcc9aff6860c0c9639ffb35c46ebf882720cf"
SRC_URI[glibc-langpack-he.sha256sum] = "592fb19e8a80943bb7a53ba52273e00b413ab7e684274370ababf31f4cbecabe"
SRC_URI[glibc-langpack-hi.sha256sum] = "67eeb87824f02b75417c68fe86342a33baf487c25d1045558f33cb7d612b25a8"
SRC_URI[glibc-langpack-hif.sha256sum] = "db29b07de628f9a802c60d54e789dfe7e332bffb0428974c0ec41437d608f76e"
SRC_URI[glibc-langpack-hne.sha256sum] = "554d93182150e775b9ea906fc5a19cc4905efe2c39992837947780e23b5feb75"
SRC_URI[glibc-langpack-hr.sha256sum] = "d9c649b758106a62fe2134234e6c002f7100abd46d47a0e05c50a646e68aeab5"
SRC_URI[glibc-langpack-hsb.sha256sum] = "45ba4c879de38b191e5af8e4c3b25cc88c72f23cedcd1600549594268919e69e"
SRC_URI[glibc-langpack-ht.sha256sum] = "17821e14dd1e53cef480565ccce89fe7e2dd7da2a6081563385b85fc1ecd4f6f"
SRC_URI[glibc-langpack-hu.sha256sum] = "683ad60b43acba24895a000ae682b15cff961d692eb1167c51d3d6444b56886b"
SRC_URI[glibc-langpack-hy.sha256sum] = "75da778bcd198664e5b500368e399b2024648f768b18b3f07e59e21a45eef827"
SRC_URI[glibc-langpack-ia.sha256sum] = "8309d5c09f91477cf5fa7493fb1a3dce3194d56fe6832cece835f06cc9ce0e90"
SRC_URI[glibc-langpack-id.sha256sum] = "1ac7966c3f42bc12e3a480aa0333dc59aeda2a147de29995d2e8ecf9fe7d61b0"
SRC_URI[glibc-langpack-ig.sha256sum] = "cd4bc46a7511ac632df6dd0d01fbdc1f7189278e8c9d08734311ad501a9cade9"
SRC_URI[glibc-langpack-ik.sha256sum] = "5c39d502847853e91d816e2aac87b74d283d51ca07f22e40e754277337669388"
SRC_URI[glibc-langpack-is.sha256sum] = "df4e8bce30055d57103a14e26e230acd64e2e4575a04719daf5437ce8f590ae8"
SRC_URI[glibc-langpack-it.sha256sum] = "1aa7ef66a44e8bf962d4d1e30f19717d589048b478140844a359ab95b8e74e6a"
SRC_URI[glibc-langpack-iu.sha256sum] = "fd1d522dc545675e43997e74078e2bda7915cfc8d8355bb06a7370a0480ceaa5"
SRC_URI[glibc-langpack-ja.sha256sum] = "5f6566196407707aadc30c92316441a5d98eb63e27b6146a3814494fbe4ff8bd"
SRC_URI[glibc-langpack-ka.sha256sum] = "4b04c14c6c3f28100fe26c148d500e891d0dfc5ad1ef17a121717c7bd7ca2c67"
SRC_URI[glibc-langpack-kab.sha256sum] = "ec9045f60c97dec4f38bc7261b9f0e7bb5463cbbe54e67406d18abbaec02e7a4"
SRC_URI[glibc-langpack-kk.sha256sum] = "2873854c8a0e3665e3e29fad019f6627e89e592c4215c2ae6e70d33a387e8482"
SRC_URI[glibc-langpack-kl.sha256sum] = "cf6b0c94e20c66a7038ab45b462d95de8e4766d82981f7b7748ec88ea6a683ed"
SRC_URI[glibc-langpack-km.sha256sum] = "55196cbe60950c94552504aa770518a7d207820923e450a6ef3cd8bc494418d7"
SRC_URI[glibc-langpack-kn.sha256sum] = "18e4d0eba466770201d4e6152acfc5d880ec43c2d1fd11046a94a03869d99332"
SRC_URI[glibc-langpack-ko.sha256sum] = "de2e07e70eb170014f945baddb0c49ccc6be8d153a1394ab544f82a8f4251abc"
SRC_URI[glibc-langpack-kok.sha256sum] = "f8f3635f995a0bcc39257cd8b29dceaf59eddcea7420d57b9643af67d48101f4"
SRC_URI[glibc-langpack-ks.sha256sum] = "167ad83da6ac0f4daf1149124b7b1cd692753b0eae0d6079fcc44d38d47b6e33"
SRC_URI[glibc-langpack-ku.sha256sum] = "21b7416813694a60dd67704afab7b6129c5b79de2ce4e1650de64c1ecc0c306c"
SRC_URI[glibc-langpack-kw.sha256sum] = "1fdb1374f9d7777674617e0a6f0b239dc0ca43c09b5a0d4ae651c4f54cce6872"
SRC_URI[glibc-langpack-ky.sha256sum] = "da990c056d858f1392d1d0c2f0a81205a63fb30f5c8c33c4b72dfbac05a4c593"
SRC_URI[glibc-langpack-lb.sha256sum] = "b733e876a17f3d78539be0b8da0f82bf3315d5269cb246c53a13b72b7c303898"
SRC_URI[glibc-langpack-lg.sha256sum] = "36e92f409aba89abe8294c6ba9460ebbe1f9fbd7523dc99feba38e6217564cd9"
SRC_URI[glibc-langpack-li.sha256sum] = "73c7d90d6282c26367af0e1fb492619912c6e5ec2f18cc0b87a2162b44844d90"
SRC_URI[glibc-langpack-lij.sha256sum] = "e3268b42c6a47e64aea67d6ac262993f81fb6b83e27dd2842504ba94bf30df73"
SRC_URI[glibc-langpack-ln.sha256sum] = "620e9b315dc54cd353cb885a86e6eb235dca5baf5aab82dc12e9d5f3d25fde20"
SRC_URI[glibc-langpack-lo.sha256sum] = "a2a6e5d97fd233e5b73a2e467ad5847bcc323193845055866a9853d041faee17"
SRC_URI[glibc-langpack-lt.sha256sum] = "b13e7b09d16ecf91d72d9254d1f0d0c655c04ebdf312603cb9956a57cda661d4"
SRC_URI[glibc-langpack-lv.sha256sum] = "e71f4ac341bf8ff2ad4640f82e8f496deef4a819e9b5936d1f7de7f49efceb52"
SRC_URI[glibc-langpack-lzh.sha256sum] = "b8f47df9225c76edbdcab06f3c15a9fd807e072a426d74f28aad9c7a61b23fdd"
SRC_URI[glibc-langpack-mag.sha256sum] = "6de6ea7ebe660ee5a285663b847636486ed29e5c1dd73a1ba02f64d8f8ca9ba2"
SRC_URI[glibc-langpack-mai.sha256sum] = "de0a7b9563b7c894b44ad6811803db6443fc54f767af5f1c09f0f0cb2668bd59"
SRC_URI[glibc-langpack-mfe.sha256sum] = "267aeae8b0a3491d14728fe1f01bc669be0b00611c271641c885576c6e0313d7"
SRC_URI[glibc-langpack-mg.sha256sum] = "7ebf32b8a2867c59a2771896266cd602aab32a8de1d67bd46ba33d7ef2f189d4"
SRC_URI[glibc-langpack-mhr.sha256sum] = "e8b0f9c609bfc37b1c489557cfedbb7c0fc49c3231a9c27e779a0e9d33ba0755"
SRC_URI[glibc-langpack-mi.sha256sum] = "c21ac4fbb38fc502c97c4aff617c4543b69213b385c3ff1886ec89f57f8b7007"
SRC_URI[glibc-langpack-miq.sha256sum] = "9728684c2892f112bdd3fcddac4f62b5dc0a48b2493180bafad123c146a14a36"
SRC_URI[glibc-langpack-mjw.sha256sum] = "6112b21e9c9ba64a5bf37dab412db4a436b363d0cc41027d5182dc043d5ad6ae"
SRC_URI[glibc-langpack-mk.sha256sum] = "2fc2f2fcd433d3f12879ede7119e0abbd0c500099e8cfba50e389e5a0b7872eb"
SRC_URI[glibc-langpack-ml.sha256sum] = "3d8dee353a50dfad9f09d46d7917a2c50c1d8cbecdb5731f9b6397ca94794974"
SRC_URI[glibc-langpack-mn.sha256sum] = "8d5a35be063f697a4d550b9a6a0b075350a718b8e1b9c4c86adc5a8c48dbe565"
SRC_URI[glibc-langpack-mni.sha256sum] = "0a9eef178336d552a9509d9032ebeab2381b964124137e28af6493a9724b6a32"
SRC_URI[glibc-langpack-mr.sha256sum] = "8da263830837ec03c5d462eda39914a55356372edf4c5711b5141421f7d11f8f"
SRC_URI[glibc-langpack-ms.sha256sum] = "11168c622e28555bf2a86ca4722fd1fa6bd2185d1e2733a01c709aaa998f97fd"
SRC_URI[glibc-langpack-mt.sha256sum] = "0a172ed863ad65097e937f590b8d433d5b3a4c42c7696827979f740e78d96eb3"
SRC_URI[glibc-langpack-my.sha256sum] = "1f94de111a350719dfe997948803417233e20cf3c7ac6811e7765c61b2677565"
SRC_URI[glibc-langpack-nan.sha256sum] = "5d6533799c2f11fa39d724149daa88e00b8c54724accb89e5817d6f26b9619d0"
SRC_URI[glibc-langpack-nb.sha256sum] = "2b75048d3a306ae0bb49472f49d94e8f09010f6c702ef50a32bff3266735d0e2"
SRC_URI[glibc-langpack-nds.sha256sum] = "9a8ccea7731e04a05edb028e433497a79ec7c82f3c3e8b2172bcd515a8537044"
SRC_URI[glibc-langpack-ne.sha256sum] = "53e6b9804dfac0686a7caf07e792c94890a9587144ed59a5f0bb334e19f76991"
SRC_URI[glibc-langpack-nhn.sha256sum] = "88fda99fb06d9e7f1cfe896b4376b4a3324811e4d675f47dd1665a35f6cc3599"
SRC_URI[glibc-langpack-niu.sha256sum] = "eaa9332601f2b3cbc38d60eb0d92112957a7273a9c27462b1b295748172fcc0a"
SRC_URI[glibc-langpack-nl.sha256sum] = "160931f82cae4171c186cff0d80c628c2d3d0bc9ad9bcc826d4f4102d08562fe"
SRC_URI[glibc-langpack-nn.sha256sum] = "ac3de83eb9e6bdfa5da49aff39462682bc8ddee9d53245ec378fa51b2e84b3b4"
SRC_URI[glibc-langpack-nr.sha256sum] = "4f2054a104346238756de29e7f75f9976445757c700bac6e3de25616c80393b2"
SRC_URI[glibc-langpack-nso.sha256sum] = "95669616bb62886bb574437358249768b3f7962c3ded82c0ffbfe1aa28642e48"
SRC_URI[glibc-langpack-oc.sha256sum] = "5fde92b2d4e490782757aa0ef0c9b5ee83d031566765a9c0afe1ee424c1a7546"
SRC_URI[glibc-langpack-om.sha256sum] = "861cca8f34600d4a6ad2bf003ed66350b04a0718d96c65fd0337befdb8362ce4"
SRC_URI[glibc-langpack-or.sha256sum] = "12c17ff616f7239ab1ffa9024bb1f2d4ea93c3f2e03370fd6c187939c17d6528"
SRC_URI[glibc-langpack-os.sha256sum] = "af350765ca10182e38a842b3085e6667f3667a1a54f0b92383527920bf6ed938"
SRC_URI[glibc-langpack-pa.sha256sum] = "a00446c1e4f41e838771be865e92a3e601d8826f9ab309317a8b5f9b50fcd67f"
SRC_URI[glibc-langpack-pap.sha256sum] = "d42349696315f4f9877e315d502b5fcd77897777d8b9aac358474fbb07e6e744"
SRC_URI[glibc-langpack-pl.sha256sum] = "39d833fdc28cc726e22a36d85ebd958039a8e4e91be9294390955994df77346c"
SRC_URI[glibc-langpack-ps.sha256sum] = "ddc7e7dab3b418cfcc8e3d766c9423ea87c017c67c6cd409521995fd5d46ae2a"
SRC_URI[glibc-langpack-pt.sha256sum] = "eea0e807401dd56e83d243973aab3eedb963778b68422d03de0f2a55214a740a"
SRC_URI[glibc-langpack-quz.sha256sum] = "948983264d650db23a77bed586f379bf66a8c007e5a984057a356bac07abeb0a"
SRC_URI[glibc-langpack-raj.sha256sum] = "7ae0fa6f7ee7a93a742a76b3fc5444541a9a1a755e9a6e6f276c919a93494c8a"
SRC_URI[glibc-langpack-ro.sha256sum] = "f85361f38aabb69c574ee5d540a2dfd134ad9b3d4a1da3e3e690897f694cecf7"
SRC_URI[glibc-langpack-ru.sha256sum] = "3fe2e69f825be18a410e71d6cadc90ce0f76c80daa2a034c2096f71d8100e31c"
SRC_URI[glibc-langpack-rw.sha256sum] = "49a232014f2cc08566df835e829f41b2ddd17ab21cc84135088cdb23d2d7fc48"
SRC_URI[glibc-langpack-sa.sha256sum] = "9c8f2da9c2c26d0315b92ec79eff5598236661b114ad4c8ee7d2a39368894fa9"
SRC_URI[glibc-langpack-sah.sha256sum] = "7c1c087a28e1415ef07c20a5dbcf152074de5701544c1f86fbe96528c51c37e1"
SRC_URI[glibc-langpack-sat.sha256sum] = "6dd6ab36f2772066a666cad49261c65e8a9363eef39f96fa71e850e6716bc89b"
SRC_URI[glibc-langpack-sc.sha256sum] = "0ea03d2c4e8946494690eeff29e3c401ce7a777e5863d3c971f60fe17ed1e199"
SRC_URI[glibc-langpack-sd.sha256sum] = "4712a71ed95c6d9aa9a6cb29fdc9d9a44f8a30f9159202712dd02ada3bc57d4a"
SRC_URI[glibc-langpack-se.sha256sum] = "e2f2469d5d25c3b379cbf7fe22e5d007735eac30a9c8adefd3ed2865d2a3ce7d"
SRC_URI[glibc-langpack-sgs.sha256sum] = "a99c9fe69d31bc38d5802bab63f61a423a1badc85dc658d8561eb91290642a35"
SRC_URI[glibc-langpack-shn.sha256sum] = "32d74a73021d13b064fd64cb062a4a460208fe94f98f25db28398ed72aa1af9f"
SRC_URI[glibc-langpack-shs.sha256sum] = "5a4fd4c3ac7a21ba76ab6febd056f359b1d166c85bab1c08717cd62727f6cd2e"
SRC_URI[glibc-langpack-si.sha256sum] = "952780e3dfedecc53d52ca6b587239651d741f65910550b7686c787d06fe69e4"
SRC_URI[glibc-langpack-sid.sha256sum] = "ea128e987faa9b4188dbc455e539cdbe0917a9c15529a2ccd45c3c5b84fc45b7"
SRC_URI[glibc-langpack-sk.sha256sum] = "f1848684cf6f284c00e39ddb6cba8b139d103802d6966b1314d661aa2eb66e50"
SRC_URI[glibc-langpack-sl.sha256sum] = "a3d4340e6b8e1565918d0399fe5f949143adbb889c134dd4e28b5cf859406b9b"
SRC_URI[glibc-langpack-sm.sha256sum] = "fc60e8520d08c6a53f643c8fc4afd48facd3f43891bf1a25a4717710fdff901b"
SRC_URI[glibc-langpack-so.sha256sum] = "c9891fbf58d92656376330507007ef2024facb10284e4804a0559034a8c2136c"
SRC_URI[glibc-langpack-sq.sha256sum] = "df19ef854effc19598ac0b25dd73c6546fd3cca4111f2eee742a8cf36d14d616"
SRC_URI[glibc-langpack-sr.sha256sum] = "2426458dc6dd20b0c326b4900bdd7768b288607f547956b093041134524ecf3d"
SRC_URI[glibc-langpack-ss.sha256sum] = "c76633d4427017167201b12a086c9b5396d0c476949f52e40f3d257333fbd97f"
SRC_URI[glibc-langpack-st.sha256sum] = "9888bec6fe584e7c7e5c6b52ab8c754618cd4875d981c1a42bfa4d924f321e7b"
SRC_URI[glibc-langpack-sv.sha256sum] = "1d237ec5ee05cabf06467cc88af2ee90bac504177604a42a4862984a58fcbf43"
SRC_URI[glibc-langpack-sw.sha256sum] = "176087981e5b5ad98a35f2f2bc6aab186d1b34532653946ca30cf4f92f99e84b"
SRC_URI[glibc-langpack-szl.sha256sum] = "6975f8e8e396dbe8fdf53bfd1652c8d485ea405496d72d8c6fe01a5ab776cdc5"
SRC_URI[glibc-langpack-ta.sha256sum] = "7ea9eac92341d243d9a6fb0d1af6a6089879a496e531886bef8cab81bbb02258"
SRC_URI[glibc-langpack-tcy.sha256sum] = "6804f8bd3c60cf7adb5bfd2d9c9254495eab97b87b2f29dcbc76b6506f681b51"
SRC_URI[glibc-langpack-te.sha256sum] = "883dd4628ff51e9a309f33655871b658edb38d20e7deab5abc1fe5981cf6bb57"
SRC_URI[glibc-langpack-tg.sha256sum] = "59dfe02635af0376e9fe9831160fae1b8cef610ff1a211454f07181e8d5a9986"
SRC_URI[glibc-langpack-th.sha256sum] = "3cd5c01e4399333cd3dc9f3aa4279dfb90c5be18e03973a16d3e660ffb3a572a"
SRC_URI[glibc-langpack-the.sha256sum] = "227a937dd7bd07e8a1443d66ccb84b8ac61249879ca0dbf61a390c8e7f0cecfe"
SRC_URI[glibc-langpack-ti.sha256sum] = "1f8e04076085092f532e7af5bf0668a1716f6ea34e6a3914307290a45d74e933"
SRC_URI[glibc-langpack-tig.sha256sum] = "243430c44e44e1ddc7679a4fc1f46958f6418aa1e8a836d8a55e407be30db4fe"
SRC_URI[glibc-langpack-tk.sha256sum] = "3790665f343dfc43989d53ad13d85b05efea49bc62405642354f166a21c7934c"
SRC_URI[glibc-langpack-tl.sha256sum] = "eb93b6470c2460dcfa8e3b291e71779aa35482efa3d77690308347284f6fceab"
SRC_URI[glibc-langpack-tn.sha256sum] = "ff2289378ffe486db16f8756e635b6a13354e3a31ae8ef87cb2abd2e1c79f192"
SRC_URI[glibc-langpack-to.sha256sum] = "0880013497b159ae23813e5e6d96c333470d8365c531950f27497b201c8ad3a6"
SRC_URI[glibc-langpack-tpi.sha256sum] = "3cbd7ec06a44680392574daa2410ca60c35ae56ffe21db73a1ac1c7c4afc6c2b"
SRC_URI[glibc-langpack-tr.sha256sum] = "1b976b88f3a444074a500534e01cf74a5c3dbd80daf8a6f2dba784ab9e516d63"
SRC_URI[glibc-langpack-ts.sha256sum] = "1d33dd36af88ddd96fa4733268286f4711ad668487f597acb04a15b5351dea3f"
SRC_URI[glibc-langpack-tt.sha256sum] = "78d25b352c29b961b1ea675c6df1feac9bb137bab115b612a074951a1f5b6364"
SRC_URI[glibc-langpack-ug.sha256sum] = "1067da69593cf77ae7747f55d0c8688400176f8a00c3f34c64734c28e9dba9bc"
SRC_URI[glibc-langpack-uk.sha256sum] = "ee9573457c0bb2da4bdd6e44e557e3024732b6ea2aa770da85a833ae340d81d4"
SRC_URI[glibc-langpack-unm.sha256sum] = "a4ed1279cf50fb2a0419297a01ec32086d5e39c6cd905b5026df6244b2fcb8c7"
SRC_URI[glibc-langpack-ur.sha256sum] = "dbe3d215e5eff3b95b61ffebea20f36030e58291ee963759c1fec7613758f106"
SRC_URI[glibc-langpack-uz.sha256sum] = "044eeb8318ee69b11e12be0cbf290eb9d917bbcbd86e0c06e09a05829e55cf35"
SRC_URI[glibc-langpack-ve.sha256sum] = "dd99c2913986036dcfa8dcbae1155c34d4c72a36b75f1889967e997578241c15"
SRC_URI[glibc-langpack-vi.sha256sum] = "0b69221409514a65cd4da54bb5e1d94b9ff5cc552101feb812bbcfe58811dfe3"
SRC_URI[glibc-langpack-wa.sha256sum] = "26c526c56ef3cac08561512c3b745957ff4a41e56ba0332a8e9ba3e8ef304502"
SRC_URI[glibc-langpack-wae.sha256sum] = "f820a71f7339cb4290470b04adf3aa6d1dc3ec99d7acf5229997d1a834162c4a"
SRC_URI[glibc-langpack-wal.sha256sum] = "cb258ae598eddd777e2b97511c1b1b2faf163026c62be7ef873d8e045e2e0d86"
SRC_URI[glibc-langpack-wo.sha256sum] = "206f9a5f066146dd2da0560d8516e98e930ebce05da083e6cb83f2825032ff7d"
SRC_URI[glibc-langpack-xh.sha256sum] = "55b1789971d6b6b10c2d9955568bd9647880fc2793a7923c8b5bc0c0b8881a06"
SRC_URI[glibc-langpack-yi.sha256sum] = "4ec26e44af10d64bd3fa1b1eb068533df4679f1b0c80068c2f5eaf10653dd739"
SRC_URI[glibc-langpack-yo.sha256sum] = "a136a5f836ee7ee6825f0aa0013fe7c14618d26cd0226a7e1ea4fb38abceceff"
SRC_URI[glibc-langpack-yue.sha256sum] = "5ca955fd6a7a84dfd45e8be91070fb90df613af1b902b1ac56a35636f4116230"
SRC_URI[glibc-langpack-yuw.sha256sum] = "d2d80b57348183465765fe5a510f15f1af0f871ce465160052701620b2518d73"
SRC_URI[glibc-langpack-zh.sha256sum] = "cd179c6dbceb5822ffb927d587f0b079e96001fcd08ecffd9ecba184e944faf1"
SRC_URI[glibc-langpack-zu.sha256sum] = "70bb712031017c18280f905fc4d13f6027fdd8fccad377c3947f645ebf0c380c"
SRC_URI[glibc-locale-source.sha256sum] = "ce84580217b817fce4c3c854fa4a0303152d5d05712c4899d6672d688a262932"
SRC_URI[glibc-minimal-langpack.sha256sum] = "6cc8f3283cbe4dc33a9cb0a76a3451ac4a7ad12984ef0f928e0429098210c324"
SRC_URI[glibc-nss-devel.sha256sum] = "5afbecaa631e8c38b2ede22089c3b9bd0d4043cfcb4a19bf3fdaaa5b456debf4"
SRC_URI[glibc-static.sha256sum] = "e7ef1fb1a36f2f0c8c983675e55842ce1e5c4c8423a6e3cd38631e92641e07f5"
SRC_URI[libnsl.sha256sum] = "f498899da64b52245726e0e3c9f28c6456707a94e76e2eba137073c1ec218b54"
SRC_URI[nscd.sha256sum] = "675cae894615dcbb261df736a02aaa58ffa79734f8c1b9d92e6060fc2a81210f"
SRC_URI[nss_db.sha256sum] = "5d6287c55e39ea6bd0f4190d532e3033f8592756eed0f599ec863e6d510297cb"
SRC_URI[nss_hesiod.sha256sum] = "441a01497f29f0d3ae2a44573f77961291252c2f3dd5f9d873ae781ea8d704c2"
