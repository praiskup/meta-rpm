SUMMARY = "generated recipe based on ostree srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl fuse glib-2.0 gpgme libarchive libgcc libgpg-error libmount libselinux openssl pkgconfig-native systemd-libs xz zlib"
RPM_SONAME_REQ_ostree = "libarchive.so.13 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libfuse.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 liblzma.so.5 libmount.so.1 libostree-1.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libz.so.1"
RDEPENDS_ostree = "bash dracut fuse-libs glib2 glibc gnupg2 gpgme libarchive libcurl libgcc libgpg-error libmount libselinux openssl-libs ostree-libs systemd systemd-libs xz-libs zlib"
RPM_SONAME_REQ_ostree-devel = "libostree-1.so.1"
RPROVIDES_ostree-devel = "ostree-dev (= 2019.6)"
RDEPENDS_ostree-devel = "glib2-devel ostree-libs pkgconf-pkg-config"
RDEPENDS_ostree-grub2 = "bash grub2-pc ostree"
RPM_SONAME_PROV_ostree-libs = "libostree-1.so.1"
RPM_SONAME_REQ_ostree-libs = "libarchive.so.13 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 liblzma.so.5 libmount.so.1 libpthread.so.0 libselinux.so.1 libsystemd.so.0 libz.so.1"
RDEPENDS_ostree-libs = "glib2 glibc gpgme libarchive libcurl libgcc libgpg-error libmount libselinux openssl-libs systemd-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ostree-2019.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ostree-devel-2019.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ostree-grub2-2019.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ostree-libs-2019.6-2.el8.x86_64.rpm \
          "

SRC_URI[ostree.sha256sum] = "3c88fe2f0f59c33884f32fa3b6a4875235bbd311f8360de9c82ddc05e5d0aae0"
SRC_URI[ostree-devel.sha256sum] = "0cb27156ae85887fe7120aba89ea131d8679b8d2b1059d31adc8719021a1a642"
SRC_URI[ostree-grub2.sha256sum] = "1d1ac60962dc593787d591177922ec35b971cfeed7cda65ad25d0e1adca866d6"
SRC_URI[ostree-libs.sha256sum] = "163a76b84623563aa8002cda9b356ae1c111be5dc2c2462ceefdc1752a8092c1"
