SUMMARY = "generated recipe based on libXp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxau libxext pkgconfig-native"
RPM_SONAME_PROV_libXp = "libXp.so.6"
RPM_SONAME_REQ_libXp = "libX11.so.6 libXau.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXp = "glibc libX11 libXau libXext"
RPM_SONAME_REQ_libXp-devel = "libXp.so.6"
RPROVIDES_libXp-devel = "libXp-dev (= 1.0.3)"
RDEPENDS_libXp-devel = "libX11-devel libXau-devel libXext-devel libXp pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXp-1.0.3-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXp-devel-1.0.3-3.el8.x86_64.rpm \
          "

SRC_URI[libXp.sha256sum] = "2fb8509a945688011a8d1c8c59b27aff2fd55adb15ccc83d488adfef0b1607d0"
SRC_URI[libXp-devel.sha256sum] = "43af5497e1c950e50b59043e36bf48f783c0a4cefe978ab62f2a49cd310322b9"
