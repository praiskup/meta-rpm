SUMMARY = "generated recipe based on perl-XML-DOM srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-DOM = "perl-Carp perl-Exporter perl-XML-Parser perl-XML-RegExp perl-interpreter perl-libs perl-libwww-perl"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-XML-DOM-1.46-5.el8.noarch.rpm \
          "

SRC_URI[perl-XML-DOM.sha256sum] = "ad2f14f701a342cf887848aca63e81577c3a45fee6be2955a3b4da04291e81a3"
