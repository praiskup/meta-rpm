SUMMARY = "generated recipe based on shim srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_shim-ia32 = "dbxtool efi-filesystem mokutil"
RDEPENDS_shim-x64 = "dbxtool efi-filesystem mokutil"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/shim-ia32-15-15.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/shim-x64-15-15.el8_2.x86_64.rpm \
          "

SRC_URI[shim-ia32.sha256sum] = "4474ac352ddc25fc174e15485f1340fc57ef4ff73fa3530a50abde89fec89d24"
SRC_URI[shim-x64.sha256sum] = "f0346c485da9a7e8c8d93624f335cbb25790a7f37c6c03e43232517bb73bbacb"
