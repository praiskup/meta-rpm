SUMMARY = "generated recipe based on sblim-indication_helper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sblim-indication_helper = "libind_helper.so.0"
RPM_SONAME_REQ_sblim-indication_helper = "libc.so.6 libpthread.so.0"
RDEPENDS_sblim-indication_helper = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sblim-indication_helper-0.5.0-2.el8.x86_64.rpm \
          "

SRC_URI[sblim-indication_helper.sha256sum] = "a7a0ed4ee99ea23d748dbd58d350ded5f3def45f59f6b12b364c091e00ee0656"
