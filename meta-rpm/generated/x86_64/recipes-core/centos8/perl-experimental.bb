SUMMARY = "generated recipe based on perl-experimental srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-experimental = "perl-Carp perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-experimental-0.019-2.el8.noarch.rpm \
          "

SRC_URI[perl-experimental.sha256sum] = "8f1e70e23e11e689b3ce917ef56c5b63794d636d29f12db4c905e3b46e5df036"
