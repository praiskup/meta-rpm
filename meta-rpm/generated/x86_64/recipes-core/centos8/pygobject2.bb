SUMMARY = "generated recipe based on pygobject2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_pygobject2 = "libpyglib-2.0-python2.so.0"
RPM_SONAME_REQ_pygobject2 = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libpyglib-2.0-python2.so.0"
RDEPENDS_pygobject2 = "glib2 glibc platform-python"
RDEPENDS_pygobject2-codegen = "bash"
RPROVIDES_pygobject2-devel = "pygobject2-dev (= 2.28.7)"
RDEPENDS_pygobject2-devel = "glib2-devel pkgconf-pkg-config pygobject2 pygobject2-codegen pygobject2-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pygobject2-2.28.7-4.module_el8.0.0+36+bb6a76a2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pygobject2-codegen-2.28.7-4.module_el8.0.0+36+bb6a76a2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pygobject2-devel-2.28.7-4.module_el8.0.0+36+bb6a76a2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pygobject2-doc-2.28.7-4.module_el8.0.0+36+bb6a76a2.x86_64.rpm \
          "

SRC_URI[pygobject2.sha256sum] = "443ef0b9be7e578ec72a5590fe47680cdb3bbb776500817f623055471bcaf4b8"
SRC_URI[pygobject2-codegen.sha256sum] = "066c947fe106dd32438d2d9f0a0e7f2ebdb61dfab97348bffd1b79b42a0ae3fa"
SRC_URI[pygobject2-devel.sha256sum] = "0a1042a6ffe606610384436a409e0cf6bc76718f3e7d01f8f6788f75b1931412"
SRC_URI[pygobject2-doc.sha256sum] = "f66b5eb4893f32f9adbe00a903a08758f0039f05b9a0418307ea67155cb55b24"
