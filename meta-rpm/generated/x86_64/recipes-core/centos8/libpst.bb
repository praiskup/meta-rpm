SUMMARY = "generated recipe based on libpst srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpst-libs = "libpst.so.4"
RPM_SONAME_REQ_libpst-libs = "libc.so.6 libpthread.so.0"
RDEPENDS_libpst-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpst-libs-0.6.71-8.el8.x86_64.rpm \
          "

SRC_URI[libpst-libs.sha256sum] = "a8dd98011590535cad33f0779eb200c8a399fa1aaff25e37447960d9748f9729"
