SUMMARY = "generated recipe based on libXaw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext libxmu libxpm libxt pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXaw = "libXaw.so.7"
RPM_SONAME_REQ_libXaw = "libX11.so.6 libXext.so.6 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6"
RDEPENDS_libXaw = "glibc libX11 libXext libXmu libXpm libXt"
RPM_SONAME_REQ_libXaw-devel = "libXaw.so.7"
RPROVIDES_libXaw-devel = "libXaw-dev (= 1.0.13)"
RDEPENDS_libXaw-devel = "libX11-devel libXaw libXext-devel libXmu-devel libXpm-devel libXt-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXaw-1.0.13-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXaw-devel-1.0.13-10.el8.x86_64.rpm \
          "

SRC_URI[libXaw.sha256sum] = "d82ca1d1d9aea848d05ac0ffe889f921a19f37883ae1cf6ba1ca0528e2ab46e4"
SRC_URI[libXaw-devel.sha256sum] = "bcdf22c7efddd6593be5d315762ced7877f79c8be9766a012d7ffcf83f0e9d76"
