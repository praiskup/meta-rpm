SUMMARY = "generated recipe based on uuid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_uuid = "libossp-uuid.so.16"
RPM_SONAME_REQ_uuid = "libc.so.6 libossp-uuid.so.16"
RDEPENDS_uuid = "glibc"
RPM_SONAME_REQ_uuid-devel = "libossp-uuid.so.16"
RPROVIDES_uuid-devel = "uuid-dev (= 1.6.2)"
RDEPENDS_uuid-devel = "bash pkgconf-pkg-config uuid"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/uuid-1.6.2-42.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/uuid-devel-1.6.2-42.el8.x86_64.rpm \
          "

SRC_URI[uuid.sha256sum] = "43efc75b14e094573760345535c97ea5eea633e6c8be3c3ad26684d5575d2455"
SRC_URI[uuid-devel.sha256sum] = "71a63dcee49a35a52287870a3cbd156da3265650883a1a1355046d023276ab58"
