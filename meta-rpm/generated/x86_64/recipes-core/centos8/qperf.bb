SUMMARY = "generated recipe based on qperf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native rdma-core"
RPM_SONAME_REQ_qperf = "libc.so.6 libibverbs.so.1 librdmacm.so.1"
RDEPENDS_qperf = "glibc libibverbs librdmacm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qperf-0.4.11-1.el8.x86_64.rpm \
          "

SRC_URI[qperf.sha256sum] = "7a313574ac2feaaa785fbe4541dfb0895ca8a094d39e9bc1677d5d0ad2165965"
