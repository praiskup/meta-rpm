SUMMARY = "generated recipe based on brotli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_brotli = "libbrotlicommon.so.1 libbrotlidec.so.1 libbrotlienc.so.1"
RPM_SONAME_REQ_brotli = "libbrotlicommon.so.1 libc.so.6 libm.so.6"
RDEPENDS_brotli = "glibc"
RPM_SONAME_REQ_brotli-devel = "libbrotlicommon.so.1 libbrotlidec.so.1 libbrotlienc.so.1"
RPROVIDES_brotli-devel = "brotli-dev (= 1.0.6)"
RDEPENDS_brotli-devel = "brotli pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brotli-devel-1.0.6-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/brotli-1.0.6-1.el8.x86_64.rpm \
          "

SRC_URI[brotli.sha256sum] = "0628f6d4372fdb65824c43847bf1147338cf8c8d55815f4c542e7761535f54eb"
SRC_URI[brotli-devel.sha256sum] = "7be90a32ce725955dae9410105369521face5a29df0415f50aa29f90434db061"
