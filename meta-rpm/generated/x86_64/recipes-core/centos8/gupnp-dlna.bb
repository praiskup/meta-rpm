SUMMARY = "generated recipe based on gupnp-dlna srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libxml2 pkgconfig-native"
RPM_SONAME_PROV_gupnp-dlna = "libgstreamer.so libgupnp-dlna-2.0.so.3 libgupnp-dlna-gst-2.0.so.3"
RPM_SONAME_REQ_gupnp-dlna = "libc.so.6 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgupnp-dlna-2.0.so.3 libgupnp-dlna-gst-2.0.so.3 libpthread.so.0 libxml2.so.2"
RDEPENDS_gupnp-dlna = "glib2 glibc gstreamer1 gstreamer1-plugins-base libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gupnp-dlna-0.10.5-9.el8.x86_64.rpm \
          "

SRC_URI[gupnp-dlna.sha256sum] = "e123feca59e68c1d22f8151d38d439e0e5f75a4ab531c04ef4c34bfe5c2a9fa0"
