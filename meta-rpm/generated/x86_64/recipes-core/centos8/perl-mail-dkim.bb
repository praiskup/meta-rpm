SUMMARY = "generated recipe based on perl-Mail-DKIM srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Mail-DKIM = "perl-Carp perl-Crypt-OpenSSL-RSA perl-Digest-SHA perl-MIME-Base64 perl-MailTools perl-Net-DNS perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Mail-DKIM-0.54-1.el8.noarch.rpm \
          "

SRC_URI[perl-Mail-DKIM.sha256sum] = "3f4dda55681fc7abe5a2d8362945bd290c3bb47b7fef169f84a78e58e7a5bb0f"
