SUMMARY = "generated recipe based on libstaroffice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge pkgconfig-native zlib"
RPM_SONAME_PROV_libstaroffice = "libstaroffice-0.0.so.0"
RPM_SONAME_REQ_libstaroffice = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_libstaroffice = "glibc libgcc librevenge libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libstaroffice-0.0.6-1.el8.x86_64.rpm \
          "

SRC_URI[libstaroffice.sha256sum] = "6bc5a3b9b3b05eb6579f48b138803af76d2115128f19bb26b430c1d482ce2fc8"
