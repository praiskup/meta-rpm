SUMMARY = "generated recipe based on glade srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gdk-pixbuf glib-2.0 gtk+3 libxml2 pango pkgconfig-native platform-python3 webkit2gtk3"
RPM_SONAME_REQ_glade-devel = "libgladeui-2.so.6"
RPROVIDES_glade-devel = "glade-dev (= 3.22.1)"
RDEPENDS_glade-devel = "glade-libs gtk3-devel libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_glade-libs = "libgladegtk.so libgladepython.so libgladeui-2.so.6 libgladewebkit2gtk.so"
RPM_SONAME_REQ_glade-libs = "libc.so.6 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgladeui-2.so.6 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libpython3.6m.so.1.0 libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_glade-libs = "cairo gdk-pixbuf2 glib2 glibc gtk3 libxml2 pango python3-libs webkit2gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glade-libs-3.22.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glade-devel-3.22.1-1.el8.x86_64.rpm \
          "

SRC_URI[glade-devel.sha256sum] = "6937b1af6c3fad48c7b9617dd78f8c94eb693403c9b1e0978f33f91a708cda89"
SRC_URI[glade-libs.sha256sum] = "e62c4a94a6312d6940f8f749d3975a3e86e01cbca66007bc5459615218e966f5"
