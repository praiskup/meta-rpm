SUMMARY = "generated recipe based on gedit-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gobject-introspection gtk+3 gtksourceview3 libpeas pango pkgconfig-native"
RPM_SONAME_PROV_gedit-plugin-bookmarks = "libbookmarks.so"
RPM_SONAME_REQ_gedit-plugin-bookmarks = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-bookmarks = "atk cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RDEPENDS_gedit-plugin-bracketcompletion = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-codecomment = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-colorpicker = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-colorschemer = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-commander = "gedit-plugins-data libpeas-loader-python3"
RPM_SONAME_PROV_gedit-plugin-drawspaces = "libdrawspaces.so"
RPM_SONAME_REQ_gedit-plugin-drawspaces = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-drawspaces = "atk bash cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RPM_SONAME_PROV_gedit-plugin-findinfiles = "libfindinfiles.so"
RPM_SONAME_REQ_gedit-plugin-findinfiles = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-findinfiles = "atk cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RDEPENDS_gedit-plugin-joinlines = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-multiedit = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-smartspaces = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-terminal = "bash gedit-plugins-data libpeas-loader-python3 vte291"
RDEPENDS_gedit-plugin-textsize = "gedit-plugins-data libpeas-loader-python3"
RDEPENDS_gedit-plugin-translate = "gedit-plugins-data libpeas-loader-python3"
RPM_SONAME_PROV_gedit-plugin-wordcompletion = "libwordcompletion.so"
RPM_SONAME_REQ_gedit-plugin-wordcompletion = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RDEPENDS_gedit-plugin-wordcompletion = "atk bash cairo cairo-gobject gdk-pixbuf2 gedit-plugins-data glib2 glibc gobject-introspection gtk3 gtksourceview3 libpeas libpeas-gtk pango"
RDEPENDS_gedit-plugins = "gedit-plugin-bookmarks gedit-plugin-bracketcompletion gedit-plugin-codecomment gedit-plugin-colorpicker gedit-plugin-colorschemer gedit-plugin-commander gedit-plugin-drawspaces gedit-plugin-findinfiles gedit-plugin-joinlines gedit-plugin-multiedit gedit-plugin-smartspaces gedit-plugin-terminal gedit-plugin-textsize gedit-plugin-translate gedit-plugin-wordcompletion"
RDEPENDS_gedit-plugins-data = "gedit python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-bookmarks-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-bracketcompletion-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-codecomment-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-colorpicker-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-colorschemer-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-commander-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-drawspaces-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-findinfiles-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-joinlines-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-multiedit-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-smartspaces-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-terminal-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-textsize-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-translate-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugin-wordcompletion-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugins-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-plugins-data-3.28.1-8.el8.x86_64.rpm \
          "

SRC_URI[gedit-plugin-bookmarks.sha256sum] = "c58e69be88d89649b50bcf5817af7f7904ccf21779c7b0488ae05c1f187b06b9"
SRC_URI[gedit-plugin-bracketcompletion.sha256sum] = "929c018d3ca5f362f55ee83707bc761365b29aadeedbe7b46ef7f264720d4b2f"
SRC_URI[gedit-plugin-codecomment.sha256sum] = "d98ebf269086fb49050dc784044164b92d0c07ce26bbf000d482833f79581daf"
SRC_URI[gedit-plugin-colorpicker.sha256sum] = "396ef2fb34f2f02a1fcea8ac2ac7e75f8eca2d6869907e8aaea1cbc8b5daf69c"
SRC_URI[gedit-plugin-colorschemer.sha256sum] = "0dd74a5e1ab16b5c78840f7a30c6b80a158277a9c7f3b4a5e17ac12321350894"
SRC_URI[gedit-plugin-commander.sha256sum] = "6020b18e943183e8e6c5af213a354836add09d5e257435b1476d7f7bae07e486"
SRC_URI[gedit-plugin-drawspaces.sha256sum] = "b96b1bc6cc877447a72ad32bdd3a4f2febb29f318237fc3b533c967c3d5fd551"
SRC_URI[gedit-plugin-findinfiles.sha256sum] = "3bd86a548b3ac52820aaf53cb7e48fd5362f00153f2ba39f033539d9962527f0"
SRC_URI[gedit-plugin-joinlines.sha256sum] = "77130ec539378fb31ba546e3b9fd9f5bfbdcbfdc9d5d2ba8bf59b116c8b1d4b8"
SRC_URI[gedit-plugin-multiedit.sha256sum] = "0bf33b81a59068db661d2bc16a5a38d3203ef8e2d66715d553088e2f14a0036d"
SRC_URI[gedit-plugin-smartspaces.sha256sum] = "35fe9c63b13d7dcf2e4b8c8281c375fdc484d6950f36ef57f7c301b2fccd22db"
SRC_URI[gedit-plugin-terminal.sha256sum] = "1c90f0db678ede415cbe0cef20c69857b73773a494b98078b4c36455c8bb853b"
SRC_URI[gedit-plugin-textsize.sha256sum] = "f3ad9199256293108faf71800727c8c22e1d175b034701604c102a64d90846b4"
SRC_URI[gedit-plugin-translate.sha256sum] = "5a1a31192fd264530e3031a67b6df228848f2012c0bd4ed969a5220bbbe41300"
SRC_URI[gedit-plugin-wordcompletion.sha256sum] = "04860d6899a967879d02289e979dc771e818895fb2b51429de7bce9cc9d72842"
SRC_URI[gedit-plugins.sha256sum] = "95fb93bd3f5c51389c092d061c4e9536c825d26e3e6dfdec8af8e8881b21240e"
SRC_URI[gedit-plugins-data.sha256sum] = "3a1ead123777fe8e5523e12020018b56962dad8162fb66d1f5793bdb1e5027ea"
