SUMMARY = "generated recipe based on openwsman srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc libxcrypt libxml2 openssl pam pkgconfig-native ruby sblim-sfcc"
RPM_SONAME_REQ_libwsman-devel = "libwsman.so.1 libwsman_client.so.4 libwsman_clientpp.so.1 libwsman_curl_client_transport.so.1 libwsman_server.so.1"
RPROVIDES_libwsman-devel = "libwsman-dev (= 2.6.5)"
RDEPENDS_libwsman-devel = "libcurl-devel libwsman1 libxml2-devel openwsman-client openwsman-server pam-devel pkgconf-pkg-config sblim-sfcc-devel"
RPM_SONAME_PROV_libwsman1 = "libwsman.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1"
RPM_SONAME_REQ_libwsman1 = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libpthread.so.0 libssl.so.1.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1 libxml2.so.2"
RDEPENDS_libwsman1 = "glibc libcurl libxml2 openssl-libs"
RPM_SONAME_PROV_openwsman-client = "libwsman_clientpp.so.1"
RPM_SONAME_REQ_openwsman-client = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_openwsman-client = "glibc libgcc libstdc++"
RPM_SONAME_REQ_openwsman-python3 = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libpthread.so.0 libssl.so.1.1 libwsman.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1 libxml2.so.2"
RDEPENDS_openwsman-python3 = "glibc libcurl libwsman1 libxml2 openssl-libs platform-python"
RPM_SONAME_PROV_openwsman-server = "libredirect.so.1 libwsman_cim_plugin.so.1 libwsman_file_auth.so.1 libwsman_identify_plugin.so.1 libwsman_pam_auth.so.1 libwsman_ruby_plugin.so libwsman_server.so.1 libwsman_test.so.1"
RPM_SONAME_REQ_openwsman-server = "libc.so.6 libcmpisfcc.so.1 libcrypt.so.1 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libpam.so.0 libpthread.so.0 libredirect.so.1 libruby.so.2.5 libssl.so.1.1 libwsman.so.1 libwsman_cim_plugin.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1 libwsman_file_auth.so.1 libwsman_identify_plugin.so.1 libwsman_pam_auth.so.1 libwsman_server.so.1 libwsman_test.so.1 libxml2.so.2"
RDEPENDS_openwsman-server = "bash glibc libcurl libwsman1 libxcrypt libxml2 openssl-libs pam ruby-libs sblim-sfcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwsman1-2.6.5-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openwsman-client-2.6.5-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openwsman-python3-2.6.5-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openwsman-server-2.6.5-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwsman-devel-2.6.5-5.el8.x86_64.rpm \
          "

SRC_URI[libwsman-devel.sha256sum] = "e06dfb4d8ad598b35edfdb0191173c6b2732e1503b36ea83dc64aeabebe97cfb"
SRC_URI[libwsman1.sha256sum] = "2cc800611a2c5bc668989f9a13e0f04b3a6954bc456aa710e11f7dd0bb4fac92"
SRC_URI[openwsman-client.sha256sum] = "eb75dcebc991286d2a04422cd97988c56caea2080918e8f610aa5d28b3757b05"
SRC_URI[openwsman-python3.sha256sum] = "df8ef801029aca4e3678bb03cd5d2bfd42268dadf232b4a35faf4516a8650f89"
SRC_URI[openwsman-server.sha256sum] = "aed1e258b2771c89d4d05361d827d453097b946e90c49ffa2a6b30d5bc9c3c31"
