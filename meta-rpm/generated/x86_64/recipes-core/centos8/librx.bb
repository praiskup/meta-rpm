SUMMARY = "generated recipe based on librx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_librx = "librx.so.0"
RPM_SONAME_REQ_librx = "libc.so.6"
RDEPENDS_librx = "glibc"
RPM_SONAME_REQ_librx-devel = "librx.so.0"
RPROVIDES_librx-devel = "librx-dev (= 1.5)"
RDEPENDS_librx-devel = "bash librx"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librx-1.5-31.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librx-devel-1.5-31.el8.x86_64.rpm \
          "

SRC_URI[librx.sha256sum] = "8d2631494a0795f1de487b3480422e3043b7f99be6af09b1e82a1ec5f280027f"
SRC_URI[librx-devel.sha256sum] = "ee37386b4ad580202031b7f1cc3595fd1464fca2eb2a603530b87da1eac23482"
