SUMMARY = "generated recipe based on libvisual srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libvisual = "libvisual-0.4.so.0"
RPM_SONAME_REQ_libvisual = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_libvisual = "glibc"
RPM_SONAME_REQ_libvisual-devel = "libvisual-0.4.so.0"
RPROVIDES_libvisual-devel = "libvisual-dev (= 0.4.0)"
RDEPENDS_libvisual-devel = "libvisual pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvisual-0.4.0-24.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvisual-devel-0.4.0-24.el8.x86_64.rpm \
          "

SRC_URI[libvisual.sha256sum] = "e720949fb94c853b19d4835e502e258a34c675b530b8d631694b051808a3af15"
SRC_URI[libvisual-devel.sha256sum] = "ae6dc9220b2199ed65e14cfcf9a7e059b9904146afa1a7800a2771f3ae8c64e7"
