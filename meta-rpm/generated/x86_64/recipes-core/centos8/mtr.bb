SUMMARY = "generated recipe based on mtr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 ncurses pango pkgconfig-native"
RPM_SONAME_REQ_mtr = "libc.so.6 libm.so.6 libncurses.so.6 libresolv.so.2 libtinfo.so.6"
RDEPENDS_mtr = "glibc ncurses-libs"
RPM_SONAME_REQ_mtr-gtk = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libncurses.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libresolv.so.2 libtinfo.so.6"
RDEPENDS_mtr-gtk = "atk bash cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 mtr ncurses-libs pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mtr-gtk-0.92-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mtr-0.92-3.el8.x86_64.rpm \
          "

SRC_URI[mtr.sha256sum] = "753b00c518408ecdda38341e53b59197c42fb202eaf410666d0cf2700274d134"
SRC_URI[mtr-gtk.sha256sum] = "9932af1e02810937de64118551b23c92de5d3366223c0baa94686d9995f67123"
