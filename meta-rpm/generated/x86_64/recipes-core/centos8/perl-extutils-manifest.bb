SUMMARY = "generated recipe based on perl-ExtUtils-Manifest srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-ExtUtils-Manifest = "perl-Carp perl-Exporter perl-File-Path perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-ExtUtils-Manifest-1.70-395.el8.noarch.rpm \
          "

SRC_URI[perl-ExtUtils-Manifest.sha256sum] = "0d68e2a1bc4177b153ec0e06664a0bf48935b655cbce2f1c94e524582bfc1ab4"
