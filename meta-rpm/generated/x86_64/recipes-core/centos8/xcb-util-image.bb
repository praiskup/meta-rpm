SUMMARY = "generated recipe based on xcb-util-image srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxcb pkgconfig-native xcb-util"
RPM_SONAME_PROV_xcb-util-image = "libxcb-image.so.0"
RPM_SONAME_REQ_xcb-util-image = "libc.so.6 libxcb-shm.so.0 libxcb-util.so.1 libxcb.so.1"
RDEPENDS_xcb-util-image = "glibc libxcb xcb-util"
RPM_SONAME_REQ_xcb-util-image-devel = "libxcb-image.so.0"
RPROVIDES_xcb-util-image-devel = "xcb-util-image-dev (= 0.4.0)"
RDEPENDS_xcb-util-image-devel = "libxcb-devel pkgconf-pkg-config xcb-util-image"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xcb-util-image-0.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xcb-util-image-devel-0.4.0-9.el8.x86_64.rpm \
          "

SRC_URI[xcb-util-image.sha256sum] = "cf9da1ca86745a7c6f61c469ef686491cc14281725b9debebe4840bdbe0069f0"
SRC_URI[xcb-util-image-devel.sha256sum] = "6b3314fed00e97de90548f7cb004e1129dfec2abf5b3b505facb29b5b9638c46"
