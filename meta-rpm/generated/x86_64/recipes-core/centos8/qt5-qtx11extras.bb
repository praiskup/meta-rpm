SUMMARY = "generated recipe based on qt5-qtx11extras srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qt5-qtx11extras = "libQt5X11Extras.so.5"
RPM_SONAME_REQ_qt5-qtx11extras = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtx11extras = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_qt5-qtx11extras-devel = "libQt5X11Extras.so.5"
RPROVIDES_qt5-qtx11extras-devel = "qt5-qtx11extras-dev (= 5.12.5)"
RDEPENDS_qt5-qtx11extras-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtx11extras"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtx11extras-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtx11extras-devel-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtx11extras.sha256sum] = "6994e210149d07cfe9ab0dc771fdfdd0c632b55ef83193ec9c34534aa3801daa"
SRC_URI[qt5-qtx11extras-devel.sha256sum] = "557e5d5ba5438b935dfca7a916ed44c8d4b94d5f5d9086bbac7801a41cb4836e"
