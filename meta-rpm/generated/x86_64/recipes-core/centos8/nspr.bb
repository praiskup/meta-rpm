SUMMARY = "generated recipe based on nspr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_nspr = "libnspr4.so libplc4.so libplds4.so"
RPM_SONAME_REQ_nspr = "libc.so.6 libdl.so.2 libnspr4.so libpthread.so.0 librt.so.1"
RDEPENDS_nspr = "glibc"
RPROVIDES_nspr-devel = "nspr-dev (= 4.25.0)"
RDEPENDS_nspr-devel = "bash nspr pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nspr-4.25.0-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nspr-devel-4.25.0-2.el8_2.x86_64.rpm \
          "

SRC_URI[nspr.sha256sum] = "34321e09964f724b712ed709115f794b1953f4a4d472d655bec0993d27c51a22"
SRC_URI[nspr-devel.sha256sum] = "9fc60a950b8a0a824ede4ee186a5ed731621be7d80b5d9bf679fe60058604297"
