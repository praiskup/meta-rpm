SUMMARY = "generated recipe based on hivex srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxml2 perl pkgconfig-native readline ruby"
RPM_SONAME_PROV_hivex = "libhivex.so.0"
RPM_SONAME_REQ_hivex = "libc.so.6 libhivex.so.0 libreadline.so.7 libxml2.so.2"
RDEPENDS_hivex = "bash glibc libxml2 readline"
RPM_SONAME_REQ_hivex-devel = "libhivex.so.0"
RPROVIDES_hivex-devel = "hivex-dev (= 1.3.15)"
RDEPENDS_hivex-devel = "hivex pkgconf-pkg-config"
RPM_SONAME_REQ_ocaml-hivex = "libc.so.6 libhivex.so.0"
RDEPENDS_ocaml-hivex = "glibc hivex ocaml-runtime"
RPROVIDES_ocaml-hivex-devel = "ocaml-hivex-dev (= 1.3.15)"
RDEPENDS_ocaml-hivex-devel = "hivex-devel ocaml-hivex"
RPM_SONAME_REQ_perl-hivex = "libc.so.6 libhivex.so.0 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-hivex = "glibc hivex perl-Carp perl-Encode perl-Exporter perl-Getopt-Long perl-Pod-Usage perl-interpreter perl-libs"
RPM_SONAME_PROV_python3-hivex = "libhivexmod.cpython-36m-x86_64-linux-gnu.so"
RPM_SONAME_REQ_python3-hivex = "libc.so.6 libhivex.so.0"
RDEPENDS_python3-hivex = "glibc hivex platform-python"
RPM_SONAME_REQ_ruby-hivex = "libc.so.6 libhivex.so.0 libruby.so.2.5"
RDEPENDS_ruby-hivex = "glibc hivex ruby ruby-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hivex-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hivex-devel-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ruby-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-hivex-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-hivex-devel-1.3.15-7.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[hivex.sha256sum] = "1f04e7d10cb29c1f37c5105e959a1b401dad675c610f95f7a2cd41a6cc4bb88e"
SRC_URI[hivex-devel.sha256sum] = "d924bb1e58919319d9d778bd01ad1b49b8e68bec44cff47babd17ff34b4693e5"
SRC_URI[ocaml-hivex.sha256sum] = "8c36ea6f0f6779898a810d1355761168717f4b7ddd06a2ac3f0ba6d5f79687d6"
SRC_URI[ocaml-hivex-devel.sha256sum] = "dcbcb7e54b2e2653dca4c08a0ff01a52192761d2a988f79725aad8be957f7ad3"
SRC_URI[perl-hivex.sha256sum] = "9864982052d9dede6c4602fe3bb39b7ca7699a0c2ac00939236bd832f28e3f0c"
SRC_URI[python3-hivex.sha256sum] = "c7bfc3605968ffb92d355251c00ac4137e19e066a07f48bc14c3d20462983f70"
SRC_URI[ruby-hivex.sha256sum] = "aec0b19fb086f7f0adbc1b26f14bf517c3ebb40750b1987c7f95fdc09d01495e"
