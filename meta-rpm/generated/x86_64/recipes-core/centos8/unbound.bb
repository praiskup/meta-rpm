SUMMARY = "generated recipe based on unbound srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libevent openssl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-unbound = "libc.so.6 libcrypto.so.1.1 libevent-2.1.so.6 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1 libunbound.so.2"
RDEPENDS_python3-unbound = "glibc libevent openssl-libs platform-python python3-libs unbound-libs"
RPM_SONAME_REQ_unbound = "libc.so.6 libcrypto.so.1.1 libevent-2.1.so.6 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1 libunbound.so.2"
RDEPENDS_unbound = "bash glibc libevent openssl-libs python3-libs systemd unbound-libs"
RPM_SONAME_REQ_unbound-devel = "libunbound.so.2"
RPROVIDES_unbound-devel = "unbound-dev (= 1.7.3)"
RDEPENDS_unbound-devel = "libevent-devel openssl-devel pkgconf-pkg-config platform-python-devel unbound-libs"
RPM_SONAME_PROV_unbound-libs = "libunbound.so.2"
RPM_SONAME_REQ_unbound-libs = "libc.so.6 libcrypto.so.1.1 libevent-2.1.so.6 libexpat.so.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1 libunbound.so.2"
RDEPENDS_unbound-libs = "bash expat glibc libevent openssl-libs python3-libs shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-unbound-1.7.3-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/unbound-1.7.3-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/unbound-devel-1.7.3-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/unbound-libs-1.7.3-11.el8_2.x86_64.rpm \
          "

SRC_URI[python3-unbound.sha256sum] = "4a76a279f41b5b91ee7bdb815aaa532aed217a63141159f6b95ba94ab9c98a78"
SRC_URI[unbound.sha256sum] = "2a1e9d2a5a4a4a0a7800c7aa4dd1dff03485772b7ba5982cde394b13cf9b071d"
SRC_URI[unbound-devel.sha256sum] = "473d8b23578545793c69718f8581235bf839e303cc2ab26f0c779e150a67d575"
SRC_URI[unbound-libs.sha256sum] = "6c643acdc61a59c359c2a24da2616ff1f7cd8a23e61d26ddec7392876d7066a5"
