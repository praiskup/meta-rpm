SUMMARY = "generated recipe based on perl-namespace-clean srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-namespace-clean = "perl-B-Hooks-EndOfScope perl-Exporter perl-Package-Stash perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-namespace-clean-0.27-7.el8.noarch.rpm \
          "

SRC_URI[perl-namespace-clean.sha256sum] = "34ef0eed4638ecca53e07c51b2c366e8fe40f2936410ae537fb1c5796a4a4471"
