SUMMARY = "generated recipe based on llvm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libffi libgcc ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_llvm = "ld-linux-x86-64.so.2 libLLVM-9.so libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_llvm = "glibc libgcc libstdc++ llvm-libs ncurses-libs platform-python zlib"
RPM_SONAME_REQ_llvm-devel = "libLLVM-9.so libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_llvm-devel = "llvm-dev (= 9.0.1)"
RDEPENDS_llvm-devel = "bash chkconfig cmake-filesystem glibc libgcc libstdc++ llvm llvm-libs"
RDEPENDS_llvm-doc = "llvm"
RPM_SONAME_PROV_llvm-libs = "libLLVM-9.so libLTO.so.9 libRemarks.so.9"
RPM_SONAME_REQ_llvm-libs = "ld-linux-x86-64.so.2 libLLVM-9.so libLTO.so.9 libRemarks.so.9 libc.so.6 libdl.so.2 libffi.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_llvm-libs = "glibc libffi libgcc libstdc++ ncurses-libs zlib"
RPM_SONAME_REQ_llvm-test = "ld-linux-x86-64.so.2 libLLVM-9.so libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_llvm-test = "bash binutils findutils gcc glibc libgcc libstdc++ llvm llvm-devel llvm-libs ncurses-libs python3-lit zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-9.0.1-5.module_el8.2.0+461+2e15bd5f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-devel-9.0.1-5.module_el8.2.0+461+2e15bd5f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-doc-9.0.1-5.module_el8.2.0+461+2e15bd5f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-googletest-9.0.1-5.module_el8.2.0+461+2e15bd5f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-libs-9.0.1-5.module_el8.2.0+461+2e15bd5f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-static-9.0.1-5.module_el8.2.0+461+2e15bd5f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/llvm-test-9.0.1-5.module_el8.2.0+461+2e15bd5f.x86_64.rpm \
          "

SRC_URI[llvm.sha256sum] = "cd6ee4e18e5c4b26c89792bb8275960eb212a61e89117f48819e94f16de3f2cc"
SRC_URI[llvm-devel.sha256sum] = "99a8b15fa2bdb842521e784aef2a095582eff486db6c51161121d9770ca6c684"
SRC_URI[llvm-doc.sha256sum] = "078a9ed150a50fa0bb2716611614c6fd85a737ccbf845879d1f417deee12fa59"
SRC_URI[llvm-googletest.sha256sum] = "90322657b53f13ce642853178e507630867e33edbc2e090b31ad54522bfe2b57"
SRC_URI[llvm-libs.sha256sum] = "13726c393667985d038af004c778533f518aa453896a993315ad55347bd6fc2f"
SRC_URI[llvm-static.sha256sum] = "9e8bbf89091206d8deb3f1c3227f9079aec5de1edbbfeed0567b717253707ab3"
SRC_URI[llvm-test.sha256sum] = "2b301d088a94615ed6bce386c3df0a8c6e56ac1cdf07e200732f02134faf5ff2"
