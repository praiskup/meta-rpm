SUMMARY = "generated recipe based on maven-remote-resources-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-remote-resources-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-artifact-resolver maven-common-artifact-filters maven-filtering maven-lib maven-model maven-monitor maven-project maven-settings plexus-interpolation plexus-resources plexus-utils velocity"
RDEPENDS_maven-remote-resources-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-remote-resources-plugin-1.5-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-remote-resources-plugin-javadoc-1.5-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-remote-resources-plugin.sha256sum] = "3744a3f89e095365153b3b0ce4156ef00ad174dc84adfdd9e5585d0e89063e03"
SRC_URI[maven-remote-resources-plugin-javadoc.sha256sum] = "3a4159220cc7c2a5773da4671c932f9e54ac46c9b4d6b8b2124732d1fa79e04a"
