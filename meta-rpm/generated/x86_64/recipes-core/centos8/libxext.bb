SUMMARY = "generated recipe based on libXext srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xext"
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXext = "libXext.so.6"
RPM_SONAME_REQ_libXext = "libX11.so.6 libc.so.6"
RDEPENDS_libXext = "glibc libX11"
RPM_SONAME_REQ_libXext-devel = "libXext.so.6"
RPROVIDES_libXext-devel = "libXext-dev (= 1.3.3)"
RDEPENDS_libXext-devel = "libX11-devel libXext pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXext-1.3.3-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXext-devel-1.3.3-9.el8.x86_64.rpm \
          "

SRC_URI[libXext.sha256sum] = "bf2034b0c758ede4ec4dba35008845951bd18df643d75fc34aa536cdee66eb8b"
SRC_URI[libXext-devel.sha256sum] = "870104787e9fe9a05e8b81d62b662046e11be0478a51726a22fbf22315c063f9"
