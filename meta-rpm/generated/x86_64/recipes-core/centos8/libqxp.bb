SUMMARY = "generated recipe based on libqxp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libqxp = "libqxp-0.0.so.0"
RPM_SONAME_REQ_libqxp = "libc.so.6 libgcc_s.so.1 libicudata.so.60 libicuuc.so.60 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libqxp = "glibc libgcc libicu librevenge libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libqxp-0.0.1-2.el8.x86_64.rpm \
          "

SRC_URI[libqxp.sha256sum] = "7da3bbb808c1eafbbb1006b1f18adfd6423ccb5ff6b564d3d8491c1c279e2abf"
