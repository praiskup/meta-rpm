SUMMARY = "generated recipe based on python-dns srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-dns = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-dns-1.15.0-10.el8.noarch.rpm \
          "

SRC_URI[python3-dns.sha256sum] = "b248646dc953587300a2134fc489821a5b9b1d299e8b7dea382c78b18a9ffe51"
