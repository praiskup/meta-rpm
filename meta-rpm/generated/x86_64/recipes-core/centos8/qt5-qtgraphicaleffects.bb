SUMMARY = "generated recipe based on qt5-qtgraphicaleffects srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtgraphicaleffects = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtgraphicaleffects = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtgraphicaleffects-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtgraphicaleffects.sha256sum] = "56f4c12e9698c08df6ffa0fb125590de534e7055ff9359aff3b371a4a9c2f4ac"
