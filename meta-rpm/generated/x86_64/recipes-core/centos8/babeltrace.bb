SUMMARY = "generated recipe based on babeltrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils glib-2.0 libuuid pkgconfig-native popt"
RPM_SONAME_PROV_libbabeltrace = "libbabeltrace-ctf-metadata.so.1 libbabeltrace-ctf-text.so.1 libbabeltrace-ctf.so.1 libbabeltrace-dummy.so.1 libbabeltrace-lttng-live.so.1 libbabeltrace.so.1"
RPM_SONAME_REQ_libbabeltrace = "ld-linux-x86-64.so.2 libbabeltrace-ctf.so.1 libbabeltrace.so.1 libc.so.6 libdw.so.1 libelf.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libpopt.so.0 libpthread.so.0 libuuid.so.1"
RDEPENDS_libbabeltrace = "elfutils-libelf elfutils-libs glib2 glibc libuuid popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libbabeltrace-1.5.4-2.el8.x86_64.rpm \
          "

SRC_URI[libbabeltrace.sha256sum] = "fa5d9f9bddee88b225d7ed922c2dd67f234014624c24df4b8cddd40ed49ff099"
