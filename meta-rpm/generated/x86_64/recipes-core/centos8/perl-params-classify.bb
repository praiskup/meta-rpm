SUMMARY = "generated recipe based on perl-Params-Classify srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Classify = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Classify = "glibc perl-Devel-CallChecker perl-Exporter perl-Scalar-List-Utils perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Params-Classify-0.015-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Params-Classify.sha256sum] = "e4b46a1683a3d9171e4daaebee74b4a4a7da2a527ea92f1e4f57cce6cd88955d"
