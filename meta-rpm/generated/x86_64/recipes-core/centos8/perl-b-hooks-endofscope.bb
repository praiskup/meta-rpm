SUMMARY = "generated recipe based on perl-B-Hooks-EndOfScope srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-B-Hooks-EndOfScope = "perl-Module-Implementation perl-Scalar-List-Utils perl-Sub-Exporter-Progressive perl-Variable-Magic perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-B-Hooks-EndOfScope-0.21-6.el8.noarch.rpm \
          "

SRC_URI[perl-B-Hooks-EndOfScope.sha256sum] = "562acf53b8221835479b48bbb2f517a8a5002b5553569233b217692a3918f868"
