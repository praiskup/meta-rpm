SUMMARY = "generated recipe based on nftables srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp iptables jansson libmnl libnftnl pkgconfig-native readline"
RPM_SONAME_PROV_nftables = "libnftables.so.1"
RPM_SONAME_REQ_nftables = "libc.so.6 libgmp.so.10 libjansson.so.4 libmnl.so.0 libnftables.so.1 libnftnl.so.11 libreadline.so.7 libxtables.so.12"
RDEPENDS_nftables = "bash glibc gmp iptables-libs jansson libmnl libnftnl readline"
RDEPENDS_python3-nftables = "nftables platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nftables-0.9.3-12.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-nftables-0.9.3-12.el8_2.1.x86_64.rpm \
          "

SRC_URI[nftables.sha256sum] = "60596774bd5582ed4058191ece8ef94a8a9ec270d44d0b55445544669a38b9e4"
SRC_URI[python3-nftables.sha256sum] = "c3e9aafc183e59a855dd7b634de2a67e19bcf16af0c0625b3ebede139cac1181"
