SUMMARY = "generated recipe based on babl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_babl = "libbabl-0.1.so.0"
RPM_SONAME_REQ_babl = "ld-linux-x86-64.so.2 libbabl-0.1.so.0 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_babl = "glibc"
RPM_SONAME_REQ_babl-devel = "libbabl-0.1.so.0"
RPROVIDES_babl-devel = "babl-dev (= 0.1.52)"
RDEPENDS_babl-devel = "babl pkgconf-pkg-config"
RDEPENDS_babl-devel-docs = "babl-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/babl-0.1.52-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/babl-devel-0.1.52-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/babl-devel-docs-0.1.52-1.el8.noarch.rpm \
          "

SRC_URI[babl.sha256sum] = "4326d2ff396039341b14d0e47a398de4543a03d0cacfc980769302c2de3a6dd0"
SRC_URI[babl-devel.sha256sum] = "3d0245b956c15f1476f14bac5cc45bdc576ae8fe870afe7f1e41813171eebec5"
SRC_URI[babl-devel-docs.sha256sum] = "5991ce2d0dbc7e0f3dd5c3add25d44083733b724264446e7a7c4974b88f28940"
