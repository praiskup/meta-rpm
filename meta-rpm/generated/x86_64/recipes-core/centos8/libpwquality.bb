SUMMARY = "generated recipe based on libpwquality srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cracklib pam pkgconfig-native platform-python3"
RPM_SONAME_PROV_libpwquality = "libpwquality.so.1"
RPM_SONAME_REQ_libpwquality = "libc.so.6 libcrack.so.2 libpam.so.0 libpwquality.so.1"
RDEPENDS_libpwquality = "cracklib glibc pam"
RPM_SONAME_REQ_libpwquality-devel = "libpwquality.so.1"
RPROVIDES_libpwquality-devel = "libpwquality-dev (= 1.4.0)"
RDEPENDS_libpwquality-devel = "libpwquality pkgconf-pkg-config"
RPM_SONAME_REQ_python3-pwquality = "libc.so.6 libpthread.so.0 libpwquality.so.1 libpython3.6m.so.1.0"
RDEPENDS_python3-pwquality = "glibc libpwquality platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpwquality-1.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pwquality-1.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpwquality-devel-1.4.0-9.el8.x86_64.rpm \
          "

SRC_URI[libpwquality.sha256sum] = "343e1994c432d8245a80bc6e1e123374d5653739a9a9d0b1d6e1061ac8358853"
SRC_URI[libpwquality-devel.sha256sum] = "53fadef6bb4805e0f03a09dabe442bff449185d1cd792e2cd7419ccaffb4d743"
SRC_URI[python3-pwquality.sha256sum] = "f7a5a5e51012fc8318090239d9e407ea0144b9eb147b6a671ac564b8007da1d4"
