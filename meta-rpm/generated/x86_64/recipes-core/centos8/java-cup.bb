SUMMARY = "generated recipe based on java_cup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_java_cup = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_java_cup-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/java_cup-0.11b-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/java_cup-javadoc-0.11b-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/java_cup-manual-0.11b-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[java_cup.sha256sum] = "1028e3433e40c41d9cc1b50efdc7bf90b269006472f69b11128e91435e5bbf4c"
SRC_URI[java_cup-javadoc.sha256sum] = "ec22be05bccf5064d33a6610c84d259c7cd218fb48432963d191e288898efbc0"
SRC_URI[java_cup-manual.sha256sum] = "a7cf732b61bc8f2b309125d6fdb20e9d4b31e6bdbd1c57eaff53b90afc8fbb78"
