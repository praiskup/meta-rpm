SUMMARY = "generated recipe based on cyrus-sasl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db e2fsprogs krb5-libs libxcrypt pkgconfig-native"
RPM_SONAME_REQ_cyrus-sasl-devel = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libsasl2.so.3"
RPROVIDES_cyrus-sasl-devel = "cyrus-sasl-dev (= 2.1.27)"
RDEPENDS_cyrus-sasl-devel = "cyrus-sasl cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt pkgconf-pkg-config"
RPM_SONAME_PROV_cyrus-sasl-lib = "libanonymous.so.3 libsasl2.so.3 libsasldb.so.3"
RPM_SONAME_REQ_cyrus-sasl-lib = "libanonymous.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libdb-5.3.so libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libsasl2.so.3 libsasldb.so.3"
RDEPENDS_cyrus-sasl-lib = "glibc krb5-libs libcom_err libdb libxcrypt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-devel-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-lib-2.1.27-1.el8.x86_64.rpm \
          "

SRC_URI[cyrus-sasl-devel.sha256sum] = "5cad2df13ebd114811a5de5c705fc69b0274be657969ba27cdc9c521e633d97e"
SRC_URI[cyrus-sasl-lib.sha256sum] = "d160310fc4a31a400596ee4cf9234e4440c45e8969a6d8ed8478439a9622a478"
