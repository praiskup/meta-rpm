SUMMARY = "generated recipe based on bluez srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs glib-2.0 libical pkgconfig-native readline systemd-libs"
RPM_SONAME_REQ_bluez = "libc.so.6 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libreadline.so.7 librt.so.1 libudev.so.1"
RDEPENDS_bluez = "bash dbus dbus-libs glib2 glibc readline systemd systemd-libs"
RPM_SONAME_REQ_bluez-cups = "libc.so.6 libdbus-1.so.3 libglib-2.0.so.0"
RDEPENDS_bluez-cups = "bluez cups dbus-libs glib2 glibc"
RPM_SONAME_REQ_bluez-hid2hci = "libc.so.6 libudev.so.1"
RDEPENDS_bluez-hid2hci = "bash bluez glibc systemd-libs"
RPM_SONAME_PROV_bluez-libs = "libbluetooth.so.3"
RPM_SONAME_REQ_bluez-libs = "libc.so.6"
RDEPENDS_bluez-libs = "glibc"
RPM_SONAME_REQ_bluez-libs-devel = "libbluetooth.so.3"
RPROVIDES_bluez-libs-devel = "bluez-libs-dev (= 5.50)"
RDEPENDS_bluez-libs-devel = "bluez-libs pkgconf-pkg-config"
RPM_SONAME_REQ_bluez-obexd = "libc.so.6 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libical.so.3 libicalss.so.3 libicalvcal.so.3"
RDEPENDS_bluez-obexd = "bash bluez bluez-libs dbus-libs glib2 glibc libical"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bluez-cups-5.50-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bluez-5.50-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bluez-hid2hci-5.50-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bluez-libs-5.50-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bluez-obexd-5.50-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/bluez-libs-devel-5.50-3.el8.x86_64.rpm \
          "

SRC_URI[bluez.sha256sum] = "9648e603f7a160e67fb2c25f73fc16f0c6b5b8fb60a976aa68240fc4068f5163"
SRC_URI[bluez-cups.sha256sum] = "d6f25ba59c73b90eeae7abbef2112d05b0e927f569402117d64a952df7217559"
SRC_URI[bluez-hid2hci.sha256sum] = "5d07673168ba912cd50f08abf91869bf524f4494253ad811cfd696edc4efe11d"
SRC_URI[bluez-libs.sha256sum] = "7ffa4bf98fbe4126d58895893d6781e1cb956ddc7854dfcb3cb4e3acb9e6be8c"
SRC_URI[bluez-libs-devel.sha256sum] = "dc6f0cb312434d86ee3d9b4aab0f9d86bb5808a87d778fda552030571e682d2e"
SRC_URI[bluez-obexd.sha256sum] = "960e8a3b0a38bf9e61e232c84da7cbfe4afdd58898bac67d621ea5232cdf2422"
