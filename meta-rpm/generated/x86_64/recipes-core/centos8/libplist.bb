SUMMARY = "generated recipe based on libplist srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libplist = "libplist++.so.3 libplist.so.3"
RPM_SONAME_REQ_libplist = "libc.so.6 libgcc_s.so.1 libm.so.6 libplist.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libplist = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libplist-devel = "libplist++.so.3 libplist.so.3"
RPROVIDES_libplist-devel = "libplist-dev (= 2.0.0)"
RDEPENDS_libplist-devel = "libplist pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libplist-2.0.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libplist-devel-2.0.0-10.el8.x86_64.rpm \
          "

SRC_URI[libplist.sha256sum] = "cfa1e54b1415aabde6f6c74923ac747e4d16b9947dad9495bf1e9d8355a92bbd"
SRC_URI[libplist-devel.sha256sum] = "15b985b8fcbb07ea5d3e7b8f12714f539dd1ef16ff39f1aaddedb2e106fd04a7"
