SUMMARY = "generated recipe based on python-hwdata srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-hwdata = "hwdata platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-hwdata-2.3.6-3.el8.noarch.rpm \
          "

SRC_URI[python3-hwdata.sha256sum] = "7db269ccc445b3b913faf7bd3d90d48cd7dbffaf210a3734be4051b4c06090cf"
