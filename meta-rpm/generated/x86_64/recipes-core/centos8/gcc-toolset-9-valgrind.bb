SUMMARY = "generated recipe based on gcc-toolset-9-valgrind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-valgrind = "libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0"
RDEPENDS_gcc-toolset-9-valgrind = "gcc-toolset-9-runtime glibc libgcc perl-interpreter"
RPROVIDES_gcc-toolset-9-valgrind-devel = "gcc-toolset-9-valgrind-dev (= 3.15.0)"
RDEPENDS_gcc-toolset-9-valgrind-devel = "gcc-toolset-9-runtime gcc-toolset-9-valgrind pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-valgrind-3.15.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-valgrind-devel-3.15.0-9.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-valgrind.sha256sum] = "5c639c189561d28fa0a238c702e88f6b5e51af1e079516c14230a0297f53da37"
SRC_URI[gcc-toolset-9-valgrind-devel.sha256sum] = "d996d06a5eb3182809377018384d94f536be0a6554eedd3f870cdb7f520755d5"
