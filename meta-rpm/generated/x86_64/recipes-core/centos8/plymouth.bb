SUMMARY = "generated recipe based on plymouth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo glib-2.0 libdrm libpng pango pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_plymouth = "libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply.so.4 librt.so.1 libudev.so.1"
RDEPENDS_plymouth = "bash glibc plymouth-core-libs plymouth-scripts systemd-libs"
RPM_SONAME_PROV_plymouth-core-libs = "libply-boot-client.so.4 libply-splash-core.so.4 libply.so.4"
RPM_SONAME_REQ_plymouth-core-libs = "libc.so.6 libdl.so.2 libm.so.6 libply.so.4 librt.so.1 libudev.so.1"
RDEPENDS_plymouth-core-libs = "glibc systemd-libs"
RPM_SONAME_PROV_plymouth-graphics-libs = "libply-splash-graphics.so.4"
RPM_SONAME_REQ_plymouth-graphics-libs = "libc.so.6 libdl.so.2 libdrm.so.2 libm.so.6 libply-splash-core.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-graphics-libs = "centos-logos glibc libdrm libpng plymouth-core-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-fade-throbber = "libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-fade-throbber = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-label = "libc.so.6 libcairo.so.2 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-label = "cairo glib2 glibc libpng pango plymouth plymouth-core-libs plymouth-graphics-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-script = "libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-script = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-space-flares = "libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-space-flares = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs plymouth-plugin-label systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-throbgress = "libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-throbgress = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs plymouth-plugin-label systemd-libs zlib"
RPM_SONAME_REQ_plymouth-plugin-two-step = "libc.so.6 libdl.so.2 libm.so.6 libply-splash-core.so.4 libply-splash-graphics.so.4 libply.so.4 libpng16.so.16 librt.so.1 libudev.so.1 libz.so.1"
RDEPENDS_plymouth-plugin-two-step = "glibc libpng plymouth plymouth-core-libs plymouth-graphics-libs plymouth-plugin-label systemd-libs zlib"
RDEPENDS_plymouth-scripts = "bash coreutils cpio dracut findutils gzip plymouth"
RDEPENDS_plymouth-system-theme = "plymouth-theme-charge"
RDEPENDS_plymouth-theme-charge = "bash plymouth-plugin-two-step plymouth-scripts"
RDEPENDS_plymouth-theme-fade-in = "bash plymouth-plugin-fade-throbber plymouth-scripts"
RDEPENDS_plymouth-theme-script = "plymouth-plugin-script plymouth-scripts"
RDEPENDS_plymouth-theme-solar = "bash plymouth-plugin-space-flares plymouth-scripts"
RDEPENDS_plymouth-theme-spinfinity = "bash plymouth-plugin-throbgress plymouth-scripts"
RDEPENDS_plymouth-theme-spinner = "bash plymouth-plugin-two-step plymouth-scripts"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-core-libs-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-graphics-libs-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-plugin-fade-throbber-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-plugin-label-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-plugin-script-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-plugin-space-flares-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-plugin-throbgress-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-plugin-two-step-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-scripts-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-system-theme-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-theme-charge-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-theme-fade-in-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-theme-script-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-theme-solar-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-theme-spinfinity-0.9.3-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/plymouth-theme-spinner-0.9.3-16.el8.x86_64.rpm \
          "

SRC_URI[plymouth.sha256sum] = "f4a368ff6ff06926e1063a9ae5b3765c6e6e485d75ff45c5db936b488407d8cc"
SRC_URI[plymouth-core-libs.sha256sum] = "fc0e6f38501e7784cdba94dcbf864aaf7763dc2b447150fcd158174aaa16228c"
SRC_URI[plymouth-graphics-libs.sha256sum] = "d3269ae06b8667ce6a9ab6a37bc0408e8d030bd5d32e86faeb162b9be52ba8db"
SRC_URI[plymouth-plugin-fade-throbber.sha256sum] = "9e5be30fc2301c3b6500a703ccb7a15d20f4f8f6f2d7c605825791a3bc052949"
SRC_URI[plymouth-plugin-label.sha256sum] = "cf8cadcf27248efa76b58b4e4a02f97e62ce96a3585465f192350cef52767cc2"
SRC_URI[plymouth-plugin-script.sha256sum] = "09a2c5017cc28deb5ad3aa76f51f407fd161289609ff15698e75d1d877501bb8"
SRC_URI[plymouth-plugin-space-flares.sha256sum] = "97a2b63d43ac32c293820cc7b2d81d071b7f66f5eb1922e1e53fca38ac15f0e9"
SRC_URI[plymouth-plugin-throbgress.sha256sum] = "ca4ba83022a34b5762b457a118d284e1b8ae423c1ea3d2d35c5adde894b7cfb2"
SRC_URI[plymouth-plugin-two-step.sha256sum] = "b4d7a5b41a2a085747330d294969f7e9abce9f3acf3245fe2c784d6d97943940"
SRC_URI[plymouth-scripts.sha256sum] = "e8a3d8ff881cb6796a02196d11a6a2f2bcd701a19185f64165d9510beafe2e7a"
SRC_URI[plymouth-system-theme.sha256sum] = "09327d934032a543917de8f54c2dc61581a5c4a92ebfcf53292eae147008f74f"
SRC_URI[plymouth-theme-charge.sha256sum] = "3a2f98cf6829ef2c4b7f264c5de72026214ec8f550c516b8a91077bbdda2b1ff"
SRC_URI[plymouth-theme-fade-in.sha256sum] = "882af94a373ce95a020adde1213e2163f05021017d731f001f896dcc6e3acd6c"
SRC_URI[plymouth-theme-script.sha256sum] = "a0ae2a4da005c1d563a3a6a949a14fb399550f646d3be3a6da8ee5667173ff74"
SRC_URI[plymouth-theme-solar.sha256sum] = "9cc60d5189dfbff6a5456c1b359f0ac374cd6e4e767da133b35765b06d20b36c"
SRC_URI[plymouth-theme-spinfinity.sha256sum] = "23c201fca3dabc65ab9c3b5d019a8cb5516cc1e16c4f561803721b46e0e89d55"
SRC_URI[plymouth-theme-spinner.sha256sum] = "561e39ee60286fcfb8a5d3feb3ed092bde61132260aa51d8f6a55716af17bc5b"
