SUMMARY = "generated recipe based on xdg-user-dirs-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_REQ_xdg-user-dirs-gtk = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_xdg-user-dirs-gtk = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 pango xdg-user-dirs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xdg-user-dirs-gtk-0.10-13.el8.x86_64.rpm \
          "

SRC_URI[xdg-user-dirs-gtk.sha256sum] = "a71937386205268cd61480ba18e2b83a5158ea00a5ad4a6df17fb27b0709107e"
