SUMMARY = "generated recipe based on libloader srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_libloader = "java-1.8.0-openjdk-headless javapackages-tools libbase"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libloader-1.1.3-17.el8.noarch.rpm \
          "

SRC_URI[libloader.sha256sum] = "2e9a1d39f896ae899dde7abb159aa5fe727ef8b8eb4f899fac4eba4ca0a9a86a"
