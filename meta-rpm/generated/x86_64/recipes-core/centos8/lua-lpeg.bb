SUMMARY = "generated recipe based on lua-lpeg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lua-lpeg = "libc.so.6"
RDEPENDS_lua-lpeg = "glibc lua-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lua-lpeg-1.0.1-6.el8.x86_64.rpm \
          "

SRC_URI[lua-lpeg.sha256sum] = "e6a173648e745d570f3340322dc97b83036570a340bd22a532301c1bcfdc7089"
