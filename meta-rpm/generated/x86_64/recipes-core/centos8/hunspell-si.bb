SUMMARY = "generated recipe based on hunspell-si srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-si = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-si-0.2.1-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-si.sha256sum] = "9e35d040f67d8ace7dc973eb765673502919783fed21c7add92cd84c8fbded55"
