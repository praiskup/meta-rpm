SUMMARY = "generated recipe based on hyperv-daemons srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyperv-daemons = "hypervfcopyd hypervkvpd hypervvssd"
RDEPENDS_hyperv-tools = "platform-python"
RPM_SONAME_REQ_hypervfcopyd = "libc.so.6"
RDEPENDS_hypervfcopyd = "bash glibc hyperv-daemons-license systemd"
RPM_SONAME_REQ_hypervkvpd = "libc.so.6"
RDEPENDS_hypervkvpd = "bash glibc hyperv-daemons-license systemd"
RPM_SONAME_REQ_hypervvssd = "libc.so.6"
RDEPENDS_hypervvssd = "bash glibc hyperv-daemons-license systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyperv-daemons-0-0.28.20180415git.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyperv-daemons-license-0-0.28.20180415git.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyperv-tools-0-0.28.20180415git.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hypervfcopyd-0-0.28.20180415git.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hypervkvpd-0-0.28.20180415git.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hypervvssd-0-0.28.20180415git.el8.x86_64.rpm \
          "

SRC_URI[hyperv-daemons.sha256sum] = "b37a5f85effe5b9f4db9e74088945dd15de83882ecd269db6c7a26a97b221bd0"
SRC_URI[hyperv-daemons-license.sha256sum] = "7389b5563a49c01bae66aab2797f99b35bf906b589209ac4acf6d83b654f6ec1"
SRC_URI[hyperv-tools.sha256sum] = "cc6a38c268262db61ee90fa23bee7e8638c2121c63545e5a05a29d7ed3e0b678"
SRC_URI[hypervfcopyd.sha256sum] = "63ed4042f4f6d28945e5df71ea06367d9f7e0f2aa801d8e8d1be4de15a362f42"
SRC_URI[hypervkvpd.sha256sum] = "b97c57996d437ab04d4bb0126d4cd6519db279a3a24cf244c0d14dbcb7c3d83d"
SRC_URI[hypervvssd.sha256sum] = "b98cf99991058beb8c98e190f890c1fd32488ad6ef2d2cbb329f6b34a3df2b28"
