SUMMARY = "generated recipe based on mingw-gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libgcc libmpc mpfr pkgconfig-native zlib"
RPM_SONAME_REQ_mingw32-cpp = "libc.so.6 libgcc_s.so.1 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_mingw32-cpp = "glibc gmp libgcc libmpc libstdc++ mingw32-filesystem mpfr zlib"
RPM_SONAME_PROV_mingw32-gcc = "liblto_plugin.so.0"
RPM_SONAME_REQ_mingw32-gcc = "libc.so.6 libgcc_s.so.1 libgmp.so.10 liblto_plugin.so.0 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_mingw32-gcc = "bash glibc gmp libgcc libmpc libstdc++ mingw32-binutils mingw32-cpp mingw32-crt mingw32-filesystem mingw32-headers mingw32-winpthreads mpfr zlib"
RPM_SONAME_REQ_mingw32-gcc-c++ = "libc.so.6 libgcc_s.so.1 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_mingw32-gcc-c++ = "glibc gmp libgcc libmpc libstdc++ mingw32-crt mingw32-filesystem mingw32-gcc mingw32-winpthreads mpfr zlib"
RPM_SONAME_REQ_mingw64-cpp = "libc.so.6 libgcc_s.so.1 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_mingw64-cpp = "glibc gmp libgcc libmpc libstdc++ mingw64-filesystem mpfr zlib"
RPM_SONAME_PROV_mingw64-gcc = "liblto_plugin.so.0"
RPM_SONAME_REQ_mingw64-gcc = "libc.so.6 libgcc_s.so.1 libgmp.so.10 liblto_plugin.so.0 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_mingw64-gcc = "bash glibc gmp libgcc libmpc libstdc++ mingw64-binutils mingw64-cpp mingw64-crt mingw64-filesystem mingw64-headers mingw64-winpthreads mpfr zlib"
RPM_SONAME_REQ_mingw64-gcc-c++ = "libc.so.6 libgcc_s.so.1 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_mingw64-gcc-c++ = "glibc gmp libgcc libmpc libstdc++ mingw64-crt mingw64-filesystem mingw64-gcc mingw64-winpthreads mpfr zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-cpp-7.2.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-gcc-7.2.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-gcc-c++-7.2.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-cpp-7.2.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-gcc-7.2.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-gcc-c++-7.2.0-2.el8.x86_64.rpm \
          "

SRC_URI[mingw32-cpp.sha256sum] = "9962b106ba7dd6177fcd101d55f9f3726512bd39e4b454408405104aa77e04de"
SRC_URI[mingw32-gcc.sha256sum] = "0807db84214d9ebc9545905f81aa55dc44657bd855f9f2f7c06c37e1465b04bf"
SRC_URI[mingw32-gcc-c++.sha256sum] = "d272ec37409ac3ebcbc0eef8c1a2f894b4a33983c8bfb204ad8d7296a5a90c06"
SRC_URI[mingw64-cpp.sha256sum] = "3c5060cb6098764a1648a80fe7e2993f7b9659581bd1dd64cf88f2f29951df41"
SRC_URI[mingw64-gcc.sha256sum] = "f5d59e0a72d52d9c45fcf1255ac9fdf6e24be362e6d93cd669dc92301861cbc4"
SRC_URI[mingw64-gcc-c++.sha256sum] = "2ae69e54a88a0bb8b824971df0c17eb043db0bc61d38cac25bc4f75d7f72d997"
