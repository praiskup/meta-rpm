SUMMARY = "generated recipe based on python-pip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_platform-python-pip = "ca-certificates platform-python platform-python-setuptools"
RDEPENDS_python3-pip = "platform-python-pip python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pip-9.0.3-16.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/platform-python-pip-9.0.3-16.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pip-wheel-9.0.3-16.el8.noarch.rpm \
          "

SRC_URI[platform-python-pip.sha256sum] = "e2624ca93ae44ba17164a7be05244c85464ee5ffa35a7306448161ef3264c526"
SRC_URI[python3-pip.sha256sum] = "1c841656baf31afeac83c897c531ea782b879a77b651859275f10be801d7c8aa"
SRC_URI[python3-pip-wheel.sha256sum] = "a789f3d16f940aa34d57b2a19b1af8e975feb36c2ffef8470fbff2ee160f29bb"
