SUMMARY = "generated recipe based on lensfun srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc pkgconfig-native"
RPM_SONAME_PROV_lensfun = "liblensfun.so.1"
RPM_SONAME_REQ_lensfun = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libm.so.6 libstdc++.so.6"
RDEPENDS_lensfun = "glib2 glibc libgcc libstdc++"
RPM_SONAME_REQ_lensfun-devel = "liblensfun.so.1"
RPROVIDES_lensfun-devel = "lensfun-dev (= 0.3.2)"
RDEPENDS_lensfun-devel = "glib2-devel lensfun pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lensfun-0.3.2-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lensfun-devel-0.3.2-14.el8.x86_64.rpm \
          "

SRC_URI[lensfun.sha256sum] = "b59d537650346d413ee139319b206a4a051d9a80a8e204c02550b1ab71e30e34"
SRC_URI[lensfun-devel.sha256sum] = "ac378d03ea7a823edcffeb723805cbb29a14070d650d3cb95a3ed31d17e2f621"
