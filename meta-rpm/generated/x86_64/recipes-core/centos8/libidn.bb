SUMMARY = "generated recipe based on libidn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libidn = "libidn.so.11"
RPM_SONAME_REQ_libidn = "libc.so.6 libidn.so.11"
RDEPENDS_libidn = "bash emacs-filesystem glibc info"
RPM_SONAME_REQ_libidn-devel = "libidn.so.11"
RPROVIDES_libidn-devel = "libidn-dev (= 1.34)"
RDEPENDS_libidn-devel = "libidn pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libidn-1.34-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libidn-devel-1.34-5.el8.x86_64.rpm \
          "

SRC_URI[libidn.sha256sum] = "ea587257d999d3701d851f1a99ade2f47e8453880f43cbaa73cf0ee2524d01ea"
SRC_URI[libidn-devel.sha256sum] = "0400b5737fa4703b99032ec5e794e10e48b60a300dd9efdbfb709b53334071cc"
