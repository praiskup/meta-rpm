SUMMARY = "generated recipe based on pango srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo fontconfig freetype fribidi glib-2.0 harfbuzz libthai libx11 libxft libxrender pkgconfig-native"
RPM_SONAME_PROV_pango = "libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0"
RPM_SONAME_REQ_pango = "libX11.so.6 libXft.so.2 libXrender.so.1 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libfribidi.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libharfbuzz.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0 libthai.so.0"
RDEPENDS_pango = "bash cairo fontconfig freetype fribidi glib2 glibc harfbuzz libX11 libXft libXrender libthai"
RPM_SONAME_REQ_pango-devel = "libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0"
RPROVIDES_pango-devel = "pango-dev (= 1.42.4)"
RDEPENDS_pango-devel = "cairo-devel fontconfig-devel freetype-devel fribidi-devel glib2-devel harfbuzz-devel libXft-devel pango pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pango-1.42.4-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pango-devel-1.42.4-6.el8.x86_64.rpm \
          "

SRC_URI[pango.sha256sum] = "8246d152767da7e6dd297599ed4c55b8bde2a5c18e54d97a80d44eb3099d6e89"
SRC_URI[pango-devel.sha256sum] = "42a290d19ba4b7a35ebc9b42c9592e51551bc7b5505c862bb99799fed1014ba6"
