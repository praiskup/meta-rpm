SUMMARY = "generated recipe based on perl-Data-Dumper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Data-Dumper = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Data-Dumper = "glibc perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Data-Dumper-2.167-399.el8.x86_64.rpm \
          "

SRC_URI[perl-Data-Dumper.sha256sum] = "2cd7e38beedf2751fa1a82c4441c460e7cde47dbef58b0ff2f473c7f392ac267"
