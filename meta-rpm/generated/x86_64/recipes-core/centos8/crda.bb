SUMMARY = "generated recipe based on crda srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_crda = "bash iw kernel systemd systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/crda-3.18_2018.05.31-3.el8.noarch.rpm \
          "

SRC_URI[crda.sha256sum] = "d2d436ffdf509c92850dd2599f25429014200fd74dad2c1517b7872467696391"
