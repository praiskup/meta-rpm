SUMMARY = "generated recipe based on rarian srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_rarian = "librarian.so.0"
RPM_SONAME_REQ_rarian = "libc.so.6 libgcc_s.so.1 libm.so.6 librarian.so.0 libstdc++.so.6"
RDEPENDS_rarian = "coreutils gawk glibc libgcc libstdc++ libxml2 libxslt util-linux"
RPM_SONAME_REQ_rarian-compat = "libc.so.6 libgcc_s.so.1 libm.so.6 librarian.so.0 libstdc++.so.6"
RDEPENDS_rarian-compat = "bash glibc libgcc libstdc++ rarian"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rarian-0.8.1-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rarian-compat-0.8.1-19.el8.x86_64.rpm \
          "

SRC_URI[rarian.sha256sum] = "43981cc85efc7781279702a73c69bb68a5868e493c5b5d7c2c992231385261ce"
SRC_URI[rarian-compat.sha256sum] = "a6fb0b949d4527bb964f5faa9c63ac0655e43a8831642ea248acbe68928ab2ad"
