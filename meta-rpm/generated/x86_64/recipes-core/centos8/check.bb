SUMMARY = "generated recipe based on check srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_check = "libcheck.so.0"
RPM_SONAME_REQ_check = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_check = "bash glibc info libgcc"
RPM_SONAME_REQ_check-devel = "libcheck.so.0"
RPROVIDES_check-devel = "check-dev (= 0.12.0)"
RDEPENDS_check-devel = "check pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/check-0.12.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/check-devel-0.12.0-2.el8.x86_64.rpm \
          "

SRC_URI[check.sha256sum] = "97dc3a302a71f461988ebe33156d81969e4bc536adc780a4ec9f691220e6d682"
SRC_URI[check-devel.sha256sum] = "62ea19d868779550320b7a9fc6b4c0340247cdc8f80dce2911b6ace4373333cf"
