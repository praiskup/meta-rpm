SUMMARY = "generated recipe based on perl-File-HomeDir srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-HomeDir = "perl-Carp perl-Exporter perl-File-Path perl-File-Temp perl-File-Which perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-File-HomeDir-1.002-4.el8.noarch.rpm \
          "

SRC_URI[perl-File-HomeDir.sha256sum] = "488557338b80b9a722691c6150b2bc63dd64aef05af178377d6e8a0763681df9"
