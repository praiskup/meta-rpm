SUMMARY = "generated recipe based on hunspell-fur srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fur = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-fur-0.20050912-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-fur.sha256sum] = "2ff7da7992a68ec274a8f4564f3aa500b898443a1f770c58ac5ef97cc72378f5"
