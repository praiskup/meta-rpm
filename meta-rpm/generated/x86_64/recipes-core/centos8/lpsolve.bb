SUMMARY = "generated recipe based on lpsolve srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lpsolve = "liblpsolve55.so"
RPM_SONAME_REQ_lpsolve = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_lpsolve = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lpsolve-5.5.2.0-20.el8.x86_64.rpm \
          "

SRC_URI[lpsolve.sha256sum] = "d55a1d6b9b40fa3cac97d3d09745ee5d65e70308f87c13cf20de73c1441c5b89"
