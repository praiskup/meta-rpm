SUMMARY = "generated recipe based on libsass srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libsass = "libsass.so.0"
RPM_SONAME_REQ_libsass = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libsass = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libsass-devel = "libsass.so.0"
RPROVIDES_libsass-devel = "libsass-dev (= 3.4.5)"
RDEPENDS_libsass-devel = "libsass pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsass-3.4.5-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsass-devel-3.4.5-5.el8.x86_64.rpm \
          "

SRC_URI[libsass.sha256sum] = "dd346f9d18bc31c286effc951ca836b42f947305146a84e4be0f5544033bcecd"
SRC_URI[libsass-devel.sha256sum] = "6f055ef4b4a2605c97e91953f79e4258e2196fbcb99fa42bd87efb51e3a060ba"
