SUMMARY = "generated recipe based on libxml2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3 xz zlib"
RPM_SONAME_PROV_libxml2 = "libxml2.so.2"
RPM_SONAME_REQ_libxml2 = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libxml2 = "glibc xz-libs zlib"
RPM_SONAME_REQ_libxml2-devel = "libxml2.so.2"
RPROVIDES_libxml2-devel = "libxml2-dev (= 2.9.7)"
RDEPENDS_libxml2-devel = "bash cmake-filesystem libxml2 pkgconf-pkg-config xz-devel zlib-devel"
RPM_SONAME_PROV_python3-libxml2 = "libxml2mod.so"
RPM_SONAME_REQ_python3-libxml2 = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_python3-libxml2 = "glibc libxml2 platform-python python3-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxml2-devel-2.9.7-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libxml2-2.9.7-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libxml2-2.9.7-7.el8.x86_64.rpm \
          "

SRC_URI[libxml2.sha256sum] = "e9bb15193a9ebc036aa948a9a7d85f67c5337f58bed53fec1780b0158916dd34"
SRC_URI[libxml2-devel.sha256sum] = "9990efe5a99ce81c0ed40294fce463670a7ae1f3325e541d3705d8b6320da422"
SRC_URI[python3-libxml2.sha256sum] = "e17dd17213feab898126b9a8042b8a7941a13f18218a6e2e2335199eb53d89d3"
