SUMMARY = "generated recipe based on latex2html srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_latex2html = "bash netpbm-progs perl-Carp perl-Encode perl-Exporter perl-Getopt-Long perl-PathTools perl-interpreter perl-libs texlive-collection-latexrecommended texlive-dvips texlive-url"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/latex2html-2018.2-2.el8.noarch.rpm \
          "

SRC_URI[latex2html.sha256sum] = "47642fe2dd985aa265b81dbf6aa402e59322b61fcbc6cfa199c0632ce0608125"
