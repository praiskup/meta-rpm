SUMMARY = "generated recipe based on libpsm2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libuuid numactl pkgconfig-native"
RPM_SONAME_PROV_libpsm2 = "libpsm2.so.2"
RPM_SONAME_REQ_libpsm2 = "libc.so.6 libdl.so.2 libnuma.so.1 libpthread.so.0 librt.so.1"
RDEPENDS_libpsm2 = "glibc numactl-libs"
RPM_SONAME_REQ_libpsm2-compat = "libc.so.6 libpsm2.so.2 libpthread.so.0"
RDEPENDS_libpsm2-compat = "bash glibc libpsm2 systemd-udev"
RPM_SONAME_REQ_libpsm2-devel = "libpsm2.so.2"
RPROVIDES_libpsm2-devel = "libpsm2-dev (= 11.2.86)"
RDEPENDS_libpsm2-devel = "libpsm2 libuuid-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpsm2-11.2.86-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpsm2-compat-11.2.86-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpsm2-devel-11.2.86-1.el8.x86_64.rpm \
          "

SRC_URI[libpsm2.sha256sum] = "bcd011a0b04dab461b03bf55898e3efb09db61f6b745799599a49f6055e370dd"
SRC_URI[libpsm2-compat.sha256sum] = "e48c5dca23dbc9f17c240e9dff247920eeb73d4b55879baa5ede456bd57f4fe5"
SRC_URI[libpsm2-devel.sha256sum] = "51f8e63907347c9dd502297486f120cf6462a99b50b012cce26935ac07765fdb"
