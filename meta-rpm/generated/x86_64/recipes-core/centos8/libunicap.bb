SUMMARY = "generated recipe based on libunicap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libraw1394 pkgconfig-native v4l-utils"
RPM_SONAME_PROV_libunicap = "libeuvccam_cpi.so libunicap.so.2"
RPM_SONAME_REQ_libunicap = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libraw1394.so.11 librt.so.1 libunicap.so.2 libv4l2.so.0"
RDEPENDS_libunicap = "glibc libraw1394 libv4l"
RPM_SONAME_REQ_libunicap-devel = "libunicap.so.2"
RPROVIDES_libunicap-devel = "libunicap-dev (= 0.9.12)"
RDEPENDS_libunicap-devel = "libunicap pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libunicap-0.9.12-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libunicap-devel-0.9.12-21.el8.x86_64.rpm \
          "

SRC_URI[libunicap.sha256sum] = "a6921d1d4eb5546ae5487a1a9c3065406973aa4686bb4f6acddc98f745ac30df"
SRC_URI[libunicap-devel.sha256sum] = "5471ce56b113d98144c3612acab2104a9ec5b9f0b513b1a90a2b271087b4b983"
