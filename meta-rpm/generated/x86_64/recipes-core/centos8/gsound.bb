SUMMARY = "generated recipe based on gsound srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libcanberra pkgconfig-native"
RPM_SONAME_PROV_gsound = "libgsound.so.0"
RPM_SONAME_REQ_gsound = "libc.so.6 libcanberra.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgsound.so.0"
RDEPENDS_gsound = "glib2 glibc libcanberra"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gsound-1.0.2-6.el8.x86_64.rpm \
          "

SRC_URI[gsound.sha256sum] = "7097cd7e6f66eef1a562fb4b26ee01fdfbaa34d74fc70b5811f4cc85fcf91688"
