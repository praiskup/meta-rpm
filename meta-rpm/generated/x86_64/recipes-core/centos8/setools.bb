SUMMARY = "generated recipe based on setools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_setools = "python3-setools"
RDEPENDS_setools-console = "libselinux platform-python python3-setools"
RDEPENDS_setools-console-analyses = "libselinux platform-python python3-networkx python3-setools"
RDEPENDS_setools-gui = "platform-python python3-networkx python3-qt5 python3-setools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/setools-4.2.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/setools-console-analyses-4.2.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/setools-gui-4.2.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/setools-console-4.2.2-2.el8.x86_64.rpm \
          "

SRC_URI[setools.sha256sum] = "6af88003275483369da96aad8e0df5d0fa3ea90c66f6450495c879ab22ddfdf4"
SRC_URI[setools-console.sha256sum] = "ab8b1fd975fc6ccee39115662b9fe9fdef2a3fc9e44d2249d8d0ae2944cd9e46"
SRC_URI[setools-console-analyses.sha256sum] = "9cb017c56884431d10c4cd2d4c7fe3612173854c562f696adc754882c660e8bf"
SRC_URI[setools-gui.sha256sum] = "27ceeb902956020af1e965c3d6bca358ff41fff0e421802286525b40c1dbea5a"
