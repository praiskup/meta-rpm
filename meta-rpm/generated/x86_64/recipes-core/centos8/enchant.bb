SUMMARY = "generated recipe based on enchant srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 hunspell libgcc pkgconfig-native"
RPM_SONAME_PROV_enchant = "libenchant.so.1 libenchant_myspell.so"
RPM_SONAME_REQ_enchant = "libc.so.6 libenchant.so.1 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libhunspell-1.6.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_enchant = "glib2 glibc hunspell libgcc libstdc++"
RPM_SONAME_REQ_enchant-devel = "libenchant.so.1"
RPROVIDES_enchant-devel = "enchant-dev (= 1.6.0)"
RDEPENDS_enchant-devel = "enchant glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/enchant-1.6.0-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/enchant-devel-1.6.0-21.el8.x86_64.rpm \
          "

SRC_URI[enchant.sha256sum] = "b04b5dbbdf7da328e6cfe8022b1d0fe6beffb865a9bcc76503f9c8f9ac578223"
SRC_URI[enchant-devel.sha256sum] = "1530c1dd2052e16b2378f0c0b83dda6e9a5e3c0dd603c1b45bfa50aeb74b71ea"
