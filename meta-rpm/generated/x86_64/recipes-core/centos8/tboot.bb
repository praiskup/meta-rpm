SUMMARY = "generated recipe based on tboot srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl pkgconfig-native trousers zlib"
RPM_SONAME_REQ_tboot = "libc.so.6 libcrypto.so.1.1 libtspi.so.1 libz.so.1"
RDEPENDS_tboot = "bash glibc openssl-libs trousers-lib zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tboot-1.9.10-1.el8.x86_64.rpm \
          "

SRC_URI[tboot.sha256sum] = "c74472d2886e7e873f4e75eff199a215732f2b4de43e5f482a2e57472f8b6590"
