SUMMARY = "generated recipe based on libSM srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libice libuuid pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libSM = "libSM.so.6"
RPM_SONAME_REQ_libSM = "libICE.so.6 libc.so.6 libuuid.so.1"
RDEPENDS_libSM = "glibc libICE libuuid"
RPM_SONAME_REQ_libSM-devel = "libSM.so.6"
RPROVIDES_libSM-devel = "libSM-dev (= 1.2.3)"
RDEPENDS_libSM-devel = "libICE-devel libSM pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libSM-1.2.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libSM-devel-1.2.3-1.el8.x86_64.rpm \
          "

SRC_URI[libSM.sha256sum] = "962688a146f961fd8dece4a4f3e1a9f79e51d40b26355fbe7d579bc97ffa8d58"
SRC_URI[libSM-devel.sha256sum] = "04a9a3eaff0b424f6114e982ed93f0ab8b9c498cd631dd67c908aae3f81de105"
