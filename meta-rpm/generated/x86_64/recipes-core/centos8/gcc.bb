SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp isl libmpc mpfr pkgconfig-native zlib"
RPM_SONAME_REQ_cpp = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_cpp = "bash filesystem glibc gmp info libmpc mpfr zlib"
RPM_SONAME_PROV_gcc = "liblto_plugin.so.0"
RPM_SONAME_REQ_gcc = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgmp.so.10 libgomp.so.1 libisl.so.15 liblto_plugin.so.0 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc = "bash binutils cpp glibc glibc-devel gmp info isl libgcc libgomp libmpc mpfr zlib"
RPM_SONAME_REQ_gcc-c++ = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_gcc-c++ = "gcc glibc gmp libmpc libstdc++ libstdc++-devel mpfr zlib"
RPM_SONAME_PROV_gcc-gdb-plugin = "libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0"
RPM_SONAME_REQ_gcc-gdb-plugin = "libc.so.6 libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gcc-gdb-plugin = "gcc glibc libgcc libstdc++"
RPM_SONAME_REQ_gcc-gfortran = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgfortran.so.5 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-gfortran = "bash gcc glibc gmp info libgfortran libmpc libquadmath libquadmath-devel mpfr zlib"
RPM_SONAME_REQ_gcc-offload-nvptx = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libstdc++.so.6 libz.so.1"
RDEPENDS_gcc-offload-nvptx = "gcc glibc gmp libgcc libgomp-offload-nvptx libmpc libstdc++ mpfr zlib"
RPM_SONAME_REQ_gcc-plugin-devel = "libc.so.6 libm.so.6"
RPROVIDES_gcc-plugin-devel = "gcc-plugin-dev (= 8.3.1)"
RDEPENDS_gcc-plugin-devel = "gcc glibc gmp-devel libmpc-devel mpfr-devel"
RPM_SONAME_PROV_libgfortran = "libgfortran.so.5"
RPM_SONAME_REQ_libgfortran = "libc.so.6 libgcc_s.so.1 libm.so.6 libquadmath.so.0 libz.so.1"
RDEPENDS_libgfortran = "glibc libgcc libquadmath zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cpp-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-c++-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-gdb-plugin-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-gfortran-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-offload-nvptx-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgfortran-8.3.1-5.el8.0.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gcc-plugin-devel-8.3.1-5.el8.0.2.x86_64.rpm \
          "

SRC_URI[cpp.sha256sum] = "fa07ea87bb6bb09cc15d2dfaa90e507910eb49ad817b92816461b3257e80f32d"
SRC_URI[gcc.sha256sum] = "ce042ab5f9ed2851c7c692b494df4e4599e20a552e41e69e160c5986f2a7fe25"
SRC_URI[gcc-c++.sha256sum] = "0d1f81380884caa2d2a5dac01e9c736917716948024050f6ce9ab7f2d2de2982"
SRC_URI[gcc-gdb-plugin.sha256sum] = "683f7d384de22218ec11e85ce67b02f5fc52da3e527126c491ca32751801a06c"
SRC_URI[gcc-gfortran.sha256sum] = "8d6c79580669235c14197ed573fa513fe8a7fbd48a5ddd9a985107b70c15f3e1"
SRC_URI[gcc-offload-nvptx.sha256sum] = "ebe858806e089052e4c7a0c5404ac6d781e5291c20c807512f4b297603aa2101"
SRC_URI[gcc-plugin-devel.sha256sum] = "b75c40e9e92f862f770a654d73dbdc4870fd9e9ba6db1c765d83cd313ffa8924"
SRC_URI[libgfortran.sha256sum] = "c4e73dd86d8821f3a19258b0d98dd2b22b7542e7bbab5079592edb5a88e82402"
