SUMMARY = "generated recipe based on nasm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_nasm = "libc.so.6"
RDEPENDS_nasm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/nasm-2.13.03-2.el8.x86_64.rpm \
          "

SRC_URI[nasm.sha256sum] = "d4ca7dbeb91014d46b3ffdd47ee71e9fd50c0e2a3cb3a52ced71881337c91b82"
