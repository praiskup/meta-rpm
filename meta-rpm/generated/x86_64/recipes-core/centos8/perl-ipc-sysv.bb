SUMMARY = "generated recipe based on perl-IPC-SysV srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-IPC-SysV = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-IPC-SysV = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IPC-SysV-2.07-397.el8.x86_64.rpm \
          "

SRC_URI[perl-IPC-SysV.sha256sum] = "a4575502e1513f395b1961928312550d39411fb1458b445eb68cb30e1b4128b9"
