SUMMARY = "generated recipe based on valgrind srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_valgrind = "libc.so.6 libdl.so.2 libgcc_s.so.1 libpthread.so.0"
RDEPENDS_valgrind = "glibc libgcc perl-interpreter"
RPROVIDES_valgrind-devel = "valgrind-dev (= 3.15.0)"
RDEPENDS_valgrind-devel = "pkgconf-pkg-config valgrind"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/valgrind-3.15.0-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/valgrind-devel-3.15.0-11.el8.x86_64.rpm \
          "

SRC_URI[valgrind.sha256sum] = "549a287adb0f501adad173a2b7ecd94e8716eb1672f1160a8b6094b25da7ce67"
SRC_URI[valgrind-devel.sha256sum] = "1a550014bb096aeccf55310ca4f4c3712f6f8d8039a164f07f509e606d66e235"
