SUMMARY = "generated recipe based on glibmm24 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libsigc++20 pkgconfig-native"
RPM_SONAME_PROV_glibmm24 = "libgiomm-2.4.so.1 libglibmm-2.4.so.1 libglibmm_generate_extra_defs-2.4.so.1"
RPM_SONAME_REQ_glibmm24 = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libglibmm-2.4.so.1 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_glibmm24 = "glib2 glibc libgcc libsigc++20 libstdc++"
RPM_SONAME_REQ_glibmm24-devel = "libgiomm-2.4.so.1 libglibmm-2.4.so.1 libglibmm_generate_extra_defs-2.4.so.1"
RPROVIDES_glibmm24-devel = "glibmm24-dev (= 2.56.0)"
RDEPENDS_glibmm24-devel = "glib2-devel glibmm24 libsigc++20-devel perl-Exporter perl-XML-Parser perl-constant perl-interpreter perl-libs perl-open pkgconf-pkg-config"
RDEPENDS_glibmm24-doc = "glibmm24 libsigc++20-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glibmm24-2.56.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glibmm24-devel-2.56.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glibmm24-doc-2.56.0-1.el8.noarch.rpm \
          "

SRC_URI[glibmm24.sha256sum] = "16125798df49f910f0c9e368bdc6cc822fc3a6de00328b264930f51131135419"
SRC_URI[glibmm24-devel.sha256sum] = "8e08d8076a82c83e6e16e666aa8438ff77d8ef299c0748e1f10e4b55301d5c4e"
SRC_URI[glibmm24-doc.sha256sum] = "0cbf6d529fb1d1a82ef6599be20db87013359bfa1835a1347c7ae903899a188b"
