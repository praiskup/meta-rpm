SUMMARY = "generated recipe based on hunspell-tk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-tk-0.02-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-tk.sha256sum] = "f3aeacac7bf0a0c50b545e725875c9af5cce79f256312189227d186357d7b2a0"
