SUMMARY = "generated recipe based on perl-Role-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Role-Tiny = "perl-Class-Method-Modifiers perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Role-Tiny-2.000006-2.el8.noarch.rpm \
          "

SRC_URI[perl-Role-Tiny.sha256sum] = "54a3008c491f942db53e96630d9db9e42a85f03cfd295e75eb861007e3f29599"
