SUMMARY = "generated recipe based on libdc1394 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libraw1394 libusb1 pkgconfig-native"
RPM_SONAME_PROV_libdc1394 = "libdc1394.so.22"
RPM_SONAME_REQ_libdc1394 = "libc.so.6 libm.so.6 libraw1394.so.11 libusb-1.0.so.0"
RDEPENDS_libdc1394 = "glibc libraw1394 libusbx"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdc1394-2.2.2-10.el8.x86_64.rpm \
          "

SRC_URI[libdc1394.sha256sum] = "4e66f6a4e40e7dc78c7a82fcb8a65aa8015f5194c3471bbf44d492bb3b28f8d7"
