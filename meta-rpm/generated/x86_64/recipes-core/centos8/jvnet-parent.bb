SUMMARY = "generated recipe based on jvnet-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_jvnet-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jvnet-parent-4-10.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jvnet-parent.sha256sum] = "150269f355136f7184134ae87e52a2343ccde9f83fead6f17b90fc1e87792b1d"
