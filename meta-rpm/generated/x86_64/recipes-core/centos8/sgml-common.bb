SUMMARY = "generated recipe based on sgml-common srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sgml-common = "bash coreutils"
RDEPENDS_xml-common = "bash libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sgml-common-0.6.3-50.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/xml-common-0.6.3-50.el8.noarch.rpm \
          "

SRC_URI[sgml-common.sha256sum] = "96bdf4d80bf676260ee88413b3ddc2dff18ee59c005e81cd51bcb16c5dd8b36e"
SRC_URI[xml-common.sha256sum] = "6d7676847b3c0dbac22983c85c0a419af43029cc3b8ff5dc26c9f85174fc85d8"
