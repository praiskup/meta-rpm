SUMMARY = "generated recipe based on perl-CPAN-Meta-Requirements srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-CPAN-Meta-Requirements = "perl-Carp perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-CPAN-Meta-Requirements-2.140-396.el8.noarch.rpm \
          "

SRC_URI[perl-CPAN-Meta-Requirements.sha256sum] = "0c337f93aebdb80f83500f74b32e675d4d2314c0a3d49d594d607a15c4d78a7b"
