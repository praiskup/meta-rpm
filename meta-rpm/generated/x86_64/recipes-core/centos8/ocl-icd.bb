SUMMARY = "generated recipe based on ocl-icd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_ocl-icd = "libOpenCL.so.1"
RPM_SONAME_REQ_ocl-icd = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2"
RDEPENDS_ocl-icd = "glibc"
RPM_SONAME_REQ_ocl-icd-devel = "libOpenCL.so.1"
RPROVIDES_ocl-icd-devel = "ocl-icd-dev (= 2.2.12)"
RDEPENDS_ocl-icd-devel = "ocl-icd opencl-headers pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ocl-icd-2.2.12-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocl-icd-devel-2.2.12-1.el8.x86_64.rpm \
          "

SRC_URI[ocl-icd.sha256sum] = "075917f00475e81a66d8bca53f58f41bf502491fd5567bec1a87deb1bf35cd86"
SRC_URI[ocl-icd-devel.sha256sum] = "c905de2f6f9114d60bd27b1f019428157b0af24d85a0bc28e6e33126192655bc"
