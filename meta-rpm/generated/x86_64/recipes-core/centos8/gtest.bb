SUMMARY = "generated recipe based on gtest srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_gmock = "libgmock.so.1.8.0 libgmock_main.so.1.8.0"
RPM_SONAME_REQ_gmock = "libc.so.6 libgcc_s.so.1 libgmock.so.1.8.0 libgtest.so.1.8.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gmock = "glibc gtest libgcc libstdc++"
RPM_SONAME_REQ_gmock-devel = "libgmock.so.1.8.0 libgmock_main.so.1.8.0"
RPROVIDES_gmock-devel = "gmock-dev (= 1.8.0)"
RDEPENDS_gmock-devel = "gmock"
RPM_SONAME_PROV_gtest = "libgtest.so.1.8.0 libgtest_main.so.1.8.0"
RPM_SONAME_REQ_gtest = "libc.so.6 libgcc_s.so.1 libgtest.so.1.8.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gtest = "glibc libgcc libstdc++"
RPM_SONAME_REQ_gtest-devel = "libgtest.so.1.8.0 libgtest_main.so.1.8.0"
RPROVIDES_gtest-devel = "gtest-dev (= 1.8.0)"
RDEPENDS_gtest-devel = "gtest"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gmock-1.8.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gmock-devel-1.8.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtest-1.8.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtest-devel-1.8.0-5.el8.x86_64.rpm \
          "

SRC_URI[gmock.sha256sum] = "0ef4b2288a95f664b9f4bb2d920df70778539a90804628814be6ab0711bbed6f"
SRC_URI[gmock-devel.sha256sum] = "fbafc057c9efbfd4646f47fd07c7cc210c759620592f7726aa791430bce7364f"
SRC_URI[gtest.sha256sum] = "81be2e1b9751399f7b58cf9341d88262620946e425aa659de4b27c60a9682791"
SRC_URI[gtest-devel.sha256sum] = "f1d65756922e94f790dc9eb28659372b3cb2957d3e8670092c065fa719de0451"
