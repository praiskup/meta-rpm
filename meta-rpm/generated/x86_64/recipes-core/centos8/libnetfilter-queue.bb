SUMMARY = "generated recipe based on libnetfilter_queue srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl libnfnetlink pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_queue = "libnetfilter_queue.so.1"
RPM_SONAME_REQ_libnetfilter_queue = "libc.so.6 libmnl.so.0 libnfnetlink.so.0"
RDEPENDS_libnetfilter_queue = "glibc libmnl libnfnetlink"
RPM_SONAME_REQ_libnetfilter_queue-devel = "libnetfilter_queue.so.1"
RPROVIDES_libnetfilter_queue-devel = "libnetfilter_queue-dev (= 1.0.2)"
RDEPENDS_libnetfilter_queue-devel = "kernel-headers libnetfilter_queue libnfnetlink-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnetfilter_queue-1.0.2-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnetfilter_queue-devel-1.0.2-11.el8.x86_64.rpm \
          "

SRC_URI[libnetfilter_queue.sha256sum] = "1dbe53be56c5b04f61d9847da9ad5b34db8f0166fb0567e6918afb2616e100ed"
SRC_URI[libnetfilter_queue-devel.sha256sum] = "043fc97240774214a930d0421709887454d3464ba34ba6fabfd3a16b12738fa2"
