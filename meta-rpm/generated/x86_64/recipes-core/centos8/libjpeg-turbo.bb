SUMMARY = "generated recipe based on libjpeg-turbo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "jpeg"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libjpeg-turbo = "libjpeg.so.62"
RPM_SONAME_REQ_libjpeg-turbo = "libc.so.6"
RDEPENDS_libjpeg-turbo = "glibc"
RPM_SONAME_REQ_libjpeg-turbo-devel = "libjpeg.so.62"
RPROVIDES_libjpeg-turbo-devel = "libjpeg-turbo-dev (= 1.5.3)"
RDEPENDS_libjpeg-turbo-devel = "libjpeg-turbo pkgconf-pkg-config"
RPM_SONAME_REQ_libjpeg-turbo-utils = "libc.so.6 libjpeg.so.62"
RDEPENDS_libjpeg-turbo-utils = "glibc libjpeg-turbo"
RPM_SONAME_PROV_turbojpeg = "libturbojpeg.so.0"
RPM_SONAME_REQ_turbojpeg = "libc.so.6"
RDEPENDS_turbojpeg = "glibc"
RPM_SONAME_REQ_turbojpeg-devel = "libturbojpeg.so.0"
RPROVIDES_turbojpeg-devel = "turbojpeg-dev (= 1.5.3)"
RDEPENDS_turbojpeg-devel = "pkgconf-pkg-config turbojpeg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libjpeg-turbo-1.5.3-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libjpeg-turbo-devel-1.5.3-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libjpeg-turbo-utils-1.5.3-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/turbojpeg-1.5.3-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/turbojpeg-devel-1.5.3-10.el8.x86_64.rpm \
          "

SRC_URI[libjpeg-turbo.sha256sum] = "6fca7ccea71b8c55d19d4236e60c2dd8dcf968253f82608829232c007ffe716a"
SRC_URI[libjpeg-turbo-devel.sha256sum] = "fd2742179dd606a22ad3b85ea8b8c90af1a30fc7920dcbb1015ec2b595ad53c4"
SRC_URI[libjpeg-turbo-utils.sha256sum] = "6573a5bf17dafd25b9f3b5c2fcec34b420fc9e410934cbc87ff46ec8c3c5ef35"
SRC_URI[turbojpeg.sha256sum] = "32051450aa65cf5fc6a6d308f3ccab2f26766fe82b1ccbf0c8dc12c01a6de366"
SRC_URI[turbojpeg-devel.sha256sum] = "96d179cc6e339dcd707deba5a13526cca483162aba0130478610d6ea84a6feb3"
