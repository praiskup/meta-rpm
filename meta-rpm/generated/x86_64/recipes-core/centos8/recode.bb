SUMMARY = "generated recipe based on recode srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_recode = "librecode.so.0"
RPM_SONAME_REQ_recode = "libc.so.6 librecode.so.0"
RDEPENDS_recode = "bash glibc info"
RPM_SONAME_REQ_recode-devel = "librecode.so.0"
RPROVIDES_recode-devel = "recode-dev (= 3.6)"
RDEPENDS_recode-devel = "recode"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/recode-3.6-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/recode-devel-3.6-47.el8.x86_64.rpm \
          "

SRC_URI[recode.sha256sum] = "c78342cb2ba8fd55778187d3c5d7734cc0d692e4a4d722b8b3f0ad7df2a2af05"
SRC_URI[recode-devel.sha256sum] = "0c30b2e1c97b67cd2694a97808cfca7943493110f291eb3a53f9aceef66700ce"
