SUMMARY = "generated recipe based on libgtop2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libgtop2 = "libgtop-2.0.so.11"
RPM_SONAME_REQ_libgtop2 = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libgtop2 = "glib2 glibc"
RPM_SONAME_REQ_libgtop2-devel = "libgtop-2.0.so.11"
RPROVIDES_libgtop2-devel = "libgtop2-dev (= 2.38.0)"
RDEPENDS_libgtop2-devel = "glib2-devel libgtop2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgtop2-2.38.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgtop2-devel-2.38.0-3.el8.x86_64.rpm \
          "

SRC_URI[libgtop2.sha256sum] = "59e0fe3d29597b1c5ad16d3c36925ab1d37ddad5d27abce72062e13af2917874"
SRC_URI[libgtop2-devel.sha256sum] = "293e9b26cfc7582aa695bc7eb978c541945a8a14cd1d32e1678bb6c888e5c853"
