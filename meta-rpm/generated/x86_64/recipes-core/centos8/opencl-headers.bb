SUMMARY = "generated recipe based on opencl-headers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opencl-headers-2.2-1.20180306gite986688.el8.noarch.rpm \
          "

SRC_URI[opencl-headers.sha256sum] = "04b159a7182de1964e3dd32ad9aaf7cd4dcf2926050b126063a38d51c5f72381"
