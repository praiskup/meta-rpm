SUMMARY = "generated recipe based on bash-completion srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_bash-completion = "bash pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bash-completion-2.7-5.el8.noarch.rpm \
          "

SRC_URI[bash-completion.sha256sum] = "c03abd17ea35752cc712d9174b4a6a5aee116e6219f452bcf51b5c91aebf5885"
