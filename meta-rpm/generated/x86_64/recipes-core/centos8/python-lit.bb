SUMMARY = "generated recipe based on python-lit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-lit = "platform-python python3-setuptools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-lit-0.9.0-1.module_el8.2.0+309+0c7b6b03.noarch.rpm \
          "

SRC_URI[python3-lit.sha256sum] = "8d6a64c17502e5b899d8bb7a988c3979c5379e906355aaadc697dd56ac031dfa"
