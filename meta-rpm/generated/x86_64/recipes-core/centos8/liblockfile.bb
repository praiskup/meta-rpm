SUMMARY = "generated recipe based on liblockfile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liblockfile = "liblockfile.so.1"
RPM_SONAME_REQ_liblockfile = "libc.so.6"
RDEPENDS_liblockfile = "glibc"
RPM_SONAME_REQ_liblockfile-devel = "liblockfile.so.1"
RPROVIDES_liblockfile-devel = "liblockfile-dev (= 1.14)"
RDEPENDS_liblockfile-devel = "liblockfile"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblockfile-1.14-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/liblockfile-devel-1.14-1.el8.x86_64.rpm \
          "

SRC_URI[liblockfile.sha256sum] = "df37836ee977aebe767dcca8ac899c03846c2b12efbcce7647b74d70f6afaae7"
SRC_URI[liblockfile-devel.sha256sum] = "fabd3ef0e97be4ae8a07c8f9cfa9fa7b934bd9023357ce5fa41f3f5d7f2b268b"
