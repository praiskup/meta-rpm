SUMMARY = "generated recipe based on lz4 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lz4 = "libc.so.6"
RDEPENDS_lz4 = "glibc"
RPM_SONAME_REQ_lz4-devel = "liblz4.so.1"
RPROVIDES_lz4-devel = "lz4-dev (= 1.8.1.2)"
RDEPENDS_lz4-devel = "lz4-libs pkgconf-pkg-config"
RPM_SONAME_PROV_lz4-libs = "liblz4.so.1"
RPM_SONAME_REQ_lz4-libs = "libc.so.6"
RDEPENDS_lz4-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lz4-1.8.1.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lz4-devel-1.8.1.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lz4-libs-1.8.1.2-4.el8.x86_64.rpm \
          "

SRC_URI[lz4.sha256sum] = "1f2425f2fc862259c82ef8e61bddd37d7e199d531f24c356de755bafc4595e8f"
SRC_URI[lz4-devel.sha256sum] = "f6ac51c2e9654a9c8b7fbe4716a948f03bba0d5867a3be46465bf21468bf89dd"
SRC_URI[lz4-libs.sha256sum] = "e557dd4b8e4892c224f8a8a8a7443a80c8bcf574069884b639bd6752762e8c8c"
