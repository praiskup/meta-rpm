SUMMARY = "generated recipe based on perl-XML-SAX-Base srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-SAX-Base = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-XML-SAX-Base-1.09-4.el8.noarch.rpm \
          "

SRC_URI[perl-XML-SAX-Base.sha256sum] = "96c87413205259b2b5eeb6e322b1d43e744e547441b7a57e94df3af78ec9acb7"
