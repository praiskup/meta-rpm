SUMMARY = "generated recipe based on libpfm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_PROV_libpfm = "libpfm.so.4"
RPM_SONAME_REQ_libpfm = "libc.so.6"
RDEPENDS_libpfm = "glibc"
RPM_SONAME_REQ_libpfm-devel = "libpfm.so.4"
RPROVIDES_libpfm-devel = "libpfm-dev (= 4.10.1)"
RDEPENDS_libpfm-devel = "libpfm"
RDEPENDS_libpfm-static = "libpfm"
RPM_SONAME_REQ_python3-libpfm = "libc.so.6 libpfm.so.4 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-libpfm = "glibc libpfm platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpfm-4.10.1-2.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpfm-devel-4.10.1-2.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpfm-static-4.10.1-2.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-libpfm-4.10.1-2.el8_2.1.x86_64.rpm \
          "

SRC_URI[libpfm.sha256sum] = "38f0c90c939315298bccee0c58439d5a6c0b30147de19bf964e417683660fb5e"
SRC_URI[libpfm-devel.sha256sum] = "e8c73b224bea16c7c6c2f0c296c3b0a9067381576c0ddff4a527243beebc18a0"
SRC_URI[libpfm-static.sha256sum] = "fa15acf9610f184892d2aaf45d111b68b0d9d5cfc82cb730afd2b441ef243023"
SRC_URI[python3-libpfm.sha256sum] = "adb897a7c205cc8cc00de32dc06462c2a04a0b4770b924561a1991ff063d5322"
