SUMMARY = "generated recipe based on qrencode srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng pkgconfig-native zlib"
RPM_SONAME_REQ_qrencode = "libc.so.6 libpng16.so.16 libqrencode.so.3 libz.so.1"
RDEPENDS_qrencode = "glibc libpng qrencode-libs zlib"
RPM_SONAME_REQ_qrencode-devel = "libqrencode.so.3"
RPROVIDES_qrencode-devel = "qrencode-dev (= 3.4.4)"
RDEPENDS_qrencode-devel = "pkgconf-pkg-config qrencode-libs"
RPM_SONAME_PROV_qrencode-libs = "libqrencode.so.3"
RPM_SONAME_REQ_qrencode-libs = "libc.so.6"
RDEPENDS_qrencode-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qrencode-3.4.4-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qrencode-libs-3.4.4-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qrencode-devel-3.4.4-5.el8.x86_64.rpm \
          "

SRC_URI[qrencode.sha256sum] = "af6cc8b278d1e10967b62ebbf648d99538f7b366506a806cf37b7fcbf52d3a5c"
SRC_URI[qrencode-devel.sha256sum] = "1357c8d2d0d770e8de00447765ada8d359d01c5101084448ecf4ef707511cd48"
SRC_URI[qrencode-libs.sha256sum] = "8b8cd80338793ee4221c73d9d8a7f17d393974c18350b4ef607a09cbab646aba"
