SUMMARY = "generated recipe based on gd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libjpeg-turbo libpng libwebp libx11 libxpm pkgconfig-native tiff zlib"
RPM_SONAME_PROV_gd = "libgd.so.3"
RPM_SONAME_REQ_gd = "libX11.so.6 libXpm.so.4 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libjpeg.so.62 libm.so.6 libpng16.so.16 libtiff.so.5 libwebp.so.7 libz.so.1"
RDEPENDS_gd = "fontconfig freetype glibc libX11 libXpm libjpeg-turbo libpng libtiff libwebp zlib"
RPM_SONAME_REQ_gd-devel = "libgd.so.3"
RPROVIDES_gd-devel = "gd-dev (= 2.2.5)"
RDEPENDS_gd-devel = "bash fontconfig-devel freetype-devel gd libX11-devel libXpm-devel libjpeg-turbo-devel libpng-devel libtiff-devel libwebp-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gd-2.2.5-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gd-devel-2.2.5-6.el8.x86_64.rpm \
          "

SRC_URI[gd.sha256sum] = "aa8004c699e074c39dd4caf131f102ed7101fa7bb549e42c9a22766edd37feb7"
SRC_URI[gd-devel.sha256sum] = "d43a0d978dee728b352007b0c9ff35b7d63262561ab24797cb0bb9bc5232a6db"
