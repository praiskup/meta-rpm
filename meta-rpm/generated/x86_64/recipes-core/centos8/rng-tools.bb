SUMMARY = "generated recipe based on rng-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcrypt libxml2 openssl pkgconfig-native sysfsutils"
RPM_SONAME_REQ_rng-tools = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcrypt.so.20 libpthread.so.0 libssl.so.1.1 libsysfs.so.2 libxml2.so.2"
RDEPENDS_rng-tools = "bash glibc libcurl libgcrypt libsysfs libxml2 openssl openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rng-tools-6.8-3.el8.x86_64.rpm \
          "

SRC_URI[rng-tools.sha256sum] = "401dc71b00d9ceb50dfc628e7f805ed5aa13c3c4e371dcf6fb727ef01c78b6cc"
