SUMMARY = "generated recipe based on perl-Date-ISO8601 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Date-ISO8601 = "perl-Carp perl-Exporter perl-constant perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Date-ISO8601-0.005-2.el8.noarch.rpm \
          "

SRC_URI[perl-Date-ISO8601.sha256sum] = "fbcd660bb6cb7436d9ec14eb0719a64fa4ec7a654b36d6be0cd03bca82e0c8a3"
