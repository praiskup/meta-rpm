SUMMARY = "generated recipe based on perl-Text-CharWidth srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Text-CharWidth = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Text-CharWidth = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Text-CharWidth-0.04-32.el8.x86_64.rpm \
          "

SRC_URI[perl-Text-CharWidth.sha256sum] = "b00b114439227f94f23815069104a07e0194fd1898dea9b3f3033b4e59bf80ff"
