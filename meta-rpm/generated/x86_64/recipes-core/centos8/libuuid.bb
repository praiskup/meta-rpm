SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuuid = "libuuid.so.1"
RPM_SONAME_REQ_libuuid = "ld-linux-x86-64.so.2 libc.so.6"
RDEPENDS_libuuid = "glibc"
RPM_SONAME_REQ_libuuid-devel = "libuuid.so.1"
RPROVIDES_libuuid-devel = "libuuid-dev (= 2.32.1)"
RDEPENDS_libuuid-devel = "libuuid pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libuuid-2.32.1-22.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libuuid-devel-2.32.1-22.el8.x86_64.rpm \
          "

SRC_URI[libuuid.sha256sum] = "01f0acd36d1b6ea4afff5343523511b26b912c59bdb6ca4b5488cf01c5d9daa6"
SRC_URI[libuuid-devel.sha256sum] = "81118414cc8e9ad4c3593490a679288ed9cf1d353e1ed689210846209047a289"
