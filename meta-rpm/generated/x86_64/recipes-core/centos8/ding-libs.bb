SUMMARY = "generated recipe based on ding-libs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libbasicobjects = "libbasicobjects.so.0"
RPM_SONAME_REQ_libbasicobjects = "libc.so.6"
RDEPENDS_libbasicobjects = "glibc"
RPM_SONAME_REQ_libbasicobjects-devel = "libbasicobjects.so.0"
RPROVIDES_libbasicobjects-devel = "libbasicobjects-dev (= 0.1.1)"
RDEPENDS_libbasicobjects-devel = "libbasicobjects pkgconf-pkg-config"
RPM_SONAME_PROV_libcollection = "libcollection.so.4"
RPM_SONAME_REQ_libcollection = "libc.so.6"
RDEPENDS_libcollection = "glibc"
RPM_SONAME_REQ_libcollection-devel = "libcollection.so.4"
RPROVIDES_libcollection-devel = "libcollection-dev (= 0.7.0)"
RDEPENDS_libcollection-devel = "libcollection pkgconf-pkg-config"
RPM_SONAME_PROV_libdhash = "libdhash.so.1"
RPM_SONAME_REQ_libdhash = "libc.so.6"
RDEPENDS_libdhash = "glibc"
RPM_SONAME_PROV_libini_config = "libini_config.so.5"
RPM_SONAME_REQ_libini_config = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libpath_utils.so.1 libref_array.so.1"
RDEPENDS_libini_config = "glibc libbasicobjects libcollection libpath_utils libref_array"
RPM_SONAME_REQ_libini_config-devel = "libini_config.so.5"
RPROVIDES_libini_config-devel = "libini_config-dev (= 1.3.1)"
RDEPENDS_libini_config-devel = "libbasicobjects-devel libcollection-devel libini_config libref_array-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libpath_utils = "libpath_utils.so.1"
RPM_SONAME_REQ_libpath_utils = "libc.so.6"
RDEPENDS_libpath_utils = "glibc"
RPM_SONAME_REQ_libpath_utils-devel = "libpath_utils.so.1"
RPROVIDES_libpath_utils-devel = "libpath_utils-dev (= 0.2.1)"
RDEPENDS_libpath_utils-devel = "libpath_utils pkgconf-pkg-config"
RPM_SONAME_PROV_libref_array = "libref_array.so.1"
RPM_SONAME_REQ_libref_array = "libc.so.6"
RDEPENDS_libref_array = "glibc"
RPM_SONAME_REQ_libref_array-devel = "libref_array.so.1"
RPROVIDES_libref_array-devel = "libref_array-dev (= 0.1.5)"
RDEPENDS_libref_array-devel = "libref_array pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libbasicobjects-0.1.1-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcollection-0.7.0-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libdhash-0.5.0-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libini_config-1.3.1-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpath_utils-0.2.1-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libref_array-0.1.5-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libbasicobjects-devel-0.1.1-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcollection-devel-0.7.0-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libini_config-devel-1.3.1-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpath_utils-devel-0.2.1-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libref_array-devel-0.1.5-39.el8.x86_64.rpm \
          "

SRC_URI[libbasicobjects.sha256sum] = "5452b96e4b57c275dd41e48d55af1ca4f77ccfb3ae403796e599a039d7382b91"
SRC_URI[libbasicobjects-devel.sha256sum] = "57a45f6b06938f45c6233daed52bd422065e4bce756d7694381c4db82c97df00"
SRC_URI[libcollection.sha256sum] = "708a8fd75f7c6987d66e2d62de664b5a84c6f75fd4e5230e244681897b15a25d"
SRC_URI[libcollection-devel.sha256sum] = "a9b99a9fab98a5aa434c47f1fcf0c8df8ae2b02d5924ac69517a20e3ea5098cb"
SRC_URI[libdhash.sha256sum] = "6327624b7b0d726a3c95f2245619efb1df8e70023fadcc8158afed39f57859e7"
SRC_URI[libini_config.sha256sum] = "b06fa62d0a585a7bc3b9d3341923736b3ee4b6cc6d7ca83bebcb97225ca86ac9"
SRC_URI[libini_config-devel.sha256sum] = "f4a177660f32389c3fa3de64dfc37fff2ba657979e8a949784e0320abf5f42f4"
SRC_URI[libpath_utils.sha256sum] = "185f36b3af9367e6142233af358df22f23ee623697bc6a45c47091013707872e"
SRC_URI[libpath_utils-devel.sha256sum] = "db28022965bd7accaf102d27f8a08a212af63909c3168adba85e36aefc102e2c"
SRC_URI[libref_array.sha256sum] = "6b740563ba5858573ff3c2d2a827aeaf6e7166178e36066755d2cde4cf8a016f"
SRC_URI[libref_array-devel.sha256sum] = "ba87f657cbfbc3df02dd126635f19c881e3a25c9171405acb7c76899955e2482"
