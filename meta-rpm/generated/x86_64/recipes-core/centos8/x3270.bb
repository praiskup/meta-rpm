SUMMARY = "generated recipe based on x3270 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxaw libxmu libxt ncurses openssl pkgconfig-native readline"
RPM_SONAME_REQ_x3270 = "libc.so.6 libcrypto.so.1.1 libssl.so.1.1 libutil.so.1"
RDEPENDS_x3270 = "glibc openssl-libs"
RPM_SONAME_REQ_x3270-text = "libc.so.6 libcrypto.so.1.1 libncursesw.so.6 libreadline.so.7 libssl.so.1.1 libtinfo.so.6 libutil.so.1"
RDEPENDS_x3270-text = "glibc ncurses-libs openssl-libs readline x3270"
RPM_SONAME_REQ_x3270-x11 = "libX11.so.6 libXaw.so.7 libXmu.so.6 libXt.so.6 libc.so.6 libcrypto.so.1.1 libssl.so.1.1 libutil.so.1"
RDEPENDS_x3270-x11 = "bash glibc libX11 libXaw libXmu libXt openssl-libs x3270 xorg-x11-font-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/x3270-x11-3.6ga5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/x3270-3.6ga5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/x3270-text-3.6ga5-1.el8.x86_64.rpm \
          "

SRC_URI[x3270.sha256sum] = "bd304e8911c41229196c51ed4ed94d87e67aa3b0793a684f9fb394834a32a362"
SRC_URI[x3270-text.sha256sum] = "095f322bbdb62b3de66a1480ee25bae07e235d13e2f573c72768ba418a29e6ae"
SRC_URI[x3270-x11.sha256sum] = "cff8e9fc7c3eb789dd267b487c991da61f1ef1d6763277b3e0f451c5e4510718"
