SUMMARY = "generated recipe based on libiscsi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cunit libgcrypt pkgconfig-native rdma-core"
RPM_SONAME_PROV_libiscsi = "libiscsi.so.8"
RPM_SONAME_REQ_libiscsi = "libc.so.6 libgcrypt.so.20 libibverbs.so.1 librdmacm.so.1"
RDEPENDS_libiscsi = "glibc libgcrypt libibverbs librdmacm"
RPM_SONAME_REQ_libiscsi-devel = "libiscsi.so.8"
RPROVIDES_libiscsi-devel = "libiscsi-dev (= 1.18.0)"
RDEPENDS_libiscsi-devel = "libiscsi pkgconf-pkg-config"
RPM_SONAME_REQ_libiscsi-utils = "libc.so.6 libcunit.so.1 libdl.so.2 libgcrypt.so.20 libibverbs.so.1 libiscsi.so.8 librdmacm.so.1"
RDEPENDS_libiscsi-utils = "CUnit glibc libgcrypt libibverbs libiscsi librdmacm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libiscsi-1.18.0-8.module_el8.2.0+524+f765f7e0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libiscsi-devel-1.18.0-8.module_el8.2.0+524+f765f7e0.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libiscsi-utils-1.18.0-8.module_el8.2.0+524+f765f7e0.x86_64.rpm \
          "

SRC_URI[libiscsi.sha256sum] = "5f4f55b1f213133d5ea8a8174576a33c2ea8c58b39774ebc451a432a9cba34ac"
SRC_URI[libiscsi-devel.sha256sum] = "14c151a735b59909e36130af96f28faae1ee078e0e2cb02743a7667896b036d6"
SRC_URI[libiscsi-utils.sha256sum] = "8180896f8961da9c795f8aa91d3e64f9f5b02b6113c54c8c0605c6bb5fd6bc31"
