SUMMARY = "generated recipe based on plexus-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-pom = "java-1.8.0-openjdk-headless javapackages-filesystem maven-enforcer-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-pom-5.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-pom.sha256sum] = "acf209e14b2ab37352e9d938050da098efcaf455d8bb2ee49e84576042acab7d"
