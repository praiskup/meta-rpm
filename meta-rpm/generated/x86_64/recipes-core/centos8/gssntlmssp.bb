SUMMARY = "generated recipe based on gssntlmssp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "e2fsprogs krb5-libs libunistring openssl pkgconfig-native samba zlib"
RPM_SONAME_REQ_gssntlmssp = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0 libssl.so.1.1 libunistring.so.2 libwbclient.so.0 libz.so.1"
RDEPENDS_gssntlmssp = "glibc krb5-libs libcom_err libunistring libwbclient openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gssntlmssp-0.7.0-6.el8.x86_64.rpm \
          "

SRC_URI[gssntlmssp.sha256sum] = "91fe7c2c32eed93c050274439ed356480f94cf4fa1a0101b7d42fb8c905c1395"
