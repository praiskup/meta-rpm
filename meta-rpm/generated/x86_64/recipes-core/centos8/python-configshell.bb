SUMMARY = "generated recipe based on python-configshell srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-configshell = "platform-python python3-pyparsing python3-urwid"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-configshell-1.1.27-1.el8.noarch.rpm \
          "

SRC_URI[python3-configshell.sha256sum] = "911c7ea022e9ac587c39d9decd8b1e132e013a9d70180eb9ea8b5fdf1d9e6f6a"
