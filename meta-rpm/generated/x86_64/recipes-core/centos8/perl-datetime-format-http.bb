SUMMARY = "generated recipe based on perl-DateTime-Format-HTTP srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Format-HTTP = "perl-DateTime perl-HTTP-Date perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-Format-HTTP-0.42-9.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Format-HTTP.sha256sum] = "47e62522c28286aeacb8a46400c896ad76597053a8bf21a30ee8974aac368582"
