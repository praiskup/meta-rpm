SUMMARY = "generated recipe based on perl-URI srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-URI = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-MIME-Base64 perl-PathTools perl-Scalar-List-Utils perl-constant perl-interpreter perl-libnet perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-URI-1.73-3.el8.noarch.rpm \
          "

SRC_URI[perl-URI.sha256sum] = "9feaf80c733951790bc3578db42ea2abef65643ebb36779e46dccdaf4c76b525"
