SUMMARY = "generated recipe based on perl-Net-SSLeay srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Net-SSLeay = "libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0 libssl.so.1.1"
RDEPENDS_perl-Net-SSLeay = "glibc openssl-libs perl-Carp perl-Errno perl-Exporter perl-MIME-Base64 perl-Socket perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Net-SSLeay-1.88-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Net-SSLeay.sha256sum] = "1e253318c542798bd81aa78a2807902d6bda648d5bd9e6f0ba84c79b3822171a"
