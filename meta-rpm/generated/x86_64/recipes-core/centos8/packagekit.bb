SUMMARY = "generated recipe based on PackageKit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo dbus fontconfig freetype gdk-pixbuf glib-2.0 gstreamer1.0 gtk+3 gtk2 libappstream-glib libdnf libgcc librepo pango pkgconfig-native polkit rpm sqlite3 systemd-libs"
RPM_SONAME_PROV_PackageKit = "libpk_backend_dnf.so libpk_backend_dummy.so libpk_backend_test_fail.so libpk_backend_test_nop.so libpk_backend_test_spawn.so libpk_backend_test_succeed.so libpk_backend_test_thread.so"
RPM_SONAME_REQ_PackageKit = "libappstream-glib.so.8 libc.so.6 libdnf.so.2 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpackagekit-glib2.so.18 libpolkit-gobject-1.so.0 libpthread.so.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libsystemd.so.0"
RDEPENDS_PackageKit = "PackageKit-glib bash gdk-pixbuf2 glib2 glibc libappstream-glib libdnf libgcc librepo polkit-libs rpm-libs shared-mime-info sqlite-libs systemd systemd-libs"
RPM_SONAME_REQ_PackageKit-command-not-found = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpackagekit-glib2.so.18 libsqlite3.so.0"
RDEPENDS_PackageKit-command-not-found = "PackageKit PackageKit-glib bash glib2 glibc libgcc sqlite-libs"
RDEPENDS_PackageKit-cron = "PackageKit bash crontabs"
RPM_SONAME_PROV_PackageKit-glib = "libpackagekit-glib2.so.18"
RPM_SONAME_REQ_PackageKit-glib = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libsqlite3.so.0"
RDEPENDS_PackageKit-glib = "dbus glib2 glibc gobject-introspection libgcc sqlite-libs"
RPM_SONAME_REQ_PackageKit-glib-devel = "libpackagekit-glib2.so.18"
RPROVIDES_PackageKit-glib-devel = "PackageKit-glib-dev (= 1.1.12)"
RDEPENDS_PackageKit-glib-devel = "PackageKit-glib dbus-devel glib2-devel pkgconf-pkg-config sqlite-devel"
RPM_SONAME_REQ_PackageKit-gstreamer-plugin = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstreamer-1.0.so.0 libpackagekit-glib2.so.18 libsqlite3.so.0"
RDEPENDS_PackageKit-gstreamer-plugin = "PackageKit-glib glib2 glibc gstreamer1 libgcc sqlite-libs"
RPM_SONAME_PROV_PackageKit-gtk3-module = "libpk-gtk-module.so"
RPM_SONAME_REQ_PackageKit-gtk3-module = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_PackageKit-gtk3-module = "PackageKit-glib atk cairo cairo-gobject fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 gtk3 libgcc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/PackageKit-1.1.12-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/PackageKit-command-not-found-1.1.12-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/PackageKit-cron-1.1.12-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/PackageKit-glib-1.1.12-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/PackageKit-gstreamer-plugin-1.1.12-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/PackageKit-gtk3-module-1.1.12-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/PackageKit-glib-devel-1.1.12-4.el8.x86_64.rpm \
          "

SRC_URI[PackageKit.sha256sum] = "3e8a1e4b8380fde65efdcd69ac6a73decfb18d4d08d1cfb6f604887ed83601c9"
SRC_URI[PackageKit-command-not-found.sha256sum] = "988e4c086351ebddde10721379e07d7788d1bb991c999202f16bf53c144081bc"
SRC_URI[PackageKit-cron.sha256sum] = "09d38314166379da36d46066b1f53f9dd3a4bbcd5e31ce254bea66d029219d6d"
SRC_URI[PackageKit-glib.sha256sum] = "bf2ea548574e79f571b349d60151b2e69858603a867061bd873228a4edc57cad"
SRC_URI[PackageKit-glib-devel.sha256sum] = "4f866e6593fa2adaa4daccd6b4c240328dfdaaf12090e8809e15282f224e4fb7"
SRC_URI[PackageKit-gstreamer-plugin.sha256sum] = "cdf4379029879e9f5ce2a3509165277315d08895524505847ed4a71ea06b4951"
SRC_URI[PackageKit-gtk3-module.sha256sum] = "337ac01df47c4ef4cedad1cdcd4ceafc32ee54e02546e655bb062940665ee7a7"
