SUMMARY = "generated recipe based on libXxf86dga srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXxf86dga = "libXxf86dga.so.1"
RPM_SONAME_REQ_libXxf86dga = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXxf86dga = "glibc libX11 libXext"
RPM_SONAME_REQ_libXxf86dga-devel = "libXxf86dga.so.1"
RPROVIDES_libXxf86dga-devel = "libXxf86dga-dev (= 1.1.4)"
RDEPENDS_libXxf86dga-devel = "libX11-devel libXext-devel libXxf86dga pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXxf86dga-1.1.4-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXxf86dga-devel-1.1.4-12.el8.x86_64.rpm \
          "

SRC_URI[libXxf86dga.sha256sum] = "7e9e564425d60986e9844463e6b9f0fb107e5b771d0c1eed2964fdfa8fe9f7e7"
SRC_URI[libXxf86dga-devel.sha256sum] = "86dbe58a8782f64f1c747b2e28f51dc701cbec6c2e99ad4ba5bc9f6a1f274d4a"
