SUMMARY = "generated recipe based on automake srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_automake = "autoconf bash perl-Carp perl-Errno perl-Exporter perl-File-Path perl-Getopt-Long perl-IO perl-PathTools perl-Thread-Queue perl-constant perl-interpreter perl-libs perl-threads"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/automake-1.16.1-6.el8.noarch.rpm \
          "
SRC_URI = "file://automake-1.16.1-6.el8.patch"

SRC_URI[automake.sha256sum] = "0d6116025284dd9ab6e380d3e2b617af0907e721b2d51cc92e062d7de0b06352"
