SUMMARY = "generated recipe based on mc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gpm pkgconfig-native slang"
RPM_SONAME_REQ_mc = "libc.so.6 libglib-2.0.so.0 libgmodule-2.0.so.0 libgpm.so.2 libpthread.so.0 libslang.so.2"
RDEPENDS_mc = "bash glib2 glibc gpm-libs perl-File-Temp perl-interpreter perl-libs slang"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mc-4.8.19-9.el8.x86_64.rpm \
          "

SRC_URI[mc.sha256sum] = "d5730d451c766c9b46f507402023f503563e419abab1536a5b375a45bef5bc0a"
