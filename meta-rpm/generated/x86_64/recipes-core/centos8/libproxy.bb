SUMMARY = "generated recipe based on libproxy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgcc libmodman pkgconfig-native"
RPM_SONAME_PROV_libproxy = "libproxy.so.1"
RPM_SONAME_REQ_libproxy = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmodman.so.1 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libproxy = "glibc libgcc libmodman libstdc++"
RPM_SONAME_REQ_libproxy-bin = "libc.so.6 libproxy.so.1"
RDEPENDS_libproxy-bin = "glibc libproxy"
RPM_SONAME_REQ_libproxy-devel = "libproxy.so.1"
RPROVIDES_libproxy-devel = "libproxy-dev (= 0.4.15)"
RDEPENDS_libproxy-devel = "libproxy pkgconf-pkg-config"
RPM_SONAME_REQ_libproxy-gnome = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libproxy.so.1 libstdc++.so.6"
RDEPENDS_libproxy-gnome = "glib2 glibc libgcc libproxy libstdc++"
RDEPENDS_python3-libproxy = "libproxy platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libproxy-bin-0.4.15-5.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libproxy-gnome-0.4.15-5.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libproxy-0.4.15-5.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libproxy-0.4.15-5.2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libproxy-devel-0.4.15-5.2.el8.x86_64.rpm \
          "

SRC_URI[libproxy.sha256sum] = "c9597eecf39a25497b2ac3c69bc9777eda05b9eaa6d5d29d004a81d71a45d0d7"
SRC_URI[libproxy-bin.sha256sum] = "1dcd657beabe1de9c6eed7e15218f378591214917f35907d7f009922073d2601"
SRC_URI[libproxy-devel.sha256sum] = "12a69ae8b51696f811037a1c0bf9405f6c35335857c0cc81f0a95e37edc187d1"
SRC_URI[libproxy-gnome.sha256sum] = "569337e5ff59afb803da3aa4ec2e740828dd524addd9442636f95123caf9d57c"
SRC_URI[python3-libproxy.sha256sum] = "bdb4f750253d06adbf3a44c571068d8248b2a850e0e453712764cbf0e394d236"
