SUMMARY = "generated recipe based on mingw-bzip2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-bzip2 = "mingw32-crt mingw32-filesystem mingw32-pkg-config"
RDEPENDS_mingw32-bzip2-static = "mingw32-bzip2"
RDEPENDS_mingw64-bzip2 = "mingw64-crt mingw64-filesystem mingw64-pkg-config"
RDEPENDS_mingw64-bzip2-static = "mingw64-bzip2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-bzip2-1.0.6-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-bzip2-static-1.0.6-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-bzip2-1.0.6-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-bzip2-static-1.0.6-12.el8.noarch.rpm \
          "

SRC_URI[mingw32-bzip2.sha256sum] = "98e781ce17dd2e03707425c56a3928c0b0e40fe73ead5076d86aed0e65a9c107"
SRC_URI[mingw32-bzip2-static.sha256sum] = "1f03e5e98ac1d95f84e4253899e728e6e7dc52964bdd6e574bea4cc3e1ba6bec"
SRC_URI[mingw64-bzip2.sha256sum] = "9baf0a2775c278de768c95c1a4680639793d307781e84ac16ef2d5c83c563fa8"
SRC_URI[mingw64-bzip2-static.sha256sum] = "7f34f9b90ed9302652c01c69bbd4ca61946f6aaeb1c917adc3e32f43f0038f84"
