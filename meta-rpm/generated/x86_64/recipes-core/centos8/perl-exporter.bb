SUMMARY = "generated recipe based on perl-Exporter srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Exporter = "perl-Carp perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Exporter-5.72-396.el8.noarch.rpm \
          "

SRC_URI[perl-Exporter.sha256sum] = "7edc503f5a919c489b651757095d8031982d530cc88088fdaeb743188364e9b0"
