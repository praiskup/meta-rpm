SUMMARY = "generated recipe based on libucil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib fontconfig freetype glib-2.0 libogg libpng libtheora libunicap libvorbis pango pkgconfig-native zlib"
RPM_SONAME_PROV_libucil = "libucil.so.2"
RPM_SONAME_REQ_libucil = "libasound.so.2 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libogg.so.0 libpango-1.0.so.0 libpangoft2-1.0.so.0 libpng16.so.16 librt.so.1 libtheora.so.0 libunicap.so.2 libvorbis.so.0 libvorbisenc.so.2 libz.so.1"
RDEPENDS_libucil = "alsa-lib fontconfig freetype glib2 glibc libogg libpng libtheora libunicap libvorbis pango zlib"
RPM_SONAME_REQ_libucil-devel = "libucil.so.2"
RPROVIDES_libucil-devel = "libucil-dev (= 0.9.10)"
RDEPENDS_libucil-devel = "glib2-devel libucil libunicap-devel pango-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libucil-0.9.10-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libucil-devel-0.9.10-16.el8.x86_64.rpm \
          "

SRC_URI[libucil.sha256sum] = "82977db6a93e49013cdb398ca711c9a59e1cded5e7995ef40ac2abc449f85b98"
SRC_URI[libucil-devel.sha256sum] = "871a5f9a647e21d5b7d77602df94ae7890fde943756e9247a3adb50acc1a54c0"
