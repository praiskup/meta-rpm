SUMMARY = "generated recipe based on gsettings-desktop-schemas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gsettings-desktop-schemas = "glib2"
RPROVIDES_gsettings-desktop-schemas-devel = "gsettings-desktop-schemas-dev (= 3.32.0)"
RDEPENDS_gsettings-desktop-schemas-devel = "gsettings-desktop-schemas pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gsettings-desktop-schemas-devel-3.32.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gsettings-desktop-schemas-3.32.0-4.el8.x86_64.rpm \
          "

SRC_URI[gsettings-desktop-schemas.sha256sum] = "8fdcc92fb423c8d948341ac9af2a945fb2051bbb6d85e0ec7f96f2793cc78d55"
SRC_URI[gsettings-desktop-schemas-devel.sha256sum] = "dd29935ca6e99228f3a3a0e493e8962cb6c4f9ed9587a44e56964dc29cc75547"
