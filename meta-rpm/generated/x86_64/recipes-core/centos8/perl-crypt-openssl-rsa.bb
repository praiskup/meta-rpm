SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-RSA srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-RSA = "libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0 libssl.so.1.1"
RDEPENDS_perl-Crypt-OpenSSL-RSA = "glibc openssl-libs perl-Carp perl-Crypt-OpenSSL-Bignum perl-Crypt-OpenSSL-Random perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Crypt-OpenSSL-RSA-0.31-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-RSA.sha256sum] = "e7742a5f3741bbb733233b5650eb82b6e6e41715a6d2b662f52cf0a1b170c1ae"
