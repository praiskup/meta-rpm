SUMMARY = "generated recipe based on gcc-toolset-9-annobin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-annobin = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-annobin = "bash gcc-toolset-9-gcc gcc-toolset-9-runtime glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-annobin-9.08-4.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-annobin.sha256sum] = "e3d2d06b93709745e8ddb987a6ab048f5044884e96f08ebe8dcf5b35b8172d42"
