SUMMARY = "generated recipe based on glm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_glm-devel = "glm-dev (= 0.9.8.5)"
RDEPENDS_glm-devel = "cmake-filesystem pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glm-devel-0.9.8.5-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glm-doc-0.9.8.5-2.el8.noarch.rpm \
          "

SRC_URI[glm-devel.sha256sum] = "3e6d5444194c49dec05411ae4d041404b96173c7c2e3b3b98714ad276c2c3290"
SRC_URI[glm-doc.sha256sum] = "f37e0be2ada53b1b5084edd91898b560b5c1071bf326931e98bfabb6a9f4ae3a"
