SUMMARY = "generated recipe based on hunspell-la srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-la = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-la-0.20130331-11.el8.noarch.rpm \
          "

SRC_URI[hunspell-la.sha256sum] = "82321509744f5f8b602d37ac1fff808c22989aa8b99a7270870c8e2307e18673"
