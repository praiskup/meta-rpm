SUMMARY = "generated recipe based on udisks2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl glib-2.0 iscsi-initiator-utils libatasmart libblockdev libgcc libgudev libmount pkgconfig-native polkit systemd-libs"
RPM_SONAME_PROV_libudisks2 = "libudisks2.so.0"
RPM_SONAME_REQ_libudisks2 = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_libudisks2 = "glib2 glibc"
RPM_SONAME_REQ_udisks2 = "ld-linux-x86-64.so.2 libacl.so.1 libatasmart.so.4 libbd_utils.so.2 libblockdev.so.2 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmount.so.1 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsystemd.so.0 libudisks2.so.0"
RDEPENDS_udisks2 = "bash dbus dosfstools e2fsprogs gdisk glib2 glibc libacl libatasmart libblockdev libblockdev-crypto libblockdev-fs libblockdev-loop libblockdev-mdraid libblockdev-part libblockdev-swap libblockdev-utils libgcc libgudev libmount libudisks2 polkit-libs systemd systemd-libs systemd-udev util-linux xfsprogs"
RPM_SONAME_PROV_udisks2-iscsi = "libudisks2_iscsi.so"
RPM_SONAME_REQ_udisks2-iscsi = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libiscsi.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_udisks2-iscsi = "glib2 glibc iscsi-initiator-utils libgudev polkit-libs udisks2"
RPM_SONAME_PROV_udisks2-lvm2 = "libudisks2_lvm2.so"
RPM_SONAME_REQ_udisks2-lvm2 = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_udisks2-lvm2 = "glib2 glibc libblockdev-lvm libgudev lvm2 polkit-libs udisks2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libudisks2-2.8.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/udisks2-2.8.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/udisks2-iscsi-2.8.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/udisks2-lvm2-2.8.3-2.el8.x86_64.rpm \
          "

SRC_URI[libudisks2.sha256sum] = "1743ddd52d830f1452e9b2af41daecf9282e6ca1305b7ede10d909f59874eca6"
SRC_URI[udisks2.sha256sum] = "45be49a047fac8b1dbee393fe6527170584bf192803507806abbcc3a02c8854c"
SRC_URI[udisks2-iscsi.sha256sum] = "1ea622d3b99eb87f193b3377df478123501b037972d5af814354d6af5d6f7e0b"
SRC_URI[udisks2-lvm2.sha256sum] = "a1a97036965d785dd98e83fa34784476dae1037a7e9c418da6decf6709632100"
