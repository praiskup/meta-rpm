SUMMARY = "generated recipe based on perl-IO-stringy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-stringy = "perl-Carp perl-Exporter perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-IO-stringy-2.111-9.el8.noarch.rpm \
          "

SRC_URI[perl-IO-stringy.sha256sum] = "1541a76c75250f7ba7ae7c1782f6f505b37e622c54193f6e0c83d96de868e453"
