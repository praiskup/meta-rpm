SUMMARY = "generated recipe based on qt5-qtserialbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtserialport"
RPM_SONAME_PROV_qt5-qtserialbus = "libQt5SerialBus.so.5 libqtpassthrucanbus.so libqtpeakcanbus.so libqtsocketcanbus.so libqttinycanbus.so libqtvirtualcanbus.so"
RPM_SONAME_REQ_qt5-qtserialbus = "libQt5Core.so.5 libQt5Network.so.5 libQt5SerialBus.so.5 libQt5SerialPort.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtserialbus = "glibc libgcc libstdc++ qt5-qtbase qt5-qtserialport"
RPM_SONAME_REQ_qt5-qtserialbus-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5SerialBus.so.5 libQt5SerialPort.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtserialbus-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtserialbus qt5-qtserialport"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtserialbus-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtserialbus-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtserialbus.sha256sum] = "1d5c9d4eb3c20afaa0b32fe24e3f93f70e14ef1585b458432e0bd040c45c93c3"
SRC_URI[qt5-qtserialbus-examples.sha256sum] = "37986a26cd25f87e9dac6d8c2e13ae66dd6ae7c710420cd4b1c2859006f5fff7"
