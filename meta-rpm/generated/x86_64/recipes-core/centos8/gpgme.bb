SUMMARY = "generated recipe based on gpgme srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libassuan libgcc libgpg-error pkgconfig-native platform-python3"
RPM_SONAME_PROV_gpgme = "libgpgme.so.11"
RPM_SONAME_REQ_gpgme = "ld-linux-x86-64.so.2 libassuan.so.0 libc.so.6 libgpg-error.so.0"
RDEPENDS_gpgme = "glibc gnupg2 libassuan libgpg-error"
RPM_SONAME_REQ_gpgme-devel = "libassuan.so.0 libc.so.6 libgpg-error.so.0 libgpgme.so.11"
RPROVIDES_gpgme-devel = "gpgme-dev (= 1.10.0)"
RDEPENDS_gpgme-devel = "bash glibc gpgme info libassuan libgpg-error libgpg-error-devel"
RPM_SONAME_PROV_gpgmepp = "libgpgmepp.so.6"
RPM_SONAME_REQ_gpgmepp = "libassuan.so.0 libc.so.6 libgcc_s.so.1 libgpg-error.so.0 libgpgme.so.11 libm.so.6 libstdc++.so.6"
RDEPENDS_gpgmepp = "glibc gpgme libassuan libgcc libgpg-error libstdc++"
RPM_SONAME_REQ_gpgmepp-devel = "libgpgmepp.so.6"
RPROVIDES_gpgmepp-devel = "gpgmepp-dev (= 1.10.0)"
RDEPENDS_gpgmepp-devel = "cmake-filesystem gpgme-devel gpgmepp"
RPM_SONAME_REQ_python3-gpg = "libc.so.6 libgpgme.so.11 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-gpg = "glibc gpgme platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gpgme-1.10.0-6.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gpgmepp-1.10.0-6.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-gpg-1.10.0-6.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gpgme-devel-1.10.0-6.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gpgmepp-devel-1.10.0-6.el8.0.1.x86_64.rpm \
          "

SRC_URI[gpgme.sha256sum] = "602f0c27e1f612a9f099152678b7ebb7a273c0ce6da70f5ad3e688c0d0461dc2"
SRC_URI[gpgme-devel.sha256sum] = "ab55f3a4a39299503f7deb776621a3d9f9963c5afa3b0c69e273060c0077900e"
SRC_URI[gpgmepp.sha256sum] = "d5db8fc59fe5aff48c6b18f30acb51e964f43d17678f2836d783523346b0dd3e"
SRC_URI[gpgmepp-devel.sha256sum] = "10dfef0ecf633190cf0742bae74d901ad5f8be1edf0534fbaba169c04423f2c4"
SRC_URI[python3-gpg.sha256sum] = "5e44412bcf9dae2d330d854a00f62379a6784eed0c8b2ca715586586683d70c0"
