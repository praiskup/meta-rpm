SUMMARY = "generated recipe based on libijs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libijs = "libijs-0.35.so"
RPM_SONAME_REQ_libijs = "libc.so.6"
RDEPENDS_libijs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libijs-0.35-5.el8.x86_64.rpm \
          "

SRC_URI[libijs.sha256sum] = "2f90e2ee809dad24fc29296a838ca8fe38a9be4ec977cae381debe51e4a6406b"
