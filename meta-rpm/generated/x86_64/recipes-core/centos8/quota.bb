SUMMARY = "generated recipe based on quota srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs e2fsprogs libnl libtirpc openldap pkgconfig-native"
RPM_SONAME_REQ_quota = "libc.so.6 libcom_err.so.2 libext2fs.so.2 libtirpc.so.3"
RDEPENDS_quota = "e2fsprogs-libs glibc libcom_err libtirpc quota-nls"
RDEPENDS_quota-doc = "quota"
RPM_SONAME_REQ_quota-nld = "libc.so.6 libdbus-1.so.3 libnl-3.so.200 libnl-genl-3.so.200 libtirpc.so.3"
RDEPENDS_quota-nld = "bash dbus-libs glibc libnl3 libtirpc quota-nls systemd"
RPM_SONAME_REQ_quota-rpc = "libc.so.6 libtirpc.so.3"
RDEPENDS_quota-rpc = "bash glibc libtirpc quota-nls rpcbind systemd"
RPM_SONAME_REQ_quota-warnquota = "libc.so.6 liblber-2.4.so.2 libldap-2.4.so.2 libtirpc.so.3"
RDEPENDS_quota-warnquota = "glibc libtirpc openldap quota-nls"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/quota-4.04-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/quota-doc-4.04-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/quota-nld-4.04-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/quota-nls-4.04-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/quota-rpc-4.04-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/quota-warnquota-4.04-10.el8.x86_64.rpm \
          "

SRC_URI[quota.sha256sum] = "0cf4afd40b73603ed81bb24ce96b2fe2a0e077c9eaa3d5ebee204e8f8e661172"
SRC_URI[quota-doc.sha256sum] = "ef0775544e977ba8747d1c4354f69cf69261b82c6cbe4e229c9fe6e3c8855edf"
SRC_URI[quota-nld.sha256sum] = "52fc282c9e0c14fb959be2dcbc09eb2c0cc3a9dca0f787f10e040dadccc3a218"
SRC_URI[quota-nls.sha256sum] = "35f810182b9605123bb81da116e441c254c01726607477188f18e07bf0582ee1"
SRC_URI[quota-rpc.sha256sum] = "f2e5cfc9145a219f155692fb12d26373c80487d8d1b034f9ff529fef8fdbf1ce"
SRC_URI[quota-warnquota.sha256sum] = "56cf7d467fe25eb4476a8d5143d34c76748f540a70bf8e71f6d1f4dddd059501"
