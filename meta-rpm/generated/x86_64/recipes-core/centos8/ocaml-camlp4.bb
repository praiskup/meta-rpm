SUMMARY = "generated recipe based on ocaml-camlp4 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_ocaml-camlp4 = "ocaml-compiler-libs ocaml-runtime"
RPM_SONAME_REQ_ocaml-camlp4-devel = "libc.so.6 libdl.so.2 libm.so.6"
RPROVIDES_ocaml-camlp4-devel = "ocaml-camlp4-dev (= 4.07.0)"
RDEPENDS_ocaml-camlp4-devel = "glibc ocaml-camlp4 ocaml-runtime"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-camlp4-4.07.0-0.gitd32d9973.1.el8.3.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-camlp4-devel-4.07.0-0.gitd32d9973.1.el8.3.x86_64.rpm \
          "

SRC_URI[ocaml-camlp4.sha256sum] = "51748881f28eacdbc7ec564e0d60492828275d5db0550e0f0d3f68854f2f9262"
SRC_URI[ocaml-camlp4-devel.sha256sum] = "db55d99a16915eb92d11484cda796a66d3618472e1b73189dddb08be97986228"
