SUMMARY = "generated recipe based on libspectre srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ghostscript pkgconfig-native"
RPM_SONAME_PROV_libspectre = "libspectre.so.1"
RPM_SONAME_REQ_libspectre = "libc.so.6 libgs.so.9"
RDEPENDS_libspectre = "glibc libgs"
RPM_SONAME_REQ_libspectre-devel = "libspectre.so.1"
RPROVIDES_libspectre-devel = "libspectre-dev (= 0.2.8)"
RDEPENDS_libspectre-devel = "libspectre pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libspectre-0.2.8-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libspectre-devel-0.2.8-5.el8.x86_64.rpm \
          "

SRC_URI[libspectre.sha256sum] = "bbd9cef1860cb7b6ec72d3d67761f1b28592e8fae0d65ab7704e650085e7c560"
SRC_URI[libspectre-devel.sha256sum] = "6c8e1c03f81e02edf55daa7dea89dc30ec1d5c7568ec014cd1d5c70190f6a311"
