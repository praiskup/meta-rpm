SUMMARY = "generated recipe based on opa-fm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libgcc openssl pkgconfig-native rdma-core zlib"
RPM_SONAME_REQ_opa-fm = "libc.so.6 libcrypto.so.1.1 libexpat.so.1 libgcc_s.so.1 libgomp.so.1 libibumad.so.3 libibverbs.so.1 libpthread.so.0 librt.so.1 libssl.so.1.1 libz.so.1"
RDEPENDS_opa-fm = "bash expat glibc libgcc libgomp libibumad libibverbs openssl-libs systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opa-fm-10.10.0.0.444-2.el8.x86_64.rpm \
          "

SRC_URI[opa-fm.sha256sum] = "6b5bfc7383da5c97e035bcee40c91e66905f325c0b78b8e978a4494f62efedee"
