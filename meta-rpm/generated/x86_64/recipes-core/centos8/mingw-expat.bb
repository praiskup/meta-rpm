SUMMARY = "generated recipe based on mingw-expat srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-expat = "mingw32-crt mingw32-filesystem mingw32-pkg-config"
RDEPENDS_mingw64-expat = "mingw64-crt mingw64-filesystem mingw64-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-expat-2.2.4-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-expat-2.2.4-3.el8.noarch.rpm \
          "

SRC_URI[mingw32-expat.sha256sum] = "5827c709756be57f6994ece4fdb4922521e0632aaab5682ad0af25ce848e0c95"
SRC_URI[mingw64-expat.sha256sum] = "7cd495b1ddbec946a19eae2d6797ec1c333659682f0b01a523247f879aa2de92"
