SUMMARY = "generated recipe based on NetworkManager srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit bluez curl glib-2.0 gnutls jansson libgcc libndp libnewt libselinux libteam libuuid modemmanager pkgconfig-native polkit readline systemd-libs"
RPM_SONAME_PROV_NetworkManager = "libnm-settings-plugin-ifcfg-rh.so"
RPM_SONAME_REQ_NetworkManager = "ld-linux-x86-64.so.2 libaudit.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libndp.so.0 libnm.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libreadline.so.7 libselinux.so.1 libsystemd.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager = "NetworkManager-libnm audit-libs bash chkconfig dbus glib2 glibc gnutls libcurl libgcc libndp libselinux libuuid polkit-libs readline systemd systemd-libs"
RPM_SONAME_PROV_NetworkManager-adsl = "libnm-device-plugin-adsl.so"
RPM_SONAME_REQ_NetworkManager-adsl = "libc.so.6 libdl.so.2 libgcc_s.so.1 libudev.so.1"
RDEPENDS_NetworkManager-adsl = "NetworkManager glibc libgcc systemd-libs"
RPM_SONAME_PROV_NetworkManager-bluetooth = "libnm-device-plugin-bluetooth.so"
RPM_SONAME_REQ_NetworkManager-bluetooth = "libbluetooth.so.3 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libmm-glib.so.0 libnm-wwan.so libpthread.so.0 libsystemd.so.0"
RDEPENDS_NetworkManager-bluetooth = "ModemManager-glib NetworkManager NetworkManager-wwan bluez bluez-libs glib2 glibc libgcc systemd-libs"
RPM_SONAME_REQ_NetworkManager-cloud-setup = "ld-linux-x86-64.so.2 libc.so.6 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libnm.so.0 libpthread.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager-cloud-setup = "NetworkManager NetworkManager-libnm bash glib2 glibc gnutls libcurl libgcc libuuid systemd-libs"
RDEPENDS_NetworkManager-dispatcher-routing-rules = "bash"
RPM_SONAME_PROV_NetworkManager-libnm = "libnm.so.0"
RPM_SONAME_REQ_NetworkManager-libnm = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libpthread.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager-libnm = "glib2 glibc gnutls libgcc libuuid systemd-libs"
RPM_SONAME_REQ_NetworkManager-libnm-devel = "libnm.so.0"
RPROVIDES_NetworkManager-libnm-devel = "NetworkManager-libnm-dev (= 1.22.8)"
RDEPENDS_NetworkManager-libnm-devel = "NetworkManager-libnm glib2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_NetworkManager-ovs = "libnm-device-plugin-ovs.so"
RPM_SONAME_REQ_NetworkManager-ovs = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjansson.so.4 libpthread.so.0"
RDEPENDS_NetworkManager-ovs = "NetworkManager glib2 glibc jansson libgcc"
RPM_SONAME_PROV_NetworkManager-ppp = "libnm-ppp-plugin.so"
RPM_SONAME_REQ_NetworkManager-ppp = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_NetworkManager-ppp = "NetworkManager glib2 glibc libgcc ppp"
RPM_SONAME_PROV_NetworkManager-team = "libnm-device-plugin-team.so"
RPM_SONAME_REQ_NetworkManager-team = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjansson.so.4 libpthread.so.0 libteamdctl.so.0"
RDEPENDS_NetworkManager-team = "NetworkManager glib2 glibc jansson libgcc teamd"
RPM_SONAME_REQ_NetworkManager-tui = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libnewt.so.0.52 libnm.so.0 libpthread.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager-tui = "NetworkManager NetworkManager-libnm glib2 glibc gnutls libgcc libuuid newt systemd-libs"
RPM_SONAME_PROV_NetworkManager-wifi = "libnm-device-plugin-wifi.so"
RPM_SONAME_REQ_NetworkManager-wifi = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_NetworkManager-wifi = "NetworkManager glib2 glibc libgcc wpa_supplicant"
RPM_SONAME_PROV_NetworkManager-wwan = "libnm-device-plugin-wwan.so libnm-wwan.so"
RPM_SONAME_REQ_NetworkManager-wwan = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libmm-glib.so.0 libnm-wwan.so libpthread.so.0 libsystemd.so.0"
RDEPENDS_NetworkManager-wwan = "ModemManager ModemManager-glib NetworkManager glib2 glibc libgcc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/NetworkManager-cloud-setup-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-adsl-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-bluetooth-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-config-connectivity-redhat-1.22.8-5.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-config-server-1.22.8-5.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-dispatcher-routing-rules-1.22.8-5.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-libnm-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-ovs-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-ppp-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-team-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-tui-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-wifi-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/NetworkManager-wwan-1.22.8-5.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/NetworkManager-libnm-devel-1.22.8-5.el8_2.x86_64.rpm \
          "

SRC_URI[NetworkManager.sha256sum] = "b24d1851330aaf944f91a28ca4a8b8d339dd4a2f8bf01a48923174ca6074d9b2"
SRC_URI[NetworkManager-adsl.sha256sum] = "dc4cc9574a00e57d2020de05d1aca3aa05de9157c85484e8f66e0cd50ef4e554"
SRC_URI[NetworkManager-bluetooth.sha256sum] = "7eea0c9437cdb8018587a21fd5def2aa46ebc2ac4b0c326c3d8fc9c3ac8a4617"
SRC_URI[NetworkManager-cloud-setup.sha256sum] = "c4e89b2e407753860300e7160208c25f3643b1e4d22df81e71e43616e936e45c"
SRC_URI[NetworkManager-config-connectivity-redhat.sha256sum] = "631c75aac48e9932bb715a2577ab2b8cab6a86d8f795eabd31cf706a4a17de19"
SRC_URI[NetworkManager-config-server.sha256sum] = "0c0e3a1251185fc0418360105454f437617e15bd5c1b37f0b43a8861402bfb4c"
SRC_URI[NetworkManager-dispatcher-routing-rules.sha256sum] = "3f24eb848d077955dbad5570da37132b27e7b916241783f195afedf52e190001"
SRC_URI[NetworkManager-libnm.sha256sum] = "fc638cf806dc0b210498b1d4bcfec32e76fc70fd49660c2b2bcf06e3ec439046"
SRC_URI[NetworkManager-libnm-devel.sha256sum] = "d42edb8eb02a4ea5943715aaa425b0e2f605b330ee5bdac97ea2ab869507d311"
SRC_URI[NetworkManager-ovs.sha256sum] = "49c2d58c4cde8d91dc0ac5eea1e09ebb8817a08c9b55ca05c5c270cbd0a70cf8"
SRC_URI[NetworkManager-ppp.sha256sum] = "f1f43c36b64c42eea91f78914782dc84f624187afac715670bd500b1e8e81dbd"
SRC_URI[NetworkManager-team.sha256sum] = "ec6fec63a150da07d52028d009aca85d013ce3a16fe2a8aef75ea6bdb8f6b1ff"
SRC_URI[NetworkManager-tui.sha256sum] = "07401776373ade81c44a3fc6781dc2a0e1cda56e3dcb19b0aa1f92f07fe84e44"
SRC_URI[NetworkManager-wifi.sha256sum] = "3a875b5cd4ef5fb1b0a1a6332b0a6f6a0acee5a49265b78e23fcd417b2f8b842"
SRC_URI[NetworkManager-wwan.sha256sum] = "462a4eafbb40d532636c89a92d577e16d89d7c9a42015a9b61a83cc2bd94668a"
