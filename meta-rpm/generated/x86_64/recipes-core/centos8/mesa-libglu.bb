SUMMARY = "generated recipe based on mesa-libGLU srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native"
RPM_SONAME_PROV_mesa-libGLU = "libGLU.so.1"
RPM_SONAME_REQ_mesa-libGLU = "libGL.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_mesa-libGLU = "glibc libgcc libglvnd-glx libstdc++"
RPM_SONAME_REQ_mesa-libGLU-devel = "libGLU.so.1"
RPROVIDES_mesa-libGLU-devel = "mesa-libGLU-dev (= 9.0.0)"
RDEPENDS_mesa-libGLU-devel = "gl-manpages libglvnd-devel mesa-libGLU pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libGLU-9.0.0-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libGLU-devel-9.0.0-15.el8.x86_64.rpm \
          "

SRC_URI[mesa-libGLU.sha256sum] = "57512b0d27f8861e7f84cb780de2b92d7109a95d1e14ebef1c08af29270e649d"
SRC_URI[mesa-libGLU-devel.sha256sum] = "0aba026b386d1d8541342ad50e4dee9db7cc74c4a9a8a3d876393a19b670aed1"
