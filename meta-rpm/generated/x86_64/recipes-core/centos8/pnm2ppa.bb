SUMMARY = "generated recipe based on pnm2ppa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_pnm2ppa = "libc.so.6 libm.so.6"
RDEPENDS_pnm2ppa = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pnm2ppa-1.04-40.el8.x86_64.rpm \
          "

SRC_URI[pnm2ppa.sha256sum] = "f9d627ef58dcf21b35cd14d3778dd2e60814bd1f4b83febcb35ca84b6505a437"
