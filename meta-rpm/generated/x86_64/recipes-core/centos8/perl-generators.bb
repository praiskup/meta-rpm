SUMMARY = "generated recipe based on perl-generators srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-generators = "perl-Fedora-VSP perl-interpreter perl-libs perl-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-generators-1.10-9.el8.noarch.rpm \
          "

SRC_URI[perl-generators.sha256sum] = "3aad9996ecb4de9508c858fe255c9aa40b309bd48cb6947d363bf0e0658b267b"
