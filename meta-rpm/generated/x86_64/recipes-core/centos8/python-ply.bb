SUMMARY = "generated recipe based on python-ply srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-ply = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-ply-3.9-8.el8.noarch.rpm \
          "

SRC_URI[python3-ply.sha256sum] = "aa7a74c4c58c00f98ebbaf24d7dd84499c908760246194f8d58b4518bd78bcce"
