SUMMARY = "generated recipe based on mdadm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mdadm = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_mdadm = "bash chkconfig coreutils glibc libreport-filesystem systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mdadm-4.1-13.el8.x86_64.rpm \
          "

SRC_URI[mdadm.sha256sum] = "c0fdb7887239752074760d1aa3d8bc78a729e7def1c6e5a7a13b10bf727702fd"
