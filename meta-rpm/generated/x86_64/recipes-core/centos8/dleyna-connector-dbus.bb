SUMMARY = "generated recipe based on dleyna-connector-dbus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dleyna-core glib-2.0 gupnp pkgconfig-native"
RPM_SONAME_REQ_dleyna-connector-dbus = "libc.so.6 libdleyna-core-1.0.so.5 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgupnp-1.0.so.4 libpthread.so.0"
RDEPENDS_dleyna-connector-dbus = "dleyna-core glib2 glibc gupnp"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dleyna-connector-dbus-0.3.0-2.el8.x86_64.rpm \
          "

SRC_URI[dleyna-connector-dbus.sha256sum] = "3505f9ab54b7334b80af0ceb0f2ffdfb60029239edfc37a9e96719199c73eefe"
