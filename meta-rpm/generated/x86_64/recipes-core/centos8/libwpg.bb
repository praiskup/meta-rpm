SUMMARY = "generated recipe based on libwpg srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc librevenge libwpd pkgconfig-native"
RPM_SONAME_PROV_libwpg = "libwpg-0.3.so.3"
RPM_SONAME_REQ_libwpg = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libwpd-0.10.so.10"
RDEPENDS_libwpg = "glibc libgcc librevenge libstdc++ libwpd"
RPM_SONAME_REQ_libwpg-devel = "libwpg-0.3.so.3"
RPROVIDES_libwpg-devel = "libwpg-dev (= 0.3.2)"
RDEPENDS_libwpg-devel = "librevenge-devel libwpd-devel libwpg pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libwpg-0.3.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwpg-devel-0.3.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libwpg-doc-0.3.2-1.el8.noarch.rpm \
          "

SRC_URI[libwpg.sha256sum] = "36d2ccf2505597845338cd5775dff12092210dcec9e0e0ac289ac95ae01d0c02"
SRC_URI[libwpg-devel.sha256sum] = "11d5f331fbbabf233e90d70189d827ccf294755520bbe270a0f2f5e490f5e64e"
SRC_URI[libwpg-doc.sha256sum] = "c869a5cdf6239106e93083a331972d943968cfff9ab7691f4b3aa52e35163bb2"
