SUMMARY = "generated recipe based on libXt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xt"
DEPENDS = "libice libsm libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXt = "libXt.so.6"
RPM_SONAME_REQ_libXt = "libICE.so.6 libSM.so.6 libX11.so.6 libc.so.6"
RDEPENDS_libXt = "glibc libICE libSM libX11"
RPM_SONAME_REQ_libXt-devel = "libXt.so.6"
RPROVIDES_libXt-devel = "libXt-dev (= 1.1.5)"
RDEPENDS_libXt-devel = "libICE-devel libSM-devel libX11-devel libXt pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXt-1.1.5-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXt-devel-1.1.5-12.el8.x86_64.rpm \
          "

SRC_URI[libXt.sha256sum] = "c16aa57ac6bb2f1f2b4fb26c391d31f95217ed1643870b8f9c356ccffddbcee5"
SRC_URI[libXt-devel.sha256sum] = "0c5de551306b77cbdc93de1296041d1379dc0d73b44d36e56269fa242184ad5d"
