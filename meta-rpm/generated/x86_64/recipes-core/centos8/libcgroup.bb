SUMMARY = "generated recipe based on libcgroup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pam pkgconfig-native"
RPM_SONAME_PROV_libcgroup = "libcgroup.so.1"
RPM_SONAME_REQ_libcgroup = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_libcgroup = "bash glibc shadow-utils systemd"
RPM_SONAME_REQ_libcgroup-pam = "libc.so.6 libcgroup.so.1 libpam.so.0 libpthread.so.0"
RDEPENDS_libcgroup-pam = "glibc libcgroup pam"
RPM_SONAME_REQ_libcgroup-tools = "libc.so.6 libcgroup.so.1 libpthread.so.0"
RDEPENDS_libcgroup-tools = "bash glibc libcgroup systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcgroup-0.41-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcgroup-pam-0.41-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcgroup-tools-0.41-19.el8.x86_64.rpm \
          "

SRC_URI[libcgroup.sha256sum] = "8c43ac47aa8469bf24ec71db299439df2daec4a63507a6653542f0a682b9652c"
SRC_URI[libcgroup-pam.sha256sum] = "7ac04bd0a3db7159a140ec8e526b2c2570bcf02a7d24f64dc0f86fc9a6380f25"
SRC_URI[libcgroup-tools.sha256sum] = "11bfef8f5cfa5cca88dbeb8c842b771dbe42ab403e60723322283c95f4169a39"
