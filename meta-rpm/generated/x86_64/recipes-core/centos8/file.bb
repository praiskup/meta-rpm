SUMMARY = "generated recipe based on file srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_REQ_file = "libc.so.6 libmagic.so.1 libz.so.1"
RDEPENDS_file = "file-libs glibc zlib"
RPM_SONAME_REQ_file-devel = "libmagic.so.1"
RPROVIDES_file-devel = "file-dev (= 5.33)"
RDEPENDS_file-devel = "file file-libs"
RPM_SONAME_PROV_file-libs = "libmagic.so.1"
RPM_SONAME_REQ_file-libs = "libc.so.6 libz.so.1"
RDEPENDS_file-libs = "glibc zlib"
RDEPENDS_python3-magic = "file platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/file-5.33-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/file-libs-5.33-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-magic-5.33-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/file-devel-5.33-13.el8.x86_64.rpm \
          "

SRC_URI[file.sha256sum] = "3e3625a0e6d9c528c32eaa9ba70150af4107b80048180e467d579e96d86dd1c4"
SRC_URI[file-devel.sha256sum] = "309368989cef22c28a2cddb257247410522947f96d54c95da5ea2557a417ba11"
SRC_URI[file-libs.sha256sum] = "e27a087f6b5ad69de19776141737d26084ea5c2a61011abaae9501d038101d29"
SRC_URI[python3-magic.sha256sum] = "a42993ea23df5011cab9b18c8c6c148394b6523f2797292303ac99f8f6411d72"
