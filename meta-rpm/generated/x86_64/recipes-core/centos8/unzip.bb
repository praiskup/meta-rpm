SUMMARY = "generated recipe based on unzip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 pkgconfig-native"
RPM_SONAME_REQ_unzip = "libbz2.so.1 libc.so.6"
RDEPENDS_unzip = "bash bzip2-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/unzip-6.0-43.el8.x86_64.rpm \
          "

SRC_URI[unzip.sha256sum] = "dcaca6f8b73c28c45f014fb7a05c61ef2b18c16a5d8b3c084e4ecfde4d830c62"
