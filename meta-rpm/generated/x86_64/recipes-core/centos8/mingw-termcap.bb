SUMMARY = "generated recipe based on mingw-termcap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-termcap = "mingw32-crt mingw32-filesystem"
RDEPENDS_mingw64-termcap = "mingw64-crt mingw64-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-termcap-1.3.1-23.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-termcap-1.3.1-23.el8.noarch.rpm \
          "

SRC_URI[mingw32-termcap.sha256sum] = "30835d1c2d8d56d53e4ec544d549b52b2f180cb946fffde5c1a4c44bf25e29f5"
SRC_URI[mingw64-termcap.sha256sum] = "d6996a664f2b6493d8e5c2fb7f25ed37b261f4d238fe7d53a11158989c5d3c3f"
