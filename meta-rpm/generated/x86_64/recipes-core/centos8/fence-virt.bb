SUMMARY = "generated recipe based on fence-virt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libuuid libvirt libxml2 nss pkgconfig-native xz zlib"
RPM_SONAME_REQ_fence-virt = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libnss3.so libxml2.so.2 libz.so.1"
RDEPENDS_fence-virt = "bash glibc libxml2 nss systemd xz-libs zlib"
RPM_SONAME_REQ_fence-virtd = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libuuid.so.1"
RDEPENDS_fence-virtd = "glibc libgcc libstdc++ libuuid"
RPM_SONAME_PROV_fence-virtd-libvirt = "libvirt.so"
RPM_SONAME_REQ_fence-virtd-libvirt = "libc.so.6 libpthread.so.0 libuuid.so.1 libvirt.so.0"
RDEPENDS_fence-virtd-libvirt = "fence-virtd glibc libuuid libvirt libvirt-libs"
RPM_SONAME_REQ_fence-virtd-multicast = "libc.so.6 libnss3.so libpthread.so.0 libuuid.so.1"
RDEPENDS_fence-virtd-multicast = "fence-virtd glibc libuuid nss"
RPM_SONAME_REQ_fence-virtd-serial = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libpthread.so.0 libuuid.so.1 libvirt.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_fence-virtd-serial = "fence-virtd glibc libuuid libvirt libvirt-libs libxml2 xz-libs zlib"
RPM_SONAME_REQ_fence-virtd-tcp = "libc.so.6 libnss3.so libpthread.so.0 libuuid.so.1"
RDEPENDS_fence-virtd-tcp = "fence-virtd glibc libuuid nss"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fence-virt-0.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fence-virtd-0.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fence-virtd-libvirt-0.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fence-virtd-multicast-0.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fence-virtd-serial-0.4.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fence-virtd-tcp-0.4.0-9.el8.x86_64.rpm \
          "

SRC_URI[fence-virt.sha256sum] = "fdae478e266e914c6937005c4ee7100e14842c917a274333a252255c84a6eaa3"
SRC_URI[fence-virtd.sha256sum] = "eab9570db9f2d2eec06c02be45af737cc395282c0b898d994d1d059b09e3bd95"
SRC_URI[fence-virtd-libvirt.sha256sum] = "e1cf9d0d0475bf43745986832a2eff5f145ccfe2d7a6b43537a2d4fdf075d981"
SRC_URI[fence-virtd-multicast.sha256sum] = "73e9eb6c732bd8ea7c6bbc17910a71a1375f0338e8e82ad2cc963f15cfe8886d"
SRC_URI[fence-virtd-serial.sha256sum] = "15bf492323bc01cd2d14c650fdce7bd74fa4d6a3503386ea45875e51fc79c9fe"
SRC_URI[fence-virtd-tcp.sha256sum] = "525d6a45fb60641a144dccb93d388649d887917b18bd7fb675e4bf3335cd0168"
