SUMMARY = "generated recipe based on gnome-font-viewer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo fontconfig freetype gdk-pixbuf glib-2.0 gnome-desktop3 gtk+3 harfbuzz pango pkgconfig-native"
RPM_SONAME_REQ_gnome-font-viewer = "libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libharfbuzz.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_gnome-font-viewer = "bash cairo cairo-gobject fontconfig freetype gdk-pixbuf2 glib2 glibc gnome-desktop3 gtk3 harfbuzz pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-font-viewer-3.28.0-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-font-viewer.sha256sum] = "c621bea12b7336928ba52a030913bf7de7fc5ddf139ec4b5226a7afd55e4c876"
