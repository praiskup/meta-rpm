SUMMARY = "generated recipe based on mingw-headers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-headers = "mingw32-filesystem mingw32-winpthreads"
RDEPENDS_mingw64-headers = "mingw64-filesystem mingw64-winpthreads"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-headers-5.0.2-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-headers-5.0.2-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-headers.sha256sum] = "2b09bde4e9c14b0d3a0db37ec4d3d04f509223208d1ed5167a65156cd47c7678"
SRC_URI[mingw64-headers.sha256sum] = "2cd52fdf9e1f58f3d5bed40a469670ff7e4153befea20ec5fad17ce303743d6e"
