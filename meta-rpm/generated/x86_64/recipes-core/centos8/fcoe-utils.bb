SUMMARY = "generated recipe based on fcoe-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpciaccess pkgconfig-native"
RPM_SONAME_REQ_fcoe-utils = "libc.so.6 libpciaccess.so.0 librt.so.1"
RDEPENDS_fcoe-utils = "bash device-mapper-multipath glibc iproute libpciaccess lldpad systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fcoe-utils-1.0.32-7.el8.x86_64.rpm \
          "

SRC_URI[fcoe-utils.sha256sum] = "6e1140cdc1c01fd96e5ba292e22774167943a218a36513c3587544ac56f24e87"
