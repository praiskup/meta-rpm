SUMMARY = "generated recipe based on libgsf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 gdk-pixbuf glib-2.0 libxml2 pkgconfig-native zlib"
RPM_SONAME_PROV_libgsf = "libgsf-1.so.114"
RPM_SONAME_REQ_libgsf = "libbz2.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgsf-1.so.114 libxml2.so.2 libz.so.1"
RDEPENDS_libgsf = "bzip2-libs gdk-pixbuf2 glib2 glibc libxml2 zlib"
RPM_SONAME_REQ_libgsf-devel = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgsf-1.so.114 libxml2.so.2"
RPROVIDES_libgsf-devel = "libgsf-dev (= 1.14.41)"
RDEPENDS_libgsf-devel = "glib2 glib2-devel glibc libgsf libxml2 libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgsf-1.14.41-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgsf-devel-1.14.41-5.el8.x86_64.rpm \
          "

SRC_URI[libgsf.sha256sum] = "b0575a4458e14a7f58cfebfed588bba01edeaa3f242a7d16f972cee10cd41064"
SRC_URI[libgsf-devel.sha256sum] = "5dee1b5f2aab6ed37851cd98594ff154f2392d89e4ef9572e22aae8aa0039606"
