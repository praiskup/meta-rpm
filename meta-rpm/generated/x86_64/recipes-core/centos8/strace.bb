SUMMARY = "generated recipe based on strace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_strace = "libc.so.6 libdw.so.1 librt.so.1"
RDEPENDS_strace = "bash elfutils-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/strace-4.24-9.el8.x86_64.rpm \
          "

SRC_URI[strace.sha256sum] = "ba1eb8eef630c25cfe76d60a602e20a5f5a283fa9c6469ee33438922232e24d6"
