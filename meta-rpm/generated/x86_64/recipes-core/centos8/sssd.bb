SUMMARY = "generated recipe based on sssd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "c-ares cyrus-sasl-lib dbus-libs ding-libs e2fsprogs glib-2.0 jansson keyutils krb5-libs libldb libnl libpcre libselinux libsemanage libtalloc libtdb libtevent libuuid nfs-utils openldap openssl p11-kit pam pkgconfig-native platform-python3 popt samba systemd-libs"
RPM_SONAME_PROV_libipa_hbac = "libipa_hbac.so.0"
RPM_SONAME_REQ_libipa_hbac = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_libipa_hbac = "glib2 glibc"
RPM_SONAME_PROV_libsss_autofs = "libsss_autofs.so"
RPM_SONAME_REQ_libsss_autofs = "libc.so.6"
RDEPENDS_libsss_autofs = "glibc"
RPM_SONAME_PROV_libsss_certmap = "libsss_certmap.so.0"
RPM_SONAME_REQ_libsss_certmap = "libc.so.6 libcrypto.so.1.1 libtalloc.so.2"
RDEPENDS_libsss_certmap = "glibc libtalloc openssl-libs"
RPM_SONAME_PROV_libsss_idmap = "libsss_idmap.so.0"
RPM_SONAME_REQ_libsss_idmap = "libc.so.6"
RDEPENDS_libsss_idmap = "glibc"
RPM_SONAME_PROV_libsss_nss_idmap = "libsss_nss_idmap.so.0"
RPM_SONAME_REQ_libsss_nss_idmap = "libc.so.6 libpthread.so.0"
RDEPENDS_libsss_nss_idmap = "glibc"
RPM_SONAME_REQ_libsss_nss_idmap-devel = "libsss_nss_idmap.so.0"
RPROVIDES_libsss_nss_idmap-devel = "libsss_nss_idmap-dev (= 2.2.3)"
RDEPENDS_libsss_nss_idmap-devel = "libsss_nss_idmap pkgconf-pkg-config"
RPM_SONAME_PROV_libsss_simpleifp = "libsss_simpleifp.so.0"
RPM_SONAME_REQ_libsss_simpleifp = "libc.so.6 libdbus-1.so.3 libdhash.so.1"
RDEPENDS_libsss_simpleifp = "dbus-libs glibc libdhash sssd-dbus"
RPM_SONAME_PROV_libsss_sudo = "libsss_sudo.so"
RPM_SONAME_REQ_libsss_sudo = "libc.so.6"
RDEPENDS_libsss_sudo = "glibc"
RPM_SONAME_REQ_python3-libipa_hbac = "libc.so.6 libdl.so.2 libglib-2.0.so.0 libipa_hbac.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_python3-libipa_hbac = "glib2 glibc libipa_hbac platform-python python3-libs"
RPM_SONAME_REQ_python3-libsss_nss_idmap = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libsss_nss_idmap.so.0 libutil.so.1"
RDEPENDS_python3-libsss_nss_idmap = "glibc libsss_nss_idmap platform-python python3-libs"
RPM_SONAME_REQ_python3-sss = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libldb.so.2 libm.so.6 libpcre.so.1 libpopt.so.0 libpthread.so.0 libpython3.6m.so.1.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0 libutil.so.1"
RDEPENDS_python3-sss = "dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pcre platform-python popt python3-libs sssd-common systemd-libs"
RPM_SONAME_REQ_python3-sss-murmur = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_python3-sss-murmur = "glibc platform-python python3-libs"
RDEPENDS_python3-sssdconfig = "platform-python"
RDEPENDS_sssd = "python3-sssdconfig sssd-ad sssd-common sssd-ipa sssd-krb5 sssd-ldap"
RPM_SONAME_PROV_sssd-ad = "libsss_ad.so"
RPM_SONAME_REQ_sssd-ad = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libndr-krb5pac.so.0 libndr-nbt.so.0 libndr-standard.so.0 libndr.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libsamba-util.so.0 libsasl2.so.3 libselinux.so.1 libsmbclient.so.0 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_krb5_common.so libsss_ldap_common.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-ad = "cyrus-sasl-lib dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsmbclient libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs pcre popt samba-client-libs sssd-common sssd-common-pac sssd-krb5-common systemd-libs"
RPM_SONAME_PROV_sssd-client = "libnss_sss.so.2"
RPM_SONAME_REQ_sssd-client = "libc.so.6 libcom_err.so.2 libk5crypto.so.3 libkrb5.so.3 libpam.so.0 libpthread.so.0 libsss_idmap.so.0 libsss_nss_idmap.so.0"
RDEPENDS_sssd-client = "bash chkconfig glibc krb5-libs libcom_err libsss_idmap libsss_nss_idmap pam"
RPM_SONAME_PROV_sssd-common = "libifp_iface.so libifp_iface_sync.so libsss_cert.so libsss_child.so libsss_crypt.so libsss_debug.so libsss_files.so libsss_iface.so libsss_iface_sync.so libsss_krb5_common.so libsss_ldap_common.so libsss_sbus.so libsss_sbus_sync.so libsss_semanage.so libsss_simple.so libsss_util.so"
RPM_SONAME_REQ_sssd-common = "libbasicobjects.so.0 libc.so.6 libcares.so.2 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libnl-3.so.200 libnl-route-3.so.200 libp11-kit.so.0 libpam.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsemanage.so.1 libssl.so.1.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_iface.so libsss_krb5_common.so libsss_sbus.so libsss_sbus_sync.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-common = "bash c-ares dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libnl3 libref_array libselinux libsemanage libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs p11-kit pam pcre popt shadow-utils sssd-client systemd systemd-libs"
RPM_SONAME_REQ_sssd-common-pac = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libldb.so.2 libndr-krb5pac.so.0 libndr-standard.so.0 libndr.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libsamba-util.so.0 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-common-pac = "dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libsss_idmap libtalloc libtdb libtevent openssl-libs pcre popt samba-client-libs sssd-common systemd-libs"
RPM_SONAME_REQ_sssd-dbus = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libifp_iface.so libini_config.so.5 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-dbus = "bash dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pcre popt sssd-common systemd systemd-libs"
RPM_SONAME_PROV_sssd-ipa = "libsss_ipa.so"
RPM_SONAME_REQ_sssd-ipa = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libipa_hbac.so.0 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libndr-krb5pac.so.0 libndr-nbt.so.0 libndr-standard.so.0 libndr.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libsamba-util.so.0 libselinux.so.1 libsemanage.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_krb5_common.so libsss_ldap_common.so libsss_semanage.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-ipa = "bash dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libipa_hbac libldb libref_array libselinux libsemanage libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs pcre popt samba-client-libs shadow-utils sssd-common sssd-common-pac sssd-krb5-common systemd-libs"
RPM_SONAME_PROV_sssd-kcm = "libsss_secrets.so"
RPM_SONAME_REQ_sssd-kcm = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libjansson.so.4 libk5crypto.so.3 libkrb5.so.3 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface.so libsss_sbus.so libsss_secrets.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0 libuuid.so.1"
RDEPENDS_sssd-kcm = "bash dbus-libs glib2 glibc jansson krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent libuuid openssl-libs pcre popt sssd-common systemd systemd-libs"
RPM_SONAME_PROV_sssd-krb5 = "libsss_krb5.so"
RPM_SONAME_REQ_sssd-krb5 = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_krb5_common.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-krb5 = "dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pcre popt sssd-common sssd-krb5-common systemd-libs"
RPM_SONAME_REQ_sssd-krb5-common = "libc.so.6 libcom_err.so.2 libdhash.so.1 libk5crypto.so.3 libkrb5.so.3 libpopt.so.0 libsss_debug.so libsystemd.so.0 libtalloc.so.2"
RDEPENDS_sssd-krb5-common = "bash cyrus-sasl-gssapi glibc krb5-libs libcom_err libdhash libtalloc popt shadow-utils sssd-common systemd-libs"
RPM_SONAME_PROV_sssd-ldap = "libsss_ldap.so"
RPM_SONAME_REQ_sssd-ldap = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_krb5_common.so libsss_ldap_common.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-ldap = "dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs pcre popt sssd-common sssd-krb5-common systemd-libs"
RPM_SONAME_REQ_sssd-libwbclient = "libc.so.6 libdl.so.2 libpthread.so.0 libsss_nss_idmap.so.0"
RDEPENDS_sssd-libwbclient = "bash glibc libsss_nss_idmap"
RPM_SONAME_REQ_sssd-nfs-idmap = "libc.so.6 libnfsidmap.so.1"
RDEPENDS_sssd-nfs-idmap = "glibc libnfsidmap"
RDEPENDS_sssd-polkit-rules = "polkit sssd-common"
RPM_SONAME_PROV_sssd-proxy = "libsss_proxy.so"
RPM_SONAME_REQ_sssd-proxy = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libldb.so.2 libpam.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-proxy = "bash dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pam pcre popt shadow-utils sssd-common systemd-libs"
RPM_SONAME_REQ_sssd-tools = "libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libifp_iface_sync.so libini_config.so.5 libldb.so.2 libpam.so.0 libpam_misc.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface_sync.so libsss_sbus_sync.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-tools = "bash dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pam pcre platform-python popt python3-sss python3-sssdconfig sssd-common systemd-libs"
RPM_SONAME_REQ_sssd-winbind-idmap = "libc.so.6 libpthread.so.0 libsss_idmap.so.0 libsss_nss_idmap.so.0 libtalloc.so.2"
RDEPENDS_sssd-winbind-idmap = "glibc libsss_idmap libsss_nss_idmap libtalloc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libipa_hbac-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsss_autofs-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsss_certmap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsss_idmap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsss_nss_idmap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsss_simpleifp-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsss_sudo-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libipa_hbac-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libsss_nss_idmap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-sss-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-sss-murmur-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-sssdconfig-2.2.3-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-ad-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-client-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-common-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-common-pac-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-dbus-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-ipa-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-kcm-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-krb5-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-krb5-common-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-ldap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-libwbclient-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-nfs-idmap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-polkit-rules-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-proxy-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-tools-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sssd-winbind-idmap-2.2.3-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsss_nss_idmap-devel-2.2.3-20.el8.x86_64.rpm \
          "

SRC_URI[libipa_hbac.sha256sum] = "e452beb13b0d3c5b37a82700a126f3dd90d477830231daf5219a46527efd3624"
SRC_URI[libsss_autofs.sha256sum] = "cee46be1f2ec11a6894611349245549295043b9f3d70877d3add582d14d2122d"
SRC_URI[libsss_certmap.sha256sum] = "3247f671620f6de008fb62e4640101dfc242dd1781a2fe7772ad31ab706d1519"
SRC_URI[libsss_idmap.sha256sum] = "2a919cb30b648015283c8065629e7b21a3aa893709db7a40c64175750b0065a9"
SRC_URI[libsss_nss_idmap.sha256sum] = "6e0e6e3a50ffae3680292781e608bba18dbb6bbf6899cd15dbc1a250a75093c9"
SRC_URI[libsss_nss_idmap-devel.sha256sum] = "0166bc613e17e540c5f2183a59b64ceac1e73cfe35f4b25f38f52848a5d24f58"
SRC_URI[libsss_simpleifp.sha256sum] = "a276a2991b19033c6d5cd8f2fd9831c654673bba19f3574b78a7069fe31df23d"
SRC_URI[libsss_sudo.sha256sum] = "c79bcc08adab3ebcb2d2a847b64326db6cd7210c9f1f0932888577302cccce2f"
SRC_URI[python3-libipa_hbac.sha256sum] = "d1eacdb721b8f672aba1789393f5053cdcbd79903ca13b95ad68cbc13405b925"
SRC_URI[python3-libsss_nss_idmap.sha256sum] = "69a336514ebae3329e523f10343f98b572ce8e17cc1e4f045f6e03efa4c5d2dd"
SRC_URI[python3-sss.sha256sum] = "0190dc4cef64ca65558adbad7982c8fe6d1cb9cedb2a40922523ac3255eb1e57"
SRC_URI[python3-sss-murmur.sha256sum] = "591a1ac009f6268e0a882d9c0975e685658d0d1d88e54d7363f8e80ef7534295"
SRC_URI[python3-sssdconfig.sha256sum] = "e26bbd19357780bfb4c70403c7bf722d24f16a5fe330dd074da86542c57e6200"
SRC_URI[sssd.sha256sum] = "9b548789aa7459b67850abb9fef5120a2a44813536b3debd70c07bf7f3d8b0b2"
SRC_URI[sssd-ad.sha256sum] = "be29dda590419467230c645cb9a6a4636f1bc4d5788a6b4abe324f5b7befcce0"
SRC_URI[sssd-client.sha256sum] = "6d775e169b8b45fcff410886b77915a6ebe9ef8fdd44914a2bc1c44f51f115c0"
SRC_URI[sssd-common.sha256sum] = "6d79f2cc74c7354f9eacbda97c3d4eecc9d96bc509dc0fb7d81474304eb9f295"
SRC_URI[sssd-common-pac.sha256sum] = "c2f51e12626321bea9bb2643b6492a7d3c3aeb943113561016264f6db9368ab5"
SRC_URI[sssd-dbus.sha256sum] = "c069be4caf99c3ec3048d2755e376fb96d5a6964be3c71d1159028ac9705ac9f"
SRC_URI[sssd-ipa.sha256sum] = "b0183446110c045449dce4ad379b5a628ce2179b5e312eec33eb8e0465b1b152"
SRC_URI[sssd-kcm.sha256sum] = "8ca8d4e2622b4183a3dcb1f535fa2edee94966d7cc81dd6846ac38ad56c21d33"
SRC_URI[sssd-krb5.sha256sum] = "deaf41ddac455aa7ec2060f36f9ffb62d01238aac4dc2ba5f460958defcd0666"
SRC_URI[sssd-krb5-common.sha256sum] = "cb31330513f024004b52aeaeff13bbd9e09e585795fb9567ab0cde64f5307f7f"
SRC_URI[sssd-ldap.sha256sum] = "8fc7f525292d298f40e8849c99de88ebebffc32c7575ee4c41a837421cbf87bf"
SRC_URI[sssd-libwbclient.sha256sum] = "884daac7b1d044645bad5942e7b20a08e3a7a1c19d4a2309b343b2342ff86c04"
SRC_URI[sssd-nfs-idmap.sha256sum] = "fa49e404c1cede58c19d63264f7141c6091bc4606be161b46a44e2816f67cd14"
SRC_URI[sssd-polkit-rules.sha256sum] = "e18c6cacc1498332f0f5a864a3a80c5953f63e88dd1343106007efe8b17d088e"
SRC_URI[sssd-proxy.sha256sum] = "2fc150a643f8099aff34623a040541701859c4cdcf1cce664b3a500471508474"
SRC_URI[sssd-tools.sha256sum] = "546fdf1c9105e8e67c87e2a26d4b34740198dc8cf50054cb8b0aeba5aad9885d"
SRC_URI[sssd-winbind-idmap.sha256sum] = "1fca4045c5a3be498218dd62f1cb96613e3f63252e445bd020fcdd308b6af2ca"
