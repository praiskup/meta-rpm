SUMMARY = "generated recipe based on patch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr libselinux pkgconfig-native"
RPM_SONAME_REQ_patch = "libattr.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_patch = "glibc libattr libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/patch-2.7.6-11.el8.x86_64.rpm \
          "

SRC_URI[patch.sha256sum] = "7471008600d502598a05f6c19c851cb769761367792fbe7cd595c5891ee0efb7"
