SUMMARY = "generated recipe based on crash-ptdump-command srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-ptdump-command = "libc.so.6"
RDEPENDS_crash-ptdump-command = "crash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/crash-ptdump-command-1.0.3-5.el8.x86_64.rpm \
          "

SRC_URI[crash-ptdump-command.sha256sum] = "87574e2d45d198928495883e455f150f2b31189f9259012634aebebb81258379"
