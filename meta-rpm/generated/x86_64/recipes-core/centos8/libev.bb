SUMMARY = "generated recipe based on libev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libev = "libev.so.4"
RPM_SONAME_REQ_libev = "libc.so.6 libm.so.6"
RDEPENDS_libev = "glibc"
RPM_SONAME_REQ_libev-devel = "libev.so.4"
RPROVIDES_libev-devel = "libev-dev (= 4.24)"
RDEPENDS_libev-devel = "libev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libev-4.24-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libev-devel-4.24-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libev-source-4.24-6.el8.noarch.rpm \
          "

SRC_URI[libev.sha256sum] = "83549217540abd259f74b84d9359ab200c2cbe6e9b2e25a73d7236cf441aed4c"
SRC_URI[libev-devel.sha256sum] = "58a0a94686f8f9207e948d5f7a52b816b3f1fe3c19cf8c96ab28d8b77ee419db"
SRC_URI[libev-source.sha256sum] = "c27cd56149df04c3af07a8f06a6a722986613bd7775f6ae6324de9ef70c63113"
