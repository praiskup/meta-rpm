SUMMARY = "generated recipe based on rasqal srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpcre mpfr pkgconfig-native raptor2"
RPM_SONAME_PROV_rasqal = "librasqal.so.3"
RPM_SONAME_REQ_rasqal = "libc.so.6 libm.so.6 libmpfr.so.4 libpcre.so.1 libraptor2.so.0 librasqal.so.3"
RDEPENDS_rasqal = "glibc mpfr pcre raptor2"
RPM_SONAME_REQ_rasqal-devel = "librasqal.so.3"
RPROVIDES_rasqal-devel = "rasqal-dev (= 0.9.33)"
RDEPENDS_rasqal-devel = "bash pkgconf-pkg-config raptor2-devel rasqal"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rasqal-0.9.33-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/rasqal-devel-0.9.33-6.el8.x86_64.rpm \
          "

SRC_URI[rasqal.sha256sum] = "459224eef8ea7a2f53482337391cedd685e1b8bc62afaf907223f236606600ac"
SRC_URI[rasqal-devel.sha256sum] = "606e22c3b7586b80d37069958d21fe7d0bdd6f4ba470b89fd1ec557d445d2bea"
