SUMMARY = "generated recipe based on lua-lunit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lua-lunit = "bash lua"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lua-lunit-0.5-13.el8.noarch.rpm \
          "

SRC_URI[lua-lunit.sha256sum] = "67ecaeb7467795f0e65736d6478ee2e0603ef61c092d1ef64d93ed61610ef7f4"
