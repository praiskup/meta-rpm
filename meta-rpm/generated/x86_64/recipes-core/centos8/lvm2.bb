SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper device-mapper-event dlm libaio libblkid libselinux libsepol pkgconfig-native readline sanlock systemd-libs"
RPM_SONAME_REQ_lvm2 = "libaio.so.1 libblkid.so.1 libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libm.so.6 libpthread.so.0 libreadline.so.7 libselinux.so.1 libsepol.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_lvm2 = "bash device-mapper-event-libs device-mapper-libs device-mapper-persistent-data glibc kmod libaio libblkid libselinux libsepol lvm2-libs readline systemd systemd-libs"
RDEPENDS_lvm2-dbusd = "bash dbus lvm2 platform-python python3-dbus python3-gobject-base python3-pyudev systemd"
RPM_SONAME_REQ_lvm2-devel = "libdevmapper-event-lvm2.so.2.03 liblvm2cmd.so.2.03"
RPROVIDES_lvm2-devel = "lvm2-dev (= 2.03.08)"
RDEPENDS_lvm2-devel = "device-mapper-devel device-mapper-event-devel lvm2 lvm2-libs pkgconf-pkg-config"
RPM_SONAME_PROV_lvm2-libs = "libdevmapper-event-lvm2.so.2.03 libdevmapper-event-lvm2mirror.so.2.03 libdevmapper-event-lvm2raid.so.2.03 libdevmapper-event-lvm2snapshot.so.2.03 libdevmapper-event-lvm2thin.so.2.03 libdevmapper-event-lvm2vdo.so.2.03 liblvm2cmd.so.2.03"
RPM_SONAME_REQ_lvm2-libs = "libaio.so.1 libblkid.so.1 libc.so.6 libdevmapper-event-lvm2.so.2.03 libdevmapper-event.so.1.02 libdevmapper.so.1.02 liblvm2cmd.so.2.03 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_lvm2-libs = "device-mapper-event device-mapper-event-libs device-mapper-libs glibc libaio libblkid libselinux libsepol systemd-libs"
RPM_SONAME_REQ_lvm2-lockd = "libblkid.so.1 libc.so.6 libdlm_lt.so.3 libdlmcontrol.so.3 libm.so.6 libpthread.so.0 libsanlock_client.so.1 libselinux.so.1 libsepol.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_lvm2-lockd = "bash dlm-lib glibc libblkid libselinux libsepol lvm2 sanlock-lib systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lvm2-2.03.08-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lvm2-dbusd-2.03.08-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lvm2-libs-2.03.08-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lvm2-lockd-2.03.08-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lvm2-devel-2.03.08-3.el8.x86_64.rpm \
          "

SRC_URI[lvm2.sha256sum] = "b47fa3e83bf544d54472fea0a82fc791ae76afc9a4df27d5e182c88e3cd82f6d"
SRC_URI[lvm2-dbusd.sha256sum] = "251d3a67117d570a1ff6b939dac0f70f6780bc4477a8d4b2921aff195663fbaf"
SRC_URI[lvm2-devel.sha256sum] = "bb11ac6d5a1c82ef80af4e753d61781489272f8099b6b49e886b7ea3729c4080"
SRC_URI[lvm2-libs.sha256sum] = "05c20ddee5e2b483ded396cbeb5f7ba689935e8467cfb2d200afb74f8f411887"
SRC_URI[lvm2-lockd.sha256sum] = "3dc0427abd19de4d4aba671f83cef5c781e5493180912bb1705d90e0e119e1e5"
