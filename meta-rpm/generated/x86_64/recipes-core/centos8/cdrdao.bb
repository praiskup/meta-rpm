SUMMARY = "generated recipe based on cdrdao srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lame libao libgcc libmad libvorbis pkgconfig-native"
RPM_SONAME_REQ_cdrdao = "libao.so.4 libc.so.6 libgcc_s.so.1 libm.so.6 libmad.so.0 libmp3lame.so.0 libpthread.so.0 libstdc++.so.6 libvorbisfile.so.3"
RDEPENDS_cdrdao = "glibc lame-libs libao libgcc libmad libstdc++ libvorbis"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cdrdao-1.2.3-32.el8.x86_64.rpm \
          "

SRC_URI[cdrdao.sha256sum] = "a3a60318864902a1dcc259976d7584f50786b1d260e615041010f44a64d7d111"
