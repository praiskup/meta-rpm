SUMMARY = "generated recipe based on sombok srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libthai pkgconfig-native"
RPM_SONAME_PROV_sombok = "libsombok.so.3"
RPM_SONAME_REQ_sombok = "libc.so.6 libthai.so.0"
RDEPENDS_sombok = "glibc libthai"
RPM_SONAME_REQ_sombok-devel = "libsombok.so.3"
RPROVIDES_sombok-devel = "sombok-dev (= 2.4.0)"
RDEPENDS_sombok-devel = "pkgconf-pkg-config sombok"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sombok-2.4.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sombok-devel-2.4.0-6.el8.x86_64.rpm \
          "

SRC_URI[sombok.sha256sum] = "b9c107964283d8366081efbd7cb76da976c508964777f62ee1b0b9943252da7e"
SRC_URI[sombok-devel.sha256sum] = "30d063388d660c02fc71116c580bb66c3a7e693579e7976dce1b033006f61d0c"
