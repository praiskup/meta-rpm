SUMMARY = "generated recipe based on gtk-vnc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo cyrus-sasl-lib gdk-pixbuf glib-2.0 gnutls gtk+3 libgcrypt libgpg-error libx11 pango pkgconfig-native zlib"
RPM_SONAME_PROV_gtk-vnc2 = "libgtk-vnc-2.0.so.0"
RPM_SONAME_REQ_gtk-vnc2 = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libgvnc-1.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsasl2.so.3 libz.so.1"
RDEPENDS_gtk-vnc2 = "atk cairo cairo-gobject cyrus-sasl-lib gdk-pixbuf2 glib2 glibc gnutls gtk3 gvnc libX11 libgcrypt libgpg-error pango zlib"
RPM_SONAME_PROV_gvnc = "libgvnc-1.0.so.0"
RPM_SONAME_REQ_gvnc = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgpg-error.so.0 libpthread.so.0 libsasl2.so.3 libz.so.1"
RDEPENDS_gvnc = "cyrus-sasl-lib gdk-pixbuf2 glib2 glibc gnutls libgcrypt libgpg-error zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtk-vnc2-0.9.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gvnc-0.9.0-2.el8.x86_64.rpm \
          "

SRC_URI[gtk-vnc2.sha256sum] = "2a0c1582438811bb22ccb5d48751aabdf9061e0921db821cb40ade58fce6eb0d"
SRC_URI[gvnc.sha256sum] = "e69244002484c0ed55a83fb0e0deb4535afdbeedf7f64c8aaf5da8420c935e7f"
