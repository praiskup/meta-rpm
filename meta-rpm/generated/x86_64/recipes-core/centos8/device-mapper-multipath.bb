SUMMARY = "generated recipe based on device-mapper-multipath srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs json-c libaio libgcc pkgconfig-native readline systemd-libs userspace-rcu"
RPM_SONAME_REQ_device-mapper-multipath = "libc.so.6 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libmpathcmd.so.0 libmpathpersist.so.0 libmultipath.so.0 libpthread.so.0 libreadline.so.7 libsystemd.so.0 libudev.so.1 liburcu.so.6"
RDEPENDS_device-mapper-multipath = "bash device-mapper device-mapper-libs device-mapper-multipath-libs glibc kpartx libgcc readline systemd systemd-libs userspace-rcu"
RPM_SONAME_REQ_device-mapper-multipath-devel = "libmpathcmd.so.0 libmpathpersist.so.0"
RPROVIDES_device-mapper-multipath-devel = "device-mapper-multipath-dev (= 0.8.3)"
RDEPENDS_device-mapper-multipath-devel = "device-mapper-multipath device-mapper-multipath-libs"
RPM_SONAME_PROV_device-mapper-multipath-libs = "libcheckcciss_tur.so libcheckdirectio.so libcheckemc_clariion.so libcheckhp_sw.so libcheckrdac.so libcheckreadsector0.so libchecktur.so libforeign-nvme.so libmpathcmd.so.0 libmpathpersist.so.0 libmultipath.so.0 libprioalua.so libprioana.so libprioconst.so libpriodatacore.so libprioemc.so libpriohds.so libpriohp_sw.so libprioiet.so libprioontap.so libpriopath_latency.so libpriorandom.so libpriordac.so libpriosysfs.so libprioweightedpath.so"
RPM_SONAME_REQ_device-mapper-multipath-libs = "libaio.so.1 libc.so.6 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libm.so.6 libmpathcmd.so.0 libmultipath.so.0 libpthread.so.0 libsystemd.so.0 libudev.so.1 liburcu.so.6"
RDEPENDS_device-mapper-multipath-libs = "device-mapper-libs glibc libaio libgcc systemd-libs userspace-rcu"
RPM_SONAME_REQ_kpartx = "libc.so.6 libdevmapper.so.1.02"
RDEPENDS_kpartx = "bash device-mapper-libs glibc"
RPM_SONAME_PROV_libdmmp = "libdmmp.so.0.2.0"
RPM_SONAME_REQ_libdmmp = "libc.so.6 libjson-c.so.4 libmpathcmd.so.0 libpthread.so.0"
RDEPENDS_libdmmp = "device-mapper-multipath device-mapper-multipath-libs glibc json-c"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-multipath-0.8.3-3.el8_2.3.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-multipath-libs-0.8.3-3.el8_2.3.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kpartx-0.8.3-3.el8_2.3.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libdmmp-0.8.3-3.el8_2.3.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/device-mapper-multipath-devel-0.8.3-3.el8_2.3.x86_64.rpm \
          "

SRC_URI[device-mapper-multipath.sha256sum] = "46593f4885b678f205abe51bb5e28624bc24d6ed02ee25083b8a8e1f7646987d"
SRC_URI[device-mapper-multipath-devel.sha256sum] = "cba78821183ba7236aaefcbfc4b9274af9383c61d43907ae9f8002653a874138"
SRC_URI[device-mapper-multipath-libs.sha256sum] = "29c9f400cf8fcf7f586d054f4f9bde13edd329ff3db1b39020d232f34c0f9d80"
SRC_URI[kpartx.sha256sum] = "2ba82eba94bb48cd07f1e0c892a35ffc8f1682a1339088104227566f770d15cd"
SRC_URI[libdmmp.sha256sum] = "9caccf5abfd3b33dd5c7fb5fa0d075265ae4eb01a3a0cfad39f51c0248e505ed"
