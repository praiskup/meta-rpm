SUMMARY = "generated recipe based on doxygen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native qt5-qtbase"
RPM_SONAME_REQ_doxygen = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_doxygen = "glibc libgcc libstdc++ perl-interpreter platform-python"
RPM_SONAME_REQ_doxygen-doxywizard = "libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_doxygen-doxywizard = "doxygen glibc libgcc libstdc++ qt5-qtbase qt5-qtbase-gui"
RDEPENDS_doxygen-latex = "doxygen texlive-appendix texlive-collection-latexrecommended texlive-epstopdf texlive-import texlive-multirow texlive-sectsty texlive-tabu texlive-tocloft texlive-xtab"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/doxygen-1.8.14-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/doxygen-doxywizard-1.8.14-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/doxygen-latex-1.8.14-12.el8.x86_64.rpm \
          "

SRC_URI[doxygen.sha256sum] = "47ba48a5c6a4c4532e21cc0213c95eeba31bcbe74030c8f1442dc2e7386c6219"
SRC_URI[doxygen-doxywizard.sha256sum] = "79c5b99054f13890f19df2f9cd4cc5a81a1e2f9eb675dbb2a03af2ea72c0ec5f"
SRC_URI[doxygen-latex.sha256sum] = "9a5723d2430ec7cd0bcb54290f03ed49e037909d5d874bb360b3c0660b43df52"
