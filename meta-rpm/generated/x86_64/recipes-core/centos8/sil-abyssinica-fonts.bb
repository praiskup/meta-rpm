SUMMARY = "generated recipe based on sil-abyssinica-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-abyssinica-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-abyssinica-fonts-1.200-13.el8.noarch.rpm \
          "

SRC_URI[sil-abyssinica-fonts.sha256sum] = "34a928cd01a998db63cbf930bea8c9f246848e479498866b7c026c3cc3c55778"
