SUMMARY = "generated recipe based on enchant2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "aspell glib-2.0 hunspell libgcc pkgconfig-native"
RPM_SONAME_PROV_enchant2 = "libenchant-2.so.2"
RPM_SONAME_REQ_enchant2 = "libaspell.so.15 libc.so.6 libenchant-2.so.2 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libhunspell-1.6.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_enchant2 = "aspell glib2 glibc hunspell libgcc libstdc++"
RPM_SONAME_REQ_enchant2-devel = "libenchant-2.so.2"
RPROVIDES_enchant2-devel = "enchant2-dev (= 2.2.3)"
RDEPENDS_enchant2-devel = "enchant2 glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/enchant2-2.2.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/enchant2-devel-2.2.3-2.el8.x86_64.rpm \
          "

SRC_URI[enchant2.sha256sum] = "554266e74c2939b484488c0757d7310cb6e74100fe80a12619563ce376873a42"
SRC_URI[enchant2-devel.sha256sum] = "6240a04fe67b36eeaf8126cb8f8974610fc1780f9be728aaa361de3226450b23"
