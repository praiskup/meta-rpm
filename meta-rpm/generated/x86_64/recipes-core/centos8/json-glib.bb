SUMMARY = "generated recipe based on json-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_json-glib = "libjson-glib-1.0.so.0"
RPM_SONAME_REQ_json-glib = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_json-glib = "glib2 glibc"
RPM_SONAME_REQ_json-glib-devel = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0"
RPROVIDES_json-glib-devel = "json-glib-dev (= 1.4.4)"
RDEPENDS_json-glib-devel = "glib2 glib2-devel glibc json-glib pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/json-glib-devel-1.4.4-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/json-glib-1.4.4-1.el8.x86_64.rpm \
          "

SRC_URI[json-glib.sha256sum] = "98a6386df94fc9595365c3ecbc630708420fa68d1774614a723dec4a55e84b9c"
SRC_URI[json-glib-devel.sha256sum] = "cbe44da159e6e3026d5997d6da6bbf96e912547b1f1d7a44d579166ff185c7e0"
