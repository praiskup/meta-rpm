SUMMARY = "generated recipe based on xorg-x11-drv-qxl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libxfont2 pkgconfig-native spice systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-qxl = "libc.so.6 libudev.so.1"
RDEPENDS_xorg-x11-drv-qxl = "glibc systemd-libs xorg-x11-server-Xorg"
RPM_SONAME_REQ_xorg-x11-server-Xspice = "libXfont2.so.2 libc.so.6 libpthread.so.0 libspice-server.so.1"
RDEPENDS_xorg-x11-server-Xspice = "glibc libXfont2 platform-python spice-server xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-qxl-0.1.5-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-server-Xspice-0.1.5-11.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-qxl.sha256sum] = "cb7c1d9cb679c3185aa39a9a75f0fbedffc773000cb4774184f2629737b14d95"
SRC_URI[xorg-x11-server-Xspice.sha256sum] = "a18ad303cd646f4ce1aac8085e04e037e4a779c84af31ed3c5ea2c4c639ace8b"
