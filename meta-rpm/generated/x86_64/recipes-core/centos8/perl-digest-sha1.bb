SUMMARY = "generated recipe based on perl-Digest-SHA1 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-SHA1 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-SHA1 = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Digest-SHA1-2.13-23.el8.x86_64.rpm \
          "

SRC_URI[perl-Digest-SHA1.sha256sum] = "9daf203a877f29e5aaf8f9fe8b302577cdf89a2e36bcdc9d85258e81dccade97"
