SUMMARY = "generated recipe based on pinentry srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo fontconfig freetype gcr gdk-pixbuf glib-2.0 gtk+3 gtk2 libassuan libgpg-error libsecret ncurses p11-kit pango pkgconfig-native"
RPM_SONAME_REQ_pinentry = "libassuan.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libncursesw.so.6 libsecret-1.so.0 libtinfo.so.6"
RDEPENDS_pinentry = "bash chkconfig glib2 glibc info libassuan libgpg-error libsecret ncurses-libs"
RPM_SONAME_REQ_pinentry-emacs = "libassuan.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libsecret-1.so.0"
RDEPENDS_pinentry-emacs = "glib2 glibc libassuan libgpg-error libsecret pinentry"
RPM_SONAME_REQ_pinentry-gnome3 = "libassuan.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libncursesw.so.6 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libsecret-1.so.0 libtinfo.so.6"
RDEPENDS_pinentry-gnome3 = "atk cairo cairo-gobject gcr gdk-pixbuf2 glib2 glibc gtk3 libassuan libgpg-error libsecret ncurses-libs p11-kit pango pinentry"
RPM_SONAME_REQ_pinentry-gtk = "libassuan.so.0 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-x11-2.0.so.0 libncursesw.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libsecret-1.so.0 libtinfo.so.6"
RDEPENDS_pinentry-gtk = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libassuan libgpg-error libsecret ncurses-libs pango pinentry"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pinentry-1.1.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pinentry-emacs-1.1.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pinentry-gnome3-1.1.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pinentry-gtk-1.1.0-2.el8.x86_64.rpm \
          "

SRC_URI[pinentry.sha256sum] = "9a6e8072669988348eb5bc011ea2173a5d5fb2b2247f5889bd610d20f1bf8d29"
SRC_URI[pinentry-emacs.sha256sum] = "02f28fa537cbccc4150bf8266378030cad95c860539203d894bae23b4156de3b"
SRC_URI[pinentry-gnome3.sha256sum] = "2c3c6dce88fb97646404b5c593bcd12132612e03306a8903c4db9520fcfaf7be"
SRC_URI[pinentry-gtk.sha256sum] = "58b9f61ebf94706f43766a9a2a70af5ca61af27125394a6b4d79216c6ce7c6d8"
