SUMMARY = "generated recipe based on pcp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi-libs cyrus-sasl-lib device-mapper-libs libgcc libglvnd libpfm libuv libvarlink ncurses nspr nss openssl perl pkgconfig-native platform-python3 qt5-qtbase qt5-qtsvg rdma-core readline rpm systemd-libs xz"
RPM_SONAME_REQ_pcp = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpcp.so.3 libpcp_gui.so.2 libpcp_mmv.so.1 libpcp_pmda.so.3 libpcp_trace.so.2 libpcp_web.so.1 libpthread.so.0 libreadline.so.7 librt.so.1 libssl.so.1.1 libuv.so.1"
RDEPENDS_pcp = "bash findutils gawk glibc grep hostname libuv openssl-libs pcp-libs pcp-selinux readline sed which xz"
RPM_SONAME_REQ_pcp-devel = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RPROVIDES_pcp-devel = "pcp-dev (= 5.0.2)"
RDEPENDS_pcp-devel = "bash glibc pcp pcp-libs pcp-libs-devel perl-PCP-PMDA perl-interpreter perl-libs"
RDEPENDS_pcp-export-pcp2elasticsearch = "pcp pcp-libs python3-pcp python3-requests"
RDEPENDS_pcp-export-pcp2graphite = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2influxdb = "pcp pcp-libs python3-pcp python3-requests"
RDEPENDS_pcp-export-pcp2json = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2spark = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2xml = "pcp pcp-libs python3-pcp"
RDEPENDS_pcp-export-pcp2zabbix = "pcp pcp-libs python3-pcp"
RPM_SONAME_REQ_pcp-export-zabbix-agent = "libc.so.6 libdl.so.2 libpcp.so.3"
RDEPENDS_pcp-export-zabbix-agent = "glibc pcp-libs"
RPM_SONAME_REQ_pcp-gui = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Svg.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpcp.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_pcp-gui = "bash glibc liberation-sans-fonts libgcc libglvnd-glx libstdc++ pcp pcp-libs qt5-qtbase qt5-qtbase-gui qt5-qtsvg"
RPM_SONAME_REQ_pcp-import-collectl2pcp = "libc.so.6 libpcp.so.3 libpcp_import.so.1"
RDEPENDS_pcp-import-collectl2pcp = "glibc pcp-libs"
RDEPENDS_pcp-import-ganglia2pcp = "pcp-libs perl-PCP-LogImport perl-TimeDate perl-interpreter perl-libs rrdtool-perl"
RDEPENDS_pcp-import-iostat2pcp = "pcp-libs perl-PCP-LogImport perl-TimeDate perl-interpreter perl-libs"
RDEPENDS_pcp-import-mrtg2pcp = "pcp-libs perl-PCP-LogImport perl-interpreter perl-libs"
RDEPENDS_pcp-import-sar2pcp = "pcp-libs perl-PCP-LogImport perl-TimeDate perl-XML-TokeParser perl-interpreter perl-libs"
RPM_SONAME_PROV_pcp-libs = "libpcp.so.3 libpcp_gui.so.2 libpcp_import.so.1 libpcp_mmv.so.1 libpcp_pmda.so.3 libpcp_trace.so.2 libpcp_web.so.1"
RPM_SONAME_REQ_pcp-libs = "ld-linux-x86-64.so.2 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcrypto.so.1.1 libdl.so.2 liblzma.so.5 libm.so.6 libnspr4.so libnss3.so libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0 librt.so.1 libsasl2.so.3 libssl.so.1.1 libssl3.so libuv.so.1"
RDEPENDS_pcp-libs = "avahi-libs cyrus-sasl-lib glibc libuv nspr nss openssl-libs pcp-conf xz-libs"
RPM_SONAME_REQ_pcp-libs-devel = "libpcp.so.3 libpcp_gui.so.2 libpcp_import.so.1 libpcp_mmv.so.1 libpcp_pmda.so.3 libpcp_trace.so.2 libpcp_web.so.1"
RPROVIDES_pcp-libs-devel = "pcp-libs-dev (= 5.0.2)"
RDEPENDS_pcp-libs-devel = "pcp pcp-libs pkgconf-pkg-config"
RPM_SONAME_REQ_pcp-manager = "libc.so.6 libgcc_s.so.1 libm.so.6 libpcp.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_pcp-manager = "bash glibc libgcc libstdc++ pcp pcp-libs"
RDEPENDS_pcp-pmda-activemq = "bash perl-Digest-MD5 perl-JSON perl-PCP-PMDA perl-Time-HiRes perl-libs perl-libwww-perl"
RPM_SONAME_REQ_pcp-pmda-apache = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libpcp_web.so.1 libpthread.so.0 librt.so.1 libuv.so.1"
RDEPENDS_pcp-pmda-apache = "bash glibc libuv pcp-libs"
RPM_SONAME_REQ_pcp-pmda-bash = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-bash = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-bcc = "bash pcp python3-bcc python3-pcp"
RDEPENDS_pcp-pmda-bind2 = "bash perl-File-Slurp perl-PCP-PMDA perl-XML-LibXML perl-libwww-perl"
RDEPENDS_pcp-pmda-bonding = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-bpftrace = "bash bpftrace platform-python python3-pcp python36"
RPM_SONAME_REQ_pcp-pmda-cifs = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-cifs = "bash glibc pcp-libs"
RPM_SONAME_REQ_pcp-pmda-cisco = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0"
RDEPENDS_pcp-pmda-cisco = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-dbping = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-dm = "libc.so.6 libdevmapper.so.1.02 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-dm = "bash device-mapper-libs glibc pcp-libs"
RPM_SONAME_REQ_pcp-pmda-docker = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libpcp_web.so.1 libpthread.so.0 librt.so.1 libssl.so.1.1 libuv.so.1"
RDEPENDS_pcp-pmda-docker = "bash glibc libuv openssl-libs pcp pcp-libs"
RDEPENDS_pcp-pmda-ds389 = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-ds389log = "bash perl-Date-Manip perl-PCP-PMDA"
RDEPENDS_pcp-pmda-elasticsearch = "bash pcp python3-pcp"
RPM_SONAME_REQ_pcp-pmda-gfs2 = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-gfs2 = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-gluster = "bash pcp python3-pcp"
RDEPENDS_pcp-pmda-gpfs = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-gpsd = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-haproxy = "bash pcp python3-pcp"
RPM_SONAME_REQ_pcp-pmda-infiniband = "libc.so.6 libibmad.so.5 libibumad.so.3 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-infiniband = "bash glibc infiniband-diags libibumad pcp pcp-libs"
RDEPENDS_pcp-pmda-json = "bash pcp python3-jsonpointer python3-pcp python3-six"
RDEPENDS_pcp-pmda-libvirt = "bash pcp python3-libvirt python3-lxml python3-pcp"
RDEPENDS_pcp-pmda-lio = "bash pcp python3-pcp python3-rtslib"
RDEPENDS_pcp-pmda-lmsensors = "bash pcp pcp-libs python3-pcp"
RPM_SONAME_REQ_pcp-pmda-logger = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-logger = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-lustre = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-lustrecomm = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-lustrecomm = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-mailq = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-mailq = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-memcache = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-mic = "bash pcp python3-pcp"
RPM_SONAME_REQ_pcp-pmda-mounts = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-mounts = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-mssql = "bash pcp pcp-libs python3-pcp"
RDEPENDS_pcp-pmda-mysql = "bash perl-DBI perl-PCP-PMDA"
RDEPENDS_pcp-pmda-named = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-netcheck = "bash pcp pcp-libs python3-pcp"
RDEPENDS_pcp-pmda-netfilter = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-news = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-nfsclient = "bash pcp python3-pcp"
RDEPENDS_pcp-pmda-nginx = "bash perl-PCP-PMDA perl-libwww-perl"
RPM_SONAME_REQ_pcp-pmda-nvidia-gpu = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-nvidia-gpu = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-openmetrics = "bash pcp pcp-libs python3-pcp python3-requests"
RDEPENDS_pcp-pmda-oracle = "bash perl-DBI perl-PCP-PMDA perl-interpreter perl-libs"
RDEPENDS_pcp-pmda-pdns = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-perfevent = "libc.so.6 libm.so.6 libpcp.so.3 libpcp_pmda.so.3 libpfm.so.4 libpthread.so.0"
RDEPENDS_pcp-pmda-perfevent = "bash glibc libpfm pcp pcp-libs perl-interpreter perl-libs"
RPM_SONAME_REQ_pcp-pmda-podman = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libvarlink.so.0"
RDEPENDS_pcp-pmda-podman = "bash glibc libvarlink pcp pcp-libs"
RDEPENDS_pcp-pmda-postfix = "bash perl-PCP-PMDA perl-Time-HiRes postfix-perl-scripts"
RDEPENDS_pcp-pmda-postgresql = "bash pcp python3-pcp python3-psycopg2"
RDEPENDS_pcp-pmda-redis = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-roomtemp = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-roomtemp = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-rpm = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0 librpm.so.8"
RDEPENDS_pcp-pmda-rpm = "bash glibc pcp pcp-libs rpm-libs"
RDEPENDS_pcp-pmda-rsyslog = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-samba = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-sendmail = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-sendmail = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-shping = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0"
RDEPENDS_pcp-pmda-shping = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-slurm = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-smart = "libc.so.6 libdl.so.2 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-smart = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-snmp = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-summary = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3"
RDEPENDS_pcp-pmda-summary = "bash glibc pcp pcp-libs"
RPM_SONAME_REQ_pcp-pmda-systemd = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libsystemd.so.0"
RDEPENDS_pcp-pmda-systemd = "bash glibc pcp-libs systemd-libs"
RPM_SONAME_REQ_pcp-pmda-trace = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpcp_trace.so.2"
RDEPENDS_pcp-pmda-trace = "bash glibc pcp-libs"
RDEPENDS_pcp-pmda-unbound = "bash pcp python3-pcp"
RDEPENDS_pcp-pmda-vmware = "bash perl-PCP-PMDA"
RPM_SONAME_REQ_pcp-pmda-weblog = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0"
RDEPENDS_pcp-pmda-weblog = "bash glibc pcp pcp-libs"
RDEPENDS_pcp-pmda-zimbra = "bash perl-PCP-PMDA"
RDEPENDS_pcp-pmda-zswap = "bash pcp python3-pcp"
RDEPENDS_pcp-selinux = "bash policycoreutils selinux-policy-targeted"
RPM_SONAME_REQ_pcp-system-tools = "libc.so.6 libm.so.6 libncurses.so.6 libpcp.so.3 libpcp_gui.so.2 libtinfo.so.6"
RDEPENDS_pcp-system-tools = "bash glibc ncurses-libs pcp pcp-libs python3-pcp"
RPM_SONAME_REQ_pcp-testsuite = "libQt5Core.so.5 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpcp.so.3 libpcp_pmda.so.3 libpthread.so.0 libstdc++.so.6"
RDEPENDS_pcp-testsuite = "bash bc bzip2 gcc glibc gzip libgcc libstdc++ pcp pcp-devel pcp-gui pcp-libs pcp-libs-devel pcp-pmda-activemq pcp-pmda-apache pcp-pmda-bash pcp-pmda-bcc pcp-pmda-bind2 pcp-pmda-bonding pcp-pmda-bpftrace pcp-pmda-cisco pcp-pmda-dbping pcp-pmda-dm pcp-pmda-docker pcp-pmda-ds389 pcp-pmda-ds389log pcp-pmda-elasticsearch pcp-pmda-gfs2 pcp-pmda-gluster pcp-pmda-gpfs pcp-pmda-gpsd pcp-pmda-haproxy pcp-pmda-json pcp-pmda-libvirt pcp-pmda-lio pcp-pmda-lmsensors pcp-pmda-logger pcp-pmda-lustre pcp-pmda-lustrecomm pcp-pmda-mailq pcp-pmda-memcache pcp-pmda-mic pcp-pmda-mounts pcp-pmda-mssql pcp-pmda-mysql pcp-pmda-named pcp-pmda-netcheck pcp-pmda-netfilter pcp-pmda-news pcp-pmda-nfsclient pcp-pmda-nginx pcp-pmda-nvidia-gpu pcp-pmda-openmetrics pcp-pmda-oracle pcp-pmda-pdns pcp-pmda-podman pcp-pmda-postfix pcp-pmda-postgresql pcp-pmda-roomtemp pcp-pmda-rpm pcp-pmda-samba pcp-pmda-sendmail pcp-pmda-shping pcp-pmda-slurm pcp-pmda-smart pcp-pmda-snmp pcp-pmda-summary pcp-pmda-trace pcp-pmda-unbound pcp-pmda-vmware pcp-pmda-weblog pcp-pmda-zimbra pcp-pmda-zswap pcp-system-tools perl-Getopt-Long perl-IO perl-PCP-LogImport perl-TimeDate perl-interpreter perl-libnet perl-libs platform-python qt5-qtbase redhat-rpm-config"
RDEPENDS_pcp-zeroconf = "bash pcp pcp-doc pcp-pmda-dm pcp-pmda-nfsclient pcp-system-tools"
RPM_SONAME_REQ_perl-PCP-LogImport = "libc.so.6 libpcp.so.3 libpcp_import.so.1 libperl.so.5.26"
RDEPENDS_perl-PCP-LogImport = "glibc pcp-libs perl-Exporter perl-interpreter perl-libs"
RDEPENDS_perl-PCP-LogSummary = "pcp-libs perl-Exporter perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-PCP-MMV = "libc.so.6 libpcp.so.3 libpcp_mmv.so.1 libperl.so.5.26"
RDEPENDS_perl-PCP-MMV = "glibc pcp-libs perl-Exporter perl-Time-HiRes perl-interpreter perl-libs"
RPM_SONAME_REQ_perl-PCP-PMDA = "libc.so.6 libpcp.so.3 libpcp_pmda.so.3 libperl.so.5.26"
RDEPENDS_perl-PCP-PMDA = "glibc pcp-libs perl-Exporter perl-interpreter perl-libs"
RPM_SONAME_REQ_python3-pcp = "libc.so.6 libpcp.so.3 libpcp_gui.so.2 libpcp_import.so.1 libpcp_mmv.so.1 libpcp_pmda.so.3 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pcp = "glibc pcp pcp-libs platform-python python3-libs python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-conf-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-devel-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-doc-5.0.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2elasticsearch-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2graphite-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2influxdb-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2json-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2spark-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2xml-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-pcp2zabbix-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-export-zabbix-agent-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-gui-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-import-collectl2pcp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-import-ganglia2pcp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-import-iostat2pcp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-import-mrtg2pcp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-import-sar2pcp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-libs-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-libs-devel-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-manager-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-activemq-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-apache-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-bash-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-bcc-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-bind2-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-bonding-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-bpftrace-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-cifs-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-cisco-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-dbping-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-dm-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-docker-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-ds389-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-ds389log-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-elasticsearch-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-gfs2-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-gluster-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-gpfs-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-gpsd-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-haproxy-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-infiniband-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-json-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-libvirt-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-lio-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-lmsensors-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-logger-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-lustre-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-lustrecomm-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-mailq-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-memcache-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-mic-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-mounts-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-mssql-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-mysql-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-named-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-netcheck-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-netfilter-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-news-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-nfsclient-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-nginx-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-nvidia-gpu-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-openmetrics-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-oracle-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-pdns-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-perfevent-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-podman-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-postfix-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-postgresql-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-redis-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-roomtemp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-rpm-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-rsyslog-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-samba-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-sendmail-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-shping-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-slurm-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-smart-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-snmp-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-summary-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-systemd-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-trace-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-unbound-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-vmware-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-weblog-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-zimbra-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-pmda-zswap-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-selinux-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-system-tools-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-testsuite-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcp-zeroconf-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-PCP-LogImport-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-PCP-LogSummary-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-PCP-MMV-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-PCP-PMDA-5.0.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pcp-5.0.2-5.el8.x86_64.rpm \
          "

SRC_URI[pcp.sha256sum] = "47957c68a6177eccecf2c1d073960cd24986dae664d351a49ab4ee08c898d8c0"
SRC_URI[pcp-conf.sha256sum] = "4e2cddf66cba480810e2a9ba0222d879ce22c3c8352e119f355c93d383f451b8"
SRC_URI[pcp-devel.sha256sum] = "b51c273baeeb6782b324bdd9515264f9e88afa865055ab5d8a53c79299bc3729"
SRC_URI[pcp-doc.sha256sum] = "d1f44417b64b8397b541623d4686824f52434cea11692782be93ae803ef05e2a"
SRC_URI[pcp-export-pcp2elasticsearch.sha256sum] = "76581f045814c0fa0a34ce0df5d1301758dc852608a3745db5a21149e7afc88f"
SRC_URI[pcp-export-pcp2graphite.sha256sum] = "ea3555acce12ac1e37dcf946ce0897c32d49cbb4cb88e589b2c289be9a7bf475"
SRC_URI[pcp-export-pcp2influxdb.sha256sum] = "a63b1095ff990cf87eb63ca3e20b2c9afa39c7d041e00e0e966185bf87769690"
SRC_URI[pcp-export-pcp2json.sha256sum] = "ebbf1e34314e278e1425f7f57f39d1ad54fe1f368f0f6842648ec2d83c7389a4"
SRC_URI[pcp-export-pcp2spark.sha256sum] = "374fb69a4018db38649b99b7b2c263fb71372614de72671e3617a8d697ab5575"
SRC_URI[pcp-export-pcp2xml.sha256sum] = "c074945c1c94149d19b736fcef51df68b2ff4866e6f9f7386503cf5628a23c01"
SRC_URI[pcp-export-pcp2zabbix.sha256sum] = "7ba59f8af863d66f3fd230fa100c5d7ea0dd9e52b9293139eabfb26a12e0f635"
SRC_URI[pcp-export-zabbix-agent.sha256sum] = "a56e5fb76b4834fd1ef68e34ac830dc4e4ca128e7f231936977f66e9510cd858"
SRC_URI[pcp-gui.sha256sum] = "798a393a319daec92191ce66bf82877c52398f54a53da0e49c805d1c1eddd943"
SRC_URI[pcp-import-collectl2pcp.sha256sum] = "2f50ad34e9e6922b341ba52e149fd0518431785191c0b2af484255ea482c5930"
SRC_URI[pcp-import-ganglia2pcp.sha256sum] = "dec449e8dfe2693712a85e6627fe7375394f9fe199ddd1db50a46a6126b8d8bf"
SRC_URI[pcp-import-iostat2pcp.sha256sum] = "b3a037289e30504b5d88591990feead9096816817a0ac271a515b022f876a9cc"
SRC_URI[pcp-import-mrtg2pcp.sha256sum] = "f8ea27962cea44dc881ba53194579b57009bb1914ff6ad75658e519b3149816e"
SRC_URI[pcp-import-sar2pcp.sha256sum] = "1ee691041451a1b26402c3a2810f80c87ae9ac8063bb29535b01ce83b0e2809e"
SRC_URI[pcp-libs.sha256sum] = "41ca33a5b4ae5fe67a96fc369e971c320e900d514b6d8940761e8ecc3cd6559b"
SRC_URI[pcp-libs-devel.sha256sum] = "eafdcbb4ada31038df156bd80e36a29e962329b85e8ad8c1d343f58487eb1dea"
SRC_URI[pcp-manager.sha256sum] = "b7953bb3d449da41a0a1ad3f60256f93a2b2f376972e99e4069d931ab4cb095a"
SRC_URI[pcp-pmda-activemq.sha256sum] = "2ef260e78f4539318081104faaef0dcaec4f8f42d59606a377f6fe535a03fb58"
SRC_URI[pcp-pmda-apache.sha256sum] = "e8bdee92e58a9fe24bf9cdc01a0101c06810b03625662d1964019b3a3ac48d64"
SRC_URI[pcp-pmda-bash.sha256sum] = "dedbee04dad69e87e930cf4c38759e086fa75b9958ce728081435971ec956a2d"
SRC_URI[pcp-pmda-bcc.sha256sum] = "30c9f6f53a5e77e6ebf37e2cd2ce32db0fe856b5c96649c3669a7b56e01395d7"
SRC_URI[pcp-pmda-bind2.sha256sum] = "5c598158a88899320c13bed362c01a05483e77c529128cbb5aab5e76b593a816"
SRC_URI[pcp-pmda-bonding.sha256sum] = "89257a1d41bfc2ac48b5976ab0cbd225f2fbb4ee1d53079175de9030237fdfa7"
SRC_URI[pcp-pmda-bpftrace.sha256sum] = "16f09390301506fb0259c1f06a4902c29270984e81bd243cdda93060ceccc6d7"
SRC_URI[pcp-pmda-cifs.sha256sum] = "fd6eb4f89ff9f380902ef0f2412b1000935a3d0aafea3693bae85a8b16e329aa"
SRC_URI[pcp-pmda-cisco.sha256sum] = "29449b9ea260d01194b0b407d09946afab9990989415e6a4e626cf12d9aa1534"
SRC_URI[pcp-pmda-dbping.sha256sum] = "1b196e9221913554c7637ef92db4e5840acca7256b215bd773a8032637a9a2b2"
SRC_URI[pcp-pmda-dm.sha256sum] = "c203af38aa1294f0df207274748dc536f10558db0eda4b82483ab6d43e59a41d"
SRC_URI[pcp-pmda-docker.sha256sum] = "176d05fd6dbf04818d18c67c50a8e00071412539c57e437e5247df511c15c358"
SRC_URI[pcp-pmda-ds389.sha256sum] = "8fae36508b8f2c757977861e27787b9a83aae0cfaa13c1eec861709bad1d8099"
SRC_URI[pcp-pmda-ds389log.sha256sum] = "bf5bec8f4d386320b6b37736b84290399d6b0db28fb60f0871844badaa3f15ed"
SRC_URI[pcp-pmda-elasticsearch.sha256sum] = "0d8a7eb1a6f436feba666ccafdb2e0b90eb8b13b1e032ebb8d3bb551c19eb790"
SRC_URI[pcp-pmda-gfs2.sha256sum] = "8014cc616553dcc1683b0915f1c2884e8c10a5b17218e12a81c80b6568bdeb69"
SRC_URI[pcp-pmda-gluster.sha256sum] = "58e9191563d98c1e3d4f39d3f6bb9c587b0ea2501e883cf333df59bd0c81500e"
SRC_URI[pcp-pmda-gpfs.sha256sum] = "62204fc7d1864df06980a44d0d08a203d2f8609f0c527fcbff3e2b18244c54f9"
SRC_URI[pcp-pmda-gpsd.sha256sum] = "f7136f4c51c72a6afab8983694e14c456c90cbe0d1d8615443bff1d3d02358ef"
SRC_URI[pcp-pmda-haproxy.sha256sum] = "67c83c1181c0fc6ba69e0d3e6d1e9d5913e102c98a7da77e429e1c2b725fafbf"
SRC_URI[pcp-pmda-infiniband.sha256sum] = "a0508958909d6701f29ba474f8f8c88caea42ff88a1a444148c65d7a324c3893"
SRC_URI[pcp-pmda-json.sha256sum] = "3d608bd61a4b090a5e47762ee146ad4e537c430d8e647a6c84933c5d280d7c88"
SRC_URI[pcp-pmda-libvirt.sha256sum] = "6a37cef1a6ab3baf15ab4c271e5e53caca9fe879bd9ecb8ec444f034984bb510"
SRC_URI[pcp-pmda-lio.sha256sum] = "193d95453d9e0fd34a1c7b823c833cf9dfb09847e1839df8b0c8dfb6bccb6d5a"
SRC_URI[pcp-pmda-lmsensors.sha256sum] = "d0778a2f0280334710dc876be60b7aa21fe46024b5afe45841580e7b1273dc5b"
SRC_URI[pcp-pmda-logger.sha256sum] = "b83801ae7aa7c36e2d1343663ab2e77389c7f1f0981e8093c89370f10467257a"
SRC_URI[pcp-pmda-lustre.sha256sum] = "36bc80fbb08f4b081639c33858b3019269010db9f5d46fe865f08f8b3c7df093"
SRC_URI[pcp-pmda-lustrecomm.sha256sum] = "4b8d7b57f40fe230ad15b72f4bdeb3c5eccd1d661f61f81eb6009a80296a8e78"
SRC_URI[pcp-pmda-mailq.sha256sum] = "c7a0b9de3d7692abbc3c1038be6be0949ed429c0e6e07fe3b11501705dd83183"
SRC_URI[pcp-pmda-memcache.sha256sum] = "dadb61a4e27c5145e85193f5a327e4b0bac380a1610d424a6bd64a8aa4f87733"
SRC_URI[pcp-pmda-mic.sha256sum] = "8c8ba486abc60f88f8e3c1fc6f4042f75b5e04db71fcab74b85948c3d1654825"
SRC_URI[pcp-pmda-mounts.sha256sum] = "8e7c36d9007f83903c11167ba8e6c114c0096c2c6bb74cdc12704487512cf440"
SRC_URI[pcp-pmda-mssql.sha256sum] = "c01a8229c5f6209ee275cd81688aa1d02346d5693add908e2ad33793bec655a4"
SRC_URI[pcp-pmda-mysql.sha256sum] = "9588f03f510bb59b5417df783f2bb62f9654acbcb377b46687f6363058183221"
SRC_URI[pcp-pmda-named.sha256sum] = "628fc99f868ca1abd448e410b381364e8591a375b02f1c39ddecf3c44b4b015a"
SRC_URI[pcp-pmda-netcheck.sha256sum] = "7c774ef2b9622df90b000e966ae36f6eb140ba31fd9dd4bfd49f4f22721b2b43"
SRC_URI[pcp-pmda-netfilter.sha256sum] = "efb8f36158d065c6f1b40111d7976d5e17ad65563c5fceb07ed1f67ddac22097"
SRC_URI[pcp-pmda-news.sha256sum] = "704fefda3f21d33ddca3d50c20c124ca693d3d921ff83d999284d148ac96c34a"
SRC_URI[pcp-pmda-nfsclient.sha256sum] = "3b76550d100617aed8cc7b7140985dae60559d032bbef3661df8fc9414e2abed"
SRC_URI[pcp-pmda-nginx.sha256sum] = "52a0732773401900ecb820fe0eeefcbfebd8a40add95092a0df542fd5e88c1e1"
SRC_URI[pcp-pmda-nvidia-gpu.sha256sum] = "e651e51040a189ab6ec7f53561214e55b8d8b575bc80735e6be82236dff9db49"
SRC_URI[pcp-pmda-openmetrics.sha256sum] = "3d764bae8f349625ec374abec80d145db3516f526cd2e5cc8086ac1f7dfa89ca"
SRC_URI[pcp-pmda-oracle.sha256sum] = "e8953f12f5c8f9b8fa494a7173b14b7c3c335ddb43194d9afdf60a6c1084c907"
SRC_URI[pcp-pmda-pdns.sha256sum] = "410ff2fa53ada08314402c7e2d0e96247a44786ef2c6cda74f1d7298eec933ce"
SRC_URI[pcp-pmda-perfevent.sha256sum] = "46e8a758580536c91b8ac0c9ecd69ba4a766c8b33864e108f8a203bc19a43ddc"
SRC_URI[pcp-pmda-podman.sha256sum] = "2ab42526f6afcd170935314bf2c3df43b40a8808a4cedf131ea6615c8cad3412"
SRC_URI[pcp-pmda-postfix.sha256sum] = "50be0574d27cb7870c98110cdbbf14003e2a7adab1be43ecdbfd83d3ad74c653"
SRC_URI[pcp-pmda-postgresql.sha256sum] = "8a921eae95019f88e327b39af86bca19f29e7f0c0ed215dd70100fc93088d684"
SRC_URI[pcp-pmda-redis.sha256sum] = "57388f3cf73f6932ea7384e1c36bb6f593f535a07598be8096e52f822e6f03ed"
SRC_URI[pcp-pmda-roomtemp.sha256sum] = "ecaf08ecc68906cb16fa3b9e82b04eb3b536f8fe7bf049cd2284e6e80d54cb23"
SRC_URI[pcp-pmda-rpm.sha256sum] = "d8712d7fe8889eb673bf76599c5e8cb1531d94625cc9d021ff16fee0669084d7"
SRC_URI[pcp-pmda-rsyslog.sha256sum] = "711a9956af4314258c77ecc2e4f7d124c59233506387cf697d6697e13e5a6744"
SRC_URI[pcp-pmda-samba.sha256sum] = "a597395f7c3865695cd1c3e100462d5ed3fd4b850b06eafe63889c0137de6e7c"
SRC_URI[pcp-pmda-sendmail.sha256sum] = "5dd48550f663cb8e831bae150617b1692308b36954c8d41eb63c9b4ab3bf2864"
SRC_URI[pcp-pmda-shping.sha256sum] = "370e6ea108cb0b1ca4132c0286c7bf73f7a9b76a59fee8f3ca025146edc05a84"
SRC_URI[pcp-pmda-slurm.sha256sum] = "01d46c294642a289c3abf988bf65481a82997ac386bd293261a02099c6787323"
SRC_URI[pcp-pmda-smart.sha256sum] = "5c4aea71709ba8dc849cc589ec9d4e1a4823cdc708e4d9107c9ac96439cbdf03"
SRC_URI[pcp-pmda-snmp.sha256sum] = "c91df545726715aaf4c2275b27aef1c34d967d35fe256308e9341757aa1f996b"
SRC_URI[pcp-pmda-summary.sha256sum] = "edc0f2cee0614873df77bb84ac485730ff3586f2c8dda29e4bb15380b98f04b0"
SRC_URI[pcp-pmda-systemd.sha256sum] = "f50874a59dc6e8c7ab125a95f0804067eef30d91bebd770740c603999832d147"
SRC_URI[pcp-pmda-trace.sha256sum] = "c928491abb8780a38d154a8faa4c39e28b5166ab8c5d40eebc6e97cdd9bca62c"
SRC_URI[pcp-pmda-unbound.sha256sum] = "f47f29c8666a92349b002afb9889686269036e9453b4daee540e34436bae98ef"
SRC_URI[pcp-pmda-vmware.sha256sum] = "e573765d3d94251a8d23bb138a92be277256dd77ecd98207fdf11bcd1583e03a"
SRC_URI[pcp-pmda-weblog.sha256sum] = "5b5af03f8ee8b6d273c6e63692868800a4d82a5c1306ee54a710e89153078149"
SRC_URI[pcp-pmda-zimbra.sha256sum] = "8e3131fc9714f98ccdd3237699bcc3c7d2986e07a53ff7bfa6e5d1ae41e8b0ab"
SRC_URI[pcp-pmda-zswap.sha256sum] = "4fe7abff178eb2df1cd2f97a8d6a48566d94a9f92c393a5b58505e8d0c8a8505"
SRC_URI[pcp-selinux.sha256sum] = "3a4860f2af77fb337592f5f023e5a4c2b1457991d64f220e67279044bc42efa7"
SRC_URI[pcp-system-tools.sha256sum] = "fb0f553f5a3a7d6a3f7d5981f862ae085e3ef8bb3cc0fcc942bac7b73f70ec91"
SRC_URI[pcp-testsuite.sha256sum] = "8f1696ceaecdef7b06dd2cc538abf29f3feaeede33437100ffd62a9de601b919"
SRC_URI[pcp-zeroconf.sha256sum] = "ab9781d4f8583c28b449d36fbc1af9ba1d91dedc8799c9cc5fce35a8547e4703"
SRC_URI[perl-PCP-LogImport.sha256sum] = "a82aeba3fa0872a38e08afe508935364300e59fbc13f9a0a9f5f223cca861ad8"
SRC_URI[perl-PCP-LogSummary.sha256sum] = "56773c10b4cad8b0474dd9eab6cada7d4627a53e242c8d467e9fdcf583150fa6"
SRC_URI[perl-PCP-MMV.sha256sum] = "cba3dd2f7494f49ae1c7ed65a7092a9fc9a66f37b9a97323d9c4937d8596ea3d"
SRC_URI[perl-PCP-PMDA.sha256sum] = "99967549d7b4814bde9dbfd9dc50d96c8f937aaebf7e05607d9fa5591a4643cd"
SRC_URI[python3-pcp.sha256sum] = "2412592e39de44ff5052b495179bdf849cf754c879932c73b548db627b029308"
