SUMMARY = "generated recipe based on perl-HTML-Parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-HTML-Parser = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-HTML-Parser = "glibc perl-Carp perl-Exporter perl-HTML-Tagset perl-HTTP-Message perl-IO perl-URI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-HTML-Parser-3.72-14.el8.x86_64.rpm \
          "

SRC_URI[perl-HTML-Parser.sha256sum] = "387d3a7786c64482266522d273dbea20078236a02f58d12e3a75c963b8764c5f"
