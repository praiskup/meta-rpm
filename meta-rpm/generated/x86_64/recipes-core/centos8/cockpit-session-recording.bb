SUMMARY = "generated recipe based on cockpit-session-recording srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_cockpit-session-recording = "cockpit-system tlog"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cockpit-session-recording-3-1.el8.noarch.rpm \
          "

SRC_URI[cockpit-session-recording.sha256sum] = "c66c000431392875c13149907af15e4e562e6f513f67c2e6e18b278820af7258"
