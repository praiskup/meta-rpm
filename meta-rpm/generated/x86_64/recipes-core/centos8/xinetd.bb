SUMMARY = "generated recipe based on xinetd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux libtirpc libxcrypt pkgconfig-native"
RPM_SONAME_REQ_xinetd = "libc.so.6 libcrypt.so.1 libm.so.6 libselinux.so.1 libtirpc.so.3"
RDEPENDS_xinetd = "bash filesystem glibc libselinux libtirpc libxcrypt setup systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xinetd-2.3.15-24.el8.x86_64.rpm \
          "

SRC_URI[xinetd.sha256sum] = "4c6c3ed7cef6c25b6f8f2b9742434ad3ae2ad32683f07f904523296f0913b3d8"
