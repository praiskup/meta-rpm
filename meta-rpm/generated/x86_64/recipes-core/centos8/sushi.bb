SUMMARY = "generated recipe based on sushi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo clutter clutter-gst3 clutter-gtk cogl evince freetype gdk-pixbuf gjs glib-2.0 gobject-introspection gstreamer1.0 gstreamer1.0-plugins-base gtk+3 gtksourceview3 harfbuzz json-glib libdrm libglvnd libmusicbrainz5 libsoup-2.4 libx11 libxcomposite libxdamage libxext libxfixes libxi libxkbcommon libxrandr mesa pango pkgconfig-native wayland webkit2gtk3"
RPM_SONAME_PROV_sushi = "libsushi-1.0.so"
RPM_SONAME_REQ_sushi = "libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libclutter-gst-3.0.so.0 libclutter-gtk-1.0.so.0 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libdrm.so.2 libevdocument3.so.4 libevview3.so.3 libfreetype.so.6 libgbm.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libgjs.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libharfbuzz.so.0 libjavascriptcoregtk-4.0.so.18 libjson-glib-1.0.so.0 libmusicbrainz5.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libsushi-1.0.so libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libwebkit2gtk-4.0.so.37 libxkbcommon.so.0"
RDEPENDS_sushi = "atk bash cairo cairo-gobject clutter clutter-gst3 clutter-gtk cogl evince-libs freetype gdk-pixbuf2 gjs glib2 glibc gobject-introspection gstreamer1 gstreamer1-plugins-base gtk3 gtksourceview3 harfbuzz json-glib libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libdrm libglvnd-egl libmusicbrainz5 libsoup libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon mesa-libgbm pango webkit2gtk3 webkit2gtk3-jsc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sushi-3.28.3-1.el8.x86_64.rpm \
          "

SRC_URI[sushi.sha256sum] = "96eab4712e228fa78816c759730780e1176e1c014600003f19e5447b8a362477"
