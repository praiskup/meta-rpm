SUMMARY = "generated recipe based on julietaula-montserrat-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_julietaula-montserrat-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/julietaula-montserrat-fonts-7.200-2.el8.noarch.rpm \
          "

SRC_URI[julietaula-montserrat-fonts.sha256sum] = "934737fa76a52df1a8733b33a57c82586af17bf1a66de5a42ed7fd7336ce1ffb"
