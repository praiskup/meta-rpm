SUMMARY = "generated recipe based on nss_wrapper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_nss_wrapper = "libnss_wrapper.so.0"
RPM_SONAME_REQ_nss_wrapper = "libc.so.6 libdl.so.2 libnss_wrapper.so.0 libpthread.so.0"
RDEPENDS_nss_wrapper = "cmake-filesystem glibc perl-Getopt-Long perl-PathTools perl-interpreter perl-libs pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nss_wrapper-1.1.5-3.el8.x86_64.rpm \
          "

SRC_URI[nss_wrapper.sha256sum] = "dd399db11bc74d92c0b764e6b2d7f43dcb9a1d8606e9bd018e1fedcce3dccd5b"
