SUMMARY = "generated recipe based on perl-Getopt-Long srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Getopt-Long = "perl-Exporter perl-Pod-Usage perl-Text-ParseWords perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Getopt-Long-2.50-4.el8.noarch.rpm \
          "

SRC_URI[perl-Getopt-Long.sha256sum] = "da4c6daa0d5406bc967cc89b02a69689491f42c543aceea1a31136f0f1a8d991"
