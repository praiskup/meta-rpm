SUMMARY = "generated recipe based on postgresql-jdbc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_postgresql-jdbc = "java-1.8.0-openjdk-headless javapackages-filesystem ongres-scram-client"
RDEPENDS_postgresql-jdbc-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postgresql-jdbc-42.2.3-3.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postgresql-jdbc-javadoc-42.2.3-3.el8_2.noarch.rpm \
          "

SRC_URI[postgresql-jdbc.sha256sum] = "1e45bc89c61a915c6e051414749367994f486d2fcdd2ba55a2400e4b8c471d49"
SRC_URI[postgresql-jdbc-javadoc.sha256sum] = "6aa9dd2d590bcfd02c3d2640e9e3a638386609174fd5ee370f77abe28e307b9c"
