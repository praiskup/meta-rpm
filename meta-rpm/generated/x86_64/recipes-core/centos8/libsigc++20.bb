SUMMARY = "generated recipe based on libsigc++20 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libsigc++20 = "libsigc-2.0.so.0"
RPM_SONAME_REQ_libsigc++20 = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libsigc++20 = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libsigc++20-devel = "libsigc-2.0.so.0"
RPROVIDES_libsigc++20-devel = "libsigc++20-dev (= 2.10.0)"
RDEPENDS_libsigc++20-devel = "libsigc++20 pkgconf-pkg-config"
RDEPENDS_libsigc++20-doc = "libsigc++20"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libsigc++20-2.10.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsigc++20-devel-2.10.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsigc++20-doc-2.10.0-5.el8.noarch.rpm \
          "

SRC_URI[libsigc++20.sha256sum] = "f46eda7a8ff87aa160ee2774ec820b6c7a8d83a3e5c4b88750246a4e1c843495"
SRC_URI[libsigc++20-devel.sha256sum] = "94c4fdd003196477b332f167934e220f9e38da3974cf2d56f1ae0ab1313df142"
SRC_URI[libsigc++20-doc.sha256sum] = "b6e7e85a879df543624300a0874cd357ba3bd69a2fb32ce7f5f9252987272b16"
