SUMMARY = "generated recipe based on emacs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl alsa-lib atk cairo dbus-libs fontconfig freetype gdk-pixbuf giflib glib-2.0 gnutls gpm gtk+3 libgcc libice libjpeg-turbo liblockfile libotf libpng librsvg libselinux libsm libsoup-2.4 libx11 libxcb libxcomposite libxext libxfixes libxft libxinerama libxml2 libxmu libxpm libxrandr libxrender libxt m17n-lib ncurses pango pkgconfig-native tiff webkit2gtk3 xaw3d zlib"
RPM_SONAME_REQ_emacs = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXcomposite.so.1 libXext.so.6 libXfixes.so.3 libXft.so.2 libXinerama.so.1 libXpm.so.4 libXrandr.so.2 libXrender.so.1 libacl.so.1 libanl.so.1 libasound.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdbus-1.so.3 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgif.so.7 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 libjpeg.so.62 libm.so.6 libm17n-core.so.0 libm17n-flt.so.0 libotf.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librsvg-2.so.2 librt.so.1 libselinux.so.1 libsoup-2.4.so.1 libtiff.so.5 libtinfo.so.6 libwebkit2gtk-4.0.so.37 libxcb.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_emacs = "alsa-lib atk bash cairo cairo-gobject chkconfig dbus-libs dejavu-sans-mono-fonts desktop-file-utils emacs-common fontconfig freetype gdk-pixbuf2 giflib glib2 glibc gnutls gtk3 libICE libSM libX11 libX11-xcb libXcomposite libXext libXfixes libXft libXinerama libXpm libXrandr libXrender libacl libgcc libjpeg-turbo libotf libpng librsvg2 libselinux libsoup libtiff libxcb libxml2 m17n-lib ncurses-libs pango webkit2gtk3 webkit2gtk3-jsc zlib"
RPM_SONAME_REQ_emacs-common = "libc.so.6 liblockfile.so.1"
RDEPENDS_emacs-common = "bash chkconfig emacs-filesystem glibc info liblockfile pkgconf-pkg-config"
RPM_SONAME_REQ_emacs-lucid = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXaw3d.so.8 libXext.so.6 libXfixes.so.3 libXft.so.2 libXinerama.so.1 libXmu.so.6 libXpm.so.4 libXrandr.so.2 libXrender.so.1 libXt.so.6 libacl.so.1 libanl.so.1 libasound.so.2 libc.so.6 libcairo.so.2 libdbus-1.so.3 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgif.so.7 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libjpeg.so.62 libm.so.6 libm17n-core.so.0 libm17n-flt.so.0 libotf.so.0 libpng16.so.16 libpthread.so.0 librsvg-2.so.2 librt.so.1 libselinux.so.1 libtiff.so.5 libtinfo.so.6 libxcb.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_emacs-lucid = "Xaw3d alsa-lib bash cairo chkconfig dbus-libs emacs-common fontconfig freetype gdk-pixbuf2 giflib glib2 glibc gnutls libICE libSM libX11 libX11-xcb libXext libXfixes libXft libXinerama libXmu libXpm libXrandr libXrender libXt libacl libgcc libjpeg-turbo libotf libpng librsvg2 libselinux libtiff libxcb libxml2 m17n-lib ncurses-libs zlib"
RPM_SONAME_REQ_emacs-nox = "libacl.so.1 libanl.so.1 libasound.so.2 libc.so.6 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libgpm.so.2 libjpeg.so.62 libm.so.6 libpthread.so.0 librt.so.1 libselinux.so.1 libtinfo.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_emacs-nox = "alsa-lib bash chkconfig dbus-libs emacs-common glibc gnutls gpm-libs libacl libgcc libjpeg-turbo libselinux libxml2 ncurses-libs zlib"
RDEPENDS_emacs-terminal = "bash emacs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/emacs-26.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/emacs-common-26.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/emacs-lucid-26.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/emacs-nox-26.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/emacs-terminal-26.1-5.el8.noarch.rpm \
          "

SRC_URI[emacs.sha256sum] = "877615ee389cf056377cb283c066070cf7ccfa2cee39424d61ff2acc99b7837f"
SRC_URI[emacs-common.sha256sum] = "774247f69b550b928a9da6dc4f4146e60e96e0cd9a08fc169e9221256e5cd318"
SRC_URI[emacs-lucid.sha256sum] = "97563257e02df5b70ed7147324d21b7095a3e8c051dff4303f6db19a6f66d0b8"
SRC_URI[emacs-nox.sha256sum] = "29a6c2d47d7cdaf19ce82e81e46ea373436495b131c216993a9f0f8247f0e694"
SRC_URI[emacs-terminal.sha256sum] = "421800bd81c51ee26da6574db1d5eb5d5949dc82809b35fd3a4f11ed8db58ff9"
