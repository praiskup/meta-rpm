SUMMARY = "generated recipe based on libaec srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libaec = "libaec.so.0 libsz.so.2"
RPM_SONAME_REQ_libaec = "libaec.so.0 libc.so.6"
RDEPENDS_libaec = "glibc"
RPM_SONAME_REQ_libaec-devel = "libaec.so.0 libsz.so.2"
RPROVIDES_libaec-devel = "libaec-dev (= 1.0.2)"
RDEPENDS_libaec-devel = "libaec"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libaec-1.0.2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libaec-devel-1.0.2-3.el8.x86_64.rpm \
          "

SRC_URI[libaec.sha256sum] = "525ada93b9e04d18316aa29c7793d1cf2f4a8c1ecca662c6e9d56258785c1c94"
SRC_URI[libaec-devel.sha256sum] = "ac91ad25ce9bfa017c38812c58c437c61ed6c0a662c0d72fe4693cb205efb5c8"
