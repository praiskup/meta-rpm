SUMMARY = "generated recipe based on python-pexpect srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pexpect = "platform-python python3-ptyprocess"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pexpect-4.3.1-3.el8.noarch.rpm \
          "

SRC_URI[python3-pexpect.sha256sum] = "0bc637f0d028043e8388b083cdd8d0a0acea7fdbc79c0bc9401dfb02c20fb817"
