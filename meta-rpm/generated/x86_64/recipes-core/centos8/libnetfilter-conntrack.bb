SUMMARY = "generated recipe based on libnetfilter_conntrack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libmnl libnfnetlink pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_conntrack = "libnetfilter_conntrack.so.3"
RPM_SONAME_REQ_libnetfilter_conntrack = "libc.so.6 libmnl.so.0 libnfnetlink.so.0"
RDEPENDS_libnetfilter_conntrack = "glibc libmnl libnfnetlink"
RPM_SONAME_REQ_libnetfilter_conntrack-devel = "libnetfilter_conntrack.so.3"
RPROVIDES_libnetfilter_conntrack-devel = "libnetfilter_conntrack-dev (= 1.0.6)"
RDEPENDS_libnetfilter_conntrack-devel = "kernel-headers libnetfilter_conntrack libnfnetlink-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnetfilter_conntrack-1.0.6-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnetfilter_conntrack-devel-1.0.6-5.el8.x86_64.rpm \
          "

SRC_URI[libnetfilter_conntrack.sha256sum] = "224100af3ecfc80c416796ec02c7c4dd113a38d42349d763485f3b42f260493f"
SRC_URI[libnetfilter_conntrack-devel.sha256sum] = "66e0f45ee443b04757b28eafe4ff31746ac0a5e7e681db21a215690666c8b9cf"
