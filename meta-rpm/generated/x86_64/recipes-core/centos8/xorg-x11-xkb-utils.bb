SUMMARY = "generated recipe based on xorg-x11-xkb-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxkbfile pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_REQ_xorg-x11-xkb-utils = "libX11.so.6 libc.so.6 libxkbfile.so.1"
RDEPENDS_xorg-x11-xkb-utils = "glibc libX11 libxkbfile"
RPROVIDES_xorg-x11-xkb-utils-devel = "xorg-x11-xkb-utils-dev (= 7.7)"
RDEPENDS_xorg-x11-xkb-utils-devel = "libX11-devel libxkbfile-devel pkgconf-pkg-config xorg-x11-proto-devel xorg-x11-xkb-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-xkb-utils-7.7-27.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xorg-x11-xkb-utils-devel-7.7-27.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-xkb-utils.sha256sum] = "30d9ac66dc412fa7ca78f412aa30bf4cca17493739488bf001bc9e9f98a56984"
SRC_URI[xorg-x11-xkb-utils-devel.sha256sum] = "d4819fd8b0ba94c1c0679cc2f62116738bc21cbed97552c2f49b4612bc53f4b9"
