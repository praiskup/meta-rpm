SUMMARY = "generated recipe based on genwqe-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_REQ_genwqe-tools = "libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1 libz.so.1 libzADC.so.4"
RDEPENDS_genwqe-tools = "bash genwqe-zlib glibc zlib"
RPM_SONAME_REQ_genwqe-vpd = "libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_genwqe-vpd = "glibc"
RPM_SONAME_PROV_genwqe-zlib = "libzADC.so.4"
RPM_SONAME_REQ_genwqe-zlib = "libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_genwqe-zlib = "glibc"
RPM_SONAME_REQ_genwqe-zlib-devel = "libzADC.so.4"
RPROVIDES_genwqe-zlib-devel = "genwqe-zlib-dev (= 4.0.20)"
RDEPENDS_genwqe-zlib-devel = "genwqe-zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/genwqe-tools-4.0.20-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/genwqe-vpd-4.0.20-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/genwqe-zlib-4.0.20-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/genwqe-zlib-devel-4.0.20-5.el8.x86_64.rpm \
          "

SRC_URI[genwqe-tools.sha256sum] = "469bd7ee75bbadeef7344a1a6aed2d3e1d4f4eb08a58c5b7ee0ca6d22f15a691"
SRC_URI[genwqe-vpd.sha256sum] = "b2b367d5e2a9483f8cc6eb3234c7a8e5c28ee8d89442250ad22c837fab94f10c"
SRC_URI[genwqe-zlib.sha256sum] = "d92fe3ae295f20620c5335c6b95d52e83cfdc8dc97ffad5037e7239875f80b22"
SRC_URI[genwqe-zlib-devel.sha256sum] = "2374e4cced3777c48bd900d554f8427977d00edfd68487ff767fb436e75aae37"
