SUMMARY = "generated recipe based on mingw-sqlite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-sqlite = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-pkg-config pkgconf-pkg-config"
RDEPENDS_mingw32-sqlite-static = "mingw32-sqlite"
RDEPENDS_mingw64-sqlite = "mingw64-crt mingw64-filesystem mingw64-pkg-config pkgconf-pkg-config"
RDEPENDS_mingw64-sqlite-static = "mingw64-sqlite"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-sqlite-3.22.0.0-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-sqlite-static-3.22.0.0-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-sqlite-3.22.0.0-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-sqlite-static-3.22.0.0-3.el8.noarch.rpm \
          "

SRC_URI[mingw32-sqlite.sha256sum] = "1eac078219b4510fe5ef89c8990d21f79544ba5dddd086299b156fd2fab3123b"
SRC_URI[mingw32-sqlite-static.sha256sum] = "1559450b4bf053b8d2c41e32e4fd37187a11a587d0fd00316f49919aa6de7ebc"
SRC_URI[mingw64-sqlite.sha256sum] = "7645247a8ff36f12180cf47fdcf5ff07d0de9c4960e32dd0b93e31888df51636"
SRC_URI[mingw64-sqlite-static.sha256sum] = "9c291dc4f29959638327bcf284bffe554c5f2f01c172b00ec7999ef4deff794f"
