SUMMARY = "generated recipe based on jq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "oniguruma pkgconfig-native"
RPM_SONAME_PROV_jq = "libjq.so.1"
RPM_SONAME_REQ_jq = "ld-linux-x86-64.so.2 libc.so.6 libjq.so.1 libm.so.6 libonig.so.5"
RDEPENDS_jq = "glibc oniguruma"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jq-1.5-12.el8.x86_64.rpm \
          "

SRC_URI[jq.sha256sum] = "65d37df250e218828523bea2882f4bfad1ffc4dcc0f56aadd92b9c5265e7ae37"
