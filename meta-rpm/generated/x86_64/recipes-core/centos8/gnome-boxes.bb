SUMMARY = "generated recipe based on gnome-boxes srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo gdk-pixbuf glib-2.0 gtk+3 gtk-vnc libarchive libgovirt libgudev libosinfo libsecret libsoup-2.4 libusb1 libvirt-glib libxml2 pango pkgconfig-native rest spice-gtk tracker webkit2gtk3"
RPM_SONAME_REQ_gnome-boxes = "libarchive.so.13 libc.so.6 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgovirt.so.2 libgtk-3.so.0 libgtk-vnc-2.0.so.0 libgudev-1.0.so.0 libosinfo-1.0.so.0 libpango-1.0.so.0 libpthread.so.0 librest-0.7.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libspice-client-glib-2.0.so.8 libspice-client-gtk-3.0.so.5 libtracker-sparql-2.0.so.0 libusb-1.0.so.0 libvirt-gconfig-1.0.so.0 libvirt-gobject-1.0.so.0 libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_gnome-boxes = "adwaita-icon-theme cairo gdk-pixbuf2 genisoimage glib2 glibc gtk-vnc2 gtk3 libarchive libgovirt libgudev libosinfo libsecret libsoup libusbx libvirt-daemon-config-network libvirt-daemon-kvm libvirt-gconfig libvirt-gobject libxml2 mtools pango rest spice-glib spice-gtk3 tracker webkit2gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-boxes-3.28.5-8.el8.x86_64.rpm \
          "

SRC_URI[gnome-boxes.sha256sum] = "fd09be27f6cca2a85c811a944ed78360c151c7c7b71eb712eeb3885924d5ba53"
