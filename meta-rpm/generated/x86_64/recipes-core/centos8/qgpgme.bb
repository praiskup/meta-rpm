SUMMARY = "generated recipe based on gpgme srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libassuan libgcc libgpg-error pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qgpgme = "libqgpgme.so.7"
RPM_SONAME_REQ_qgpgme = "libQt5Core.so.5 libassuan.so.0 libc.so.6 libgcc_s.so.1 libgpg-error.so.0 libgpgme.so.11 libgpgmepp.so.6 libm.so.6 libstdc++.so.6"
RDEPENDS_qgpgme = "glibc gpgme gpgmepp libassuan libgcc libgpg-error libstdc++ qt5-qtbase"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qgpgme-1.10.0-6.el8.0.1.x86_64.rpm \
          "

SRC_URI[qgpgme.sha256sum] = "e4b478114e11a6c9446ff5a41d49aa4500aa94504c6b7edb149e8136a9789397"
