SUMMARY = "generated recipe based on hunspell-bn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-bn = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-bn-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-bn.sha256sum] = "ab2553dbacec0620cf15581d570431c5103a7a4878a4f75011e04ec62ab70274"
