SUMMARY = "generated recipe based on cmocka srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libcmocka = "libcmocka.so.0"
RPM_SONAME_REQ_libcmocka = "ld-linux-x86-64.so.2 libc.so.6 librt.so.1"
RDEPENDS_libcmocka = "glibc"
RPM_SONAME_REQ_libcmocka-devel = "libcmocka.so.0"
RPROVIDES_libcmocka-devel = "libcmocka-dev (= 1.1.5)"
RDEPENDS_libcmocka-devel = "cmake-filesystem libcmocka pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcmocka-1.1.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcmocka-devel-1.1.5-1.el8.x86_64.rpm \
          "

SRC_URI[libcmocka.sha256sum] = "e0f83e21bdf73191930f49de5202de69c67558344838aac8735ded537b82253b"
SRC_URI[libcmocka-devel.sha256sum] = "dbda7cdc247582418999b6067f443bf6b775e2e2ae26cb7de5f08a83a81843fc"
