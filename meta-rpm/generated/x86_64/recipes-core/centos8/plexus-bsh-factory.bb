SUMMARY = "generated recipe based on plexus-bsh-factory srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-bsh-factory = "bsh java-1.8.0-openjdk-headless javapackages-filesystem plexus-classworlds plexus-containers-container-default plexus-utils"
RDEPENDS_plexus-bsh-factory-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-bsh-factory-1.0-0.19.a7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-bsh-factory-javadoc-1.0-0.19.a7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-bsh-factory.sha256sum] = "5f2b21a174b9b901b5a03a35b0cbbaf65451dc963e58a379208553ea30d2bccc"
SRC_URI[plexus-bsh-factory-javadoc.sha256sum] = "d4188da8fee6094a5730c5bb807fcf81db7f63bf9a19ff1351d28d53ea989155"
