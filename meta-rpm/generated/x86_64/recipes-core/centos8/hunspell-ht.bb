SUMMARY = "generated recipe based on hunspell-ht srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ht = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ht-0.06-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-ht.sha256sum] = "78e9ca3ae145615d453e25ca06c6dce30517f9b58c667009acf36a85c69ac57b"
