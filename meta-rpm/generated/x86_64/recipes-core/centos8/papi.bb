SUMMARY = "generated recipe based on papi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc libgcc libpfm pkgconfig-native"
RPM_SONAME_REQ_papi = "libc.so.6 libdl.so.2 libm.so.6 libpapi.so.5"
RDEPENDS_papi = "glibc papi-libs"
RPM_SONAME_REQ_papi-devel = "libpapi.so.5"
RPROVIDES_papi-devel = "papi-dev (= 5.6.0)"
RDEPENDS_papi-devel = "papi papi-libs pkgconf-pkg-config"
RPM_SONAME_PROV_papi-libs = "libpapi.so.5"
RPM_SONAME_REQ_papi-libs = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libpfm.so.4"
RDEPENDS_papi-libs = "glibc libpfm"
RPM_SONAME_REQ_papi-testsuite = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpapi.so.5 libpthread.so.0 libquadmath.so.0 librt.so.1"
RDEPENDS_papi-testsuite = "bash glibc libgcc libgfortran libgomp libquadmath papi papi-libs perl-Getopt-Long perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/papi-5.6.0-9.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/papi-devel-5.6.0-9.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/papi-libs-5.6.0-9.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/papi-testsuite-5.6.0-9.el8_2.x86_64.rpm \
          "

SRC_URI[papi.sha256sum] = "ef7604187f8e05912318f3a26a04f09ce3ea19ff2126e8fa29d1cdf7219f8e45"
SRC_URI[papi-devel.sha256sum] = "36815557ad982f565b3de4933a9d857ce9f2251ba23b3ec076cb89fcf1860486"
SRC_URI[papi-libs.sha256sum] = "47c6fee6fdb8f645b4a9001cd061de9c4c549ad6f5b5f69898bf8371945af679"
SRC_URI[papi-testsuite.sha256sum] = "bded28bc4c3f0d8025e4394bb648009998f3997305ffb6ccdbb4419e1f4ce556"
