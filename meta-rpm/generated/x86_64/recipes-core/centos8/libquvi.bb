SUMMARY = "generated recipe based on libquvi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 libgcrypt libgpg-error libproxy lua pkgconfig-native"
RPM_SONAME_PROV_libquvi = "libquvi-0.9-0.9.4.so"
RPM_SONAME_REQ_libquvi = "libc.so.6 libcurl.so.4 libdl.so.2 libgcrypt.so.20 libglib-2.0.so.0 libgpg-error.so.0 liblua-5.3.so libm.so.6 libproxy.so.1"
RDEPENDS_libquvi = "glib2 glibc libcurl libgcrypt libgpg-error libproxy libquvi-scripts lua-libs"
RPROVIDES_libquvi-devel = "libquvi-dev (= 0.9.4)"
RDEPENDS_libquvi-devel = "libquvi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libquvi-0.9.4-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libquvi-devel-0.9.4-12.el8.x86_64.rpm \
          "

SRC_URI[libquvi.sha256sum] = "e11e9498122effd247f7451265c42f31f3d3f200ec5ea3e599366981fe3704d3"
SRC_URI[libquvi-devel.sha256sum] = "d6dbae987c7c583efbc03925239c7847184ec168ec4bc04c1ef216e5c625bf35"
