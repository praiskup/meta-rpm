SUMMARY = "generated recipe based on lua-socket srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lua-socket = "libc.so.6"
RDEPENDS_lua-socket = "glibc lua"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lua-socket-3.0-0.17.rc1.el8.x86_64.rpm \
          "

SRC_URI[lua-socket.sha256sum] = "62080b86ab507a77dda5def96784aec47b8bbbee1854a758bf655d5df12a191b"
