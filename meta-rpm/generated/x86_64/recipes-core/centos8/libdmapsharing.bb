SUMMARY = "generated recipe based on libdmapsharing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "avahi avahi-libs gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libsoup-2.4 pkgconfig-native zlib"
RPM_SONAME_PROV_libdmapsharing = "libdmapsharing-3.0.so.2"
RPM_SONAME_REQ_libdmapsharing = "libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgthread-2.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libz.so.1"
RDEPENDS_libdmapsharing = "avahi-glib avahi-libs gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base libsoup zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdmapsharing-2.9.37-5.el8.x86_64.rpm \
          "

SRC_URI[libdmapsharing.sha256sum] = "a43ea1534c281039335a982cd6b9a730ee1765cbe7bef8f12d4e96691e7b07d0"
