SUMMARY = "generated recipe based on at-spi2-atk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "at-spi2-core atk dbus-libs glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_at-spi2-atk = "libatk-bridge-2.0.so.0 libatk-bridge.so"
RPM_SONAME_REQ_at-spi2-atk = "libatk-1.0.so.0 libatk-bridge-2.0.so.0 libatspi.so.0 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_at-spi2-atk = "at-spi2-core atk dbus-libs glib2 glibc"
RPM_SONAME_REQ_at-spi2-atk-devel = "libatk-bridge-2.0.so.0"
RPROVIDES_at-spi2-atk-devel = "at-spi2-atk-dev (= 2.26.2)"
RDEPENDS_at-spi2-atk-devel = "at-spi2-atk at-spi2-core-devel glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/at-spi2-atk-2.26.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/at-spi2-atk-devel-2.26.2-1.el8.x86_64.rpm \
          "

SRC_URI[at-spi2-atk.sha256sum] = "1d63dc4f754e976a73376541ecda8dfb6ed52d79b70f4307b6e465222f41b0cc"
SRC_URI[at-spi2-atk-devel.sha256sum] = "8edc6bc270895a6cee5d16c6a593e25b0885355caa81b378dfe6b40972fd6e28"
