SUMMARY = "generated recipe based on upower srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgudev libimobiledevice libplist libusb1 pkgconfig-native"
RPM_SONAME_PROV_upower = "libupower-glib.so.3"
RPM_SONAME_REQ_upower = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libimobiledevice.so.6 libplist.so.3 libupower-glib.so.3 libusb-1.0.so.0"
RDEPENDS_upower = "glib2 glibc gobject-introspection libgudev libimobiledevice libplist libusbx systemd-udev"
RPM_SONAME_REQ_upower-devel = "libupower-glib.so.3"
RPROVIDES_upower-devel = "upower-dev (= 0.99.7)"
RDEPENDS_upower-devel = "glib2-devel pkgconf-pkg-config upower"
RDEPENDS_upower-devel-docs = "upower"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/upower-0.99.7-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/upower-devel-0.99.7-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/upower-devel-docs-0.99.7-3.el8.noarch.rpm \
          "

SRC_URI[upower.sha256sum] = "7a3f46ece9fe35bd8506ef6949ba0103ce7f216e439f56266ce521096f61004d"
SRC_URI[upower-devel.sha256sum] = "ad334d38c4b0fbdd395116a55032f44eb8856f812fe3cb72850e277b20a12182"
SRC_URI[upower-devel-docs.sha256sum] = "fa0448182dffc5756fcd95bd49b1e732fc01f94c655119f8e0144347f43dcc51"
