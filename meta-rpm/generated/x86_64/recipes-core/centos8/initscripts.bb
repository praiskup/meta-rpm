SUMMARY = "generated recipe based on initscripts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native popt"
RPM_SONAME_REQ_initscripts = "libc.so.6 libglib-2.0.so.0 libpopt.so.0"
RDEPENDS_initscripts = "bash coreutils filesystem findutils gawk glib2 glibc grep popt procps-ng setup shadow-utils systemd util-linux"
RDEPENDS_netconsole-service = "bash coreutils filesystem gawk glibc-common initscripts iproute iputils kmod sed util-linux"
RPM_SONAME_REQ_network-scripts = "libc.so.6"
RDEPENDS_network-scripts = "bash chkconfig coreutils dbus filesystem gawk glibc grep hostname initscripts ipcalc iproute kmod procps-ng sed systemd"
RDEPENDS_readonly-root = "bash coreutils cpio filesystem findutils gawk hostname initscripts ipcalc iproute util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/initscripts-10.00.6-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/netconsole-service-10.00.6-1.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/network-scripts-10.00.6-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/readonly-root-10.00.6-1.el8_2.2.noarch.rpm \
          "

SRC_URI[initscripts.sha256sum] = "979fcf574b0353e532a32fb82c329dc15a724ffd5d097e422a0ce2fcffd98dc9"
SRC_URI[netconsole-service.sha256sum] = "6014cc61b7b137768b81982bde91ba79978fc234f4a7f1a29f73ba710a284856"
SRC_URI[network-scripts.sha256sum] = "842a64e88f20fc9f9697abc6be026b093e7e6c2133d908d35ae7f541e73e4b4b"
SRC_URI[readonly-root.sha256sum] = "d88137051e13445a3542152c01bc244cd5b5c91fc0ff7ad0b710fde550e5b129"
