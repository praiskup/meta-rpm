SUMMARY = "generated recipe based on gpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_gpm = "libc.so.6 libgpm.so.2 libm.so.6"
RDEPENDS_gpm = "bash glibc gpm-libs info linuxconsoletools systemd"
RPM_SONAME_REQ_gpm-devel = "libgpm.so.2"
RPROVIDES_gpm-devel = "gpm-dev (= 1.20.7)"
RDEPENDS_gpm-devel = "gpm gpm-libs"
RPM_SONAME_PROV_gpm-libs = "libgpm.so.2"
RPM_SONAME_REQ_gpm-libs = "libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_gpm-libs = "glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gpm-1.20.7-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gpm-devel-1.20.7-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gpm-libs-1.20.7-15.el8.x86_64.rpm \
          "

SRC_URI[gpm.sha256sum] = "35a97f7867c7f18d5093fd39b43411158900eedc131a31f71aa3a9d34e397f8e"
SRC_URI[gpm-devel.sha256sum] = "25dbd0c534642441c3aa414330eb23cebf969e909f2780c73ce54d91dd335403"
SRC_URI[gpm-libs.sha256sum] = "68c617d21f03ada63f07a334c43e272c459404d2848944d9dccc9bffc1b1637e"
