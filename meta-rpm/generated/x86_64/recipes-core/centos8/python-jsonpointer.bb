SUMMARY = "generated recipe based on python-jsonpointer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jsonpointer = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-jsonpointer-1.10-11.el8.noarch.rpm \
          "

SRC_URI[python3-jsonpointer.sha256sum] = "60b0cbc5e435be346bb06f45f3336f8936d7362022d8bceda80ff16bc3fb7dd2"
