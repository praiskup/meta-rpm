SUMMARY = "generated recipe based on openssh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk audit cairo e2fsprogs fipscheck fontconfig freetype gdk-pixbuf glib-2.0 gtk2 krb5-libs libedit libselinux libx11 libxcrypt openldap openssl pam pango pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_openssh = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh = "audit-libs bash fipscheck-lib glibc libselinux libxcrypt openssl-libs util-linux zlib"
RPM_SONAME_REQ_openssh-askpass = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_openssh-askpass = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libX11 openssh pango"
RPM_SONAME_REQ_openssh-cavs = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-cavs = "fipscheck-lib glibc libselinux libxcrypt openssh openssl-libs perl-interpreter perl-libs zlib"
RPM_SONAME_REQ_openssh-clients = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libedit.so.0 libfipscheck.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-clients = "bash crypto-policies fipscheck-lib glibc krb5-libs libcom_err libedit libselinux libxcrypt openssh openssl-libs zlib"
RPM_SONAME_REQ_openssh-keycat = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libpam.so.0 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-keycat = "fipscheck-lib glibc libselinux libxcrypt openssh openssl-libs pam zlib"
RPM_SONAME_REQ_openssh-ldap = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libresolv.so.2 libselinux.so.1 libutil.so.1 libz.so.1"
RDEPENDS_openssh-ldap = "bash fipscheck-lib glibc libselinux libxcrypt openldap openssh openssl-libs zlib"
RPM_SONAME_REQ_openssh-server = "libaudit.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libfipscheck.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpam.so.0 libresolv.so.2 libselinux.so.1 libsystemd.so.0 libutil.so.1 libz.so.1"
RDEPENDS_openssh-server = "audit-libs bash crypto-policies fipscheck-lib glibc krb5-libs libcom_err libselinux libxcrypt openssh openssl-libs pam shadow-utils systemd systemd-libs zlib"
RPM_SONAME_REQ_pam_ssh_agent_auth = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libpam.so.0 libresolv.so.2 libselinux.so.1 libutil.so.1"
RDEPENDS_pam_ssh_agent_auth = "glibc libselinux libxcrypt openssl-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openssh-askpass-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssh-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssh-cavs-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssh-clients-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssh-keycat-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssh-ldap-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssh-server-8.0p1-4.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/pam_ssh_agent_auth-0.10.3-7.4.el8_1.x86_64.rpm \
          "

SRC_URI[openssh.sha256sum] = "c5ab2fca9a2fb34f1273cc4218f9f745dff2384c7be8477938d929edf7efa491"
SRC_URI[openssh-askpass.sha256sum] = "a6d3e7b71b3c95fd6d28eb5bfc18468dde3a4486362ec1abd33fc1d08e16ca7f"
SRC_URI[openssh-cavs.sha256sum] = "14b8b8c6f45ac51b133c58140da03eb028e93221de720942405f92159881cc3f"
SRC_URI[openssh-clients.sha256sum] = "8d33890840d5162604b942278b8f835c187185e03901745bb956b90d08eeaa00"
SRC_URI[openssh-keycat.sha256sum] = "cd5c48c001bdca5fe8bd32efc2511dee428bec36525fbc2fa60e70ead1e541c1"
SRC_URI[openssh-ldap.sha256sum] = "0b91b0f1ee56ed6481c763fd084a36bf669d0e675937d29f64cf7edc4bf6c189"
SRC_URI[openssh-server.sha256sum] = "ddc60e8d750c1477119c0720f85b09ba491bf4e27f06d5543dfeae3e8bdf1d98"
SRC_URI[pam_ssh_agent_auth.sha256sum] = "f76f2b388ee9d50bde29729485943c4cedb56ae0e2f9e53d18def04b21ccfce4"
