SUMMARY = "generated recipe based on man-pages srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/man-pages-4.15-6.el8.x86_64.rpm \
          "

SRC_URI[man-pages.sha256sum] = "67a86aa6105425a06b01f753121d4e0e89214bae60a1f7c9e50679d2001cef40"
