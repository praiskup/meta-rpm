SUMMARY = "generated recipe based on nmap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libpcap libpcre openssl pkgconfig-native"
RPM_SONAME_REQ_nmap = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpcap.so.1 libpcre.so.1 libssl.so.1.1 libstdc++.so.6"
RDEPENDS_nmap = "glibc libgcc libpcap libstdc++ nmap-ncat openssl-libs pcre"
RPM_SONAME_REQ_nmap-ncat = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpcap.so.1 libssl.so.1.1"
RDEPENDS_nmap-ncat = "bash glibc libpcap openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nmap-7.70-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nmap-ncat-7.70-5.el8.x86_64.rpm \
          "

SRC_URI[nmap.sha256sum] = "f8765989b2e16742809c72b9ff3aedb5856d7fd2f2d3a377b7cbcedf56511d08"
SRC_URI[nmap-ncat.sha256sum] = "114353eb1f2c97125230c40385039824d83032672e64629f9c589393a361dfed"
