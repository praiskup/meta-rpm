SUMMARY = "generated recipe based on perl-List-MoreUtils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-List-MoreUtils = "perl-Carp perl-Exporter-Tiny perl-List-MoreUtils-XS perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-List-MoreUtils-0.428-2.el8.noarch.rpm \
          "

SRC_URI[perl-List-MoreUtils.sha256sum] = "b4f744bd61a1f9bea052275a48d94e8ee883907ce4f33798179124d07883ef0b"
