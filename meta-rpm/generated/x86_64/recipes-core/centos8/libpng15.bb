SUMMARY = "generated recipe based on libpng15 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libpng15 = "libpng15.so.15"
RPM_SONAME_REQ_libpng15 = "libc.so.6 libm.so.6 libz.so.1"
RDEPENDS_libpng15 = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpng15-1.5.30-7.el8.x86_64.rpm \
          "

SRC_URI[libpng15.sha256sum] = "fbc42bff114db645676b7a165f5b239ff03f148b48d9c91408193a0d0efa4feb"
