SUMMARY = "generated recipe based on pptp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_pptp = "libc.so.6 libutil.so.1"
RDEPENDS_pptp = "glibc iproute ppp systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pptp-1.10.0-3.el8.x86_64.rpm \
          "

SRC_URI[pptp.sha256sum] = "9b5fcf5fc0aa918271dd8b5142d5feca234abaf69bd4c0a0a0fabedd8d6418ab"
