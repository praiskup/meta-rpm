SUMMARY = "generated recipe based on libselinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-libselinux = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libselinux.so.1"
RDEPENDS_python3-libselinux = "glibc libselinux platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libselinux-2.9-3.el8.x86_64.rpm \
          "

SRC_URI[python3-libselinux.sha256sum] = "79e6582af418a297c56ec9849743b6cc06b172b02351696bef6f43d6b2fbab49"
