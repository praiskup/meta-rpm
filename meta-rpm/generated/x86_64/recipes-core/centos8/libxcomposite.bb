SUMMARY = "generated recipe based on libXcomposite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "xcomposite"
DEPENDS = "libx11 libxfixes pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXcomposite = "libXcomposite.so.1"
RPM_SONAME_REQ_libXcomposite = "libX11.so.6 libc.so.6"
RDEPENDS_libXcomposite = "glibc libX11"
RPM_SONAME_REQ_libXcomposite-devel = "libXcomposite.so.1"
RPROVIDES_libXcomposite-devel = "libXcomposite-dev (= 0.4.4)"
RDEPENDS_libXcomposite-devel = "libX11-devel libXcomposite libXfixes-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXcomposite-0.4.4-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXcomposite-devel-0.4.4-14.el8.x86_64.rpm \
          "

SRC_URI[libXcomposite.sha256sum] = "4a93949edfc613fd6f42844e7c29951f357f085ac36541a676c421165914b4d4"
SRC_URI[libXcomposite-devel.sha256sum] = "eecd3454c633beabd90b9ae082b754e1ee86250a38f08de0479b07ee8c1f7259"
