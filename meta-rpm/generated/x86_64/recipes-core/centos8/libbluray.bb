SUMMARY = "generated recipe based on libbluray srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype libxml2 pkgconfig-native"
RPM_SONAME_PROV_libbluray = "libbluray.so.2"
RPM_SONAME_REQ_libbluray = "libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libpthread.so.0 libxml2.so.2"
RDEPENDS_libbluray = "fontconfig freetype glibc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libbluray-1.0.2-3.el8.x86_64.rpm \
          "

SRC_URI[libbluray.sha256sum] = "48c813002b21f9399b425815f07ae3480a47e61e00c1d44aeb2ad76e3b936619"
