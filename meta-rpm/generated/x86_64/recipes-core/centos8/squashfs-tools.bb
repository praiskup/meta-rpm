SUMMARY = "generated recipe based on squashfs-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc lz4 lzo pkgconfig-native xz zlib"
RPM_SONAME_REQ_squashfs-tools = "libc.so.6 libgcc_s.so.1 liblz4.so.1 liblzma.so.5 liblzo2.so.2 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_squashfs-tools = "glibc libgcc lz4-libs lzo xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/squashfs-tools-4.3-19.el8.x86_64.rpm \
          "

SRC_URI[squashfs-tools.sha256sum] = "a0c8f83cef355dd98d7750e3d8eb3e9f3e9f8f5e9a3ee441cb4bc4014c647e31"
