SUMMARY = "generated recipe based on python-imagesize srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-imagesize = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-imagesize-1.0.0-2.el8.noarch.rpm \
          "

SRC_URI[python3-imagesize.sha256sum] = "70b26b6b3d17a10d2da975936b3d41cf85d7739f2b1921ba7073d777544d61a4"
