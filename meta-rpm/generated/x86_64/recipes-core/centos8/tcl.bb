SUMMARY = "generated recipe based on tcl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_tcl = "libtcl8.6.so"
RPM_SONAME_REQ_tcl = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libtcl8.6.so libz.so.1"
RDEPENDS_tcl = "glibc zlib"
RPROVIDES_tcl-devel = "tcl-dev (= 8.6.8)"
RDEPENDS_tcl-devel = "pkgconf-pkg-config tcl zlib-devel"
RDEPENDS_tcl-doc = "tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tcl-8.6.8-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tcl-devel-8.6.8-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tcl-doc-8.6.8-2.el8.noarch.rpm \
          "

SRC_URI[tcl.sha256sum] = "be0a4b52ecb25dc0ddcd39b297cdcc16a373a1b3cec7704e3d1e3f3dd352d697"
SRC_URI[tcl-devel.sha256sum] = "dad07cadf28ede3aea0f16ab8a17724c7e5a5a77f07a6b5daf6dd075de2b2a25"
SRC_URI[tcl-doc.sha256sum] = "b7804f68ff5391ec2132c2a46e3fd2ca8ebc287271c2c6aa827b9f6ac5bc7204"
