SUMMARY = "generated recipe based on hyphen-is srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-is = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-is-0.20030920-19.el8.noarch.rpm \
          "

SRC_URI[hyphen-is.sha256sum] = "2edb61507c859fbc2e1c1cb5a4ced60790f52b876cecb6f89f02ca03dc2e20d3"
