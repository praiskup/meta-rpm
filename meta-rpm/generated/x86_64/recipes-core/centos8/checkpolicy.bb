SUMMARY = "generated recipe based on checkpolicy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_checkpolicy = "libc.so.6"
RDEPENDS_checkpolicy = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/checkpolicy-2.9-1.el8.x86_64.rpm \
          "

SRC_URI[checkpolicy.sha256sum] = "d5c283da0d2666742635754626263f6f78e273cd46d83d2d66ed43730a731685"
