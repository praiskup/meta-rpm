SUMMARY = "generated recipe based on perl-XML-Parser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat perl pkgconfig-native"
RPM_SONAME_REQ_perl-XML-Parser = "libc.so.6 libexpat.so.1 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-XML-Parser = "expat glibc perl-Carp perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-XML-Parser-2.44-11.el8.x86_64.rpm \
          "

SRC_URI[perl-XML-Parser.sha256sum] = "4d37203728fd7f2e5628a459dd08adf5d368decb23bc5a4f5a594d424cbe9dad"
