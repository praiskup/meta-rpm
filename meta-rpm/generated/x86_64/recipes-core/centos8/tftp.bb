SUMMARY = "generated recipe based on tftp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native readline"
RPM_SONAME_REQ_tftp = "libc.so.6 libreadline.so.7 libtinfo.so.6"
RDEPENDS_tftp = "glibc ncurses-libs readline"
RPM_SONAME_REQ_tftp-server = "libc.so.6"
RDEPENDS_tftp-server = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tftp-5.2-24.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tftp-server-5.2-24.el8.x86_64.rpm \
          "

SRC_URI[tftp.sha256sum] = "767a3e908b4b2dab3212f571aaf645f7d62ad1cee76ebbc739ebc1dd46db70dc"
SRC_URI[tftp-server.sha256sum] = "a2f60f516c08099d37c3bd06204ca3a51537c150db6c3a326eec5e7b57336535"
