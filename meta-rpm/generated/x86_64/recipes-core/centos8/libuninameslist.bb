SUMMARY = "generated recipe based on libuninameslist srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuninameslist = "libuninameslist.so.1"
RPM_SONAME_REQ_libuninameslist = "libc.so.6"
RDEPENDS_libuninameslist = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libuninameslist-20170701-4.el8.x86_64.rpm \
          "

SRC_URI[libuninameslist.sha256sum] = "685dd76854fcbd764d51048b7fbff487ace87d5598d04e68f85e62a6e840915e"
