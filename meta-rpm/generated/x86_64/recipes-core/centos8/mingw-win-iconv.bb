SUMMARY = "generated recipe based on mingw-win-iconv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-win-iconv = "mingw32-crt mingw32-filesystem"
RDEPENDS_mingw32-win-iconv-static = "mingw32-win-iconv"
RDEPENDS_mingw64-win-iconv = "mingw64-crt mingw64-filesystem"
RDEPENDS_mingw64-win-iconv-static = "mingw64-win-iconv"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-win-iconv-0.0.6-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-win-iconv-static-0.0.6-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-win-iconv-0.0.6-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-win-iconv-static-0.0.6-9.el8.noarch.rpm \
          "

SRC_URI[mingw32-win-iconv.sha256sum] = "41020a992d6355b442a19c99e83ea54d8e18d751da6357c9a17646360cad0854"
SRC_URI[mingw32-win-iconv-static.sha256sum] = "5fcb179698713c7bb4c0a982c98d7b3f353ddb4e2ea828802d71cbfc34318d62"
SRC_URI[mingw64-win-iconv.sha256sum] = "dacfcddf532c6ea3e9f8cb514e09e5667eb4b045d6024cdd1dc5ff6bce7aca0c"
SRC_URI[mingw64-win-iconv-static.sha256sum] = "7f0f1ec03767def2555e9bc19260037509d7c7fffda0e79ad2c0a04414c1cb0b"
