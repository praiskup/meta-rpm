SUMMARY = "generated recipe based on usb_modeswitch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_REQ_usb_modeswitch = "libc.so.6 libusb-1.0.so.0"
RDEPENDS_usb_modeswitch = "bash glibc jimtcl libusbx systemd usb_modeswitch-data"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/usb_modeswitch-2.5.2-1.el8.x86_64.rpm \
          "

SRC_URI[usb_modeswitch.sha256sum] = "558406483c59102d13bcc0bc18bc5210c13d34dd8715c6badf74d644f3682a8b"
