SUMMARY = "generated recipe based on fontpackages srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_fontpackages-devel = "fontpackages-dev (= 1.44)"
RDEPENDS_fontpackages-devel = "fontconfig fontpackages-filesystem rpmdevtools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fontpackages-filesystem-1.44-22.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fontpackages-devel-1.44-22.el8.noarch.rpm \
          "

SRC_URI[fontpackages-devel.sha256sum] = "3549642e88877f0a6c71d89690925d90ab75a8a598e6b5827574359a6ac37f4b"
SRC_URI[fontpackages-filesystem.sha256sum] = "700b9050aa490b5eca6d1f8630cbebceb122fce11c370689b5ccb37f5a43ee2f"
