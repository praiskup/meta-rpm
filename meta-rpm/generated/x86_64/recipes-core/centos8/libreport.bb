SUMMARY = "generated recipe based on libreport srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "augeas curl dbus-libs glib-2.0 json-c libnewt libproxy libtar libxml2 pkgconfig-native platform-python3 satyr systemd-libs xmlrpc-c"
RPM_SONAME_PROV_libreport = "libabrt_dbus.so.0 libreport.so.0"
RPM_SONAME_REQ_libreport = "libaugeas.so.0 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport = "augeas-libs dbus-libs glib2 glibc libreport-filesystem libtar lz4 satyr systemd-libs xz"
RDEPENDS_libreport-anaconda = "libreport libreport-plugin-reportuploader libreport-plugin-rhtsupport"
RPM_SONAME_REQ_libreport-cli = "libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-cli = "augeas-libs glib2 glibc libreport libtar satyr systemd-libs"
RPM_SONAME_REQ_libreport-newt = "libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libnewt.so.0.52 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-newt = "augeas-libs glib2 glibc libreport libtar newt satyr systemd-libs"
RPM_SONAME_REQ_libreport-plugin-bugzilla = "libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-bugzilla = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-kerneloops = "libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-kerneloops = "augeas-libs curl glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-logger = "libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-plugin-logger = "augeas-libs glib2 glibc libreport libtar satyr systemd-libs"
RPM_SONAME_REQ_libreport-plugin-mailx = "libaugeas.so.0 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-plugin-mailx = "augeas-libs glib2 glibc libreport libtar mailx satyr systemd-libs"
RPM_SONAME_REQ_libreport-plugin-reportuploader = "libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-reportuploader = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-rhtsupport = "libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-rhtsupport = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_libreport-plugin-ureport = "libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport-web.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-plugin-ureport = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libreport-web libtar libxml2 python3-subscription-manager-rhsm satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RDEPENDS_libreport-rhel = "libreport"
RDEPENDS_libreport-rhel-anaconda-bugzilla = "libreport libreport-plugin-bugzilla"
RDEPENDS_libreport-rhel-bugzilla = "libreport libreport-plugin-bugzilla libreport-plugin-ureport"
RPM_SONAME_PROV_libreport-web = "libreport-web.so.0"
RPM_SONAME_REQ_libreport-web = "libaugeas.so.0 libc.so.6 libcurl.so.4 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libproxy.so.1 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1 libxml2.so.2 libxmlrpc.so.3 libxmlrpc_client.so.3 libxmlrpc_util.so.4"
RDEPENDS_libreport-web = "augeas-libs glib2 glibc json-c libcurl libproxy libreport libtar libxml2 satyr systemd-libs xmlrpc-c xmlrpc-c-client"
RPM_SONAME_REQ_python3-libreport = "libaugeas.so.0 libc.so.6 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_python3-libreport = "augeas-libs glib2 glibc libreport libtar platform-python python3-dnf python3-libs satyr systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-anaconda-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-cli-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-newt-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-bugzilla-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-kerneloops-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-logger-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-mailx-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-reportuploader-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-rhtsupport-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-plugin-ureport-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-rhel-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-rhel-anaconda-bugzilla-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-rhel-bugzilla-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-web-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-libreport-2.9.5-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libreport-filesystem-2.9.5-10.el8.x86_64.rpm \
          "

SRC_URI[libreport.sha256sum] = "ea7e9d39c33438a872ea22d4de0eef2215d496fc602337eafa0ff0c127c338e8"
SRC_URI[libreport-anaconda.sha256sum] = "bfe4a90e6a4e70570d55d72bb22d2e92bd546701285cdb4cc16a0439bffc1c3c"
SRC_URI[libreport-cli.sha256sum] = "f91ea027d84f205fe4dbfadd9d059a007f6e6de788af0ee46ae28ea12ea4b18c"
SRC_URI[libreport-filesystem.sha256sum] = "5c26afe4fccc5652252ca02f0e5189891c5875fa7ad4cb861e504620b5203be3"
SRC_URI[libreport-newt.sha256sum] = "bd716c6d4c143e6c2da357e6c3c4186487b30effd539227dac8678b1379ea343"
SRC_URI[libreport-plugin-bugzilla.sha256sum] = "ee26a612a9d19a81552c9aadf79684be4a8c5343600f3a6efa7a560bfff7e014"
SRC_URI[libreport-plugin-kerneloops.sha256sum] = "4aa356298b49bce1b593d59587b7238f737afdd9bde079fb6d9f7f12c4e715fb"
SRC_URI[libreport-plugin-logger.sha256sum] = "c03b2db339cf9fa61dbb4465d5413f0d6477767a6be5646d52168846025ad690"
SRC_URI[libreport-plugin-mailx.sha256sum] = "d8c6a039e5ad141b9e9745f71c1f7637d936fbc3fd7a16f1b850909ac337a70e"
SRC_URI[libreport-plugin-reportuploader.sha256sum] = "bbe99acd0f80c5a8ef3e567e3c7442011ab094d97b452c16f52bdc5a9d72def6"
SRC_URI[libreport-plugin-rhtsupport.sha256sum] = "1b8afee96df5c72486229884c19cb72176f3468bfc0ead4669d5ef66b624fb98"
SRC_URI[libreport-plugin-ureport.sha256sum] = "544a38d14f9dd5902fff244112e38158943f3e3ac3a2fe35e2165941da93e97b"
SRC_URI[libreport-rhel.sha256sum] = "c66ec143bc3d24ef52ab7cbd477df9ff4c469e3a479810de44abb5e14c910e90"
SRC_URI[libreport-rhel-anaconda-bugzilla.sha256sum] = "2c37c6e9b62f8fc41641b8e9c4dc6b514d1c833e9b432a339b8d75b36a0e3a13"
SRC_URI[libreport-rhel-bugzilla.sha256sum] = "d48e83a663f59bb300deef48b8d2d4964400cb88aab98a0204b94d553683a3a6"
SRC_URI[libreport-web.sha256sum] = "0411b58a748ec8df6ad743b50a8cc600d2c582106848d6f0cead2033f0dfacf0"
SRC_URI[python3-libreport.sha256sum] = "7975150123a51fe03ccb0efb1b06699059413017c6448a063ba20ce57033ea02"
