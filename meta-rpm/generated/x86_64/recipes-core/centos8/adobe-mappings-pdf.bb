SUMMARY = "generated recipe based on adobe-mappings-pdf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/adobe-mappings-pdf-20180407-1.el8.noarch.rpm \
          "

SRC_URI[adobe-mappings-pdf.sha256sum] = "19d1c57a8549319046cefec70b9268e8e7b09190ca52bd579dc2e20a63756298"
