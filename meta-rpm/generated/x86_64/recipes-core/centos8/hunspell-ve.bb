SUMMARY = "generated recipe based on hunspell-ve srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ve = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ve-0.20091030-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-ve.sha256sum] = "af4f5b37d8ba688ee72f51c72a3eda8e4c9c419f8109ace60b75171c937690e9"
