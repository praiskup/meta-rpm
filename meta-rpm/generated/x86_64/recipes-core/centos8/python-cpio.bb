SUMMARY = "generated recipe based on python-cpio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-cpio = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-cpio-0.1-29.el8.noarch.rpm \
          "

SRC_URI[python3-cpio.sha256sum] = "fa5de5be9a703bb9e7209179339e180b59da108ba0b2adb26e31fe29dae166d5"
