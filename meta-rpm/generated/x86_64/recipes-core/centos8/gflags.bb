SUMMARY = "generated recipe based on gflags srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_gflags = "libgflags.so.2.1 libgflags_nothreads.so.2.1"
RPM_SONAME_REQ_gflags = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gflags = "bash glibc libgcc libstdc++"
RPM_SONAME_REQ_gflags-devel = "libgflags.so.2.1 libgflags_nothreads.so.2.1"
RPROVIDES_gflags-devel = "gflags-dev (= 2.1.2)"
RDEPENDS_gflags-devel = "cmake-filesystem gflags"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gflags-2.1.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gflags-devel-2.1.2-6.el8.x86_64.rpm \
          "

SRC_URI[gflags.sha256sum] = "af049d0caf1b94ff45b67b71ac4efbf8c6835e9e63effbdd06a40e36cd9d8504"
SRC_URI[gflags-devel.sha256sum] = "b54fe59ca8e008fee8f4f06c0e9d5cb183e4648109bbf873a8e7514f673a92a7"
