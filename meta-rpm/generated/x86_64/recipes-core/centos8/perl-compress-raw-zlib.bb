SUMMARY = "generated recipe based on perl-Compress-Raw-Zlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native zlib"
RPM_SONAME_REQ_perl-Compress-Raw-Zlib = "libc.so.6 libperl.so.5.26 libpthread.so.0 libz.so.1"
RDEPENDS_perl-Compress-Raw-Zlib = "glibc perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Compress-Raw-Zlib-2.081-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Compress-Raw-Zlib.sha256sum] = "edfd3596fee4b9e7b3c14165c62f5f07318df79683c17d2904645082c64411c9"
