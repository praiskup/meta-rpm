SUMMARY = "generated recipe based on hunspell-tet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tet = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-tet-0.20050108-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-tet.sha256sum] = "e6ee80e4efe220ae7ab8e3e48f0f816279ff1e6107a2405a4d4ccf3c0ad0004b"
