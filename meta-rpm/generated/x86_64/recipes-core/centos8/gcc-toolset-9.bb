SUMMARY = "generated recipe based on gcc-toolset-9 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_gcc-toolset-9 = "gcc-toolset-9-perftools gcc-toolset-9-runtime gcc-toolset-9-toolchain"
RDEPENDS_gcc-toolset-9-build = "gcc-toolset-9-runtime scl-utils-build"
RDEPENDS_gcc-toolset-9-perftools = "gcc-toolset-9-dyninst gcc-toolset-9-runtime gcc-toolset-9-systemtap gcc-toolset-9-valgrind"
RDEPENDS_gcc-toolset-9-runtime = "bash policycoreutils policycoreutils-python-utils scl-utils"
RDEPENDS_gcc-toolset-9-toolchain = "gcc-toolset-9-annobin gcc-toolset-9-binutils gcc-toolset-9-dwz gcc-toolset-9-elfutils gcc-toolset-9-gcc gcc-toolset-9-gcc-c++ gcc-toolset-9-gcc-gfortran gcc-toolset-9-gdb gcc-toolset-9-ltrace gcc-toolset-9-make gcc-toolset-9-runtime gcc-toolset-9-strace"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-build-9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-perftools-9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-runtime-9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-toolchain-9.0-4.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9.sha256sum] = "2588217c6380ded641cb29e7ec9332066c83516f843c0e0594e1c81334fa1d4d"
SRC_URI[gcc-toolset-9-build.sha256sum] = "514d4808d95422e5afb6f51b503ab9aa72b40c53a92865a1e7b80ef300e86845"
SRC_URI[gcc-toolset-9-perftools.sha256sum] = "099d2abfb1ceedaf3a02f668eb659b2321f67db105fe04efb3331e01cbc1ba57"
SRC_URI[gcc-toolset-9-runtime.sha256sum] = "486c94b9e51176f904df83a8673a0a561c2f7df0a8656128355609f065f93dae"
SRC_URI[gcc-toolset-9-toolchain.sha256sum] = "d06d394f008b951b1bedd61f012c32284e850b17e4c9972ed752f3165e20e3db"
