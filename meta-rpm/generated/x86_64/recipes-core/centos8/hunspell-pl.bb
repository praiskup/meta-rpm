SUMMARY = "generated recipe based on hunspell-pl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-pl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-pl-0.20180707-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-pl.sha256sum] = "fec89c9d8e0285969eed1da6eeee3a276d31043065bebcdad550b89eb90334cb"
