SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_selinux-policy-mls = "bash coreutils mcstrans policycoreutils policycoreutils-newrole selinux-policy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/selinux-policy-mls-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy-mls.sha256sum] = "1ab26980eae3621622d6d500120f04b548d9f451c7b5cc1eadf0e6a56b4be867"
