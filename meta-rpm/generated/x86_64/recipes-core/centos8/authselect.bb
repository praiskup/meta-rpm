SUMMARY = "generated recipe based on authselect srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libselinux pkgconfig-native popt"
RPM_SONAME_REQ_authselect = "libauthselect.so.1 libc.so.6 libpopt.so.0 libselinux.so.1"
RDEPENDS_authselect = "authselect-libs glibc libselinux popt"
RDEPENDS_authselect-compat = "authselect bash platform-python sed"
RPM_SONAME_PROV_authselect-libs = "libauthselect.so.1"
RPM_SONAME_REQ_authselect-libs = "libc.so.6 libselinux.so.1"
RDEPENDS_authselect-libs = "bash coreutils findutils gawk glibc grep libselinux sed systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/authselect-compat-1.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/authselect-1.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/authselect-libs-1.1-2.el8.x86_64.rpm \
          "

SRC_URI[authselect.sha256sum] = "9b63ce30958de2f979fb9ece78e8ed7db91c828140521f76fcdad0c2a0053a1c"
SRC_URI[authselect-compat.sha256sum] = "e206ccb784fd55ad04292cb9a66e8dc2c4cf200508f1bb4e697e5dc36432bbe9"
SRC_URI[authselect-libs.sha256sum] = "e43e1cab55e35ec4f6546347b046d2f07802ddd80c8aafce4368dd193f9c2c90"
