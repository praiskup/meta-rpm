SUMMARY = "generated recipe based on xalan-j2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xalan-j2 = "bash java-1.8.0-openjdk-headless javapackages-filesystem xerces-j2 xml-commons-apis"
RDEPENDS_xalan-j2-demo = "glassfish-servlet-api xalan-j2"
RDEPENDS_xalan-j2-javadoc = "javapackages-filesystem"
RDEPENDS_xalan-j2-xsltc = "bcel java-1.8.0-openjdk-headless java_cup javapackages-filesystem regexp xalan-j2 xerces-j2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xalan-j2-2.7.1-38.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xalan-j2-demo-2.7.1-38.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xalan-j2-javadoc-2.7.1-38.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xalan-j2-manual-2.7.1-38.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xalan-j2-xsltc-2.7.1-38.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xalan-j2.sha256sum] = "1376fa8385fa8c831772a24bd8c296b4d64ed874734e3182f50658f45348ce9f"
SRC_URI[xalan-j2-demo.sha256sum] = "2da01edf2f15aeb867c8d171947bea054e3b4705ecc904750801a12e2910bba5"
SRC_URI[xalan-j2-javadoc.sha256sum] = "7e2b76050f2f000ba6d489e4c22ce2862f3c666c267a8e4b4d9d0f402726572d"
SRC_URI[xalan-j2-manual.sha256sum] = "406dc8b05286ee2d29f8868c62a119ff4425b9322db57f1d2bdccfbf0aafb1ad"
SRC_URI[xalan-j2-xsltc.sha256sum] = "92b7ac4c57ffcd027ee2e1cda04e45c6f0b4efd178823107f00df2e0480e1258"
