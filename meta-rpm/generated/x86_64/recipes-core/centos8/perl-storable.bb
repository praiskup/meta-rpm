SUMMARY = "generated recipe based on perl-Storable srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Storable = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Storable = "glibc perl-Carp perl-Exporter perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Storable-3.11-3.el8.x86_64.rpm \
          "

SRC_URI[perl-Storable.sha256sum] = "0c3007b68a37325866aaade4ae076232bca15e268f66c3d3b3a6d236bb85e1e9"
