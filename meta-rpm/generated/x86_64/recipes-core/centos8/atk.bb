SUMMARY = "generated recipe based on atk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_atk = "libatk-1.0.so.0"
RPM_SONAME_REQ_atk = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_atk = "glib2 glibc"
RPM_SONAME_REQ_atk-devel = "libatk-1.0.so.0"
RPROVIDES_atk-devel = "atk-dev (= 2.28.1)"
RDEPENDS_atk-devel = "atk glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/atk-2.28.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/atk-devel-2.28.1-1.el8.x86_64.rpm \
          "

SRC_URI[atk.sha256sum] = "e2e78b8bfed233002b701921ed6fb395cd51b625ee1460ee4bd7ab7b16c0e70a"
SRC_URI[atk-devel.sha256sum] = "dc044103818b92ef08d17322a956bf736d13787e7206ab49fa40f4dfb4ea0114"
