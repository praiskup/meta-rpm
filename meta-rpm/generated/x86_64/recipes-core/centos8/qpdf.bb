SUMMARY = "generated recipe based on qpdf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcc libjpeg-turbo pkgconfig-native zlib"
RPM_SONAME_REQ_qpdf = "libc.so.6 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libqpdf.so.18 libstdc++.so.6 libz.so.1"
RDEPENDS_qpdf = "glibc gnutls libgcc libjpeg-turbo libstdc++ perl-interpreter perl-libs qpdf-libs zlib"
RDEPENDS_qpdf-doc = "qpdf-libs"
RPM_SONAME_PROV_qpdf-libs = "libqpdf.so.18"
RPM_SONAME_REQ_qpdf-libs = "libc.so.6 libgcc_s.so.1 libgnutls.so.30 libjpeg.so.62 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_qpdf-libs = "glibc gnutls libgcc libjpeg-turbo libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qpdf-7.1.1-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qpdf-doc-7.1.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qpdf-libs-7.1.1-10.el8.x86_64.rpm \
          "

SRC_URI[qpdf.sha256sum] = "f74ee4c4d6baac4febd5803a0953162e1d4e22f2ebcc8caffa2dafc621d33a2e"
SRC_URI[qpdf-doc.sha256sum] = "521fabf0e78fb33b4d1a482fc70eac7af2e506c161813324cc2b0ab513623c26"
SRC_URI[qpdf-libs.sha256sum] = "f5fbce8685557e956e2b8a3d1a3e4a6f8ca154d77d3901dcc18b0f3fcaae60f5"
