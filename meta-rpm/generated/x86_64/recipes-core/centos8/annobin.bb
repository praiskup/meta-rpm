SUMMARY = "generated recipe based on annobin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libgcc pkgconfig-native rpm"
RPM_SONAME_REQ_annobin = "libc.so.6 libdw.so.1 libelf.so.1 libgcc_s.so.1 libm.so.6 librpm.so.8 librpmio.so.8 libstdc++.so.6"
RDEPENDS_annobin = "bash elfutils-libelf elfutils-libs glibc libgcc libstdc++ rpm-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/annobin-8.90-1.el8.0.1.x86_64.rpm \
          "

SRC_URI[annobin.sha256sum] = "2245767afcec66af954169b2cc05aedcf347f9980469bf892fceea64d9e2cb55"
