SUMMARY = "generated recipe based on icu srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_icu = "libc.so.6 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuio.so.60 libicutu.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_icu = "glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_libicu = "libicudata.so.60 libicui18n.so.60 libicuio.so.60 libicutest.so.60 libicutu.so.60 libicuuc.so.60"
RPM_SONAME_REQ_libicu = "libc.so.6 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicutu.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libicu = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libicu-devel = "libc.so.6 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuio.so.60 libicutest.so.60 libicutu.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_libicu-devel = "libicu-dev (= 60.3)"
RDEPENDS_libicu-devel = "bash glibc libgcc libicu libstdc++ pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/icu-60.3-2.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libicu-60.3-2.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libicu-devel-60.3-2.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libicu-doc-60.3-2.el8_1.noarch.rpm \
          "

SRC_URI[icu.sha256sum] = "d342e26011497ff8c61bb18f12e0fee3d58d2fd22bb1a82a7ab487681d6190e7"
SRC_URI[libicu.sha256sum] = "d703112d21afadf069e0ba6ef2a34b0ef760ccc969a2b7dd5d38761113c3d17e"
SRC_URI[libicu-devel.sha256sum] = "13593be7f7e7c70c842ba7ed2da9f1460ce1accdbd6d47bb1a617c69b5afe122"
SRC_URI[libicu-doc.sha256sum] = "779b80c0b92442477af96167d035690beed9891eb5061ab4e800d8b81df667d2"
