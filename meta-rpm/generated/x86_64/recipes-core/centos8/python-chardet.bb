SUMMARY = "generated recipe based on python-chardet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-chardet = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-chardet-3.0.4-7.el8.noarch.rpm \
          "

SRC_URI[python3-chardet.sha256sum] = "176ffcb2cf0fdcbdd2d8119cbd98eef60a30fdb0fb065a9382d2c95e90c79652"
