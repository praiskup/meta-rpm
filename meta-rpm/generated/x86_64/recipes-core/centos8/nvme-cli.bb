SUMMARY = "generated recipe based on nvme-cli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libuuid pkgconfig-native"
RPM_SONAME_REQ_nvme-cli = "libc.so.6 libuuid.so.1"
RDEPENDS_nvme-cli = "bash glibc libuuid"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nvme-cli-1.9-7.el8_2.x86_64.rpm \
          "

SRC_URI[nvme-cli.sha256sum] = "4393a9cb3bb6a8148990f2ac52628f09f4974ac938659145945d19a60e17f129"
