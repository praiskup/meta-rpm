SUMMARY = "generated recipe based on apache-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-parent = "apache-resource-bundles java-1.8.0-openjdk-headless javapackages-filesystem maven-enforcer-plugin maven-remote-resources-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-parent-19-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-parent.sha256sum] = "8a67059099425ba64b4cd127c8479b6c4f5dc16cf6195a017c85d727b4c6fe8d"
