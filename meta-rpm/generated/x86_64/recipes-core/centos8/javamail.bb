SUMMARY = "generated recipe based on javamail srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_javamail = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_javamail-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/javamail-1.5.2-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/javamail-javadoc-1.5.2-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[javamail.sha256sum] = "396997c8f4576a89a1d8aedad180c763e2e1384ae265102407f247912716528f"
SRC_URI[javamail-javadoc.sha256sum] = "2deddce5ac45746996145b7de8226071b9bcce18ed25c4a6c5f0067ffc7feb50"
