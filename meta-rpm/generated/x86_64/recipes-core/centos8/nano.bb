SUMMARY = "generated recipe based on nano srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "file ncurses pkgconfig-native"
RPM_SONAME_REQ_nano = "libc.so.6 libmagic.so.1 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_nano = "bash file-libs glibc info ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nano-2.9.8-1.el8.x86_64.rpm \
          "

SRC_URI[nano.sha256sum] = "e8e445c2120e6921228416cd4d0f341a7032cfb9a7bbccda1084a6d47a0f3ced"
