SUMMARY = "generated recipe based on python-netaddr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-netaddr = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-netaddr-0.7.19-8.el8.noarch.rpm \
          "

SRC_URI[python3-netaddr.sha256sum] = "9c674d1ea7ce6af341bbd6122a09b0bd209a79b102416e31ae3dd44320cd21eb"
