SUMMARY = "generated recipe based on sblim-wbemcli srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc pkgconfig-native"
RPM_SONAME_REQ_sblim-wbemcli = "libc.so.6 libcurl.so.4 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_sblim-wbemcli = "curl glibc libcurl libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sblim-wbemcli-1.6.3-15.el8.x86_64.rpm \
          "

SRC_URI[sblim-wbemcli.sha256sum] = "04c0504bf125656714130886d62750baae64ee99a7565c6e8092b45d736bc662"
