SUMMARY = "generated recipe based on sendmail srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib db libnsl2 libxcrypt openldap openssl pkgconfig-native"
RPM_SONAME_REQ_sendmail = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdb-5.3.so liblber-2.4.so.2 libldap-2.4.so.2 libnsl.so.2 libresolv.so.2 libsasl2.so.3 libssl.so.1.1"
RDEPENDS_sendmail = "bash chkconfig coreutils cyrus-sasl cyrus-sasl-lib glibc libdb libnsl2 libxcrypt openldap openssl openssl-libs procmail setup shadow-utils systemd"
RDEPENDS_sendmail-cf = "bash m4 sendmail"
RDEPENDS_sendmail-doc = "sendmail"
RPM_SONAME_PROV_sendmail-milter = "libmilter.so.1.0"
RPM_SONAME_REQ_sendmail-milter = "libc.so.6 libpthread.so.0"
RDEPENDS_sendmail-milter = "glibc"
RPM_SONAME_REQ_sendmail-milter-devel = "libmilter.so.1.0"
RPROVIDES_sendmail-milter-devel = "sendmail-milter-dev (= 8.15.2)"
RDEPENDS_sendmail-milter-devel = "sendmail-milter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sendmail-8.15.2-32.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sendmail-cf-8.15.2-32.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sendmail-doc-8.15.2-32.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sendmail-milter-8.15.2-32.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sendmail-milter-devel-8.15.2-32.el8.x86_64.rpm \
          "

SRC_URI[sendmail.sha256sum] = "e1c6e6b1fcd09e39a6b701d320a0460f61906326fd1df59947fd7987ec5717b2"
SRC_URI[sendmail-cf.sha256sum] = "c9e1de4de52b731052c58ad3e53f22f5e7b2cb73d57bb75cb00922413466b008"
SRC_URI[sendmail-doc.sha256sum] = "c2ffadb95511127412ef9ed0754daa07a1eca6775a66fbbd39a02e09d08d3110"
SRC_URI[sendmail-milter.sha256sum] = "7d051f14ab8a2faac27b3edb9149cc279dcc855e608519748abf4f342a9ab8f8"
SRC_URI[sendmail-milter-devel.sha256sum] = "1869865186d4c1e1ec9e8ecb70110244c0883eb5548250f51779db2fb97a60cb"
