SUMMARY = "generated recipe based on libXScrnSaver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXScrnSaver = "libXss.so.1"
RPM_SONAME_REQ_libXScrnSaver = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXScrnSaver = "glibc libX11 libXext"
RPM_SONAME_REQ_libXScrnSaver-devel = "libXss.so.1"
RPROVIDES_libXScrnSaver-devel = "libXScrnSaver-dev (= 1.2.3)"
RDEPENDS_libXScrnSaver-devel = "libX11-devel libXScrnSaver libXext-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXScrnSaver-1.2.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXScrnSaver-devel-1.2.3-1.el8.x86_64.rpm \
          "

SRC_URI[libXScrnSaver.sha256sum] = "eec507c1988d92cb8ad406040fd2c5105a115b049ef098626cb1c6ffe0714893"
SRC_URI[libXScrnSaver-devel.sha256sum] = "40cabc44bd119e4d84eaefef9fe11bf0bcd2a993d5e05f722033a982fb8fe3f8"
