SUMMARY = "generated recipe based on environment-modules srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_environment-modules = "bash chkconfig less man-db platform-python procps-ng sed tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/environment-modules-4.1.4-4.el8.x86_64.rpm \
          "

SRC_URI[environment-modules.sha256sum] = "f207d009c850202b94cbd2bb9514da4bd56eb7b68763a6ccaed88cb5a0556b18"
