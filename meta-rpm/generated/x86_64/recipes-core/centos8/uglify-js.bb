SUMMARY = "generated recipe based on uglify-js srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_js-uglify = "web-assets-filesystem"
RDEPENDS_uglify-js = "js-uglify"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/js-uglify-2.8.29-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/uglify-js-2.8.29-4.el8.noarch.rpm \
          "

SRC_URI[js-uglify.sha256sum] = "35c2361df58e615be0a99cf85b1ac6400640e03f3637dc2e79c620d55cb4bc57"
SRC_URI[uglify-js.sha256sum] = "386cf37eb2e736b7be5aeea4bb45fafe35f4235392bdfb153c263a19fccf732c"
