SUMMARY = "generated recipe based on pipewire srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib dbus-libs glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base pkgconfig-native sbc systemd-libs"
RPM_SONAME_PROV_pipewire = "libpipewire-module-audio-dsp.so libpipewire-module-autolink.so libpipewire-module-client-node.so libpipewire-module-link-factory.so libpipewire-module-mixer.so libpipewire-module-portal.so libpipewire-module-protocol-native.so libpipewire-module-rtkit.so libpipewire-module-spa-monitor.so libpipewire-module-spa-node-factory.so libpipewire-module-spa-node.so libpipewire-module-suspend-on-idle.so libspa-alsa.so libspa-audiomixer.so libspa-audiotestsrc.so libspa-bluez5.so libspa-dbus.so libspa-support.so libspa-test.so libspa-v4l2.so libspa-videotestsrc.so libspa-volume.so"
RPM_SONAME_REQ_pipewire = "libasound.so.2 libc.so.6 libdbus-1.so.3 libdl.so.2 libm.so.6 libpipewire-0.2.so.1 libpthread.so.0 libsbc.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_pipewire = "alsa-lib bash dbus-libs glibc pipewire-libs rtkit sbc shadow-utils systemd systemd-libs"
RPM_SONAME_REQ_pipewire-devel = "libpipewire-0.2.so.1"
RPROVIDES_pipewire-devel = "pipewire-dev (= 0.2.7)"
RDEPENDS_pipewire-devel = "pipewire-libs pkgconf-pkg-config"
RPM_SONAME_PROV_pipewire-libs = "libgstpipewire.so libpipewire-0.2.so.1"
RPM_SONAME_REQ_pipewire-libs = "libc.so.6 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libgstallocators-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libpipewire-0.2.so.1 libpthread.so.0"
RDEPENDS_pipewire-libs = "glib2 glibc gstreamer1 gstreamer1-plugins-base"
RPM_SONAME_REQ_pipewire-utils = "libc.so.6 libdl.so.2 libpipewire-0.2.so.1 libpthread.so.0"
RDEPENDS_pipewire-utils = "glibc pipewire pipewire-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pipewire-0.2.7-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pipewire-devel-0.2.7-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pipewire-doc-0.2.7-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pipewire-libs-0.2.7-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pipewire-utils-0.2.7-1.el8.x86_64.rpm \
          "

SRC_URI[pipewire.sha256sum] = "12fafb0e8cc5cd6cc5c49f5ea15d98f6ec865e2f414ecef897dfd226f63711b5"
SRC_URI[pipewire-devel.sha256sum] = "7f7430f35462515f71e9ff7404a0d1392759e177c4f59e906ca2f860654cbc46"
SRC_URI[pipewire-doc.sha256sum] = "c3d412db33d111c75c3114b6e4c73aa0e287243af8a13f7e414634cc1a8e0bc7"
SRC_URI[pipewire-libs.sha256sum] = "e5fcc65fe8818a569c89820005f30252a11b9bb769e749ff5709b445640d0932"
SRC_URI[pipewire-utils.sha256sum] = "96c9fec67bd38e9e02f898f1aa68083ff43c420595c3a4b0a47a2be66f559c32"
