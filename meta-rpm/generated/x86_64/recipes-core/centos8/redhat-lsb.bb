SUMMARY = "generated recipe based on redhat-lsb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libpng12 pkgconfig-native"
RPROVIDES_redhat-lsb = "lsb-release (= 4.1)"
RDEPENDS_redhat-lsb = "bash redhat-lsb-core redhat-lsb-cxx redhat-lsb-desktop redhat-lsb-languages redhat-lsb-printing"
RDEPENDS_redhat-lsb-core = "at bash bc binutils coreutils cpio cronie cups-client diffutils ed file findutils gawk gettext glibc glibc-common grep gzip hostname libgcc m4 mailx make man-db ncurses-compat-libs pam passwd patch postfix procps-ng psmisc redhat-lsb-submod-security sed shadow-utils spax systemd tar time util-linux util-linux-user zlib"
RDEPENDS_redhat-lsb-cxx = "libstdc++ redhat-lsb-core"
RPM_SONAME_REQ_redhat-lsb-desktop = "libpng12.so.0"
RDEPENDS_redhat-lsb-desktop = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 gtk2 libICE libSM libX11 libXext libXft libXi libXrender libXt libXtst libjpeg-turbo libpng libpng12 libxml2 mesa-libGL mesa-libGLU pango redhat-lsb-core redhat-lsb-submod-multimedia xdg-utils"
RDEPENDS_redhat-lsb-languages = "perl-B-Lint perl-CGI perl-CPAN perl-Class-ISA perl-Env perl-ExtUtils-MakeMaker perl-File-CheckTree perl-Getopt-Long perl-Locale-Codes perl-Locale-Maketext perl-PathTools perl-Pod-Checker perl-Pod-LaTeX perl-Pod-Plainer perl-Scalar-List-Utils perl-Sys-Syslog perl-Test-Harness perl-Test-Simple perl-Text-Soundex perl-Time-HiRes perl-XML-LibXML perl-autodie perl-interpreter python36 redhat-lsb-core"
RDEPENDS_redhat-lsb-printing = "cups-filters cups-libs ghostscript redhat-lsb-core"
RDEPENDS_redhat-lsb-submod-multimedia = "alsa-lib"
RDEPENDS_redhat-lsb-submod-security = "nspr nss"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-core-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-cxx-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-desktop-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-languages-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-printing-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-submod-multimedia-4.1-47.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-lsb-submod-security-4.1-47.el8.x86_64.rpm \
          "

SRC_URI[redhat-lsb.sha256sum] = "b80664d8a22197e3642797df6d4f9b0f4c30cef1b011b33329edc179d77a6563"
SRC_URI[redhat-lsb-core.sha256sum] = "4f1060968bfcb1aec9aaa1a4c73c511741b0b3975e6797c046d43e9e991d2f97"
SRC_URI[redhat-lsb-cxx.sha256sum] = "3f1d5f0ec6ecc71b563209bc3c066624d3aaadc586b3ea26cbbfb99a59ffca35"
SRC_URI[redhat-lsb-desktop.sha256sum] = "89ccdb6ce956cdd68d27f29e3820508f8afaf67284b6fe9ef6668ed0e680f50a"
SRC_URI[redhat-lsb-languages.sha256sum] = "1aeea6bfefa1e17245d5c36930829854450915addedbff9ebfbb4f0b1810fac0"
SRC_URI[redhat-lsb-printing.sha256sum] = "955fd3f41922f498ed0ae8be7742b4d5c796b1aa84f50063f98b0de82ef491cb"
SRC_URI[redhat-lsb-submod-multimedia.sha256sum] = "ae610b158c352ffbefb7e58b83f53ca13e66166c1500508d90c46f2b586f319e"
SRC_URI[redhat-lsb-submod-security.sha256sum] = "27519e6c44b36a4ec30c603f7ec96623ccc21f9de9eef8f9efd0d2f3f7d2a749"
