SUMMARY = "generated recipe based on keybinder3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libx11 libxext libxrender pango pkgconfig-native"
RPM_SONAME_PROV_keybinder3 = "libkeybinder-3.0.so.0"
RPM_SONAME_REQ_keybinder3 = "libX11.so.6 libXext.so.6 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_keybinder3 = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libXext libXrender pango"
RPM_SONAME_REQ_keybinder3-devel = "libkeybinder-3.0.so.0"
RPROVIDES_keybinder3-devel = "keybinder3-dev (= 0.3.2)"
RDEPENDS_keybinder3-devel = "gtk3-devel keybinder3 pkgconf-pkg-config"
RDEPENDS_keybinder3-doc = "devhelp keybinder3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/keybinder3-0.3.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/keybinder3-devel-0.3.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/keybinder3-doc-0.3.2-4.el8.noarch.rpm \
          "

SRC_URI[keybinder3.sha256sum] = "be4573d49c87a35c8b07bcbdffe705f0c19ce62d4d38348189b9233a4861b4ee"
SRC_URI[keybinder3-devel.sha256sum] = "f9cfc40f3d422951bf1adba85bba72f12e2a583ddc09d285bc9ec92e6f53236d"
SRC_URI[keybinder3-doc.sha256sum] = "e629f1b270ff50bab3b530efffdea0f793561e6d680f2cb1dfcd40f619757f87"
