SUMMARY = "generated recipe based on exiv2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_exiv2 = "libc.so.6 libexiv2.so.27 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_exiv2 = "exiv2-libs glibc libgcc libstdc++"
RPM_SONAME_REQ_exiv2-devel = "libexiv2.so.27"
RPROVIDES_exiv2-devel = "exiv2-dev (= 0.27.2)"
RDEPENDS_exiv2-devel = "cmake-filesystem exiv2-libs pkgconf-pkg-config"
RPM_SONAME_PROV_exiv2-libs = "libexiv2.so.27"
RPM_SONAME_REQ_exiv2-libs = "libc.so.6 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_exiv2-libs = "expat glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/exiv2-0.27.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/exiv2-libs-0.27.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/exiv2-devel-0.27.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/exiv2-doc-0.27.2-5.el8.noarch.rpm \
          "

SRC_URI[exiv2.sha256sum] = "cfee16dd647d0101b1f5d32df0fbe8b692561fa6b357aa74643896561dc3dc59"
SRC_URI[exiv2-devel.sha256sum] = "bb4dcf79a4af0a81773b81a282bff98a94b378b5750e0b9038d50f837a5ca1e5"
SRC_URI[exiv2-doc.sha256sum] = "7f2bb71efcca211e098c92d56d16b9ae439eb1d883bd433eef085bfe521ecf20"
SRC_URI[exiv2-libs.sha256sum] = "778e9a280ac710183e362f11d913f172a0da353e71b02765c489476cb2768de4"
