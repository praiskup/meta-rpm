SUMMARY = "generated recipe based on gzip srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_gzip = "libc.so.6"
RDEPENDS_gzip = "bash coreutils glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gzip-1.9-9.el8.x86_64.rpm \
          "

SRC_URI[gzip.sha256sum] = "6697e1ec0e3b6eedb1f7c1c25e1e4505926e1de929bb4235304f4a6f9faa5bcb"
