SUMMARY = "generated recipe based on brasero srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 libburn libcanberra libice libisofs libnotify libsm libx11 libxml2 nautilus pango pkgconfig-native totem-pl-parser tracker"
RPM_SONAME_PROV_brasero = "libbrasero-audio2cue.so libbrasero-burn-uri.so libbrasero-cdda2wav.so libbrasero-cdrdao.so libbrasero-cdrecord.so libbrasero-checksum-file.so libbrasero-checksum.so libbrasero-dvdauthor.so libbrasero-dvdcss.so libbrasero-dvdrwformat.so libbrasero-genisoimage.so libbrasero-growisofs.so libbrasero-libburn.so libbrasero-libisofs.so libbrasero-local-track.so libbrasero-mkisofs.so libbrasero-normalize.so libbrasero-readcd.so libbrasero-readom.so libbrasero-transcode.so libbrasero-vcdimager.so libbrasero-vob.so libbrasero-wodim.so"
RPM_SONAME_REQ_brasero = "libICE.so.6 libSM.so.6 libX11.so.6 libatk-1.0.so.0 libbrasero-burn3.so.1 libbrasero-media3.so.1 libbrasero-utils3.so.1 libburn.so.4 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libisofs.so.6 libm.so.6 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libtotem-plparser.so.18 libtracker-sparql-2.0.so.0 libxml2.so.2"
RDEPENDS_brasero = "atk brasero-libs cairo cairo-gobject cdrdao dvd+rw-tools gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 icedax libICE libSM libX11 libburn libcanberra libcanberra-gtk3 libisofs libnotify libxml2 pango totem-pl-parser tracker wodim xorriso"
RPM_SONAME_REQ_brasero-devel = "libbrasero-burn3.so.1 libbrasero-media3.so.1 libbrasero-utils3.so.1"
RPROVIDES_brasero-devel = "brasero-dev (= 3.12.2)"
RDEPENDS_brasero-devel = "brasero-libs glib2-devel gtk3-devel pkgconf-pkg-config"
RPM_SONAME_PROV_brasero-libs = "libbrasero-burn3.so.1 libbrasero-media3.so.1 libbrasero-utils3.so.1"
RPM_SONAME_REQ_brasero-libs = "libbrasero-media3.so.1 libbrasero-utils3.so.1 libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libtotem-plparser.so.18"
RDEPENDS_brasero-libs = "cairo gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 libcanberra-gtk3 libnotify pango totem-pl-parser"
RPM_SONAME_PROV_brasero-nautilus = "libnautilus-brasero-extension.so"
RPM_SONAME_REQ_brasero-nautilus = "libbrasero-burn3.so.1 libbrasero-media3.so.1 libbrasero-utils3.so.1 libc.so.6 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnautilus-extension.so.1 libpthread.so.0"
RDEPENDS_brasero-nautilus = "brasero brasero-libs glib2 glibc gtk3 nautilus-extensions"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brasero-3.12.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brasero-libs-3.12.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brasero-nautilus-3.12.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/brasero-devel-3.12.2-4.el8.x86_64.rpm \
          "

SRC_URI[brasero.sha256sum] = "ac0f30c40759904451299d50f84a3d3375c2eab210d176ee994d6e51ab915836"
SRC_URI[brasero-devel.sha256sum] = "5285fe81faf3e9045108498a6915a76c684451de7f9789d977b62821ad5f5033"
SRC_URI[brasero-libs.sha256sum] = "6bf48633d7a29133d4b14b20d18c54f4c1c6cf8afe9866e050aee0e3c8b825af"
SRC_URI[brasero-nautilus.sha256sum] = "4201ed36a054007307258aafd6c1e0abbd33fe29a24f65f0b40a84cd03f339b3"
