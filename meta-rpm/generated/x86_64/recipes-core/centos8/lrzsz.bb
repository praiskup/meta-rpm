SUMMARY = "generated recipe based on lrzsz srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lrzsz = "libc.so.6"
RDEPENDS_lrzsz = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lrzsz-0.12.20-43.el8.x86_64.rpm \
          "

SRC_URI[lrzsz.sha256sum] = "69056adbd7f4cf2d188d2fd7f4108e662b9e51200db9e411ecae7caac51bfaa5"
