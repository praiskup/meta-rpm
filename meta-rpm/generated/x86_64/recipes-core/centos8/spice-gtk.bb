SUMMARY = "generated recipe based on spice-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl atk cairo celt051 cyrus-sasl-lib gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 json-glib libcacard libepoxy libjpeg-turbo libusb1 libx11 lz4 openssl opus pango pixman pkgconfig-native polkit pulseaudio spice-protocol usbredir zlib"
RPM_SONAME_PROV_spice-glib = "libspice-client-glib-2.0.so.8"
RPM_SONAME_REQ_spice-glib = "libacl.so.1 libc.so.6 libcacard.so.0 libcelt051.so.0 libcrypto.so.1.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 liblz4.so.1 libm.so.6 libopus.so.0 libpixman-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsasl2.so.3 libssl.so.1.1 libusb-1.0.so.0 libusbredirhost.so.1 libusbredirparser.so.1 libz.so.1"
RDEPENDS_spice-glib = "celt051 cyrus-sasl-lib glib2 glibc gstreamer1 gstreamer1-plugins-base json-glib libacl libcacard libjpeg-turbo libusbx lz4-libs openssl-libs opus pixman polkit-libs pulseaudio-libs pulseaudio-libs-glib2 usbredir zlib"
RPM_SONAME_REQ_spice-glib-devel = "libspice-client-glib-2.0.so.8"
RPROVIDES_spice-glib-devel = "spice-glib-dev (= 0.37)"
RDEPENDS_spice-glib-devel = "glib2-devel openssl-devel pixman-devel pkgconf-pkg-config spice-glib spice-protocol"
RDEPENDS_spice-gtk = "spice-glib"
RPM_SONAME_REQ_spice-gtk-tools = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcacard.so.0 libcairo-gobject.so.2 libcairo.so.2 libcelt051.so.0 libcrypto.so.1.1 libepoxy.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 liblz4.so.1 libm.so.6 libopus.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpixman-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsasl2.so.3 libspice-client-glib-2.0.so.8 libspice-client-gtk-3.0.so.5 libssl.so.1.1 libusb-1.0.so.0 libusbredirhost.so.1 libusbredirparser.so.1 libz.so.1"
RDEPENDS_spice-gtk-tools = "atk cairo cairo-gobject celt051 cyrus-sasl-lib gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 json-glib libX11 libcacard libepoxy libjpeg-turbo libusbx lz4-libs openssl-libs opus pango pixman pulseaudio-libs pulseaudio-libs-glib2 spice-glib spice-gtk3 usbredir zlib"
RPM_SONAME_PROV_spice-gtk3 = "libspice-client-gtk-3.0.so.5"
RPM_SONAME_REQ_spice-gtk3 = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcacard.so.0 libcairo-gobject.so.2 libcairo.so.2 libcelt051.so.0 libcrypto.so.1.1 libepoxy.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 liblz4.so.1 libm.so.6 libopus.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpixman-1.so.0 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libsasl2.so.3 libspice-client-glib-2.0.so.8 libssl.so.1.1 libusb-1.0.so.0 libusbredirhost.so.1 libusbredirparser.so.1 libz.so.1"
RDEPENDS_spice-gtk3 = "atk cairo cairo-gobject celt051 cyrus-sasl-lib gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 json-glib libX11 libcacard libepoxy libjpeg-turbo libusbx lz4-libs openssl-libs opus pango pixman pulseaudio-libs pulseaudio-libs-glib2 spice-glib usbredir zlib"
RPM_SONAME_REQ_spice-gtk3-devel = "libspice-client-gtk-3.0.so.5"
RPROVIDES_spice-gtk3-devel = "spice-gtk3-dev (= 0.37)"
RDEPENDS_spice-gtk3-devel = "gtk3-devel pkgconf-pkg-config spice-glib-devel spice-gtk3"
RDEPENDS_spice-gtk3-vala = "spice-gtk3 spice-gtk3-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-glib-0.37-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-glib-devel-0.37-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-gtk-0.37-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-gtk-tools-0.37-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-gtk3-0.37-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-gtk3-devel-0.37-1.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-gtk3-vala-0.37-1.el8_2.2.x86_64.rpm \
          "

SRC_URI[spice-glib.sha256sum] = "417213c34e4ad4fb431db4f99b9c5bb5a97be7a808fd81286373ae9b00ea0dfb"
SRC_URI[spice-glib-devel.sha256sum] = "a0835905cbc54dffd2053f9ee6b8f3955c64d46cbae95c7ecd9cbff72dc13869"
SRC_URI[spice-gtk.sha256sum] = "ecf36e7807b40a8027555a2f7cef931198df72338e0a26179fb62ead6461105b"
SRC_URI[spice-gtk-tools.sha256sum] = "3acf1eb40bda5a9de7fe3fbd21cc19dae76a1df9a16a73ac691cb2b98ffbcc35"
SRC_URI[spice-gtk3.sha256sum] = "5647a98c27f5a3ed977a330e1448b6e7c7fbf4caa95089443ce8b779ae5b448e"
SRC_URI[spice-gtk3-devel.sha256sum] = "b9931e3131083a10259ea669ed8169fa066b8e81699fc78131cbadca6dc78f0f"
SRC_URI[spice-gtk3-vala.sha256sum] = "3df78a7e228de5e4aec4f5cbb3c63579fc1da9bf41b7fee0b6269146a35091fd"
