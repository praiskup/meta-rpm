SUMMARY = "generated recipe based on iproute srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "db elfutils iptables libmnl libselinux pkgconfig-native"
RPM_SONAME_REQ_iproute = "libc.so.6 libdb-5.3.so libdl.so.2 libelf.so.1 libm.so.6 libmnl.so.0 libselinux.so.1"
RDEPENDS_iproute = "bash elfutils-libelf glibc libdb libmnl libselinux"
RPROVIDES_iproute-devel = "iproute-dev (= 5.3.0)"
RDEPENDS_iproute-devel = "iproute"
RPM_SONAME_REQ_iproute-tc = "libc.so.6 libdl.so.2 libelf.so.1 libm.so.6 libmnl.so.0 libselinux.so.1 libxtables.so.12"
RDEPENDS_iproute-tc = "bash elfutils-libelf glibc iproute iptables-libs libmnl libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iproute-5.3.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iproute-tc-5.3.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/iproute-devel-5.3.0-1.el8.x86_64.rpm \
          "

SRC_URI[iproute.sha256sum] = "ebf7ae767c759c0c3e4bc7eae40d95320ee469645aae2d384a4119bce162a7cb"
SRC_URI[iproute-devel.sha256sum] = "921d0869304c20fa2433e6a5b9e3d4786630a9ea7638e46e1cbd3d92eba4afa7"
SRC_URI[iproute-tc.sha256sum] = "564eeff4d7446636e422a19d32dfc734245b40e09bd0519cec7b9bcc94b795e1"
