SUMMARY = "generated recipe based on easymock srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_easymock = "cglib java-1.8.0-openjdk-headless javapackages-filesystem objectweb-asm objenesis"
RDEPENDS_easymock-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/easymock-3.5-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/easymock-javadoc-3.5-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[easymock.sha256sum] = "944febdc334bc7ce0235320a00651ec694cdb8639a4d209d12250a91beddfbb3"
SRC_URI[easymock-javadoc.sha256sum] = "a027b3ff0186f71109abb5f1ed0070b7880b35ddf56afcb13db88535d73431f3"
