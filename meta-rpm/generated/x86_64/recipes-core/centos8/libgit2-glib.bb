SUMMARY = "generated recipe based on libgit2-glib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgit2 pkgconfig-native"
RPM_SONAME_PROV_libgit2-glib = "libgit2-glib-1.0.so.0"
RPM_SONAME_REQ_libgit2-glib = "libc.so.6 libgio-2.0.so.0 libgit2.so.26 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libgit2-glib = "glib2 glibc libgit2 platform-python python3-gobject"
RPM_SONAME_REQ_libgit2-glib-devel = "libgit2-glib-1.0.so.0"
RPROVIDES_libgit2-glib-devel = "libgit2-glib-dev (= 0.26.4)"
RDEPENDS_libgit2-glib-devel = "glib2-devel libgit2-devel libgit2-glib pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgit2-glib-0.26.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgit2-glib-devel-0.26.4-2.el8.x86_64.rpm \
          "

SRC_URI[libgit2-glib.sha256sum] = "aa436fecf5b0b1b0c3c5f7834aa481a79310e4047f0677078e5e0f184a69050f"
SRC_URI[libgit2-glib-devel.sha256sum] = "e344e9e52e034d6b31a17fb01ca2a7539b3b101ed5059f24760fcee545a9f95c"
