SUMMARY = "generated recipe based on cyrus-sasl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cyrus-sasl-lib db e2fsprogs krb5-libs libpq libxcrypt mariadb-connector-c openldap openssl pam pkgconfig-native"
RPM_SONAME_REQ_cyrus-sasl = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libpam.so.0 libresolv.so.2 libsasl2.so.3"
RDEPENDS_cyrus-sasl = "bash chkconfig cyrus-sasl-lib glibc krb5-libs libcom_err libdb libxcrypt openldap openssl-libs pam shadow-utils systemd util-linux"
RPM_SONAME_PROV_cyrus-sasl-gs2 = "libgs2.so.3"
RPM_SONAME_REQ_cyrus-sasl-gs2 = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libgs2.so.3 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-gs2 = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt"
RPM_SONAME_PROV_cyrus-sasl-gssapi = "libgssapiv2.so.3"
RPM_SONAME_REQ_cyrus-sasl-gssapi = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libgssapiv2.so.3 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-gssapi = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt"
RPM_SONAME_PROV_cyrus-sasl-ldap = "libldapdb.so.3"
RPM_SONAME_REQ_cyrus-sasl-ldap = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldapdb.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-ldap = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openldap"
RPM_SONAME_PROV_cyrus-sasl-md5 = "libcrammd5.so.3 libdigestmd5.so.3"
RPM_SONAME_REQ_cyrus-sasl-md5 = "libc.so.6 libcom_err.so.2 libcrammd5.so.3 libcrypt.so.1 libcrypto.so.1.1 libdigestmd5.so.3 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-md5 = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openssl-libs"
RPM_SONAME_PROV_cyrus-sasl-ntlm = "libntlm.so.3"
RPM_SONAME_REQ_cyrus-sasl-ntlm = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libntlm.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-ntlm = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openssl-libs"
RPM_SONAME_PROV_cyrus-sasl-plain = "liblogin.so.3 libplain.so.3"
RPM_SONAME_REQ_cyrus-sasl-plain = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 liblogin.so.3 libplain.so.3 libresolv.so.2"
RDEPENDS_cyrus-sasl-plain = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt"
RPM_SONAME_PROV_cyrus-sasl-scram = "libscram.so.3"
RPM_SONAME_REQ_cyrus-sasl-scram = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libscram.so.3"
RDEPENDS_cyrus-sasl-scram = "cyrus-sasl-lib glibc krb5-libs libcom_err libxcrypt openssl-libs"
RPM_SONAME_PROV_cyrus-sasl-sql = "libsql.so.3"
RPM_SONAME_REQ_cyrus-sasl-sql = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libmariadb.so.3 libpq.so.5 libresolv.so.2 libsql.so.3"
RDEPENDS_cyrus-sasl-sql = "cyrus-sasl-lib glibc krb5-libs libcom_err libpq libxcrypt mariadb-connector-c"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cyrus-sasl-sql-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-gs2-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-gssapi-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-ldap-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-md5-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-ntlm-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-plain-2.1.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cyrus-sasl-scram-2.1.27-1.el8.x86_64.rpm \
          "

SRC_URI[cyrus-sasl.sha256sum] = "2322d7d22620aecf7bc4faa961bef5797f8b7a4b17dade523b8100978e5c7afd"
SRC_URI[cyrus-sasl-gs2.sha256sum] = "41b89fd517cb6b0011d37b646eeed273d0fca8c7fc31d36f668380d31d57c2ae"
SRC_URI[cyrus-sasl-gssapi.sha256sum] = "f20d24ba12a929a59d935a3e602751f9e797c7711ba77dbefe95cb85c383f0af"
SRC_URI[cyrus-sasl-ldap.sha256sum] = "6e99076945b7ec333738fc4c55c39f17f16de848cbd395b5b445b258c4a3bce4"
SRC_URI[cyrus-sasl-md5.sha256sum] = "3a016b60c821071f903ebff6dfc33848b8dc3af99a3ced7dc9c76d8058f386af"
SRC_URI[cyrus-sasl-ntlm.sha256sum] = "dff4170744816334f9722b7d34a95b527741e292c0efbbcacb3cdb7d4da56c1d"
SRC_URI[cyrus-sasl-plain.sha256sum] = "b8e3524666326cc63c32e71013575fe7a1b6722aaf5476a397311914c4b7b3ac"
SRC_URI[cyrus-sasl-scram.sha256sum] = "41457df90a024f8344128c174b6343dd452a6da60d20d5ee7810d1d0296813ad"
SRC_URI[cyrus-sasl-sql.sha256sum] = "55a86c8b4619a828360c268eeccd54cf5ed7d4f4cbbda163d1895f02ff848a32"
