SUMMARY = "generated recipe based on mingw-libffi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-libffi = "mingw32-crt mingw32-filesystem mingw32-pkg-config"
RDEPENDS_mingw64-libffi = "mingw64-crt mingw64-filesystem mingw64-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libffi-3.1-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libffi-3.1-4.el8.noarch.rpm \
          "

SRC_URI[mingw32-libffi.sha256sum] = "5e4b919d0859f5f04cea812559e9ed1630940980fcf1691de4cec498c93bac4d"
SRC_URI[mingw64-libffi.sha256sum] = "450fc3f94654e9d23de1dfd25d62eb42cf5aff0b04dd842481d7d205cc97f024"
