SUMMARY = "generated recipe based on pangomm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo cairomm glib-2.0 glibmm24 libgcc libsigc++20 pango pkgconfig-native"
RPM_SONAME_PROV_pangomm = "libpangomm-1.4.so.1"
RPM_SONAME_REQ_pangomm = "libc.so.6 libcairo.so.2 libcairomm-1.0.so.1 libgcc_s.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_pangomm = "cairo cairomm glib2 glibc glibmm24 libgcc libsigc++20 libstdc++ pango"
RPM_SONAME_REQ_pangomm-devel = "libpangomm-1.4.so.1"
RPROVIDES_pangomm-devel = "pangomm-dev (= 2.40.1)"
RDEPENDS_pangomm-devel = "cairomm-devel glibmm24-devel pango-devel pangomm pkgconf-pkg-config"
RDEPENDS_pangomm-doc = "glibmm24-doc libsigc++20-doc pangomm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pangomm-2.40.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pangomm-devel-2.40.1-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pangomm-doc-2.40.1-5.el8.noarch.rpm \
          "

SRC_URI[pangomm.sha256sum] = "e6f7355d8b62b6dd6891ded17ae7e0d1e10cd10b9bec9e974bffe1c20cc3bd94"
SRC_URI[pangomm-devel.sha256sum] = "c78845114c0a48b373eb6bd17f735289573b6519c6a1ffa99e552f35d04628e3"
SRC_URI[pangomm-doc.sha256sum] = "a551143cee5db5a095f7be46f0fff24b594a0d05c09e5ab9560994a1a39dfab7"
