SUMMARY = "generated recipe based on cracklib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_cracklib = "libcrack.so.2"
RPM_SONAME_REQ_cracklib = "libc.so.6 libcrack.so.2 libz.so.1"
RDEPENDS_cracklib = "bash glibc gzip zlib"
RPM_SONAME_REQ_cracklib-devel = "libcrack.so.2"
RPROVIDES_cracklib-devel = "cracklib-dev (= 2.9.6)"
RDEPENDS_cracklib-devel = "cracklib"
RDEPENDS_cracklib-dicts = "cracklib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cracklib-2.9.6-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cracklib-dicts-2.9.6-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cracklib-devel-2.9.6-15.el8.x86_64.rpm \
          "

SRC_URI[cracklib.sha256sum] = "dbbc9e20caabc30070354d91f61f383081f6d658e09d3c09e6df8764559e5aca"
SRC_URI[cracklib-devel.sha256sum] = "0440ad2138cb1945837fdc64f4186b2334de8594bfa9ab64cfc07a7abef02f65"
SRC_URI[cracklib-dicts.sha256sum] = "f1ce23ee43c747a35367dada19ca200a7758c50955ccc44aa946b86b647077ca"
