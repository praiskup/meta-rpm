SUMMARY = "generated recipe based on libvncserver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gnutls libgcrypt libjpeg-turbo libpng lzo pkgconfig-native zlib"
RPM_SONAME_PROV_libvncserver = "libvncclient.so.0 libvncserver.so.0"
RPM_SONAME_REQ_libvncserver = "ld-linux-x86-64.so.2 libc.so.6 libgcrypt.so.20 libgnutls.so.30 libjpeg.so.62 libminilzo.so.0 libpng16.so.16 libpthread.so.0 libresolv.so.2 libz.so.1"
RDEPENDS_libvncserver = "glibc gnutls libgcrypt libjpeg-turbo libpng lzo-minilzo zlib"
RPM_SONAME_REQ_libvncserver-devel = "libvncclient.so.0 libvncserver.so.0"
RPROVIDES_libvncserver-devel = "libvncserver-dev (= 0.9.11)"
RDEPENDS_libvncserver-devel = "bash coreutils libvncserver pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvncserver-0.9.11-15.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvncserver-devel-0.9.11-15.el8_2.1.x86_64.rpm \
          "

SRC_URI[libvncserver.sha256sum] = "d854177fcde3f1517470324fa67578919af187b7c21c61a0de86b67c72b65a52"
SRC_URI[libvncserver-devel.sha256sum] = "2f892d68a83dee852158ae98e892f2e37e3ef64b715f9bd978f811723b136aff"
