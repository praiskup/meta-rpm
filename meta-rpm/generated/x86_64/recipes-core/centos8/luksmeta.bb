SUMMARY = "generated recipe based on luksmeta srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cryptsetup cryptsetup-libs pkgconfig-native"
RPM_SONAME_PROV_libluksmeta = "libluksmeta.so.0"
RPM_SONAME_REQ_libluksmeta = "libc.so.6 libcryptsetup.so.12"
RDEPENDS_libluksmeta = "cryptsetup-libs glibc"
RPM_SONAME_REQ_libluksmeta-devel = "libluksmeta.so.0"
RPROVIDES_libluksmeta-devel = "libluksmeta-dev (= 9)"
RDEPENDS_libluksmeta-devel = "cryptsetup-devel libluksmeta pkgconf-pkg-config"
RPM_SONAME_REQ_luksmeta = "libc.so.6 libcryptsetup.so.12 libluksmeta.so.0"
RDEPENDS_luksmeta = "cryptsetup-libs glibc libluksmeta"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libluksmeta-9-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libluksmeta-devel-9-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/luksmeta-9-4.el8.x86_64.rpm \
          "

SRC_URI[libluksmeta.sha256sum] = "05d9ffd0ca48b308040afca0cad7da8699eded1e9f32f52c8f440806429ca6c9"
SRC_URI[libluksmeta-devel.sha256sum] = "fc1153fa21d36e3b3d87d9bce73d3278d793fbce9f2065c1da9df8c231146b01"
SRC_URI[luksmeta.sha256sum] = "14408a22eca2e145292b51249e74433f0a0b034cedc418ac1091f5942499fee9"
