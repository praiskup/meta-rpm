SUMMARY = "generated recipe based on v4l-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libjpeg-turbo pkgconfig-native"
RPM_SONAME_PROV_libv4l = "libv4l-mplane.so libv4l1.so.0 libv4l2.so.0 libv4l2rds.so.0 libv4lconvert.so.0"
RPM_SONAME_REQ_libv4l = "libc.so.6 libdl.so.2 libjpeg.so.62 libm.so.6 libpthread.so.0 librt.so.1 libv4l1.so.0 libv4l2.so.0 libv4lconvert.so.0"
RDEPENDS_libv4l = "glibc libjpeg-turbo"
RPM_SONAME_REQ_libv4l-devel = "libv4l1.so.0 libv4l2.so.0 libv4l2rds.so.0 libv4lconvert.so.0"
RPROVIDES_libv4l-devel = "libv4l-dev (= 1.14.2)"
RDEPENDS_libv4l-devel = "libv4l pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libv4l-1.14.2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libv4l-devel-1.14.2-3.el8.x86_64.rpm \
          "

SRC_URI[libv4l.sha256sum] = "fcf4d4f6a4c5d8d0ed9b7fb82d441557f9c29274d697962362f3bb74c85b0557"
SRC_URI[libv4l-devel.sha256sum] = "5024d3440455fca2b77501987abe152a1f3d7e09c92f2075696d3d53a75d5dff"
