SUMMARY = "generated recipe based on maven-filtering srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-filtering = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-utils plexus-build-api plexus-interpolation plexus-utils"
RDEPENDS_maven-filtering-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-filtering-3.1.1-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-filtering-javadoc-3.1.1-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-filtering.sha256sum] = "0bbe8fbfd1c1df785cd755708bbcc7729d8dad8949792e4c52d7b705bc3c142a"
SRC_URI[maven-filtering-javadoc.sha256sum] = "eb836a231abedbaa7bbf0ef2e61fb4c1c2ee88b45019c2033cafcf294ee8da5d"
