SUMMARY = "generated recipe based on systemd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libcap libgcc libgcrypt libmount lz4 pkgconfig-native xz"
RPM_SONAME_PROV_systemd-libs = "libnss_myhostname.so.2 libnss_resolve.so.2 libnss_systemd.so.2 libsystemd.so.0 libudev.so.1"
RPM_SONAME_REQ_systemd-libs = "ld-linux-x86-64.so.2 libc.so.6 libcap.so.2 libgcc_s.so.1 libgcrypt.so.20 liblz4.so.1 liblzma.so.5 libmount.so.1 libpthread.so.0 librt.so.1"
RPROVIDES_systemd-libs = "libudev (= 239)"
RDEPENDS_systemd-libs = "bash coreutils glibc glibc-common grep libcap libgcc libgcrypt libmount lz4-libs sed xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-libs-239-31.el8_2.2.x86_64.rpm \
          "

SRC_URI[systemd-libs.sha256sum] = "629dfc70a744374e8f089573007e09b66acff726faf6b1afd380d855a8512a00"
