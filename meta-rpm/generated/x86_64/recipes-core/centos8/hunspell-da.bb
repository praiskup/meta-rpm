SUMMARY = "generated recipe based on hunspell-da srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-da = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-da-1.7.42-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-da.sha256sum] = "11210372b4b97efb3348ecb1594453f4e4b254e0d0320d24ea02677c853d9e41"
