SUMMARY = "generated recipe based on tmpwatch srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_tmpwatch = "libc.so.6"
RDEPENDS_tmpwatch = "glibc psmisc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tmpwatch-2.11-14.el8.x86_64.rpm \
          "

SRC_URI[tmpwatch.sha256sum] = "9aae1ae6be3e94dd75e5313dc39fd28aace95b1e6be05d60ebe3d8ed66c743fe"
