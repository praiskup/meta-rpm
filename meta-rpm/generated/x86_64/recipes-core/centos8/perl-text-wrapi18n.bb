SUMMARY = "generated recipe based on perl-Text-WrapI18N srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-WrapI18N = "perl-Exporter perl-Text-CharWidth perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Text-WrapI18N-0.06-30.el8.noarch.rpm \
          "

SRC_URI[perl-Text-WrapI18N.sha256sum] = "5da416fb8567fa4c49d54805e58338f2194995a01780983dbd707a77a68191fe"
