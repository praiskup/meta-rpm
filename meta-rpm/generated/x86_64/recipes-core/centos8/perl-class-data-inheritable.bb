SUMMARY = "generated recipe based on perl-Class-Data-Inheritable srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Data-Inheritable = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Data-Inheritable-0.08-27.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Data-Inheritable.sha256sum] = "c7875018c5aec5f8a6f8f3e8076072b3c84bff4ef930cac692b95811c92bebe6"
