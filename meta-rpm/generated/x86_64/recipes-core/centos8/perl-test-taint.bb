SUMMARY = "generated recipe based on perl-Test-Taint srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Test-Taint = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Test-Taint = "glibc perl-Scalar-List-Utils perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Taint-1.06-19.el8.x86_64.rpm \
          "

SRC_URI[perl-Test-Taint.sha256sum] = "1266e012ee66290c779a111195bf6f0f89b4b191464fb6cf39069aa78654e672"
