SUMMARY = "generated recipe based on zenity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libnotify libx11 pango pkgconfig-native"
RPM_SONAME_REQ_zenity = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_zenity = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libnotify pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/zenity-3.28.1-1.el8.x86_64.rpm \
          "

SRC_URI[zenity.sha256sum] = "e2b4bd939259f92360fb8de4a4196c633004b4fc6d53e838d3d145ea4e3f3b9e"
