SUMMARY = "generated recipe based on rpm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl audit bzip2 db dbus-libs elfutils file ima-evm-utils libarchive libcap libselinux lua openssl pkgconfig-native platform-python3 popt xz zlib zstd"
RPM_SONAME_REQ_python3-rpm = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 libpython3.6m.so.1.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_python3-rpm = "audit-libs bzip2-libs elfutils-libelf elfutils-libs file-libs glibc ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs platform-python popt python3-libs rpm-build-libs rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm = "libacl.so.1 libarchive.so.13 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm = "audit-libs bash bzip2-libs coreutils curl elfutils-libelf glibc libacl libarchive libcap libdb libdb-utils libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-build = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-build = "audit-libs bash binutils bzip2 bzip2-libs cpio diffutils elfutils elfutils-libelf elfutils-libs file file-libs findutils gawk gdb-headless glibc grep gzip libacl libcap libdb libzstd lua-libs openssl-libs patch pkgconf-pkg-config popt redhat-rpm-config rpm rpm-build-libs rpm-libs sed tar unzip xz xz-libs zlib zstd"
RPM_SONAME_PROV_rpm-build-libs = "librpmbuild.so.8 librpmsign.so.8"
RPM_SONAME_REQ_rpm-build-libs = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-build-libs = "audit-libs bzip2-libs elfutils-libelf elfutils-libs file-libs glibc gnupg2 ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RDEPENDS_rpm-cron = "bash crontabs logrotate rpm"
RPM_SONAME_REQ_rpm-devel = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RPROVIDES_rpm-devel = "rpm-dev (= 4.14.2)"
RDEPENDS_rpm-devel = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd libzstd-devel lua-libs openssl-libs pkgconf-pkg-config popt popt-devel rpm rpm-build-libs rpm-libs xz-libs zlib"
RPM_SONAME_PROV_rpm-libs = "librpm.so.8 librpmio.so.8"
RPM_SONAME_REQ_rpm-libs = "ld-linux-x86-64.so.2 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-libs = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-ima = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-ima = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-prioreset = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-prioreset = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-selinux = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-selinux = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libselinux libzstd lua-libs openssl-libs popt rpm-libs selinux-policy-minimum xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-syslog = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-syslog = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-systemd-inhibit = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdbus-1.so.3 libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-systemd-inhibit = "audit-libs bzip2-libs dbus-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-sign = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-sign = "audit-libs bzip2-libs elfutils-libelf glibc ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-build-libs rpm-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpm-build-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-rpm-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-apidocs-4.14.2-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-build-libs-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-cron-4.14.2-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-devel-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-libs-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-ima-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-prioreset-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-selinux-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-syslog-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-systemd-inhibit-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-sign-4.14.2-37.el8.x86_64.rpm \
          "

SRC_URI[python3-rpm.sha256sum] = "a5a9510004eb53f67f5efc3788e5cfba675f09e56cd419b33eaaae7033bcf90e"
SRC_URI[rpm.sha256sum] = "7d8d850130c6772c0921693233aada04b42fa9d71bb2a8c41ea0ab42dda2b81b"
SRC_URI[rpm-apidocs.sha256sum] = "f36fd2c3094c35fe1f75e2991d33e598991810aea9135d60e32b264c17b34739"
SRC_URI[rpm-build.sha256sum] = "fbbdb43b9f0d36fab62ccf53c7e5d5cd6dcd5c840c8d6ca1e338b1366c227bf1"
SRC_URI[rpm-build-libs.sha256sum] = "712a169d7a35a5c14bee17f492f594465fcdd604295ac75642531f1f64d589f7"
SRC_URI[rpm-cron.sha256sum] = "601f9565125f201503dbba97accc4215c157b29ecf8087124b02a7b1e4cf073d"
SRC_URI[rpm-devel.sha256sum] = "4b78e8cf3c3a7f12b5f4ab1a78c7cc4ed760b3f5df9fa1c0a0f93b1881d76f75"
SRC_URI[rpm-libs.sha256sum] = "29ac185f858263a69104489c09f240eca59625acf5e2d239303ff720cebad90e"
SRC_URI[rpm-plugin-ima.sha256sum] = "48f8e6928715f6d35549e61f65bcbf939d6a0e47a70aa2be47281fbd237b4b03"
SRC_URI[rpm-plugin-prioreset.sha256sum] = "b1f678ed2ab302d076a86c067feef55d595456d137a1b9f7eb253397e65d6f04"
SRC_URI[rpm-plugin-selinux.sha256sum] = "e53bcd420a2480eb24b81e5d006ced844f9ef01c69f1032f8a2ae9b118c2fb47"
SRC_URI[rpm-plugin-syslog.sha256sum] = "b2d2a32e0d64afa24f48190eba6bb816fdc6e7a5a20b67561fa2ab15f61a69ba"
SRC_URI[rpm-plugin-systemd-inhibit.sha256sum] = "b2513e90289244083d5657c35c2a82fc51b0f59d0aa03dab26a250663294c621"
SRC_URI[rpm-sign.sha256sum] = "bc94e1ee80be917814c312419a907eba81c81a4c77aeee54e4cab76a27d14db5"
