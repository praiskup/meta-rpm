SUMMARY = "generated recipe based on qemu-kvm srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl ceph curl cyrus-sasl-lib device-mapper-multipath glib-2.0 glusterfs gnutls libaio libcacard libdrm libepoxy libgcc libgcrypt libgpg-error libiscsi libpng libseccomp libssh libusb1 libuuid libxkbcommon lzo mesa numactl pixman pkgconfig-native pmdk rdma-core snappy spice systemd-libs usbredir zlib"
RPM_SONAME_REQ_qemu-guest-agent = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libudev.so.1"
RDEPENDS_qemu-guest-agent = "bash glib2 glibc libgcc libstdc++ systemd systemd-libs"
RPM_SONAME_REQ_qemu-img = "libaio.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-img = "glib2 glibc gnutls libaio libgcc libgcrypt libgpg-error libstdc++ zlib"
RDEPENDS_qemu-kvm = "libpmem mesa-dri-drivers mesa-libEGL mesa-libGL qemu-kvm-block-curl qemu-kvm-block-gluster qemu-kvm-block-iscsi qemu-kvm-block-rbd qemu-kvm-block-ssh qemu-kvm-core"
RPM_SONAME_REQ_qemu-kvm-block-curl = "libc.so.6 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-curl = "glib2 glibc gnutls libcurl libgcc libgcrypt libgpg-error libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-gluster = "libacl.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgfapi.so.0 libgfrpc.so.0 libgfxdr.so.0 libglib-2.0.so.0 libglusterfs.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libuuid.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-gluster = "glib2 glibc glusterfs-api glusterfs-libs gnutls libacl libgcc libgcrypt libgpg-error libstdc++ libuuid qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-iscsi = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libiscsi.so.8 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-iscsi = "glib2 glibc gnutls libgcc libgcrypt libgpg-error libiscsi libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-rbd = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librados.so.2 librbd.so.1 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-rbd = "glib2 glibc gnutls libgcc libgcrypt libgpg-error librados2 librbd1 libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-block-ssh = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssh.so.4 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-block-ssh = "glib2 glibc gnutls libgcc libgcrypt libgpg-error libssh libstdc++ qemu-kvm-common zlib"
RPM_SONAME_REQ_qemu-kvm-common = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libmpathpersist.so.0 libmultipath.so.0 libpthread.so.0 librt.so.1 libstdc++.so.6 libudev.so.1 libutil.so.1 libxkbcommon.so.0 libz.so.1"
RDEPENDS_qemu-kvm-common = "bash device-mapper-multipath-libs glib2 glibc glibc-common gnutls libgcc libgcrypt libgpg-error libstdc++ libxkbcommon platform-python shadow-utils systemd systemd-libs zlib"
RPM_SONAME_REQ_qemu-kvm-core = "libaio.so.1 libc.so.6 libcacard.so.0 libdl.so.2 libdrm.so.2 libepoxy.so.0 libgbm.so.1 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libibumad.so.3 libibverbs.so.1 liblzo2.so.2 libm.so.6 libnuma.so.1 libpixman-1.so.0 libpmem.so.1 libpng16.so.16 libpthread.so.0 librdmacm.so.1 librt.so.1 libsasl2.so.3 libseccomp.so.2 libsnappy.so.1 libspice-server.so.1 libstdc++.so.6 libusb-1.0.so.0 libusbredirparser.so.1 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-core = "bash cyrus-sasl-lib edk2-ovmf glib2 glibc glusterfs-api gnutls ipxe-roms-qemu libaio libcacard libdrm libepoxy libgcc libgcrypt libgpg-error libibumad libibverbs libpmem libpng librdmacm libseccomp libstdc++ libusbx lzo mesa-libgbm numactl-libs pixman qemu-img qemu-kvm-common seabios-bin seavgabios-bin sgabios-bin snappy spice-server usbredir zlib"
RPM_SONAME_REQ_qemu-kvm-tests = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgpg-error.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libutil.so.1 libz.so.1"
RDEPENDS_qemu-kvm-tests = "bash glib2 glibc gnutls libgcc libgcrypt libgpg-error libstdc++ platform-python qemu-kvm zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-guest-agent-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-img-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-block-curl-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-block-gluster-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-block-iscsi-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-block-rbd-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-block-ssh-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-common-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qemu-kvm-core-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qemu-kvm-tests-2.12.0-99.module_el8.2.0+524+f765f7e0.4.x86_64.rpm \
          "

SRC_URI[qemu-guest-agent.sha256sum] = "d1e67e5c1c663951e5df831d5c2dc5a1bfe2ae1eb765cf5e6a70c810c9ba2b51"
SRC_URI[qemu-img.sha256sum] = "04e088205be0b85ef8852d72364e15e014e49390120e398db12299afb8d900b7"
SRC_URI[qemu-kvm.sha256sum] = "d94e97d2205387558ebd05867a05899426b9b85fecec1928194db818c838eb77"
SRC_URI[qemu-kvm-block-curl.sha256sum] = "082d2412bc96bbec005b63e32a8538a3c57e09d2b03acdfe8528895be8a85c88"
SRC_URI[qemu-kvm-block-gluster.sha256sum] = "6a0511b5d0ba80f9ae98dc5d618c827352f4ec4e8afab28421aae67e04597ded"
SRC_URI[qemu-kvm-block-iscsi.sha256sum] = "f8a70d467cd2abf7e9c1c6e5df3c8e796b59ed7277cdccf854b7a774b0b401f8"
SRC_URI[qemu-kvm-block-rbd.sha256sum] = "b019eea8597433fdf10058775be403831762a011c9d7bac6adf0364b17084d78"
SRC_URI[qemu-kvm-block-ssh.sha256sum] = "5ad93f61d738de569fe4bb645ab1e30f72c90ee87da896a746ac9564bf87dfff"
SRC_URI[qemu-kvm-common.sha256sum] = "7f0e2082944887992748500e71c0b116c35a5cb09678bbc913d220d847ed1cef"
SRC_URI[qemu-kvm-core.sha256sum] = "85bb16e29234990d7bd08111836482de56538ee57f047fa49c901645e18d6a6e"
SRC_URI[qemu-kvm-tests.sha256sum] = "11bea0679b049c207bf218144a568b6bd3dc20777b181e8abd9eb79ffa95ada5"
