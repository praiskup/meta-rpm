SUMMARY = "generated recipe based on nautilus srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gnome-autoar gnome-desktop3 gtk+3 libgcc libgexiv2 libselinux pango pkgconfig-native tracker"
RPM_SONAME_PROV_nautilus = "libnautilus-image-properties.so libnautilus-sendto.so"
RPM_SONAME_REQ_nautilus = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgailutil-3.so.0 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgexiv2.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-autoar-0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libnautilus-extension.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libselinux.so.1 libtracker-sparql-2.0.so.0"
RDEPENDS_nautilus = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnome-autoar gnome-desktop3 gsettings-desktop-schemas gtk3 gvfs libexif libgcc libgexiv2 libselinux nautilus-extensions pango tracker"
RPM_SONAME_REQ_nautilus-devel = "libnautilus-extension.so.1"
RPROVIDES_nautilus-devel = "nautilus-dev (= 3.28.1)"
RDEPENDS_nautilus-devel = "glib2-devel gtk3-devel nautilus nautilus-extensions pkgconf-pkg-config"
RPM_SONAME_PROV_nautilus-extensions = "libnautilus-extension.so.1"
RPM_SONAME_REQ_nautilus-extensions = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0"
RDEPENDS_nautilus-extensions = "glib2 glibc gtk3 libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nautilus-3.28.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nautilus-extensions-3.28.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/nautilus-devel-3.28.1-12.el8.x86_64.rpm \
          "

SRC_URI[nautilus.sha256sum] = "aa82797609cc17c19470e52865b2a21264dc524e884e52d337cb5fbd4db1b262"
SRC_URI[nautilus-devel.sha256sum] = "0011885c691ccc72016656dbca2c538dfcc7a200121f0f5c5982e655003a7880"
SRC_URI[nautilus-extensions.sha256sum] = "705d6d49ab98531be476a8ca40c3efbe7221f94971df3028ede97dbbd8fae1b7"
