SUMMARY = "generated recipe based on netlabel_tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native"
RPM_SONAME_REQ_netlabel_tools = "libc.so.6 libnl-3.so.200 libnl-genl-3.so.200"
RDEPENDS_netlabel_tools = "bash glibc kernel libnl3 systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/netlabel_tools-0.30.0-3.el8.x86_64.rpm \
          "

SRC_URI[netlabel_tools.sha256sum] = "ce51d57e175b3970f91c269ebeb0609e23003e55815ccbc07be8fc57277e8f73"
