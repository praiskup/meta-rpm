SUMMARY = "generated recipe based on maven-doxia srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-doxia = "java-1.8.0-openjdk-headless javapackages-filesystem maven-parent plexus-containers-component-metadata"
RDEPENDS_maven-doxia-core = "apache-commons-lang httpcomponents-client httpcomponents-core java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-logging-api maven-doxia-sink-api plexus-containers-component-annotations plexus-containers-container-default plexus-utils xmlunit"
RDEPENDS_maven-doxia-javadoc = "javapackages-filesystem"
RDEPENDS_maven-doxia-logging-api = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default"
RDEPENDS_maven-doxia-module-apt = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-confluence = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-docbook-simple = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-fml = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-latex = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-rtf = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations"
RDEPENDS_maven-doxia-module-twiki = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-xdoc = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-module-xhtml = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-doxia-modules = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia maven-doxia-core maven-doxia-sink-api plexus-containers-component-annotations"
RDEPENDS_maven-doxia-sink-api = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-logging-api"
RDEPENDS_maven-doxia-test-docs = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_maven-doxia-tests = "apache-commons-lang httpcomponents-client httpcomponents-core java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-logging-api maven-doxia-sink-api plexus-containers-component-annotations plexus-containers-container-default plexus-utils xmlunit"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-core-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-javadoc-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-logging-api-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-apt-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-confluence-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-docbook-simple-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-fml-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-latex-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-rtf-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-twiki-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-xdoc-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-module-xhtml-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-modules-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-sink-api-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-test-docs-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-tests-1.7-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-doxia.sha256sum] = "4ee59afa676641f8e1d3c37af3e88834fd6d206fc5fd6aaca9df4d16d8e53bba"
SRC_URI[maven-doxia-core.sha256sum] = "fb90c05ebb47ff7fb23f97fa314afa4a97d0668ee4c7306c887c564877abc2b1"
SRC_URI[maven-doxia-javadoc.sha256sum] = "c8eee5ced0ad25374416ff52dd2b1b5bad682fee16ab2e1e8fa83d241940dcef"
SRC_URI[maven-doxia-logging-api.sha256sum] = "12cc591675f02217761744c14110075deeb9026ae0d84065d73824a494e8e82d"
SRC_URI[maven-doxia-module-apt.sha256sum] = "9f10a69ca799f8c2013af82145bb479acd01c59565a0ff794c03d5a2f89ad33b"
SRC_URI[maven-doxia-module-confluence.sha256sum] = "66c06ae7889efe720b0f34e0fc608b60e1da14a51279df767e2d2eb91c0fa9e8"
SRC_URI[maven-doxia-module-docbook-simple.sha256sum] = "6eca65cbc2888931401d7be07af2bd9d0df806d4c703993305db0d36158fecac"
SRC_URI[maven-doxia-module-fml.sha256sum] = "dfc4bc18ce1a5dce53d0e9fb0a53bf920f15126e69dfa59c8f328788d187be07"
SRC_URI[maven-doxia-module-latex.sha256sum] = "499600f7892be9bff9c357c5f65780ad42c50f51b6ef6e0a26ec0d440bdd11bb"
SRC_URI[maven-doxia-module-rtf.sha256sum] = "02e84bbb7b4d52e15624ba03dab35cca44f684ea4709db99a7d6126ebc73be27"
SRC_URI[maven-doxia-module-twiki.sha256sum] = "f2d278f69888f69468fbf4e412ea83f5fefdc101fada5834a9219fecba6543b3"
SRC_URI[maven-doxia-module-xdoc.sha256sum] = "ee39663b27a6d5047873b2f33aeebedda3887a7a86d4c37351738c86c86e0c4b"
SRC_URI[maven-doxia-module-xhtml.sha256sum] = "c6e1561b2dcd8b3afd1e772cc2d46a6eb55edc83eb5fd60765063a99bcc3de30"
SRC_URI[maven-doxia-modules.sha256sum] = "35513a1c635e763a0380fdf65ceee7283489a3ae823ff3cf5ef4755b6820a514"
SRC_URI[maven-doxia-sink-api.sha256sum] = "63541a8572eb7ac7c95fa25aaef7b83009712c2248efca9e21f9ce8cd6a36cb9"
SRC_URI[maven-doxia-test-docs.sha256sum] = "acc3172cf1f02967263484cda631160909119d075fe089267e7496cc212331fe"
SRC_URI[maven-doxia-tests.sha256sum] = "d5bbeb3ac114ab8d00e69590ee9f5b2ff8fe3540855e1709dd1f1b6b0917cf2b"
