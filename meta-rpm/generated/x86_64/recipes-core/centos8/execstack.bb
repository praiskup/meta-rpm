SUMMARY = "generated recipe based on execstack srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "elfutils libselinux pkgconfig-native"
RPM_SONAME_REQ_execstack = "libc.so.6 libelf.so.1 libselinux.so.1"
RDEPENDS_execstack = "coreutils elfutils-libelf findutils gawk glibc grep libselinux util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/execstack-0.5.0-15.el8.x86_64.rpm \
          "

SRC_URI[execstack.sha256sum] = "bf76dd6a86421911d9accc48ae2bebd2090baf76386e9daa2b9d7a4b975000c8"
