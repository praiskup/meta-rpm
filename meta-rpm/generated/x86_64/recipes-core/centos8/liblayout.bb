SUMMARY = "generated recipe based on liblayout srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_liblayout = "flute java-1.8.0-openjdk-headless javapackages-tools libbase libfonts libloader librepository pentaho-libxml sac"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblayout-0.2.10-17.el8.noarch.rpm \
          "

SRC_URI[liblayout.sha256sum] = "7552b32a0ec67c5fcb6fb3d64c420502047de6205724d836c75cce71b088cee4"
