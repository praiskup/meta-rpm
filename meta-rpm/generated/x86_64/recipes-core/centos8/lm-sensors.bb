SUMMARY = "generated recipe based on lm_sensors srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native rrdtool"
RPM_SONAME_REQ_lm_sensors = "libc.so.6 libsensors.so.4"
RDEPENDS_lm_sensors = "bash dmidecode glibc kmod lm_sensors-libs perl-constant perl-interpreter perl-libs systemd"
RPM_SONAME_REQ_lm_sensors-devel = "libsensors.so.4"
RPROVIDES_lm_sensors-devel = "lm_sensors-dev (= 3.4.0)"
RDEPENDS_lm_sensors-devel = "lm_sensors-libs"
RPM_SONAME_PROV_lm_sensors-libs = "libsensors.so.4"
RPM_SONAME_REQ_lm_sensors-libs = "libc.so.6 libm.so.6"
RDEPENDS_lm_sensors-libs = "glibc"
RPM_SONAME_REQ_lm_sensors-sensord = "libc.so.6 librrd.so.8 libsensors.so.4"
RDEPENDS_lm_sensors-sensord = "bash glibc lm_sensors lm_sensors-libs rrdtool"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lm_sensors-sensord-3.4.0-21.20180522git70f7e08.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lm_sensors-3.4.0-21.20180522git70f7e08.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lm_sensors-devel-3.4.0-21.20180522git70f7e08.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lm_sensors-libs-3.4.0-21.20180522git70f7e08.el8.x86_64.rpm \
          "

SRC_URI[lm_sensors.sha256sum] = "7c8f7ff960920ecc69e533283928cd565a8150be72af3ef6ba9c4ab6ce844083"
SRC_URI[lm_sensors-devel.sha256sum] = "c46bdd85268eadf6d56f5b18c2322b4f6fe2f02ecae33a8cb2ba2367594f7d3b"
SRC_URI[lm_sensors-libs.sha256sum] = "b0c6b56b376385774f464acd4b90c7f9ebc45c347d206f6fba2355499b01701c"
SRC_URI[lm_sensors-sensord.sha256sum] = "e8fa52b7353ece171b88e49586322b814b235da2a2f05b661f318948ac657931"
