SUMMARY = "generated recipe based on i2c-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_i2c-tools = "libc.so.6 libi2c.so.0"
RDEPENDS_i2c-tools = "bash glibc kmod libi2c systemd-udev"
RDEPENDS_i2c-tools-perl = "libi2c perl-constant perl-interpreter perl-libs"
RPM_SONAME_PROV_libi2c = "libi2c.so.0"
RPM_SONAME_REQ_libi2c = "libc.so.6"
RDEPENDS_libi2c = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/i2c-tools-4.0-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/i2c-tools-perl-4.0-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libi2c-4.0-12.el8.x86_64.rpm \
          "

SRC_URI[i2c-tools.sha256sum] = "a7de65d4e890d7cb797e759801b2c3e93641007372097bcd5ae81b241a833724"
SRC_URI[i2c-tools-perl.sha256sum] = "296716291693f45c4bc8f0f3c32f4573cbaee69d48a4db313cc0d6fd37cf439a"
SRC_URI[libi2c.sha256sum] = "2ad0c43956ae4239d8b5a67c7d178d45adf68f4c9a634a4ef3ef32510f0cf7cb"
