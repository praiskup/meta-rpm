SUMMARY = "generated recipe based on pidgin-sipe srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs e2fsprogs farstream02 glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base krb5-libs libnice libxml2 nspr nss pidgin pkgconfig-native"
RDEPENDS_pidgin-sipe = "purple-sipe"
RPM_SONAME_PROV_purple-sipe = "libsipe.so"
RPM_SONAME_REQ_purple-sipe = "libc.so.6 libcom_err.so.2 libdbus-1.so.3 libdl.so.2 libfarstream-0.2.so.5 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstrtp-1.0.so.0 libk5crypto.so.3 libkrb5.so.3 libnice.so.10 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libpurple.so.0 libsmime3.so libssl3.so libxml2.so.2"
RDEPENDS_purple-sipe = "dbus-libs farstream02 glib2 glibc gssntlmssp gstreamer1 gstreamer1-plugins-base krb5-libs libcom_err libnice libpurple libxml2 nspr nss nss-util"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pidgin-sipe-1.23.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/purple-sipe-1.23.2-1.el8.x86_64.rpm \
          "

SRC_URI[pidgin-sipe.sha256sum] = "1676257c8c03291e9c3fae6cae076c7874af702b6cf78c14a7edccbe6a5dc526"
SRC_URI[purple-sipe.sha256sum] = "e71a1d4ac01a7b700a6471b73ff53511fb99340643406716631cc621e1e422b9"
