SUMMARY = "generated recipe based on gnome-remote-desktop srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gnutls libgcc libnotify libsecret libvncserver pipewire pkgconfig-native"
RPM_SONAME_REQ_gnome-remote-desktop = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libnotify.so.4 libpipewire-0.2.so.1 libsecret-1.so.0 libvncserver.so.0"
RDEPENDS_gnome-remote-desktop = "bash glib2 glibc gnutls libgcc libnotify libsecret libvncserver pipewire pipewire-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-remote-desktop-0.1.6-8.el8.x86_64.rpm \
          "

SRC_URI[gnome-remote-desktop.sha256sum] = "a3bf608768fd1e38492c49988ef98858d84cb1e1e0981710901444b779c055df"
