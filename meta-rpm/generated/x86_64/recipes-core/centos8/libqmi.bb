SUMMARY = "generated recipe based on libqmi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libgudev libmbim pkgconfig-native"
RPM_SONAME_PROV_libqmi = "libqmi-glib.so.5"
RPM_SONAME_REQ_libqmi = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libmbim-glib.so.4"
RDEPENDS_libqmi = "glib2 glibc libmbim"
RPM_SONAME_REQ_libqmi-utils = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmbim-glib.so.4 libqmi-glib.so.5"
RDEPENDS_libqmi-utils = "bash glib2 glibc libgudev libmbim libqmi"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libqmi-1.24.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libqmi-utils-1.24.0-1.el8.x86_64.rpm \
          "

SRC_URI[libqmi.sha256sum] = "406aea2bb2a5f83cabee5a50bad08a3516776c3cd42db8b159f926624f61aa42"
SRC_URI[libqmi-utils.sha256sum] = "020d5402ebe44d38a6ec58e601483e51b7a2b407051583487d958426d582081e"
