SUMMARY = "generated recipe based on libiec61883 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libraw1394 pkgconfig-native"
RPM_SONAME_PROV_libiec61883 = "libiec61883.so.0"
RPM_SONAME_REQ_libiec61883 = "libc.so.6 libraw1394.so.11"
RDEPENDS_libiec61883 = "glibc libraw1394"
RPM_SONAME_REQ_libiec61883-devel = "libiec61883.so.0"
RPROVIDES_libiec61883-devel = "libiec61883-dev (= 1.2.0)"
RDEPENDS_libiec61883-devel = "libiec61883 libraw1394-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libiec61883-1.2.0-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libiec61883-devel-1.2.0-18.el8.x86_64.rpm \
          "

SRC_URI[libiec61883.sha256sum] = "6f1c17113ebb1c306706d2687b909fa677aef279e45aa3a8f26718bff2286dfa"
SRC_URI[libiec61883-devel.sha256sum] = "5448cf02f766b76af530b0516ca0a9b0537076966a934eb7c8b9bbd45ea734cf"
