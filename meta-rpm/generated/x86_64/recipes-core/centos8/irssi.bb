SUMMARY = "generated recipe based on irssi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 libxcrypt ncurses openssl perl pkgconfig-native"
RPM_SONAME_PROV_irssi = "libirc_proxy.so"
RPM_SONAME_REQ_irssi = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libglib-2.0.so.0 libgmodule-2.0.so.0 libm.so.6 libperl.so.5.26 libpthread.so.0 libresolv.so.2 libssl.so.1.1 libtinfo.so.6 libutil.so.1"
RDEPENDS_irssi = "glib2 glibc libxcrypt ncurses-libs openssl-libs perl-Carp perl-Exporter perl-interpreter perl-libs"
RPROVIDES_irssi-devel = "irssi-dev (= 1.1.1)"
RDEPENDS_irssi-devel = "irssi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/irssi-1.1.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/irssi-devel-1.1.1-3.el8.x86_64.rpm \
          "

SRC_URI[irssi.sha256sum] = "1d4694787e3b81e386c71b62d7a28ab215ae18fb76aff31630b34b7984892c6c"
SRC_URI[irssi-devel.sha256sum] = "b9fc87726aedfb823f7e80d1c38290d027fa5b468e53e211d3925b0c021ef55b"
