SUMMARY = "generated recipe based on scipy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc libgcc openblas pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-scipy = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libgfortran.so.5 libm.so.6 libopenblas.so.0 libopenblasp.so.0 libpthread.so.0 libpython3.6m.so.1.0 libquadmath.so.0 libstdc++.so.6"
RDEPENDS_python3-scipy = "glibc libgcc libgfortran libquadmath libstdc++ openblas openblas-threads platform-python python3-libs python3-numpy python3-numpy-f2py python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-scipy-1.0.0-20.module_el8.1.0+245+c39af44f.x86_64.rpm \
          "

SRC_URI[python3-scipy.sha256sum] = "0fdbf88a443a41d04fe17a16cca9effd6c884599385c45c2d1500deb1a9f3061"
