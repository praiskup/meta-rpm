SUMMARY = "generated recipe based on iptstate srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libnetfilter-conntrack ncurses pkgconfig-native"
RPM_SONAME_REQ_iptstate = "libc.so.6 libgcc_s.so.1 libm.so.6 libncurses.so.6 libnetfilter_conntrack.so.3 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_iptstate = "glibc iptables libgcc libnetfilter_conntrack libstdc++ ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iptstate-2.2.6-6.el8.x86_64.rpm \
          "

SRC_URI[iptstate.sha256sum] = "40b398f25aa72d1e3ec18d3675a036171cff5c259052a84cd5ed6672133c24c4"
