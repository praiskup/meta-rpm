SUMMARY = "generated recipe based on nfs4-acl-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "attr pkgconfig-native"
RPM_SONAME_REQ_nfs4-acl-tools = "libattr.so.1 libc.so.6"
RDEPENDS_nfs4-acl-tools = "glibc libattr"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nfs4-acl-tools-0.3.5-3.el8.x86_64.rpm \
          "

SRC_URI[nfs4-acl-tools.sha256sum] = "85b600bfefce845490b12254c6994e7ac7225ee71d7221cbd7d59087b2d80bff"
