SUMMARY = "generated recipe based on systemtap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_systemtap-sdt-devel = "systemtap-sdt-dev (= 4.2)"
RDEPENDS_systemtap-sdt-devel = "platform-python python3-pyparsing"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-sdt-devel-4.2-6.el8.x86_64.rpm \
          "

SRC_URI[systemtap-sdt-devel.sha256sum] = "405283b568287597cffcc77effb40d4e41035d5b00ea1c5761a442cd3a1a3b6b"
