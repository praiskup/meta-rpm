SUMMARY = "generated recipe based on virt-top srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt ncurses pkgconfig-native"
RPM_SONAME_REQ_virt-top = "libc.so.6 libdl.so.2 libm.so.6 libncursesw.so.6 libtinfo.so.6 libvirt.so.0"
RDEPENDS_virt-top = "glibc libvirt-libs ncurses-libs platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/virt-top-1.0.8-32.el8.x86_64.rpm \
          "

SRC_URI[virt-top.sha256sum] = "ebb2147ed713bd8853b267188dafc4eda19f1304fb3c2b5c02d86e8c07855c21"
