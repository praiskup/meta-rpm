SUMMARY = "generated recipe based on hunspell-nl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-nl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-nl-2.10-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-nl.sha256sum] = "d2df1cdb7e61480d071a8581da886c8637c313d02655c78282b7aa5ab99df7c6"
