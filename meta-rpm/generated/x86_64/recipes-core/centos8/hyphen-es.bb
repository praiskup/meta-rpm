SUMMARY = "generated recipe based on hyphen-es srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-es = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-es-2.3-2.el8.noarch.rpm \
          "

SRC_URI[hyphen-es.sha256sum] = "8d69453d336fda4f1cf11ef5dfbf95f8ad142e747efb8d932ee89905093225f6"
