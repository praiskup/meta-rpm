SUMMARY = "generated recipe based on perl-Carp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Carp = "perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Carp-1.42-396.el8.noarch.rpm \
          "

SRC_URI[perl-Carp.sha256sum] = "d03b9f4b9848e3a88d62bcf6e536d659c325b2dc03b2136be7342b5fe5e2b6a9"
