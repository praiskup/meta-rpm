SUMMARY = "generated recipe based on hunspell-uk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-uk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-uk-1.8.0-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-uk.sha256sum] = "3bfb61544d45824ab090981895f592f1ae2b341921b180fea099c7b34f823076"
