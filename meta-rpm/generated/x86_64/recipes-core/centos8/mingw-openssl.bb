SUMMARY = "generated recipe based on mingw-openssl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-openssl = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-pkg-config mingw32-zlib pkgconf-pkg-config"
RDEPENDS_mingw64-openssl = "mingw64-crt mingw64-filesystem mingw64-pkg-config mingw64-zlib pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-openssl-1.0.2k-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-openssl-1.0.2k-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-openssl.sha256sum] = "a51039a06bf3d626b2e47c7296907161c7ed8047386690f567c0132edc196820"
SRC_URI[mingw64-openssl.sha256sum] = "3b9e0fd3f13f55b02f82f9fbc955a684af5f7ade1a3c2c7a0c18670452160d1b"
