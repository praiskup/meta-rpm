SUMMARY = "generated recipe based on hunspell-ti srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ti = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ti-0.20090911-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-ti.sha256sum] = "5ebec6fd3e705d5f7ff38d214671ef2892c9cf5abed367f8030964d7222759ca"
