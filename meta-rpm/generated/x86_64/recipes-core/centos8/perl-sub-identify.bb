SUMMARY = "generated recipe based on perl-Sub-Identify srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sub-Identify = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sub-Identify = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Sub-Identify-0.14-6.el8.x86_64.rpm \
          "

SRC_URI[perl-Sub-Identify.sha256sum] = "474110dc671e3a353dcc317de2b49dbe29f25abf6f357b45705f1277cf8410a8"
