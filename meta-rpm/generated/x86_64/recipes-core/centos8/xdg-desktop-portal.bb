SUMMARY = "generated recipe based on xdg-desktop-portal srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "fontconfig freetype fuse geoclue2 glib-2.0 json-glib libgcc pipewire pkgconfig-native"
RPM_SONAME_REQ_xdg-desktop-portal = "libc.so.6 libfontconfig.so.1 libfreetype.so.6 libfuse.so.2 libgcc_s.so.1 libgeoclue-2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpipewire-0.2.so.1 libpthread.so.0"
RDEPENDS_xdg-desktop-portal = "bash dbus fontconfig freetype fuse fuse-libs geoclue2 geoclue2-libs glib2 glibc json-glib libgcc pipewire-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xdg-desktop-portal-1.4.2-1.el8.x86_64.rpm \
          "

SRC_URI[xdg-desktop-portal.sha256sum] = "cc94f0e03f628cbdbfc0d26b2480f3bd3d6f47b2937c857100979cb88d4b4c3e"
