SUMMARY = "generated recipe based on velocity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_velocity = "apache-commons-collections apache-commons-lang java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_velocity-demo = "velocity"
RDEPENDS_velocity-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/velocity-1.7-24.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/velocity-demo-1.7-24.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/velocity-javadoc-1.7-24.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/velocity-manual-1.7-24.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[velocity.sha256sum] = "b8ac4ed0fb41e242f52fc972f6349dcfaf58c10ec5a2ed5495c5782104043ef9"
SRC_URI[velocity-demo.sha256sum] = "e1c0c23569143d741124b58f8697f384977a92831bdc69a55e71af856f45f6de"
SRC_URI[velocity-javadoc.sha256sum] = "d99ae6bf56c6e0fa177a4cc5622e733f400bbe1d13f8ce646ddb878b217e2b90"
SRC_URI[velocity-manual.sha256sum] = "a4b676021e63f112ed6c2ee24821d64ef7ca0d7b9663e14140b55dcf26236fa3"
