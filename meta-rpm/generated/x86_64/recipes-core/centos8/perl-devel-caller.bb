SUMMARY = "generated recipe based on perl-Devel-Caller srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Devel-Caller = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Devel-Caller = "glibc perl-Exporter perl-PadWalker perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Devel-Caller-2.06-15.el8.x86_64.rpm \
          "

SRC_URI[perl-Devel-Caller.sha256sum] = "9eb2e42890edad380c0188b2e5744d88ef25b17154d904daa4d194035c678214"
