SUMMARY = "generated recipe based on mod_auth_mellon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl glib-2.0 lasso openssl pkgconfig-native"
RPM_SONAME_REQ_mod_auth_mellon = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libglib-2.0.so.0 liblasso.so.3 libssl.so.1.1"
RDEPENDS_mod_auth_mellon = "bash glib2 glibc httpd lasso libcurl openssl-libs"
RPM_SONAME_REQ_mod_auth_mellon-diagnostics = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libglib-2.0.so.0 liblasso.so.3 libssl.so.1.1"
RDEPENDS_mod_auth_mellon-diagnostics = "glib2 glibc lasso libcurl mod_auth_mellon openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_auth_mellon-0.14.0-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_auth_mellon-diagnostics-0.14.0-11.el8.x86_64.rpm \
          "

SRC_URI[mod_auth_mellon.sha256sum] = "33cba98a17e83e0a0e0988bf2554f6bee53201d5f7737e1560b31d3928e91b66"
SRC_URI[mod_auth_mellon-diagnostics.sha256sum] = "729f2b24e8a4702d389caf724a094a68261c2cd93d22fcce4431ed352d51a251"
