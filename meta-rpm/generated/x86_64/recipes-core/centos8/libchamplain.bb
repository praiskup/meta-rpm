SUMMARY = "generated recipe based on libchamplain srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo clutter clutter-gtk cogl gdk-pixbuf glib-2.0 gtk+3 libsoup-2.4 pango pkgconfig-native sqlite3"
RPM_SONAME_PROV_libchamplain = "libchamplain-0.12.so.0"
RPM_SONAME_REQ_libchamplain = "libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libcogl-path.so.20 libcogl.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0"
RDEPENDS_libchamplain = "cairo cairo-gobject clutter cogl gdk-pixbuf2 glib2 glibc gtk3 libsoup pango sqlite-libs"
RPM_SONAME_REQ_libchamplain-devel = "libchamplain-0.12.so.0 libchamplain-gtk-0.12.so.0"
RPROVIDES_libchamplain-devel = "libchamplain-dev (= 0.12.16)"
RDEPENDS_libchamplain-devel = "cairo-devel clutter-devel clutter-gtk-devel glib2-devel gtk3-devel libchamplain libchamplain-gtk libsoup-devel pkgconf-pkg-config sqlite-devel"
RPM_SONAME_PROV_libchamplain-gtk = "libchamplain-gtk-0.12.so.0"
RPM_SONAME_REQ_libchamplain-gtk = "libc.so.6 libchamplain-0.12.so.0 libclutter-1.0.so.0 libclutter-gtk-1.0.so.0 libgdk-3.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpthread.so.0"
RDEPENDS_libchamplain-gtk = "clutter clutter-gtk glib2 glibc gtk3 libchamplain"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libchamplain-0.12.16-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libchamplain-devel-0.12.16-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libchamplain-gtk-0.12.16-2.el8.x86_64.rpm \
          "

SRC_URI[libchamplain.sha256sum] = "ff40ce11f38cddd1b7a6d037fedf7e4a1996e2834f0f375040605d9c0f281745"
SRC_URI[libchamplain-devel.sha256sum] = "9dfdfaf885bb83e9c68c0530136bccf5b3a85a88fbe4ee6a66aaa81f9ba938a5"
SRC_URI[libchamplain-gtk.sha256sum] = "5fce92c12c9ed65967fb7ee20a8c5bd20cf690d6ca32283945ace0a2c7e608b9"
