SUMMARY = "generated recipe based on libpsl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libidn2 libunistring pkgconfig-native"
RPM_SONAME_PROV_libpsl = "libpsl.so.5"
RPM_SONAME_REQ_libpsl = "libc.so.6 libidn2.so.0 libunistring.so.2"
RDEPENDS_libpsl = "glibc libidn2 libunistring publicsuffix-list-dafsa"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpsl-0.20.2-5.el8.x86_64.rpm \
          "

SRC_URI[libpsl.sha256sum] = "5db383ff786e862a8de88a06393720e2aba9f8847c1f4738a693a0c536f489dc"
