SUMMARY = "generated recipe based on fio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ceph libaio numactl pkgconfig-native pmdk rdma-core zlib"
RPM_SONAME_REQ_fio = "libaio.so.1 libc.so.6 libdl.so.2 libibverbs.so.1 libm.so.6 libnuma.so.1 libpmem.so.1 libpmemblk.so.1 libpthread.so.0 librados.so.2 librbd.so.1 librdmacm.so.1 librt.so.1 libz.so.1"
RDEPENDS_fio = "bash glibc libaio libibverbs libpmem libpmemblk librados2 librbd1 librdmacm numactl-libs platform-python zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fio-3.7-3.el8.x86_64.rpm \
          "

SRC_URI[fio.sha256sum] = "ad7f33c8f741377010b9e2147199cb3fb071814b1e344b1b7a2c26c580b9745a"
