SUMMARY = "generated recipe based on qt5-qtcanvas3d srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtcanvas3d = "libqtcanvas3d.so"
RPM_SONAME_REQ_qt5-qtcanvas3d = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtcanvas3d = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtcanvas3d-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtcanvas3d-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtcanvas3d qt5-qtdeclarative"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtcanvas3d-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtcanvas3d-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtcanvas3d.sha256sum] = "5e0f803cdef43a570dc421e2e9ca6f2da14426a7f32f51d1eb12622d7adc573a"
SRC_URI[qt5-qtcanvas3d-examples.sha256sum] = "8cc7e5af3043a48c3d580668f5565769f43e7ffff2d4e9128e6c8146b0a42f9c"
