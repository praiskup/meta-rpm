SUMMARY = "generated recipe based on pmdk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libfabric libgcc ndctl pkgconfig-native"
RPM_SONAME_REQ_daxio = "libc.so.6 libdaxctl.so.1 libdl.so.2 libndctl.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_daxio = "daxctl-libs glibc libpmem ndctl-libs"
RPM_SONAME_PROV_libpmem = "libpmem.so.1"
RPM_SONAME_REQ_libpmem = "libc.so.6 libpthread.so.0"
RDEPENDS_libpmem = "glibc"
RPM_SONAME_REQ_libpmem-debug = "libc.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_libpmem-debug = "glibc libpmem"
RPM_SONAME_REQ_libpmem-devel = "libpmem.so.1"
RPROVIDES_libpmem-devel = "libpmem-dev (= 1.6.1)"
RDEPENDS_libpmem-devel = "libpmem pkgconf-pkg-config"
RPM_SONAME_PROV_libpmemblk = "libpmemblk.so.1"
RPM_SONAME_REQ_libpmemblk = "libc.so.6 libdaxctl.so.1 libndctl.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_libpmemblk = "daxctl-libs glibc libpmem ndctl-libs"
RPM_SONAME_REQ_libpmemblk-debug = "libc.so.6 libdaxctl.so.1 libndctl.so.6 libpmem.so.1 libpmemblk.so.1 libpthread.so.0"
RDEPENDS_libpmemblk-debug = "daxctl-libs glibc libpmem libpmemblk ndctl-libs"
RPM_SONAME_REQ_libpmemblk-devel = "libpmemblk.so.1"
RPROVIDES_libpmemblk-devel = "libpmemblk-dev (= 1.6.1)"
RDEPENDS_libpmemblk-devel = "daxctl-devel libpmem-devel libpmemblk ndctl-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libpmemlog = "libpmemlog.so.1"
RPM_SONAME_REQ_libpmemlog = "libc.so.6 libdaxctl.so.1 libndctl.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_libpmemlog = "daxctl-libs glibc libpmem ndctl-libs"
RPM_SONAME_REQ_libpmemlog-debug = "libc.so.6 libdaxctl.so.1 libndctl.so.6 libpmem.so.1 libpmemlog.so.1 libpthread.so.0"
RDEPENDS_libpmemlog-debug = "daxctl-libs glibc libpmem libpmemlog ndctl-libs"
RPM_SONAME_REQ_libpmemlog-devel = "libpmemlog.so.1"
RPROVIDES_libpmemlog-devel = "libpmemlog-dev (= 1.6.1)"
RDEPENDS_libpmemlog-devel = "daxctl-devel libpmem-devel libpmemlog ndctl-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libpmemobj = "libpmemobj.so.1"
RPM_SONAME_REQ_libpmemobj = "ld-linux-x86-64.so.2 libc.so.6 libdaxctl.so.1 libdl.so.2 libndctl.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_libpmemobj = "daxctl-libs glibc libpmem ndctl-libs"
RPM_SONAME_REQ_libpmemobj-debug = "ld-linux-x86-64.so.2 libc.so.6 libdaxctl.so.1 libdl.so.2 libgcc_s.so.1 libndctl.so.6 libpmem.so.1 libpmemobj.so.1 libpthread.so.0"
RDEPENDS_libpmemobj-debug = "daxctl-libs glibc libgcc libpmem libpmemobj ndctl-libs"
RPM_SONAME_REQ_libpmemobj-devel = "libpmemobj.so.1"
RPROVIDES_libpmemobj-devel = "libpmemobj-dev (= 1.6.1)"
RDEPENDS_libpmemobj-devel = "daxctl-devel libpmem-devel libpmemobj ndctl-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libpmempool = "libpmempool.so.1"
RPM_SONAME_REQ_libpmempool = "libc.so.6 libdaxctl.so.1 libdl.so.2 libndctl.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_libpmempool = "daxctl-libs glibc libpmem ndctl-libs"
RPM_SONAME_REQ_libpmempool-debug = "libc.so.6 libdaxctl.so.1 libdl.so.2 libndctl.so.6 libpmem.so.1 libpmempool.so.1 libpthread.so.0"
RDEPENDS_libpmempool-debug = "daxctl-libs glibc libpmem libpmempool ndctl-libs"
RPM_SONAME_REQ_libpmempool-devel = "libpmempool.so.1"
RPROVIDES_libpmempool-devel = "libpmempool-dev (= 1.6.1)"
RDEPENDS_libpmempool-devel = "daxctl-devel libpmem-devel libpmempool ndctl-devel pkgconf-pkg-config"
RPM_SONAME_PROV_librpmem = "librpmem.so.1"
RPM_SONAME_REQ_librpmem = "libc.so.6 libfabric.so.1 libpthread.so.0"
RDEPENDS_librpmem = "glibc libfabric openssh-clients"
RPM_SONAME_REQ_librpmem-debug = "libc.so.6 libfabric.so.1 libpthread.so.0 librpmem.so.1"
RDEPENDS_librpmem-debug = "glibc libfabric librpmem"
RPM_SONAME_REQ_librpmem-devel = "librpmem.so.1"
RPROVIDES_librpmem-devel = "librpmem-dev (= 1.6.1)"
RDEPENDS_librpmem-devel = "librpmem pkgconf-pkg-config"
RPM_SONAME_PROV_libvmem = "libvmem.so.1"
RPM_SONAME_REQ_libvmem = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_libvmem = "glibc"
RPM_SONAME_REQ_libvmem-debug = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0 libvmem.so.1"
RDEPENDS_libvmem-debug = "glibc libvmem"
RPM_SONAME_REQ_libvmem-devel = "libvmem.so.1"
RPROVIDES_libvmem-devel = "libvmem-dev (= 1.6.1)"
RDEPENDS_libvmem-devel = "libvmem pkgconf-pkg-config"
RPM_SONAME_PROV_libvmmalloc = "libvmmalloc.so.1"
RPM_SONAME_REQ_libvmmalloc = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_libvmmalloc = "glibc"
RPM_SONAME_REQ_libvmmalloc-debug = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0 libvmmalloc.so.1"
RDEPENDS_libvmmalloc-debug = "glibc libvmmalloc"
RPM_SONAME_REQ_libvmmalloc-devel = "libvmmalloc.so.1"
RPROVIDES_libvmmalloc-devel = "libvmmalloc-dev (= 1.6.1)"
RDEPENDS_libvmmalloc-devel = "libvmmalloc pkgconf-pkg-config"
RPM_SONAME_REQ_pmempool = "ld-linux-x86-64.so.2 libc.so.6 libdaxctl.so.1 libdl.so.2 libndctl.so.6 libpmem.so.1 libpmemblk.so.1 libpmemlog.so.1 libpmemobj.so.1 libpmempool.so.1 libpthread.so.0"
RDEPENDS_pmempool = "daxctl-libs glibc libpmem libpmemblk libpmemlog libpmemobj libpmempool ndctl-libs"
RDEPENDS_pmreorder = "bash platform-python"
RPM_SONAME_REQ_rpmemd = "libc.so.6 libdaxctl.so.1 libdl.so.2 libfabric.so.1 libndctl.so.6 libpmem.so.1 libpthread.so.0"
RDEPENDS_rpmemd = "daxctl-libs glibc libfabric libpmem ndctl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/daxio-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmem-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmem-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemblk-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemblk-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemlog-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemlog-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemobj-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemobj-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmempool-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmempool-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librpmem-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librpmem-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvmem-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvmem-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvmmalloc-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvmmalloc-devel-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pmempool-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pmreorder-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpmemd-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpmem-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpmemblk-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpmemlog-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpmemobj-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpmempool-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librpmem-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvmem-debug-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvmmalloc-debug-1.6.1-1.el8.x86_64.rpm \
          "

SRC_URI[daxio.sha256sum] = "a990fd048fd78a1d931672edf9bf606812811c806195704d6bf9daf3b6fa9110"
SRC_URI[libpmem.sha256sum] = "6144ca7a1ae17b5b14f0fd1eb60017350112338cab034f883ea4f434fe573b04"
SRC_URI[libpmem-debug.sha256sum] = "dd17b9f6aa172f2d29b68391d18c443fe801420c15a439cb8869dfad724ee369"
SRC_URI[libpmem-devel.sha256sum] = "109307e34d75219c38c0ec8ed5edd2e70e8cd16bbda6128b89650b205d05a226"
SRC_URI[libpmemblk.sha256sum] = "7c38c873e6ae684565580e7c8a52141f6b59a471f921794b386afab020116b36"
SRC_URI[libpmemblk-debug.sha256sum] = "d070de92aefa2d6b7b6b192804df03e4fe548451a44955fe57fd97f234a52b31"
SRC_URI[libpmemblk-devel.sha256sum] = "404f3b172ac8ddf928c174ac09a5275a53731774eb9a9846554dc6e48bf490ad"
SRC_URI[libpmemlog.sha256sum] = "7b172d53f808e8c28e61921440168a277f6c171abdc0c5d4b15c9e5ea82f519c"
SRC_URI[libpmemlog-debug.sha256sum] = "1e0e291290e6c9ad299d92f115ba9b9bf9ac76b20ad7dae0c05565fa6de380ed"
SRC_URI[libpmemlog-devel.sha256sum] = "44db29af816e73406a176bcece416bb17e7a52e4498d46accfdbeac156c33c42"
SRC_URI[libpmemobj.sha256sum] = "022b790b325a4956c68138076959080c38952fe09b81366004f81011021cd740"
SRC_URI[libpmemobj-debug.sha256sum] = "c1511d1df1d8bf8cd46c507b4f71f59bc18d2d78bd9cc7cd4ff44a3e20622eb7"
SRC_URI[libpmemobj-devel.sha256sum] = "8f306652f94bf223a62808048a5f17047bab4c142dc314c8b710b208c0b3ea07"
SRC_URI[libpmempool.sha256sum] = "72e81bfbad23e2f71142e3a85d9ea845e2cb28d100d92470952ff3c3a3c7af6a"
SRC_URI[libpmempool-debug.sha256sum] = "4ff4b39f9636240faf47bae807ff9d3f5efef700dc0d22d1bd7f3d92dccb27ae"
SRC_URI[libpmempool-devel.sha256sum] = "3db557bf8d83dc580c7ceca412eadb4a49fcd4b7f362d6694c97571bc7f2eaf3"
SRC_URI[librpmem.sha256sum] = "1290df2c08bc628792f3e0d93e816f622578278eadc5f5bff89dc71c88f00d8f"
SRC_URI[librpmem-debug.sha256sum] = "f59f76f3930b847e22c0616a9c355c5cd4dc960a23c3bfd30cd500ccd83a8e10"
SRC_URI[librpmem-devel.sha256sum] = "47ef9a185aadd0739d983bfbca13403444b75e9a63dc8efdabb39e1daeb82b04"
SRC_URI[libvmem.sha256sum] = "23e221701e7ce2534be8b7d539266f15dce9c2f5cd4104da39e397997d9be0d9"
SRC_URI[libvmem-debug.sha256sum] = "44d30aba0b7f6f6742002a8fa93a4603824825b29243d434ececa1e6f5309cd7"
SRC_URI[libvmem-devel.sha256sum] = "e54e100504d675a6c69bb6a53417647fb944f1be56d74715418eb4cc6df7dde7"
SRC_URI[libvmmalloc.sha256sum] = "4425c5daeebb7547a0cf6197461b78a5b6cca5fd039de270dad5ea7282e5290a"
SRC_URI[libvmmalloc-debug.sha256sum] = "56900aee89fec38867b32d76f27334158a69c361f3a7a9c55560293a7ace51f4"
SRC_URI[libvmmalloc-devel.sha256sum] = "b481a51d4ac8b1ea9261e6bf9eb54958d6327f225ca77c1c055db8765fb2e83e"
SRC_URI[pmempool.sha256sum] = "94f7c35cb373a7546a524d191b1cbd1a197540fbe8438645b83ed670a41c4a8f"
SRC_URI[pmreorder.sha256sum] = "9c9382134287c61143fca223aa00345ceaad36fcaa1a31476b48b69ed3c1ceb4"
SRC_URI[rpmemd.sha256sum] = "7e58278e4a8bba5978f88271d9a4b0738f9c7ac29750a97a788bd52f1d72dca5"
