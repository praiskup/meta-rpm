SUMMARY = "generated recipe based on ibus-sayura srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 ibus pkgconfig-native"
RPM_SONAME_REQ_ibus-sayura = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libibus-1.0.so.5 libpthread.so.0"
RDEPENDS_ibus-sayura = "glib2 glibc ibus ibus-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-sayura-1.3.2-13.el8.x86_64.rpm \
          "

SRC_URI[ibus-sayura.sha256sum] = "c1ff9c8b06d48db63e8694d62cb8fbfc33b7742de33fdcfd1b725f051e8d3257"
