SUMMARY = "generated recipe based on glusterfs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "acl libgcc libtirpc libuuid libxml2 ncurses openssl pkgconfig-native rdma-core readline zlib"
RPM_SONAME_REQ_glusterfs = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs = "bash glibc glusterfs-libs libgcc libtirpc libuuid openssl-libs shadow-utils systemd zlib"
RPM_SONAME_PROV_glusterfs-api = "libgfapi.so.0"
RPM_SONAME_REQ_glusterfs-api = "libacl.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfapi.so.0 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-api = "bash glibc glusterfs glusterfs-client-xlators glusterfs-libs libacl libtirpc libuuid openssl-libs zlib"
RPM_SONAME_REQ_glusterfs-api-devel = "libgfapi.so.0"
RPROVIDES_glusterfs-api-devel = "glusterfs-api-dev (= 6.0)"
RDEPENDS_glusterfs-api-devel = "glusterfs glusterfs-api glusterfs-devel libacl-devel libuuid-devel pkgconf-pkg-config"
RPM_SONAME_REQ_glusterfs-cli = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 librt.so.1 libtinfo.so.6 libtirpc.so.3 libuuid.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_glusterfs-cli = "bash glibc glusterfs-libs libtirpc libuuid libxml2 ncurses-libs openssl-libs readline zlib"
RPM_SONAME_REQ_glusterfs-client-xlators = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-client-xlators = "glibc glusterfs-libs libtirpc libuuid openssl-libs zlib"
RPM_SONAME_REQ_glusterfs-devel = "libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0"
RPROVIDES_glusterfs-devel = "glusterfs-dev (= 6.0)"
RDEPENDS_glusterfs-devel = "glusterfs glusterfs-libs"
RPM_SONAME_REQ_glusterfs-fuse = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-fuse = "attr bash glibc glusterfs glusterfs-client-xlators glusterfs-libs libtirpc libuuid openssl-libs psmisc zlib"
RPM_SONAME_PROV_glusterfs-libs = "libgfchangelog.so.0 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0"
RPM_SONAME_REQ_glusterfs-libs = "ld-linux-x86-64.so.2 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-libs = "bash glibc libtirpc libuuid openssl-libs zlib"
RPM_SONAME_REQ_glusterfs-rdma = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libibverbs.so.1 libm.so.6 libpthread.so.0 librdmacm.so.1 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-rdma = "glibc glusterfs glusterfs-libs libibverbs librdmacm libtirpc libuuid openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glusterfs-api-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glusterfs-cli-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glusterfs-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glusterfs-client-xlators-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glusterfs-fuse-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glusterfs-libs-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glusterfs-rdma-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glusterfs-api-devel-6.0-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glusterfs-devel-6.0-37.el8.x86_64.rpm \
          "

SRC_URI[glusterfs.sha256sum] = "eac33431b5bd21333fbd87fcce418fd2cdce8d57d33115d2d0ca5a7a8e1d7632"
SRC_URI[glusterfs-api.sha256sum] = "f50cdbf15cacb049b9eb03cdea143470e0675ea36f012caa67eaf364a07e084f"
SRC_URI[glusterfs-api-devel.sha256sum] = "9292e42ee8cfabe0cf5c9750ad1ea9340f83755e3a2cf98301b3edfe5e1d072b"
SRC_URI[glusterfs-cli.sha256sum] = "f72c60a116ae74b6a7da8724b2f692f2a2aaf534532c0b74a6bb7d3f2ba191c2"
SRC_URI[glusterfs-client-xlators.sha256sum] = "1ef98d45991bed0583ad0bd0946d1268295be132643029d62348697599390100"
SRC_URI[glusterfs-devel.sha256sum] = "a9c5eb5c15c3278851f1498d0a047525c5a1528b21795bce2bf4b3535da7494d"
SRC_URI[glusterfs-fuse.sha256sum] = "0cf019c3280915f20e20721f636bbf95f61ad36a961a08d309de26c0f6bee026"
SRC_URI[glusterfs-libs.sha256sum] = "766b3c3e5966de07f957158743a2b4df5aa7caa2e030b919a7e1f08cc337ef39"
SRC_URI[glusterfs-rdma.sha256sum] = "6bd4a1f5db0798bd37aff30783952fc15ce405f7770b75bef199ac2e6e0e6bf3"
