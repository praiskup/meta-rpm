SUMMARY = "generated recipe based on autogen srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "guile libxml2 pkgconfig-native xz zlib"
RPM_SONAME_REQ_autogen = "libc.so.6 libdl.so.2 libguile-2.0.so.22 liblzma.so.5 libm.so.6 libopts.so.25 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_autogen = "autogen-libopts bash glibc guile info libxml2 perl-Carp perl-Exporter perl-Scalar-List-Utils perl-Text-ParseWords perl-constant perl-interpreter perl-libs xz-libs zlib"
RPM_SONAME_PROV_autogen-libopts = "libopts.so.25"
RPM_SONAME_REQ_autogen-libopts = "libc.so.6"
RDEPENDS_autogen-libopts = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autogen-libopts-5.18.12-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/autogen-5.18.12-7.el8.x86_64.rpm \
          "

SRC_URI[autogen.sha256sum] = "2934a2cb868520578fcc2d6ba62217229b2e6eb4d2a8a6267af2ef80bded7f64"
SRC_URI[autogen-libopts.sha256sum] = "726dce002a5cf5ad49db93fa18dfc66863c87a3d92ab716de59d419b9751c745"
