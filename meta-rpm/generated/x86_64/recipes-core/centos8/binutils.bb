SUMMARY = "generated recipe based on binutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
RPM_SONAME_PROV_binutils = "libbfd-2.30-73.el8.so libopcodes-2.30-73.el8.so"
RPM_SONAME_REQ_binutils = "libbfd-2.30-73.el8.so libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libopcodes-2.30-73.el8.so libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_binutils = "bash chkconfig coreutils glibc info libgcc libstdc++ zlib"
RPROVIDES_binutils-devel = "binutils-dev (= 2.30)"
RDEPENDS_binutils-devel = "binutils coreutils info zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/binutils-devel-2.30-73.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/binutils-2.30-73.el8.x86_64.rpm \
          "

SRC_URI[binutils.sha256sum] = "0c7a33d5ff80e698bb36fc63c176e572bd090feebf9fb934f838444fe778b7a5"
SRC_URI[binutils-devel.sha256sum] = "b679dddc74bf3c627f45008c56ab47b823bb032feb1ae97d92bde8f52f933f10"
