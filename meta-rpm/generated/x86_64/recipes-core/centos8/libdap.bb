SUMMARY = "generated recipe based on libdap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl libgcc libtirpc libuuid libxml2 openssl pkgconfig-native"
RPM_SONAME_PROV_libdap = "libdap.so.25 libdapclient.so.6 libdapserver.so.7"
RPM_SONAME_REQ_libdap = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdap.so.25 libdapclient.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libtirpc.so.3 libuuid.so.1 libxml2.so.2"
RDEPENDS_libdap = "glibc libcurl libgcc libstdc++ libtirpc libuuid libxml2 openssl-libs"
RPM_SONAME_REQ_libdap-devel = "libdap.so.25 libdapclient.so.6 libdapserver.so.7"
RPROVIDES_libdap-devel = "libdap-dev (= 3.19.1)"
RDEPENDS_libdap-devel = "automake bash libcurl-devel libdap libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdap-3.19.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdap-devel-3.19.1-2.el8.x86_64.rpm \
          "

SRC_URI[libdap.sha256sum] = "b0c29ad561a4e7a37e45636494a09a8986c3d7f298350f93fa2f6cbaab387d1a"
SRC_URI[libdap-devel.sha256sum] = "d0c8deab0e3e23b541a936e9fc68720f79d713c8b96b13f985415de754c3911e"
