SUMMARY = "generated recipe based on m17n-lib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libthai libxml2 pkgconfig-native xz zlib"
RPM_SONAME_PROV_m17n-lib = "libm17n-core.so.0 libm17n-flt.so.0 libm17n.so.0"
RPM_SONAME_REQ_m17n-lib = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libm17n-core.so.0 libm17n.so.0 libthai.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_m17n-lib = "glibc libthai libxml2 m17n-db xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/m17n-lib-1.8.0-2.el8.x86_64.rpm \
          "

SRC_URI[m17n-lib.sha256sum] = "0fd53aa0be38ff9291b783bf6f84dac0a7cc217f5ce8de1ee372ebb184b685de"
