SUMMARY = "generated recipe based on libvirt-python srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libvirt pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-libvirt = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libvirt-lxc.so.0 libvirt-qemu.so.0 libvirt.so.0"
RDEPENDS_python3-libvirt = "glibc libvirt-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-libvirt-4.5.0-2.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[python3-libvirt.sha256sum] = "ee89430127b2143d820b9bfd37fc85fe2f32c57d00cac4205313d20711885992"
