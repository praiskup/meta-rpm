SUMMARY = "generated recipe based on pulseaudio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib dbus-libs glib-2.0 libasyncns libcap libgcc libice libsm libsndfile2 libtdb libtool libx11 libxcb libxtst orc pkgconfig-native sbc speexdsp systemd-libs webrtc-audio-processing"
RPM_SONAME_PROV_pulseaudio = "libalsa-util.so libcli.so libprotocol-cli.so libprotocol-esound.so libprotocol-http.so libprotocol-native.so libprotocol-simple.so libpulsecore-11.1.so librtp.so libwebrtc-util.so"
RPM_SONAME_REQ_pulseaudio = "ld-linux-x86-64.so.2 libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libalsa-util.so libasound.so.2 libasyncns.so.0 libc.so.6 libcap.so.2 libcli.so libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libltdl.so.7 libm.so.6 liborc-0.4.so.0 libprotocol-cli.so libprotocol-esound.so libprotocol-http.so libprotocol-native.so libprotocol-simple.so libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsecore-11.1.so librt.so.1 librtp.so libsndfile.so.1 libspeexdsp.so.1 libstdc++.so.6 libsystemd.so.0 libtdb.so.1 libudev.so.1 libwebrtc-util.so libwebrtc_audio_processing.so.1 libxcb.so.1"
RDEPENDS_pulseaudio = "alsa-lib bash dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libgcc libsndfile libstdc++ libtdb libtool-ltdl libxcb orc pulseaudio-libs rtkit shadow-utils speexdsp systemd systemd-libs webrtc-audio-processing"
RPM_SONAME_PROV_pulseaudio-libs = "libpulse-simple.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsedsp.so"
RPM_SONAME_REQ_pulseaudio-libs = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libm.so.6 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so librt.so.1 libsndfile.so.1 libsystemd.so.0 libxcb.so.1"
RDEPENDS_pulseaudio-libs = "dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libxcb systemd-libs"
RPM_SONAME_REQ_pulseaudio-libs-devel = "libpulse-mainloop-glib.so.0 libpulse-simple.so.0 libpulse.so.0"
RPROVIDES_pulseaudio-libs-devel = "pulseaudio-libs-dev (= 11.1)"
RDEPENDS_pulseaudio-libs-devel = "glib2-devel pkgconf-pkg-config pulseaudio-libs pulseaudio-libs-glib2"
RPM_SONAME_PROV_pulseaudio-libs-glib2 = "libpulse-mainloop-glib.so.0"
RPM_SONAME_REQ_pulseaudio-libs-glib2 = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libglib-2.0.so.0 libm.so.6 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so librt.so.1 libsndfile.so.1 libsystemd.so.0 libxcb.so.1"
RDEPENDS_pulseaudio-libs-glib2 = "dbus-libs glib2 glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libxcb pulseaudio-libs systemd-libs"
RPM_SONAME_PROV_pulseaudio-module-bluetooth = "libbluez5-util.so"
RPM_SONAME_REQ_pulseaudio-module-bluetooth = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libbluez5-util.so libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libltdl.so.7 libm.so.6 liborc-0.4.so.0 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsecore-11.1.so librt.so.1 libsbc.so.1 libsndfile.so.1 libspeexdsp.so.1 libsystemd.so.0 libtdb.so.1 libxcb.so.1"
RDEPENDS_pulseaudio-module-bluetooth = "bluez dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libtdb libtool-ltdl libxcb orc pulseaudio pulseaudio-libs sbc speexdsp systemd-libs"
RPM_SONAME_REQ_pulseaudio-module-x11 = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libltdl.so.7 libm.so.6 liborc-0.4.so.0 libprotocol-native.so libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so libpulsecore-11.1.so librt.so.1 libsndfile.so.1 libspeexdsp.so.1 libsystemd.so.0 libtdb.so.1 libxcb.so.1"
RDEPENDS_pulseaudio-module-x11 = "bash dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libtdb libtool-ltdl libxcb orc pulseaudio pulseaudio-libs pulseaudio-utils speexdsp systemd-libs"
RPM_SONAME_REQ_pulseaudio-utils = "libICE.so.6 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXtst.so.6 libasyncns.so.0 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libm.so.6 libpthread.so.0 libpulse.so.0 libpulsecommon-11.1.so librt.so.1 libsndfile.so.1 libsystemd.so.0 libxcb.so.1"
RDEPENDS_pulseaudio-utils = "bash dbus-libs glibc libICE libSM libX11 libX11-xcb libXtst libasyncns libcap libsndfile libxcb pulseaudio-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-11.1-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-libs-11.1-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-libs-devel-11.1-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-libs-glib2-11.1-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-module-bluetooth-11.1-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-module-x11-11.1-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pulseaudio-utils-11.1-23.el8.x86_64.rpm \
          "

SRC_URI[pulseaudio.sha256sum] = "a772ed4bb62db386b6bc9a09c4ca6d52843c3996c7da806c9a29f4acc069412b"
SRC_URI[pulseaudio-libs.sha256sum] = "9b3b8bf3239852ed293a8c9ce537360acc2f5011dea5424d42908181dc6a3317"
SRC_URI[pulseaudio-libs-devel.sha256sum] = "bb5b63c66b7e5d85f43dab23d02a9e00d4f87c2b60e92a546a2894513d864b9f"
SRC_URI[pulseaudio-libs-glib2.sha256sum] = "4e555b6a1b35eba5b5b9f2f89713eb47ad92904847abdae9d57ad1cb2953e560"
SRC_URI[pulseaudio-module-bluetooth.sha256sum] = "2fe012e5bd157129f5311801ecea40cd82c8d79f840ca6a309bf667ecfd2c43b"
SRC_URI[pulseaudio-module-x11.sha256sum] = "5bc0a93c74053b35dad0427f23c1440220ed5dd6746fdd024d5f00beb8bfae52"
SRC_URI[pulseaudio-utils.sha256sum] = "bf80013765559ae01cbee93bcf1bcea46c080e5c9bce3d20c249691948bea906"
