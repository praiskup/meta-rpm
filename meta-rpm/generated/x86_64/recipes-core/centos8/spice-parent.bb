SUMMARY = "generated recipe based on spice-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_spice-parent = "forge-parent java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/spice-parent-26-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[spice-parent.sha256sum] = "395cf70431e6024c6116363468addc5ef024b9bcbfcae3fa4a3adb3b068bf076"
