SUMMARY = "generated recipe based on hunspell-eo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-eo = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-eo-1.0-0.15.dev.el8.noarch.rpm \
          "

SRC_URI[hunspell-eo.sha256sum] = "03fe42828fa9e5ab63fb24704cea27dae115c328c09d91a8cbbc9e63730fbf7b"
