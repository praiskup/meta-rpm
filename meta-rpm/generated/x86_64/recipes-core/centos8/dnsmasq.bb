SUMMARY = "generated recipe based on dnsmasq srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "dbus-libs gmp libidn2 nettle pkgconfig-native"
RPM_SONAME_REQ_dnsmasq = "libc.so.6 libdbus-1.so.3 libgmp.so.10 libhogweed.so.4 libidn2.so.0 libnettle.so.6"
RDEPENDS_dnsmasq = "bash dbus-libs glibc gmp libidn2 nettle systemd"
RPM_SONAME_REQ_dnsmasq-utils = "libc.so.6"
RDEPENDS_dnsmasq-utils = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dnsmasq-2.79-11.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dnsmasq-utils-2.79-11.el8_2.1.x86_64.rpm \
          "

SRC_URI[dnsmasq.sha256sum] = "c01e8b3940322fb9783e778eb4d3db1b8ff54fab001826b8e4746e0771edb991"
SRC_URI[dnsmasq-utils.sha256sum] = "8f5d856c5a4cabda2c4faa35bdbe1838e08dc457cecbb35e18cc4407e51cc10d"
