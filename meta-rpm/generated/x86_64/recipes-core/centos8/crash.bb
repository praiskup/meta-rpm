SUMMARY = "generated recipe based on crash srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "lzo ncurses pkgconfig-native snappy zlib"
RPM_SONAME_REQ_crash = "libc.so.6 libdl.so.2 liblzo2.so.2 libm.so.6 libncurses.so.6 libsnappy.so.1 libtinfo.so.6 libz.so.1"
RDEPENDS_crash = "binutils glibc lzo ncurses-libs snappy zlib"
RPROVIDES_crash-devel = "crash-dev (= 7.2.7)"
RDEPENDS_crash-devel = "crash lzo-devel snappy-devel zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/crash-7.2.7-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/crash-devel-7.2.7-3.el8.x86_64.rpm \
          "

SRC_URI[crash.sha256sum] = "a797ceb8704935349eaa79b006f8ecb69b1360d87f0bd997b0717f4856ce4916"
SRC_URI[crash-devel.sha256sum] = "3d3741ab76a72f4b3ae3e505987a1d67bd2b07e1d43d5c531040abf74ef6dd07"
