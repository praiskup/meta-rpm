SUMMARY = "generated recipe based on alsa-firmware srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_alsa-firmware = "alsa-tools-firmware systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-firmware-1.0.29-6.el8.noarch.rpm \
          "

SRC_URI[alsa-firmware.sha256sum] = "0f8996e38fa0ffbcb8e0feccf67065cb42f2676e6397987a899cbcdcd7b8ab41"
