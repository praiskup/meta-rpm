SUMMARY = "generated recipe based on eigen3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPROVIDES_eigen3-devel = "eigen3-dev (= 3.3.4)"
RDEPENDS_eigen3-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/eigen3-devel-3.3.4-6.el8.noarch.rpm \
          "

SRC_URI[eigen3-devel.sha256sum] = "b32230c380931ee3580cf877c608cada7fc9cfda234ceed90e2c3f377e232daf"
