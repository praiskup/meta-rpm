SUMMARY = "generated recipe based on mokutil srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "efivar libxcrypt openssl pkgconfig-native"
RPM_SONAME_REQ_mokutil = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libefivar.so.1 libssl.so.1.1"
RDEPENDS_mokutil = "efivar-libs glibc libxcrypt openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mokutil-0.3.0-9.el8.x86_64.rpm \
          "

SRC_URI[mokutil.sha256sum] = "336eedb5d56f5cce3e1bb04c4319360829b6fc29db83e68e913cffe940b79304"
