SUMMARY = "generated recipe based on qt5 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_qt5-rpm-macros = "bash cmake gcc-c++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-rpm-macros-5.12.5-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-srpm-macros-5.12.5-3.el8.noarch.rpm \
          "

SRC_URI[qt5-rpm-macros.sha256sum] = "60d703a7ba3e65660c63eeeb9c9f12666f506c6d0703879d2a2dff3c62412b1f"
SRC_URI[qt5-srpm-macros.sha256sum] = "bd7d4cc40a56d9dc00308bdb093f540fe344ba1b215abfb0c7613f16ee379d13"
