SUMMARY = "generated recipe based on libusbx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libusbx = "libusb-1.0.so.0"
RPM_SONAME_REQ_libusbx = "libc.so.6 libpthread.so.0 libudev.so.1"
RDEPENDS_libusbx = "glibc systemd-libs"
RPM_SONAME_REQ_libusbx-devel = "libusb-1.0.so.0"
RPROVIDES_libusbx-devel = "libusbx-dev (= 1.0.22)"
RDEPENDS_libusbx-devel = "libusbx pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libusbx-1.0.22-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libusbx-devel-1.0.22-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libusbx-devel-doc-1.0.22-1.el8.noarch.rpm \
          "

SRC_URI[libusbx.sha256sum] = "8ff5069483db979abf138129983c15bc145239823dcd31c0ac81aa84bbfb61e4"
SRC_URI[libusbx-devel.sha256sum] = "b150cd6abf03569dfeca45fad679ac64449a4eee0c8c978296fe5eb84bd4afef"
SRC_URI[libusbx-devel-doc.sha256sum] = "e3a9b98b692c8346bf5a0427bb2deecf7b634908e411b0d4242cdeba5b94c5f5"
