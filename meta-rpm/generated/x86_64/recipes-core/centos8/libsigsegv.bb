SUMMARY = "generated recipe based on libsigsegv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsigsegv = "libsigsegv.so.2"
RPM_SONAME_REQ_libsigsegv = "libc.so.6"
RDEPENDS_libsigsegv = "glibc"
RPM_SONAME_REQ_libsigsegv-devel = "libsigsegv.so.2"
RPROVIDES_libsigsegv-devel = "libsigsegv-dev (= 2.11)"
RDEPENDS_libsigsegv-devel = "libsigsegv"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsigsegv-2.11-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libsigsegv-devel-2.11-5.el8.x86_64.rpm \
          "

SRC_URI[libsigsegv.sha256sum] = "02d728cf74eb47005babeeab5ac68ca04472c643203a1faef0037b5f33710fe2"
SRC_URI[libsigsegv-devel.sha256sum] = "513f61f4d17be84585748c2de8aa9f11809e7a87105e3dcf68360c83bc6526f2"
