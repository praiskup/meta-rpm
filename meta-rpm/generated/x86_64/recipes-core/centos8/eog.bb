SUMMARY = "generated recipe based on eog srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo exempi gdk-pixbuf glib-2.0 gnome-desktop3 gobject-introspection gtk+3 lcms2 libexif libjpeg-turbo libpeas librsvg libx11 pkgconfig-native zlib"
RPM_SONAME_PROV_eog = "libeog.so libfullscreen.so libreload.so libstatusbar-date.so"
RPM_SONAME_REQ_eog = "libX11.so.6 libc.so.6 libcairo.so.2 libeog.so libexempi.so.3 libexif.so.12 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0 librsvg-2.so.2 libz.so.1"
RDEPENDS_eog = "cairo exempi gdk-pixbuf2 glib2 glibc gnome-desktop3 gobject-introspection gsettings-desktop-schemas gtk3 lcms2 libX11 libexif libjpeg-turbo libpeas libpeas-gtk librsvg2 zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/eog-3.28.4-1.el8.x86_64.rpm \
          "

SRC_URI[eog.sha256sum] = "58808e638a8d282d91bf6c5d33f8a3839735f970209c06e82887319b50c48a53"
