SUMMARY = "generated recipe based on plotutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc libice libpng libsm libx11 libxaw libxext libxmu libxt pkgconfig-native zlib"
RPM_SONAME_PROV_plotutils = "libplot.so.2 libplotter.so.2 libxmi.so.0"
RPM_SONAME_REQ_plotutils = "libICE.so.6 libSM.so.6 libX11.so.6 libXaw.so.7 libXext.so.6 libXmu.so.6 libXt.so.6 libc.so.6 libgcc_s.so.1 libm.so.6 libplot.so.2 libpng16.so.16 libstdc++.so.6 libz.so.1"
RDEPENDS_plotutils = "bash glibc info libICE libSM libX11 libXaw libXext libXmu libXt libgcc libpng libstdc++ zlib"
RPM_SONAME_REQ_plotutils-devel = "libplot.so.2 libplotter.so.2 libxmi.so.0"
RPROVIDES_plotutils-devel = "plotutils-dev (= 2.6)"
RDEPENDS_plotutils-devel = "plotutils"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plotutils-2.6-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plotutils-devel-2.6-20.el8.x86_64.rpm \
          "

SRC_URI[plotutils.sha256sum] = "26992d58950470386c7e73149c9bb663158222cffd186177b4e8a921f03d47a0"
SRC_URI[plotutils-devel.sha256sum] = "7a051734c7f2e98a8f21d81aefb3023c4ceabf0c082b896febe310b126e41ed9"
