SUMMARY = "generated recipe based on raptor2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl icu libxml2 libxslt pkgconfig-native xz zlib"
RPM_SONAME_PROV_raptor2 = "libraptor2.so.0"
RPM_SONAME_REQ_raptor2 = "libc.so.6 libcurl.so.4 libdl.so.2 libicuuc.so.60 liblzma.so.5 libm.so.6 libraptor2.so.0 libxml2.so.2 libxslt.so.1 libz.so.1"
RDEPENDS_raptor2 = "glibc libcurl libicu libxml2 libxslt xz-libs zlib"
RPM_SONAME_REQ_raptor2-devel = "libraptor2.so.0"
RPROVIDES_raptor2-devel = "raptor2-dev (= 2.0.15)"
RDEPENDS_raptor2-devel = "pkgconf-pkg-config raptor2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/raptor2-2.0.15-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/raptor2-devel-2.0.15-13.el8.x86_64.rpm \
          "

SRC_URI[raptor2.sha256sum] = "1ae52863d0ae0d978e74bda60d776636782db52f3646abfc797cab014f7edcac"
SRC_URI[raptor2-devel.sha256sum] = "f6b5d45a471a47098943463b462eecafd23be706d9698cea47cc54c6e9d2ffe4"
