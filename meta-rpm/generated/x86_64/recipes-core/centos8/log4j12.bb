SUMMARY = "generated recipe based on log4j12 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_log4j12 = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_log4j12-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/log4j12-1.2.17-22.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/log4j12-javadoc-1.2.17-22.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[log4j12.sha256sum] = "34ebda8999378740f59c39f23505806825db68d276879ca5e05c3b492349af31"
SRC_URI[log4j12-javadoc.sha256sum] = "be25e22177b9be059656be81d84c0462a9d1c68c109c83cb4ae0a07cf638e035"
