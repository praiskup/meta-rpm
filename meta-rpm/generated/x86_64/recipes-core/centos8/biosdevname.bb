SUMMARY = "generated recipe based on biosdevname srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pciutils pkgconfig-native zlib"
RPM_SONAME_REQ_biosdevname = "libc.so.6 libpci.so.3 libz.so.1"
RDEPENDS_biosdevname = "glibc pciutils-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/biosdevname-0.7.3-2.el8.x86_64.rpm \
          "

SRC_URI[biosdevname.sha256sum] = "0caa72888379f82fae1bc8f448bcf0dbaead20e92f2ef4c714afadf8979c3181"
