SUMMARY = "generated recipe based on hunspell-sc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sc = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-sc-0.20081101-19.el8.noarch.rpm \
          "

SRC_URI[hunspell-sc.sha256sum] = "d84fbd5e8742a74f6ab4c8e7ac99342cb179374fea7ae1750ae77c7cee41f43f"
