SUMMARY = "generated recipe based on vulkan-loader srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_vulkan-loader = "libvulkan.so.1"
RPM_SONAME_REQ_vulkan-loader = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_vulkan-loader = "glibc mesa-vulkan-drivers"
RPM_SONAME_REQ_vulkan-loader-devel = "libvulkan.so.1"
RPROVIDES_vulkan-loader-devel = "vulkan-loader-dev (= 1.2.135.0)"
RDEPENDS_vulkan-loader-devel = "pkgconf-pkg-config vulkan-headers vulkan-loader"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vulkan-loader-1.2.135.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vulkan-loader-devel-1.2.135.0-1.el8.x86_64.rpm \
          "

SRC_URI[vulkan-loader.sha256sum] = "dd8a843ba9c03db9f67aea048e18945a2523465cbf29fe2c38ae297a17824626"
SRC_URI[vulkan-loader-devel.sha256sum] = "ad2e171ec07f9150df82a69b1b6efbfaaff79364dc064edbdc88ff8c6bf13de6"
