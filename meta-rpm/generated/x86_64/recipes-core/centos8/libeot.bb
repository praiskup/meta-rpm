SUMMARY = "generated recipe based on libeot srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libeot = "libeot.so.0"
RPM_SONAME_REQ_libeot = "libc.so.6"
RDEPENDS_libeot = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libeot-0.01-9.el8.x86_64.rpm \
          "

SRC_URI[libeot.sha256sum] = "aa3c5d81012c389696fd15dc560a4b7c488c6a784f36721e0f4f0a8ae3581b03"
