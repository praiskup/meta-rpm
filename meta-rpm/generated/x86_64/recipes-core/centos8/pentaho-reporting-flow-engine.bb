SUMMARY = "generated recipe based on pentaho-reporting-flow-engine srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_pentaho-reporting-flow-engine = "flute java-1.8.0-openjdk-headless javapackages-tools libbase libfonts libformula liblayout librepository libserializer pentaho-libxml sac"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pentaho-reporting-flow-engine-0.9.4-15.el8.noarch.rpm \
          "

SRC_URI[pentaho-reporting-flow-engine.sha256sum] = "a0b046db7795fe10770b9cf17d352a3c6e9493fa4cb40c76d3546b3f9059ee35"
