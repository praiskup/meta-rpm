SUMMARY = "generated recipe based on xorg-x11-drv-fbdev srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-fbdev = "libc.so.6"
RDEPENDS_xorg-x11-drv-fbdev = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-fbdev-0.5.0-2.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-fbdev.sha256sum] = "bffb6eb311a2e0c5e043c6432b2df4e8302bb23ceb1a0c6eea15ebedbcf0f813"
