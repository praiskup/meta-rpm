SUMMARY = "generated recipe based on cryptsetup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs json-c libblkid libuuid openssl pkgconfig-native"
RPM_SONAME_PROV_cryptsetup-libs = "libcryptsetup.so.12"
RPM_SONAME_REQ_cryptsetup-libs = "libblkid.so.1 libc.so.6 libcrypto.so.1.1 libdevmapper.so.1.02 libjson-c.so.4 libpthread.so.0 libssl.so.1.1 libuuid.so.1"
RDEPENDS_cryptsetup-libs = "device-mapper-libs glibc json-c libblkid libuuid openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cryptsetup-libs-2.2.2-1.el8.x86_64.rpm \
          "

SRC_URI[cryptsetup-libs.sha256sum] = "4bfaee4b6ee6fb74807fe113fbc1527d2e1fe97a3ddf47b3e6b3c4cf9699683b"
