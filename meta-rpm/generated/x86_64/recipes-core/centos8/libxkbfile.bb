SUMMARY = "generated recipe based on libxkbfile srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libxkbfile = "libxkbfile.so.1"
RPM_SONAME_REQ_libxkbfile = "libX11.so.6 libc.so.6"
RDEPENDS_libxkbfile = "glibc libX11"
RPM_SONAME_REQ_libxkbfile-devel = "libxkbfile.so.1"
RPROVIDES_libxkbfile-devel = "libxkbfile-dev (= 1.0.9)"
RDEPENDS_libxkbfile-devel = "libX11-devel libxkbfile pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxkbfile-1.0.9-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libxkbfile-devel-1.0.9-9.el8.x86_64.rpm \
          "

SRC_URI[libxkbfile.sha256sum] = "757527e8548109a9a7052bcf52b37449dba676c5fc7fbf5c8e44cf728dfda64d"
SRC_URI[libxkbfile-devel.sha256sum] = "07a79ec49ba792b0c881b1be22a7b3be85b05a160deee0094bc75f8b5bf81ad1"
