SUMMARY = "generated recipe based on rubygem-bson srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native ruby"
RPM_SONAME_REQ_rubygem-bson = "libc.so.6 libruby.so.2.5"
RDEPENDS_rubygem-bson = "glibc ruby-libs rubygem-bigdecimal rubygems"
RDEPENDS_rubygem-bson-doc = "rubygem-bson"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-bson-4.3.0-2.module_el8.1.0+214+9be47fd7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rubygem-bson-doc-4.3.0-2.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-bson.sha256sum] = "a737ffa9c59bb0cb7ef8c917bec995b311950002f1035ef69cc804408714838d"
SRC_URI[rubygem-bson-doc.sha256sum] = "61225ffccd81a172428af5c13495c64e2484e9d88109e52eea82b625750c54ec"
