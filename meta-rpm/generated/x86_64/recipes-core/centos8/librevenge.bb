SUMMARY = "generated recipe based on librevenge srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_librevenge = "librevenge-0.0.so.0 librevenge-generators-0.0.so.0 librevenge-stream-0.0.so.0"
RPM_SONAME_REQ_librevenge = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_librevenge = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_librevenge-devel = "librevenge-0.0.so.0 librevenge-generators-0.0.so.0 librevenge-stream-0.0.so.0"
RPROVIDES_librevenge-devel = "librevenge-dev (= 0.0.4)"
RDEPENDS_librevenge-devel = "librevenge pkgconf-pkg-config"
RDEPENDS_librevenge-gdb = "librevenge python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librevenge-0.0.4-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librevenge-gdb-0.0.4-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/librevenge-devel-0.0.4-12.el8.x86_64.rpm \
          "

SRC_URI[librevenge.sha256sum] = "f6c387006b5a4996aadb18268435de60f595c2ee884d0e9a3d0579ee04cc4969"
SRC_URI[librevenge-devel.sha256sum] = "bfba5170c259710cb768fc1bcc4ad96beff6ee12d00ad6cadaff95464604ab7b"
SRC_URI[librevenge-gdb.sha256sum] = "0f84975e52c5812cd8b245442764d452ebf807ab5553d20af40d1ed0066e4d0f"
