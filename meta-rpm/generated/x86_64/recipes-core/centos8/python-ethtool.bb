SUMMARY = "generated recipe based on python-ethtool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libnl pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-ethtool = "libc.so.6 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-ethtool = "glibc libnl3 platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-ethtool-0.14-3.el8.x86_64.rpm \
          "

SRC_URI[python3-ethtool.sha256sum] = "c746d43bd63dd184b8c585dd7323e261567928e521e830720d4754fca76157e0"
