SUMMARY = "generated recipe based on perl-DynaLoader-Functions srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DynaLoader-Functions = "perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DynaLoader-Functions-0.003-2.el8.noarch.rpm \
          "

SRC_URI[perl-DynaLoader-Functions.sha256sum] = "343af8f87e4cef0158ef71760aecbaef385ee9eae324ba679f1e7347698abff0"
