SUMMARY = "generated recipe based on libcap-ng srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libcap-ng = "libcap-ng.so.0"
RPM_SONAME_REQ_libcap-ng = "ld-linux-x86-64.so.2 libc.so.6"
RDEPENDS_libcap-ng = "glibc"
RPM_SONAME_REQ_libcap-ng-devel = "libcap-ng.so.0"
RPROVIDES_libcap-ng-devel = "libcap-ng-dev (= 0.7.9)"
RDEPENDS_libcap-ng-devel = "kernel-headers libcap-ng pkgconf-pkg-config"
RPM_SONAME_REQ_libcap-ng-utils = "libc.so.6 libcap-ng.so.0"
RDEPENDS_libcap-ng-utils = "glibc libcap-ng"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcap-ng-0.7.9-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcap-ng-devel-0.7.9-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcap-ng-utils-0.7.9-5.el8.x86_64.rpm \
          "

SRC_URI[libcap-ng.sha256sum] = "bc449afb5b82f36c4b9a03343b83c09ed76308bcc1adc285a62f6aab39774af4"
SRC_URI[libcap-ng-devel.sha256sum] = "e7877dfba769182adf40df3bf57d04c8bcef58797a95c9def318517b8f017053"
SRC_URI[libcap-ng-utils.sha256sum] = "97b71ee551259ee6ed473879234772030c1109854dd6aa53f95b9b3a3278ed9a"
