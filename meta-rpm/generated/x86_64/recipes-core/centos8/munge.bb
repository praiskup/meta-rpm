SUMMARY = "generated recipe based on munge srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "bzip2 libgcc libgcrypt libgpg-error pkgconfig-native zlib"
RPM_SONAME_REQ_munge = "libbz2.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgpg-error.so.0 libmunge.so.2 libpthread.so.0 libz.so.1"
RDEPENDS_munge = "bash bzip2-libs glibc libgcc libgcrypt libgpg-error munge-libs shadow-utils systemd zlib"
RPM_SONAME_REQ_munge-devel = "libmunge.so.2"
RPROVIDES_munge-devel = "munge-dev (= 0.5.13)"
RDEPENDS_munge-devel = "munge-libs pkgconf-pkg-config"
RPM_SONAME_PROV_munge-libs = "libmunge.so.2"
RPM_SONAME_REQ_munge-libs = "libc.so.6"
RDEPENDS_munge-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/munge-0.5.13-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/munge-libs-0.5.13-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/munge-devel-0.5.13-1.el8.x86_64.rpm \
          "

SRC_URI[munge.sha256sum] = "d71d8e3c1fff17d507edd0b7e7aea4183f86bd1f603b232e95c8a3c838a57a2c"
SRC_URI[munge-devel.sha256sum] = "b18acefc832c7c6a3badd01d55757e15683b93be9ee5951af5bf423e05af1b83"
SRC_URI[munge-libs.sha256sum] = "858379cf0ee9af56d574c6a9359ac7cc359c9f0874ac42869f84e23f45a3b18c"
