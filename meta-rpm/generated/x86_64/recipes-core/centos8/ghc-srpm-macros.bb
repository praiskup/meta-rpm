SUMMARY = "generated recipe based on ghc-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ghc-srpm-macros-1.4.2-7.el8.noarch.rpm \
          "

SRC_URI[ghc-srpm-macros.sha256sum] = "6f6b7880f614617e77ebf16b40ce9c86c1481c23c51b6dcec2eeda37d2c04c96"
