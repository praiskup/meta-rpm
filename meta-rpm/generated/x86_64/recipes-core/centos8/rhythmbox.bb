SUMMARY = "generated recipe based on rhythmbox srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk brasero cairo gdk-pixbuf glib-2.0 gobject-introspection grilo gstreamer1.0 gstreamer1.0-plugins-base gtk+3 json-glib libdmapsharing libgpod libgudev libmtp libnotify libpeas libsecret libsoup-2.4 libtdb libx11 libxml2 pango pkgconfig-native totem-pl-parser zlib"
RPM_SONAME_PROV_rhythmbox = "libandroid.so libaudiocd.so libaudioscrobbler.so libcd-recorder.so libdaap.so libdbus-media-server.so libfmradio.so libgeneric-player.so libgrilo.so libipod.so libiradio.so libmmkeys.so libmpris.so libmtpdevice.so libnotification.so libpower-manager.so librhythmbox-core.so.10 librhythmbox-itms-detection-plugin.so libsample.so"
RPM_SONAME_REQ_rhythmbox = "libX11.so.6 libatk-1.0.so.0 libbrasero-media3.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdmapsharing-3.0.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpod.so.4 libgrilo-0.3.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstcontroller-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgudev-1.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libmtp.so.9 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0 librhythmbox-core.so.10 libsecret-1.so.0 libsoup-2.4.so.1 libtdb.so.1 libtotem-plparser.so.18 libxml2.so.2 libz.so.1"
RDEPENDS_rhythmbox = "atk brasero-libs cairo cairo-gobject gdk-pixbuf2 glib2 glibc gobject-introspection grilo gstreamer1 gstreamer1-plugins-base gstreamer1-plugins-good gtk3 gvfs-afc json-glib libX11 libdmapsharing libgpod libgudev libmtp libnotify libpeas libpeas-gtk libpeas-loader-python3 libsecret libsoup libtdb libxml2 media-player-info pango python3-gobject python3-mako totem-pl-parser zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rhythmbox-3.4.2-8.el8.x86_64.rpm \
          "

SRC_URI[rhythmbox.sha256sum] = "67f6286ab5d467ca3dd3b53e25fa9f3fddaceecf48aee4cc17fba591e22b4a95"
