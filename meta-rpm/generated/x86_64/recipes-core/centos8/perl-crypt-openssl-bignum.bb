SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-Bignum srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-Bignum = "libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Crypt-OpenSSL-Bignum = "glibc openssl-libs perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Crypt-OpenSSL-Bignum-0.09-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-Bignum.sha256sum] = "133d4a7db450be596504a81605546d81badcd35c4975bd54dab675a184d738a6"
