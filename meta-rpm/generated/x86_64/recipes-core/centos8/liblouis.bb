SUMMARY = "generated recipe based on liblouis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_liblouis = "liblouis.so.2"
RPM_SONAME_REQ_liblouis = "libc.so.6"
RDEPENDS_liblouis = "bash glibc info"
RDEPENDS_python3-louis = "liblouis platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblouis-2.6.2-21.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-louis-2.6.2-21.el8.noarch.rpm \
          "

SRC_URI[liblouis.sha256sum] = "1559cded7c6d6af2a997652aace355a5e11bb1050d285627e61590f35f0e1b88"
SRC_URI[python3-louis.sha256sum] = "3708d5c74a26e71fad43cffba2be1f3482a924148f75c4ec1ef6a8c607953ea7"
