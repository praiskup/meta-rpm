SUMMARY = "generated recipe based on usbguard srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "audit dbus-glib dbus-libs glib-2.0 libgcc libgcrypt libgpg-error libqb pkgconfig-native protobuf"
RPM_SONAME_PROV_usbguard = "libusbguard.so.0"
RPM_SONAME_REQ_usbguard = "ld-linux-x86-64.so.2 libaudit.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libprotobuf.so.15 libpthread.so.0 libqb.so.0 libstdc++.so.6 libusbguard.so.0"
RDEPENDS_usbguard = "audit-libs bash glibc libgcc libgcrypt libgpg-error libqb libstdc++ protobuf systemd"
RPM_SONAME_REQ_usbguard-dbus = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libm.so.6 libprotobuf.so.15 libpthread.so.0 libqb.so.0 libstdc++.so.6 libusbguard.so.0"
RDEPENDS_usbguard-dbus = "bash dbus dbus-glib dbus-libs glib2 glibc libgcc libgcrypt libgpg-error libqb libstdc++ polkit protobuf usbguard"
RPM_SONAME_REQ_usbguard-tools = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libprotobuf.so.15 libpthread.so.0 libqb.so.0 libstdc++.so.6 libusbguard.so.0"
RDEPENDS_usbguard-tools = "glibc libgcc libgcrypt libgpg-error libqb libstdc++ protobuf usbguard"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/usbguard-0.7.4-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/usbguard-dbus-0.7.4-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/usbguard-tools-0.7.4-4.el8.x86_64.rpm \
          "

SRC_URI[usbguard.sha256sum] = "2671de15546b7bbe4c2120664399a8506adf00ed153ba06811100b1d984c7edc"
SRC_URI[usbguard-dbus.sha256sum] = "fa12f19088d5e2c6872cfa7cfab079ef217ef3a55b3307c588998ead868f5480"
SRC_URI[usbguard-tools.sha256sum] = "f8238494a7b1a084b4a6944618ffe950fd51709f121c60d4ae2ad9ac54d3e11f"
