SUMMARY = "generated recipe based on os-maven-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_os-maven-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib plexus-utils"
RDEPENDS_os-maven-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/os-maven-plugin-1.2.3-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/os-maven-plugin-javadoc-1.2.3-9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[os-maven-plugin.sha256sum] = "a9237f9a678d24b47ccbb0461a0b888e6d8029d129564ef59714c6c3ab9fba74"
SRC_URI[os-maven-plugin-javadoc.sha256sum] = "a4d45a56615b384c6dd1eb46d046aaf000c34b30da266469ec759ecc0ddc1b2a"
