SUMMARY = "generated recipe based on regexp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_regexp = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_regexp-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/regexp-1.5-26.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/regexp-javadoc-1.5-26.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[regexp.sha256sum] = "deb5a3ffcbc76fc2c1ed0769a8d41af72b6ce2fa80d1e98a524193928e9b6d68"
SRC_URI[regexp-javadoc.sha256sum] = "39a039bee719705b7ede21665fdbe82775550586c44c181c31ee0016af7526e4"
