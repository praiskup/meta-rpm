SUMMARY = "generated recipe based on libvpx srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libvpx = "libvpx.so.5"
RPM_SONAME_REQ_libvpx = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libvpx = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libvpx-devel = "libvpx.so.5"
RPROVIDES_libvpx-devel = "libvpx-dev (= 1.7.0)"
RDEPENDS_libvpx-devel = "libvpx pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvpx-1.7.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvpx-devel-1.7.0-6.el8.x86_64.rpm \
          "

SRC_URI[libvpx.sha256sum] = "98fda8270fe96d6ba0c7ac82d9e9fbd8c71318f703190643fc915ab391000569"
SRC_URI[libvpx-devel.sha256sum] = "1c8c82a11f900481185930352f508fc294957efba7381be7855aa4ac5761137a"
