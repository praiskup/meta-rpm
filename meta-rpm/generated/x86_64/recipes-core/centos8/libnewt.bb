SUMMARY = "generated recipe based on newt srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native platform-python3 popt slang"
RPM_SONAME_PROV_newt = "libnewt.so.0.52"
RPM_SONAME_REQ_newt = "libc.so.6 libnewt.so.0.52 libpopt.so.0 libslang.so.2"
RDEPENDS_newt = "glibc popt slang"
RPM_SONAME_REQ_newt-devel = "libnewt.so.0.52"
RPROVIDES_newt-devel = "newt-dev (= 0.52.20)"
RDEPENDS_newt-devel = "newt pkgconf-pkg-config slang-devel"
RPM_SONAME_REQ_python3-newt = "libc.so.6 libdl.so.2 libm.so.6 libnewt.so.0.52 libpthread.so.0 libpython3.6m.so.1.0 libslang.so.2 libutil.so.1"
RDEPENDS_python3-newt = "glibc newt platform-python python3-libs slang"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/newt-devel-0.52.20-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-newt-0.52.20-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/newt-0.52.20-11.el8.x86_64.rpm \
          "

SRC_URI[newt.sha256sum] = "c9df7c58da59500e1717e435c565e221daa344212db8e83eb33b6a9c8e9a67bb"
SRC_URI[newt-devel.sha256sum] = "e3245b5fd9b67d8a881b6a042afc77c501259d2b7038d7cd99c465e35ba505a1"
SRC_URI[python3-newt.sha256sum] = "5f2c2db957f9759a33b611a90d6801f6dee12f4552ccc0c6f096bc371fc701be"
