SUMMARY = "generated recipe based on numad srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_numad = "libc.so.6 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_numad = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/numad-0.5-26.20150602git.el8.x86_64.rpm \
          "

SRC_URI[numad.sha256sum] = "5d975c08273b1629683275c32f16e52ca8e37e6836598e211092c915d38878bf"
