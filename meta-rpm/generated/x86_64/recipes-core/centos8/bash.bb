SUMMARY = "generated recipe based on bash srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_bash = "libc.so.6 libdl.so.2 libtinfo.so.6"
RDEPENDS_bash = "filesystem glibc ncurses-libs"
RDEPENDS_bash-doc = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bash-4.4.19-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/bash-doc-4.4.19-10.el8.x86_64.rpm \
          "

SRC_URI[bash.sha256sum] = "f07777300c4827aa8c2145aa78dd0f962348ba9f4cfa27baaf6e9f1a1ca0282a"
SRC_URI[bash-doc.sha256sum] = "3ac31560e8535cb0f2624af958d88ec9dbc3eb1bfa90cac904481ee43fd4fbad"
