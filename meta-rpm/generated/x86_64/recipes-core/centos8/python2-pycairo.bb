SUMMARY = "generated recipe based on python2-pycairo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "cairo pkgconfig-native"
RPM_SONAME_REQ_python2-cairo = "libc.so.6 libcairo.so.2 libpthread.so.0 libpython2.7.so.1.0"
RDEPENDS_python2-cairo = "cairo glibc platform-python"
RPROVIDES_python2-cairo-devel = "python2-cairo-dev (= 1.16.3)"
RDEPENDS_python2-cairo-devel = "cairo-devel pkgconf-pkg-config python2-cairo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python2-cairo-1.16.3-6.module_el8.0.0+36+bb6a76a2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python2-cairo-devel-1.16.3-6.module_el8.0.0+36+bb6a76a2.x86_64.rpm \
          "

SRC_URI[python2-cairo.sha256sum] = "ee7d07d251c34b6bbe48c614633cb6a238d6f7a53653486dc93cba9e65f0bcab"
SRC_URI[python2-cairo-devel.sha256sum] = "80a86ea02828be325494fee9f5e8eecca39ec0f0fee686d70622612dd03d19ec"
