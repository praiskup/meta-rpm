SUMMARY = "generated recipe based on libbytesize srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp libpcre mpfr pkgconfig-native"
RPM_SONAME_PROV_libbytesize = "libbytesize.so.1"
RPM_SONAME_REQ_libbytesize = "libc.so.6 libgmp.so.10 libmpfr.so.4 libpcre.so.1"
RDEPENDS_libbytesize = "glibc gmp mpfr pcre"
RDEPENDS_python3-bytesize = "libbytesize platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libbytesize-1.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-bytesize-1.4-3.el8.x86_64.rpm \
          "

SRC_URI[libbytesize.sha256sum] = "d1cc79976965be3fe769cb8c23a281064816eaa195caf6057b8d1da0e9ff7ae5"
SRC_URI[python3-bytesize.sha256sum] = "1135f4474177dd80cbc039b431924e14198433e778a96c24e86abcda59bd9d6b"
