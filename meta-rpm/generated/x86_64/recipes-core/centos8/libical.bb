SUMMARY = "generated recipe based on libical srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "icu libgcc pkgconfig-native"
RPM_SONAME_PROV_libical = "libical.so.3 libical_cxx.so.3 libicalss.so.3 libicalss_cxx.so.3 libicalvcal.so.3"
RPM_SONAME_REQ_libical = "libc.so.6 libgcc_s.so.1 libical.so.3 libical_cxx.so.3 libicalss.so.3 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libical = "glibc libgcc libicu libstdc++ tzdata"
RPM_SONAME_REQ_libical-devel = "libical.so.3 libical_cxx.so.3 libicalss.so.3 libicalss_cxx.so.3 libicalvcal.so.3"
RPROVIDES_libical-devel = "libical-dev (= 3.0.3)"
RDEPENDS_libical-devel = "cmake-filesystem libical libicu-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libical-devel-3.0.3-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libical-3.0.3-3.el8.x86_64.rpm \
          "

SRC_URI[libical.sha256sum] = "d2fd188b589c4db13cd9447fe6c801560ea458e6ceb3ffdcd981fcfc12c08992"
SRC_URI[libical-devel.sha256sum] = "70d2e1edd77743dc89fa97fd428559ace3447c47953f2aeb032201d71fa7de4f"
