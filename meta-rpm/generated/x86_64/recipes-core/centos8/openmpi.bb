SUMMARY = "generated recipe based on openmpi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gcc hwloc libevent libfabric libgcc libpsm2 opensm pkgconfig-native pmix rdma-core torque ucx zlib"
RPM_SONAME_PROV_openmpi = "libmpi.so.40 libmpi_cxx.so.40 libmpi_java.so.40 libmpi_mpifh.so.40 libmpi_usempi_ignore_tkr.so.40 libmpi_usempif08.so.40 liboshmem.so.40"
RPM_SONAME_REQ_openmpi = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libfabric.so.1 libgcc_s.so.1 libgfortran.so.5 libhwloc.so.5 libibverbs.so.1 libm.so.6 libmpi.so.40 libmpi_mpifh.so.40 liboshmem.so.40 libosmcomp.so.5 libpmix.so.2 libpsm2.so.2 libpthread.so.0 libquadmath.so.0 librdmacm.so.1 librt.so.1 libstdc++.so.6 libtorque.so.2 libucm.so.0 libucp.so.0 libucs.so.0 libuct.so.0 libutil.so.1 libz.so.1"
RDEPENDS_openmpi = "environment-modules glibc hwloc-libs libevent libfabric libgcc libgfortran libibverbs libpsm2 libquadmath librdmacm libstdc++ opensm-libs openssh-clients perl-interpreter perl-libs pmix torque-libs ucx zlib"
RPM_SONAME_REQ_openmpi-devel = "libc.so.6 libdl.so.2 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libhwloc.so.5 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libmpi_java.so.40 libmpi_mpifh.so.40 libmpi_usempi_ignore_tkr.so.40 libmpi_usempif08.so.40 liboshmem.so.40 libpthread.so.0 librt.so.1 libutil.so.1 libz.so.1"
RPROVIDES_openmpi-devel = "openmpi-dev (= 4.0.2)"
RDEPENDS_openmpi-devel = "gcc-gfortran glibc hwloc-libs libevent openmpi pkgconf-pkg-config rpm-mpi-hooks zlib"
RDEPENDS_python3-openmpi = "openmpi platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openmpi-4.0.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openmpi-devel-4.0.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-openmpi-4.0.2-2.el8.x86_64.rpm \
          "

SRC_URI[openmpi.sha256sum] = "1deb0c27fe5d88ae54f965235ac0009711f6730a5d9e26adcf7b4312c3f587f1"
SRC_URI[openmpi-devel.sha256sum] = "fc39e4e502e15e459ff30911ac3b2490f44173ef974cf970592f9c0841115b24"
SRC_URI[python3-openmpi.sha256sum] = "ad2ec7531747e26c4e80610c97701aae3851ba1a0f1744b51b891f1c7d99f705"
