SUMMARY = "generated recipe based on zlib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_zlib = "libz.so.1"
RPM_SONAME_REQ_zlib = "libc.so.6"
RDEPENDS_zlib = "glibc"
RPM_SONAME_REQ_zlib-devel = "libz.so.1"
RPROVIDES_zlib-devel = "zlib-dev (= 1.2.11)"
RDEPENDS_zlib-devel = "pkgconf-pkg-config zlib"
RDEPENDS_zlib-static = "zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/zlib-1.2.11-16.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/zlib-devel-1.2.11-16.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/zlib-static-1.2.11-16.el8_2.x86_64.rpm \
          "

SRC_URI[zlib.sha256sum] = "9fcab8d93bd6164535362c65b783df8b3b2f761746fee3bd8f5bb0b4d1fd8df7"
SRC_URI[zlib-devel.sha256sum] = "09c85a1b82bdfb0440fa92c2742864b3325a1d1ad3d7c5f8f3d4a9be63715049"
SRC_URI[zlib-static.sha256sum] = "aa916e619019195afe3298ab447acf06b96ad419070338adab9ca7f65c068139"
