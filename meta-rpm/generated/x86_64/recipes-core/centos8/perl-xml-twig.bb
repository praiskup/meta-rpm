SUMMARY = "generated recipe based on perl-XML-Twig srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-Twig = "perl-Carp perl-Encode perl-File-Temp perl-Getopt-Long perl-HTML-Tree perl-IO-stringy perl-PathTools perl-Pod-Usage perl-Scalar-List-Utils perl-Text-Tabs+Wrap perl-XML-Parser perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-XML-Twig-3.52-7.el8.noarch.rpm \
          "

SRC_URI[perl-XML-Twig.sha256sum] = "5f674f1884f5ee596d35718eac2301ebf9214b73fd24870b0026bb031a18c52a"
