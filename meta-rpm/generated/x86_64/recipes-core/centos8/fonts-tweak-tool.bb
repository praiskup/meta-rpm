SUMMARY = "generated recipe based on fonts-tweak-tool srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_fonts-tweak-tool = "libfontstweak-resources.so.0"
RPM_SONAME_REQ_fonts-tweak-tool = "libc.so.6 libpthread.so.0"
RDEPENDS_fonts-tweak-tool = "glibc gtk3 hicolor-icon-theme libeasyfc-gobject platform-python python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fonts-tweak-tool-0.4.5-3.el8.x86_64.rpm \
          "

SRC_URI[fonts-tweak-tool.sha256sum] = "77725aa5aa5dc73a069d601bc20a55698151ff605d949ac51b5f626c75c73343"
