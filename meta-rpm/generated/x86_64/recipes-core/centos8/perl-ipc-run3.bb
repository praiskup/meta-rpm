SUMMARY = "generated recipe based on perl-IPC-Run3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-Run3 = "perl-Carp perl-Exporter perl-File-Temp perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-IPC-Run3-0.048-12.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-Run3.sha256sum] = "ea165c017caaf756264d4e04de27bd680b4178da20fe5076ae824b38ca694911"
