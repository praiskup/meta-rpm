SUMMARY = "generated recipe based on maven-artifact-transfer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-artifact-transfer = "apache-commons-codec java-1.8.0-openjdk-headless javapackages-filesystem maven-common-artifact-filters maven-lib maven-resolver-util plexus-containers-component-annotations plexus-utils slf4j"
RDEPENDS_maven-artifact-transfer-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-artifact-transfer-0.9.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-artifact-transfer-javadoc-0.9.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-artifact-transfer.sha256sum] = "9fac33b73509857bd5ee497a1a00aaff06e5afca256820aa18d2c33acc85778c"
SRC_URI[maven-artifact-transfer-javadoc.sha256sum] = "9d69dcda21ed9e660790669867b972483f3f582f62ed53fbdaab7cab1dd59978"
