SUMMARY = "generated recipe based on jansson srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_jansson = "libjansson.so.4"
RPM_SONAME_REQ_jansson = "libc.so.6"
RDEPENDS_jansson = "glibc"
RPM_SONAME_REQ_jansson-devel = "libjansson.so.4"
RPROVIDES_jansson-devel = "jansson-dev (= 2.11)"
RDEPENDS_jansson-devel = "jansson pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jansson-devel-2.11-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/jansson-2.11-3.el8.x86_64.rpm \
          "

SRC_URI[jansson.sha256sum] = "a06e1d34df03aaf429d290d5c281356fefe0ad510c229189405b88b3c0f40374"
SRC_URI[jansson-devel.sha256sum] = "7b597d41de7f3516bc4f9cda66d6c43a51325cb73592a9c8fa37a220791e9e7d"
