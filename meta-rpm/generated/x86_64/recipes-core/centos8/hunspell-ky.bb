SUMMARY = "generated recipe based on hunspell-ky srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ky = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ky-0.20090415-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-ky.sha256sum] = "e40a004123d41b12c48abbc423c19f7de6a474342e986ff64c32569196745f6f"
