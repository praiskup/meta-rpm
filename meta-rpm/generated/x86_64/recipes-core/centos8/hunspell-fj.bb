SUMMARY = "generated recipe based on hunspell-fj srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fj = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-fj-1.2-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-fj.sha256sum] = "2cc8bc3bce946c7584af3f777440ff727d224520c0ba2f0bd9d21e0524acbbf5"
