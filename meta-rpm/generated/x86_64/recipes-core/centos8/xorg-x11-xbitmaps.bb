SUMMARY = "generated recipe based on xorg-x11-xbitmaps srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_xorg-x11-xbitmaps = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-xbitmaps-1.1.1-13.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-xbitmaps.sha256sum] = "89361b84005f6e199d764b9b30b0fffcd90431ee1c79e289643d03bf80a1dab8"
