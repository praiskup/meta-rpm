SUMMARY = "generated recipe based on git srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "curl expat glib-2.0 libpcre2 libsecret openssl pkgconfig-native zlib"
RPM_SONAME_REQ_git = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libsecret-1.so.0"
RDEPENDS_git = "bash emacs-filesystem git-core git-core-doc glib2 glibc libsecret perl-Getopt-Long perl-Git perl-PathTools perl-TermReadKey perl-interpreter perl-libs platform-python"
RDEPENDS_git-all = "git git-email git-gui git-instaweb git-subtree git-svn gitk perl-Git perl-TermReadKey"
RPM_SONAME_REQ_git-core = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libexpat.so.1 libpcre2-8.so.0 libpthread.so.0 librt.so.1 libssl.so.1.1 libz.so.1"
RDEPENDS_git-core = "bash expat glibc less libcurl openssh-clients openssl-libs pcre2 zlib"
RDEPENDS_git-core-doc = "git-core"
RPM_SONAME_REQ_git-daemon = "libc.so.6 libcrypto.so.1.1 libpcre2-8.so.0 libpthread.so.0 librt.so.1 libz.so.1"
RDEPENDS_git-daemon = "bash git-core glibc openssl-libs pcre2 systemd zlib"
RDEPENDS_git-email = "git perl-Authen-SASL perl-Error perl-File-Temp perl-Getopt-Long perl-Git perl-MailTools perl-Net-SMTP-SSL perl-PathTools perl-Term-ANSIColor perl-Text-ParseWords perl-interpreter perl-libnet perl-libs"
RDEPENDS_git-gui = "bash gitk tk"
RDEPENDS_git-instaweb = "bash git gitweb httpd"
RDEPENDS_git-subtree = "bash git-core"
RPM_SONAME_REQ_git-svn = "libc.so.6 libcrypto.so.1.1 libpcre2-8.so.0 libpthread.so.0 librt.so.1 libz.so.1"
RDEPENDS_git-svn = "git glibc openssl-libs pcre2 perl-Carp perl-Digest-MD5 perl-File-Path perl-Getopt-Long perl-Git perl-Git-SVN perl-Memoize perl-PathTools perl-TermReadKey perl-interpreter perl-libs zlib"
RDEPENDS_gitk = "bash git tk"
RDEPENDS_gitweb = "git perl-CGI perl-Digest-MD5 perl-Encode perl-Time-HiRes perl-constant perl-interpreter perl-libs"
RDEPENDS_perl-Git = "git perl-Carp perl-Error perl-Exporter perl-File-Temp perl-PathTools perl-Time-Local perl-interpreter perl-libs"
RDEPENDS_perl-Git-SVN = "git perl-Carp perl-Exporter perl-File-Path perl-Git perl-Memoize perl-Storable perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-2.18.4-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-all-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-core-2.18.4-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-core-doc-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-daemon-2.18.4-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-email-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-gui-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-instaweb-2.18.4-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-subtree-2.18.4-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/git-svn-2.18.4-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gitk-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gitweb-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Git-2.18.4-2.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Git-SVN-2.18.4-2.el8_2.noarch.rpm \
          "

SRC_URI[git.sha256sum] = "8213f5ce72e131ed6bb40c3259f18291dc773bb10f73dc75e0786c7261becb10"
SRC_URI[git-all.sha256sum] = "665ea52c3484153ef914efeb42c3bc486b2206669935ff01fa4434fea843c0b6"
SRC_URI[git-core.sha256sum] = "5db95c6e9931974d9aa8ae198954b500a5de8a6fa5daf8e264597de4c2428198"
SRC_URI[git-core-doc.sha256sum] = "1a0a16f37c5589149cbe9fd7f611128189c93a0208699013fb6ff83bb0f02bbb"
SRC_URI[git-daemon.sha256sum] = "67d770b9883af02637b569d3bff230bc9ef6c319f8f2ac22370d0e232f95c1d8"
SRC_URI[git-email.sha256sum] = "19f8e7dc41962fdfa828082c815694190fc2b2f34d1bd2453f0fcba63adb5b85"
SRC_URI[git-gui.sha256sum] = "3663897a8c590682c9f214f331541e296ab4cd8dd22a7473f61562bb98443f6e"
SRC_URI[git-instaweb.sha256sum] = "e6fe858acb92aa4279834a05187306e64d131c220d5d6a30a305f9bcde954c5c"
SRC_URI[git-subtree.sha256sum] = "9f04c4298d8d1b28cb5f2280d0cf3c6727584b2f54937017ee81a894ce0cb4bb"
SRC_URI[git-svn.sha256sum] = "36408bcbbb0878e791feb993d6fb0ecbf22d9cc2ebc6c804c47fae1d76861d84"
SRC_URI[gitk.sha256sum] = "987d2188e5e039b7846295fb6b0b65f4f7d07d180b34bbbdc73b0e243c67b144"
SRC_URI[gitweb.sha256sum] = "5ec13e2478620d1e15b81526edeb7f16c7d21400fe486493e6179657b7610615"
SRC_URI[perl-Git.sha256sum] = "bf7083fa81e001b2127df644e89b0cee819309b1bb7fa562786ed6b4ffc16ec4"
SRC_URI[perl-Git-SVN.sha256sum] = "5544406f5dc5e4387d573d03129586e9eb3ded9832028cea961d450f291cbbf2"
