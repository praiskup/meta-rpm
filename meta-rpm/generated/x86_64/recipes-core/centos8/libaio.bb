SUMMARY = "generated recipe based on libaio srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libaio = "libaio.so.1 libaio.so.1.0.0"
RPM_SONAME_REQ_libaio = "libc.so.6"
RDEPENDS_libaio = "glibc"
RPM_SONAME_REQ_libaio-devel = "libaio.so.1"
RPROVIDES_libaio-devel = "libaio-dev (= 0.3.112)"
RDEPENDS_libaio-devel = "libaio"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libaio-0.3.112-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libaio-devel-0.3.112-1.el8.x86_64.rpm \
          "

SRC_URI[libaio.sha256sum] = "2c63399bee449fb6e921671a9bbf3356fda73f890b578820f7d926202e98a479"
SRC_URI[libaio-devel.sha256sum] = "7738d2df1f367491cc4043da5ae35aa2fb76905516d50b0e97dc8f38ceae1997"
