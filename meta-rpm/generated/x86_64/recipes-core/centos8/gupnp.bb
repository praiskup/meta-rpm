SUMMARY = "generated recipe based on gupnp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "glib-2.0 gssdp libsoup-2.4 libuuid libxml2 pkgconfig-native"
RPM_SONAME_PROV_gupnp = "libgupnp-1.0.so.4"
RPM_SONAME_REQ_gupnp = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgssdp-1.0.so.3 libpthread.so.0 libsoup-2.4.so.1 libuuid.so.1 libxml2.so.2"
RDEPENDS_gupnp = "dbus glib2 glibc gssdp libsoup libuuid libxml2"
RPM_SONAME_REQ_gupnp-devel = "libgupnp-1.0.so.4"
RPROVIDES_gupnp-devel = "gupnp-dev (= 1.0.3)"
RDEPENDS_gupnp-devel = "gssdp-devel gupnp libsoup-devel libuuid-devel libxml2-devel pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gupnp-1.0.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gupnp-devel-1.0.3-2.el8.x86_64.rpm \
          "

SRC_URI[gupnp.sha256sum] = "9fe27966b20ab489316adc214e8edd1a724240f47d3f699cb98b533920128d50"
SRC_URI[gupnp-devel.sha256sum] = "16bbcace1ef7f0ff13da5aecfa346e1100666af1359833a72821b2369cf32265"
