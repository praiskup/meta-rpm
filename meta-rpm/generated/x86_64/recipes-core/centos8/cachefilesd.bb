SUMMARY = "generated recipe based on cachefilesd srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cachefilesd = "libc.so.6"
RDEPENDS_cachefilesd = "bash glibc selinux-policy-minimum systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cachefilesd-0.10.10-4.el8.x86_64.rpm \
          "

SRC_URI[cachefilesd.sha256sum] = "07139488664989e19e6601a96085d0b8cdd92bb21e2b8a7d76c497232ba55e89"
