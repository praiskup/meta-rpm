SUMMARY = "generated recipe based on isl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gmp pkgconfig-native"
RPM_SONAME_PROV_isl = "libisl.so.13 libisl.so.15"
RPM_SONAME_REQ_isl = "libc.so.6 libgmp.so.10"
RDEPENDS_isl = "glibc gmp"
RPM_SONAME_REQ_isl-devel = "libisl.so.15"
RPROVIDES_isl-devel = "isl-dev (= 0.16.1)"
RDEPENDS_isl-devel = "gmp-devel isl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/isl-0.16.1-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/isl-devel-0.16.1-6.el8.x86_64.rpm \
          "

SRC_URI[isl.sha256sum] = "0cbdbdf53c8c12f48493bdae47d2bda45425011e67801a5827d164d6e10759ae"
SRC_URI[isl-devel.sha256sum] = "1dab8ddacac91128490303fefb3f5187861224beafd4a1936aa19e64cda2d917"
