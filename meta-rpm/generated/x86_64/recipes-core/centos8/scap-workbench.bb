SUMMARY = "generated recipe based on scap-workbench srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libgcc openscap pkgconfig-native qt5-qtbase qt5-qtxmlpatterns"
RPM_SONAME_REQ_scap-workbench = "libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Widgets.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libopenscap.so.25 libstdc++.so.6"
RDEPENDS_scap-workbench = "bash glibc libgcc libstdc++ openscap openscap-utils openssh-askpass openssh-clients polkit qt5-qtbase qt5-qtbase-gui qt5-qtxmlpatterns scap-security-guide util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/scap-workbench-1.2.0-4.el8.x86_64.rpm \
          "

SRC_URI[scap-workbench.sha256sum] = "f6ea604cd2ab2c8066e29f3d5279c083982af722aa38d805c70b900a5ea5a047"
