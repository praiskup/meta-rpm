SUMMARY = "generated recipe based on perl-Term-Size-Perl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Size-Perl = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Term-Size-Perl-0.031-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Term-Size-Perl.sha256sum] = "11ac94610db8ff012c4d255f688e74327b6729425546e8a5c90ae43cc339b03b"
