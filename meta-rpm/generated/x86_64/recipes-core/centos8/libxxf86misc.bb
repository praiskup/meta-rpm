SUMMARY = "generated recipe based on libXxf86misc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXxf86misc = "libXxf86misc.so.1"
RPM_SONAME_REQ_libXxf86misc = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXxf86misc = "glibc libX11 libXext"
RPM_SONAME_REQ_libXxf86misc-devel = "libXxf86misc.so.1"
RPROVIDES_libXxf86misc-devel = "libXxf86misc-dev (= 1.0.4)"
RDEPENDS_libXxf86misc-devel = "libX11-devel libXext-devel libXxf86misc pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXxf86misc-1.0.4-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXxf86misc-devel-1.0.4-1.el8.x86_64.rpm \
          "

SRC_URI[libXxf86misc.sha256sum] = "08ce46ea8c7958627b017cc489ea264e131a535f23cc2eae157ab3e6f5bf2d11"
SRC_URI[libXxf86misc-devel.sha256sum] = "12fb8bbcb537e7947aa26954b1e8296d3fbf5f3e854f0f21275607be2e2f83fe"
