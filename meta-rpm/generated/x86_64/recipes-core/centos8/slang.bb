SUMMARY = "generated recipe based on slang srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_slang = "libslang.so.2"
RPM_SONAME_REQ_slang = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_slang = "glibc"
RPM_SONAME_REQ_slang-devel = "libslang.so.2"
RPROVIDES_slang-devel = "slang-dev (= 2.3.2)"
RDEPENDS_slang-devel = "pkgconf-pkg-config slang"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/slang-devel-2.3.2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/slang-2.3.2-3.el8.x86_64.rpm \
          "

SRC_URI[slang.sha256sum] = "be628d396303d6547b9d7239897fbf4fad7447375cb5e898b394a458f420b550"
SRC_URI[slang-devel.sha256sum] = "27c1ccc6fa3f07521d2201cfc9addd592f5417d57c196591a64a7c142482196e"
