SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_policycoreutils-gui = "bash gtk3 platform-python policycoreutils-dbus policycoreutils-devel python3-gobject python3-policycoreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/policycoreutils-gui-2.9-9.el8.noarch.rpm \
          "

SRC_URI[policycoreutils-gui.sha256sum] = "fa3d4862eb1cb53cd83f214d8afd847f2bfe76ad045edf2bd675fb0a46a750df"
