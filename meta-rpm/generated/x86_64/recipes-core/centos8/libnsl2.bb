SUMMARY = "generated recipe based on libnsl2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "libtirpc pkgconfig-native"
RPM_SONAME_PROV_libnsl2 = "libnsl.so.2"
RPM_SONAME_REQ_libnsl2 = "ld-linux-x86-64.so.2 libc.so.6 libtirpc.so.3"
RDEPENDS_libnsl2 = "glibc libtirpc"
RPM_SONAME_REQ_libnsl2-devel = "libnsl.so.2"
RPROVIDES_libnsl2-devel = "libnsl2-dev (= 1.2.0)"
RDEPENDS_libnsl2-devel = "libnsl2 libtirpc-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnsl2-1.2.0-2.20180605git4a062cf.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnsl2-devel-1.2.0-2.20180605git4a062cf.el8.x86_64.rpm \
          "

SRC_URI[libnsl2.sha256sum] = "5846c73edfa2ff673989728e9621cce6a1369eb2f8a269ac5205c381a10d327a"
SRC_URI[libnsl2-devel.sha256sum] = "2baa1d5b8580ba38b81bd09601dbe7484b6364e2e6a47f280fabc7baf4e3543a"
