SUMMARY = "generated recipe based on spice-vdagent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "alsa-lib dbus-libs glib-2.0 libdrm libpciaccess libx11 libxfixes libxinerama libxrandr pkgconfig-native systemd-libs"
RPM_SONAME_REQ_spice-vdagent = "libX11.so.6 libXfixes.so.3 libXinerama.so.1 libXrandr.so.2 libasound.so.2 libc.so.6 libdbus-1.so.3 libdrm.so.2 libglib-2.0.so.0 libpciaccess.so.0 libsystemd.so.0"
RDEPENDS_spice-vdagent = "alsa-lib bash dbus-libs glib2 glibc libX11 libXfixes libXinerama libXrandr libdrm libpciaccess systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-vdagent-0.19.0-3.el8.x86_64.rpm \
          "

SRC_URI[spice-vdagent.sha256sum] = "1a9ecc9c0af12b6057de483ad5906e75766f2449c5e741ce61eb7d185cc949cf"
