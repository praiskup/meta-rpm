SUMMARY = "generated recipe based on ibus-m17n srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 ibus m17n-lib pango pkgconfig-native"
RPM_SONAME_REQ_ibus-m17n = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libibus-1.0.so.5 libm17n-core.so.0 libm17n.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_ibus-m17n = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 ibus ibus-libs m17n-lib pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-m17n-1.3.4-26.el8.x86_64.rpm \
          "

SRC_URI[ibus-m17n.sha256sum] = "4533193a10dc0ed31f400b5bbe4f6f6b8dea24ef3bd6b42e3149e052da35a379"
