SUMMARY = "generated recipe based on madan-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_madan-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/madan-fonts-2.000-20.el8.noarch.rpm \
          "

SRC_URI[madan-fonts.sha256sum] = "98630cf45e0ea6d528e286465065b91c51c1fa56e1997fc8fca6dff16686570f"
