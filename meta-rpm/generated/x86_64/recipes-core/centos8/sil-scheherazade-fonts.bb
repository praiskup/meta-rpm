SUMMARY = "generated recipe based on sil-scheherazade-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-scheherazade-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-scheherazade-fonts-2.100-5.el8.noarch.rpm \
          "

SRC_URI[sil-scheherazade-fonts.sha256sum] = "12260b45041120845b0c2510febdc41a2031811e3e16e9e86492eb974c821b67"
