SUMMARY = "generated recipe based on zsh srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "gdbm libpcre ncurses pkgconfig-native"
RPM_SONAME_REQ_zsh = "libc.so.6 libdl.so.2 libgdbm.so.6 libm.so.6 libncursesw.so.6 libpcre.so.1 librt.so.1 libtinfo.so.6"
RDEPENDS_zsh = "bash coreutils gdbm-libs glibc grep info ncurses-libs pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/zsh-html-5.5.1-6.el8_1.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/zsh-5.5.1-6.el8_1.2.x86_64.rpm \
          "

SRC_URI[zsh.sha256sum] = "84063349dd6ff4c9bc2d364cf0666b46d18cdee1ad20a793ad00464ae432a6e2"
SRC_URI[zsh-html.sha256sum] = "4688983035295c77ca41a830ff5152bbc188d3650742461b6794894f8cc7c692"
