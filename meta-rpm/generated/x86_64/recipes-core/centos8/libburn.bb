SUMMARY = "generated recipe based on libburn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libburn = "libburn.so.4"
RPM_SONAME_REQ_libburn = "libc.so.6 libpthread.so.0"
RDEPENDS_libburn = "glibc"
RPM_SONAME_REQ_libburn-devel = "libburn.so.4"
RPROVIDES_libburn-devel = "libburn-dev (= 1.4.8)"
RDEPENDS_libburn-devel = "libburn pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libburn-1.4.8-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libburn-devel-1.4.8-3.el8.x86_64.rpm \
          "

SRC_URI[libburn.sha256sum] = "d4b0815ced6c1ec209b78fee4e2c1ee74efcd401d5462268b47d94a28ebfaf31"
SRC_URI[libburn-devel.sha256sum] = "5c4192222e9e860b05d6beeb44f7caf5f08b8548cec71ab90161d825e5bafe53"
