SUMMARY = "generated recipe based on opa-ff srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "expat libgcc ncurses openssl pkgconfig-native rdma-core zlib"
RPM_SONAME_PROV_opa-address-resolution = "libdsap.so.1.0.0 libopasadb.so.1.0.0"
RPM_SONAME_REQ_opa-address-resolution = "libc.so.6 libcrypto.so.1.1 libexpat.so.1 libgcc_s.so.1 libibumad.so.3 libibverbs.so.1 libm.so.6 libopasadb.so.1.0.0 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6"
RDEPENDS_opa-address-resolution = "expat glibc ibacm libgcc libibumad libibverbs libstdc++ opa-basic-tools openssl-libs"
RPM_SONAME_REQ_opa-basic-tools = "libc.so.6 libcrypto.so.1.1 libexpat.so.1 libgcc_s.so.1 libgomp.so.1 libibumad.so.3 libibverbs.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1"
RDEPENDS_opa-basic-tools = "bash bc expat glibc libgcc libgomp libibumad libibverbs openssl-libs perl-interpreter rdma-core tcl"
RPM_SONAME_PROV_opa-fastfabric = "libqlgc_fork.so"
RPM_SONAME_REQ_opa-fastfabric = "libc.so.6 libcrypto.so.1.1 libexpat.so.1 libgcc_s.so.1 libibumad.so.3 libibverbs.so.1 libm.so.6 libncurses.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_opa-fastfabric = "bash expat expect glibc libgcc libibumad libibverbs libstdc++ ncurses-libs opa-basic-tools openssl-libs perl-interpreter tcl zlib"
RPM_SONAME_PROV_opa-libopamgt = "libopamgt.so.0"
RPM_SONAME_REQ_opa-libopamgt = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libibumad.so.3 libibverbs.so.1 libm.so.6 libopamgt.so.0 libpthread.so.0 libssl.so.1.1 libstdc++.so.6"
RDEPENDS_opa-libopamgt = "glibc libgcc libibumad libibverbs libstdc++ openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opa-address-resolution-10.10.0.0.445-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opa-basic-tools-10.10.0.0.445-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opa-fastfabric-10.10.0.0.445-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opa-libopamgt-10.10.0.0.445-1.el8.x86_64.rpm \
          "

SRC_URI[opa-address-resolution.sha256sum] = "501bc5e809a66136867a7984764d024c2d880cc20274848928fef8ce3b20f60d"
SRC_URI[opa-basic-tools.sha256sum] = "8621b717362ada62dc60d01a0b81a1b99105748c7328414039eb07e22797b077"
SRC_URI[opa-fastfabric.sha256sum] = "6e6459ab7e4408edbe61f20951872e90cc35a2c18b4749307a8b6d0c9b4ab9f8"
SRC_URI[opa-libopamgt.sha256sum] = "984c8779e1d5ba23916172ddb1cdc3cda574476470f930a72275fb52de0d1a09"
