SUMMARY = "generated recipe based on exec-maven-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_exec-maven-plugin = "apache-commons-exec java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-artifact-manager maven-lib maven-model maven-project maven-toolchain plexus-utils"
RDEPENDS_exec-maven-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/exec-maven-plugin-1.6.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/exec-maven-plugin-javadoc-1.6.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[exec-maven-plugin.sha256sum] = "2fb7f9ba03bf6a83486da3ddcbf530c46e37072b35b792ca8d9518600d44299e"
SRC_URI[exec-maven-plugin-javadoc.sha256sum] = "e1d5e7f1db2646f667121aa9a7bab651c418f7dfd99f3eac573ca7a624340b67"
