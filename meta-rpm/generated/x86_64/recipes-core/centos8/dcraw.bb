SUMMARY = "generated recipe based on dcraw srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "jasper lcms2 libjpeg-turbo pkgconfig-native"
RPM_SONAME_REQ_dcraw = "libc.so.6 libjasper.so.4 libjpeg.so.62 liblcms2.so.2 libm.so.6"
RDEPENDS_dcraw = "glibc jasper-libs lcms2 libjpeg-turbo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dcraw-9.27.0-9.el8.x86_64.rpm \
          "

SRC_URI[dcraw.sha256sum] = "0dade72893d1d9ce8f677d15ed1a5f3c29b8c1dc67c18d8b3886f276424314c3"
