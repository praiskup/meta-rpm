SUMMARY = "generated recipe based on lldb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "clang libedit libgcc libxml2 llvm ncurses pkgconfig-native platform-python3"
RPM_SONAME_PROV_lldb = "liblldb.so.9 liblldbIntelFeatures.so.9"
RPM_SONAME_REQ_lldb = "ld-linux-x86-64.so.2 libLLVM-9.so libc.so.6 libclangAST.so.9 libclangBasic.so.9 libclangCodeGen.so.9 libclangDriver.so.9 libclangEdit.so.9 libclangFrontend.so.9 libclangLex.so.9 libclangParse.so.9 libclangRewrite.so.9 libclangRewriteFrontend.so.9 libclangSema.so.9 libclangSerialization.so.9 libclangTooling.so.9 libdl.so.2 libedit.so.0 libform.so.6 libgcc_s.so.1 liblldb.so.9 libm.so.6 libncurses.so.6 libpanel.so.6 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6 libtinfo.so.6 libxml2.so.2"
RDEPENDS_lldb = "clang-libs glibc libedit libgcc libstdc++ libxml2 llvm-libs ncurses-libs python3-libs python3-lldb"
RPM_SONAME_REQ_lldb-devel = "liblldb.so.9 liblldbIntelFeatures.so.9"
RPROVIDES_lldb-devel = "lldb-dev (= 9.0.1)"
RDEPENDS_lldb-devel = "lldb"
RPM_SONAME_REQ_python3-lldb = "liblldb.so.9"
RDEPENDS_python3-lldb = "lldb platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lldb-9.0.1-2.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lldb-devel-9.0.1-2.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-lldb-9.0.1-2.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
          "

SRC_URI[lldb.sha256sum] = "7f4a54af69285abbe5acf55d1e31e65c128af5f0c9815e16d431ce4cc1050450"
SRC_URI[lldb-devel.sha256sum] = "55e7a5e6eaddab8f5f302da7638374e87a021d074a558b965e248c4a619f03ca"
SRC_URI[python3-lldb.sha256sum] = "b46f833af48bad0a20aca0824bd43675911fd5ec59e312bbb41f26514c793157"
