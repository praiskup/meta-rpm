SUMMARY = "generated recipe based on nfs-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "device-mapper-libs e2fsprogs keyutils krb5-libs libblkid libcap libevent libmount libtirpc libxml2 openldap pkgconfig-native sqlite3"
RPM_SONAME_PROV_libnfsidmap = "libnfsidmap.so.1"
RPM_SONAME_REQ_libnfsidmap = "libc.so.6 libdl.so.2 libldap-2.4.so.2 libresolv.so.2 libtirpc.so.3"
RDEPENDS_libnfsidmap = "glibc libtirpc openldap"
RPM_SONAME_REQ_libnfsidmap-devel = "libnfsidmap.so.1"
RPROVIDES_libnfsidmap-devel = "libnfsidmap-dev (= 2.3.3)"
RDEPENDS_libnfsidmap-devel = "libnfsidmap pkgconf-pkg-config"
RPM_SONAME_REQ_nfs-utils = "libblkid.so.1 libc.so.6 libcap.so.2 libcom_err.so.2 libdevmapper.so.1.02 libdl.so.2 libevent-2.1.so.6 libgssapi_krb5.so.2 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 libmount.so.1 libnfsidmap.so.1 libpthread.so.0 libresolv.so.2 libsqlite3.so.0 libtirpc.so.3 libxml2.so.2"
RDEPENDS_nfs-utils = "bash coreutils device-mapper-libs gawk glibc grep gssproxy keyutils keyutils-libs kmod krb5-libs libblkid libcap libcom_err libevent libmount libnfsidmap libtirpc libxml2 platform-python quota rpcbind sed shadow-utils sqlite-libs systemd util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnfsidmap-2.3.3-31.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/nfs-utils-2.3.3-31.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libnfsidmap-devel-2.3.3-31.el8.x86_64.rpm \
          "

SRC_URI[libnfsidmap.sha256sum] = "b4d59ab9da9d278d7ec7e5d8674db0d35f5de02d975b4a164534d81797ac2d7d"
SRC_URI[libnfsidmap-devel.sha256sum] = "1f965dcfbedaf2fd50a6ead70fef783cbaaa15685c94dc46c62737919b7c11b3"
SRC_URI[nfs-utils.sha256sum] = "e315ee9483f00f73d2b01a0c2062dd5dcb84adb1c70abf9dbffebb73d173e44e"
