SUMMARY = "generated recipe based on perl-Test-Requires srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Requires = "perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Requires-0.10-10.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Requires.sha256sum] = "4319669bc00fc8b49a3216ac4e2985b17377e69edff65d65d061357f5e3b0b03"
