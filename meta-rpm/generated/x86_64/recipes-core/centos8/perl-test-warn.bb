SUMMARY = "generated recipe based on perl-Test-Warn srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Warn = "perl-Carp perl-Exporter perl-Sub-Uplevel perl-Test-Simple perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Warn-0.32-5.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Warn.sha256sum] = "13c0387926bc39ccaeef9e3f976cafb67e02d0c544c6058824cb7281b4d76884"
