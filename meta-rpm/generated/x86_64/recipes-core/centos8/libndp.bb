SUMMARY = "generated recipe based on libndp srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libndp = "libndp.so.0"
RPM_SONAME_REQ_libndp = "libc.so.6 libndp.so.0"
RDEPENDS_libndp = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libndp-1.7-3.el8.x86_64.rpm \
          "

SRC_URI[libndp.sha256sum] = "c357a350aa169402db3c898c7edcfee333e1d11a44f62bbeaba3fd3d2f31644d"
