SUMMARY = "generated recipe based on librepository srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_librepository = "java-1.8.0-openjdk-headless javapackages-tools libbase"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/librepository-1.1.3-17.el8.noarch.rpm \
          "

SRC_URI[librepository.sha256sum] = "bc963b006234cf2406169f36e20927748d9b6111391e1c14e9936ea5569d73cd"
