SUMMARY = "generated recipe based on libverto srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libverto = "libverto.so.1"
RPM_SONAME_REQ_libverto = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_libverto = "glibc"
RPM_SONAME_REQ_libverto-devel = "libverto.so.1"
RPROVIDES_libverto-devel = "libverto-dev (= 0.3.0)"
RDEPENDS_libverto-devel = "libverto pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libverto-0.3.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libverto-devel-0.3.0-5.el8.x86_64.rpm \
          "

SRC_URI[libverto.sha256sum] = "f95f673fc9236dc712270a343807cdac06297d847001e78cd707482c751b2d0d"
SRC_URI[libverto-devel.sha256sum] = "d1b5c2a86b43e7a7cd5b23f81ae9e8ef9f690601a05d294f1fee7a70909c4b2a"
