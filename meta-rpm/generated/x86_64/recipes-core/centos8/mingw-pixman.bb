SUMMARY = "generated recipe based on mingw-pixman srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-pixman = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-pkg-config"
RDEPENDS_mingw64-pixman = "mingw64-crt mingw64-filesystem mingw64-gcc mingw64-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-pixman-0.34.0-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-pixman-0.34.0-5.el8.noarch.rpm \
          "

SRC_URI[mingw32-pixman.sha256sum] = "3b08309e8ba2d4205c0d7e276d8d63772c017eef8390dc6056d250d50a15f9cc"
SRC_URI[mingw64-pixman.sha256sum] = "b145b6fcc09007a189fbbd2e04483f247849ae8f47076f32f54fd4b79f4b6f30"
