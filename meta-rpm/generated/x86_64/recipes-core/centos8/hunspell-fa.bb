SUMMARY = "generated recipe based on hunspell-fa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fa = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-fa-0.20070116-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-fa.sha256sum] = "f1d6d589254bdc3a9ee4622fa4f624067c391c1c7ca1dd58a0cb421573c02202"
