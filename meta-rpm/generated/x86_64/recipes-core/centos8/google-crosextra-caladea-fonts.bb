SUMMARY = "generated recipe based on google-crosextra-caladea-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_google-crosextra-caladea-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-crosextra-caladea-fonts-1.002-0.10.20130214.el8.noarch.rpm \
          "

SRC_URI[google-crosextra-caladea-fonts.sha256sum] = "bbe1927db54119e6886b4ae92dcc277d5aed955fd6a7527b88847bb494654141"
