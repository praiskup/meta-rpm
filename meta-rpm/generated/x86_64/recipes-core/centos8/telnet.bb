SUMMARY = "generated recipe based on telnet srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_telnet = "libc.so.6 libncurses.so.6 libtinfo.so.6 libutil.so.1"
RDEPENDS_telnet = "glibc ncurses-libs"
RPM_SONAME_REQ_telnet-server = "libc.so.6 libutil.so.1"
RDEPENDS_telnet-server = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/telnet-0.17-73.el8_1.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/telnet-server-0.17-73.el8_1.1.x86_64.rpm \
          "

SRC_URI[telnet.sha256sum] = "0b01d9c4e791414d06554a1c11d050f0548ebf6c754e22e848b692918feab708"
SRC_URI[telnet-server.sha256sum] = "4edef37f30c2f43a7751841770ec13a97c4ca85b1609f2828d3608bfd99486c9"
