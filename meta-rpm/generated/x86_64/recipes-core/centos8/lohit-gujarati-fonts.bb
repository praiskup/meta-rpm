SUMMARY = "generated recipe based on lohit-gujarati-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-gujarati-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-gujarati-fonts-2.92.4-3.el8.noarch.rpm \
          "

SRC_URI[lohit-gujarati-fonts.sha256sum] = "caefcb5ff693bada745f3a4886d2c8f3690e210a55490a099a9ff223d87ba26d"
