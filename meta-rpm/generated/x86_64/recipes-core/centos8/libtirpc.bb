SUMMARY = "generated recipe based on libtirpc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
PROVIDES = "virtual/librpc"
DEPENDS = "e2fsprogs krb5-libs pkgconfig-native"
RPM_SONAME_PROV_libtirpc = "libtirpc.so.3"
RPM_SONAME_REQ_libtirpc = "libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpthread.so.0"
RDEPENDS_libtirpc = "glibc krb5-libs libcom_err"
RPM_SONAME_REQ_libtirpc-devel = "libtirpc.so.3"
RPROVIDES_libtirpc-devel = "libtirpc-dev (= 1.1.4)"
RDEPENDS_libtirpc-devel = "bash libtirpc man-db pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtirpc-1.1.4-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtirpc-devel-1.1.4-4.el8.x86_64.rpm \
          "

SRC_URI[libtirpc.sha256sum] = "4d7acc5861e8fbd998d0b76e93694f2b152fe98e7782cc4307a2d3103e987d11"
SRC_URI[libtirpc-devel.sha256sum] = "4d3a43db83a983b7a375f18c87a9cc0298867e875b11571496ce42eaa3653d75"
