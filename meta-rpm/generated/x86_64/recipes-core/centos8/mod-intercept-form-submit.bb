SUMMARY = "generated recipe based on mod_intercept_form_submit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

inherit rpmbased
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mod_intercept_form_submit = "libc.so.6"
RDEPENDS_mod_intercept_form_submit = "glibc httpd mod_authnz_pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_intercept_form_submit-1.1.0-5.el8.x86_64.rpm \
          "

SRC_URI[mod_intercept_form_submit.sha256sum] = "0db1dbc43a02f3ab89192cf059d3c51c9904b34fbcae73d5d2b67909c7a3d36d"
