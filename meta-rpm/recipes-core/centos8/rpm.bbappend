FILESEXTRAPATHS_prepend := "${THISDIR}/rpm:"

DEPENDS_append_class-native = " file-replacement-native bzip2-replacement-native"

RPM_URI_class-native = " \
        file://python3-rpm-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-apidocs-4.14.2-37.noshortcircuit.el8.noarch.rpm \
        file://rpm-build-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-build-libs-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-cron-4.14.2-37.noshortcircuit.el8.noarch.rpm \
        file://rpm-devel-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-libs-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-plugin-ima-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-plugin-prioreset-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-plugin-selinux-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-plugin-syslog-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-plugin-systemd-inhibit-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
        file://rpm-sign-4.14.2-37.noshortcircuit.el8.${BUILD_ARCH}.rpm \
"

do_install_append() {
    dest = d.getVar('D')
    base_prefix = d.getVar('base_prefix')
    if base_prefix:
       extract_path = os.path.join(dest, base_prefix[1:])
    else:
       extract_path = dest
    os.symlink("../lib/rpm", os.path.join(extract_path, "usr/lib64/rpm"))
    os.symlink("../lib/rpm/rpmdeps", os.path.join(extract_path, "usr/bin/rpmdeps"))
}

WRAPPER_TOOLS = " \
   ${bindir}/rpm \
   ${bindir}/rpm2archive \
   ${bindir}/rpm2cpio \
   ${bindir}/rpmbuild \
   ${bindir}/rpmdb \
   ${bindir}/rpmgraph \
   ${bindir}/rpmkeys \
   ${bindir}/rpmsign \
   ${bindir}/rpmspec \
   ${nonarch_libdir}/rpm/rpmdeps \
"

post_rpm_install_append_class-native() {
        for tool in ${WRAPPER_TOOLS}; do
                test -x ${D}$tool && create_wrapper ${D}$tool \
                        RPM_CONFIGDIR=${STAGING_LIBDIR_NATIVE}/rpm \
                        RPM_ETCCONFIGDIR=${STAGING_DIR_NATIVE} \
                        MAGIC=${STAGING_DIR_NATIVE}${datadir_native}/misc/magic.mgc \
                        RPM_NO_CHROOT_FOR_SCRIPTS=1
        done
}
