This directory contains a rebuild of the Centos8 rpm with an added
patch that disables the generation of the ShortCircuited requirement
when building short-circuited rpms.

This patch is normally added by the rpm in poky, because yocto actually
builds the final rpms with short-circuiting. However, since we're not
building rpm in yocto (currently) we instead have a custom pre-built
rpm with the patch applied.

The patch is in rpm-4.14.2-no-short-circuit.patch, and was built with 
rpm.spec + the other normal contents of the centos8 srpm.
