inherit cross
require ../../generated/${TARGET_ARCH}/recipes-core/centos8/binutils.bb

PROVIDES = "virtual/${TARGET_PREFIX}binutils"

PN = "binutils-cross-${TARGET_ARCH}"
BPN = "binutils"

rpmbased_post_rpm_install() {
   # Compute this before creating bindir below (as it will otherwise get picked up)
   tools=`basename -a ${D}/${prefix}/bin/*`
   mkdir -p ${D}${bindir}
   for tool in $tools; do
       mv ${D}/${prefix}/bin/$tool ${D}${bindir}/${TARGET_PREFIX}$tool
   done

  # "ld" is set using alternatives which does not run in install, so manually create it
  ln -s ${TARGET_PREFIX}ld.bfd ${D}${bindir}/${TARGET_PREFIX}ld
}
