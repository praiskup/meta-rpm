SUMMARY = "generated recipe based on libsolv srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"

# For whatever reason, libsolv-devel is not in the repos, so this is a rebuild of the srpm

inherit rpmbased
RDEPENDS_libsolv = "bzip2-libs openssl-libs xz-libs rpm-libs libxml2 zlib"

RPM_URI = "file://libsolv-0.7.7-1.el8.${BUILD_ARCH}.rpm \
           file://libsolv-devel-0.7.7-1.el8.${BUILD_ARCH}.rpm \
           file://libsolv-tools-0.7.7-1.el8.${BUILD_ARCH}.rpm \
           file://python3-solv-0.7.7-1.el8.${BUILD_ARCH}.rpm \
           file://ruby-solv-0.7.7-1.el8.${BUILD_ARCH}.rpm \
           file://perl-solv-0.7.7-1.el8.${BUILD_ARCH}.rpm \
           file://libsolv-demo-0.7.7-1.el8.${BUILD_ARCH}.rpm \
          "
