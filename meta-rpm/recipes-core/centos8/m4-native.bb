# Yocto ships with a non-virtual m4 file, so we have to do the same or there will
# be a conflcit between virtual:native:m4.bb and m4-native.bb
require ../../generated/${TARGET_ARCH}/recipes-core/centos8/m4.bb


inherit native
