SUMMARY = "RH production image"

LICENSE = "MIT"

inherit rpm-image

IMAGE_INSTALL = "abattis-cantarell-fonts zenity"
